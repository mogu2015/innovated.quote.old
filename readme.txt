#
# IRIS (Grails) Application
#

 Fixes / Changes:
================================================================================
02-10-09
	Made changes to the AuthenticateService class in the Acegi plugin which
	caused a runtime exception when updating Roles, when using a non-RequestMap
	method of securing URL's
	

	