
<%@ page import="com.innovated.iris.domain.Address" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta name="layout" content="main" />
        <title>Show Address</title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${resource(dir:'')}">Home</a></span>
            <span class="menuButton"><g:link class="list" action="list">Address List</g:link></span>
            <span class="menuButton"><g:link class="create" action="create">New Address</g:link></span>
        </div>
        <div class="body">
            <h1>Show Address</h1>
            <g:if test="${flash.message}">
            <div class="message ${flash.iconClass}">${flash.message}</div>
            </g:if>
            <div class="dialog">
                <table>
                    <tbody>
                    	<g:ifAnyGranted role="ROLE_ADMIN,ROLE_SUPER">
                    	<tr class="prop">
                            <td valign="top" class="name">Address For:</td>
                            <td valign="top" class="value"><g:link controller="entity" action="show" id="${addressInstance?.entity?.id}">${addressInstance?.entity?.encodeAsHTML()}</g:link></td>
                        </tr>
                        <tr class="prop">
                            <td valign="top" class="name">Address Type:</td>
                            <td valign="top" class="value"><g:link controller="addressType" action="show" id="${addressInstance?.addressType?.id}">${addressInstance?.addressType?.encodeAsHTML()}</g:link></td>
                        </tr>
                        <tr class="prop">
                            <td valign="top" class="name">Street:</td>
                            <td valign="top" class="value">${fieldValue(bean:addressInstance, field:'street')}</td>
                        </tr>
                        <tr class="prop">
                            <td valign="top" class="name">Suburb:</td>
                            <td valign="top" class="value">${fieldValue(bean:addressInstance, field:'suburb')}</td>
                        </tr>
                        <tr class="prop">
                            <td valign="top" class="name">State:</td>
                            <td valign="top" class="value">${fieldValue(bean:addressInstance, field:'state')}</td>
                        </tr>
                        <tr class="prop">
                            <td valign="top" class="name">Postcode:</td>
                            <td valign="top" class="value">${fieldValue(bean:addressInstance, field:'postcode')}</td>
                        </tr>
                        <tr class="prop">
                            <td valign="top" class="name">Lat/Long:</td>
							<g:if test="${addressInstance.lat && addressInstance.lng}">
                            <td valign="top" class="value">(${fieldValue(bean:addressInstance, field:'lat')}, ${fieldValue(bean:addressInstance, field:'lng')})</td>
                        	</g:if>
                        	<g:else>
                        	<td valign="top" class="value"><i>(unknown)</i></td>
                        	</g:else>
                        </tr>
                        </g:ifAnyGranted>
                        <g:ifNotGranted role="ROLE_ADMIN,ROLE_SUPER">
                        <tr class="prop">
                            <td valign="top" class="name">Address For:</td>
                            <td valign="top" class="value">${addressInstance?.entity?.encodeAsHTML()}</td>
                        </tr>
                        <tr class="prop">
                            <td valign="top" class="name">Address Type:</td>
                            <td valign="top" class="value">${addressInstance?.addressType?.encodeAsHTML()}</td>
                        </tr>
                        <tr class="prop">
                            <td valign="top" class="name">Location:</td>
                            <td valign="top" class="value">${addressInstance?.encodeAsHTML()}</td>
                        </tr>
                        </g:ifNotGranted>
                    </tbody>
                </table>
            </div>
            <div class="buttons">
                <g:form>
                    <input type="hidden" name="id" value="${addressInstance?.id}" />
                    <span class="button"><g:actionSubmit class="edit" value="Edit" /></span>
                    <span class="button"><g:actionSubmit class="delete" onclick="return confirm('Are you sure?');" value="Delete" /></span>
                </g:form>
            </div>
        </div>
    </body>
</html>
