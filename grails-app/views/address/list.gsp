
<%@ page import="com.innovated.iris.domain.Address" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta name="layout" content="main" />
        <title>Address List</title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${resource(dir:'')}">Home</a></span>
            <span class="menuButton"><g:link class="create" action="create">New Address</g:link></span>
        </div>
        <div class="body">
            <h1>Address List</h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <div class="list">
                <table>
                    <thead>
                        <tr>
                        
                   	        <g:sortableColumn property="id" title="Id" />
                        
                   	        <th>Address Type</th>
                   	    
                   	        <g:sortableColumn property="street" title="Street" />
                        
                   	        <g:sortableColumn property="suburb" title="Suburb" />
                        
                   	        <g:sortableColumn property="state" title="State" />
                        
                   	        <g:sortableColumn property="postcode" title="Postcode" />
                        
                        </tr>
                    </thead>
                    <tbody>
                    <g:each in="${addressInstanceList}" status="i" var="addressInstance">
                        <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
                        
                            <td><g:link action="show" id="${addressInstance.id}">${fieldValue(bean:addressInstance, field:'id')}</g:link></td>
                        
                            <td>${fieldValue(bean:addressInstance, field:'addressType')}</td>
                        
                            <td>${fieldValue(bean:addressInstance, field:'street')}</td>
                        
                            <td>${fieldValue(bean:addressInstance, field:'suburb')}</td>
                        
                            <td>${fieldValue(bean:addressInstance, field:'state')}</td>
                        
                            <td>${fieldValue(bean:addressInstance, field:'postcode')}</td>
                        
                        </tr>
                    </g:each>
                    </tbody>
                </table>
            </div>
            <div class="paginateButtons">
                <g:paginate total="${addressInstanceTotal}" />
            </div>
        </div>
    </body>
</html>

