<%@
page import="com.innovated.iris.domain.*"
page import="com.innovated.iris.domain.enums.*"
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <title><g:message code="address.create" default="Create Address" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLinkTo(dir: '')}"><g:message code="home" default="Home" /></a></span>
            <span class="menuButton"><g:link class="list" action="list"><g:message code="address.list" default="Address List" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="address.create" default="Create Address" /></h1>
            <g:if test="${flash.message}">
            	<div class="message"><g:message code="${flash.message}" args="${flash.args}" default="${flash.defaultMessage}" /></div>
            </g:if>
            <g:hasErrors bean="${addressInstance}">
				<div class="errors">
					<g:renderErrors bean="${addressInstance}" as="list" />
				</div>
            </g:hasErrors>
            <g:form action="save" method="post" >
                <div class="dialog">
                    <fieldset>
                        <legend><g:message code="address.create.legend" default="Enter Address Details"/></legend>
                        
                            <div class="prop mandatory ${hasErrors(bean: addressInstance, field: 'entity', 'error')}">
                                <label for="entity">
                                    <g:message code="address.entity" default="Entity" />
                                    <span class="indicator">*</span>
                                </label>
                                <g:select optionKey="id" from="${Entity.list()}" name="entity.id" value="${addressInstance?.entity?.id}" ></g:select>
                            </div>
                            <div class="prop mandatory ${hasErrors(bean: addressInstance, field: 'addressType', 'error')}">
                                <label for="addressType">
                                    <g:message code="address.addressType" default="Address Type" />
                                    <span class="indicator">*</span>
                                </label>
                                <g:select optionKey="id" from="${AddressType.list()}" name="addressType.id" value="${addressInstance?.addressType?.id}" ></g:select>
                            </div>
                            <div class="prop mandatory ${hasErrors(bean: addressInstance, field: 'street', 'error')}">
                                <label for="street">
                                    <g:message code="address.street" default="Street" />
                                    <span class="indicator">*</span>
                                </label>
                                <input type="text" id="street" name="street" value="${fieldValue(bean:addressInstance,field:'street')}"/>
                            </div>
                            <div class="prop mandatory ${hasErrors(bean: addressInstance, field: 'suburb', 'error')}">
                                <label for="suburb">
                                    <g:message code="address.suburb" default="Suburb" />
                                    <span class="indicator">*</span>
                                </label>
                                <input type="text" id="suburb" name="suburb" value="${fieldValue(bean:addressInstance,field:'suburb')}"/>
                            </div>
                            <div class="prop mandatory ${hasErrors(bean: addressInstance, field: 'state', 'error')}">
                                <label for="state">
                                    <g:message code="address.state" default="State" />
                                    <span class="indicator">*</span>
                                </label>
                                <g:select name="state.id" from="${AustralianState.list()}" optionKey="id" value="${addressInstance?.state?.id}"  />
                            </div>
                            <div class="prop mandatory ${hasErrors(bean: addressInstance, field: 'postcode', 'error')}">
                                <label for="postcode">
                                    <g:message code="address.postcode" default="Postcode" />
                                    <span class="indicator">*</span>
                                </label>
                                <input type="text" id="postcode" name="postcode" value="${fieldValue(bean:addressInstance,field:'postcode')}"/>
                            </div>
                    </fieldset>
                </div>
                <div class="buttons">
                    <span class="button"><g:submitButton name="create" class="save" value="${message(code: 'create', 'default': 'Create')}" /></span>
                </div>
            </g:form>
        </div>
    </body>
</html>

