
<%@ page import="com.innovated.iris.domain.lookup.QuoteExportTemplateParameter" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <title><g:message code="quoteExportTemplateParameter.edit" default="Edit QuoteExportTemplateParameter" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLinkTo(dir: '')}"><g:message code="home" default="Home" /></a></span>
            <span class="menuButton"><g:link class="list" action="list"><g:message code="quoteExportTemplateParameter.list" default="QuoteExportTemplateParameter List" /></g:link></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="quoteExportTemplateParameter.new" default="New QuoteExportTemplateParameter" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="quoteExportTemplateParameter.edit" default="Edit QuoteExportTemplateParameter" /></h1>
            <g:if test="${flash.message}">
            <div class="message"><g:message code="${flash.message}" args="${flash.args}" default="${flash.defaultMessage}" /></div>
            </g:if>
            <g:hasErrors bean="${quoteExportTemplateParameterInstance}">
            <div class="errors">
                <g:renderErrors bean="${quoteExportTemplateParameterInstance}" as="list" />
            </div>
            </g:hasErrors>
            <g:form method="post" >
                <g:hiddenField name="id" value="${quoteExportTemplateParameterInstance?.id}" />
                <g:hiddenField name="version" value="${quoteExportTemplateParameterInstance?.version}" />
                <div class="dialog">
                    <table>
                        <tbody>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="dateFrom"><g:message code="quoteExportTemplateParameter.dateFrom" default="Date From" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: quoteExportTemplateParameterInstance, field: 'dateFrom', 'errors')}">
                                    <g:datePicker name="dateFrom" value="${quoteExportTemplateParameterInstance?.dateFrom}"  />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="customerCode"><g:message code="quoteExportTemplateParameter.customerCode" default="Customer Code" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: quoteExportTemplateParameterInstance, field: 'customerCode', 'errors')}">
                                    <g:textField name="customerCode" value="${fieldValue(bean: quoteExportTemplateParameterInstance, field: 'customerCode')}" />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="templateName"><g:message code="quoteExportTemplateParameter.templateName" default="Template Name" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: quoteExportTemplateParameterInstance, field: 'templateName', 'errors')}">
                                    <g:textField name="templateName" value="${fieldValue(bean: quoteExportTemplateParameterInstance, field: 'templateName')}" />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="dateCreated"><g:message code="quoteExportTemplateParameter.dateCreated" default="Date Created" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: quoteExportTemplateParameterInstance, field: 'dateCreated', 'errors')}">
                                    <g:datePicker name="dateCreated" value="${quoteExportTemplateParameterInstance?.dateCreated}"  />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="lastUpdated"><g:message code="quoteExportTemplateParameter.lastUpdated" default="Last Updated" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: quoteExportTemplateParameterInstance, field: 'lastUpdated', 'errors')}">
                                    <g:datePicker name="lastUpdated" value="${quoteExportTemplateParameterInstance?.lastUpdated}"  />

                                </td>
                            </tr>
                        
                        </tbody>
                    </table>
                </div>
                <div class="buttons">
                    <span class="button"><g:actionSubmit class="save" action="update" value="${message(code: 'update', 'default': 'Update')}" /></span>
                    <span class="button"><g:actionSubmit class="delete" action="delete" value="${message(code: 'delete', 'default': 'Delete')}" onclick="return confirm('${message(code: 'delete.confirm', 'default': 'Are you sure?')}');" /></span>
                </div>
            </g:form>
        </div>
    </body>
</html>
