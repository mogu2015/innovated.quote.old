
<script type="text/javascript">
$(document).ready(function() {
	var table = $('#quoteExportTemplateParameterDataTable').dataTable({
		"iDisplayLength": 100, "sPaginationType": "full_numbers",
		"iCookieDuration": 3600, "bStateSave": true, "bProcessing": true, "bServerSide": true,
		"sAjaxSource": "${createLink(controller:'quoteExportTemplateParameter', action:'listAjax')}",
		"aLengthMenu": [[25, 50, 100, 250, 500, 1000], [25, 50, "100 (Default)", 250, 500, 1000]],
		"aaSorting": [[ 3, "asc" ]],	// column sorting defaults (zero-indexed)
		"aoColumns": [
			{ "sClass":"cellLeft", "bSortable":false },	// checkbox column
			{ "sClass":"cellCtr", "bSortable":false }	// icon/spacer column
			,{ "sClass":"cellCtr" }
			,{ "sClass":"cellCtr" }
			,{ "sClass":"cellCtr" }
			,{ "sClass":"cellCtr" }
			,{ "sClass":"cellRight" }
		],
		"fnDrawCallback":function(){
			$('td').bind('mouseenter',function(){ $(this).parent().children().each(function(){$(this).addClass('tabelRowHighlight');}); });
			$('td').bind('mouseleave',function(){ $(this).parent().children().each(function(){$(this).removeClass('tabelRowHighlight');}); });
		}
	});
	table.fnSetFilteringDelay(300);	// 300 msec after "keyup" the ajax lookup will fire
});
</script>

<table id="quoteExportTemplateParameterDataTable" class="list">
<thead>
	<tr>
		<th class="hdrLeft" width="20"><g:checkBox id="chkAll" name="chkAll" value="${false}" title="Select all rows" /></th>
		<th class="hdrCtr" width="16">&nbsp;</th>
		<th class="hdrCtr"><g:message code="quoteExportTemplateParameter.id" default="Id" /></th>
		<th class="hdrCtr"><g:message code="quoteExportTemplateParameter.dateFrom" default="Date From" /></th>
		<th class="hdrCtr"><g:message code="quoteExportTemplateParameter.customerCode" default="Customer Code" /></th>
		<th class="hdrCtr"><g:message code="quoteExportTemplateParameter.templateName" default="Template Name" /></th>
		<th class="hdrRight"><g:message code="quoteExportTemplateParameter.dateCreated" default="Date Created" /></th>
	</tr>
</thead>
<tbody>
	<tr>
		<td colspan="7" class="dataTables_empty">Loading data from server</td>
	</tr>
</tbody>
</table>
