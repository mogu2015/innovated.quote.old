
<%@ page import="com.innovated.iris.domain.enums.AddressType" %>
<html>
    <head>
        <g:ifAllGranted role="ROLE_SUPER"><meta name="layout" content="super" /></g:ifAllGranted>
        <g:ifNotGranted role="ROLE_SUPER"><meta name="layout" content="main" /></g:ifNotGranted>
        <title>Edit AddressType</title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${resource(dir:'')}">Home</a></span>
            <span class="menuButton"><g:link class="list" action="list">AddressType List</g:link></span>
            <span class="menuButton"><g:link class="create" action="create">New AddressType</g:link></span>
        </div>
        <div class="body">
            <h1>Edit AddressType</h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <g:hasErrors bean="${addressTypeInstance}">
            <div class="errors">
                <g:renderErrors bean="${addressTypeInstance}" as="list" />
            </div>
            </g:hasErrors>
            <g:form method="post" >
                <input type="hidden" name="id" value="${addressTypeInstance?.id}" />
                <input type="hidden" name="version" value="${addressTypeInstance?.version}" />
                <div class="dialog">
                    <table>
                        <tbody>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="name">Name:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean:addressTypeInstance,field:'name','errors')}">
                                    <input type="text" id="name" name="name" value="${fieldValue(bean:addressTypeInstance,field:'name')}"/>
                                </td>
                            </tr> 
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="visible">Visible:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean:addressTypeInstance,field:'visible','errors')}">
                                    <g:checkBox name="visible" value="${addressTypeInstance?.visible}" ></g:checkBox>
                                </td>
                            </tr> 
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="enabled">Enabled:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean:addressTypeInstance,field:'enabled','errors')}">
                                    <g:checkBox name="enabled" value="${addressTypeInstance?.enabled}" ></g:checkBox>
                                </td>
                            </tr> 
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="dateCreated">Date Created:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean:addressTypeInstance,field:'dateCreated','errors')}">
                                    <g:datePicker name="dateCreated" value="${addressTypeInstance?.dateCreated}" precision="minute" ></g:datePicker>
                                </td>
                            </tr> 
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="lastUpdated">Last Updated:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean:addressTypeInstance,field:'lastUpdated','errors')}">
                                    <g:datePicker name="lastUpdated" value="${addressTypeInstance?.lastUpdated}" precision="minute" ></g:datePicker>
                                </td>
                            </tr> 
                        
                        </tbody>
                    </table>
                </div>
                <div class="buttons">
                    <span class="button"><g:actionSubmit class="save" value="Update" /></span>
                    <span class="button"><g:actionSubmit class="delete" onclick="return confirm('Are you sure?');" value="Delete" /></span>
                </div>
            </g:form>
        </div>
    </body>
</html>
