
<%@ page import="com.innovated.iris.domain.lookup.TyreParameter" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <title><g:message code="tyreParameter.create" default="Create TyreParameter" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLinkTo(dir: '')}"><g:message code="home" default="Home" /></a></span>
            <span class="menuButton"><g:link class="list" action="list"><g:message code="tyreParameter.list" default="TyreParameter List" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="tyreParameter.create" default="Create TyreParameter" /></h1>
            <g:if test="${flash.message}">
            <div class="message"><g:message code="${flash.message}" args="${flash.args}" default="${flash.defaultMessage}" /></div>
            </g:if>
            <g:hasErrors bean="${tyreParameterInstance}">
            <div class="errors">
                <g:renderErrors bean="${tyreParameterInstance}" as="list" />
            </div>
            </g:hasErrors>
            <g:form action="save" method="post" >
                <div class="dialog">
                    <table>
                        <tbody>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="dateFrom"><g:message code="tyreParameter.dateFrom" default="Date From" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: tyreParameterInstance, field: 'dateFrom', 'errors')}">
                                    <g:datePicker name="dateFrom" value="${tyreParameterInstance?.dateFrom}" precision="day" />

                                </td>
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="make"><g:message code="tyreParameter.make" default="Make" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: tyreParameterInstance, field: 'make', 'errors')}">
                                    <g:select name="make.id" from="${com.innovated.iris.domain.enums.VehicleManufacturer.list()}" optionKey="id" value="${tyreParameterInstance?.make?.id}"  />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="model"><g:message code="tyreParameter.model" default="Model" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: tyreParameterInstance, field: 'model', 'errors')}">
                                    <g:select name="model.id" from="${com.innovated.iris.domain.enums.VehicleModel.list()}" optionKey="id" value="${tyreParameterInstance?.model?.id}" noSelection="['null': '']" />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="variant"><g:message code="tyreParameter.variant" default="Variant" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: tyreParameterInstance, field: 'variant', 'errors')}">
                                    <g:select name="variant.id" from="${com.innovated.iris.domain.MakeModel.list()}" optionKey="id" value="${tyreParameterInstance?.variant?.id}" noSelection="['null': '']" />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="size"><g:message code="tyreParameter.size" default="Size" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: tyreParameterInstance, field: 'size', 'errors')}">
                                    <g:textField name="size" value="${fieldValue(bean: tyreParameterInstance, field: 'size')}" />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="standardPrice"><g:message code="tyreParameter.standardPrice" default="Standard Price" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: tyreParameterInstance, field: 'standardPrice', 'errors')}">
                                    <g:textField name="standardPrice" value="${fieldValue(bean: tyreParameterInstance, field: 'standardPrice')}" />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="premiumPrice"><g:message code="tyreParameter.premiumPrice" default="Premium Price" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: tyreParameterInstance, field: 'premiumPrice', 'errors')}">
                                    <g:textField name="premiumPrice" value="${fieldValue(bean: tyreParameterInstance, field: 'premiumPrice')}" />

                                </td>
                            </tr>
                        
                        </tbody>
                    </table>
                </div>
                <div class="buttons">
                    <span class="button"><g:submitButton name="create" class="save" value="${message(code: 'create', 'default': 'Create')}" /></span>
                </div>
            </g:form>
        </div>
    </body>
</html>
