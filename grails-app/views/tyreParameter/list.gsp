
<%@ page import="com.innovated.iris.domain.lookup.TyreParameter" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <title><g:message code="tyreParameter.list" default="TyreParameter List" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLinkTo(dir: '')}"><g:message code="home" default="Home" /></a></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="tyreParameter.new" default="New TyreParameter" /></g:link></span>
            <span class="menuButton"><g:link class="save" action="importFile"><g:message code="tyreParameter.import" default="Import Tyre Costs" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="tyreParameter.list" default="TyreParameter List" /></h1>
            <g:if test="${flash.message}">
            <div class="message"><g:message code="${flash.message}" args="${flash.args}" default="${flash.defaultMessage}" /></div>
            </g:if>
            <div class="list">
                <table>
                    <thead>
                        <tr>
		               	    <g:sortableColumn property="id" title="Id" titleKey="tyreParameter.id" />
		               	    <g:sortableColumn property="dateFrom" title="Date From" titleKey="tyreParameter.dateFrom" />
		               	    <g:sortableColumn property="make" title="Make" titleKey="tyreParameter.make" />
		               	    <g:sortableColumn property="model" title="Model" titleKey="tyreParameter.model" />
		               	    <g:sortableColumn property="variant" title="Variant" titleKey="tyreParameter.variant" />
		               	    <g:sortableColumn class="numeric" property="size" title="Size" titleKey="tyreParameter.size" />
		               	    <g:sortableColumn class="numeric" property="standardPrice" title="Standard Price" titleKey="tyreParameter.standardPrice" />
		               	    <g:sortableColumn class="numeric" property="premiumPrice" title="Premium Price" titleKey="tyreParameter.premiumPrice" />
		               	    <th class="numeric">Diameter</th>
		               	    <th class="numeric">Width</th>
		               	    <th class="numeric">Height</th>
		               	</tr>
                    </thead>
                    <tbody>
                    <g:each in="${tyreParameterInstanceList}" status="i" var="tyreParameterInstance">
                        <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
                            <td><g:link action="show" id="${tyreParameterInstance.id}">${fieldValue(bean: tyreParameterInstance, field: "id")}</g:link></td>
                            <td><g:formatDate date="${tyreParameterInstance.dateFrom}" format="d MMM yyyy" /></td>
                            <td>${fieldValue(bean: tyreParameterInstance, field: "make")}</td>
                            <td>${fieldValue(bean: tyreParameterInstance, field: "model")}</td>
                            <td>${fieldValue(bean: tyreParameterInstance, field: "variant")}</td>
                            <td class="numeric">${fieldValue(bean: tyreParameterInstance, field: "size")}</td>
                            <td class="numeric">${formatNumber(number:tyreParameterInstance.standardPrice, type:'currency', minFractionDigits:2, maxFractionDigits:2)}</td>
                            <td class="numeric">${formatNumber(number:tyreParameterInstance.premiumPrice, type:'currency', minFractionDigits:2, maxFractionDigits:2)}</td>
                            <g:if test="${tyreParameterInstance?.size}">
                            <td class="numeric">${fieldValue(bean: tyreParameterInstance, field: "diameter")} inch</td>
                            <td class="numeric">${fieldValue(bean: tyreParameterInstance, field: "width")} mm</td>
                            <td class="numeric">${fieldValue(bean: tyreParameterInstance, field: "height")} mm</td>
                            </g:if>
                            <g:else>
                            <td class="numeric">-</td>
                            <td class="numeric">-</td>
                            <td class="numeric">-</td>
                            </g:else>
                        </tr>
                    </g:each>
                    </tbody>
                </table>
            </div>
            <div class="paginateButtons">
                <g:paginate total="${tyreParameterInstanceTotal}" />
            </div>
        </div>
    </body>
</html>
