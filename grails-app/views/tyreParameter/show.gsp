
<%@ page import="com.innovated.iris.domain.lookup.TyreParameter" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <title><g:message code="tyreParameter.show" default="Show TyreParameter" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLinkTo(dir: '')}"><g:message code="home" default="Home" /></a></span>
            <span class="menuButton"><g:link class="list" action="list"><g:message code="tyreParameter.list" default="TyreParameter List" /></g:link></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="tyreParameter.new" default="New TyreParameter" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="tyreParameter.show" default="Show TyreParameter" /></h1>
            <g:if test="${flash.message}">
            <div class="message"><g:message code="${flash.message}" args="${flash.args}" default="${flash.defaultMessage}" /></div>
            </g:if>
            <g:form>
                <g:hiddenField name="id" value="${tyreParameterInstance?.id}" />
                <div class="dialog">
                    <table>
                        <tbody>
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="tyreParameter.id" default="Id" />:</td>
                                <td valign="top" class="value">${fieldValue(bean: tyreParameterInstance, field: "id")}</td>
                            </tr>
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="tyreParameter.dateFrom" default="Date From" />:</td>
                                <td valign="top" class="value"><g:formatDate date="${tyreParameterInstance?.dateFrom}" format="d MMM yyyy"/></td>
                            </tr>
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="tyreParameter.make" default="Make" />:</td>
                                <td valign="top" class="value">${fieldValue(bean: tyreParameterInstance, field: "make")}</td>
                            </tr>
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="tyreParameter.model" default="Model" />:</td>
                                <td valign="top" class="value">${fieldValue(bean: tyreParameterInstance, field: "model")}</td>
                            </tr>
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="tyreParameter.variant" default="Variant" />:</td>
                                <td valign="top" class="value">${fieldValue(bean: tyreParameterInstance, field: "variant")}</td>
                            </tr>
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="tyreParameter.size" default="Size" />:</td>
                                <td valign="top" class="value">${fieldValue(bean: tyreParameterInstance, field: "size")}</td>
                            </tr>
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="tyreParameter.standardPrice" default="Standard Price" />:</td>
                                <td valign="top" class="value">${formatNumber(number:tyreParameterInstance.standardPrice, type:'currency', minFractionDigits:2, maxFractionDigits:2)}</td>
                            </tr>
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="tyreParameter.premiumPrice" default="Premium Price" />:</td>
                                <td valign="top" class="value">${formatNumber(number:tyreParameterInstance.premiumPrice, type:'currency', minFractionDigits:2, maxFractionDigits:2)}</td>
                            </tr>
                            <g:if test="${tyreParameterInstance?.size}">
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="tyreParameter.diameter" default="Diameter" />:</td>
                                <td valign="top" class="value">${fieldValue(bean: tyreParameterInstance, field: "diameter")} inch</td>
                            </tr>
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="tyreParameter.width" default="Width" />:</td>
                                <td valign="top" class="value">${fieldValue(bean: tyreParameterInstance, field: "width")} mm</td>
                            </tr>
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="tyreParameter.height" default="Height" />:</td>
                                <td valign="top" class="value">${fieldValue(bean: tyreParameterInstance, field: "height")} mm</td>
                            </tr>
                            </g:if>
                        </tbody>
                    </table>
                </div>
                <div class="buttons">
                    <span class="button"><g:actionSubmit class="edit" action="edit" value="${message(code: 'edit', 'default': 'Edit')}" /></span>
                    <span class="button"><g:actionSubmit class="delete" action="delete" value="${message(code: 'delete', 'default': 'Delete')}" onclick="return confirm('${message(code: 'delete.confirm', 'default': 'Are you sure?')}');" /></span>
                </div>
            </g:form>
        </div>
    </body>
</html>
