
<%@ page import="com.innovated.iris.domain.Asset" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <title><g:message code="asset.edit" default="Edit Asset" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLinkTo(dir: '')}"><g:message code="home" default="Home" /></a></span>
            <span class="menuButton"><g:link class="list" action="list"><g:message code="asset.list" default="Asset List" /></g:link></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="asset.new" default="New Asset" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="asset.edit" default="Edit Asset" /></h1>
            <g:if test="${flash.message}">
            	<div class="message"><g:message code="${flash.message}" args="${flash.args}" default="${flash.defaultMessage}" /></div>
            </g:if>
            <g:hasErrors bean="${assetInstance}">
				<div class="errors">
					<g:renderErrors bean="${assetInstance}" as="list" />
				</div>
            </g:hasErrors>
            <g:form method="post" >
                <g:hiddenField name="id" value="${assetInstance?.id}" />
                <g:hiddenField name="version" value="${assetInstance?.version}" />
                <div class="dialog">
                    <fieldset>
                        <legend><g:message code="asset.edit.legend" default="Update Asset Details"/></legend>
                        
                        <div class="prop ${hasErrors(bean: assetInstance, field: 'assetCode', 'error')}">
                            <label for="assetCode">
                                <g:message code="asset.assetCode" default="Asset Code" />
                                
                            </label>
                            <g:textField name="assetCode" value="${fieldValue(bean: assetInstance, field: 'assetCode')}" />

                        </div>
                        
                        <div class="prop mandatory ${hasErrors(bean: assetInstance, field: 'owner', 'error')}">
                            <label for="owner">
                                <g:message code="asset.owner" default="Owner" />
                                <span class="indicator">*</span>
                            </label>
                            <g:select name="owner.id" from="${com.innovated.iris.domain.Customer.list()}" optionKey="id" value="${assetInstance?.owner?.id}"  />

                        </div>
                        
                        <div class="prop mandatory ${hasErrors(bean: assetInstance, field: 'acquisitionDate', 'error')}">
                            <label for="acquisitionDate">
                                <g:message code="asset.acquisitionDate" default="Acquisition Date" />
                                <span class="indicator">*</span>
                            </label>
                            <g:datePicker name="acquisitionDate" value="${assetInstance?.acquisitionDate}"  />

                        </div>
                        
                        <div class="prop mandatory ${hasErrors(bean: assetInstance, field: 'acquisitionPrice', 'error')}">
                            <label for="acquisitionPrice">
                                <g:message code="asset.acquisitionPrice" default="Acquisition Price" />
                                <span class="indicator">*</span>
                            </label>
                            <g:textField name="acquisitionPrice" value="${fieldValue(bean: assetInstance, field: 'acquisitionPrice')}" />

                        </div>
                        
                        <div class="prop ${hasErrors(bean: assetInstance, field: 'disposalDate', 'error')}">
                            <label for="disposalDate">
                                <g:message code="asset.disposalDate" default="Disposal Date" />
                                
                            </label>
                            <g:datePicker name="disposalDate" value="${assetInstance?.disposalDate}" noSelection="['': '']" />

                        </div>
                        
                        <div class="prop ${hasErrors(bean: assetInstance, field: 'disposalPrice', 'error')}">
                            <label for="disposalPrice">
                                <g:message code="asset.disposalPrice" default="Disposal Price" />
                                
                            </label>
                            <g:textField name="disposalPrice" value="${fieldValue(bean: assetInstance, field: 'disposalPrice')}" />

                        </div>
                        
                        <div class="prop ${hasErrors(bean: assetInstance, field: 'information', 'error')}">
                            <label for="information">
                                <g:message code="asset.information" default="Information" />
                                
                            </label>
                            <g:textField name="information" value="${fieldValue(bean: assetInstance, field: 'information')}" />

                        </div>
                        
                        <div class="prop mandatory ${hasErrors(bean: assetInstance, field: 'budgets', 'error')}">
                            <label for="budgets">
                                <g:message code="asset.budgets" default="Budgets" />
                                <span class="indicator">*</span>
                            </label>
                            <g:select name="budgets"
from="${com.innovated.iris.domain.Budget.list()}"
size="5" multiple="yes" optionKey="id"
value="${assetInstance?.budgets}" />


                        </div>
                        
                        <div class="prop mandatory ${hasErrors(bean: assetInstance, field: 'dateCreated', 'error')}">
                            <label for="dateCreated">
                                <g:message code="asset.dateCreated" default="Date Created" />
                                <span class="indicator">*</span>
                            </label>
                            <g:datePicker name="dateCreated" value="${assetInstance?.dateCreated}"  />

                        </div>
                        
                        <div class="prop mandatory ${hasErrors(bean: assetInstance, field: 'lastUpdated', 'error')}">
                            <label for="lastUpdated">
                                <g:message code="asset.lastUpdated" default="Last Updated" />
                                <span class="indicator">*</span>
                            </label>
                            <g:datePicker name="lastUpdated" value="${assetInstance?.lastUpdated}"  />

                        </div>
                        
                    </fieldset>
                </div>
                <div class="buttons">
                    <span class="button"><g:actionSubmit class="save" action="update" value="${message(code: 'update', 'default': 'Update')}" /></span>
                    <span class="button"><g:actionSubmit class="delete" action="delete" value="${message(code: 'delete', 'default': 'Delete')}" onclick="return confirm('${message(code: 'delete.confirm', 'default': 'Are you sure?')}');" /></span>
                </div>
            </g:form>
        </div>
    </body>
</html>

