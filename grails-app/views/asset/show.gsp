
<%@ page import="com.innovated.iris.domain.Asset" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <title><g:message code="asset.show" default="Show Asset" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLinkTo(dir: '')}"><g:message code="home" default="Home" /></a></span>
            <span class="menuButton"><g:link class="list" action="list"><g:message code="asset.list" default="Asset List" /></g:link></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="asset.new" default="New Asset" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="asset.show" default="Show Asset" /></h1>
            <g:if test="${flash.message}">
            <div class="message"><g:message code="${flash.message}" args="${flash.args}" default="${flash.defaultMessage}" /></div>
            </g:if>
            <g:form>
                <g:hiddenField name="id" value="${assetInstance?.id}" />
                <div class="dialog">
                    <table>
                        <tbody>
                        
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="asset.id" default="Id" />:</td>
                                
                                <td valign="top" class="value">${fieldValue(bean: assetInstance, field: "id")}</td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="asset.assetCode" default="Asset Code" />:</td>
                                
                                <td valign="top" class="value">${fieldValue(bean: assetInstance, field: "assetCode")}</td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="asset.owner" default="Owner" />:</td>
                                
                                <td valign="top" class="value"><g:link controller="customer" action="show" id="${assetInstance?.owner?.id}">${assetInstance?.owner?.encodeAsHTML()}</g:link></td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="asset.acquisitionDate" default="Acquisition Date" />:</td>
                                
                                <td valign="top" class="value"><g:formatDate date="${assetInstance?.acquisitionDate}" /></td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="asset.acquisitionPrice" default="Acquisition Price" />:</td>
                                
                                <td valign="top" class="value"><g:formatNumber number="${assetInstance?.acquisitionPrice}" /></td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="asset.disposalDate" default="Disposal Date" />:</td>
                                
                                <td valign="top" class="value"><g:formatDate date="${assetInstance?.disposalDate}" /></td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="asset.disposalPrice" default="Disposal Price" />:</td>
                                
                                <td valign="top" class="value"><g:formatNumber number="${assetInstance?.disposalPrice}" /></td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="asset.information" default="Information" />:</td>
                                
                                <td valign="top" class="value">${fieldValue(bean: assetInstance, field: "information")}</td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="asset.budgets" default="Budgets" />:</td>
                                
                                <td  valign="top" style="text-align: left;" class="value">
                                    <ul>
                                    <g:each in="${assetInstance?.budgets}" var="budgetInstance">
                                        <li><g:link controller="budget" action="show" id="${budgetInstance.id}">${budgetInstance.encodeAsHTML()}</g:link></li>
                                    </g:each>
                                    </ul>
                                </td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="asset.dateCreated" default="Date Created" />:</td>
                                
                                <td valign="top" class="value"><g:formatDate date="${assetInstance?.dateCreated}" /></td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="asset.lastUpdated" default="Last Updated" />:</td>
                                
                                <td valign="top" class="value"><g:formatDate date="${assetInstance?.lastUpdated}" /></td>
                                
                            </tr>
                            
                        </tbody>
                    </table>
                </div>
                <div class="buttons">
                    <span class="button"><g:actionSubmit class="edit" action="edit" value="${message(code: 'edit', 'default': 'Edit')}" /></span>
                    <span class="button"><g:actionSubmit class="delete" action="delete" value="${message(code: 'delete', 'default': 'Delete')}" onclick="return confirm('${message(code: 'delete.confirm', 'default': 'Are you sure?')}');" /></span>
                </div>
            </g:form>
        </div>
    </body>
</html>
