
<%@ page import="com.innovated.iris.domain.Asset" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <title><g:message code="asset.list" default="Asset List" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLinkTo(dir: '')}"><g:message code="home" default="Home" /></a></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="asset.new" default="New Asset" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="asset.list" default="Asset List" /></h1>
            <g:if test="${flash.message}">
            <div class="message"><g:message code="${flash.message}" args="${flash.args}" default="${flash.defaultMessage}" /></div>
            </g:if>
            <div class="list">
                <table>
                    <thead>
                        <tr>
                        
                   	    <g:sortableColumn property="id" title="Id" titleKey="asset.id" />
                        
                   	    <g:sortableColumn property="assetCode" title="Asset Code" titleKey="asset.assetCode" />
                        
                   	    <th><g:message code="asset.owner" default="Owner" /></th>
                   	    
                   	    <g:sortableColumn property="acquisitionDate" title="Acquisition Date" titleKey="asset.acquisitionDate" />
                        
                   	    <g:sortableColumn property="acquisitionPrice" title="Acquisition Price" titleKey="asset.acquisitionPrice" />
                        
                   	    <g:sortableColumn property="disposalDate" title="Disposal Date" titleKey="asset.disposalDate" />
                        
                        </tr>
                    </thead>
                    <tbody>
                    <g:each in="${assetInstanceList}" status="i" var="assetInstance">
                        <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
                        
                            <td><g:link action="show" id="${assetInstance.id}">${fieldValue(bean: assetInstance, field: "id")}</g:link></td>
                        
                            <td>${fieldValue(bean: assetInstance, field: "assetCode")}</td>
                        
                            <td>${fieldValue(bean: assetInstance, field: "owner")}</td>
                        
                            <td><g:formatDate date="${assetInstance.acquisitionDate}" /></td>
                        
                            <td><g:formatNumber number="${assetInstance.acquisitionPrice}" /></td>
                        
                            <td><g:formatDate date="${assetInstance.disposalDate}" /></td>
                        
                        </tr>
                    </g:each>
                    </tbody>
                </table>
            </div>
            <div class="paginateButtons">
                <g:paginate total="${assetInstanceTotal}" />
            </div>
        </div>
    </body>
</html>
