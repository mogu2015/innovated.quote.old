
<%@ page import="com.innovated.iris.domain.Asset" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <title><g:message code="asset.create" default="Create Asset" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLinkTo(dir: '')}"><g:message code="home" default="Home" /></a></span>
            <span class="menuButton"><g:link class="list" action="list"><g:message code="asset.list" default="Asset List" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="asset.create" default="Create Asset" /></h1>
            <g:if test="${flash.message}">
            	<div class="message"><g:message code="${flash.message}" args="${flash.args}" default="${flash.defaultMessage}" /></div>
            </g:if>
            <g:hasErrors bean="${assetInstance}">
		        <div class="errors">
		            <g:renderErrors bean="${assetInstance}" as="list" />
		        </div>
            </g:hasErrors>
            <g:form action="save" method="post" >
                <div class="dialog">
                    <table>
                        <tbody>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="assetCode"><g:message code="asset.assetCode" default="Asset Code" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: assetInstance, field: 'assetCode', 'errors')}">
                                    <g:textField name="assetCode" value="${fieldValue(bean: assetInstance, field: 'assetCode')}" />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="owner"><g:message code="asset.owner" default="Owner" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: assetInstance, field: 'owner', 'errors')}">
                                    <g:select name="owner.id" from="${com.innovated.iris.domain.Customer.list()}" optionKey="id" value="${assetInstance?.owner?.id}"  />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="acquisitionDate"><g:message code="asset.acquisitionDate" default="Acquisition Date" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: assetInstance, field: 'acquisitionDate', 'errors')}">
                                    <g:datePicker name="acquisitionDate" value="${assetInstance?.acquisitionDate}"  />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="acquisitionPrice"><g:message code="asset.acquisitionPrice" default="Acquisition Price" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: assetInstance, field: 'acquisitionPrice', 'errors')}">
                                    <g:textField name="acquisitionPrice" value="${fieldValue(bean: assetInstance, field: 'acquisitionPrice')}" />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="disposalDate"><g:message code="asset.disposalDate" default="Disposal Date" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: assetInstance, field: 'disposalDate', 'errors')}">
                                    <g:datePicker name="disposalDate" value="${assetInstance?.disposalDate}" noSelection="['': '']" />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="disposalPrice"><g:message code="asset.disposalPrice" default="Disposal Price" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: assetInstance, field: 'disposalPrice', 'errors')}">
                                    <g:textField name="disposalPrice" value="${fieldValue(bean: assetInstance, field: 'disposalPrice')}" />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="information"><g:message code="asset.information" default="Information" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: assetInstance, field: 'information', 'errors')}">
                                    <g:textField name="information" value="${fieldValue(bean: assetInstance, field: 'information')}" />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="dateCreated"><g:message code="asset.dateCreated" default="Date Created" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: assetInstance, field: 'dateCreated', 'errors')}">
                                    <g:datePicker name="dateCreated" value="${assetInstance?.dateCreated}"  />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="lastUpdated"><g:message code="asset.lastUpdated" default="Last Updated" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: assetInstance, field: 'lastUpdated', 'errors')}">
                                    <g:datePicker name="lastUpdated" value="${assetInstance?.lastUpdated}"  />

                                </td>
                            </tr>
                        
                        </tbody>
                    </table>
                </div>
                <div class="buttons">
                    <span class="button"><g:submitButton name="create" class="save" value="${message(code: 'create', 'default': 'Create')}" /></span>
                </div>
            </g:form>
        </div>
    </body>
</html>
