
<%@ page import="com.innovated.iris.domain.enums.NonVehicleType" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <title><g:message code="nonVehicleType.edit" default="Edit NonVehicleType" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLinkTo(dir: '')}"><g:message code="home" default="Home" /></a></span>
            <span class="menuButton"><g:link class="list" action="list"><g:message code="nonVehicleType.list" default="NonVehicleType List" /></g:link></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="nonVehicleType.new" default="New NonVehicleType" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="nonVehicleType.edit" default="Edit NonVehicleType" /></h1>
            <g:if test="${flash.message}">
            	<div class="message"><g:message code="${flash.message}" args="${flash.args}" default="${flash.defaultMessage}" /></div>
            </g:if>
            <g:hasErrors bean="${nonVehicleTypeInstance}">
				<div class="errors">
					<g:renderErrors bean="${nonVehicleTypeInstance}" as="list" />
				</div>
            </g:hasErrors>
            <g:form method="post" >
                <g:hiddenField name="id" value="${nonVehicleTypeInstance?.id}" />
                <g:hiddenField name="version" value="${nonVehicleTypeInstance?.version}" />
                <div class="dialog">
                    <fieldset>
                        <legend><g:message code="nonVehicleType.edit.legend" default="Update NonVehicleType Details"/></legend>
                        
                        <div class="prop mandatory ${hasErrors(bean: nonVehicleTypeInstance, field: 'name', 'error')}">
                            <label for="name">
                                <g:message code="nonVehicleType.name" default="Name" />
                                <span class="indicator">*</span>
                            </label>
                            <g:textField name="name" value="${fieldValue(bean: nonVehicleTypeInstance, field: 'name')}" />

                        </div>
                        
                        <div class="prop mandatory ${hasErrors(bean: nonVehicleTypeInstance, field: 'visible', 'error')}">
                            <label for="visible">
                                <g:message code="nonVehicleType.visible" default="Visible" />
                                <span class="indicator">*</span>
                            </label>
                            <g:checkBox name="visible" value="${nonVehicleTypeInstance?.visible}" />

                        </div>
                        
                        <div class="prop mandatory ${hasErrors(bean: nonVehicleTypeInstance, field: 'enabled', 'error')}">
                            <label for="enabled">
                                <g:message code="nonVehicleType.enabled" default="Enabled" />
                                <span class="indicator">*</span>
                            </label>
                            <g:checkBox name="enabled" value="${nonVehicleTypeInstance?.enabled}" />

                        </div>
                        
                        <div class="prop mandatory ${hasErrors(bean: nonVehicleTypeInstance, field: 'dateCreated', 'error')}">
                            <label for="dateCreated">
                                <g:message code="nonVehicleType.dateCreated" default="Date Created" />
                                <span class="indicator">*</span>
                            </label>
                            <g:datePicker name="dateCreated" value="${nonVehicleTypeInstance?.dateCreated}"  />

                        </div>
                        
                        <div class="prop mandatory ${hasErrors(bean: nonVehicleTypeInstance, field: 'lastUpdated', 'error')}">
                            <label for="lastUpdated">
                                <g:message code="nonVehicleType.lastUpdated" default="Last Updated" />
                                <span class="indicator">*</span>
                            </label>
                            <g:datePicker name="lastUpdated" value="${nonVehicleTypeInstance?.lastUpdated}"  />

                        </div>
                        
                    </fieldset>
                </div>
                <div class="buttons">
                    <span class="button"><g:actionSubmit class="save" action="update" value="${message(code: 'update', 'default': 'Update')}" /></span>
                    <span class="button"><g:actionSubmit class="delete" action="delete" value="${message(code: 'delete', 'default': 'Delete')}" onclick="return confirm('${message(code: 'delete.confirm', 'default': 'Are you sure?')}');" /></span>
                </div>
            </g:form>
        </div>
    </body>
</html>

