
<%@ page import="com.innovated.iris.domain.enums.NonVehicleType" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <title><g:message code="nonVehicleType.list" default="NonVehicleType List" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLinkTo(dir: '')}"><g:message code="home" default="Home" /></a></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="nonVehicleType.new" default="New NonVehicleType" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="nonVehicleType.list" default="NonVehicleType List" /></h1>
            <g:if test="${flash.message}">
            <div class="message"><g:message code="${flash.message}" args="${flash.args}" default="${flash.defaultMessage}" /></div>
            </g:if>
            <div class="list">
                <table>
                    <thead>
                        <tr>
                        
                   	    <g:sortableColumn property="id" title="Id" titleKey="nonVehicleType.id" />
                        
                   	    <g:sortableColumn property="name" title="Name" titleKey="nonVehicleType.name" />
                        
                   	    <g:sortableColumn property="visible" title="Visible" titleKey="nonVehicleType.visible" />
                        
                   	    <g:sortableColumn property="enabled" title="Enabled" titleKey="nonVehicleType.enabled" />
                        
                   	    <g:sortableColumn property="dateCreated" title="Date Created" titleKey="nonVehicleType.dateCreated" />
                        
                   	    <g:sortableColumn property="lastUpdated" title="Last Updated" titleKey="nonVehicleType.lastUpdated" />
                        
                        </tr>
                    </thead>
                    <tbody>
                    <g:each in="${nonVehicleTypeInstanceList}" status="i" var="nonVehicleTypeInstance">
                        <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
                        
                            <td><g:link action="show" id="${nonVehicleTypeInstance.id}">${fieldValue(bean: nonVehicleTypeInstance, field: "id")}</g:link></td>
                        
                            <td>${fieldValue(bean: nonVehicleTypeInstance, field: "name")}</td>
                        
                            <td><g:formatBoolean boolean="${nonVehicleTypeInstance.visible}" /></td>
                        
                            <td><g:formatBoolean boolean="${nonVehicleTypeInstance.enabled}" /></td>
                        
                            <td><g:formatDate date="${nonVehicleTypeInstance.dateCreated}" /></td>
                        
                            <td><g:formatDate date="${nonVehicleTypeInstance.lastUpdated}" /></td>
                        
                        </tr>
                    </g:each>
                    </tbody>
                </table>
            </div>
            <div class="paginateButtons">
                <g:paginate total="${nonVehicleTypeInstanceTotal}" />
            </div>
        </div>
    </body>
</html>
