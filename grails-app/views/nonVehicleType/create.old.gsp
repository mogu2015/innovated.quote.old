
<%@ page import="com.innovated.iris.domain.enums.NonVehicleType" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <title><g:message code="nonVehicleType.create" default="Create NonVehicleType" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLinkTo(dir: '')}"><g:message code="home" default="Home" /></a></span>
            <span class="menuButton"><g:link class="list" action="list"><g:message code="nonVehicleType.list" default="NonVehicleType List" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="nonVehicleType.create" default="Create NonVehicleType" /></h1>
            <g:if test="${flash.message}">
            	<div class="message"><g:message code="${flash.message}" args="${flash.args}" default="${flash.defaultMessage}" /></div>
            </g:if>
            <g:hasErrors bean="${nonVehicleTypeInstance}">
		        <div class="errors">
		            <g:renderErrors bean="${nonVehicleTypeInstance}" as="list" />
		        </div>
            </g:hasErrors>
            <g:form action="save" method="post" >
                <div class="dialog">
                    <table>
                        <tbody>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="name"><g:message code="nonVehicleType.name" default="Name" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: nonVehicleTypeInstance, field: 'name', 'errors')}">
                                    <g:textField name="name" value="${fieldValue(bean: nonVehicleTypeInstance, field: 'name')}" />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="visible"><g:message code="nonVehicleType.visible" default="Visible" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: nonVehicleTypeInstance, field: 'visible', 'errors')}">
                                    <g:checkBox name="visible" value="${nonVehicleTypeInstance?.visible}" />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="enabled"><g:message code="nonVehicleType.enabled" default="Enabled" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: nonVehicleTypeInstance, field: 'enabled', 'errors')}">
                                    <g:checkBox name="enabled" value="${nonVehicleTypeInstance?.enabled}" />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="dateCreated"><g:message code="nonVehicleType.dateCreated" default="Date Created" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: nonVehicleTypeInstance, field: 'dateCreated', 'errors')}">
                                    <g:datePicker name="dateCreated" value="${nonVehicleTypeInstance?.dateCreated}"  />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="lastUpdated"><g:message code="nonVehicleType.lastUpdated" default="Last Updated" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: nonVehicleTypeInstance, field: 'lastUpdated', 'errors')}">
                                    <g:datePicker name="lastUpdated" value="${nonVehicleTypeInstance?.lastUpdated}"  />

                                </td>
                            </tr>
                        
                        </tbody>
                    </table>
                </div>
                <div class="buttons">
                    <span class="button"><g:submitButton name="create" class="save" value="${message(code: 'create', 'default': 'Create')}" /></span>
                </div>
            </g:form>
        </div>
    </body>
</html>
