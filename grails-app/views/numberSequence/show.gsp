

<%@ page import="com.innovated.iris.domain.system.NumberSequence" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="prod">
        <title><g:message code="numberSequence.show" default="Show NumberSequence" /></title>
    </head>
	<body>
		<div class="twocol">
			<div id="content">
				<div id="leftcol">
					<div id="contentMain">
						<p class="bread">
							<a class="home" href="${createLinkTo(dir: '')}"><g:message code="home" default="Home" /></a>
							<span class="breadArrow">&nbsp;</span>
							<g:message code="numberSequence.show" default="Show NumberSequence" />
						</p>
						
						<g:if test="${flash.message}">
						<div class="flashMsg">
							<p><g:message code="${flash.message}" args="${flash.args}" default="${flash.defaultMessage}" /></p>
						</div>
						</g:if>
						
						<h1 class="contentH1"><g:message code="numberSequence.show" default="Show NumberSequence" /></h1>
						<div class="formBg">
							<div class="formWrap">
								<g:form>
								<g:hiddenField name="id" value="${numberSequenceInstance?.id}" />
								
								<h3 class="topPad"><g:message code="numberSequence.show" default="Show NumberSequence" /></h3>
								
								<div class="formSet">
									<table class="grid">
									<tbody>
										<tr>
											<th>ID #</th>
											<td class="special">${numberSequenceInstance?.id}</td>
											<td class="spacer">&nbsp;</td>
											<th>Version</th>
											<td class="special">${numberSequenceInstance?.version}</td>
										</tr>
										
									
										<tr>
											<th><g:message code="numberSequence.code" default="Code" /></th>
											<td colspan="4">${fieldValue(bean: numberSequenceInstance, field: "code")}</td>
										</tr>
										
										<tr>
											<th><g:message code="numberSequence.name" default="Name" /></th>
											<td colspan="4">${fieldValue(bean: numberSequenceInstance, field: "name")}</td>
										</tr>
										
										<tr>
											<th><g:message code="numberSequence.format" default="Format" /></th>
											<td colspan="4">${fieldValue(bean: numberSequenceInstance, field: "format")}</td>
										</tr>
										<tr>
											<th class="last">Date Created</th>
											<td class="last"><g:formatDate date="${numberSequenceInstance?.dateCreated}" format="d/MM/yyyy H:mm:ss" /></td>
											<td class="last spacer">&nbsp;</td>
											<th class="last">Last Updated</th>
											<td class="last"><g:formatDate date="${numberSequenceInstance?.lastUpdated}" format="d/MM/yyyy H:mm:ss" /></td>
										</tr>
									</tbody>
								</table>
								</div>
								
								<button id="btnEdit" name="_action_edit" class="greybutton" type="submit">
						       		<span><img width="16" height="16" alt="" src="${resource(dir:'images/skin',file:'database_edit.png')}"/><g:message code="edit" default="Edit" /></span>
						    	</button>
						    	<button id="btnDelete" name="_action_delete" class="greybutton" type="submit" onclick="return confirm('${message(code: 'delete.confirm', 'default': 'Are you sure?')}');">
						       		<span><img width="16" height="16" alt="" src="${resource(dir:'images/skin',file:'database_delete.png')}"/><g:message code="delete" default="Delete" /></span>
						    	</button>
						    	<span class="formcancel"> or&nbsp;&nbsp;<g:link action="list"><g:message code="cancel" default="cancel" /></g:link></span>
						    	
								</g:form>
							</div>
						</div>
						
					</div> <!-- #contentMain -->
					
				</div>
				<div id="rightcol">
					<div id="options">
						<div class="optGrpHdr"><h3 class="optHdr"><g:message code="default.rcol.current.opts" default="Current Options" /></h3></div>
						<dl class="optGrpMenu">
							<iris:sidebarItem controller="numberSequence" action="list" icon="page_white_stack.png" titleCode="numberSequence.rcol.title.list" msgCode="numberSequence.rcol.msg.list" />
						</dl>
					</div>
				</div>
				<div class="clear"></div>
			</div>
		</div>
	</body>
</html>



