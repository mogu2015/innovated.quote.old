

<%@ page import="com.innovated.iris.domain.system.NumberSequence" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="prod">
        <title><g:message code="numberSequence.list" default="NumberSequence List" /></title>
        <g:javascript library="jquery/jquery.dataTables.min" />
        <g:javascript library="jquery/dataTables.fnSetFilteringDelay" />
    </head>
	<body>
		<div class="twocol">
			<div id="content">
				<div id="leftcol">
					<p class="bread">
						<a class="home" href="${createLinkTo(dir: '')}"><g:message code="home" default="Home" /></a>
						<span class="breadArrow">&nbsp;</span>
						<g:message code="numberSequence.list" default="NumberSequence List" />
					</p>
					<g:if test="${flash.message}"><div class="flashMsg">
						<p><g:message code="${flash.message}" args="${flash.args}" default="${flash.defaultMessage}" /></p>
					</div></g:if>
					
					<h1 class="contentH1"><g:message code="numberSequence.list" default="NumberSequence List" /></h1>
					<g:if test="${numberSequenceInstanceList?.size() > 0}">
					<div class="alt_pagination">
						<g:render template="listdatatable" />
					</div>
					</g:if>
					<g:else>
					<div class="emptyListPanel">
						<h1><g:link class="create" action="create"><g:message code="numberSequence.new" default="New NumberSequence" /></g:link></h1>
						<p><g:message code="numberSequence.emptylist" default="There are no NumberSequence objects to display here..." /></p>
					</div>
					</g:else>
					
				</div>
				<div id="rightcol">
					<div id="options">
						<div class="optGrpHdr"><h3 class="optHdr"><g:message code="default.rcol.current.opts" default="Current Options" /></h3></div>
						<dl class="optGrpMenu">
							<iris:sidebarItem controller="numberSequence" action="list" icon="page_white_stack.png" titleCode="numberSequence.rcol.title.list" msgCode="numberSequence.rcol.msg.list" />
						</dl>
					</div>
				</div>
				<div class="clear"></div>
			</div>
		</div>
	</body>
</html>


