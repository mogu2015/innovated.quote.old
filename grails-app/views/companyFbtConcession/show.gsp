
<%@ page import="com.innovated.iris.domain.CompanyFbtConcession" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <title><g:message code="companyFbtConcession.show" default="Show CompanyFbtConcession" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLinkTo(dir: '')}"><g:message code="home" default="Home" /></a></span>
            <span class="menuButton"><g:link class="list" action="list"><g:message code="companyFbtConcession.list" default="CompanyFbtConcession List" /></g:link></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="companyFbtConcession.new" default="New CompanyFbtConcession" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="companyFbtConcession.show" default="Show CompanyFbtConcession" /></h1>
            <g:if test="${flash.message}">
            <div class="message"><g:message code="${flash.message}" args="${flash.args}" default="${flash.defaultMessage}" /></div>
            </g:if>
            <g:form>
                <g:hiddenField name="id" value="${companyFbtConcessionInstance?.id}" />
                <div class="dialog">
                    <table>
                        <tbody>
                        
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="companyFbtConcession.id" default="Id" />:</td>
                                
                                <td valign="top" class="value">${fieldValue(bean: companyFbtConcessionInstance, field: "id")}</td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="companyFbtConcession.dateFrom" default="Date From" />:</td>
                                
                                <td valign="top" class="value"><g:formatDate date="${companyFbtConcessionInstance?.dateFrom}" /></td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="companyFbtConcession.type" default="Type" />:</td>
                                
                                <td valign="top" class="value">${companyFbtConcessionInstance?.type?.encodeAsHTML()}</td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="companyFbtConcession.company" default="Company" />:</td>
                                
                                <td valign="top" class="value"><g:link controller="company" action="show" id="${companyFbtConcessionInstance?.company?.id}">${companyFbtConcessionInstance?.company?.encodeAsHTML()}</g:link></td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="companyFbtConcession.lastUpdated" default="Last Updated" />:</td>
                                
                                <td valign="top" class="value"><g:formatDate date="${companyFbtConcessionInstance?.lastUpdated}" /></td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="companyFbtConcession.dateCreated" default="Date Created" />:</td>
                                
                                <td valign="top" class="value"><g:formatDate date="${companyFbtConcessionInstance?.dateCreated}" /></td>
                                
                            </tr>
                            
                        </tbody>
                    </table>
                </div>
                <div class="buttons">
                    <span class="button"><g:actionSubmit class="edit" action="edit" value="${message(code: 'edit', 'default': 'Edit')}" /></span>
                    <span class="button"><g:actionSubmit class="delete" action="delete" value="${message(code: 'delete', 'default': 'Delete')}" onclick="return confirm('${message(code: 'delete.confirm', 'default': 'Are you sure?')}');" /></span>
                </div>
            </g:form>
        </div>
    </body>
</html>
