
<%@ page import="com.innovated.iris.domain.CompanyFbtConcession" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <title><g:message code="companyFbtConcession.list" default="CompanyFbtConcession List" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLinkTo(dir: '')}"><g:message code="home" default="Home" /></a></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="companyFbtConcession.new" default="New CompanyFbtConcession" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="companyFbtConcession.list" default="CompanyFbtConcession List" /></h1>
            <g:if test="${flash.message}">
            <div class="message"><g:message code="${flash.message}" args="${flash.args}" default="${flash.defaultMessage}" /></div>
            </g:if>
            <div class="list">
                <table>
                    <thead>
                        <tr>
                        
                   	    <g:sortableColumn property="id" title="Id" titleKey="companyFbtConcession.id" />
                        
                   	    <g:sortableColumn property="dateFrom" title="Date From" titleKey="companyFbtConcession.dateFrom" />
                        
                   	    <g:sortableColumn property="type" title="Type" titleKey="companyFbtConcession.type" />
                        
                   	    <th><g:message code="companyFbtConcession.company" default="Company" /></th>
                   	    
                   	    <g:sortableColumn property="lastUpdated" title="Last Updated" titleKey="companyFbtConcession.lastUpdated" />
                        
                   	    <g:sortableColumn property="dateCreated" title="Date Created" titleKey="companyFbtConcession.dateCreated" />
                        
                        </tr>
                    </thead>
                    <tbody>
                    <g:each in="${companyFbtConcessionInstanceList}" status="i" var="companyFbtConcessionInstance">
                        <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
                        
                            <td><g:link action="show" id="${companyFbtConcessionInstance.id}">${fieldValue(bean: companyFbtConcessionInstance, field: "id")}</g:link></td>
                        
                            <td><g:formatDate date="${companyFbtConcessionInstance.dateFrom}" /></td>
                        
                            <td>${fieldValue(bean: companyFbtConcessionInstance, field: "type")}</td>
                        
                            <td>${fieldValue(bean: companyFbtConcessionInstance, field: "company")}</td>
                        
                            <td><g:formatDate date="${companyFbtConcessionInstance.lastUpdated}" /></td>
                        
                            <td><g:formatDate date="${companyFbtConcessionInstance.dateCreated}" /></td>
                        
                        </tr>
                    </g:each>
                    </tbody>
                </table>
            </div>
            <div class="paginateButtons">
                <g:paginate total="${companyFbtConcessionInstanceTotal}" />
            </div>
        </div>
    </body>
</html>
