
<%@ page import="com.innovated.iris.domain.CompanyFbtConcession" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <title><g:message code="companyFbtConcession.create" default="Create CompanyFbtConcession" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLinkTo(dir: '')}"><g:message code="home" default="Home" /></a></span>
            <span class="menuButton"><g:link class="list" action="list"><g:message code="companyFbtConcession.list" default="CompanyFbtConcession List" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="companyFbtConcession.create" default="Create CompanyFbtConcession" /></h1>
            <g:if test="${flash.message}">
            <div class="message"><g:message code="${flash.message}" args="${flash.args}" default="${flash.defaultMessage}" /></div>
            </g:if>
            <g:hasErrors bean="${companyFbtConcessionInstance}">
            <div class="errors">
                <g:renderErrors bean="${companyFbtConcessionInstance}" as="list" />
            </div>
            </g:hasErrors>
            <g:form action="save" method="post" >
                <div class="dialog">
                    <table>
                        <tbody>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="dateFrom"><g:message code="companyFbtConcession.dateFrom" default="Date From" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: companyFbtConcessionInstance, field: 'dateFrom', 'errors')}">
                                    <g:datePicker name="dateFrom" value="${companyFbtConcessionInstance?.dateFrom}"  />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="type"><g:message code="companyFbtConcession.type" default="Type" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: companyFbtConcessionInstance, field: 'type', 'errors')}">
                                    <g:select name="type" from="${com.innovated.iris.server.fbt.FbtConcessionType?.values()}" value="${companyFbtConcessionInstance?.type}"  />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="company"><g:message code="companyFbtConcession.company" default="Company" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: companyFbtConcessionInstance, field: 'company', 'errors')}">
                                    <g:select name="company.id" from="${com.innovated.iris.domain.Company.list()}" optionKey="id" value="${companyFbtConcessionInstance?.company?.id}"  />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="lastUpdated"><g:message code="companyFbtConcession.lastUpdated" default="Last Updated" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: companyFbtConcessionInstance, field: 'lastUpdated', 'errors')}">
                                    <g:datePicker name="lastUpdated" value="${companyFbtConcessionInstance?.lastUpdated}"  />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="dateCreated"><g:message code="companyFbtConcession.dateCreated" default="Date Created" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: companyFbtConcessionInstance, field: 'dateCreated', 'errors')}">
                                    <g:datePicker name="dateCreated" value="${companyFbtConcessionInstance?.dateCreated}"  />

                                </td>
                            </tr>
                        
                        </tbody>
                    </table>
                </div>
                <div class="buttons">
                    <span class="button"><g:submitButton name="create" class="save" value="${message(code: 'create', 'default': 'Create')}" /></span>
                </div>
            </g:form>
        </div>
    </body>
</html>
