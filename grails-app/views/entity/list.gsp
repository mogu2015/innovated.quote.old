
<%@ page import="com.innovated.iris.domain.Entity" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta name="layout" content="prod" />
        <title>Entity List</title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${resource(dir:'')}">Home</a></span>
            <span class="menuButton"><g:link class="create" action="create">New Entity</g:link></span>
        </div>
        <div class="body">
            <h1>Entity List</h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <div class="list">
                <table>
                    <thead>
                        <tr>
                        
                   	        <g:sortableColumn property="id" title="Id" />
                        
                   	        <g:sortableColumn property="name" title="Name" />
                        
                   	        <th>Addresses</th>
                   	    
                   	        <th>Phones</th>
                   	    
                   	        <th>Emails</th>
                   	    
                   	        <g:sortableColumn property="dateCreated" title="Date Created" />
                        
                        </tr>
                    </thead>
                    <tbody>
                    <g:each in="${entityInstanceList}" status="i" var="entityInstance">
                        <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
                        
                            <td><g:link action="show" id="${entityInstance.id}">${fieldValue(bean:entityInstance, field:'id')}</g:link></td>
                        
                            <td>${fieldValue(bean:entityInstance, field:'name')}</td>
                        
                            <td>${fieldValue(bean:entityInstance, field:'addresses')}</td>
                        
                            <td>${fieldValue(bean:entityInstance, field:'phones')}</td>
                        
                            <td>${fieldValue(bean:entityInstance, field:'emails')}</td>
                        
                            <td>${fieldValue(bean:entityInstance, field:'dateCreated')}</td>
                        
                        </tr>
                    </g:each>
                    </tbody>
                </table>
            </div>
            <div class="paginateButtons">
                <g:paginate total="${entityInstanceTotal}" />
            </div>
        </div>
    </body>
</html>
