
<%@ page import="com.innovated.iris.domain.Entity" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="prod">
        <title><g:message code="entity.create" default="Create Entity" /></title>
    </head>
	<body>
		<div class="twocol">
			<div id="content">
				<div id="leftcol">
					<div id="contentMain">
						<p class="bread">
							<a class="home" href="${createLinkTo(dir: '')}"><g:message code="home" default="Home" /></a>
							<span class="breadArrow">&nbsp;</span>
							<g:link action="list"><g:message code="entity.list" default="Entity List" /></g:link>
							<span class="breadArrow">&nbsp;</span>
							<g:message code="entity.create" default="Create Entity" />
						</p>
						
						<g:if test="${flash.message}">
						<div class="flashMsg">
							<p><g:message code="${flash.message}" args="${flash.args}" default="${flash.defaultMessage}" /></p>
						</div>
						</g:if>
			            <g:hasErrors bean="${entityInstance}">
						<div class="errors">
							<ul>
								<g:eachError bean="${entityInstance}"><li><g:message error="${it}"/></li>
								</g:eachError>
							</ul>
						</div>
		            	</g:hasErrors>
						
						<h1 class="contentH1"><g:message code="entity.create" default="Create Entity" /></h1>
						<div class="formBg">
							<div class="formWrap">
								<g:form action="save" method="post" >
								
								<h3 class="topPad"><g:message code="entity.create" default="Create Entity" /></h3>
								<p>Please enter the details for this new Entity:</p>
								<div class="formSet">
						
									<div class="clearfix ${hasErrors(bean: entityInstance, field: 'name', 'errors')}">
										<label for="name"><g:message code="entity.name" default="Name" /></label>
		                                <g:textField name="name" value="${fieldValue(bean: entityInstance, field: 'name')}" />

		                            </div>
						
									<div class="clearfix ${hasErrors(bean: entityInstance, field: 'type', 'errors')}">
										<label for="type"><g:message code="entity.type" default="Type" /></label>
		                                <g:select class="short" name="type" from="${['Individual', 'Company']}" value="${fieldValue(bean: entityInstance, field: 'type')}"/>
		                            </div>
						
								</div>
								
								<button id="btnCreate" name="btnCreate" class="greybutton" type="submit">
						       		<span><img width="16" height="16" alt="" src="${resource(dir:'images/skin',file:'disk.png')}"/><g:message code="create" default="Create" /></span>
						    	</button>
						    	<span class="formcancel"> or&nbsp;&nbsp;<g:link action="list"><g:message code="cancel" default="cancel" /></g:link></span>
						    	
								</g:form>
							</div>
						</div>
						
					</div> <!-- #contentMain -->
					
				</div>
				<div id="rightcol">
					<div id="options">
						<div class="optGrpHdr"><h3 class="optHdr"><g:message code="default.rcol.current.opts" default="Current Options" /></h3></div>
						<dl class="optGrpMenu">
							<iris:sidebarItem controller="entity" action="list" icon="page_white_stack.png" titleCode="entity.rcol.title.list" msgCode="entity.rcol.msg.list" />
						</dl>
					</div>
				</div>
				<div class="clear"></div>
			</div>
		</div>
	</body>
</html>


