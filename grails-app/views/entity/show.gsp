

<%@ page import="com.innovated.iris.domain.Entity" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="prod">
        <title><g:message code="entity.show" default="Show Entity" /></title>
    </head>
	<body>
		<div class="twocol">
			<div id="content">
				<div id="leftcol">
					<div id="contentMain">
						<p class="bread">
							<a class="home" href="${createLinkTo(dir: '')}"><g:message code="home" default="Home" /></a>
							<span class="breadArrow">&nbsp;</span>
							<g:link action="list"><g:message code="entity.list" default="Entity List" /></g:link>
							<span class="breadArrow">&nbsp;</span>
							<g:message code="entity.show" default="Show Entity" />
						</p>
						
						<g:if test="${flash.message}">
						<div class="flashMsg">
							<p><g:message code="${flash.message}" args="${flash.args}" default="${flash.defaultMessage}" /></p>
						</div>
						</g:if>
						
						<h1 class="contentH1"><g:message code="entity.show" default="Show Entity" /></h1>
						<div class="formBg">
							<div class="formWrap">
								<g:form>
								<g:hiddenField name="id" value="${entityInstance?.id}" />
								
								<h3 class="topPad"><g:message code="entity.show" default="Show Entity" /></h3>
								
								<div class="formSet">
									<table class="grid">
									<tbody>
										<tr>
											<th>ID #</th>
											<td class="special">${entityInstance?.id}</td>
											<td class="spacer">&nbsp;</td>
											<th>Version</th>
											<td class="special">${entityInstance?.version}</td>
										</tr>
										
									
										<tr>
											<th><g:message code="entity.name" default="Name" /></th>
											<td colspan="4">${fieldValue(bean: entityInstance, field: "name")}</td>
										</tr>
										
										<tr>
											<th><g:message code="entity.type" default="Type" /></th>
											<td colspan="4">${fieldValue(bean: entityInstance, field: "type")}</td>
										</tr>
										
										<tr>
											<th><g:message code="entity.addresses" default="Addresses" /></th>
											<td colspan="4">
												<g:if test="${entityInstance?.addresses?.size() > 0}"><ol>
												<g:each in="${entityInstance?.addresses}" var="addressInstance">
													<li><g:link controller="address" action="show" id="${addressInstance.id}">${addressInstance.encodeAsHTML()}</g:link></li>
												</g:each></ol></g:if>
												<g:else>There are <b>currently no items</b> recorded for this address.</g:else>
											</td>
											</tr>
										
										<tr>
											<th><g:message code="entity.phones" default="Phones" /></th>
											<td colspan="4">
												<g:if test="${entityInstance?.phones?.size() > 0}"><ol>
												<g:each in="${entityInstance?.phones}" var="phoneInstance">
													<li><g:link controller="phone" action="show" id="${phoneInstance.id}">${phoneInstance.encodeAsHTML()}</g:link></li>
												</g:each></ol></g:if>
												<g:else>There are <b>currently no items</b> recorded for this phone.</g:else>
											</td>
											</tr>
										
										<tr>
											<th><g:message code="entity.emails" default="Emails" /></th>
											<td colspan="4">
												<g:if test="${entityInstance?.emails?.size() > 0}"><ol>
												<g:each in="${entityInstance?.emails}" var="emailInstance">
													<li><g:link controller="email" action="show" id="${emailInstance.id}">${emailInstance.encodeAsHTML()}</g:link></li>
												</g:each></ol></g:if>
												<g:else>There are <b>currently no items</b> recorded for this email.</g:else>
											</td>
											</tr>
										<tr>
											<th class="last">Date Created</th>
											<td class="last"><g:formatDate date="${entityInstance?.dateCreated}" format="d/MM/yyyy H:mm:ss" /></td>
											<td class="last spacer">&nbsp;</td>
											<th class="last">Last Updated</th>
											<td class="last"><g:formatDate date="${entityInstance?.lastUpdated}" format="d/MM/yyyy H:mm:ss" /></td>
										</tr>
									</tbody>
								</table>
								</div>
								
								<button id="btnEdit" name="_action_edit" class="greybutton" type="submit">
						       		<span><img width="16" height="16" alt="" src="${resource(dir:'images/skin',file:'database_edit.png')}"/><g:message code="edit" default="Edit" /></span>
						    	</button>
						    	<button id="btnDelete" name="_action_delete" class="greybutton" type="submit" onclick="return confirm('${message(code: 'delete.confirm', 'default': 'Are you sure?')}');">
						       		<span><img width="16" height="16" alt="" src="${resource(dir:'images/skin',file:'database_delete.png')}"/><g:message code="delete" default="Delete" /></span>
						    	</button>
						    	<span class="formcancel"> or&nbsp;&nbsp;<g:link action="list"><g:message code="cancel" default="cancel" /></g:link></span>
						    	
								</g:form>
							</div>
						</div>
						
					</div> <!-- #contentMain -->
					
				</div>
				<div id="rightcol">
					<div id="options">
						<div class="optGrpHdr"><h3 class="optHdr"><g:message code="default.rcol.current.opts" default="Current Options" /></h3></div>
						<dl class="optGrpMenu">
							<iris:sidebarItem controller="entity" action="list" icon="page_white_stack.png" titleCode="entity.rcol.title.list" msgCode="entity.rcol.msg.list" />
							<iris:sidebarItem controller="entity" action="create" icon="page_white_add.png" titleCode="entity.rcol.title.create" msgCode="entity.rcol.msg.create" />
							<iris:sidebarItem controller="entity" action="edit" id="${entityInstance?.id}" icon="page_white_edit.png" titleCode="entity.rcol.title.edit" msgCode="entity.rcol.msg.edit" />
						</dl>
					</div>
				</div>
				<div class="clear"></div>
			</div>
		</div>
	</body>
</html>



