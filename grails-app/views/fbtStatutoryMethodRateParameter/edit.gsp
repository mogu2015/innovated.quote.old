
<%@ page import="com.innovated.iris.domain.lookup.FbtStatutoryMethodRateParameter" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <title><g:message code="fbtStatutoryMethodRateParameter.edit" default="Edit FbtStatutoryMethodRateParameter" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLinkTo(dir: '')}"><g:message code="home" default="Home" /></a></span>
            <span class="menuButton"><g:link class="list" action="list"><g:message code="fbtStatutoryMethodRateParameter.list" default="FbtStatutoryMethodRateParameter List" /></g:link></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="fbtStatutoryMethodRateParameter.new" default="New FbtStatutoryMethodRateParameter" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="fbtStatutoryMethodRateParameter.edit" default="Edit FbtStatutoryMethodRateParameter" /></h1>
            <g:if test="${flash.message}">
            <div class="message"><g:message code="${flash.message}" args="${flash.args}" default="${flash.defaultMessage}" /></div>
            </g:if>
            <g:hasErrors bean="${fbtStatutoryMethodRateParameterInstance}">
            <div class="errors">
                <g:renderErrors bean="${fbtStatutoryMethodRateParameterInstance}" as="list" />
            </div>
            </g:hasErrors>
            <g:form method="post" >
                <g:hiddenField name="id" value="${fbtStatutoryMethodRateParameterInstance?.id}" />
                <g:hiddenField name="version" value="${fbtStatutoryMethodRateParameterInstance?.version}" />
                <div class="dialog">
                    <table>
                        <tbody>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="dateFrom"><g:message code="fbtStatutoryMethodRateParameter.dateFrom" default="Date From" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: fbtStatutoryMethodRateParameterInstance, field: 'dateFrom', 'errors')}">
                                    <g:datePicker name="dateFrom" value="${fbtStatutoryMethodRateParameterInstance?.dateFrom}"  />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="customerCode"><g:message code="fbtStatutoryMethodRateParameter.customerCode" default="Customer Code" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: fbtStatutoryMethodRateParameterInstance, field: 'customerCode', 'errors')}">
                                    <g:textField name="customerCode" value="${fieldValue(bean: fbtStatutoryMethodRateParameterInstance, field: 'customerCode')}" />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="taxableValueTypeCode"><g:message code="fbtStatutoryMethodRateParameter.taxableValueTypeCode" default="Taxable Value Type Code" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: fbtStatutoryMethodRateParameterInstance, field: 'taxableValueTypeCode', 'errors')}">
                                    <g:textField name="taxableValueTypeCode" value="${fieldValue(bean: fbtStatutoryMethodRateParameterInstance, field: 'taxableValueTypeCode')}" />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="lowerValue"><g:message code="fbtStatutoryMethodRateParameter.lowerValue" default="Lower Value" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: fbtStatutoryMethodRateParameterInstance, field: 'lowerValue', 'errors')}">
                                    <g:select name="lowerValue" from="${0..999999}" value="${fbtStatutoryMethodRateParameterInstance?.lowerValue}"  />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="upperValue"><g:message code="fbtStatutoryMethodRateParameter.upperValue" default="Upper Value" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: fbtStatutoryMethodRateParameterInstance, field: 'upperValue', 'errors')}">
                                    <g:select name="upperValue" from="${0..999999}" value="${fbtStatutoryMethodRateParameterInstance?.upperValue}"  />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="rate"><g:message code="fbtStatutoryMethodRateParameter.rate" default="Rate" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: fbtStatutoryMethodRateParameterInstance, field: 'rate', 'errors')}">
                                    <g:select name="rate" from="${0.01..1.00}" value="${fbtStatutoryMethodRateParameterInstance?.rate}"  />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="lastUpdated"><g:message code="fbtStatutoryMethodRateParameter.lastUpdated" default="Last Updated" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: fbtStatutoryMethodRateParameterInstance, field: 'lastUpdated', 'errors')}">
                                    <g:datePicker name="lastUpdated" value="${fbtStatutoryMethodRateParameterInstance?.lastUpdated}"  />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="dateCreated"><g:message code="fbtStatutoryMethodRateParameter.dateCreated" default="Date Created" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: fbtStatutoryMethodRateParameterInstance, field: 'dateCreated', 'errors')}">
                                    <g:datePicker name="dateCreated" value="${fbtStatutoryMethodRateParameterInstance?.dateCreated}"  />

                                </td>
                            </tr>
                        
                        </tbody>
                    </table>
                </div>
                <div class="buttons">
                    <span class="button"><g:actionSubmit class="save" action="update" value="${message(code: 'update', 'default': 'Update')}" /></span>
                    <span class="button"><g:actionSubmit class="delete" action="delete" value="${message(code: 'delete', 'default': 'Delete')}" onclick="return confirm('${message(code: 'delete.confirm', 'default': 'Are you sure?')}');" /></span>
                </div>
            </g:form>
        </div>
    </body>
</html>
