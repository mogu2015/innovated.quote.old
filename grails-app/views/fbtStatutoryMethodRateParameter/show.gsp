
<%@ page import="com.innovated.iris.domain.lookup.FbtStatutoryMethodRateParameter" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <title><g:message code="fbtStatutoryMethodRateParameter.show" default="Show FbtStatutoryMethodRateParameter" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLinkTo(dir: '')}"><g:message code="home" default="Home" /></a></span>
            <span class="menuButton"><g:link class="list" action="list"><g:message code="fbtStatutoryMethodRateParameter.list" default="FbtStatutoryMethodRateParameter List" /></g:link></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="fbtStatutoryMethodRateParameter.new" default="New FbtStatutoryMethodRateParameter" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="fbtStatutoryMethodRateParameter.show" default="Show FbtStatutoryMethodRateParameter" /></h1>
            <g:if test="${flash.message}">
            <div class="message"><g:message code="${flash.message}" args="${flash.args}" default="${flash.defaultMessage}" /></div>
            </g:if>
            <g:form>
                <g:hiddenField name="id" value="${fbtStatutoryMethodRateParameterInstance?.id}" />
                <div class="dialog">
                    <table>
                        <tbody>
                        
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="fbtStatutoryMethodRateParameter.id" default="Id" />:</td>
                                
                                <td valign="top" class="value">${fieldValue(bean: fbtStatutoryMethodRateParameterInstance, field: "id")}</td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="fbtStatutoryMethodRateParameter.dateFrom" default="Date From" />:</td>
                                
                                <td valign="top" class="value"><g:formatDate date="${fbtStatutoryMethodRateParameterInstance?.dateFrom}" /></td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="fbtStatutoryMethodRateParameter.customerCode" default="Customer Code" />:</td>
                                
                                <td valign="top" class="value">${fieldValue(bean: fbtStatutoryMethodRateParameterInstance, field: "customerCode")}</td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="fbtStatutoryMethodRateParameter.taxableValueTypeCode" default="Taxable Value Type Code" />:</td>
                                
                                <td valign="top" class="value">${fieldValue(bean: fbtStatutoryMethodRateParameterInstance, field: "taxableValueTypeCode")}</td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="fbtStatutoryMethodRateParameter.lowerValue" default="Lower Value" />:</td>
                                
                                <td valign="top" class="value">${fieldValue(bean: fbtStatutoryMethodRateParameterInstance, field: "lowerValue")}</td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="fbtStatutoryMethodRateParameter.upperValue" default="Upper Value" />:</td>
                                
                                <td valign="top" class="value">${fieldValue(bean: fbtStatutoryMethodRateParameterInstance, field: "upperValue")}</td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="fbtStatutoryMethodRateParameter.rate" default="Rate" />:</td>
                                
                                <td valign="top" class="value">${fieldValue(bean: fbtStatutoryMethodRateParameterInstance, field: "rate")}</td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="fbtStatutoryMethodRateParameter.lastUpdated" default="Last Updated" />:</td>
                                
                                <td valign="top" class="value"><g:formatDate date="${fbtStatutoryMethodRateParameterInstance?.lastUpdated}" /></td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="fbtStatutoryMethodRateParameter.dateCreated" default="Date Created" />:</td>
                                
                                <td valign="top" class="value"><g:formatDate date="${fbtStatutoryMethodRateParameterInstance?.dateCreated}" /></td>
                                
                            </tr>
                            
                        </tbody>
                    </table>
                </div>
                <div class="buttons">
                    <span class="button"><g:actionSubmit class="edit" action="edit" value="${message(code: 'edit', 'default': 'Edit')}" /></span>
                    <span class="button"><g:actionSubmit class="delete" action="delete" value="${message(code: 'delete', 'default': 'Delete')}" onclick="return confirm('${message(code: 'delete.confirm', 'default': 'Are you sure?')}');" /></span>
                </div>
            </g:form>
        </div>
    </body>
</html>
