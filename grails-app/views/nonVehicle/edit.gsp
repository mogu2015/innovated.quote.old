
<%@ page import="com.innovated.iris.domain.NonVehicle" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <title><g:message code="nonVehicle.edit" default="Edit NonVehicle" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLinkTo(dir: '')}"><g:message code="home" default="Home" /></a></span>
            <span class="menuButton"><g:link class="list" action="list"><g:message code="nonVehicle.list" default="NonVehicle List" /></g:link></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="nonVehicle.new" default="New NonVehicle" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="nonVehicle.edit" default="Edit NonVehicle" /></h1>
            <g:if test="${flash.message}">
            	<div class="message"><g:message code="${flash.message}" args="${flash.args}" default="${flash.defaultMessage}" /></div>
            </g:if>
            <g:hasErrors bean="${nonVehicleInstance}">
				<div class="errors">
					<g:renderErrors bean="${nonVehicleInstance}" as="list" />
				</div>
            </g:hasErrors>
            <g:form method="post" >
                <g:hiddenField name="id" value="${nonVehicleInstance?.id}" />
                <g:hiddenField name="version" value="${nonVehicleInstance?.version}" />
                <div class="dialog">
                    <fieldset>
                        <legend><g:message code="nonVehicle.edit.legend" default="Update NonVehicle Details"/></legend>
                        
                        <div class="prop ${hasErrors(bean: nonVehicleInstance, field: 'assetCode', 'error')}">
                            <label for="assetCode">
                                <g:message code="nonVehicle.assetCode" default="Asset Code" />
                                
                            </label>
                            <g:textField name="assetCode" value="${fieldValue(bean: nonVehicleInstance, field: 'assetCode')}" />

                        </div>
                        
                        <div class="prop mandatory ${hasErrors(bean: nonVehicleInstance, field: 'owner', 'error')}">
                            <label for="owner">
                                <g:message code="nonVehicle.owner" default="Owner" />
                                <span class="indicator">*</span>
                            </label>
                            <g:select name="owner.id" from="${com.innovated.iris.domain.Customer.list()}" optionKey="id" value="${nonVehicleInstance?.owner?.id}"  />

                        </div>
                        
                        <div class="prop mandatory ${hasErrors(bean: nonVehicleInstance, field: 'acquisitionDate', 'error')}">
                            <label for="acquisitionDate">
                                <g:message code="nonVehicle.acquisitionDate" default="Acquisition Date" />
                                <span class="indicator">*</span>
                            </label>
                            <g:datePicker name="acquisitionDate" value="${nonVehicleInstance?.acquisitionDate}"  />

                        </div>
                        
                        <div class="prop mandatory ${hasErrors(bean: nonVehicleInstance, field: 'acquisitionPrice', 'error')}">
                            <label for="acquisitionPrice">
                                <g:message code="nonVehicle.acquisitionPrice" default="Acquisition Price" />
                                <span class="indicator">*</span>
                            </label>
                            <g:textField name="acquisitionPrice" value="${fieldValue(bean: nonVehicleInstance, field: 'acquisitionPrice')}" />

                        </div>
                        
                        <div class="prop ${hasErrors(bean: nonVehicleInstance, field: 'disposalDate', 'error')}">
                            <label for="disposalDate">
                                <g:message code="nonVehicle.disposalDate" default="Disposal Date" />
                                
                            </label>
                            <g:datePicker name="disposalDate" value="${nonVehicleInstance?.disposalDate}" noSelection="['': '']" />

                        </div>
                        
                        <div class="prop ${hasErrors(bean: nonVehicleInstance, field: 'disposalPrice', 'error')}">
                            <label for="disposalPrice">
                                <g:message code="nonVehicle.disposalPrice" default="Disposal Price" />
                                
                            </label>
                            <g:textField name="disposalPrice" value="${fieldValue(bean: nonVehicleInstance, field: 'disposalPrice')}" />

                        </div>
                        
                        <div class="prop ${hasErrors(bean: nonVehicleInstance, field: 'information', 'error')}">
                            <label for="information">
                                <g:message code="nonVehicle.information" default="Information" />
                                
                            </label>
                            <g:textField name="information" value="${fieldValue(bean: nonVehicleInstance, field: 'information')}" />

                        </div>
                        
                        <div class="prop mandatory ${hasErrors(bean: nonVehicleInstance, field: 'budgets', 'error')}">
                            <label for="budgets">
                                <g:message code="nonVehicle.budgets" default="Budgets" />
                                <span class="indicator">*</span>
                            </label>
                            <g:select name="budgets"
from="${com.innovated.iris.domain.Budget.list()}"
size="5" multiple="yes" optionKey="id"
value="${nonVehicleInstance?.budgets}" />


                        </div>
                        
                        <div class="prop mandatory ${hasErrors(bean: nonVehicleInstance, field: 'dateCreated', 'error')}">
                            <label for="dateCreated">
                                <g:message code="nonVehicle.dateCreated" default="Date Created" />
                                <span class="indicator">*</span>
                            </label>
                            <g:datePicker name="dateCreated" value="${nonVehicleInstance?.dateCreated}"  />

                        </div>
                        
                        <div class="prop mandatory ${hasErrors(bean: nonVehicleInstance, field: 'lastUpdated', 'error')}">
                            <label for="lastUpdated">
                                <g:message code="nonVehicle.lastUpdated" default="Last Updated" />
                                <span class="indicator">*</span>
                            </label>
                            <g:datePicker name="lastUpdated" value="${nonVehicleInstance?.lastUpdated}"  />

                        </div>
                        
                    </fieldset>
                </div>
                <div class="buttons">
                    <span class="button"><g:actionSubmit class="save" action="update" value="${message(code: 'update', 'default': 'Update')}" /></span>
                    <span class="button"><g:actionSubmit class="delete" action="delete" value="${message(code: 'delete', 'default': 'Delete')}" onclick="return confirm('${message(code: 'delete.confirm', 'default': 'Are you sure?')}');" /></span>
                </div>
            </g:form>
        </div>
    </body>
</html>

