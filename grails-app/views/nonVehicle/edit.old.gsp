
<%@ page import="com.innovated.iris.domain.NonVehicle" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <title><g:message code="nonVehicle.edit" default="Edit NonVehicle" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLinkTo(dir: '')}"><g:message code="home" default="Home" /></a></span>
            <span class="menuButton"><g:link class="list" action="list"><g:message code="nonVehicle.list" default="NonVehicle List" /></g:link></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="nonVehicle.new" default="New NonVehicle" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="nonVehicle.edit" default="Edit NonVehicle" /></h1>
            <g:if test="${flash.message}">
            <div class="message"><g:message code="${flash.message}" args="${flash.args}" default="${flash.defaultMessage}" /></div>
            </g:if>
            <g:hasErrors bean="${nonVehicleInstance}">
            <div class="errors">
                <g:renderErrors bean="${nonVehicleInstance}" as="list" />
            </div>
            </g:hasErrors>
            <g:form method="post" >
                <g:hiddenField name="id" value="${nonVehicleInstance?.id}" />
                <g:hiddenField name="version" value="${nonVehicleInstance?.version}" />
                <div class="dialog">
                    <table>
                        <tbody>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="assetCode"><g:message code="nonVehicle.assetCode" default="Asset Code" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: nonVehicleInstance, field: 'assetCode', 'errors')}">
                                    <g:textField name="assetCode" value="${fieldValue(bean: nonVehicleInstance, field: 'assetCode')}" />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="owner"><g:message code="nonVehicle.owner" default="Owner" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: nonVehicleInstance, field: 'owner', 'errors')}">
                                    <g:select name="owner.id" from="${com.innovated.iris.domain.Customer.list()}" optionKey="id" value="${nonVehicleInstance?.owner?.id}"  />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="acquisitionDate"><g:message code="nonVehicle.acquisitionDate" default="Acquisition Date" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: nonVehicleInstance, field: 'acquisitionDate', 'errors')}">
                                    <g:datePicker name="acquisitionDate" value="${nonVehicleInstance?.acquisitionDate}"  />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="acquisitionPrice"><g:message code="nonVehicle.acquisitionPrice" default="Acquisition Price" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: nonVehicleInstance, field: 'acquisitionPrice', 'errors')}">
                                    <g:textField name="acquisitionPrice" value="${fieldValue(bean: nonVehicleInstance, field: 'acquisitionPrice')}" />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="disposalDate"><g:message code="nonVehicle.disposalDate" default="Disposal Date" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: nonVehicleInstance, field: 'disposalDate', 'errors')}">
                                    <g:datePicker name="disposalDate" value="${nonVehicleInstance?.disposalDate}" noSelection="['': '']" />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="disposalPrice"><g:message code="nonVehicle.disposalPrice" default="Disposal Price" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: nonVehicleInstance, field: 'disposalPrice', 'errors')}">
                                    <g:textField name="disposalPrice" value="${fieldValue(bean: nonVehicleInstance, field: 'disposalPrice')}" />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="information"><g:message code="nonVehicle.information" default="Information" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: nonVehicleInstance, field: 'information', 'errors')}">
                                    <g:textField name="information" value="${fieldValue(bean: nonVehicleInstance, field: 'information')}" />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="budgets"><g:message code="nonVehicle.budgets" default="Budgets" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: nonVehicleInstance, field: 'budgets', 'errors')}">
                                    <g:select name="budgets"
from="${com.innovated.iris.domain.Budget.list()}"
size="5" multiple="yes" optionKey="id"
value="${nonVehicleInstance?.budgets}" />


                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="dateCreated"><g:message code="nonVehicle.dateCreated" default="Date Created" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: nonVehicleInstance, field: 'dateCreated', 'errors')}">
                                    <g:datePicker name="dateCreated" value="${nonVehicleInstance?.dateCreated}"  />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="lastUpdated"><g:message code="nonVehicle.lastUpdated" default="Last Updated" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: nonVehicleInstance, field: 'lastUpdated', 'errors')}">
                                    <g:datePicker name="lastUpdated" value="${nonVehicleInstance?.lastUpdated}"  />

                                </td>
                            </tr>
                        
                        </tbody>
                    </table>
                </div>
                <div class="buttons">
                    <span class="button"><g:actionSubmit class="save" action="update" value="${message(code: 'update', 'default': 'Update')}" /></span>
                    <span class="button"><g:actionSubmit class="delete" action="delete" value="${message(code: 'delete', 'default': 'Delete')}" onclick="return confirm('${message(code: 'delete.confirm', 'default': 'Are you sure?')}');" /></span>
                </div>
            </g:form>
        </div>
    </body>
</html>
