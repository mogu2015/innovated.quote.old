
<%@ page import="com.innovated.iris.domain.NonVehicle" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <title><g:message code="nonVehicle.list" default="NonVehicle List" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLinkTo(dir: '')}"><g:message code="home" default="Home" /></a></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="nonVehicle.new" default="New NonVehicle" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="nonVehicle.list" default="NonVehicle List" /></h1>
            <g:if test="${flash.message}">
            <div class="message"><g:message code="${flash.message}" args="${flash.args}" default="${flash.defaultMessage}" /></div>
            </g:if>
            <div class="list">
                <table>
                    <thead>
                        <tr>
                        
                   	    <g:sortableColumn property="id" title="Id" titleKey="nonVehicle.id" />
                        
                   	    <g:sortableColumn property="assetCode" title="Asset Code" titleKey="nonVehicle.assetCode" />
                        
                   	    <th><g:message code="nonVehicle.owner" default="Owner" /></th>
                   	    
                   	    <g:sortableColumn property="acquisitionDate" title="Acquisition Date" titleKey="nonVehicle.acquisitionDate" />
                        
                   	    <g:sortableColumn property="acquisitionPrice" title="Acquisition Price" titleKey="nonVehicle.acquisitionPrice" />
                        
                   	    <g:sortableColumn property="disposalDate" title="Disposal Date" titleKey="nonVehicle.disposalDate" />
                        
                        </tr>
                    </thead>
                    <tbody>
                    <g:each in="${nonVehicleInstanceList}" status="i" var="nonVehicleInstance">
                        <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
                        
                            <td><g:link action="show" id="${nonVehicleInstance.id}">${fieldValue(bean: nonVehicleInstance, field: "id")}</g:link></td>
                        
                            <td>${fieldValue(bean: nonVehicleInstance, field: "assetCode")}</td>
                        
                            <td>${fieldValue(bean: nonVehicleInstance, field: "owner")}</td>
                        
                            <td><g:formatDate date="${nonVehicleInstance.acquisitionDate}" /></td>
                        
                            <td><g:formatNumber number="${nonVehicleInstance.acquisitionPrice}" /></td>
                        
                            <td><g:formatDate date="${nonVehicleInstance.disposalDate}" /></td>
                        
                        </tr>
                    </g:each>
                    </tbody>
                </table>
            </div>
            <div class="paginateButtons">
                <g:paginate total="${nonVehicleInstanceTotal}" />
            </div>
        </div>
    </body>
</html>
