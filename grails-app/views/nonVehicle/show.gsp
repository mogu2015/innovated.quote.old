
<%@ page import="com.innovated.iris.domain.NonVehicle" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <title><g:message code="nonVehicle.show" default="Show NonVehicle" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLinkTo(dir: '')}"><g:message code="home" default="Home" /></a></span>
            <span class="menuButton"><g:link class="list" action="list"><g:message code="nonVehicle.list" default="NonVehicle List" /></g:link></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="nonVehicle.new" default="New NonVehicle" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="nonVehicle.show" default="Show NonVehicle" /></h1>
            <g:if test="${flash.message}">
            <div class="message"><g:message code="${flash.message}" args="${flash.args}" default="${flash.defaultMessage}" /></div>
            </g:if>
            <g:form>
                <g:hiddenField name="id" value="${nonVehicleInstance?.id}" />
                <div class="dialog">
                    <table>
                        <tbody>
                        
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="nonVehicle.id" default="Id" />:</td>
                                
                                <td valign="top" class="value">${fieldValue(bean: nonVehicleInstance, field: "id")}</td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="nonVehicle.assetCode" default="Asset Code" />:</td>
                                
                                <td valign="top" class="value">${fieldValue(bean: nonVehicleInstance, field: "assetCode")}</td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="nonVehicle.owner" default="Owner" />:</td>
                                
                                <td valign="top" class="value"><g:link controller="customer" action="show" id="${nonVehicleInstance?.owner?.id}">${nonVehicleInstance?.owner?.encodeAsHTML()}</g:link></td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="nonVehicle.acquisitionDate" default="Acquisition Date" />:</td>
                                
                                <td valign="top" class="value"><g:formatDate date="${nonVehicleInstance?.acquisitionDate}" /></td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="nonVehicle.acquisitionPrice" default="Acquisition Price" />:</td>
                                
                                <td valign="top" class="value"><g:formatNumber number="${nonVehicleInstance?.acquisitionPrice}" /></td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="nonVehicle.disposalDate" default="Disposal Date" />:</td>
                                
                                <td valign="top" class="value"><g:formatDate date="${nonVehicleInstance?.disposalDate}" /></td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="nonVehicle.disposalPrice" default="Disposal Price" />:</td>
                                
                                <td valign="top" class="value"><g:formatNumber number="${nonVehicleInstance?.disposalPrice}" /></td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="nonVehicle.information" default="Information" />:</td>
                                
                                <td valign="top" class="value">${fieldValue(bean: nonVehicleInstance, field: "information")}</td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="nonVehicle.budgets" default="Budgets" />:</td>
                                
                                <td  valign="top" style="text-align: left;" class="value">
                                    <ul>
                                    <g:each in="${nonVehicleInstance?.budgets}" var="budgetInstance">
                                        <li><g:link controller="budget" action="show" id="${budgetInstance.id}">${budgetInstance.encodeAsHTML()}</g:link></li>
                                    </g:each>
                                    </ul>
                                </td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="nonVehicle.dateCreated" default="Date Created" />:</td>
                                
                                <td valign="top" class="value"><g:formatDate date="${nonVehicleInstance?.dateCreated}" /></td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="nonVehicle.lastUpdated" default="Last Updated" />:</td>
                                
                                <td valign="top" class="value"><g:formatDate date="${nonVehicleInstance?.lastUpdated}" /></td>
                                
                            </tr>
                            
                        </tbody>
                    </table>
                </div>
                <div class="buttons">
                    <span class="button"><g:actionSubmit class="edit" action="edit" value="${message(code: 'edit', 'default': 'Edit')}" /></span>
                    <span class="button"><g:actionSubmit class="delete" action="delete" value="${message(code: 'delete', 'default': 'Delete')}" onclick="return confirm('${message(code: 'delete.confirm', 'default': 'Are you sure?')}');" /></span>
                </div>
            </g:form>
        </div>
    </body>
</html>
