
<%@ page import="com.innovated.iris.domain.Customer" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="prod">
        <title><g:message code="customer.create" default="Create Customer" /></title>
    </head>
	<body>
		<div class="twocol">
			<div id="content">
				<div id="leftcol">
					<div id="contentMain">
						<p class="bread">
							<a class="home" href="${createLinkTo(dir: '')}"><g:message code="home" default="Home" /></a>
							<span class="breadArrow">&nbsp;</span>
							<g:link action="list"><g:message code="customer.list" default="Customer List" /></g:link>
							<span class="breadArrow">&nbsp;</span>
							<g:message code="customer.create" default="Create Customer" />
						</p>
						
						<g:if test="${flash.message}">
						<div class="flashMsg">
							<p><g:message code="${flash.message}" args="${flash.args}" default="${flash.defaultMessage}" /></p>
						</div>
						</g:if>
			            <g:hasErrors bean="${customerInstance}">
						<div class="errors">
							<ul>
								<g:eachError bean="${customerInstance}"><li><g:message error="${it}"/></li>
								</g:eachError>
							</ul>
						</div>
		            	</g:hasErrors>
						
						<h1 class="contentH1"><g:message code="customer.create" default="Create Customer" /></h1>
						<div class="formBg">
							<div class="formWrap">
								<g:form action="save" method="post" >
								
								<h3 class="topPad"><g:message code="customer.create" default="Create Customer" /></h3>
								<p>Please enter the details for this new Customer:</p>
								<div class="formSet">
						
									<div class="clearfix mandatory ${hasErrors(bean: customerInstance, field: 'code', 'error')}">
		                                <label for="code"><g:message code="customer.code" default="Code" /></label>
		                                <g:textField class="short" name="code" value="${fieldValue(bean: customerInstance, field: 'code')}" />
		                            </div>
		                        
		                            <div class="clearfix ${hasErrors(bean: customerInstance, field: 'supplierLinkCode', 'error')}">
		                                <label for="supplierLinkCode"><g:message code="customer.supplierLinkCode" default="Supplier Link Code" /></label>
		                                <g:textField class="short" name="supplierLinkCode" value="${fieldValue(bean: customerInstance, field: 'supplierLinkCode')}" />
		                            </div>
		                        
		                            <div class="clearfix mandatory ${hasErrors(bean: customerInstance, field: 'entity', 'error')}">
		                                <label for="entity"><g:message code="customer.entity" default="Entity" /></label>
		                                <g:select class="long" name="entity.id" from="${com.innovated.iris.domain.Entity.list()}" optionKey="id" value="${customerInstance?.entity?.id}"  />
		                            </div>
		                        
		                            <div class="clearfix mandatory ${hasErrors(bean: customerInstance, field: 'type', 'error')}">
		                                <label for="type"><g:message code="customer.type" default="Type" /></label>
		                                <g:select class="short" name="type.id" from="${com.innovated.iris.domain.enums.CustomerType.list()}" optionKey="id" value="${customerInstance?.type?.id}"  />
									</div>
		                        
		                            <div class="clearfix mandatory ${hasErrors(bean: customerInstance, field: 'status', 'error')}">
		                                <label for="status"><g:message code="customer.status" default="Status" /></label>
		                                <g:select class="short" name="status.id" from="${com.innovated.iris.domain.enums.CustomerStatus.list()}" optionKey="id" value="${customerInstance?.status?.id}"  />
		                            </div>
		                        
		                            <div class="clearfix mandatory ${hasErrors(bean: customerInstance, field: 'joined', 'error')}">
		                                <label for="joined"><g:message code="customer.joined" default="Joined" /></label>
		                                <g:textField class="short dateField" name="joined" value="${g.formatDate(format:'d/M/yyyy', date:customerInstance?.joined)}" />
		                            </div>
		                        </div>
								
								<button id="btnCreate" name="btnCreate" class="greybutton" type="submit">
						       		<span><img width="16" height="16" alt="" src="${resource(dir:'images/skin',file:'disk.png')}"/><g:message code="create" default="Create" /></span>
						    	</button>
						    	<span class="formcancel"> or&nbsp;&nbsp;<g:link action="list"><g:message code="cancel" default="cancel" /></g:link></span>
						    	
								</g:form>
							</div>
						</div>
						
					</div> <!-- #contentMain -->
					
				</div>
				<div id="rightcol">
					<div id="options">
						<div class="optGrpHdr"><h3 class="optHdr"><g:message code="default.rcol.current.opts" default="Current Options" /></h3></div>
						<dl class="optGrpMenu">
							<iris:sidebarItem controller="customer" action="list" icon="page_white_stack.png" titleCode="customer.rcol.title.list" msgCode="customer.rcol.msg.list" />
							<iris:sidebarItem controller="customer" action="newCompany" icon="building_add.png" titleCode="ui.opt.title.addcompany" msgCode="ui.opt.msg.addcompany" />
							<iris:sidebarItem controller="customer" action="newIndividual" icon="user_add.png" titleCode="ui.opt.title.addindiv" msgCode="ui.opt.msg.addindiv" />
						</dl>
					</div>
				</div>
				<div class="clear"></div>
			</div>
		</div>
	</body>
</html>




