
<%@ page import="com.innovated.iris.domain.Customer" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <title><g:message code="customer.edit" default="Edit Customer" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLinkTo(dir: '')}"><g:message code="home" default="Home" /></a></span>
            <span class="menuButton"><g:link class="list" action="list"><g:message code="customer.list" default="Customer List" /></g:link></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="customer.new" default="New Customer" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="customer.edit" default="Edit Customer" /></h1>
            <g:if test="${flash.message}">
            	<div class="message"><g:message code="${flash.message}" args="${flash.args}" default="${flash.defaultMessage}" /></div>
            </g:if>
            <g:hasErrors bean="${customerInstance}">
				<div class="errors">
					<g:renderErrors bean="${customerInstance}" as="list" />
				</div>
            </g:hasErrors>
            <g:form method="post" >
                <g:hiddenField name="id" value="${customerInstance?.id}" />
                <g:hiddenField name="version" value="${customerInstance?.version}" />
                <div class="dialog">
                    <fieldset>
                        <legend><g:message code="customer.edit.legend" default="Update Customer Details"/></legend>
                        
                        <div class="prop mandatory ${hasErrors(bean: customerInstance, field: 'code', 'error')}">
                            <label for="code">
                                <g:message code="customer.code" default="Code" />
                                <span class="indicator">*</span>
                            </label>
                            <g:textField name="code" value="${fieldValue(bean: customerInstance, field: 'code')}" />

                        </div>
                        
                        <div class="prop ${hasErrors(bean: customerInstance, field: 'supplierLinkCode', 'error')}">
                            <label for="supplierLinkCode">
                                <g:message code="customer.supplierLinkCode" default="Supplier Link Code" />
                                
                            </label>
                            <g:textField name="supplierLinkCode" value="${fieldValue(bean: customerInstance, field: 'supplierLinkCode')}" />

                        </div>
                        
                        <div class="prop mandatory ${hasErrors(bean: customerInstance, field: 'entity', 'error')}">
                            <label for="entity">
                                <g:message code="customer.entity" default="Entity" />
                                <span class="indicator">*</span>
                            </label>
                            <g:select name="entity.id" from="${com.innovated.iris.domain.Entity.list()}" optionKey="id" value="${customerInstance?.entity?.id}"  />

                        </div>
                        
                        <div class="prop mandatory ${hasErrors(bean: customerInstance, field: 'type', 'error')}">
                            <label for="type">
                                <g:message code="customer.type" default="Type" />
                                <span class="indicator">*</span>
                            </label>
                            <g:select name="type.id" from="${com.innovated.iris.domain.enums.CustomerType.list()}" optionKey="id" value="${customerInstance?.type?.id}"  />

                        </div>
                        
                        <div class="prop mandatory ${hasErrors(bean: customerInstance, field: 'status', 'error')}">
                            <label for="status">
                                <g:message code="customer.status" default="Status" />
                                <span class="indicator">*</span>
                            </label>
                            <g:select name="status.id" from="${com.innovated.iris.domain.enums.CustomerStatus.list()}" optionKey="id" value="${customerInstance?.status?.id}"  />

                        </div>
                        
                        <div class="prop mandatory ${hasErrors(bean: customerInstance, field: 'joined', 'error')}">
                            <label for="joined">
                                <g:message code="customer.joined" default="Joined" />
                                <span class="indicator">*</span>
                            </label>
                            <g:datePicker name="joined" value="${customerInstance?.joined}"  />

                        </div>
                        
                        <div class="prop ${hasErrors(bean: customerInstance, field: 'ceased', 'error')}">
                            <label for="ceased">
                                <g:message code="customer.ceased" default="Ceased" />
                                
                            </label>
                            <g:datePicker name="ceased" value="${customerInstance?.ceased}" noSelection="['': '']" />

                        </div>
                        
                        <div class="prop ${hasErrors(bean: customerInstance, field: 'assets', 'error')}">
                            <label for="assets">
                                <g:message code="customer.assets" default="Assets" />
                                
                            </label>
                            
<ul>
<g:each in="${customerInstance?.assets}" var="assetInstance">
    <li><g:link controller="asset" action="show" id="${assetInstance.id}">${assetInstance?.encodeAsHTML()}</g:link></li>
</g:each>
</ul>
<g:link controller="asset" params="['customer.id': customerInstance?.id]" action="create"><g:message code="asset.new" default="New Asset" /></g:link>


                        </div>
                        
                        <div class="prop mandatory ${hasErrors(bean: customerInstance, field: 'dateCreated', 'error')}">
                            <label for="dateCreated">
                                <g:message code="customer.dateCreated" default="Date Created" />
                                <span class="indicator">*</span>
                            </label>
                            <g:datePicker name="dateCreated" value="${customerInstance?.dateCreated}"  />

                        </div>
                        
                        <div class="prop mandatory ${hasErrors(bean: customerInstance, field: 'lastUpdated', 'error')}">
                            <label for="lastUpdated">
                                <g:message code="customer.lastUpdated" default="Last Updated" />
                                <span class="indicator">*</span>
                            </label>
                            <g:datePicker name="lastUpdated" value="${customerInstance?.lastUpdated}"  />

                        </div>
                        
                    </fieldset>
                </div>
                <div class="buttons">
                    <span class="button"><g:actionSubmit class="save" action="update" value="${message(code: 'update', 'default': 'Update')}" /></span>
                    <span class="button"><g:actionSubmit class="delete" action="delete" value="${message(code: 'delete', 'default': 'Delete')}" onclick="return confirm('${message(code: 'delete.confirm', 'default': 'Are you sure?')}');" /></span>
                </div>
            </g:form>
        </div>
    </body>
</html>

