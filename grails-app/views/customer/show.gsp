<%@ page import="com.innovated.iris.domain.Entity" %>
<%@ page import="com.innovated.iris.domain.Company" %>
<%@ page import="com.innovated.iris.domain.Customer" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="prod">
        <title><g:message code="customer.show" default="Show Customer" /></title>
    </head>
	<body>
		<div class="twocol">
			<div id="content">
				<div id="leftcol">
					<div id="contentMain">
						<p class="bread">
							<a class="home" href="${createLinkTo(dir: '')}"><g:message code="home" default="Home" /></a>
							<span class="breadArrow">&nbsp;</span>
							<g:link action="list"><g:message code="customer.list" default="Customer List" /></g:link>
							<span class="breadArrow">&nbsp;</span>
							<g:message code="customer.now.showing" default="Viewing Customer: ${customerInstance?.entity?.name}" args="${[customerInstance?.entity?.name]}" />
						</p>
						
						<g:if test="${flash.message}">
						<div class="flashMsg">
							<p><g:message code="${flash.message}" args="${flash.args}" default="${flash.defaultMessage}" /></p>
						</div>
						</g:if>
						
						<h1 class="contentH1">${fieldValue(bean: customerInstance, field: "entity.name")}</h1>
						<div class="formBg">
							<div class="formWrap">
								<g:form>
								<g:hiddenField name="id" value="${customerInstance?.id}" />
								
								<h3 class="topPad">Customer Details</h3>
								<div class="formSet">
									<table class="grid">
										<tbody>
											<tr>
												<th>Code</th>
												<td class="special">${customerInstance?.code}</td>
												<td class="spacer">&nbsp;</td>
												<th>Supplier Link</th>
												<td>${customerInstance?.supplierLinkCode}</td>
											</tr>
											<tr>
												<th>Type</th>
												<td class="special">${customerInstance?.type}</td>
												<td class="spacer">&nbsp;</td>
												<th>Commenced</th><g:set var="joined" value="${customerInstance?.joined}"/><g:set var="joinedDiff" value="${new Date().year - joined.year}"/>
												<td><span style="padding-right:10px;"><g:formatDate format="d MMM yyyy" date="${joined}"/></span><g:if test="${joined != null && joinedDiff > 0}">(${joinedDiff} year${joinedDiff==1 ? '' : 's' })</g:if></td>
											</tr>
											<tr>
												<th class="last">Status</th>
												<td class="last special">${customerInstance?.status}</td>
												<td class="last spacer">&nbsp;</td>
												<th class="last">Ceased</th>
												<td class="last"><g:formatDate format="d MMM yyyy" date="${customerInstance?.ceased}"/></td>
											</tr>
										</tbody>
									</table>
								</div>
								
								<h3 class="topPad">Linked Entity Details</h3>
								<g:render template="/templates/ui/customer/entityview" bean="${customerInstance}" />
								
								
								
								<button id="btnEdit" name="_action_edit" class="greybutton" type="submit">
						       		<span><img width="16" height="16" alt="" src="${resource(dir:'images/skin',file:'database_edit.png')}"/><g:message code="edit" default="Edit" /></span>
						    	</button>
						    	<button id="btnDelete" name="_action_delete" class="greybutton" type="submit" onclick="return confirm('${message(code: 'delete.confirm', 'default': 'Are you sure?')}');">
						       		<span><img width="16" height="16" alt="" src="${resource(dir:'images/skin',file:'database_delete.png')}"/><g:message code="delete" default="Delete" /></span>
						    	</button>
						    	<span class="formcancel"> or&nbsp;&nbsp;<g:link action="list"><g:message code="cancel" default="cancel" /></g:link></span>
						    	
								</g:form>
							</div>
						</div>
						
					</div> <!-- #contentMain -->
					
				</div>
				<div id="rightcol">
					<div id="options">
						<div class="optGrpHdr"><h3 class="optHdr"><g:message code="customer.rcol.opts.tasks" default="Customer Tasks" /></h3></div>
						<dl class="optGrpMenu">
							<iris:sidebarItem controller="customer" action="edit" id="${customerInstance?.id}" icon="page_white_edit.png" titleCode="customer.rcol.title.edit" msgCode="customer.rcol.msg.edit" />
						</dl>
						<div class="optGrpHdr"><h3 class="optHdr"><g:message code="default.rcol.current.opts" default="Current Options" /></h3></div>
						<dl class="optGrpMenu">
							<iris:sidebarItem controller="customer" action="list" icon="page_white_stack.png" titleCode="customer.rcol.title.list" msgCode="customer.rcol.msg.list" />
							<iris:sidebarItem controller="customer" action="newCompany" icon="building_add.png" titleCode="ui.opt.title.addcompany" msgCode="ui.opt.msg.addcompany" />
							<iris:sidebarItem controller="customer" action="newIndividual" icon="user_add.png" titleCode="ui.opt.title.addindiv" msgCode="ui.opt.msg.addindiv" />
						</dl>
					</div>
				</div>
				<div class="clear"></div>
			</div>
		</div>
	</body>
</html>
