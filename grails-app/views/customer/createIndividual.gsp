<%@ page import="com.innovated.iris.domain.Entity" %>
<%@ page import="com.innovated.iris.domain.Individual" %>
<%@ page import="com.innovated.iris.domain.Customer" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="prod">
        <title><g:message code="customer.individual.create" default="Create Individual" /></title>
        <script type="text/javascript">
        $(document).ready(function() {
        	// construct preferredName from the others entered
        	var fieldNames = "#firstName, #middleName, #lastName";
        	$(fieldNames).blur(function(){
				var names = new Array();
				$.each(fieldNames.split(", "), function(i,v) {
					var txt = $(v).val().trim();
					if (txt) names.push(txt);
				});
				$("#preferredName").val(names.join(" ").trim())
            });
        });
        </script>
    </head>
	<body>
		<div class="twocol">
			<div id="content">
				<div id="leftcol">
					<div id="contentMain">
						<p class="bread">
							<a class="home" href="${createLinkTo(dir: '')}"><g:message code="home" default="Home" /></a>
							<span class="breadArrow">&nbsp;</span>
							<g:link action="list"><g:message code="customer.list" default="Customer List" /></g:link>
							<span class="breadArrow">&nbsp;</span>
							<g:message code="customer.individual.create" default="Create Individual" />
						</p>
						
						<g:if test="${flash.message}">
						<div class="flashMsg">
							<p><g:message code="${flash.message}" args="${flash.args}" default="${flash.defaultMessage}" /></p>
						</div>
						</g:if>
			            
			            <g:set var="individual" value="${(Individual) customerInstance.entity}" />
			            
			            <g:if test="${customerInstance.hasErrors() || individual.hasErrors()}">
						<div class="errors">
							<ul>
								<g:if test="${customerInstance.hasErrors()}">
								<g:eachError bean="${customerInstance}"><li><g:message error="${it}"/></li>
								</g:eachError>
								</g:if>
								<g:if test="${individual.hasErrors()}">
								<g:eachError bean="${individual}" >
									<g:if test="${it.field != 'name'}">
									<li><g:message error="${it}"/></li>
									</g:if>
								</g:eachError>
								</g:if>
							</ul>
						</div>
						</g:if>
						
						<h1 class="contentH1"><g:message code="customer.individual.create" default="Create Individual" /></h1>
						<div class="formBg">
							<div class="formWrap">
								<g:form action="saveIndividual" method="post" >
								
								<g:render template="typeAndStatus" bean="${customerInstance}" />
								
								<h3 class="topPad">Personal Details</h3>
								<p>Please enter the details for this new Individual:</p>
								<div class="formSet">
									<div class="clearfix mandatory ${hasErrors(bean: individual, field: 'firstName', 'error')}">
		                                <label for="entity.firstName"><g:message code="individual.firstName" default="First Name" /></label>
		                                <g:textField id="firstName" class="short" name="entity.firstName" value="${individual?.firstName?.encodeAsHTML()}" />
		                            </div>
		                            <div class="clearfix ${hasErrors(bean: individual, field: 'middleName', 'error')}">
		                                <label for="entity.middleName"><g:message code="individual.middleName" default="Middle Name" /></label>
		                                <g:textField id="middleName" class="short" name="entity.middleName" value="${individual?.middleName?.encodeAsHTML()}" />
		                            </div>
		                            <div class="clearfix mandatory ${hasErrors(bean: individual, field: 'lastName', 'error')}">
		                                <label for="entity.lastName"><g:message code="individual.lastName" default="Last Name" /></label>
		                                <g:textField id="lastName" class="short" name="entity.lastName" value="${individual?.lastName?.encodeAsHTML()}" />
		                            </div>
		                            <div class="clearfix mandatory ${hasErrors(bean: individual, field: 'preferredName', 'error')}">
		                                <label for="entity.preferredName"><g:message code="individual.preferredName" default="Preferred Name" /></label>
		                                <g:textField id="preferredName" class="short" name="entity.preferredName" value="${individual?.preferredName?.encodeAsHTML()}" />
		                            </div>
		                            <div class="clearfix ${hasErrors(bean: individual, field: 'dateOfBirth', 'error')}">
		                                <label for="entity.dateOfBirth"><g:message code="individual.dateOfBirth" default="Date Of Birth" /></label>
		                                <g:textField class="short dateField" name="entity.dateOfBirth" value="${g.formatDate(format:'d/M/yyyy', date:individual?.dateOfBirth)}" />
		                            </div>
		                            <div class="clearfix mandatory ${hasErrors(bean: individual, field: 'gender', 'error')}">
		                                <label for="entity.gender"><g:message code="individual.gender" default="Gender" /></label>
		                                <g:select class="short" name="entity.gender" optionKey="${{it.value?.name()}}" optionValue="value" from="${com.innovated.iris.enums.Gender.lookup}" value="${individual?.gender?.name()}"></g:select>
		                            </div>
		                        </div>
								
								<g:render template="contactDetails" bean="${customerInstance}" />
								
								<button id="btnCreate" name="btnCreate" class="greybutton" type="submit">
						       		<span><img width="16" height="16" alt="" src="${resource(dir:'images/skin',file:'disk.png')}"/><g:message code="create" default="Create" /></span>
						    	</button>
						    	<span class="formcancel"> or&nbsp;&nbsp;<g:link action="list"><g:message code="cancel" default="cancel" /></g:link></span>
						    	
								</g:form>
							</div>
						</div>
						
					</div> <!-- #contentMain -->
					
				</div>
				<div id="rightcol">
					<div id="options">
						<div class="optGrpHdr"><h3 class="optHdr"><g:message code="default.rcol.current.opts" default="Current Options" /></h3></div>
						<dl class="optGrpMenu">
							<iris:sidebarItem controller="customer" action="list" icon="page_white_stack.png" titleCode="customer.rcol.title.list" msgCode="customer.rcol.msg.list" />
							<iris:sidebarItem controller="customer" action="newCompany" icon="building_add.png" titleCode="ui.opt.title.addcompany" msgCode="ui.opt.msg.addcompany" />
						</dl>
					</div>
				</div>
				<div class="clear"></div>
			</div>
		</div>
	</body>
</html>




