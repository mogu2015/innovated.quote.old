<%@ page import="com.innovated.iris.domain.Entity" %>
<%@ page import="com.innovated.iris.domain.Company" %>
<%@ page import="com.innovated.iris.domain.Customer" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="prod">
        <title><g:message code="customer.company.create" default="Create Company" /></title>
    </head>
	<body>
		<div class="twocol">
			<div id="content">
				<div id="leftcol">
					<div id="contentMain">
						<p class="bread">
							<a class="home" href="${createLinkTo(dir: '')}"><g:message code="home" default="Home" /></a>
							<span class="breadArrow">&nbsp;</span>
							<g:link action="list"><g:message code="customer.list" default="Customer List" /></g:link>
							<span class="breadArrow">&nbsp;</span>
							<g:message code="customer.company.create" default="Create Company" />
						</p>
						
						<g:if test="${flash.message}">
						<div class="flashMsg">
							<p><g:message code="${flash.message}" args="${flash.args}" default="${flash.defaultMessage}" /></p>
						</div>
						</g:if>
						
						<g:set var="company" value="${(Company) customerInstance.entity}" />
			            
			            <g:if test="${customerInstance.hasErrors() || company.hasErrors()}">
						<div class="errors">
							<ul>
								<g:if test="${customerInstance.hasErrors()}">
								<g:eachError bean="${customerInstance}"><li><g:message error="${it}"/></li>
								</g:eachError>
								</g:if>
								<g:if test="${company.hasErrors()}">
								<g:eachError bean="${company}" >
									<g:if test="${it.field != 'name'}">
									<li><g:message error="${it}"/></li>
									</g:if>
								</g:eachError>
								</g:if>
							</ul>
						</div>
						</g:if>
						
			            <h1 class="contentH1"><g:message code="customer.create" default="Create Company" /></h1>
						<div class="formBg">
							<div class="formWrap">
								<g:form action="saveCompany" method="post" >
								
								<g:render template="typeAndStatus" bean="${customerInstance}" />
								
								<h3 class="topPad">Company Details</h3>
								<p>Please enter the details for this new Company:</p>
								<div class="formSet">
									<div class="clearfix mandatory ${hasErrors(bean: company, field: 'tradingName', 'error')}">
		                                <label for="entity.tradingName"><g:message code="company.tradingName" default="Trading Name" /></label>
		                                <g:textField class="long" name="entity.tradingName" value="${fieldValue(bean: company, field: 'tradingName')}" />
		                            </div>
		                            <div class="clearfix ${hasErrors(bean: company, field: 'acn', 'error')}">
		                                <label for="entity.acn"><g:message code="company.acn" default="ACN" /></label>
		                                <g:textField class="short" name="entity.acn" value="${fieldValue(bean: company, field: 'acn')}" />
		                            </div>
		                            <div class="clearfix ${hasErrors(bean: company, field: 'abn', 'error')}">
		                                <label for="entity.abn"><g:message code="company.abn" default="ABN" /></label>
		                                <g:textField class="short" name="entity.abn" value="${fieldValue(bean: company, field: 'abn')}" />
		                            </div>
		                            <div class="clearfix ${hasErrors(bean: company, field: 'canClaimGst', 'error')}">
		                                <label for="entity.canClaimGst"><g:message code="company.canClaimGst" default="Can Claim GST?" /></label>
		                                <g:checkBox class="chk" name="entity.canClaimGst" value="${company?.canClaimGst}" />
		                            </div>
		                            <div class="clearfix ${hasErrors(bean: company, field: 'parent', 'error')}">
		                                <label for="entity.parent.id"><g:message code="company.parent" default="Parent Company" /></label>
		                                <g:select class="short" name="entity.parent.id" optionKey="id" from="${Company.list()}" value="${company?.id}" noSelection="['-1':'']"></g:select>
		                            </div>
		                        </div>
		                        
		                        <g:render template="contactDetails" bean="${customerInstance}" />
		                        
		                        <h3 class="topPad">Employer Details</h3>
								<div class="formSet">
									<div class="clearfix mandatory ${hasErrors(bean: company, field: 'employerCode', 'error')}">
		                                <label for="entity.employerCode"><g:message code="company.employerCode" default="Employer Code" /></label>
		                                <g:textField class="short" name="entity.employerCode" value="${fieldValue(bean: company, field: 'employerCode')}" />
		                            </div>
		                            <div class="clearfix mandatory ${hasErrors(bean: company, field: 'defaultPayFreq', 'error')}">
		                                <label for="entity.defaultPayFreq"><g:message code="company.defaultPayFreq" default="Payroll Frequency" /></label>
		                                <g:select class="short" name="entity.defaultPayFreq" optionKey="${{it.value?.name()}}" optionValue="value" from="${com.innovated.iris.util.PayrollFrequency.lookup}" value="${company?.defaultPayFreq?.name()}"></g:select>
		                            </div>
		                        </div>
		                        
		                        <h3 class="topPad">FBT Preferences</h3>
								<p>Select this company's preferences for FBT modules.</p>
								<div class="formSet">
									<div class="clearfix ${hasErrors(bean: company, field: 'allowsFbtEcm', 'error')}">
		                                <label for="entity.allowsFbtEcm"><g:message code="company.allowsFbtEcm" default="ECM Preferred?" /></label>
		                                <g:checkBox class="chk" name="entity.allowsFbtEcm" value="${company?.allowsFbtEcm}" />
		                            </div>
		                            <div class="clearfix ${hasErrors(bean: company, field: 'allowedFbtCalcMethods', 'error')}">
				                        <label><g:message code="company.allowedFbtCalcMethods" default="TV Calc Methods" /></label>
				                        <g:if test="${taxableValueMap.size() > 0}">
				                        <table>
				                        	<thead>
				                        	<tr>
				                        		<th>Allow</th>
				                        		<th>Type</th>
				                        		<th>Default</th>
				                        	</tr>
				                        	</thead>
				                        	<tbody>
				                        	<g:each status="i" var="tv" in="${taxableValueMap}">
						                    <tr>
				                        		<td class="center"><g:checkBox name="ALLOWED_TV_${tv.key.key}" value="${tv.value}" /></td>
				                        		<td>${tv.key.value.encodeAsHTML()}</td>
				                        		<g:if test="${tv.key.value == company.allowedFbtCalcMethods.get(0)}">
				                        		<td class="center"><g:radio id="tv_default_${tv.key.key}" name="tv_default" value="${tv.key.key}" checked="checked"/></td>
				                        		</g:if>
				                        		<g:else>
				                        		<td class="center"><g:radio id="tv_default_${tv.key.key}" name="tv_default" value="${tv.key.key}"/></td>
				                        		</g:else>
				                        	</tr>
						                    </g:each>
						                    </tbody>
				                        </table>
				                        </g:if>
				                    </div>
				                    <div class="clearfix ${hasErrors(bean:company, field:'concessions', 'error')}">
		                                <label for="entity.concessions.current"><g:message code="company.concessions.current" default="FBT Concession" /></label>
		                                <g:select class="long" name="entity.concessions.current" optionKey="key" optionValue="value" from="${concessionMap}" value="${currentConcession}"></g:select>
		                            </div>
		                            <div class="clearfix ${hasErrors(bean:company, field:'employerShareRate', 'error')} ${hasErrors(bean:customerInstance, field:'entity.employerShareRate', 'error')}">
		                                <label for="entity.employerShareRate"><g:message code="company.employerShareRate" default="Employer Share %" /></label>
		                                <g:textField class="numeric" name="entity.employerShareRate" value="${formatNumber(number:company?.employerShareRate, type:'number', minFractionDigits:0, maxFractionDigits:0)}" />&nbsp;%
		                            </div>
		                            <div class="clearfix ${hasErrors(bean:company, field:'employerShareFee', 'error')} ${hasErrors(bean:customerInstance, field:'entity.employerShareFee', 'error')}">
		                                <label for="entity.employerShareFee"><g:message code="company.employerShareFee" default="Admin Fee" /></label>
		                                <g:textField class="numeric" name="entity.employerShareFee" value="${formatNumber(number:company?.employerShareFee, type:'number', minFractionDigits:2, maxFractionDigits:2)}" />
		                            </div>
		                        </div>
								
								<button id="btnCreate" name="btnCreate" class="greybutton" type="submit">
						       		<span><img width="16" height="16" alt="" src="${resource(dir:'images/skin',file:'disk.png')}"/><g:message code="create" default="Create" /></span>
						    	</button>
						    	<span class="formcancel"> or&nbsp;&nbsp;<g:link action="list"><g:message code="cancel" default="cancel" /></g:link></span>
						    	
								</g:form>
							</div>
						</div>
						
					</div> <!-- #contentMain -->
					
				</div>
				<div id="rightcol">
					<div id="options">
						<div class="optGrpHdr"><h3 class="optHdr"><g:message code="default.rcol.current.opts" default="Current Options" /></h3></div>
						<dl class="optGrpMenu">
							<iris:sidebarItem controller="customer" action="list" icon="page_white_stack.png" titleCode="customer.rcol.title.list" msgCode="customer.rcol.msg.list" />
							<iris:sidebarItem controller="customer" action="newIndividual" icon="user_add.png" titleCode="ui.opt.title.addindiv" msgCode="ui.opt.msg.addindiv" />
						</dl>
					</div>
				</div>
				<div class="clear"></div>
			</div>
		</div>
	</body>
</html>




