<%@ page import="com.innovated.iris.domain.*" %>
<html>
    <head>
        <meta name="layout" content="prod">
        <title>Edit Customer</title>
        <g:javascript library="jquery/jquery.dataTables.min" />
        <g:javascript library="jquery/dataTables.fnSetFilteringDelay" />
        
        <script type="text/javascript">
        /*function showPanel(p) {
        	$("#contentMain").hide();
			$("#" + p.rel + "Panel").show();
        }
        function hidePanel(p) {
        	$("#" + p.rel + "Panel").hide();
			$("#contentMain").show();
			$(".messages").html("");	// clear any (ajax) flash messages
        }*/
        
        $(document).ready(function() {
			$(".link").click(function() { showPanel(this); });
			$(".linkCancel").click(function() { hidePanel(this); });
			$(".mapLink").click(function(e) {
				var address = $(this)/*.prev()*/.html();
				var gmaps = "http://maps.google.com.au/?";
        		window.open(gmaps + $.param({ q:address, t:"m", gl:"AU", z:16 }));
        		return false;
			});
        });
        </script>
    </head>
	<body>
		<div class="twocol">
			<div id="content">
				<div id="leftcol">
					
					<div id="contentMain">
						<p class="bread">
							<g:link controller="customer" action="list"><g:message code="ui.menu.label.customers" default="Customers" /></g:link>
							<span class="breadArrow">&nbsp;</span>
							<g:message code="ui.menu.label.customer.edit" default="Edit Customer" />
						</p>
						<div class="messages">
							<g:if test="${flash.message}">
							<div class="flashMsg">
								<g:if test="${flash.classes}"><p class="${flash.classes}"></g:if><g:else><p></g:else><g:message code="${flash.message}" args="${flash.args}" default="${flash.defaultMessage}" /></p>
							</div>
							</g:if>
				            <g:hasErrors bean="${customerInstance}">
							<div class="errors">
								<ul>
								<g:eachError bean="${customerInstance}">
									<li><g:message error="${it}"/></li>
								</g:eachError>
								</ul>
							</div>
			            	</g:hasErrors>
						</div>
						<h1 class="contentH1"><g:message code="ui.menu.label.customer.edit" default="Edit Customer" />: ${customerInstance?.entity?.name}</h1>
						<div class="formBg">
							<div class="formWrap" style="display:none;">
								<g:form controller="customer" action="update" method="post" >
									<g:hiddenField name="id" value="${customerInstance?.id}" />
                					<g:hiddenField name="version" value="${customerInstance?.version}" />
									
									<g:render template="/templates/ui/customer/tabs" bean="${customerInstance}" />
									 
									<button id="btnLogin" name="btnLogin" class="greybutton" type="submit" tabindex="26">
							       		<span><img width="16" height="16" alt="" src="${resource(dir:'images/skin',file:'disk.png')}"/>Update</span>
							    	</button>
							    	<span class="formcancel"> or&nbsp;&nbsp;<g:link controller="customer" action="list" tabindex="27">cancel</g:link></span>
							    	
								</g:form>
							</div>
							<div id="spinner" class="spinner"><img src="${resource(dir:'images',file:'spinner.gif')}" alt="Spinner" /> Loading...</div>
						</div>
					</div> <!-- #contentMain -->
					
				</div>
				<div id="rightcol">
					<div id="options">
						<g:if test="${customerInstance.entity.type == 'Company'}">
						<div class="optGrpHdr"><h3 class="optHdr">Current Options</h3></div>
						<dl class="optGrpMenu">
							<iris:sidebarItem controller="employment" action="employees" id="${customerInstance.entity.id}" icon="group.png" titleCode="ui.opt.title.listemployees" msgCode="ui.opt.msg.listemployees" />
							<iris:sidebarItem controller="customer" action="list" icon="page_white_stack.png" titleCode="ui.opt.title.listquotes" msgCode="ui.opt.msg.listquotes" />
							<iris:sidebarItem controller="customer" action="list" icon="page_white_stack.png" titleCode="ui.opt.title.listquotes" msgCode="ui.opt.msg.listquotes" />
						</dl>
						</g:if>
						<g:else>
						<div class="optGrpHdr"><h3 class="optHdr">Current Options</h3></div>
						<dl class="optGrpMenu">
							<iris:sidebarItem controller="employment" action="employers" id="${customerInstance.entity.id}" icon="building.png" titleCode="ui.opt.title.listemployers" msgCode="ui.opt.msg.listemployers" />
							<iris:sidebarItem controller="customer" action="list" icon="page_white_stack.png" titleCode="ui.opt.title.listquotes" msgCode="ui.opt.msg.listquotes" />
							<iris:sidebarItem controller="customer" action="list" icon="page_white_stack.png" titleCode="ui.opt.title.listquotes" msgCode="ui.opt.msg.listquotes" />
						</dl>
						</g:else>
					</div>
				</div>
				<div class="clear"></div>
			</div>
		</div>
	</body>
</html>

