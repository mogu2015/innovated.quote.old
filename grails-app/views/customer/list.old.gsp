
<%@ page import="com.innovated.iris.domain.Customer" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <title><g:message code="customer.list" default="Customer List" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLinkTo(dir: '')}"><g:message code="home" default="Home" /></a></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="customer.new" default="New Customer" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="customer.list" default="Customer List" /></h1>
            <g:if test="${flash.message}">
            <div class="message"><g:message code="${flash.message}" args="${flash.args}" default="${flash.defaultMessage}" /></div>
            </g:if>
            <div class="list">
                <table>
                    <thead>
                        <tr>
                        
                   	    <g:sortableColumn property="id" title="Id" titleKey="customer.id" />
                        
                   	    <g:sortableColumn property="code" title="Code" titleKey="customer.code" />
                        
                   	    <g:sortableColumn property="supplierLinkCode" title="Supplier Link Code" titleKey="customer.supplierLinkCode" />
                        
                   	    <th><g:message code="customer.entity" default="Entity" /></th>
                   	    
                   	    <th><g:message code="customer.type" default="Type" /></th>
                   	    
                   	    <th><g:message code="customer.status" default="Status" /></th>
                   	    
                        </tr>
                    </thead>
                    <tbody>
                    <g:each in="${customerInstanceList}" status="i" var="customerInstance">
                        <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
                        
                            <td><g:link action="show" id="${customerInstance.id}">${fieldValue(bean: customerInstance, field: "id")}</g:link></td>
                        
                            <td>${fieldValue(bean: customerInstance, field: "code")}</td>
                        
                            <td>${fieldValue(bean: customerInstance, field: "supplierLinkCode")}</td>
                        
                            <td>${fieldValue(bean: customerInstance, field: "entity")}</td>
                        
                            <td>${fieldValue(bean: customerInstance, field: "type")}</td>
                        
                            <td>${fieldValue(bean: customerInstance, field: "status")}</td>
                        
                        </tr>
                    </g:each>
                    </tbody>
                </table>
            </div>
            <div class="paginateButtons">
                <g:paginate total="${customerInstanceTotal}" />
            </div>
        </div>
    </body>
</html>
