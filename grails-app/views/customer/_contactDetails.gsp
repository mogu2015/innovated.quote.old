<%@ page import="com.innovated.iris.domain.enums.AddressType" %>
<%@ page import="com.innovated.iris.domain.enums.AustralianState" %>

<g:setProvider library="jquery"/>

<script type="text/javascript">
$(document).ready(function() {
	$('#add_address').click(function() { 
		//alert("address clicked");
		$('#addrWindow').modal({
			autoResize:true,
			onOpen: function(dialog) {
				dialog.overlay.fadeIn('fast', function() {
					dialog.container.slideDown('fast', function() {
						dialog.data.fadeIn('fast', function() {
							dialog.container.find('input').first().focus();
						});
					});
				});
			},
			onClose: function(dialog) {
				dialog.data.fadeOut('fast', function() {
					dialog.container.slideUp('fast', function() {
						dialog.overlay.fadeOut('fast', function() {
							$.modal.close(); // must call this!
						});
					});
				});
			}
		});
		return false;
	});

	$("#btnCreate").click(function(e) {
		e.preventDefault();
		var params = {};
		var $fields = $(".simplemodal-container .formSet input:text, .simplemodal-container .formSet select");
		$.each($fields, function(i,v){
			var name = $(v).attr("name");
			var value = $(v).val();
			params[name] = value;
		});
		$.post("${createLink(action:'validateAddress')}", params, function(data) {
			if (data.success) {
				// add the address to the underlying customer, close the form
				$.modal.close();
			} else {
				// highlight any errors
				//$("#addrMsg").html(data.msg);
			}
		});
	});
});
</script>
<h3 class="topPad">Contact Details</h3>
<div class="formSet">
	<a id="add_address" href="#addAddress">Add Address</a>
</div>

<!-- "Add Address" window/form -->
<div id="addrWindow" style="display:none;">
	<span class="window-title"></span>
	
	<h3>Add a new address</h3>
	<div id="addrMsg"></div>
	<div class="formSet">	
		<div class="clearfix mandatory">
			<label for="street"><g:message code="address.street" default="Street" /></label>
			<g:textField id="street" class="short" name="street" value="" />
		</div>
		<div class="clearfix mandatory">
			<label for="suburb"><g:message code="address.suburb" default="Suburb" /></label>
			<g:textField id="suburb" class="short" name="suburb" value="" />
		</div>
		<div class="clearfix mandatory">
			<label for="state.id"><g:message code="address.state" default="State" /></label>
			<g:select optionKey="id" optionValue="abbreviation" from="${AustralianState.listOrderByAbbreviation()}" class="shortshort" name="state.id" value="" ></g:select>
		</div>
		<div class="clearfix mandatory">
			<label for="postcode"><g:message code="address.postcode" default="Postcode" /></label>
			<g:textField id="postcode" class="shortshort" name="postcode" value="" />
		</div>
		<div class="clearfix mandatory">
			<label for="addressType.id"><g:message code="address.addressType" default="Type" /></label>
			<g:select optionKey="id" from="${AddressType.listOrderByName()}" class="short" name="addressType.id" value="" ></g:select>
		</div>
	</div>
	
	<button id="btnCreate" name="btnCreate" class="greybutton" type="submit">
		<span><img width="16" height="16" alt="" src="${resource(dir:'images/skin',file:'disk.png')}"/><g:message code="create" default="Create" /></span>
	</button>
	<span class="formcancel"> or&nbsp;&nbsp;<a class="simplemodal-close" href="#closeAddress"><g:message code="cancel" default="cancel" /></a></span>
	
</div>

