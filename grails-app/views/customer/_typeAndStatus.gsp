
<h3 class="topPad">Customer Type & Status</h3>
								<div class="formSet">
		                            <div class="clearfix mandatory ${hasErrors(bean: customerInstance, field: 'type', 'error')}">
		                                <label for="type.id"><g:message code="customer.type" default="Type" /></label>
		                                <g:select class="short" name="type.id" from="${com.innovated.iris.domain.enums.CustomerType.list()}" optionKey="id" value="${customerInstance?.type?.id}"  />
									</div>
		                            <div class="clearfix mandatory ${hasErrors(bean: customerInstance, field: 'status', 'error')}">
		                                <label for="status.id"><g:message code="customer.status" default="Status" /></label>
		                                <g:select class="short" name="status.id" from="${com.innovated.iris.domain.enums.CustomerStatus.list()}" optionKey="id" value="${customerInstance?.status?.id}"  />
		                            </div>
		                        	<div class="clearfix mandatory ${hasErrors(bean: customerInstance, field: 'joined', 'error')}">
		                                <label for="joined"><g:message code="customer.joined" default="Joined" /></label>
		                                <g:textField class="short dateField" name="joined" value="${g.formatDate(format:'d/M/yyyy', date:customerInstance?.joined)}" />
		                            </div>
		                        </div>