<html>
	<head>
		<meta name='layout' content='prod.login'>
		<title>Password Reset</title>
		<script type="text/javascript">
			$(document).ready(function(){
				if ($.browser.msie())
					$("#email").focus();
				else
			  		$("#email").select();
			});
		</script>
	</head>
	
	<body>
		<noscript>
		    <div class="login_js_alert">For this application to work properly you'll need to <strong>enable JavaScript</strong>. <a href="http://www.google.com/support/websearch/bin/answer.py?hl=en&answer=23852">Find out how</a>.</div>
		</noscript>
		<div id="additional"></div>
		<div id="formWrapper">
			<div id="formCasing">
			<div id="formBody">
				<div style="display: block;">
					<div class="logoDiv" style="text-align: center;"><img id="irisLogo" src="${resource(dir:'images',file:'iris_logo.png')}" alt="IRIS" title="IRIS :: inNovated Leasing Australia" /></div>
					<h1>Password Reset</h1>
					<g:if test="${flash.message}">
					<div class="errors">
						<p class="error" style="padding: 0px 5px; text-align: center; margin: 0;">${flash.message}</p>
					</div>
					</g:if>
					<p>Please enter the email address associated with this account.</p>
					<div id="loginForm">
						<form action="resetMyPassword.aspx?forgot=true" method="post" name="loginForm">
							<dl>
								<dt><label for="email">Email</label></dt>
								<dd><input type="text" name="email" id="email" value="" class="input" style="width: 280px;" /></dd>
								<dd>
									<button id="btnLogin" name="btnLogin" class="greybutton" type="submit" tabindex="4">
							       		<span><img width="16" height="16" alt="" src="${resource(dir:'images/skin',file:'email_go.png')}"/>Reset</span>
							    	</button>
							    	<span class="formcancel"> or&nbsp;&nbsp;<g:link action="auth">Cancel</g:link></span>
								</dd>
							</dl>
							
						</form>
					</div>
				</div>
			</div>
			</div>
			<div id="formFooter"></div>
		</div>
	</body>

</html>
