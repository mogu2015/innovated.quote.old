<html>
	<head>
		<meta name='layout' content='prod.login'>
		<title>Please Login</title>
		<script type="text/javascript">
			$(document).ready(function(){
				if ($.browser.msie())
					$("#username").focus();
				else
			  		$("#username").select();
			});
		</script>
		<style type="text/css">
			input#username { width: 280px }
			input#password { width: 150px; margin-right: 5px }
		</style>
	</head>
	
	<body>
		<noscript>
		    <div class="login_js_alert">For this application to work properly you'll need to <strong>enable JavaScript</strong>. <a href="http://www.google.com/support/websearch/bin/answer.py?hl=en&answer=23852">Find out how</a>.</div>
		</noscript>
		<div id="additional"></div>
		<div id="formWrapper">
			<div id="formCasing">
			<div id="formBody">
				<div class="logoDiv" style="text-align: center;"><img id="irisLogo" src="${resource(dir:'images',file:'iris_logo.png')}" alt="IRIS" title="IRIS :: inNovated Leasing Australia" /></div>
				<h1>Please Login:</h1>
				<g:if test="${flash.message}">
				<div class="errors">
					<p class="error" style="padding: 0px 5px; text-align: center; margin: 0;">${flash.message}</p>
				</div>
				</g:if>
				
				<div id="loginForm">
					<form action='${postUrl}' method='POST' id='loginForm'>
					  	<dl>
							<dt><label for="username">Username</label></dt>
							<dd><input type="text" name="j_username" id="username" value="${request.remoteUser}" class="input" tabindex="1" /></dd>
							<dt><label for="password">Password</label></dt>
							<dd><input type="password" name="j_password" id="password" value="" tabindex="2" class="input" /><!-- <span>&nbsp;&nbsp;<g:link action="forgotPassword">I forgot my password</g:link></span> --></dd>
							<dd><input type="checkbox" name="_spring_security_remember_me" id="remember_me" tabindex="3" value="true" class="checkbox" <g:if test='${hasCookie}'>checked='checked'</g:if> /><label for="remember_me">&nbsp;&nbsp;Remember me on this computer</label></dd>
							<dd>
								<button id="btnLogin" name="btnLogin" class="greybutton" type="submit" tabindex="4">
					       			<span><img width="16" height="16" alt="" src="${resource(dir:'images/skin',file:'application_go.png')}"/>Login</span>
					    		</button>
					    		<span class="formcancel"> or&nbsp;&nbsp;<g:link controller="user" action="register">Register...</g:link></span>
					    		<div class="clear"></div>
					    	</dd>
						</dl>
				  	</form>
				</div>
			</div>
			</div>
			<div id="formFooter"></div>
		</div>
	
	
	</body>

</html>
