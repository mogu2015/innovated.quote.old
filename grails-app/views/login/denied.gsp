<meta name='layout' content='main' />
<title>Denied</title>

<div class='body'>
	<span class="menuButton"><a class="home" href="${createLinkTo(dir: '')}"><g:message code="home" default="Home" /></a></span>
	<div class='errors'>Sorry, you're not authorized to view this page.</div>
</div>
