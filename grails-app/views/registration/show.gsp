
<%@ page import="com.innovated.iris.domain.Registration" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <title><g:message code="registration.show" default="Show Registration" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLinkTo(dir: '')}"><g:message code="home" default="Home" /></a></span>
            <span class="menuButton"><g:link class="list" action="list"><g:message code="registration.list" default="Registration List" /></g:link></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="registration.new" default="New Registration" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="registration.show" default="Show Registration" /></h1>
            <g:if test="${flash.message}">
            <div class="message"><g:message code="${flash.message}" args="${flash.args}" default="${flash.defaultMessage}" /></div>
            </g:if>
            <g:form>
                <g:hiddenField name="id" value="${registrationInstance?.id}" />
                <div class="dialog">
                    <table>
                        <tbody>
                        
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="registration.id" default="Id" />:</td>
                                
                                <td valign="top" class="value">${fieldValue(bean: registrationInstance, field: "id")}</td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="registration.vehicle" default="Vehicle" />:</td>
                                
                                <td valign="top" class="value"><g:link controller="vehicle" action="show" id="${registrationInstance?.vehicle?.id}">${registrationInstance?.vehicle?.encodeAsHTML()}</g:link></td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="registration.plate" default="Plate" />:</td>
                                
                                <td valign="top" class="value">${fieldValue(bean: registrationInstance, field: "plate")}</td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="registration.expiryDate" default="Expiry Date" />:</td>
                                
                                <td valign="top" class="value"><g:formatDate date="${registrationInstance?.expiryDate}" /></td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="registration.usage" default="Usage" />:</td>
                                
                                <td valign="top" class="value"><g:link controller="registrationUsageType" action="show" id="${registrationInstance?.usage?.id}">${registrationInstance?.usage?.encodeAsHTML()}</g:link></td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="registration.state" default="State" />:</td>
                                
                                <td valign="top" class="value"><g:link controller="australianState" action="show" id="${registrationInstance?.state?.id}">${registrationInstance?.state?.encodeAsHTML()}</g:link></td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="registration.paymentDate" default="Payment Date" />:</td>
                                
                                <td valign="top" class="value"><g:formatDate date="${registrationInstance?.paymentDate}" /></td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="registration.paymentTotal" default="Payment Total" />:</td>
                                
                                <td valign="top" class="value"><g:formatNumber number="${registrationInstance?.paymentTotal}" /></td>
                                
                            </tr>
                            
                        </tbody>
                    </table>
                </div>
                <div class="buttons">
                    <span class="button"><g:actionSubmit class="edit" action="edit" value="${message(code: 'edit', 'default': 'Edit')}" /></span>
                    <span class="button"><g:actionSubmit class="delete" action="delete" value="${message(code: 'delete', 'default': 'Delete')}" onclick="return confirm('${message(code: 'delete.confirm', 'default': 'Are you sure?')}');" /></span>
                </div>
            </g:form>
        </div>
    </body>
</html>
