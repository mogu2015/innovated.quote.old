
<%@ page import="com.innovated.iris.domain.Registration" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <title><g:message code="registration.list" default="Registration List" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLinkTo(dir: '')}"><g:message code="home" default="Home" /></a></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="registration.new" default="New Registration" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="registration.list" default="Registration List" /></h1>
            <g:if test="${flash.message}">
            <div class="message"><g:message code="${flash.message}" args="${flash.args}" default="${flash.defaultMessage}" /></div>
            </g:if>
            <div class="list">
                <table>
                    <thead>
                        <tr>
                        
                   	    <g:sortableColumn property="id" title="Id" titleKey="registration.id" />
                        
                   	    <th><g:message code="registration.vehicle" default="Vehicle" /></th>
                   	    
                   	    <g:sortableColumn property="plate" title="Plate" titleKey="registration.plate" />
                        
                   	    <g:sortableColumn property="expiryDate" title="Expiry Date" titleKey="registration.expiryDate" />
                        
                   	    <th><g:message code="registration.usage" default="Usage" /></th>
                   	    
                   	    <th><g:message code="registration.state" default="State" /></th>
                   	    
                        </tr>
                    </thead>
                    <tbody>
                    <g:each in="${registrationInstanceList}" status="i" var="registrationInstance">
                        <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
                        
                            <td><g:link action="show" id="${registrationInstance.id}">${fieldValue(bean: registrationInstance, field: "id")}</g:link></td>
                        
                            <td>${fieldValue(bean: registrationInstance, field: "vehicle")}</td>
                        
                            <td>${fieldValue(bean: registrationInstance, field: "plate")}</td>
                        
                            <td><g:formatDate date="${registrationInstance.expiryDate}" /></td>
                        
                            <td>${fieldValue(bean: registrationInstance, field: "usage")}</td>
                        
                            <td>${fieldValue(bean: registrationInstance, field: "state")}</td>
                        
                        </tr>
                    </g:each>
                    </tbody>
                </table>
            </div>
            <div class="paginateButtons">
                <g:paginate total="${registrationInstanceTotal}" />
            </div>
        </div>
    </body>
</html>
