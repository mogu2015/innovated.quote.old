
<%@ page import="com.innovated.iris.domain.Registration" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <title><g:message code="registration.edit" default="Edit Registration" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLinkTo(dir: '')}"><g:message code="home" default="Home" /></a></span>
            <span class="menuButton"><g:link class="list" action="list"><g:message code="registration.list" default="Registration List" /></g:link></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="registration.new" default="New Registration" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="registration.edit" default="Edit Registration" /></h1>
            <g:if test="${flash.message}">
            	<div class="message"><g:message code="${flash.message}" args="${flash.args}" default="${flash.defaultMessage}" /></div>
            </g:if>
            <g:hasErrors bean="${registrationInstance}">
				<div class="errors">
					<g:renderErrors bean="${registrationInstance}" as="list" />
				</div>
            </g:hasErrors>
            <g:form method="post" >
                <g:hiddenField name="id" value="${registrationInstance?.id}" />
                <g:hiddenField name="version" value="${registrationInstance?.version}" />
                <div class="dialog">
                    <fieldset>
                        <legend><g:message code="registration.edit.legend" default="Update Registration Details"/></legend>
                        
                        <div class="prop mandatory ${hasErrors(bean: registrationInstance, field: 'vehicle', 'error')}">
                            <label for="vehicle">
                                <g:message code="registration.vehicle" default="Vehicle" />
                                <span class="indicator">*</span>
                            </label>
                            <g:select name="vehicle.id" from="${com.innovated.iris.domain.Vehicle.list()}" optionKey="id" value="${registrationInstance?.vehicle?.id}"  />

                        </div>
                        
                        <div class="prop mandatory ${hasErrors(bean: registrationInstance, field: 'plate', 'error')}">
                            <label for="plate">
                                <g:message code="registration.plate" default="Plate" />
                                <span class="indicator">*</span>
                            </label>
                            <g:textField name="plate" maxlength="10" value="${fieldValue(bean: registrationInstance, field: 'plate')}" />

                        </div>
                        
                        <div class="prop mandatory ${hasErrors(bean: registrationInstance, field: 'expiryDate', 'error')}">
                            <label for="expiryDate">
                                <g:message code="registration.expiryDate" default="Expiry Date" />
                                <span class="indicator">*</span>
                            </label>
                            <g:datePicker name="expiryDate" value="${registrationInstance?.expiryDate}"  />

                        </div>
                        
                        <div class="prop mandatory ${hasErrors(bean: registrationInstance, field: 'usage', 'error')}">
                            <label for="usage">
                                <g:message code="registration.usage" default="Usage" />
                                <span class="indicator">*</span>
                            </label>
                            <g:select name="usage.id" from="${com.innovated.iris.domain.enums.RegistrationUsageType.list()}" optionKey="id" value="${registrationInstance?.usage?.id}"  />

                        </div>
                        
                        <div class="prop mandatory ${hasErrors(bean: registrationInstance, field: 'state', 'error')}">
                            <label for="state">
                                <g:message code="registration.state" default="State" />
                                <span class="indicator">*</span>
                            </label>
                            <g:select name="state.id" from="${com.innovated.iris.domain.enums.AustralianState.list()}" optionKey="id" value="${registrationInstance?.state?.id}"  />

                        </div>
                        
                        <div class="prop ${hasErrors(bean: registrationInstance, field: 'paymentDate', 'error')}">
                            <label for="paymentDate">
                                <g:message code="registration.paymentDate" default="Payment Date" />
                                
                            </label>
                            <g:datePicker name="paymentDate" value="${registrationInstance?.paymentDate}" noSelection="['': '']" />

                        </div>
                        
                        <div class="prop ${hasErrors(bean: registrationInstance, field: 'paymentTotal', 'error')}">
                            <label for="paymentTotal">
                                <g:message code="registration.paymentTotal" default="Payment Total" />
                                
                            </label>
                            <g:textField name="paymentTotal" value="${fieldValue(bean: registrationInstance, field: 'paymentTotal')}" />

                        </div>
                        
                    </fieldset>
                </div>
                <div class="buttons">
                    <span class="button"><g:actionSubmit class="save" action="update" value="${message(code: 'update', 'default': 'Update')}" /></span>
                    <span class="button"><g:actionSubmit class="delete" action="delete" value="${message(code: 'delete', 'default': 'Delete')}" onclick="return confirm('${message(code: 'delete.confirm', 'default': 'Are you sure?')}');" /></span>
                </div>
            </g:form>
        </div>
    </body>
</html>

