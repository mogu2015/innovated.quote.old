
<%@ page import="com.innovated.iris.domain.lookup.FeesBrokerageParameter" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <title><g:message code="feesBrokerageParameter.list" default="FeesBrokerageParameter List" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLinkTo(dir: '')}"><g:message code="home" default="Home" /></a></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="feesBrokerageParameter.new" default="New FeesBrokerageParameter" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="feesBrokerageParameter.list" default="FeesBrokerageParameter List" /></h1>
            <g:if test="${flash.message}">
            <div class="message"><g:message code="${flash.message}" args="${flash.args}" default="${flash.defaultMessage}" /></div>
            </g:if>
            <div class="list">
                <table>
                    <thead>
                        <tr>
                        
                   	    <g:sortableColumn property="id" title="Id" titleKey="feesBrokerageParameter.id" />
                        
                   	    <g:sortableColumn property="dateFrom" title="Date From" titleKey="feesBrokerageParameter.dateFrom" />
                        
                   	    <g:sortableColumn property="customerCode" title="Customer Code" titleKey="feesBrokerageParameter.customerCode" />
                        
                   	    <g:sortableColumn property="brokeragePercent" title="Brokerage Percent" titleKey="feesBrokerageParameter.brokeragePercent" />
                        
                   	    <g:sortableColumn property="annualFeeFltMng" title="Annual Fee Flt Mng" titleKey="feesBrokerageParameter.annualFeeFltMng" />
                        
                   	    <g:sortableColumn property="annualFeeFbtRep" title="Annual Fee Fbt Rep" titleKey="feesBrokerageParameter.annualFeeFbtRep" />
                        
                        </tr>
                    </thead>
                    <tbody>
                    <g:each in="${feesBrokerageParameterInstanceList}" status="i" var="feesBrokerageParameterInstance">
                        <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
                        
                            <td><g:link action="show" id="${feesBrokerageParameterInstance.id}">${fieldValue(bean: feesBrokerageParameterInstance, field: "id")}</g:link></td>
                        
                            <td><g:formatDate date="${feesBrokerageParameterInstance.dateFrom}" /></td>
                        
                            <td>${fieldValue(bean: feesBrokerageParameterInstance, field: "customerCode")}</td>
                        
                            <td>${fieldValue(bean: feesBrokerageParameterInstance, field: "brokeragePercent")}</td>
                        
                            <td>${fieldValue(bean: feesBrokerageParameterInstance, field: "annualFeeFltMng")}</td>
                        
                            <td>${fieldValue(bean: feesBrokerageParameterInstance, field: "annualFeeFbtRep")}</td>
                        
                        </tr>
                    </g:each>
                    </tbody>
                </table>
            </div>
            <div class="paginateButtons">
                <g:paginate total="${feesBrokerageParameterInstanceTotal}" />
            </div>
        </div>
    </body>
</html>
