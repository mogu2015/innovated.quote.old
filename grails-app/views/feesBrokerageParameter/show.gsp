
<%@ page import="com.innovated.iris.domain.lookup.FeesBrokerageParameter" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <title><g:message code="feesBrokerageParameter.show" default="Show FeesBrokerageParameter" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLinkTo(dir: '')}"><g:message code="home" default="Home" /></a></span>
            <span class="menuButton"><g:link class="list" action="list"><g:message code="feesBrokerageParameter.list" default="FeesBrokerageParameter List" /></g:link></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="feesBrokerageParameter.new" default="New FeesBrokerageParameter" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="feesBrokerageParameter.show" default="Show FeesBrokerageParameter" /></h1>
            <g:if test="${flash.message}">
            <div class="message"><g:message code="${flash.message}" args="${flash.args}" default="${flash.defaultMessage}" /></div>
            </g:if>
            <g:form>
                <g:hiddenField name="id" value="${feesBrokerageParameterInstance?.id}" />
                <div class="dialog">
                    <table>
                        <tbody>
                        
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="feesBrokerageParameter.id" default="Id" />:</td>
                                
                                <td valign="top" class="value">${fieldValue(bean: feesBrokerageParameterInstance, field: "id")}</td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="feesBrokerageParameter.dateFrom" default="Date From" />:</td>
                                
                                <td valign="top" class="value"><g:formatDate date="${feesBrokerageParameterInstance?.dateFrom}" /></td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="feesBrokerageParameter.customerCode" default="Customer Code" />:</td>
                                
                                <td valign="top" class="value">${fieldValue(bean: feesBrokerageParameterInstance, field: "customerCode")}</td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="feesBrokerageParameter.brokeragePercent" default="Brokerage Percent" />:</td>
                                
                                <td valign="top" class="value">${fieldValue(bean: feesBrokerageParameterInstance, field: "brokeragePercent")}</td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="feesBrokerageParameter.annualFeeFltMng" default="Annual Fee Flt Mng" />:</td>
                                
                                <td valign="top" class="value">${fieldValue(bean: feesBrokerageParameterInstance, field: "annualFeeFltMng")}</td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="feesBrokerageParameter.annualFeeFbtRep" default="Annual Fee Fbt Rep" />:</td>
                                
                                <td valign="top" class="value">${fieldValue(bean: feesBrokerageParameterInstance, field: "annualFeeFbtRep")}</td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="feesBrokerageParameter.annualFeeAdmin" default="Annual Fee Admin" />:</td>
                                
                                <td valign="top" class="value">${fieldValue(bean: feesBrokerageParameterInstance, field: "annualFeeAdmin")}</td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="feesBrokerageParameter.annualFeeBankChg" default="Annual Fee Bank Chg" />:</td>
                                
                                <td valign="top" class="value">${fieldValue(bean: feesBrokerageParameterInstance, field: "annualFeeBankChg")}</td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="feesBrokerageParameter.lastUpdated" default="Last Updated" />:</td>
                                
                                <td valign="top" class="value"><g:formatDate date="${feesBrokerageParameterInstance?.lastUpdated}" /></td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="feesBrokerageParameter.dateCreated" default="Date Created" />:</td>
                                
                                <td valign="top" class="value"><g:formatDate date="${feesBrokerageParameterInstance?.dateCreated}" /></td>
                                
                            </tr>
                            
                        </tbody>
                    </table>
                </div>
                <div class="buttons">
                    <span class="button"><g:actionSubmit class="edit" action="edit" value="${message(code: 'edit', 'default': 'Edit')}" /></span>
                    <span class="button"><g:actionSubmit class="delete" action="delete" value="${message(code: 'delete', 'default': 'Delete')}" onclick="return confirm('${message(code: 'delete.confirm', 'default': 'Are you sure?')}');" /></span>
                </div>
            </g:form>
        </div>
    </body>
</html>
