
<%@ page import="com.innovated.iris.domain.lookup.FeesBrokerageParameter" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <title><g:message code="feesBrokerageParameter.create" default="Create FeesBrokerageParameter" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLinkTo(dir: '')}"><g:message code="home" default="Home" /></a></span>
            <span class="menuButton"><g:link class="list" action="list"><g:message code="feesBrokerageParameter.list" default="FeesBrokerageParameter List" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="feesBrokerageParameter.create" default="Create FeesBrokerageParameter" /></h1>
            <g:if test="${flash.message}">
            <div class="message"><g:message code="${flash.message}" args="${flash.args}" default="${flash.defaultMessage}" /></div>
            </g:if>
            <g:hasErrors bean="${feesBrokerageParameterInstance}">
            <div class="errors">
                <g:renderErrors bean="${feesBrokerageParameterInstance}" as="list" />
            </div>
            </g:hasErrors>
            <g:form action="save" method="post" >
                <div class="dialog">
                    <table>
                        <tbody>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="dateFrom"><g:message code="feesBrokerageParameter.dateFrom" default="Date From" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: feesBrokerageParameterInstance, field: 'dateFrom', 'errors')}">
                                    <g:datePicker name="dateFrom" value="${feesBrokerageParameterInstance?.dateFrom}"  />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="customerCode"><g:message code="feesBrokerageParameter.customerCode" default="Customer Code" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: feesBrokerageParameterInstance, field: 'customerCode', 'errors')}">
                                    <g:textField name="customerCode" value="${fieldValue(bean: feesBrokerageParameterInstance, field: 'customerCode')}" />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="brokeragePercent"><g:message code="feesBrokerageParameter.brokeragePercent" default="Brokerage Percent" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: feesBrokerageParameterInstance, field: 'brokeragePercent', 'errors')}">
                                    <g:select name="brokeragePercent" from="${0.0..5.0}" value="${feesBrokerageParameterInstance?.brokeragePercent}"  />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="annualFeeFltMng"><g:message code="feesBrokerageParameter.annualFeeFltMng" default="Annual Fee Flt Mng" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: feesBrokerageParameterInstance, field: 'annualFeeFltMng', 'errors')}">
                                    <g:select name="annualFeeFltMng" from="${0.0..100.0}" value="${feesBrokerageParameterInstance?.annualFeeFltMng}"  />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="annualFeeFbtRep"><g:message code="feesBrokerageParameter.annualFeeFbtRep" default="Annual Fee Fbt Rep" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: feesBrokerageParameterInstance, field: 'annualFeeFbtRep', 'errors')}">
                                    <g:select name="annualFeeFbtRep" from="${0.0..50.0}" value="${feesBrokerageParameterInstance?.annualFeeFbtRep}"  />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="annualFeeAdmin"><g:message code="feesBrokerageParameter.annualFeeAdmin" default="Annual Fee Admin" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: feesBrokerageParameterInstance, field: 'annualFeeAdmin', 'errors')}">
                                    <g:select name="annualFeeAdmin" from="${0.0..500.0}" value="${feesBrokerageParameterInstance?.annualFeeAdmin}"  />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="annualFeeBankChg"><g:message code="feesBrokerageParameter.annualFeeBankChg" default="Annual Fee Bank Chg" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: feesBrokerageParameterInstance, field: 'annualFeeBankChg', 'errors')}">
                                    <g:select name="annualFeeBankChg" from="${0.0..50.0}" value="${feesBrokerageParameterInstance?.annualFeeBankChg}"  />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="lastUpdated"><g:message code="feesBrokerageParameter.lastUpdated" default="Last Updated" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: feesBrokerageParameterInstance, field: 'lastUpdated', 'errors')}">
                                    <g:datePicker name="lastUpdated" value="${feesBrokerageParameterInstance?.lastUpdated}"  />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="dateCreated"><g:message code="feesBrokerageParameter.dateCreated" default="Date Created" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: feesBrokerageParameterInstance, field: 'dateCreated', 'errors')}">
                                    <g:datePicker name="dateCreated" value="${feesBrokerageParameterInstance?.dateCreated}"  />

                                </td>
                            </tr>
                        
                        </tbody>
                    </table>
                </div>
                <div class="buttons">
                    <span class="button"><g:submitButton name="create" class="save" value="${message(code: 'create', 'default': 'Create')}" /></span>
                </div>
            </g:form>
        </div>
    </body>
</html>
