
<%@ page import="com.innovated.iris.domain.lookup.HecsHelpRateParameter" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <title><g:message code="hecsHelpRateParameter.create" default="Create HecsHelpRateParameter" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLinkTo(dir: '')}"><g:message code="home" default="Home" /></a></span>
            <span class="menuButton"><g:link class="list" action="list"><g:message code="hecsHelpRateParameter.list" default="HecsHelpRateParameter List" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="hecsHelpRateParameter.create" default="Create HecsHelpRateParameter" /></h1>
            <g:if test="${flash.message}">
            <div class="message"><g:message code="${flash.message}" args="${flash.args}" default="${flash.defaultMessage}" /></div>
            </g:if>
            <g:hasErrors bean="${hecsHelpRateParameterInstance}">
            <div class="errors">
                <g:renderErrors bean="${hecsHelpRateParameterInstance}" as="list" />
            </div>
            </g:hasErrors>
            <g:form action="save" method="post" >
                <div class="dialog">
                    <table>
                        <tbody>
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="dateFrom"><g:message code="hecsHelpRateParameter.dateFrom" default="Date From" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: hecsHelpRateParameterInstance, field: 'dateFrom', 'errors')}">
                                    <g:datePicker name="dateFrom" value="${hecsHelpRateParameterInstance?.dateFrom}" precision="day" />
                                </td>
                            </tr>
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="lowerValue"><g:message code="hecsHelpRateParameter.lowerValue" default="Lower Value" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: hecsHelpRateParameterInstance, field: 'lowerValue', 'errors')}">
                                    <g:textField name="lowerValue" value="${fieldValue(bean: hecsHelpRateParameterInstance, field: 'lowerValue')}" />
                                </td>
                            </tr>
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="upperValue"><g:message code="hecsHelpRateParameter.upperValue" default="Upper Value" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: hecsHelpRateParameterInstance, field: 'upperValue', 'errors')}">
                                    <g:textField name="upperValue" value="${fieldValue(bean: hecsHelpRateParameterInstance, field: 'upperValue')}" />
                                </td>
                            </tr>
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="rate"><g:message code="hecsHelpRateParameter.rate" default="Rate" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: hecsHelpRateParameterInstance, field: 'rate', 'errors')}">
                                    <g:textField name="rate" value="${fieldValue(bean: hecsHelpRateParameterInstance, field: 'rate')}" />
                                </td>
                            </tr>
                        <!-- 
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="lastUpdated"><g:message code="hecsHelpRateParameter.lastUpdated" default="Last Updated" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: hecsHelpRateParameterInstance, field: 'lastUpdated', 'errors')}">
                                    <g:datePicker name="lastUpdated" value="${hecsHelpRateParameterInstance?.lastUpdated}"  />
                                </td>
                            </tr>
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="dateCreated"><g:message code="hecsHelpRateParameter.dateCreated" default="Date Created" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: hecsHelpRateParameterInstance, field: 'dateCreated', 'errors')}">
                                    <g:datePicker name="dateCreated" value="${hecsHelpRateParameterInstance?.dateCreated}"  />
                                </td>
                            </tr>
                         -->
                        </tbody>
                    </table>
                </div>
                <div class="buttons">
                    <span class="button"><g:submitButton name="create" class="save" value="${message(code: 'create', 'default': 'Create')}" /></span>
                </div>
            </g:form>
        </div>
    </body>
</html>
