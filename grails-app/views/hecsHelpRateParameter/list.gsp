
<%@ page import="com.innovated.iris.domain.lookup.HecsHelpRateParameter" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <title><g:message code="hecsHelpRateParameter.list" default="HecsHelpRateParameter List" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLinkTo(dir: '')}"><g:message code="home" default="Home" /></a></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="hecsHelpRateParameter.new" default="New HecsHelpRateParameter" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="hecsHelpRateParameter.list" default="HecsHelpRateParameter List" /></h1>
            <g:if test="${flash.message}">
            <div class="message"><g:message code="${flash.message}" args="${flash.args}" default="${flash.defaultMessage}" /></div>
            </g:if>
            <div class="list">
                <table>
                    <thead>
                        <tr>
	                   	    <g:sortableColumn property="id" title="ID" titleKey="hecsHelpRateParameter.id" />
	                   	    <g:sortableColumn property="dateFrom" title="Date From" titleKey="hecsHelpRateParameterInstance.dateFrom" />
	                   	    <g:sortableColumn class="numeric" property="lowerValue" title="Lower Value" titleKey="hecsHelpRateParameter.lowerValue" />
	                   	    <g:sortableColumn class="numeric" property="upperValue" title="Upper Value" titleKey="hecsHelpRateParameter.upperValue" />
	                   	    <g:sortableColumn class="numeric" property="rate" title="Rate" titleKey="hecsHelpRateParameter.rate" />
	                   	    <g:sortableColumn property="lastUpdated" title="Last Updated" titleKey="hecsHelpRateParameter.lastUpdated" />
	                   	    <g:sortableColumn property="dateCreated" title="Date Created" titleKey="hecsHelpRateParameter.dateCreated" />
                        </tr>
                    </thead>
                    <tbody>
                    <g:each in="${hecsHelpRateParameterInstanceList}" status="i" var="hecsHelpRateParameterInstance">
                        <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
                            <td><g:link action="show" id="${hecsHelpRateParameterInstance.id}">${fieldValue(bean: hecsHelpRateParameterInstance, field: "id")}</g:link></td>
                            <td><g:formatDate format="d MMM yyyy" date="${hecsHelpRateParameterInstance.dateFrom}" /></td>
                            <td class="numeric">${formatNumber(number:hecsHelpRateParameterInstance.lowerValue, type:'currency', minFractionDigits:0, maxFractionDigits:0)}</td>
                            <td class="numeric">${formatNumber(number:hecsHelpRateParameterInstance.upperValue, type:'currency', minFractionDigits:0, maxFractionDigits:0)}</td>
                            <td class="numeric">${formatNumber(number:hecsHelpRateParameterInstance.rate, type:'percent', minFractionDigits:1, maxFractionDigits:1)}</td>
                            <td><g:formatDate date="${hecsHelpRateParameterInstance.lastUpdated}" /></td>
                            <td><g:formatDate date="${hecsHelpRateParameterInstance.dateCreated}" /></td>
                        </tr>
                    </g:each>
                    </tbody>
                </table>
            </div>
            <div class="paginateButtons">
                <g:paginate total="${hecsHelpRateParameterInstanceTotal}" />
            </div>
        </div>
    </body>
</html>
