
<%@ page import="com.innovated.iris.domain.lookup.HecsHelpRateParameter" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <title><g:message code="hecsHelpRateParameter.show" default="Show HecsHelpRateParameter" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLinkTo(dir: '')}"><g:message code="home" default="Home" /></a></span>
            <span class="menuButton"><g:link class="list" action="list"><g:message code="hecsHelpRateParameter.list" default="HecsHelpRateParameter List" /></g:link></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="hecsHelpRateParameter.new" default="New HecsHelpRateParameter" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="hecsHelpRateParameter.show" default="Show HecsHelpRateParameter" /></h1>
            <g:if test="${flash.message}">
            <div class="message"><g:message code="${flash.message}" args="${flash.args}" default="${flash.defaultMessage}" /></div>
            </g:if>
            <g:form>
                <g:hiddenField name="id" value="${hecsHelpRateParameterInstance?.id}" />
                <div class="dialog">
                    <table>
                        <tbody>
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="hecsHelpRateParameter.id" default="ID" />:</td>
                                <td valign="top" class="value">${fieldValue(bean: hecsHelpRateParameterInstance, field: "id")}</td>
                            </tr>
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="hecsHelpRateParameter.dateFrom" default="Date From" />:</td>
                                <td valign="top" class="value"><g:formatDate format="d MMM yyyy" date="${hecsHelpRateParameterInstance?.dateFrom}" /></td>
                            </tr>
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="hecsHelpRateParameter.lowerValue" default="Lower Value" />:</td>
                                <td valign="top" class="value">${formatNumber(number:hecsHelpRateParameterInstance.lowerValue, type:'currency', minFractionDigits:0, maxFractionDigits:0)}</td>
                            </tr>
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="hecsHelpRateParameter.upperValue" default="Upper Value" />:</td>
                                <td valign="top" class="value">${formatNumber(number:hecsHelpRateParameterInstance.upperValue, type:'currency', minFractionDigits:0, maxFractionDigits:0)}</td>
                            </tr>
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="hecsHelpRateParameter.rate" default="Rate" />:</td>
                                <td valign="top" class="value">${formatNumber(number:hecsHelpRateParameterInstance.rate, type:'percent', minFractionDigits:1, maxFractionDigits:1)}</td>
                            </tr>
                            <!-- 
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="hecsHelpRateParameter.lastUpdated" default="Last Updated" />:</td>
                                <td valign="top" class="value"><g:formatDate date="${hecsHelpRateParameterInstance?.lastUpdated}" /></td>
                            </tr>
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="hecsHelpRateParameter.dateCreated" default="Date Created" />:</td>
                                <td valign="top" class="value"><g:formatDate date="${hecsHelpRateParameterInstance?.dateCreated}" /></td>
                            </tr>
                             -->
                        </tbody>
                    </table>
                </div>
                <div class="buttons">
                    <span class="button"><g:actionSubmit class="edit" action="edit" value="${message(code: 'edit', 'default': 'Edit')}" /></span>
                    <span class="button"><g:actionSubmit class="delete" action="delete" value="${message(code: 'delete', 'default': 'Delete')}" onclick="return confirm('${message(code: 'delete.confirm', 'default': 'Are you sure?')}');" /></span>
                </div>
            </g:form>
        </div>
    </body>
</html>
