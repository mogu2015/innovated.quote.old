
<%@ page import="com.innovated.iris.domain.importExport.VehicleDataImport" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <title><g:message code="vehicleDataImport.edit" default="Edit VehicleDataImport" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLinkTo(dir: '')}"><g:message code="home" default="Home" /></a></span>
            <span class="menuButton"><g:link class="list" action="list"><g:message code="vehicleDataImport.list" default="VehicleDataImport List" /></g:link></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="vehicleDataImport.new" default="New VehicleDataImport" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="vehicleDataImport.edit" default="Edit VehicleDataImport" /></h1>
            <g:if test="${flash.message}">
            <div class="message"><g:message code="${flash.message}" args="${flash.args}" default="${flash.defaultMessage}" /></div>
            </g:if>
            <g:hasErrors bean="${vehicleDataImportInstance}">
            <div class="errors">
                <g:renderErrors bean="${vehicleDataImportInstance}" as="list" />
            </div>
            </g:hasErrors>
            <g:form method="post" >
                <g:hiddenField name="id" value="${vehicleDataImportInstance?.id}" />
                <g:hiddenField name="version" value="${vehicleDataImportInstance?.version}" />
                <div class="dialog">
                    <table>
                        <tbody>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="publishedVehicleID"><g:message code="vehicleDataImport.publishedVehicleID" default="Published Vehicle ID" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: vehicleDataImportInstance, field: 'publishedVehicleID', 'errors')}">
                                    <g:textField name="publishedVehicleID" value="${fieldValue(bean: vehicleDataImportInstance, field: 'publishedVehicleID')}" />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="vehicleID"><g:message code="vehicleDataImport.vehicleID" default="Vehicle ID" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: vehicleDataImportInstance, field: 'vehicleID', 'errors')}">
                                    <g:textField name="vehicleID" value="${fieldValue(bean: vehicleDataImportInstance, field: 'vehicleID')}" />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="make"><g:message code="vehicleDataImport.make" default="Make" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: vehicleDataImportInstance, field: 'make', 'errors')}">
                                    <g:textField name="make" value="${fieldValue(bean: vehicleDataImportInstance, field: 'make')}" />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="modelReleaseVersion"><g:message code="vehicleDataImport.modelReleaseVersion" default="Model Release Version" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: vehicleDataImportInstance, field: 'modelReleaseVersion', 'errors')}">
                                    <g:textField name="modelReleaseVersion" value="${fieldValue(bean: vehicleDataImportInstance, field: 'modelReleaseVersion')}" />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="marketingModel"><g:message code="vehicleDataImport.marketingModel" default="Marketing Model" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: vehicleDataImportInstance, field: 'marketingModel', 'errors')}">
                                    <g:textField name="marketingModel" value="${fieldValue(bean: vehicleDataImportInstance, field: 'marketingModel')}" />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="variant"><g:message code="vehicleDataImport.variant" default="Variant" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: vehicleDataImportInstance, field: 'variant', 'errors')}">
                                    <g:textField name="variant" value="${fieldValue(bean: vehicleDataImportInstance, field: 'variant')}" />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="makeModelVariant"><g:message code="vehicleDataImport.makeModelVariant" default="Make Model Variant" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: vehicleDataImportInstance, field: 'makeModelVariant', 'errors')}">
                                    <g:textField name="makeModelVariant" value="${fieldValue(bean: vehicleDataImportInstance, field: 'makeModelVariant')}" />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="bodyStyle"><g:message code="vehicleDataImport.bodyStyle" default="Body Style" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: vehicleDataImportInstance, field: 'bodyStyle', 'errors')}">
                                    <g:textField name="bodyStyle" value="${fieldValue(bean: vehicleDataImportInstance, field: 'bodyStyle')}" />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="seatingCapacity"><g:message code="vehicleDataImport.seatingCapacity" default="Seating Capacity" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: vehicleDataImportInstance, field: 'seatingCapacity', 'errors')}">
                                    <g:textField name="seatingCapacity" value="${fieldValue(bean: vehicleDataImportInstance, field: 'seatingCapacity')}" />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="bodySeats"><g:message code="vehicleDataImport.bodySeats" default="Body Seats" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: vehicleDataImportInstance, field: 'bodySeats', 'errors')}">
                                    <g:textField name="bodySeats" value="${fieldValue(bean: vehicleDataImportInstance, field: 'bodySeats')}" />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="engineDisplacement"><g:message code="vehicleDataImport.engineDisplacement" default="Engine Displacement" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: vehicleDataImportInstance, field: 'engineDisplacement', 'errors')}">
                                    <g:textField name="engineDisplacement" value="${fieldValue(bean: vehicleDataImportInstance, field: 'engineDisplacement')}" />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="engineConfiguration"><g:message code="vehicleDataImport.engineConfiguration" default="Engine Configuration" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: vehicleDataImportInstance, field: 'engineConfiguration', 'errors')}">
                                    <g:textField name="engineConfiguration" value="${fieldValue(bean: vehicleDataImportInstance, field: 'engineConfiguration')}" />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="induction"><g:message code="vehicleDataImport.induction" default="Induction" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: vehicleDataImportInstance, field: 'induction', 'errors')}">
                                    <g:textField name="induction" value="${fieldValue(bean: vehicleDataImportInstance, field: 'induction')}" />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="engine"><g:message code="vehicleDataImport.engine" default="Engine" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: vehicleDataImportInstance, field: 'engine', 'errors')}">
                                    <g:textField name="engine" value="${fieldValue(bean: vehicleDataImportInstance, field: 'engine')}" />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="transmissionType"><g:message code="vehicleDataImport.transmissionType" default="Transmission Type" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: vehicleDataImportInstance, field: 'transmissionType', 'errors')}">
                                    <g:textField name="transmissionType" value="${fieldValue(bean: vehicleDataImportInstance, field: 'transmissionType')}" />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="fwdGearsNo"><g:message code="vehicleDataImport.fwdGearsNo" default="Fwd Gears No" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: vehicleDataImportInstance, field: 'fwdGearsNo', 'errors')}">
                                    <g:textField name="fwdGearsNo" value="${fieldValue(bean: vehicleDataImportInstance, field: 'fwdGearsNo')}" />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="trans"><g:message code="vehicleDataImport.trans" default="Trans" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: vehicleDataImportInstance, field: 'trans', 'errors')}">
                                    <g:textField name="trans" value="${fieldValue(bean: vehicleDataImportInstance, field: 'trans')}" />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="fuelType"><g:message code="vehicleDataImport.fuelType" default="Fuel Type" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: vehicleDataImportInstance, field: 'fuelType', 'errors')}">
                                    <g:textField name="fuelType" value="${fieldValue(bean: vehicleDataImportInstance, field: 'fuelType')}" />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="drivingWheels"><g:message code="vehicleDataImport.drivingWheels" default="Driving Wheels" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: vehicleDataImportInstance, field: 'drivingWheels', 'errors')}">
                                    <g:textField name="drivingWheels" value="${fieldValue(bean: vehicleDataImportInstance, field: 'drivingWheels')}" />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="vehicleClass"><g:message code="vehicleDataImport.vehicleClass" default="Vehicle Class" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: vehicleDataImportInstance, field: 'vehicleClass', 'errors')}">
                                    <g:textField name="vehicleClass" value="${fieldValue(bean: vehicleDataImportInstance, field: 'vehicleClass')}" />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="fuelConsumption"><g:message code="vehicleDataImport.fuelConsumption" default="Fuel Consumption" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: vehicleDataImportInstance, field: 'fuelConsumption', 'errors')}">
                                    <g:textField name="fuelConsumption" value="${fieldValue(bean: vehicleDataImportInstance, field: 'fuelConsumption')}" />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="fuelConsumptionUrban"><g:message code="vehicleDataImport.fuelConsumptionUrban" default="Fuel Consumption Urban" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: vehicleDataImportInstance, field: 'fuelConsumptionUrban', 'errors')}">
                                    <g:textField name="fuelConsumptionUrban" value="${fieldValue(bean: vehicleDataImportInstance, field: 'fuelConsumptionUrban')}" />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="fuelConsumptionExtraUrban"><g:message code="vehicleDataImport.fuelConsumptionExtraUrban" default="Fuel Consumption Extra Urban" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: vehicleDataImportInstance, field: 'fuelConsumptionExtraUrban', 'errors')}">
                                    <g:textField name="fuelConsumptionExtraUrban" value="${fieldValue(bean: vehicleDataImportInstance, field: 'fuelConsumptionExtraUrban')}" />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="modelReleaseYear"><g:message code="vehicleDataImport.modelReleaseYear" default="Model Release Year" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: vehicleDataImportInstance, field: 'modelReleaseYear', 'errors')}">
                                    <g:textField name="modelReleaseYear" value="${fieldValue(bean: vehicleDataImportInstance, field: 'modelReleaseYear')}" />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="currentModel"><g:message code="vehicleDataImport.currentModel" default="Current Model" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: vehicleDataImportInstance, field: 'currentModel', 'errors')}">
                                    <g:textField name="currentModel" value="${fieldValue(bean: vehicleDataImportInstance, field: 'currentModel')}" />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="lastUpdated"><g:message code="vehicleDataImport.lastUpdated" default="Last Updated" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: vehicleDataImportInstance, field: 'lastUpdated', 'errors')}">
                                    <g:datePicker name="lastUpdated" value="${vehicleDataImportInstance?.lastUpdated}"  />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="dateCreated"><g:message code="vehicleDataImport.dateCreated" default="Date Created" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: vehicleDataImportInstance, field: 'dateCreated', 'errors')}">
                                    <g:datePicker name="dateCreated" value="${vehicleDataImportInstance?.dateCreated}"  />

                                </td>
                            </tr>
                        
                        </tbody>
                    </table>
                </div>
                <div class="buttons">
                    <span class="button"><g:actionSubmit class="save" action="update" value="${message(code: 'update', 'default': 'Update')}" /></span>
                    <span class="button"><g:actionSubmit class="delete" action="delete" value="${message(code: 'delete', 'default': 'Delete')}" onclick="return confirm('${message(code: 'delete.confirm', 'default': 'Are you sure?')}');" /></span>
                </div>
            </g:form>
        </div>
    </body>
</html>
