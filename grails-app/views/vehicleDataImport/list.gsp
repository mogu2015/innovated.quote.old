
<%@ page import="com.innovated.iris.domain.importExport.VehicleDataImport" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <title><g:message code="vehicleDataImport.list" default="Vehicle Data Import List" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLinkTo(dir: '')}"><g:message code="home" default="Home" /></a></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="vehicleDataImport.new" default="New VehicleDataImport" /></g:link></span>
            <span class="menuButton"><g:link class="save" action="importFile"><g:message code="vehicleDataImport.import" default="Import Vehicle Data" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="vehicleDataImport.list" default="Vehicle Data Import List" /></h1>
            <g:if test="${flash.message}">
            <div class="message"><g:message code="${flash.message}" args="${flash.args}" default="${flash.defaultMessage}" /></div>
            </g:if>
            <div class="list">
                <table>
                    <thead>
                        <tr>
                        
                   	    <g:sortableColumn property="id" title="Id" titleKey="vehicleDataImport.id" />
                        
                   	    <g:sortableColumn property="publishedVehicleID" title="Published Vehicle ID" titleKey="vehicleDataImport.publishedVehicleID" />
                        
                   	    <g:sortableColumn property="vehicleID" title="Vehicle ID" titleKey="vehicleDataImport.vehicleID" />
                        
                   	    <g:sortableColumn property="make" title="Make" titleKey="vehicleDataImport.make" />
                        
                   	    <g:sortableColumn property="modelReleaseVersion" title="Model Release Version" titleKey="vehicleDataImport.modelReleaseVersion" />
                        
                   	    <g:sortableColumn property="marketingModel" title="Marketing Model" titleKey="vehicleDataImport.marketingModel" />
                        
                        </tr>
                    </thead>
                    <tbody>
                    <g:each in="${vehicleDataImportInstanceList}" status="i" var="vehicleDataImportInstance">
                        <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
                        
                            <td><g:link action="show" id="${vehicleDataImportInstance.id}">${fieldValue(bean: vehicleDataImportInstance, field: "id")}</g:link></td>
                        
                            <td>${fieldValue(bean: vehicleDataImportInstance, field: "publishedVehicleID")}</td>
                        
                            <td>${fieldValue(bean: vehicleDataImportInstance, field: "vehicleID")}</td>
                        
                            <td>${fieldValue(bean: vehicleDataImportInstance, field: "make")}</td>
                        
                            <td>${fieldValue(bean: vehicleDataImportInstance, field: "modelReleaseVersion")}</td>
                        
                            <td>${fieldValue(bean: vehicleDataImportInstance, field: "marketingModel")}</td>
                        
                        </tr>
                    </g:each>
                    </tbody>
                </table>
            </div>
            <div class="paginateButtons">
                <g:paginate total="${vehicleDataImportInstanceTotal}" />
            </div>
        </div>
    </body>
</html>
