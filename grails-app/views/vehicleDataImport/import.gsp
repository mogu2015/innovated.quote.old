<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <title><g:message code="vehicleDataImport.import" default="Import Vehicle Data" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLinkTo(dir: '')}"><g:message code="home" default="Home" /></a></span>
        </div>
        <div class="body">
            <h1><g:message code="vehicleDataImport.import" default="Import Vehicle Data File (XML format)" /></h1>
            <g:if test="${flash.message}">
            <div class="message"><g:message code="${flash.message}" args="${flash.args}" default="${flash.defaultMessage}" /></div>
            </g:if>
            <div class="dialog">
                <g:uploadForm name="xmlImportForm" action="saveXml">
					<label for="importFile">Select XML File:</label>  
					<input type="file" name="importFile" />
					<div class="buttons"><span class="button"><input class="save" type="submit" value="Import"/></span></div> 
				</g:uploadForm>
            </div>
        </div>
    </body>
</html>
