
<%@ page import="com.innovated.iris.domain.importExport.VehicleDataImport" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <title><g:message code="vehicleDataImport.show" default="Show VehicleDataImport" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLinkTo(dir: '')}"><g:message code="home" default="Home" /></a></span>
            <span class="menuButton"><g:link class="list" action="list"><g:message code="vehicleDataImport.list" default="VehicleDataImport List" /></g:link></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="vehicleDataImport.new" default="New VehicleDataImport" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="vehicleDataImport.show" default="Show VehicleDataImport" /></h1>
            <g:if test="${flash.message}">
            <div class="message"><g:message code="${flash.message}" args="${flash.args}" default="${flash.defaultMessage}" /></div>
            </g:if>
            <g:form>
                <g:hiddenField name="id" value="${vehicleDataImportInstance?.id}" />
                <div class="dialog">
                    <table>
                        <tbody>
                        
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="vehicleDataImport.id" default="Id" />:</td>
                                
                                <td valign="top" class="value">${fieldValue(bean: vehicleDataImportInstance, field: "id")}</td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="vehicleDataImport.publishedVehicleID" default="Published Vehicle ID" />:</td>
                                
                                <td valign="top" class="value">${fieldValue(bean: vehicleDataImportInstance, field: "publishedVehicleID")}</td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="vehicleDataImport.vehicleID" default="Vehicle ID" />:</td>
                                
                                <td valign="top" class="value">${fieldValue(bean: vehicleDataImportInstance, field: "vehicleID")}</td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="vehicleDataImport.make" default="Make" />:</td>
                                
                                <td valign="top" class="value">${fieldValue(bean: vehicleDataImportInstance, field: "make")}</td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="vehicleDataImport.modelReleaseVersion" default="Model Release Version" />:</td>
                                
                                <td valign="top" class="value">${fieldValue(bean: vehicleDataImportInstance, field: "modelReleaseVersion")}</td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="vehicleDataImport.marketingModel" default="Marketing Model" />:</td>
                                
                                <td valign="top" class="value">${fieldValue(bean: vehicleDataImportInstance, field: "marketingModel")}</td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="vehicleDataImport.variant" default="Variant" />:</td>
                                
                                <td valign="top" class="value">${fieldValue(bean: vehicleDataImportInstance, field: "variant")}</td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="vehicleDataImport.makeModelVariant" default="Make Model Variant" />:</td>
                                
                                <td valign="top" class="value">${fieldValue(bean: vehicleDataImportInstance, field: "makeModelVariant")}</td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="vehicleDataImport.bodyStyle" default="Body Style" />:</td>
                                
                                <td valign="top" class="value">${fieldValue(bean: vehicleDataImportInstance, field: "bodyStyle")}</td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="vehicleDataImport.seatingCapacity" default="Seating Capacity" />:</td>
                                
                                <td valign="top" class="value">${fieldValue(bean: vehicleDataImportInstance, field: "seatingCapacity")}</td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="vehicleDataImport.bodySeats" default="Body Seats" />:</td>
                                
                                <td valign="top" class="value">${fieldValue(bean: vehicleDataImportInstance, field: "bodySeats")}</td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="vehicleDataImport.engineDisplacement" default="Engine Displacement" />:</td>
                                
                                <td valign="top" class="value">${fieldValue(bean: vehicleDataImportInstance, field: "engineDisplacement")}</td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="vehicleDataImport.engineConfiguration" default="Engine Configuration" />:</td>
                                
                                <td valign="top" class="value">${fieldValue(bean: vehicleDataImportInstance, field: "engineConfiguration")}</td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="vehicleDataImport.induction" default="Induction" />:</td>
                                
                                <td valign="top" class="value">${fieldValue(bean: vehicleDataImportInstance, field: "induction")}</td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="vehicleDataImport.engine" default="Engine" />:</td>
                                
                                <td valign="top" class="value">${fieldValue(bean: vehicleDataImportInstance, field: "engine")}</td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="vehicleDataImport.transmissionType" default="Transmission Type" />:</td>
                                
                                <td valign="top" class="value">${fieldValue(bean: vehicleDataImportInstance, field: "transmissionType")}</td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="vehicleDataImport.fwdGearsNo" default="Fwd Gears No" />:</td>
                                
                                <td valign="top" class="value">${fieldValue(bean: vehicleDataImportInstance, field: "fwdGearsNo")}</td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="vehicleDataImport.trans" default="Trans" />:</td>
                                
                                <td valign="top" class="value">${fieldValue(bean: vehicleDataImportInstance, field: "trans")}</td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="vehicleDataImport.fuelType" default="Fuel Type" />:</td>
                                
                                <td valign="top" class="value">${fieldValue(bean: vehicleDataImportInstance, field: "fuelType")}</td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="vehicleDataImport.drivingWheels" default="Driving Wheels" />:</td>
                                
                                <td valign="top" class="value">${fieldValue(bean: vehicleDataImportInstance, field: "drivingWheels")}</td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="vehicleDataImport.vehicleClass" default="Vehicle Class" />:</td>
                                
                                <td valign="top" class="value">${fieldValue(bean: vehicleDataImportInstance, field: "vehicleClass")}</td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="vehicleDataImport.fuelConsumption" default="Fuel Consumption" />:</td>
                                
                                <td valign="top" class="value">${fieldValue(bean: vehicleDataImportInstance, field: "fuelConsumption")}</td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="vehicleDataImport.fuelConsumptionUrban" default="Fuel Consumption Urban" />:</td>
                                
                                <td valign="top" class="value">${fieldValue(bean: vehicleDataImportInstance, field: "fuelConsumptionUrban")}</td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="vehicleDataImport.fuelConsumptionExtraUrban" default="Fuel Consumption Extra Urban" />:</td>
                                
                                <td valign="top" class="value">${fieldValue(bean: vehicleDataImportInstance, field: "fuelConsumptionExtraUrban")}</td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="vehicleDataImport.modelReleaseYear" default="Model Release Year" />:</td>
                                
                                <td valign="top" class="value">${fieldValue(bean: vehicleDataImportInstance, field: "modelReleaseYear")}</td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="vehicleDataImport.currentModel" default="Current Model" />:</td>
                                
                                <td valign="top" class="value">${fieldValue(bean: vehicleDataImportInstance, field: "currentModel")}</td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="vehicleDataImport.lastUpdated" default="Last Updated" />:</td>
                                
                                <td valign="top" class="value"><g:formatDate date="${vehicleDataImportInstance?.lastUpdated}" /></td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="vehicleDataImport.dateCreated" default="Date Created" />:</td>
                                
                                <td valign="top" class="value"><g:formatDate date="${vehicleDataImportInstance?.dateCreated}" /></td>
                                
                            </tr>
                            
                        </tbody>
                    </table>
                </div>
                <div class="buttons">
                    <span class="button"><g:actionSubmit class="edit" action="edit" value="${message(code: 'edit', 'default': 'Edit')}" /></span>
                    <span class="button"><g:actionSubmit class="delete" action="delete" value="${message(code: 'delete', 'default': 'Delete')}" onclick="return confirm('${message(code: 'delete.confirm', 'default': 'Are you sure?')}');" /></span>
                </div>
            </g:form>
        </div>
    </body>
</html>
