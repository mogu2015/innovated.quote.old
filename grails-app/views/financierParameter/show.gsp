
<%@ page import="com.innovated.iris.domain.lookup.FinancierParameter" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <title><g:message code="financierParameter.show" default="Show FinancierParameter" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLinkTo(dir: '')}"><g:message code="home" default="Home" /></a></span>
            <span class="menuButton"><g:link class="list" action="list"><g:message code="financierParameter.list" default="FinancierParameter List" /></g:link></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="financierParameter.new" default="New FinancierParameter" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="financierParameter.show" default="Show FinancierParameter" /></h1>
            <g:if test="${flash.message}">
            <div class="message"><g:message code="${flash.message}" args="${flash.args}" default="${flash.defaultMessage}" /></div>
            </g:if>
            <g:form>
                <g:hiddenField name="id" value="${financierParameterInstance?.id}" />
                <div class="dialog">
                    <table>
                        <tbody>
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="financierParameter.id" default="ID" />:</td>
                                <td valign="top" class="value">${fieldValue(bean: financierParameterInstance, field: "id")}</td>
                            </tr>
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="financierParameter.customerCode" default="Customer Code" />:</td>
                                <td valign="top" class="value">${fieldValue(bean: financierParameterInstance, field: "customerCode")}</td>
                            </tr>
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="financierParameter.dateFrom" default="Date From" />:</td>
                                <td valign="top" class="value"><g:formatDate format="d MMM yyyy" date="${financierParameterInstance?.dateFrom}"/></td>
                            </tr>
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="financierParameter.defaultSupplier" default="Default Supplier" />:</td>
                                <td valign="top" class="value"><g:link controller="supplier" action="show" id="${financierParameterInstance?.defaultSupplier?.id}">${financierParameterInstance?.defaultSupplier?.encodeAsHTML()}</g:link></td>
                            </tr>
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="financierParameter.documentationFee" default="Documentation Fee" />:</td>
                                <!--<td valign="top" class="value">${fieldValue(bean: financierParameterInstance, field: "documentationFee")}</td>-->
                                <td valign="top" class="value">${formatNumber(number:financierParameterInstance.documentationFee, type:'currency', minFractionDigits:2, maxFractionDigits:2)}</td>
                            </tr>
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="financierParameter.interestRate" default="Interest Rate" />:</td>
                                <td valign="top" class="value">${formatNumber(number:financierParameterInstance.interestRate, type:'percent', minFractionDigits:2, maxFractionDigits:8)}</td>
                            </tr>
                            <tr>
                            	<td>&nbsp;</td>
                            	<td>&nbsp;</td>
                            </tr>
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="financierParameter.dateCreated" default="Date Created" />:</td>
                                <td valign="top" class="value"><g:formatDate date="${financierParameterInstance?.dateCreated}" /></td>
                            </tr>
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="financierParameter.lastUpdated" default="Last Updated" />:</td>
                                <td valign="top" class="value"><g:formatDate date="${financierParameterInstance?.lastUpdated}" /></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="buttons">
                    <span class="button"><g:actionSubmit class="edit" action="edit" value="${message(code: 'edit', 'default': 'Edit')}" /></span>
                    <span class="button"><g:actionSubmit class="delete" action="delete" value="${message(code: 'delete', 'default': 'Delete')}" onclick="return confirm('${message(code: 'delete.confirm', 'default': 'Are you sure?')}');" /></span>
                </div>
            </g:form>
        </div>
    </body>
</html>
