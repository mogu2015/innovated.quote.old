
<%@ page import="com.innovated.iris.domain.lookup.FinancierParameter" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <title><g:message code="financierParameter.list" default="FinancierParameter List" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLinkTo(dir: '')}"><g:message code="home" default="Home" /></a></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="financierParameter.new" default="New FinancierParameter" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="financierParameter.list" default="FinancierParameter List" /></h1>
            <g:if test="${flash.message}">
            <div class="message"><g:message code="${flash.message}" args="${flash.args}" default="${flash.defaultMessage}" /></div>
            </g:if>
            <div class="list">
                <table>
                    <thead>
                        <tr>
                        
                   	    <g:sortableColumn property="id" title="Id" titleKey="financierParameter.id" />
                        
                   	    <g:sortableColumn property="customerCode" title="Customer Code" titleKey="financierParameter.customerCode" />
                        
                   	    <g:sortableColumn property="dateCreated" title="Date Created" titleKey="financierParameter.dateCreated" />
                        
                   	    <g:sortableColumn property="dateFrom" title="Date From" titleKey="financierParameter.dateFrom" />
                        
                   	    <th><g:message code="financierParameter.defaultSupplier" default="Default Supplier" /></th>
                   	    
                   	    <g:sortableColumn property="documentationFee" title="Documentation Fee" titleKey="financierParameter.documentationFee" />
                        
                        </tr>
                    </thead>
                    <tbody>
                    <g:each in="${financierParameterInstanceList}" status="i" var="financierParameterInstance">
                        <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
                        
                            <td><g:link action="show" id="${financierParameterInstance.id}">${fieldValue(bean: financierParameterInstance, field: "id")}</g:link></td>
                        
                            <td>${fieldValue(bean: financierParameterInstance, field: "customerCode")}</td>
                        
                            <td><g:formatDate date="${financierParameterInstance.dateCreated}" /></td>
                        
                            <td><g:formatDate date="${financierParameterInstance.dateFrom}" /></td>
                        
                            <td>${fieldValue(bean: financierParameterInstance, field: "defaultSupplier")}</td>
                        
                            <td>${fieldValue(bean: financierParameterInstance, field: "documentationFee")}</td>
                        
                        </tr>
                    </g:each>
                    </tbody>
                </table>
            </div>
            <div class="paginateButtons">
                <g:paginate total="${financierParameterInstanceTotal}" />
            </div>
        </div>
    </body>
</html>
