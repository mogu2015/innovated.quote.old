
<%@ page import="com.innovated.iris.domain.lookup.FinancierParameter" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <title><g:message code="financierParameter.edit" default="Edit FinancierParameter" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLinkTo(dir: '')}"><g:message code="home" default="Home" /></a></span>
            <span class="menuButton"><g:link class="list" action="list"><g:message code="financierParameter.list" default="FinancierParameter List" /></g:link></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="financierParameter.new" default="New FinancierParameter" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="financierParameter.edit" default="Edit FinancierParameter" /></h1>
            <g:if test="${flash.message}">
            	<div class="message"><g:message code="${flash.message}" args="${flash.args}" default="${flash.defaultMessage}" /></div>
            </g:if>
            <g:hasErrors bean="${financierParameterInstance}">
				<div class="errors">
					<g:renderErrors bean="${financierParameterInstance}" as="list" />
				</div>
            </g:hasErrors>
            <g:form method="post" >
                <g:hiddenField name="id" value="${financierParameterInstance?.id}" />
                <g:hiddenField name="version" value="${financierParameterInstance?.version}" />
                <div class="dialog">
                    <fieldset>
                        <legend><g:message code="financierParameter.edit.legend" default="Update FinancierParameter Details"/></legend>
                        
                        <div class="prop mandatory ${hasErrors(bean: financierParameterInstance, field: 'dateFrom', 'error')}">
                            <label for="dateFrom">
                                <g:message code="financierParameter.dateFrom" default="Date From" />
                                <span class="indicator">*</span>
                            </label>
                            <g:datePicker name="dateFrom" value="${financierParameterInstance?.dateFrom}" precision="day" />
                        </div>
                        
                        <div class="prop ${hasErrors(bean: financierParameterInstance, field: 'customerCode', 'error')}">
                            <label for="customerCode">
                                <g:message code="financierParameter.customerCode" default="Customer Code" />
                                
                            </label>
                            <g:textField name="customerCode" value="${fieldValue(bean: financierParameterInstance, field: 'customerCode')}" />

                        </div>
                        
                        <!--
                        <div class="prop mandatory ${hasErrors(bean: financierParameterInstance, field: 'dateCreated', 'error')}">
                            <label for="dateCreated">
                                <g:message code="financierParameter.dateCreated" default="Date Created" />
                                <span class="indicator">*</span>
                            </label>
                            <g:datePicker name="dateCreated" value="${financierParameterInstance?.dateCreated}"  />

                        </div>
                        -->
                        
                        <div class="prop mandatory ${hasErrors(bean: financierParameterInstance, field: 'defaultSupplier', 'error')}">
                            <label for="defaultSupplier">
                                <g:message code="financierParameter.defaultSupplier" default="Default Supplier" />
                                <span class="indicator">*</span>
                            </label>
                            <g:select name="defaultSupplier.id" from="${com.innovated.iris.domain.Supplier.list()}" optionKey="id" value="${financierParameterInstance?.defaultSupplier?.id}"  />

                        </div>
                        
                        <div class="prop mandatory ${hasErrors(bean: financierParameterInstance, field: 'documentationFee', 'error')}">
                            <label for="documentationFee">
                                <g:message code="financierParameter.documentationFee" default="Documentation Fee" />
                                <span class="indicator">*</span>
                            </label>
                            <g:textField name="documentationFee" value="${fieldValue(bean: financierParameterInstance, field: 'documentationFee')}" />

                        </div>
                        
                        <div class="prop mandatory ${hasErrors(bean: financierParameterInstance, field: 'interestRate', 'error')}">
                            <label for="interestRate">
                                <g:message code="financierParameter.interestRate" default="Interest Rate" />
                                <span class="indicator">*</span>
                            </label>
                            <g:textField name="interestRate" value="${formatNumber(number:financierParameterInstance.interestRate, type:'number', minFractionDigits:2, maxFractionDigits:8)}" />
                        </div>
                        <!--
                        <div class="prop mandatory ${hasErrors(bean: financierParameterInstance, field: 'lastUpdated', 'error')}">
                            <label for="lastUpdated">
                                <g:message code="financierParameter.lastUpdated" default="Last Updated" />
                                <span class="indicator">*</span>
                            </label>
                            <g:datePicker name="lastUpdated" value="${financierParameterInstance?.lastUpdated}"  />

                        </div>
                        -->
                    </fieldset>
                </div>
                <div class="buttons">
                    <span class="button"><g:actionSubmit class="save" action="update" value="${message(code: 'update', 'default': 'Update')}" /></span>
                    <span class="button"><g:actionSubmit class="delete" action="delete" value="${message(code: 'delete', 'default': 'Delete')}" onclick="return confirm('${message(code: 'delete.confirm', 'default': 'Are you sure?')}');" /></span>
                </div>
            </g:form>
        </div>
    </body>
</html>

