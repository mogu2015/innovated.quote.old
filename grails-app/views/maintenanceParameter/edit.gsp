
<%@ page import="com.innovated.iris.domain.lookup.MaintenanceParameter" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <title><g:message code="maintenanceParameter.edit" default="Edit MaintenanceParameter" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLinkTo(dir: '')}"><g:message code="home" default="Home" /></a></span>
            <span class="menuButton"><g:link class="list" action="list"><g:message code="maintenanceParameter.list" default="MaintenanceParameter List" /></g:link></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="maintenanceParameter.new" default="New MaintenanceParameter" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="maintenanceParameter.edit" default="Edit MaintenanceParameter" /></h1>
            <g:if test="${flash.message}">
            <div class="message"><g:message code="${flash.message}" args="${flash.args}" default="${flash.defaultMessage}" /></div>
            </g:if>
            <g:hasErrors bean="${maintenanceParameterInstance}">
            <div class="errors">
                <g:renderErrors bean="${maintenanceParameterInstance}" as="list" />
            </div>
            </g:hasErrors>
            <g:form method="post" >
                <g:hiddenField name="id" value="${maintenanceParameterInstance?.id}" />
                <g:hiddenField name="version" value="${maintenanceParameterInstance?.version}" />
                <div class="dialog">
                    <table>
                        <tbody>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="dateFrom"><g:message code="maintenanceParameter.dateFrom" default="Date From" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: maintenanceParameterInstance, field: 'dateFrom', 'errors')}">
                                    <g:datePicker name="dateFrom" value="${maintenanceParameterInstance?.dateFrom}"  />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="make"><g:message code="maintenanceParameter.make" default="Make" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: maintenanceParameterInstance, field: 'make', 'errors')}">
                                    <g:select name="make.id" from="${com.innovated.iris.domain.enums.VehicleManufacturer.list()}" optionKey="id" value="${maintenanceParameterInstance?.make?.id}"  />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="model"><g:message code="maintenanceParameter.model" default="Model" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: maintenanceParameterInstance, field: 'model', 'errors')}">
                                    <g:select name="model.id" from="${com.innovated.iris.domain.enums.VehicleModel.list()}" optionKey="id" value="${maintenanceParameterInstance?.model?.id}" noSelection="['null': '']" />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="variant"><g:message code="maintenanceParameter.variant" default="Variant" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: maintenanceParameterInstance, field: 'variant', 'errors')}">
                                    <g:select name="variant.id" from="${com.innovated.iris.domain.MakeModel.list()}" optionKey="id" value="${maintenanceParameterInstance?.variant?.id}" noSelection="['null': '']" />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="over0km"><g:message code="maintenanceParameter.over0km" default="Over0km" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: maintenanceParameterInstance, field: 'over0km', 'errors')}">
                                    <g:textField name="over0km" value="${fieldValue(bean: maintenanceParameterInstance, field: 'over0km')}" />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="over20000km"><g:message code="maintenanceParameter.over20000km" default="Over20000km" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: maintenanceParameterInstance, field: 'over20000km', 'errors')}">
                                    <g:textField name="over20000km" value="${fieldValue(bean: maintenanceParameterInstance, field: 'over20000km')}" />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="over30000km"><g:message code="maintenanceParameter.over30000km" default="Over30000km" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: maintenanceParameterInstance, field: 'over30000km', 'errors')}">
                                    <g:textField name="over30000km" value="${fieldValue(bean: maintenanceParameterInstance, field: 'over30000km')}" />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="over40000km"><g:message code="maintenanceParameter.over40000km" default="Over40000km" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: maintenanceParameterInstance, field: 'over40000km', 'errors')}">
                                    <g:textField name="over40000km" value="${fieldValue(bean: maintenanceParameterInstance, field: 'over40000km')}" />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="lastUpdated"><g:message code="maintenanceParameter.lastUpdated" default="Last Updated" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: maintenanceParameterInstance, field: 'lastUpdated', 'errors')}">
                                    <g:datePicker name="lastUpdated" value="${maintenanceParameterInstance?.lastUpdated}"  />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="dateCreated"><g:message code="maintenanceParameter.dateCreated" default="Date Created" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: maintenanceParameterInstance, field: 'dateCreated', 'errors')}">
                                    <g:datePicker name="dateCreated" value="${maintenanceParameterInstance?.dateCreated}"  />

                                </td>
                            </tr>
                        
                        </tbody>
                    </table>
                </div>
                <div class="buttons">
                    <span class="button"><g:actionSubmit class="save" action="update" value="${message(code: 'update', 'default': 'Update')}" /></span>
                    <span class="button"><g:actionSubmit class="delete" action="delete" value="${message(code: 'delete', 'default': 'Delete')}" onclick="return confirm('${message(code: 'delete.confirm', 'default': 'Are you sure?')}');" /></span>
                </div>
            </g:form>
        </div>
    </body>
</html>
