<%@
page import="com.innovated.iris.domain.lookup.MaintenanceParameter"
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <title><g:message code="maintenanceParameter.list" default="MaintenanceParameter List" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLinkTo(dir: '')}"><g:message code="home" default="Home" /></a></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="maintenanceParameter.new" default="New MaintenanceParameter" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="maintenanceParameter.list" default="MaintenanceParameter List" /></h1>
            <g:if test="${flash.message}">
            <div class="message"><g:message code="${flash.message}" args="${flash.args}" default="${flash.defaultMessage}" /></div>
            </g:if>
            <div class="list">
                <table>
                    <thead>
                        <tr>
		               	    <g:sortableColumn property="id" title="Id" titleKey="maintenanceParameter.id" />
		               	    <g:sortableColumn property="dateFrom" title="Date From" titleKey="maintenanceParameter.dateFrom" />
		               	    <th><g:message code="maintenanceParameter.make" default="Make" /></th>
		               	    <th><g:message code="maintenanceParameter.model" default="Model" /></th>
		               	    <th><g:message code="maintenanceParameter.variant" default="Variant" /></th>
		               	    <g:sortableColumn property="over0km" title="0 - 19,999km" titleKey="maintenanceParameter.over0km" />
		               	    <g:sortableColumn property="over20000km" title="20,000 - 29,999km" titleKey="maintenanceParameter.over20000km" />
		               	    <g:sortableColumn property="over30000km" title="30,000 - 39,999km" titleKey="maintenanceParameter.over30000km" />
		               	    <g:sortableColumn property="over40000km" title="Over 40,000km" titleKey="maintenanceParameter.over40000km" />
                        </tr>
                    </thead>
                    <tbody>
                    <g:each in="${maintenanceParameterInstanceList}" status="i" var="maintenanceParameterInstance">
                        <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
                            <td><g:link action="show" id="${maintenanceParameterInstance.id}">${fieldValue(bean: maintenanceParameterInstance, field: "id")}</g:link></td>
                            <td><g:formatDate date="${maintenanceParameterInstance.dateFrom}" format="d MMM yyyy" /></td>
                            <td>${fieldValue(bean: maintenanceParameterInstance, field: "make")}</td>
                            <td>${fieldValue(bean: maintenanceParameterInstance, field: "model")}</td>
                            <td>${fieldValue(bean: maintenanceParameterInstance, field: "variant")}</td>
                            <td class="numeric">${formatNumber(number:maintenanceParameterInstance.over0km, type:'currency', minFractionDigits:2, maxFractionDigits:2)}</td>
                            <td class="numeric">${formatNumber(number:maintenanceParameterInstance.over20000km, type:'currency', minFractionDigits:2, maxFractionDigits:2)}</td>
                            <td class="numeric">${formatNumber(number:maintenanceParameterInstance.over30000km, type:'currency', minFractionDigits:2, maxFractionDigits:2)}</td>
                            <td class="numeric">${formatNumber(number:maintenanceParameterInstance.over40000km, type:'currency', minFractionDigits:2, maxFractionDigits:2)}</td>
                        </tr>
                    </g:each>
                    </tbody>
                </table>
            </div>
            <div class="paginateButtons">
                <g:paginate total="${maintenanceParameterInstanceTotal}" />
            </div>
        </div>
    </body>
</html>
