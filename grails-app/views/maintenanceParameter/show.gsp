
<%@ page import="com.innovated.iris.domain.lookup.MaintenanceParameter" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <title><g:message code="maintenanceParameter.show" default="Show MaintenanceParameter" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLinkTo(dir: '')}"><g:message code="home" default="Home" /></a></span>
            <span class="menuButton"><g:link class="list" action="list"><g:message code="maintenanceParameter.list" default="MaintenanceParameter List" /></g:link></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="maintenanceParameter.new" default="New MaintenanceParameter" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="maintenanceParameter.show" default="Show MaintenanceParameter" /></h1>
            <g:if test="${flash.message}">
            <div class="message"><g:message code="${flash.message}" args="${flash.args}" default="${flash.defaultMessage}" /></div>
            </g:if>
            <g:form>
                <g:hiddenField name="id" value="${maintenanceParameterInstance?.id}" />
                <div class="dialog">
                    <table>
                        <tbody>
                        
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="maintenanceParameter.id" default="Id" />:</td>
                                
                                <td valign="top" class="value">${fieldValue(bean: maintenanceParameterInstance, field: "id")}</td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="maintenanceParameter.dateFrom" default="Date From" />:</td>
                                
                                <td valign="top" class="value"><g:formatDate date="${maintenanceParameterInstance?.dateFrom}" /></td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="maintenanceParameter.make" default="Make" />:</td>
                                
                                <td valign="top" class="value"><g:link controller="vehicleManufacturer" action="show" id="${maintenanceParameterInstance?.make?.id}">${maintenanceParameterInstance?.make?.encodeAsHTML()}</g:link></td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="maintenanceParameter.model" default="Model" />:</td>
                                
                                <td valign="top" class="value"><g:link controller="vehicleModel" action="show" id="${maintenanceParameterInstance?.model?.id}">${maintenanceParameterInstance?.model?.encodeAsHTML()}</g:link></td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="maintenanceParameter.variant" default="Variant" />:</td>
                                
                                <td valign="top" class="value"><g:link controller="makeModel" action="show" id="${maintenanceParameterInstance?.variant?.id}">${maintenanceParameterInstance?.variant?.encodeAsHTML()}</g:link></td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="maintenanceParameter.over0km" default="Over0km" />:</td>
                                
                                <td valign="top" class="value">${fieldValue(bean: maintenanceParameterInstance, field: "over0km")}</td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="maintenanceParameter.over20000km" default="Over20000km" />:</td>
                                
                                <td valign="top" class="value">${fieldValue(bean: maintenanceParameterInstance, field: "over20000km")}</td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="maintenanceParameter.over30000km" default="Over30000km" />:</td>
                                
                                <td valign="top" class="value">${fieldValue(bean: maintenanceParameterInstance, field: "over30000km")}</td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="maintenanceParameter.over40000km" default="Over40000km" />:</td>
                                
                                <td valign="top" class="value">${fieldValue(bean: maintenanceParameterInstance, field: "over40000km")}</td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="maintenanceParameter.lastUpdated" default="Last Updated" />:</td>
                                
                                <td valign="top" class="value"><g:formatDate date="${maintenanceParameterInstance?.lastUpdated}" /></td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="maintenanceParameter.dateCreated" default="Date Created" />:</td>
                                
                                <td valign="top" class="value"><g:formatDate date="${maintenanceParameterInstance?.dateCreated}" /></td>
                                
                            </tr>
                            
                        </tbody>
                    </table>
                </div>
                <div class="buttons">
                    <span class="button"><g:actionSubmit class="edit" action="edit" value="${message(code: 'edit', 'default': 'Edit')}" /></span>
                    <span class="button"><g:actionSubmit class="delete" action="delete" value="${message(code: 'delete', 'default': 'Delete')}" onclick="return confirm('${message(code: 'delete.confirm', 'default': 'Are you sure?')}');" /></span>
                </div>
            </g:form>
        </div>
    </body>
</html>
