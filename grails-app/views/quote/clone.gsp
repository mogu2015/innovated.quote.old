<%@

page import="com.innovated.iris.domain.*"

%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <title><g:message code="quote.clone" default="Clone Quote" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLinkTo(dir: '')}"><g:message code="home" default="Home" /></a></span>
            <span class="menuButton"><g:link class="list" action="list"><g:message code="quote.list" default="Quote List" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="quote.clone" default="Clone Quote" /></h1>
            <g:if test="${flash.message}">
            	<div class="message"><g:message code="${flash.message}" args="${flash.args}" default="${flash.defaultMessage}" /></div>
            </g:if>
            
            <g:if test="${testMessage}">
            	<div class="message">${testMessage}</div>
            </g:if>
            
            <g:hasErrors bean="${quoteInstance}">
				<div class="errors">
					<g:renderErrors bean="${quoteInstance}" as="list"/>
				</div>
            </g:hasErrors>
            <g:form action="save" method="post" >
                <div class="dialog">
                    
                    <g:ifAnyGranted role="ROLE_SUPER,ROLE_ADMIN">
                    <fieldset>
                        <legend><g:message code="quote.create.legend" default="Quotation Date Override"/></legend>
                            <div class="prop">
                                <label for="fromDate"><g:message code="quote.fromDate" default="Quote as at" /></label>
                                <g:datePicker name="fromDate" value="${quoteInstance?.fromDate}" precision="day" years="${1998..2010}"/>
                            </div>
					</fieldset>
                    </g:ifAnyGranted>
                    
                    <g:ifNotGranted role="ROLE_SUPER,ROLE_ADMIN">
                    <g:hiddenField name="fromDate" value="${quoteInstance?.fromDate}" />
                    </g:ifNotGranted>
                    
                    <fieldset>
                        <legend><g:message code="quote.create.legend" default="Client Details"/></legend>
                            <div class="prop mandatory ${hasErrors(bean: quoteInstance, field: 'customer', 'error')}">
                                <label for="customer">
                                    <g:message code="quote.customer" default="Employee" />
                                    <span class="indicator">*</span>
                                </label>
                                <g:select name="customer.id"
                                	from="${individuals}"
                                	optionKey="id" optionValue="${{it.entity?.name}}"
                                	value="${quoteInstance?.customer?.id}"
                                	noSelection="${['null':'Select Employee...']}" />
                            </div>
                            
                            <div class="prop mandatory">
                                <label for="phone">
                                    <g:message code="quote.phone" default="Phone/Email" />
                                    <span class="indicator">*</span>
                                </label>
                                <g:textField name="phone" value="We need phone & email details!" />
                            </div>
                            
                            <div class="prop mandatory ${hasErrors(bean: quoteInstance, field: 'address', 'error')}">
                                <label for="address">
                                    <g:message code="quote.address" default="Address" />
                                    <span class="indicator">*</span>
                                </label>
                                <g:select name="address.id"
                                	from="${addresses}"
                                	optionKey="id"
                                	value="${quoteInstance?.address?.id}"
                                	noSelection="${['null':'Select Address...']}" />
                            </div>
                            <div class="prop mandatory ${hasErrors(bean: quoteInstance, field: 'employment', 'error')}">
                                <label for="employment">
                                    <g:message code="quote.employment" default="Employer" />
                                    <span class="indicator">*</span>
                                </label>
                                <g:select name="employment.id"
                                	from="${com.innovated.iris.domain.Employment.list()}"
                                	optionKey="id" optionValue="${{it.employer?.name}}"
                                	value="${quoteInstance?.employment?.id}"
                                	noSelection="${['null':'Select Employer...']}" />
                            </div>
                            <div class="prop mandatory">
                                <label for="grossAnnualSalary">
                                    <g:message code="quote.grossAnnualSalary" default="Gross Salary" />
                                    <span class="indicator">*</span>
                                </label>
                                <g:textField name="grossAnnualSalary" value="${fieldValue(bean: quoteInstance, field: 'grossAnnualSalary')}" />
                            </div>
					</fieldset>
					<fieldset>
						<legend><g:message code="quote.create.legend" default="Vehicle Details"/></legend>
                            <div class="prop mandatory ${hasErrors(bean: quoteInstance, field: 'vehicle', 'error')}">
                                <label for="vehicle">
                                    <g:message code="quote.vehicle" default="Vehicle" />
                                    <span class="indicator">*</span>
                                </label>
                                <g:select name="vehicle.id"
                                	from="${com.innovated.iris.domain.MakeModel.list()}"
                                	optionKey="id"
                                	value="${quoteInstance?.vehicle?.id}"  />
                            </div>
                            <div class="prop ${hasErrors(bean: quoteInstance, field: 'shape', 'error')}">
                                <label for="shape">
                                    <g:message code="quote.shape" default="Shape" />
                                </label>
                                <g:select name="shape" from="${quoteInstance.constraints.shape.inList}" value="${quoteInstance.shape}" valueMessagePrefix="quote.shape"  />
                            </div>
                            <div class="prop ${hasErrors(bean: quoteInstance, field: 'transmission', 'error')}">
                                <label for="transmission">
                                    <g:message code="quote.transmission" default="Transmission" />
                                </label>
                                <g:select name="transmission" from="${quoteInstance.constraints.transmission.inList}" value="${quoteInstance.transmission}" valueMessagePrefix="quote.transmission"  />
                            </div>
                            <div class="prop ${hasErrors(bean: quoteInstance, field: 'colourPreference', 'error')}">
                                <label for="colourPreference">
                                    <g:message code="quote.colourPreference" default="Colour Preference" />
                                </label>
                                <g:textField name="colourPreference" value="${fieldValue(bean: quoteInstance, field: 'colourPreference')}" />
                            </div>
					</fieldset>
					<fieldset>
						<legend><g:message code="quote.create.usage_terms.legend" default="Vehicle Usage & Lease Term"/></legend>
                            <div class="prop mandatory ${hasErrors(bean: quoteInstance, field: 'kmPerYear', 'error')}">
                                <label for="kmPerYear">
                                    <g:message code="quote.kmPerYear" default="Km Per Year" />
                                    <span class="indicator">*</span>
                                </label>
                                <g:textField name="kmPerYear" value="${fieldValue(bean: quoteInstance, field: 'kmPerYear')}" />
                            </div>
                            <div class="prop mandatory ${hasErrors(bean: quoteInstance, field: 'leaseTerm', 'error')}">
                                <label for="leaseTerm">
                                    <g:message code="quote.leaseTerm" default="Lease Term" />
                                    <span class="indicator">*</span>
                                </label>
                                <g:select name="leaseTerm" from="${[12,24,36,48,60]}" value="${quoteInstance.leaseTerm}" valueMessagePrefix="quote.leaseTerm"  />
                            </div>
                            <div style="border:1px solid #D9D8D0; padding-top:5px; background-color:#FFFDDD;" class="prop">
                                <label for="businessUsage">
                                    <g:message code="quote.businessUsage" default="Business Usage" />
                                </label>
                                <g:textField name="businessUsage" value="${fieldValue(bean: quoteInstance, field: 'businessUsage')}" />&nbsp;%
                                <a class="help-tip" href="" title="Only used for Operating Cost (Log Book) Method"></a>
                            </div>
					</fieldset>
					<!--
					<fieldset>
						<legend><g:message code="quote.create.services.legend" default="Services managed by inNovated"/></legend>
							<div class="prop">
                                <label></label>
                                <g:checkBox name="isFuel" value="${true}" />
                                <label class="chkbox" for="isFuel"><g:message code="quote.isFuel" default="Fuel" /></label>
                            </div>
                            <div class="prop">
                            	<label></label>
                                <g:checkBox name="isMaintenance" value="${true}" />
                                <label class="chkbox" for="isMaintenance"><g:message code="quote.isMaintenance" default="Maintenance" /></label>
                            </div>
                            <div class="prop">
                            	<label></label>
                                <g:checkBox name="isTyres" value="${true}" />
                                <label class="chkbox" for="isTyres"><g:message code="quote.isTyres" default="Tyres" /></label>
                            </div>
                            <div class="prop">
                            	<label></label>
                                <g:checkBox name="isOther" value="${true}" />
                                <label class="chkbox" for="isOther"><g:message code="quote.isOther" default="Etc..." /></label>
                            </div>
                    </fieldset>
                    -->
                </div>
                <div class="buttons">
                    <span class="button"><g:submitButton name="create" class="save" value="${message(code: 'create', 'default': 'Create')}" /></span>
                </div>
            </g:form>
        </div>
    </body>
</html>

