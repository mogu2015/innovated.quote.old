<%@
page import="com.innovated.iris.domain.Quote"
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <title><g:message code="quote.show" default="Show Quote" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLinkTo(dir: '')}"><g:message code="home" default="Home" /></a></span>
            <span class="menuButton"><g:link class="list" action="list"><g:message code="quote.list" default="Quote List" /></g:link></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="quote.new" default="New Quote" /></g:link></span>
        </div>
        <div class="body">
        	<g:set var="aqn" value="${formatNumber(number:quoteInstance?.id, format:message(code:'quote.number.mask'))}" />
            
            <h1><g:message code="quote.show" default="Show Quote" /></h1>
            <h3>Quote ID: ${aqn}</h3>
            <g:if test="${flash.message}">
            <div class="message"><g:message code="${flash.message}" args="${flash.args}" default="${flash.defaultMessage}" /></div>
            </g:if>
            <g:form>
                <g:hiddenField name="id" value="${quoteInstance?.id}" />
                <div class="dialog">
                    <table>
                        <tbody>
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="quote.id" default="Quote Number" />:</td>
                                <td valign="top" class="value">${aqn}</td>
                            </tr>
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="quote.customer" default="Employee" />:</td>
                                <td valign="top" class="value"><g:link controller="customer" action="show" id="${quoteInstance?.customer?.id}">${quoteInstance?.customer?.entity?.encodeAsHTML()}</g:link></td>
                            </tr>
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="quote.address" default="Address" />:</td>
                                <td valign="top" class="value"><g:link controller="address" action="show" id="${quoteInstance?.address?.id}">${quoteInstance?.address?.encodeAsHTML()}</g:link></td>
                            </tr>
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="quote.employment" default="Employer" />:</td>
                                <td valign="top" class="value"><g:link controller="company" action="show" id="${quoteInstance?.employment?.employer?.id}">${quoteInstance?.employment?.employer?.encodeAsHTML()}</g:link></td>
                            </tr>
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="quote.vehicle" default="Vehicle" />:</td>
                                <td valign="top" class="value"><g:link controller="makeModel" action="show" id="${quoteInstance?.vehicle?.id}">${quoteInstance?.vehicle?.encodeAsHTML()}</g:link></td>
                            </tr>
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="quote.transmission" default="Transmission" />:</td>
                                <td valign="top" class="value">${fieldValue(bean: quoteInstance, field: "transmission")}</td>
                            </tr>
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="quote.shape" default="Shape" />:</td>
                                <td valign="top" class="value">${fieldValue(bean: quoteInstance, field: "shape")}</td>
                            </tr>
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="quote.colourPreference" default="Colour Preference" />:</td>
                                <td valign="top" class="value">${fieldValue(bean: quoteInstance, field: "colourPreference")}</td>
                            </tr>
							<tr class="prop">
                                <td valign="top" class="name"><g:message code="quote.kmPerYear" default="Km Per Year" />:</td>
                                <td valign="top" class="value">${fieldValue(bean: quoteInstance, field: "kmPerYear")}</td>
                            </tr>
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="quote.leaseTerm" default="Lease Term" />:</td>
                                <td valign="top" class="value">${fieldValue(bean: quoteInstance, field: "leaseTerm")}</td>
                            </tr>
                            
                        </tbody>
                    </table>
                    
                    <g:set var="AMT_SCALE" value="${0}" />
                    <g:set var="PRCNT_SCALE" value="${1}" />
                    
                    <g:if test="${quoteInstance.comparisons?.size() > 1}">
                    <br/>
                    <h1>Packaging Benefit Comparison</h1>
                    <div>
                    	<table class="quote-comparisons">
                    		<thead>
				                <tr>
						       	    <td class="header" rowspan="2"><g:message code="quote.comparison.table.header.category" default="" /></td>
						       	    <g:each in="${quoteInstance.comparisons}" var="hdr">
						       	    <td class="header sub-total" style="text-align:center;">${hdr.name}</td>
						       	    </g:each>
				           	    </tr>
				            </thead>
                    		<tbody>
                    			<tr>
                    				<td class="header">&nbsp;</td>
                    				<g:each in="${quoteInstance.comparisons}" var="defrow">
						       	    <td class="header" style="text-align:center;">
						       	    	<g:if test="${defrow.isDefault}"><b><i>(Employer Default)</i></b></g:if>
							       	    <g:else>&nbsp;</g:else>
						       	    </td>
						       	    </g:each>
                    			</tr>

                    			<g:each in="${comparisonItemTypes}" var="item" status="cnt">
                    			<tr>
                    				<td class="${ (item.isTotal() || cnt==0) ? "sub-total" : (item.isSubTotal()) ? "grand-total" : "" }">${item.getName()}</td>
                    				<g:each in="${quoteInstance.comparisons}" var="comp">
						       	    <td class="numeric ${ (item.isTotal() || cnt==0) ? "sub-total" : (item.isSubTotal()) ? "grand-total" : "" } ${comp.isBest ? "best" : ""}">
						       	    	<g:set var="compItem" value="${comp.items.find{ it.type == item }}" />
						       	    	<g:if test="${compItem}">
						       	    	${formatNumber(number:compItem.amount, type:'currency', minFractionDigits:AMT_SCALE, maxFractionDigits:AMT_SCALE)}
						       	    	</g:if>
						       	    	<g:else>
						       	    	${formatNumber(number:0, type:'currency', minFractionDigits:AMT_SCALE, maxFractionDigits:AMT_SCALE)}
						       	    	</g:else>
						       	    </td>
						       	    </g:each>
                    			</tr>
                    			</g:each>

                    			<tr>
                    				<td class="grand-total benefit-amt" style="text-align:right;" colspan="2">Annual Benefit Received</td>
                    				<g:set var="noPackageNet" value="${quoteInstance.comparisons.get(0).items[-1]}" />
                    				<g:each in="${quoteInstance.comparisons}" var="diff" status="increase">
                    				<g:if test="${increase > 0}">
						       	    <g:set var="diffItem" value="${diff.items[-1]}" />
						       	    <g:set var="diffAmount" value="${(diffItem) ? (diffItem.amount - noPackageNet.amount) : 0}" />
						       	    <td class="numeric benefit-amt grand-total ${(diffAmount >= 0) ? "" : "negative-amount" }">
						       	    	${formatNumber(number:diffAmount, type:'currency', minFractionDigits:AMT_SCALE, maxFractionDigits:AMT_SCALE)}
						       	    </td>
						       	    </g:if>
						       	    </g:each>
                    			</tr>
                    			<tr>
                    				<td class="grand-total benefit-prcnt" style="text-align:right;" colspan="2">% Net Salary Increase</td>
                    				<g:set var="noPackageNet" value="${quoteInstance.comparisons.get(0).items[-1]}" />
                    				<g:each in="${quoteInstance.comparisons}" var="diff" status="increase">
                    				<g:if test="${increase > 0}">
						       	    <g:set var="diffItem" value="${diff.items[-1]}" />
						       	    <g:set var="diffAmount" value="${(diffItem) ? (diffItem.amount - noPackageNet.amount) : 0}" />
						       	    <td class="numeric benefit-prcnt grand-total ${(diffAmount >= 0) ? "" : "negative-amount" }">
						       	    	${formatNumber(number:diffAmount/noPackageNet.amount, type:'percent', minFractionDigits:PRCNT_SCALE, maxFractionDigits:PRCNT_SCALE)}
						       	    </td>
						       	    </g:if>
						       	    </g:each>
                    			</tr>
                    			<tr>
                    				<td colspan="2"></td>
                    				<g:each in="${quoteInstance.comparisons}" var="p" status="p_idx">
                    				<g:if test="${p_idx > 0}">
                    				<td class="print-quote-btn">
                    					<span class="button"><g:link action="export" params="[id: aqn + '-' + p.id + '.pdf']" target="_blank">Print</g:link></span>
                    				</td>
                    				</g:if>
						       	    </g:each>
                    			</tr>
                    			<tr>
                    				<td colspan="2"></td>
                    				<g:each in="${quoteInstance.comparisons}" var="a" status="a_idx">
                    				<g:if test="${a_idx > 0}">
                    				<td class="accept-quote-btn">Accept ${a.id}</td>
                    				</g:if>
						       	    </g:each>
                    			</tr>
                    		</tbody>
                    	</table>
                    </div>
                    </g:if>
                    
                    <g:if test="${quoteBreakdown?.size() > 0}">
                    <br/>
                    <h1>Quotation Details</h1>
                    <div>
				        <table class="breakdown">
				            <thead>
				                <tr>
						       	    <th><g:message code="quote.breakdown.table.header.category" default="Category" /></th>
						       	    <th class="numeric"><g:message code="quote.breakdown.table.header.weekly" default="Weekly" /></th>
						       	    <th class="numeric"><g:message code="quote.breakdown.table.header.fortnightly" default="Fortnightly" /></th>
						       	    <th class="numeric"><g:message code="quote.breakdown.table.header.monthly" default="Monthly" /></th>
						       	    <th class="numeric"><g:message code="quote.breakdown.table.header.annually" default="Annually" /></th>
						       	    <th><g:message code="quote.breakdown.table.header.supplier" default="Supplier" /></th>
				           	    </tr>
				            </thead>
				            
				            <g:if test="${quoteBreakdown.size() > 0}">
				            <tbody>
				            <g:set var="counter" value="${0}" />
				            <g:each in="${quoteBreakdown}" status="i" var="breakdownItem">
				                <tr class="${(counter % 2) == 0 ? 'odd' : 'even'}">
				                    <td>${breakdownItem.category}</td>
				                    <td class="numeric">${formatNumber(number:breakdownItem.weekly, type:'currency', minFractionDigits:2, maxFractionDigits:2)}</td>
				                    <td class="numeric">${formatNumber(number:breakdownItem.fortnightly, type:'currency', minFractionDigits:2, maxFractionDigits:2)}</td>
				                    <td class="numeric">${formatNumber(number:breakdownItem.monthly, type:'currency', minFractionDigits:2, maxFractionDigits:2)}</td>
				                    <td class="numeric">${formatNumber(number:breakdownItem.annually, type:'currency', minFractionDigits:2, maxFractionDigits:2)}</td>
				                    <td>${breakdownItem.supplier}</td>
				                </tr>
				                <g:set var="counter" value="${counter + 1}" />
				            </g:each>
				            </tbody>
				            </g:if>
				            
				            <g:if test="${quoteBreakdownTotals.size() > 0}">
				            <tbody>
				            <g:each in="${quoteBreakdownTotals}" status="i" var="breakdownTotal">
				                <tr class="${(counter % 2) == 0 ? 'odd' : 'even'}">
				                    <th class="${i == 0 ? "sub-total" : (i == 1) ? "gst" : "grand-total"}">${breakdownTotal.category}</th>
				                    <th class="${i == 0 ? "sub-total" : (i == 1) ? "gst" : "grand-total"} numeric">${formatNumber(number:breakdownTotal.weekly, type:'currency', minFractionDigits:2, maxFractionDigits:2)}</th>
				                    <th class="${i == 0 ? "sub-total" : (i == 1) ? "gst" : "grand-total"} numeric">${formatNumber(number:breakdownTotal.fortnightly, type:'currency', minFractionDigits:2, maxFractionDigits:2)}</th>
				                    <th class="${i == 0 ? "sub-total" : (i == 1) ? "gst" : "grand-total"} numeric">${formatNumber(number:breakdownTotal.monthly, type:'currency', minFractionDigits:2, maxFractionDigits:2)}</th>
				                    <th class="${i == 0 ? "sub-total" : (i == 1) ? "gst" : "grand-total"} numeric">${formatNumber(number:breakdownTotal.annually, type:'currency', minFractionDigits:2, maxFractionDigits:2)}</th>
				                    <th class="${i == 0 ? "sub-total" : (i == 1) ? "gst" : "grand-total"}">${breakdownTotal.supplier}</th>
				                </tr>
				                <g:set var="counter" value="${counter + 1}" />
				            </g:each>
				            </tbody>
				            </g:if>
				        
				        </table>
				    </div>
				    <br/>
				    </g:if>
				    
                </div>
                <div class="buttons">
                    <g:ifAnyGranted role="ROLE_SUPER,ROLE_ADMIN">
                    <span class="button"><g:actionSubmit class="edit" action="edit" value="${message(code: 'edit', 'default': 'Edit')}" /></span>
                    </g:ifAnyGranted>
                    <span class="button"><g:actionSubmit class="delete" action="delete" value="${message(code: 'delete', 'default': 'Delete')}" onclick="return confirm('${message(code: 'delete.confirm', 'default': 'Are you sure?')}');" /></span>
                </div>
            </g:form>
        </div>
    </body>
</html>
