<%@ page import="com.innovated.iris.domain.enums.SupplierType; com.innovated.iris.domain.Quote" %>
<script type="text/javascript">
			function findTarget(evt) { return $(evt.target).attr('id'); }
        	function onHelpClick(evt) {
        		evt.preventDefault();
				var helpTip = findTarget(evt);
				
				switch (helpTip) {
					case "vehFinAmount":
						//console.log("Help clicked for: " + helpTip);
						$("#driveAwayPrice, #purchasePriceGst").parent().children().effect("pulsate", { times:3 }, 500);
						break;
					default:
						//console.log("No help found for for: " + helpTip);
				}
				return false;
        	}
        	function enableLookup() { $("#relookup").val(true); }
			function disableLookup() { $("#relookup").val(false); }
			function removeItem(evt) {
				evt.preventDefault();
				var $tgt = $(evt.target);
				if ($tgt.is('.remove-item')) {
					$tgt.parent().remove();
				}
				else if ($tgt.is('img')) {
					$tgt.parent().parent().remove();
				}
			}
			function saveItem(evt) {
				evt.preventDefault();
				var $tgt = $(evt.target);
			}
			function addItem(parentList, evt) {
				evt.preventDefault();
				var count = $("#" + parentList + " li").size();
				var elName = parentList + "." + count;
				var txt = '';
				if ($.browser.msie()) { txt = 'Remove'; }
				$("#" + parentList).append("<li class='quote-item'> " +
					"<input id='" + elName + ".name' name='" + elName + ".name' type='text' class='item-label' value='Edit item name...' /> " +
					"<input id='" + elName + ".value' name='" + elName + ".value' type='text' class='quote-item numeric' value='0.00' /> " +
					"<span class='remove-item' title='Remove Item'><img src='${resource(dir:'images/skin',file:'delete.png')}' alt='Remove'/>&nbsp;Remove</span> " +
					"</li>"
				);
				$(".remove-item").click(removeItem);
				$(".quote-item").blur(saveItem);
				$("#" + parentList + " li:last input:first").focus().select();
			}
			
			//when toggling between percentage/value inputs, enable/disable the relevant fields
			function toggleChoice(enable, disable, evt) {
				$.each(disable.split(" "), function(index, value) {
					$("input[name=" + value + "]").addClass("uneditable").attr("readonly", "readonly");
				});
				$.each(enable.split(" "), function(index, value) {
					$("input[name=" + value + "]").removeClass("uneditable").removeAttr("readonly");
				});
			}

			// On document load
			$(document).ready(function(){
				$("#addOptionItem").click(function(evt)    { addItem("optionItems",    evt); });
				$("#addDealerItem").click(function(evt)    { addItem("dealerItems",    evt); });
				$("#addAccessoryItem").click(function(evt) { addItem("accessoryItems", evt); });
				$("#addInclGstItem").click(function(evt)   { addItem("inclGstItems",   evt); });
				$("#addNoGstItem").click(function(evt)     { addItem("noGstItems",     evt); });
				$("#addInsuranceItem").click(function(evt) { addItem("insuranceItems", evt); });
				
				toggleChoice("${quoteInstance.brokerageChoice==0 ? 'brokeragePercent' : 'brokerageVehicle brokerageOther'}", "${quoteInstance.brokerageChoice==0 ? 'brokerageVehicle brokerageOther' : 'brokeragePercent'}", null);
				$("#brokerageChoicePercentage").click(function(evt) { toggleChoice("brokeragePercent", "brokerageVehicle brokerageOther", evt); });
				$("#brokerageChoiceDollar").click(function(evt)     { toggleChoice("brokerageVehicle brokerageOther", "brokeragePercent", evt); });
				
				toggleChoice("${quoteInstance.rvChoice==0 ? 'rvPercentage' : 'rvValue'}", "${quoteInstance.rvChoice==0 ? 'rvValue' : 'rvPercentage'}", null);
				$("#rvChoicePercentage").click(function(evt) { toggleChoice("rvPercentage", "rvValue", evt); });
				$("#rvChoiceDollar").click(function(evt)     { toggleChoice("rvValue", "rvPercentage", evt); });
				
				toggleChoice("${quoteInstance.tyreChoice==0 ? 'costPerTyreStandard' : 'costPerTyrePremium'}", "${quoteInstance.tyreChoice==0 ? 'costPerTyrePremium' : 'costPerTyreStandard'}", null);
				$("#tyreChoiceStandard").click(function(evt) { toggleChoice("costPerTyreStandard", "costPerTyrePremium", evt); });
				$("#tyreChoicePremium").click(function(evt)  { toggleChoice("costPerTyrePremium", "costPerTyreStandard", evt); });
				
				$("#fuelTypeSelect").change(function() {
					if ($('#fuelTypeSelect :selected').text() == "LPG") { $("#consumptionFactor").val('1.35') }
					else { $("#consumptionFactor").val('1.0') }
				});
				$(".remove-item").click(removeItem);
				$(".help-tip").click(onHelpClick);
				$("#btnUpdate").click(function(){ $("#quote-form").submit(); });

				/*$("#quote-form").submit(function() {
					console.log("before submit...");
					return true;
				});*/
			});
        </script>
		<g:form name="quote-form" controller="quote" action="update" method="post" >
			
                <g:hiddenField id="id" name="id" value="${quoteInstance?.id}" />
                <g:hiddenField id="version" name="version" value="${quoteInstance?.version}" />
                <g:hiddenField id="relookup" name="relookup" value="false" />
                
            <table>
			<tbody>
			<tr>
			<td valign="top" style="padding-right:15px;">
                
                <div id="quote_left_col" class="dialog">
                	<fieldset class="editable">
                        <legend><g:message code="quote.edit.legend.left.confirmed" default="Pricing Confirmation"/></legend>
                        <div class="prop mandatory ${hasErrors(bean: quoteInstance, field: 'pricingConfirmed', 'error')}">
                            <label for="listPrice"><g:message code="quote.pricingConfirmed" default="Final Pricing Confirmed?:" /></label>
                            <g:checkBox name="chk_pricingConfirmed" value="${quoteInstance?.pricingConfirmed != null}" />
                            <a class="help-tip" href="" title="Check to confirm that final pricing for this quote has been received from all the relevant 3rd parties"></a>
                        </div>

                    </fieldset>
                    
                	<fieldset class="editable">
                        <legend><g:message code="quote.edit.legend.left.top" default="Vehicle Purchase Details"/></legend>
                        
                        <!-- Vehicle supplier/dealer will go here...
                        <div class="prop mandatory ${hasErrors(bean: quoteInstance, field: 'finSupplierId', 'error')}">
                            <label for="finSupplierId">
                                <g:message code="quote.finSupplierId" default="Finance Provider" />
                                <span class="indicator">*</span>
                            </label>
                            <g:select name="finSupplierId" from="${com.innovated.iris.domain.Supplier.list()}" optionKey="id" value="${quoteInstance?.finSupplierId}"  />
                            <a class="help-tip" href="#" title="finSupplierId help text here..."></a>
                        </div>
                        -->
                        <div class="sub-total prop mandatory ${hasErrors(bean: quoteInstance, field: 'listPrice', 'error')}">
                            <label for="listPrice">
                                <g:message code="quote.listPrice" default="New Vehicle (List) Price:" />
                                <span class="indicator">*</span>
                            </label>
                            <g:textField name="listPrice" class="numeric" value="${formatNumber(number:quoteInstance.listPrice, type:'number', minFractionDigits:2, maxFractionDigits:2)}" />
                            <a class="help-tip" href="#" title="listPrice help text here..."></a>
                        </div>
                        
                        <div class="prop itemListProp mandatory ${hasErrors(bean: quoteInstance, field: 'optionItems', 'error')}">
                            <label for="tmp_optionItems">
                                <g:message code="quote.optionItems" default="Options/Accessories:" />
                                <!--<span class="indicator">*</span>-->
                            </label>
                            <span id="addOptionItem" name="addOptionItem" class='add-item' title='Add Option/Accessory'><img src="${resource(dir:'images/skin',file:'add.png')}" alt="Add Option/Accessory"/>&nbsp;Add Option/Accessory</span>
                            <div class="itemListWrapper">
                            	<ol id="optionItems" class="itemList">
                            		<g:each var="item" in="${quoteInstance.optionItems}" status="i">
                            			<li class='quote-item'>
                            				<input id="optionItems.${i}.name" name='optionItems.${i}.name' type='text' class='item-label' value='${item.name}' />
                            				<input id='optionItems.${i}.value' name='optionItems.${i}.value' type='text' class='quote-item numeric' value='${formatNumber(number:item.value, type:'number', minFractionDigits:2, maxFractionDigits:2)}' />
                            				<span class='remove-item' title='Remove Item'><img src="${resource(dir:'images/skin',file:'delete.png')}" alt="Remove"/>&nbsp;Remove</span>
                            			</li>
                            		</g:each>
                            	</ol>
                            </div>
                        </div>
                        <div style="margin-bottom:5px;" class="sgl-line-below prop itemListProp mandatory ${hasErrors(bean: quoteInstance, field: 'dealerItems', 'error')}">
                            <label for="addDealerItem">
                                <g:message code="quote.dealerItems" default="Dealer/Other Charges:" />
                                <!--<span class="indicator">*</span>-->
                            </label>
                            <span id="addDealerItem" name="addDealerItem" class='add-item' title='Add Dealer/Other Charge'><img src="${resource(dir:'images/skin',file:'add.png')}" alt="Add Dealer/Other Charge"/>&nbsp;Add Dealer/Other Charge</span>
                            <div class="itemListWrapper">
                            	<ol id="dealerItems" class="itemList">
                            		<g:each var="item" in="${quoteInstance.dealerItems}" status="i">
                            			<li class='quote-item'>
                            				<input id="dealerItems.${i}.name" name='dealerItems.${i}.name' type='text' class='item-label' value='${item.name}' />
                            				<input id='dealerItems.${i}.value' name='dealerItems.${i}.value' type='text' class='quote-item numeric' value='${formatNumber(number:item.value, type:'number', minFractionDigits:2, maxFractionDigits:2)}' />
                            				<span class='remove-item' title='Remove Item'><img src="${resource(dir:'images/skin',file:'delete.png')}" alt="Remove"/>&nbsp;Remove</span>
                            			</li>
                            		</g:each>
                            	</ol>
                            </div>
                        </div>
                        
                        <div style="margin-bottom:1px; padding-bottom:1px;" class="prop mandatory ${hasErrors(bean: quoteInstance, field: 'purchasePrice', 'error')}">
                            <label for="purchasePrice">
                                <g:message code="quote.purchasePrice" default="Purchase Price:" />
                                <span class="indicator">*</span>
                            </label>
                            <g:textField name="purchasePrice" class="numeric uneditable" readonly="readonly" value="${formatNumber(number:quoteInstance.purchasePrice, type:'number', minFractionDigits:2, maxFractionDigits:2)}" />
                            <!--<a class="help-tip" href="#" title="purchasePrice help text here..."></a>-->
                        </div>
                        <div style="margin-bottom:1px; padding-bottom:1px;" class="prop mandatory ${hasErrors(bean: quoteInstance, field: 'purchasePriceGst', 'error')}">
                            <label for="purchasePriceGstAdj">
                                <g:message code="quote.purchasePriceGst" default="GST:" />
                                <span class="indicator">*</span>
                            </label>
                            <g:textField name="purchasePriceGstCalc" class="numeric uneditable" readonly="readonly" value="${formatNumber(number:quoteInstance.purchasePriceGstCalc, type:'number', minFractionDigits:2, maxFractionDigits:2)}" />
                            <g:textField name="purchasePriceGstAdj" class="numeric" value="${formatNumber(number:quoteInstance.purchasePriceGstAdj, type:'number', minFractionDigits:2, maxFractionDigits:2)}" />&nbsp;adj
                            <!--<a class="help-tip" href="#" title="purchasePriceGst help text here..."></a>-->
                        </div>
                        <div class="sgl-line-below special prop mandatory ${hasErrors(bean: quoteInstance, field: 'purchasePriceGst', 'error')}">
                            <label for="purchasePriceGst" style="text-align: right;">
                                <g:message code="quote.purchasePriceGstEffective" default="Effective GST" />
                            </label>
                            <g:textField name="purchasePriceGst" class="special numeric uneditable" readonly="readonly" value="${formatNumber(number:quoteInstance.purchasePriceGst, type:'number', minFractionDigits:2, maxFractionDigits:2)}" />
                            <!--<a class="help-tip" href="#" title="purchasePriceGst help text here..."></a>-->
                        </div>
                        
                        <div class="sub-total sgl-line-below prop mandatory">
                            <label for="purchaseTotal">
                                <g:message code="quote.purchaseTotal" default="Purchase Total" />
                                
                            </label>
                            <g:textField name="purchaseTotal" class="numeric uneditable" readonly="readonly" value="${formatNumber(number:quoteInstance.purchasePrice + quoteInstance.purchasePriceGst, type:'number', minFractionDigits:2, maxFractionDigits:2)}" />
                            <!--<a class="help-tip" href="#" title="purchaseTotal help text here..."></a>-->
                        </div>
                        
                        <div style="margin-bottom:1px; padding-bottom:1px;" class="prop mandatory ${hasErrors(bean: quoteInstance, field: 'luxuryTax', 'error')}">
                            <label for="luxuryTaxAdj">
                                <g:message code="quote.luxuryTax" default="Luxury Car Tax (LCT)" />
                                <span class="indicator">*</span>
                            </label>
                            <g:textField name="luxuryTaxCalc" class="numeric uneditable" readonly="readonly" value="${formatNumber(number:quoteInstance.luxuryTaxCalc, type:'number', minFractionDigits:2, maxFractionDigits:2)}" />
                            <g:textField name="luxuryTaxAdj" class="numeric" value="${formatNumber(number:quoteInstance.luxuryTaxAdj, type:'number', minFractionDigits:2, maxFractionDigits:2)}" />&nbsp;adj
                            <!--<a class="help-tip" href="#" title="luxuryTax help text here..."></a>-->
                        </div>
                        <div class="sgl-line-below special prop mandatory ${hasErrors(bean: quoteInstance, field: 'luxuryTax', 'error')}">
                            <label for="luxuryTax" style="text-align: right;">
                                <g:message code="quote.luxuryTaxEffective" default="Effective LCT" />
                            </label>
                            <g:textField name="luxuryTax" class="special numeric uneditable" readonly="readonly" value="${formatNumber(number:quoteInstance.luxuryTax, type:'number', minFractionDigits:2, maxFractionDigits:2)}" />
                            <!--<a class="help-tip" href="#" title="luxuryTax help text here..."></a>-->
                        </div>
                        
                        <div style="margin-bottom:5px;" class="sgl-line-below prop itemListProp mandatory ${hasErrors(bean: quoteInstance, field: 'accessoryItems', 'error')}">
                            <label for="addAccessoryItem">
                                <g:message code="quote.accessoryItems" default="Non-Dealer Accessories (incl. GST)" />
                                <!--<span class="indicator">*</span>-->
                            </label>
                            <span id="addAccessoryItem" name="addAccessoryItem" class='add-item' title='Add accessory (incl. GST)'><img src="${resource(dir:'images/skin',file:'add.png')}" alt="Add other cost (incl. GST)"/>&nbsp;Add accessory (incl. GST)</span>
                            <div class="itemListWrapper">
                            	<ol id="accessoryItems" class="itemList">
                            		<g:each var="item" in="${quoteInstance.accessoryItems}" status="i">
                            			<li class='quote-item'>
                            				<input id="accessoryItems.${i}.name" name='accessoryItems.${i}.name' type='text' class='item-label' value='${item.name}' />
                            				<input id='accessoryItems.${i}.value' name='accessoryItems.${i}.value' type='text' class='quote-item numeric' value='${formatNumber(number:item.value, type:'number', minFractionDigits:2, maxFractionDigits:2)}' />
                            				<span class='remove-item' title='Remove Item'><img src="${resource(dir:'images/skin',file:'delete.png')}" alt="Remove"/>&nbsp;Remove</span>
                            			</li>
                            		</g:each>
                            	</ol>
                            </div>
                        </div>
                        
                        <div class="sgl-line-below special prop mandatory ${hasErrors(bean: quoteInstance, field: 'fbtBaseValue', 'error')}">
                            <label for="fbtBaseValue">
                                (<g:message code="quote.fbtBaseValue" default="FBT Base Value:" />
                                <span class="indicator">*</span>
                            </label>
                            <g:textField name="fbtBaseValue" class="special numeric uneditable" readonly="readonly" value="${formatNumber(number:quoteInstance.fbtBaseValue, type:'number', minFractionDigits:2, maxFractionDigits:2)}" />
                            <!--<a class="help-tip" href="#" title="fbtBaseValue help text here..."></a>-->)
                        </div>
                        
                        <div class="prop itemListProp mandatory ${hasErrors(bean: quoteInstance, field: 'inclGstItems', 'error')}">
                            <label for="addInclGstItem">
                                <g:message code="quote.inclGstItems" default="Other costs (incl. GST):" />
                                <!--<span class="indicator">*</span>-->
                            </label>
                            <span id="addInclGstItem" name="addInclGstItem" class='add-item' title='Add other cost (incl. GST)'><img src="${resource(dir:'images/skin',file:'add.png')}" alt="Add other cost (incl. GST)"/>&nbsp;Add other cost (incl. GST)</span>
                            <div class="itemListWrapper">
                            	<ol id="inclGstItems" class="itemList">
                            		<g:each var="item" in="${quoteInstance.inclGstItems}" status="i">
                            			<li class='quote-item'>
                            				<input id="inclGstItems.${i}.name" name='inclGstItems.${i}.name' type='text' class='item-label' value='${item.name}' />
                            				<input id='inclGstItems.${i}.value' name='inclGstItems.${i}.value' type='text' class='quote-item numeric' value='${formatNumber(number:item.value, type:'number', minFractionDigits:2, maxFractionDigits:2)}' />
                            				<span class='remove-item' title='Remove Item'><img src="${resource(dir:'images/skin',file:'delete.png')}" alt="Remove"/>&nbsp;Remove</span>
                            			</li>
                            		</g:each>
                            	</ol>
                            </div>
                        </div>
                        
                        <div style="margin-bottom:5px;" class="dbl-line-below prop itemListProp mandatory ${hasErrors(bean: quoteInstance, field: 'noGstItems', 'error')}">
                            <label for="addNoGstItem">
                                <g:message code="quote.noGstItems" default="Other costs (excl. GST):" />
                                <!--<span class="indicator">*</span>-->
                            </label>
                            <span id="addNoGstItem" name="addNoGstItem" class='add-item' title='Add other cost (excl. GST)'><img src="${resource(dir:'images/skin',file:'add.png')}" alt="Add other cost (excl. GST)"/>&nbsp;Add other cost (excl. GST)</span>
                            <div class="itemListWrapper">
                            	<ol id="noGstItems" class="itemList">
                            		<g:each var="item" in="${quoteInstance.noGstItems}" status="i">
                            			<li class='quote-item'>
                            				<input id="noGstItems.${i}.name" name='noGstItems.${i}.name' type='text' class='item-label' value='${item.name}' />
                            				<input id='noGstItems.${i}.value' name='noGstItems.${i}.value' type='text' class='quote-item numeric' value='${formatNumber(number:item.value, type:'number', minFractionDigits:2, maxFractionDigits:2)}' />
                            				<span class='remove-item' title='Remove Item'><img src="${resource(dir:'images/skin',file:'delete.png')}" alt="Remove"/>&nbsp;Remove</span>
                            			</li>
                            		</g:each>
                            	</ol>
                            </div>
                        </div>
                        
                        <div class="grand-total thk-line-below prop mandatory ${hasErrors(bean: quoteInstance, field: 'driveAwayPrice', 'error')}">
                            <label for="driveAwayPrice">
                                <g:message code="quote.driveAwayPrice" default="Drive Away Price:" />
                                <span class="indicator">*</span>
                            </label>
                            <g:textField name="driveAwayPrice" class="numeric uneditable" readonly="readonly" value="${formatNumber(number:quoteInstance.driveAwayPrice, type:'number', minFractionDigits:2, maxFractionDigits:2)}" />
                            <!--<a class="help-tip" href="#" title="driveAwayPrice help text here..."></a>-->
                        </div>
                        
                        <div class="prop mandatory ${hasErrors(bean: quoteInstance, field: 'vehFinAmountInclGst', 'error')}">
                            <label for="vehFinAmountInclGst">
                                <g:message code="quote.vehFinAmountInclGst" default="Financed Amount Includes GST?:" />
                            </label>
                            <g:checkBox name="vehFinAmountInclGst" value="${quoteInstance?.vehFinAmountInclGst}" />
                            <a id="vehFinAmountInclGst" class="help-tip" href="" title="Does Financed Amount Include GST? ie. Has the vehicle been obtained privately or from a dealer?"></a>
                        </div>
                        
                        <div class="prop mandatory ${hasErrors(bean: quoteInstance, field: 'vehFinAmount', 'error')}">
                            <label for="vehFinAmount">
                                <g:message code="quote.vehFinAmount" default="Financed Amount (Vehicle Only):" />
                            </label>
                            <g:textField name="vehFinAmount" class="numeric" value="${formatNumber(number:quoteInstance.vehFinAmount, type:'number', minFractionDigits:2, maxFractionDigits:2)}" />
                            <a id="vehFinAmount" class="help-tip" href="" title="Financed Amount (Vehicle Only) = Driveaway Price - GST (NOTE: GST capped to $5198.18!)"></a>
                        </div>
                        
                        <!--
                        <div class="prop ${hasErrors(bean: quoteInstance, field: 'lctThreshold', 'error')}">
                            <label for="lctThreshold"><g:message code="quote.lctThreshold" default="Lower LCT Amount" /></label>
                            <g:textField name="lctThreshold" class="numeric" value="${formatNumber(number:quoteInstance.lctThreshold, type:'number', minFractionDigits:2, maxFractionDigits:2)}" />
                        </div>
                        <div class="prop ${hasErrors(bean: quoteInstance, field: 'lctThreshold', 'error')}">
                            <label for="gstClaimThreshold"><g:message code="quote.gstClaimThreshold" default="Max GST claimable" /></label>
                            <g:textField name="gstClaimThreshold" class="numeric" value="${formatNumber(number:quoteInstance.lctThreshold/11, type:'number', minFractionDigits:2, maxFractionDigits:2)}" />
                        </div>
                        <div class="prop ${hasErrors(bean: quoteInstance, field: 'psdAmount', 'error')}">
                            <label for="psdAmount">
                                <g:message code="quote.psdAmount" default="PSD Amount" />
                            </label>
                            <g:textField name="psdAmount" class="numeric" value="${formatNumber(number:quoteInstance.psdAmount, type:'number', minFractionDigits:2, maxFractionDigits:2)}" />
                            <a class="help-tip" href="#" title="psdAmount help text here..."></a>
                        </div>
                        -->
                        
                    </fieldset>
                    
                    <fieldset class="editable">
                        <legend><g:message code="quote.edit.legend.left" default="Financed Items"/></legend>
                        <h3 class="form-section"><g:message code="quote.edit.financed.insur.heading" default="Financed Items (Insurances/Other)"/></h3>
                        
                        <div style="margin-bottom:5px;" class="dbl-line-below prop itemListProp mandatory ${hasErrors(bean: quoteInstance, field: 'noGstItems', 'error')}">
                            <label for="addInsuranceItem">
                                <g:message code="quote.insuranceItems" default="Add financed items:" />
                            </label>
                            <span id="addInsuranceItem" name="addInsuranceItem" class='add-item' title='Add financed item'><img src="${resource(dir:'images/skin',file:'add.png')}" alt="Add financed item"/>&nbsp;Add financed item</span>
                            <div class="itemListWrapper">
                            	<ol id="insuranceItems" class="itemList">
                            		<g:each var="item" in="${quoteInstance.insuranceItems}" status="i">
                            			<li class='quote-item'>
                            				<!--<g:checkBox name="myCheckbox" value="${true}" />-->
                            				<input id="insuranceItems.${i}.name" name='insuranceItems.${i}.name' type='text' class='item-label' value='${item.name}' />
                            				<input id='insuranceItems.${i}.value' name='insuranceItems.${i}.value' type='text' class='quote-item numeric' value='${formatNumber(number:item.value, type:'number', minFractionDigits:2, maxFractionDigits:2)}' />
                            				<span class='remove-item' title='Remove Item'><img src="${resource(dir:'images/skin',file:'delete.png')}" alt="Remove"/>&nbsp;Remove</span>
                            			</li>
                            		</g:each>
                            	</ol>
                            </div>
                        </div>
                        <div class="grand-total thk-line-below prop mandatory ${hasErrors(bean: quoteInstance, field: 'insuranceItemsTotal', 'error')}">
                            <label for="insuranceItemsTotal">
                                <g:message code="quote.insuranceItemsTotal" default="Financed Items Total:" />
                            </label>
                            <g:textField name="insuranceItemsTotal" class="numeric" value="${formatNumber(number:quoteInstance.insuranceItemsTotal, type:'number', minFractionDigits:2, maxFractionDigits:2)}" />
                        </div>
					</fieldset>
					
					<fieldset class="editable">
                        <legend><g:message code="quote.edit.legend.left" default="Brokerage & Fees"/></legend>
                        <h3 class="form-section"><g:message code="quote.edit.brokerage.heading" default="Brokerage"/></h3>
                        
                        <div class="prop mandatory ${hasErrors(bean: quoteInstance, field: 'brokeragePercent', 'error')}">
                            <label for="brokeragePercent" class="radio-choice-top">
                                % Percentage&nbsp;<g:radio id="brokerageChoicePercentage" name="brokerageChoice" value="0" checked="${quoteInstance.brokerageChoice==0}" />
                            </label>
                            <g:textField name="brokeragePercent" class="numeric" value="${formatNumber(number:quoteInstance.brokeragePercent, type:'number', minFractionDigits:2, maxFractionDigits:4)}" />&nbsp;%
                            <!--<a class="help-tip" href="#" title="rvPercentage help text here..."></a>-->
                        </div>
                        <div style="margin-bottom:1px; padding-bottom:1px;" class="prop mandatory ${hasErrors(bean: quoteInstance, field: 'brokerageVehicle', 'error')}">
                            <label for="brokerageVehicle" class="radio-choice">
                                $ Dollar&nbsp;<g:radio id="brokerageChoiceDollar" name="brokerageChoice" value="1" checked="${quoteInstance.brokerageChoice==1}" />
                            </label>
                            <g:textField name="brokerageVehicle" class="numeric" value="${formatNumber(number:quoteInstance.brokerageVehicle, type:'number', minFractionDigits:2, maxFractionDigits:2)}" />&nbsp;Vehicle Only
                            <!--<a class="help-tip" href="#" title="rvValue help text here..."></a>-->
                        </div>
                        <div class="dbl-line-below prop mandatory ${hasErrors(bean: quoteInstance, field: 'brokerageOther', 'error')}">
                            <label class="radio-choice">&nbsp;</label>
                            <g:textField name="brokerageOther" class="numeric" value="${formatNumber(number:quoteInstance.brokerageOther, type:'number', minFractionDigits:2, maxFractionDigits:2)}" />&nbsp;Insurances/Other
                            <!--<a class="help-tip" href="#" title="rvValue help text here..."></a>-->
                        </div>
                        <div class="grand-total thk-line-below prop mandatory ${hasErrors(bean: quoteInstance, field: 'brokerageTotal', 'error')}">
                            <label for="brokerageTotal">
                                <g:message code="quote.brokerageTotal" default="Brokerage Total:" />
                            </label>
                            <g:textField name="brokerageTotal" class="numeric uneditable" readonly="readonly" value="${formatNumber(number:quoteInstance.brokerageTotal, type:'number', minFractionDigits:2, maxFractionDigits:2)}" />
                        </div>
                        <br/>
                        <h3 class="form-section"><g:message code="quote.edit.fees.heading" default="Annual Fees (excl GST)"/></h3>
                        <div style="margin-bottom:1px; padding-bottom:1px;" class="prop mandatory ${hasErrors(bean: quoteInstance, field: 'annualFeeFltMng', 'error')}">
                            <label for="annualFeeFltMng">
                                <g:message code="quote.annualFeeFltMng" default="Fleet Management" />
                                <span class="indicator">*</span>
                            </label>
                            <g:textField name="annualFeeFltMng" class="numeric" value="${formatNumber(number:quoteInstance.annualFeeFltMng, type:'number', minFractionDigits:2, maxFractionDigits:2)}" />
                            <!--<a class="help-tip" href="#" title="annualFeeFltMng help text here..."></a>-->
                        </div>
                        <div style="margin-bottom:1px; padding-bottom:1px;" class="prop mandatory ${hasErrors(bean: quoteInstance, field: 'annualFeeFbtRep', 'error')}">
                            <label for="annualFeeFbtRep">
                                <g:message code="quote.annualFeeFbtRep" default="FBT Reporting" />
                                <span class="indicator">*</span>
                            </label>
                            <g:textField name="annualFeeFbtRep" class="numeric" value="${formatNumber(number:quoteInstance.annualFeeFbtRep, type:'number', minFractionDigits:2, maxFractionDigits:2)}" />
                            <!--<a class="help-tip" href="#" title="annualFeeFbtRep help text here..."></a>-->
                        </div>
                        <div style="margin-bottom:1px; padding-bottom:1px;" class="prop mandatory ${hasErrors(bean: quoteInstance, field: 'annualFeeAdmin', 'error')}">
                            <label for="annualFeeAdmin">
                                <g:message code="quote.annualFeeAdmin" default="Administration" />
                                <span class="indicator">*</span>
                            </label>
                            <g:textField name="annualFeeAdmin" class="numeric" value="${formatNumber(number:quoteInstance.annualFeeAdmin, type:'number', minFractionDigits:2, maxFractionDigits:2)}" />
                            <!--<a class="help-tip" href="#" title="annualFeeAdmin help text here..."></a>-->
                        </div>
                        <div class="dbl-line-below prop mandatory ${hasErrors(bean: quoteInstance, field: 'annualFeeBankChg', 'error')}">
                            <label for="annualFeeBankChg">
                                <g:message code="quote.annualFeeBankChg" default="Banking Charges" />
                                <span class="indicator">*</span>
                            </label>
                            <g:textField name="annualFeeBankChg" class="numeric" value="${formatNumber(number:quoteInstance.annualFeeBankChg, type:'number', minFractionDigits:2, maxFractionDigits:2)}" />
                            <!--<a class="help-tip" href="#" title="annualFeeBankChg help text here..."></a>-->
                        </div>
                        <div class="grand-total thk-line-below prop">
                            <label for="annualFeesTotal">
                                <g:message code="quote.annualFeesTotal" default="Annual Fees Total:" />
                            </label>
                            <g:textField name="annualFeesTotal" class="numeric uneditable" readonly="readonly" value="${formatNumber(number:quoteInstance.annualFeeFltMng+quoteInstance.annualFeeFbtRep+quoteInstance.annualFeeAdmin+quoteInstance.annualFeeBankChg, type:'number', minFractionDigits:2, maxFractionDigits:2)}" />
                        </div>
                    </fieldset>
                    
                    <fieldset class="editable">
                        <legend><g:message code="quote.edit.legend.left" default="Budget - Finance"/></legend>
                        
                        <h3 class="form-section"><g:message code="quote.edit.rv.heading" default="Residual"/></h3>
                        
                        <div style="margin-bottom:1px; padding-bottom:1px;" class="prop mandatory ${hasErrors(bean: quoteInstance, field: 'rvPercentage', 'error')}">
                            <label for="rvPercentage" class="radio-choice-top">
                                % Percentage&nbsp;<g:radio id="rvChoicePercentage" name="rvChoice" value="0" checked="${quoteInstance.rvChoice==0}" />
                            </label>
                            <g:textField name="rvPercentage" class="numeric" value="${formatNumber(number:quoteInstance.rvPercentage, type:'number', minFractionDigits:2, maxFractionDigits:4)}" />&nbsp;%
                            <!--<a class="help-tip" href="" title="rvPercentage help text here..."></a>-->
                        </div>
                        <div class="prop mandatory ${hasErrors(bean: quoteInstance, field: 'rvValue', 'error')}">
                            <label for="rvValue" class="radio-choice">
                                $ Dollar&nbsp;<g:radio id="rvChoiceDollar" name="rvChoice" value="1" checked="${quoteInstance.rvChoice==1}" />
                            </label>
                            <g:textField name="rvValue" class="numeric" value="${formatNumber(number:quoteInstance.rvValue, type:'number', minFractionDigits:2, maxFractionDigits:2)}" />
                            <a class="help-tip" href="" title="NOTE: This value has been rounded up to the nearest whole dollar."></a>
                        </div>
                        
                        <div style="margin-bottom:1px; padding-bottom:1px;" class="prop mandatory ${hasErrors(bean: quoteInstance, field: 'rvUseAltValue', 'error')}">
                            <label for="rvUseAltValue">
                                <g:message code="quote.rvUseAltValue" default="Use alternative residual?:" />
                            </label>
                            <g:checkBox id="rvUseAltValue" name="rvUseAltValue" value="${quoteInstance?.rvUseAltValue}" />
                            <!--<a class="help-tip" href="#" title="rvUseAltValue help text here..."></a>-->
                        </div>
                        <div class="prop ${hasErrors(bean: quoteInstance, field: 'rvAltValue', 'error')}">
                            <label for="rvAltValue">
                                <g:message code="quote.rvAltValue" default="Alternative Residual:" />
                            </label>
                            <g:textField id="rvAltValue" name="rvAltValue" class="numeric" value="${formatNumber(number:quoteInstance.rvAltValue, type:'number', minFractionDigits:2, maxFractionDigits:2)}" />
                            <a class="help-tip" href="" title="NOTE: This value has been rounded up to the nearest whole dollar."></a>
                        </div>
                        
                        <h3 class="form-section"><g:message code="quote.edit.finance.heading" default="Finance"/></h3>
                        <div style="margin-bottom:1px; padding-bottom:1px;" class="prop mandatory ${hasErrors(bean: quoteInstance, field: 'finSupplierId', 'error')}">
                            <label for="finSupplierId">
                                <g:message code="quote.finSupplierId" default="Finance Provider" />
                                <span class="indicator">*</span>
                            </label>
                            <g:select class="non-numeric" name="finSupplierId" from="${com.innovated.iris.domain.Supplier.findAllByType(SupplierType.get(3))}" optionKey="id" value="${quoteInstance?.finSupplierId}"  />
                            <!--<a class="help-tip" href="#" title="finSupplierId help text here..."></a>-->
                        </div>
                        <div style="margin-bottom:1px; padding-bottom:1px;" class="prop mandatory ${hasErrors(bean: quoteInstance, field: 'finInterestRate', 'error')}">
                            <label for="finInterestRate">
                                <g:message code="quote.finInterestRate" default="Interest Rate" />
                                <span class="indicator">*</span>
                            </label>
                            <g:textField name="finInterestRate" class="numeric" value="${formatNumber(number:quoteInstance.finInterestRate, type:'number', minFractionDigits:2, maxFractionDigits:8)}" />
                            <!--<a class="help-tip" href="#" title="finInterestRate help text here..."></a>-->
                        </div>
                        <div style="margin-bottom:1px; padding-bottom:1px;" class="prop mandatory ${hasErrors(bean: quoteInstance, field: 'finDocFee', 'error')}">
                            <label for="finDocFee">
                                <g:message code="quote.finDocFee" default="Documentation Fee" />
                                <span class="indicator">*</span>
                            </label>
                            <g:textField name="finDocFee" class="numeric" value="${formatNumber(number:quoteInstance.finDocFee, type:'number', minFractionDigits:2, maxFractionDigits:2)}" />
                            <!--<a class="help-tip" href="#" title="finDocFee help text here..."></a>-->
                        </div>
                        <div style="margin-bottom:1px; padding-bottom:1px;" class="prop mandatory ${hasErrors(bean: quoteInstance, field: 'finNumDeferredPayments', 'error')}">
                            <label for="finNumDeferredPayments">
                                <g:message code="quote.finNumDeferredPayments" default="# of Initial Deferred Payments:" />
                                <span class="indicator">*</span>
                            </label>
                            <g:textField class="numeric" name="finNumDeferredPayments" value="${fieldValue(bean: quoteInstance, field: 'finNumDeferredPayments')}" />
                        </div>
                        <div class="prop mandatory ${hasErrors(bean: quoteInstance, field: 'finPmtInAdvance', 'error')}">
                            <label for="finPmtInAdvance">
                                <g:message code="quote.finPmtInAdvance" default="Are Payments Made In Advance?" />
                                <span class="indicator">*</span>
                            </label>
                            <g:checkBox name="finPmtInAdvance" value="${quoteInstance?.finPmtInAdvance}" />
                            <!--<a class="help-tip" href="#" title="finPmtInAdvance help text here..."></a>-->
                        </div>
                        
                        <h3 class="form-section"><g:message code="quote.edit.finance.payments" default="Finance - Monthly Payments"/></h3>
                        <div style="margin-bottom:1px; padding-bottom:1px;" class="prop mandatory ${hasErrors(bean: quoteInstance, field: 'finVehMthlyPymt', 'error')}">
                            <label for="finVehMthlyPymt">
                                <g:message code="quote.finVehMthlyPymt" default="Vehicle Finance:" />
                            </label>
                            <g:textField name="finVehMthlyPymt" class="numeric uneditable" readonly="readonly" value="${formatNumber(number:quoteInstance.finVehMthlyPymt, type:'number', minFractionDigits:2, maxFractionDigits:2)}" />
                            <g:textField name="finMthlyPymtAdj" class="numeric" value="${formatNumber(number:quoteInstance.finMthlyPymtAdj, type:'number', minFractionDigits:2, maxFractionDigits:2)}" />&nbsp;adj
                        </div>
                        <div class="sgl-line-below prop mandatory ${hasErrors(bean: quoteInstance, field: 'finInsurMthlyPymt', 'error')}">
                            <label for="finInsurMthlyPymt">
                                <g:message code="quote.finInsurMthlyPymt" default="Financed Insurance(s):" />
                            </label>
                            <g:textField name="finInsurMthlyPymt" class="numeric uneditable" readonly="readonly" value="${formatNumber(number:quoteInstance.finInsurMthlyPymt, type:'number', minFractionDigits:2, maxFractionDigits:2)}" />
                        </div>
                        <div style="margin-bottom:1px; padding-bottom:1px;" class="sub-total prop mandatory ${hasErrors(bean: quoteInstance, field: 'finMthlyPymt', 'error')}">
                            <label for="finMthlyPymt">
                                <g:message code="quote.finMthlyPymt" default="Monthly Payment:" />
                            </label>
                            <g:textField name="finMthlyPymt" class="numeric uneditable" readonly="readonly" value="${formatNumber(number:quoteInstance.finMthlyPymt, type:'number', minFractionDigits:2, maxFractionDigits:2)}" />
                        </div>
                        <div class="dbl-line-below prop mandatory ${hasErrors(bean: quoteInstance, field: 'finMthlyPymtGst', 'error')}">
                            <label for="finMthlyPymtGst">
                                <g:message code="quote.finMthlyPymtGst" default="GST:" />
                            </label>
                            <g:textField name="finMthlyPymtGst" class="numeric uneditable" readonly="readonly" value="${formatNumber(number:quoteInstance.finMthlyPymtGst, type:'number', minFractionDigits:2, maxFractionDigits:2)}" />
                        </div>
                        <div class="grand-total thk-line-below prop mandatory">
                            <label for="finMthlyPymtTotal">
                                <g:message code="quote.finMthlyPymtTotal" default="Total Monthly Rental:" />
                            </label>
                            <g:textField name="finMthlyPymtTotal" class="numeric uneditable" readonly="readonly" value="${formatNumber(number:quoteInstance.finMthlyPymt+quoteInstance.finMthlyPymtGst, type:'number', minFractionDigits:2, maxFractionDigits:2)}" />
                        </div>
                        
                        <!--
                        <div class="prop mandatory ${hasErrors(bean: quoteInstance, field: 'finAdjAmt', 'error')}">
                            <label for="finAdjAmt">
                                <g:message code="quote.finAdjAmt" default="Adjustments" />
                                <span class="indicator">*</span>
                            </label>
                            <g:textField name="finAdjAmt" class="numeric" value="${formatNumber(number:quoteInstance.finAdjAmt, type:'number', minFractionDigits:2, maxFractionDigits:2)}" />
                            <a class="help-tip" href="#" title="finAdjAmt help text here..."></a>
                        </div>
                        <div class="prop mandatory ${hasErrors(bean: quoteInstance, field: 'finAmount', 'error')}">
                            <label for="finAmount">
                                <g:message code="quote.finAmount" default="Financed Amount" />
                            </label>
                            <g:textField name="finAmount" class="numeric uneditable" readonly="readonly" value="${formatNumber(number:quoteInstance.finAmount, type:'number', minFractionDigits:2, maxFractionDigits:2)}" />
                            <a class="help-tip" href="#" title="finAmount help text here..."></a>
                        </div>
                        -->
					</fieldset>
					
					<fieldset class="editable">
                        <legend><g:message code="quote.edit.legend.left" default="Budget - Maintenance"/></legend>
                        <h3 class="form-section"><g:message code="quote.edit.maintenance.heading" default="Maintenance"/></h3>
                        <div style="margin-bottom:1px; padding-bottom:1px;" class="prop mandatory ${hasErrors(bean: quoteInstance, field: 'maintCostPer1000km', 'error')}">
                            <label for="maintCostPer1000km">
                                <g:message code="quote.maintCostPer1000km" default="Cost Per 1,000km" />
                                <span class="indicator">*</span>
                            </label>
                            <g:textField name="maintCostPer1000km" class="numeric" value="${formatNumber(number:quoteInstance.maintCostPer1000km, type:'number', minFractionDigits:2, maxFractionDigits:2)}" />
                            <!--<a class="help-tip" href="#" title="maintCostPer1000km help text here..."></a>-->
                        </div>
                        <div class="dbl-line-below prop ${hasErrors(bean: quoteInstance, field: 'maintKmAdj', 'error')}">
                            <label for="maintKmAdj">
                                <g:message code="quote.maintKmAdj" default="KM Adjustment" />
                            </label>
                            <g:textField class="numeric" name="maintKmAdj" value="${fieldValue(bean: quoteInstance, field: 'maintKmAdj')}" /> km
                            <!--<a class="help-tip" href="#" title="maintKmAdj help text here..."></a>-->
                        </div>
                        <div class="grand-total thk-line-below prop mandatory ${hasErrors(bean: quoteInstance, field: 'annualMaintenance', 'error')}">
                            <label for="annualMaintenance">
                                <g:message code="quote.annualMaintenance" default="Annual Maintenance:" />
                            </label>
                            <g:textField name="annualMaintenance" class="numeric uneditable" readonly="readonly" value="${formatNumber(number:quoteInstance.annualMaintenance, type:'number', minFractionDigits:2, maxFractionDigits:2)}" />
                        </div>
                    </fieldset>
                    
                    <fieldset class="editable">
                        <legend><g:message code="quote.edit.legend.left" default="Budget - Tyres"/></legend>
                        <h3 class="form-section"><g:message code="quote.edit.tyres.heading" default="Tyres"/></h3>
                        <div style="margin-bottom:1px; padding-bottom:1px;" class="prop mandatory ${hasErrors(bean: quoteInstance, field: 'costPerTyreStandard', 'error')}">
                            <label for="costPerTyreStandard" class="radio-choice-top">
                                Cost Per Tyre (Standard)&nbsp;<g:radio id="tyreChoiceStandard" name="tyreChoice" value="0" checked="${quoteInstance.tyreChoice==0}" />
                            </label>
                            <g:textField name="costPerTyreStandard" class="numeric" value="${formatNumber(number:quoteInstance.costPerTyreStandard, type:'number', minFractionDigits:2, maxFractionDigits:2)}" />
                        </div>
                        <div class="prop mandatory ${hasErrors(bean: quoteInstance, field: 'costPerTyrePremium', 'error')}">
                            <label for="costPerTyrePremium" class="radio-choice">
                                Cost Per Tyre (Premium)&nbsp;<g:radio id="tyreChoicePremium" name="tyreChoice" value="1" checked="${quoteInstance.tyreChoice==1}" />
                            </label>
                            <g:textField name="costPerTyrePremium" class="numeric" value="${formatNumber(number:quoteInstance.costPerTyrePremium, type:'number', minFractionDigits:2, maxFractionDigits:2)}" />
                        </div>
                        <div class="dbl-line-below prop mandatory ${hasErrors(bean: quoteInstance, field: 'numTyres', 'error')}">
                            <label for="numTyres">
                                <g:message code="quote.numTyres" default="# Tyres over lease term" />
                                <span class="indicator">*</span>
                            </label>
                            <g:textField name="numTyres" class="numeric" value="${fieldValue(bean: quoteInstance, field: 'numTyres')}" />
                            <!--<a class="help-tip" href="#" title="numTyres help text here..."></a>-->
                        </div>
                        <div class="grand-total thk-line-below prop mandatory ${hasErrors(bean: quoteInstance, field: 'annualTyres', 'error')}">
                            <label for="annualTyres">
                                <g:message code="quote.annualTyres" default="Annual Tyres:" />
                            </label>
                            <g:textField name="annualTyres" class="numeric uneditable" readonly="readonly" value="${formatNumber(number:quoteInstance.annualTyres, type:'number', minFractionDigits:2, maxFractionDigits:2)}" />
                        </div>
                    </fieldset>
                    
                    <fieldset class="editable">
                        <legend><g:message code="quote.edit.legend.left" default="Budget - Fuel"/></legend>
                        <h3 class="form-section"><g:message code="quote.edit.fuel.heading" default="Fuel"/></h3>
                        <div style="margin-bottom:1px; padding-bottom:1px;" class="prop mandatory ${hasErrors(bean: quoteInstance, field: 'fuelSupplierId', 'error')}">
                            <label for="fuelSupplierId">
                                <g:message code="quote.fuelSupplierId" default="Fuel Provider" />
                                <span class="indicator">*</span>
                            </label>
                            <g:select class="non-numeric" name="fuelSupplierId" from="${com.innovated.iris.domain.Supplier.findAllByTypeInList(SupplierType.getAll([17, 32, 36]))}" optionKey="id" value="${quoteInstance?.fuelSupplierId}"  />
                            <!--<a class="help-tip" href="" title="fuelSupplierId help text here..."></a>-->
                        </div>
                        <div style="margin-bottom:1px; padding-bottom:1px;" class="prop mandatory ${hasErrors(bean: quoteInstance, field: 'fuelTypeUsed', 'error')}">
                            <label for="fuelTypeUsed">
                                <g:message code="quote.fuelTypeUsed" default="Fuel Type Used" />
                                <span class="indicator">*</span>
                            </label>
                            <g:select id="fuelTypeSelect" class="non-numeric" name="fuelTypeUsed.id" from="${com.innovated.iris.domain.enums.FuelType.list()}" optionKey="id" value="${quoteInstance?.fuelTypeUsed?.id}"  />
                            <!--<a class="help-tip" href="" title="fuelTypeUsed help text here..."></a>-->
                        </div>
                        <div style="margin-bottom:1px; padding-bottom:1px;" class="prop mandatory ${hasErrors(bean: quoteInstance, field: 'vehicle.fuelConsumptionCombined', 'error')}">
                            <label for="vehicle.fuelConsumptionCombined">
                                <g:message code="quote.vehicle.fuelConsumptionCombined" default="Fuel Economy (Combined):" />
                            </label>
                            <g:textField name="vehicle.fuelConsumptionCombined" class="numeric uneditable" readonly="readonly" value="${formatNumber(number:quoteInstance.vehicle.fuelConsumptionCombined, type:'number', minFractionDigits:2, maxFractionDigits:2)}" />&nbsp;L/100km
                            <!--<a class="help-tip" href="" title="vehicle.fuelConsumptionCombined help text here..."></a>-->
                        </div>
                        <div style="margin-bottom:1px; padding-bottom:1px;" class="prop mandatory ${hasErrors(bean: quoteInstance, field: 'fuelEconomyBuffer', 'error')}">
                            <label for="fuelEconomyBuffer">
                                <g:message code="quote.fuelEconomyBuffer" default="Fuel Economy Buffer:" />
                            </label>
                            <g:textField name="fuelEconomyBuffer" class="numeric" value="${formatNumber(number:quoteInstance.fuelEconomyBuffer, type:'number', minFractionDigits:1, maxFractionDigits:2)}" />
                            <!--<a class="help-tip" href="" title="fuelEconomyBuffer help text here..."></a>-->
                        </div>
                        <div style="margin-bottom:1px; padding-bottom:1px;" class="prop mandatory ${hasErrors(bean: quoteInstance, field: 'fuelEconomyFactor', 'error')}">
                            <label for="fuelEconomyFactor">
                                <g:message code="quote.fuelEconomyFactor" default="Consumption Factor:" />
                            </label>
                            <g:textField id="consumptionFactor" name="fuelEconomyFactor" class="numeric" value="${formatNumber(number:quoteInstance.fuelEconomyFactor, type:'number', minFractionDigits:1, maxFractionDigits:2)}" />
                            <a class="help-tip" href="" title="Consumption factor used to adjust Fuel Economy for alternate fuels. eg LPG is factored at 1.35"></a>
                        </div>
                        <div class="dbl-line-below prop mandatory ${hasErrors(bean: quoteInstance, field: 'fuelCostPerLitre', 'error')}">
                            <label for="fuelCostPerLitre">
                                <g:message code="quote.fuelCostPerLitre" default="Unit Cost (Dollars Per Litre):" />
                            </label>
                            <g:textField name="fuelCostPerLitre" class="numeric" value="${formatNumber(number:quoteInstance.fuelCostPerLitre, type:'number', minFractionDigits:3, maxFractionDigits:3)}" />
                            <!--<a class="help-tip" href="" title="fuelCostPerLitre help text here..."></a>-->
                        </div>
                        <div class="grand-total thk-line-below prop mandatory ${hasErrors(bean: quoteInstance, field: 'annualFuel', 'error')}">
                            <label for="annualFuel">
                                <g:message code="quote.annualFuel" default="Annual Fuel:" />
                            </label>
                            <g:textField name="annualFuel" class="numeric uneditable" readonly="readonly" value="${formatNumber(number:quoteInstance.annualFuel, type:'number', minFractionDigits:2, maxFractionDigits:2)}" />
                        </div>
                    </fieldset>
                    
                    <fieldset class="editable">
                        <legend><g:message code="quote.edit.legend.left" default="Budget - Comprehensive Insurance"/></legend>
                        <h3 class="form-section"><g:message code="quote.edit.compInsur.heading" default="Comprehensive Insurance"/></h3>
                        <div style="margin-bottom:1px; padding-bottom:1px;" class="prop mandatory ${hasErrors(bean: quoteInstance, field: 'compInsurSupplierId', 'error')}">
                            <label for="compInsurSupplierId">
                                <g:message code="quote.compInsurSupplierId" default="Insurance Provider" />
                                <span class="indicator">*</span>
                            </label>
                            <g:select class="non-numeric" name="compInsurSupplierId" from="${com.innovated.iris.domain.Supplier.findAllByTypeInList(SupplierType.getAll([23, 25]))}" optionKey="id" value="${quoteInstance?.compInsurSupplierId}"  />
                            <!--<a class="help-tip" href="" title="compInsurSupplierId help text here..."></a>-->
                        </div>
                        <div class="prop mandatory ${hasErrors(bean: quoteInstance, field: 'annualCompInsur', 'error')}">
                            <label for="annualCompInsur">
                                <g:message code="quote.annualCompInsur" default="Annual Comprehensive Insurance" />
                            </label>
                            <g:textField name="annualCompInsur" class="numeric" value="${formatNumber(number:quoteInstance.annualCompInsur, type:'number', minFractionDigits:2, maxFractionDigits:2)}" />
                            <!--<a class="help-tip" href="" title="annualCompInsur help text here..."></a>-->
                        </div>
                    </fieldset>
                    
                    <fieldset class="editable">
                        <legend><g:message code="quote.edit.legend.left" default="Budget - Roadside"/></legend>
                        <h3 class="form-section"><g:message code="quote.edit.roadside.heading" default="Roadside"/></h3>
                        <div style="margin-bottom:1px; padding-bottom:1px;" class="prop mandatory ${hasErrors(bean: quoteInstance, field: 'roadsideSupplierId', 'error')}">
                            <label for="roadsideSupplierId">
                                <g:message code="quote.roadsideSupplierId" default="Roadside Provider" />
                                <span class="indicator">*</span>
                            </label>
                            <g:select class="non-numeric" name="roadsideSupplierId" from="${com.innovated.iris.domain.Supplier.findAllByTypeInList(SupplierType.getAll([15, 27]))}" optionKey="id" value="${quoteInstance?.roadsideSupplierId}"  />
                            <!--<a class="help-tip" href="" title="roadsideSupplierId help text here..."></a>-->
                        </div>
                        <div class="prop mandatory ${hasErrors(bean: quoteInstance, field: 'annualRoadside', 'error')}">
                            <label for="annualRoadside">
                                <g:message code="quote.annualRoadside" default="Annual Roadside" />
                            </label>
                            <g:textField name="annualRoadside" class="numeric" value="${formatNumber(number:quoteInstance.annualRoadside, type:'number', minFractionDigits:2, maxFractionDigits:2)}" />
                            <!--<a class="help-tip" href="" title="annualRoadside help text here..."></a>-->
                        </div>
                    </fieldset>
                    
                    <fieldset class="editable">
                        <legend><g:message code="quote.edit.legend.left" default="Budget - Rego & CTP"/></legend>
                        
                        <h3 class="form-section"><g:message code="quote.edit.regoctp.heading" default="Rego & CTP"/></h3>
                        <div style="margin-bottom:1px; padding-bottom:1px;" class="prop mandatory ${hasErrors(bean: quoteInstance, field: 'regoSupplierId', 'error')}">
                            <label for="regoSupplierId">
                                <g:message code="quote.regoSupplierId" default="Registration Provider" />
                                <span class="indicator">*</span>
                            </label>
                            <g:select class="non-numeric" name="regoSupplierId" from="${com.innovated.iris.domain.Supplier.findAllByTypeInList(SupplierType.getAll([6, 35]))}" optionKey="id" value="${quoteInstance?.regoSupplierId}"  />
                            <!--<a class="help-tip" href="#" title="regoSupplier help text here..."></a>-->
                        </div>
                        <div style="margin-bottom:1px; padding-bottom:1px;" class="prop mandatory ${hasErrors(bean: quoteInstance, field: 'annualRegoCost', 'error')}">
                            <label for="annualRegoCost">
                                <g:message code="quote.annualRegoCost" default="Annual Registration" />
                                <span class="indicator">*</span>
                            </label>
                            <g:textField name="annualRegoCost" class="numeric" value="${formatNumber(number:quoteInstance.annualRegoCost, type:'number', minFractionDigits:2, maxFractionDigits:2)}" />
                            <!--<a class="help-tip" href="#" title="annualRegoCost help text here..."></a>-->
                        </div>
                        <div class="prop mandatory ${hasErrors(bean: quoteInstance, field: 'regoIncludesGst', 'error')}">
                            <label for="regoIncludesGst">
                                <g:message code="quote.regoIncludesGst" default="Rego Includes GST" />
                                <span class="indicator">*</span>
                            </label>
                            <g:checkBox name="regoIncludesGst" value="${quoteInstance?.regoIncludesGst}" />
                            <!--<a class="help-tip" href="#" title="regoIncludesGst help text here..."></a>-->
                        </div>
                        <div style="margin-bottom:1px; padding-bottom:1px;" class="prop mandatory ${hasErrors(bean: quoteInstance, field: 'ctpSupplierId', 'error')}">
                            <label for="ctpSupplierId">
                                <g:message code="quote.ctpSupplierId" default="CTP/Greenslip Provider" />
                                <span class="indicator">*</span>
                            </label>
                            <g:select class="non-numeric" name="ctpSupplierId" from="${com.innovated.iris.domain.Supplier.findAllByTypeInList(SupplierType.getAll([6, 35]))}" optionKey="id" value="${quoteInstance?.ctpSupplierId}"  />
                            <!--<a class="help-tip" href="#" title="ctpSupplier help text here..."></a>-->
                        </div>
                        <div class="prop mandatory ${hasErrors(bean: quoteInstance, field: 'annualCtpCost', 'error')}">
                            <label for="annualCtpCost">
                                <g:message code="quote.annualCtpCost" default="Annual CTP/Greenslip" />
                                <span class="indicator">*</span>
                            </label>
                            <g:textField name="annualCtpCost" class="numeric" value="${formatNumber(number:quoteInstance.annualCtpCost, type:'number', minFractionDigits:2, maxFractionDigits:2)}" />
                            <!--<a class="help-tip" href="#" title="annualCtpCost help text here..."></a>-->
                        </div>
					</fieldset>
					
					<fieldset class="editable">
                        <legend><g:message code="quote.edit.legend.left" default="Budget - Wash/Detail & Other"/></legend>
                        <h3 class="form-section"><g:message code="quote.edit.other.heading" default="Annual Other Costs"/></h3>
                        <div style="margin-bottom:1px; padding-bottom:1px;" class="prop ${hasErrors(bean: quoteInstance, field: 'annualDetailWash', 'error')}">
                            <label for="annualDetailWash">
                                <g:message code="quote.annualDetailWash" default="Car Wash / Detail" />
                            </label>
                            <g:textField name="annualDetailWash" class="numeric" value="${formatNumber(number:quoteInstance.annualDetailWash, type:'number', minFractionDigits:2, maxFractionDigits:2)}" />
                            <!--<a class="help-tip" href="#" title="annualDetailWash help text here..."></a>-->
                        </div>
                        <div class="prop ${hasErrors(bean: quoteInstance, field: 'annualOther', 'error')}">
                            <label for="annualOther">
                                <g:message code="quote.annualOther" default="Other Costs" />
                            </label>
                            <g:textField name="annualOther" class="numeric" value="${formatNumber(number:quoteInstance.annualOther, type:'number', minFractionDigits:2, maxFractionDigits:2)}" />
                            <!--<a class="help-tip" href="#" title="annualOther help text here..."></a>-->
                        </div>
                    </fieldset>
                </div>
                
			</td>
			<td valign="top">
                
                <div id="quote_right_col" class="dialog">
                    <fieldset>
                        <legend><g:message code="quote.edit.legend.right" default="Quote Inputs"/></legend>
                        <h3 class="form-section">User Input Data</h3>
                        <div class="prop mandatory">
                            <label for="fromDate">
		                        <g:message code="quote.fromDate" default="Quote as at" />
		                        <span class="indicator">*</span>
                            </label>
                        	<g:datePicker name="fromDate" value="${quoteInstance?.fromDate}" precision="day"/>
						</div>
                        
                        %{--<div class="prop mandatory ${hasErrors(bean: quoteInstance, field: 'customer', 'error')}">--}%
                            %{--<label for="customer">--}%
                                %{--<g:message code="quote.customer" default="Employee" />--}%
                                %{--<span class="indicator">*</span>--}%
                            %{--</label>--}%
                            %{--<g:select class="non-numeric" name="customer.id" from="" optionKey="id" value="${quoteInstance?.customer?.id}"  />--}%
                        %{--</div>--}%
                        
                        <div class="prop">
                            <label></label>
                            <span class="fineprint">(<b>Address:</b> ${quoteInstance.address})</span>&nbsp;<a href="#">Change...</a>
                        </div>
                        
                        <div class="prop mandatory ${hasErrors(bean: quoteInstance, field: 'employment', 'error')}">
                            <label for="employment">
                                <g:message code="quote.employment" default="Job (Employer)" />
                                <span class="indicator">*</span>
                            </label>
                            <g:select class="non-numeric" name="employment.id" from="${com.innovated.iris.domain.Employment.list()}" optionKey="id" value="${quoteInstance?.employment?.id}"  />
                        </div>
                        
                        <div class="prop mandatory ${hasErrors(bean: quoteInstance, field: 'grossAnnualSalary', 'error')}">
                            <label for="grossAnnualSalary">
                                <g:message code="quote.grossAnnualSalary" default="Gross Salary" />
                                <span class="indicator">*</span>
                            </label>
                            <g:textField name="grossAnnualSalary" class="numeric" value="${formatNumber(number:quoteInstance.grossAnnualSalary, type:'number', minFractionDigits:2, maxFractionDigits:2)}" />
                        </div>
                        
                        <div class="prop ${hasErrors(bean: quoteInstance, field: 'vehicleNew', 'error')}">
		                        <label for="vehicleNew">
		                            <g:message code="quote.vehicleNew" default="New Vehicle?" />
		                        </label>
		                        <g:checkBox name="vehicleNew" value="${quoteInstance.vehicleNew}" />
		                   </div>
                        
                        <div class="prop mandatory ${hasErrors(bean: quoteInstance, field: 'vehicleYear', 'error')}">
                            <label for="vehicleYear">
                                <g:message code="quote.vehicleYear" default="Year" />
                                <span class="indicator">*</span>
                            </label>
                            <g:select name="vehicleYear" from="${2011..2003}" value="${quoteInstance?.vehicleYear}"  />
                        </div>
                        
                        <div class="prop mandatory ${hasErrors(bean: quoteInstance, field: 'vehicle', 'error')}">
                            <label for="vehicle">
                                <g:message code="quote.vehicle" default="Vehicle" />
                                <span class="indicator">*</span>
                            </label>
                            <g:select class="non-numeric" name="vehicle.id" from="${com.innovated.iris.domain.MakeModel.list()}" optionKey="id" value="${quoteInstance?.vehicle?.id}"  />
                        </div>
                        
                        <div class="prop ${hasErrors(bean: quoteInstance, field: 'shape', 'error')}">
                            <!-- 
                            <label for="shape">
                                <g:message code="quote.shape" default="Shape" />
                            </label>
                            <g:textField class="non-numeric" name="shape" value="${fieldValue(bean: quoteInstance, field: 'shape')}" />
                             -->
                            <label for="shape">
                            	<g:message code="quote.shape" default="Shape" />
                            </label>
							<g:select class="non-numeric" name="shape" from="${quoteInstance.constraints.shape.inList}" value="${quoteInstance.shape}" valueMessagePrefix="quote.shape"  />
                        </div>
                        
                        <div class="prop ${hasErrors(bean: quoteInstance, field: 'transmission', 'error')}">
                            <!-- 
                            <label for="transmission">
                                <g:message code="quote.transmission" default="Transmission" />
                            </label>
                            <g:textField class="non-numeric" name="transmission" value="${fieldValue(bean: quoteInstance, field: 'transmission')}" />
                             -->
                            <label for="transmission">
                            	<g:message code="quote.transmission" default="Transmission" />
                            </label>
							<g:select class="non-numeric" name="transmission" from="${quoteInstance.constraints.transmission.inList}" value="${quoteInstance.transmission}" valueMessagePrefix="quote.transmission"  />
                        </div>
                        
                        <div class="prop ${hasErrors(bean: quoteInstance, field: 'colourPreference', 'error')}">
                            <label for="colourPreference">
                                <g:message code="quote.colourPreference" default="Colour Preference" />
                            </label>
                            <g:textField class="non-numeric" name="colourPreference" value="${fieldValue(bean: quoteInstance, field: 'colourPreference')}" />
                        </div>
                        
                        <div class="prop mandatory ${hasErrors(bean: quoteInstance, field: 'kmPerYear', 'error')}">
                            <label for="kmPerYear">
                                <g:message code="quote.kmPerYear" default="Km Per Year" />
                                <span class="indicator">*</span>
                            </label>
                            <g:textField class="numeric" name="kmPerYear" value="${fieldValue(bean: quoteInstance, field: 'kmPerYear')}" /> km
                        </div>
                        
                        <div class="prop mandatory ${hasErrors(bean: quoteInstance, field: 'leaseTerm', 'error')}">
                            <label for="leaseTerm">
                                <g:message code="quote.leaseTerm" default="Lease Term" />
                                <span class="indicator">*</span>
                            </label>
                            <g:textField class="numeric" name="leaseTerm" value="${fieldValue(bean: quoteInstance, field: 'leaseTerm')}" /> months
                        </div>
                        
                        <div style="border:1px solid #D9D8D0; padding-top:5px; background-color:#FFFDDD;" class="prop">
                            <label for="businessUsage">
                                <g:message code="quote.businessUsage" default="Business Usage" />
                            </label>
                            <g:textField class="numeric" name="businessUsage" value="${fieldValue(bean: quoteInstance, field: 'businessUsage')}" />&nbsp;%
                            <a class="help-tip" href="" title="Only used for Operating Cost (Log Book) Method"></a>
                        </div>
                        
                        <div class="prop">
                            <label for="relookup"></label>
                            <!--<g:actionSubmit class="lookup" action="update" value="${message(code: 'relookup', 'default': 'Lookup')}" onclick="enableLookup()"/>
                            <button class="lookup" value="${message(code: 'relookup', 'default': 'Lookup')}" onclick="enableLookup()"/>
                            <a class="help-tip-warn" href="#" title="WARNING: Looking up parameter values (again) will loose any current quote values!!"></a>-->
                        </div>
                        
                        <h3 class="form-section"><g:message code="quote.edit.defaults.heading" default="Derived Input Data"/></h3>
                        <div class="prop mandatory ${hasErrors(bean: quoteInstance, field: 'addressString', 'error')}">
                            <label for="addressString">
                                <g:message code="quote.addressString" default="Garaged Address" />
                            </label>
                            <g:textField class="non-numeric uneditable" name="addressString" value="${fieldValue(bean: quoteInstance, field: 'addressString')}" readonly="readonly"/>
                            <a class="help-tip" href="#" title="addressString help text here..."></a>
                        </div>
                        <div class="prop mandatory ${hasErrors(bean: quoteInstance, field: 'stateString', 'error')}">
                            <label for="stateString">
                                <g:message code="quote.stateString" default="Address State" />
                            </label>
                            <g:textField class="non-numeric uneditable" name="stateString" value="${fieldValue(bean: quoteInstance, field: 'stateString')}" readonly="readonly"/>
                            <a class="help-tip" href="#" title="stateString help text here..."></a>
                        </div>
                    </fieldset>
                    
                    <fieldset>
						<legend><g:message code="quote.edit.legend.right.employer" default="Employer Preference Defaults"/></legend>
							<div class="prop ${hasErrors(bean: quoteInstance, field: 'defaultPayFreq', 'error')}">
                                <label for="parent">
                                    <g:message code="quote.defaultPayFreq" default="Pay Frequency" />
                                </label>
                                <g:select name="defaultPayFreq" from="${payFreqMap}" optionKey="key" optionValue="value" value="${quoteInstance.defaultPayFreq.getCode()}"></g:select>
                            </div>
							<div class="prop ${hasErrors(bean: quoteInstance, field: 'canClaimGst', 'error')}">
		                        <label for="canClaimGst">
		                            <g:message code="quote.canClaimGst" default="Employer claims GST?" />
		                        </label>
		                        <g:checkBox name="canClaimGst" value="${quoteInstance.canClaimGst}" />
		                    </div>
							<div class="prop ${hasErrors(bean: quoteInstance, field: 'allowsFbtEcm', 'error')}">
		                        <label for="allowsFbtEcm">
		                            <g:message code="quote.allowsFbtEcm" default="ECM Preferred?" />
		                        </label>
		                        <g:checkBox name="allowsFbtEcm" value="${quoteInstance.allowsFbtEcm}" />
		                    </div>
		                    <div class="prop ${hasErrors(bean: quoteInstance, field: 'allowedFbtCalcMethods', 'error')}">
		                        <label style="vertical-align: top;">Preferred Taxable Value Types</label>
		                        <g:if test="${taxableValueMap.size() > 0}">
		                        <table>
		                        	<thead>
			                        	<tr>
			                        		<th class="ctr">Allowed?</th>
			                        		<th>Type</th>
			                        		<th class="ctr">Default</th>
			                        	</tr>
		                        	</thead>
		                        	<tbody>
			                        	<g:each status="i" var="tv" in="${taxableValueMap}">
					                    <tr>
			                        		<td class="ctr"><g:checkBox name="ALLOWED_TV_${tv.key.key}" value="${tv.value}" /></td>
			                        		<td>${tv.key.value.encodeAsHTML()}</td>
			                        		<g:if test="${quoteInstance.allowedFbtCalcMethods.size() > 0 && tv.key.value == quoteInstance.allowedFbtCalcMethods.get(0)}">
			                        		<td class="ctr"><g:radio id="tv_default_${tv.key.key}" name="tv_default" value="${tv.key.key}" checked="checked"/></td>
			                        		</g:if>
			                        		<g:else>
			                        		<td class="ctr"><g:radio id="tv_default_${tv.key.key}" name="tv_default" value="${tv.key.key}"/></td>
			                        		</g:else>
			                        	</tr>
					                    </g:each>
				                    </tbody>
		                        </table>
		                        </g:if>
		                    </div>
		                    <div class="prop ${hasErrors(bean: quoteInstance, field: 'concession', 'error')}">
                                <label for="parent">
                                    <g:message code="quote.concession" default="FBT Concession Applied" />
                                </label>
                                <g:select name="concession" from="${concessionMap}" optionKey="key" optionValue="value" value="${quoteInstance.concession.getCode()}"></g:select>
                            </div>
                            
                            <div style="border:1px solid #D9D8D0; padding:5px 0; background-color:#FFFDDD;">
	                            <div style="margin-bottom:1px; padding-bottom:1px;" class="prop">
		                            <label for="employerShareRate">
		                                <g:message code="employerShareRate" default="Employer Share %" />
		                            </label>
		                            <g:textField class="numeric" name="employerShareRate" value="${fieldValue(bean: quoteInstance, field: 'employerShareRate')}" />&nbsp;%
		                            <!-- <a class="help-tip" href="" title="Only used for Operating Cost (Log Book) Method"></a> -->
		                        </div>
		                        <div style="margin-bottom:1px; padding-bottom:1px;" class="prop ${hasErrors(bean: quoteInstance, field: 'employerShareFee', 'error')}">
		                            <label for="employerShareFee">
		                                <g:message code="employerShareFee" default="Admin Fee" />
		                            </label>
		                            <g:textField name="employerShareFee" class="numeric" value="${formatNumber(number:quoteInstance.employerShareFee, type:'number', minFractionDigits:2, maxFractionDigits:2)}" />
		                        </div>
	                        </div>
					</fieldset>
                </div>
                
                </td>
			</tr>
			</tbody>
            </table>
                
                <div class="clear"></div>
                <iris:greyButton name="btnUpdate" src="${resource(dir:'images/skin',file:'page_save.png')}" text="Update Quote">Click to update this quote</iris:greyButton>
                <span class="formcancel"> or&nbsp;&nbsp;<a href="#"><g:link controller="tools" action="viewquote" id="${quoteInstance?.id}">cancel editing</g:link></a></span>
            </g:form>
