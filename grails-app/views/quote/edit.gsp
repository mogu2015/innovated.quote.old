<%@
page import="com.innovated.iris.domain.Quote"
%>
<html>
    <head>
        <meta name="layout" content="prod">
        <title><g:message code="default.page.title" default="IRIS" /></title>
        <link rel="stylesheet" href="${resource(dir:'css',file:'forms.css')}" />
        <link rel="stylesheet" href="${resource(dir:'css',file:'iris.css')}" />
	</head>
	<body>
		<div id="content" class="body">
			<p class="bread">
				<!-- <g:link controller="tools"><g:message code="ui.menu.label.tools" default="Tools" /></g:link> -->
				<g:message code="ui.menu.label.tools" default="Tools" />
				<span class="breadArrow">&nbsp;</span>
				<g:link controller="tools" action="quote"><g:message code="ui.menu.label.novatedquote" default="Quote" /></g:link>
				<g:if test="${quoteInstance}">
					<span class="breadArrow">&nbsp;</span>
					<g:link controller="tools" action="viewquote" id="${quoteInstance?.id}"><g:formatNumber number="${quoteInstance?.id}" format="${message(code:'quote.number.mask')}" /></g:link>
				</g:if>
			</p>
					
			<g:if test="${flash.message}">
				<div class="flashMsg">
					<g:if test="${flash.classes}"><p class="${flash.classes}"></g:if><g:else><p></g:else><g:message code="${flash.message}" args="${flash.args}" default="${flash.defaultMessage}" /></p>
				</div>
			</g:if>
			<g:hasErrors bean="${quoteInstance}">
			<div class="errors">
				<ul>
					<g:eachError bean="${quoteInstance}">
					<li><g:message error="${it}"/></li>
					</g:eachError>
				</ul>
			</div>
		    </g:hasErrors>
		            
		    <h1 class="contentH1"><g:message code="ui.h1.label.editquote" default="Edit Quote" /> <g:formatNumber number="${quoteInstance?.id}" format="${message(code:'quote.number.mask')}" /></h1>
			<p class="notice">
				<g:if test="${quoteInstance?.pricingConfirmed == null}">
					<i><b><u>Note</u></b>: The <span class="bad">
					<g:if test="${!quoteInstance?.priceObtained}">
						pricing shown on this quote is indicative</span>. Final pricing is <b>yet to be obtained</b> from relevant 3rd parties.
					</g:if>
					<g:else>
						pricing shown on this quote is input by customer</span>.
					</g:else>
					</i>
				</g:if>
				<g:else>
					<i><b><u>Note</u></b>: The <span
							class="good">pricing shown on this quote has been confirmed</span> by all relevant 3rd parties as at: <b><g:formatDate
							date="${quoteInstance?.pricingConfirmed}" format="d MMM yyyy"/></b>.</i>
				</g:else>
			</p>
			<div id="topBtns">
				<iris:greyButton name="btnUpdate" src="${resource(dir:'images/skin',file:'page_save.png')}" text="Update Quote">Click to update this quote</iris:greyButton>
				<span class="formcancel"> or&nbsp;&nbsp;<a href="#"><g:link controller="tools" action="viewquote" id="${quoteInstance?.id}">cancel editing</g:link></a></span>
			</div>
			<g:render template="editForm" bean="${quoteInstance}" />
        </div>	<!-- END: content -->
	</body>
</html>

