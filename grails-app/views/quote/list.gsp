<%@
page import="com.innovated.iris.domain.Quote"
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <title><g:message code="quote.list" default="Quote List" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLinkTo(dir: '')}"><g:message code="home" default="Home" /></a></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="quote.new" default="New Quote" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="quote.list" default="Quote List" /></h1>
            <g:if test="${flash.message}">
            <div class="message"><g:message code="${flash.message}" args="${flash.args}" default="${flash.defaultMessage}" /></div>
            </g:if>
            <div class="list">
                <table>
                    <thead>
                        <tr>
	                   	    <g:sortableColumn property="id" title="Quote ID" titleKey="quote.id" />
	                   	    <th><g:message code="quote.customer" default="Customer" /></th>
	                   	    <th><g:message code="quote.employment" default="Employment" /></th>
	                   	    <g:sortableColumn property="vehicle" title="Vehicle" titleKey="quote.vehicle" />
	                   	    <g:sortableColumn property="colourPreference" title="Colour Preference" titleKey="quote.colourPreference" />
	                   	    <g:sortableColumn property="kmPerYear" title="Km Per Year" titleKey="quote.kmPerYear" />
	                   	    <g:sortableColumn property="leaseTerm" title="Term" titleKey="quote.leaseTerm" />
	                        <g:sortableColumn property="dateCreated" title="Quote Created" titleKey="quote.dateCreated" />
	                        <g:sortableColumn property="lastUpdated" title="Last Updated" titleKey="quote.lastUpdated" />
	                        
	                        <th colspan="1"><g:message code="default.label.tasks" default="Tasks" /></th>
                        </tr>
                    </thead>
                    <tbody>
                    <g:each in="${quoteInstanceList}" status="i" var="quoteInstance">
                        <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
                            <td><g:link action="show" id="${quoteInstance.id}"><g:formatNumber number="${quoteInstance?.id}" format="${message(code:'quote.number.mask')}" /></g:link></td>
                            <td>${fieldValue(bean: quoteInstance, field: "customer")}</td>
                            <td>${fieldValue(bean: quoteInstance, field: "employment")}</td>
                            <td>${fieldValue(bean: quoteInstance, field: "vehicle")}</td>
                            <td>${fieldValue(bean: quoteInstance, field: "colourPreference")}</td>
                            <td>${fieldValue(bean: quoteInstance, field: "kmPerYear")}</td>
                            <td>${fieldValue(bean: quoteInstance, field: "leaseTerm")} month</td>
                            <td><g:formatDate format="d/M/yy, HH:mm:ss" date="${quoteInstance.dateCreated}"/></td>
                            <td><prettytime:display date="${quoteInstance.lastUpdated}" capitalize="true" showTime="true" /></td>
                            
                            <td><g:link action="clone" id="${quoteInstance.id}">Clone...</g:link></td>
                        </tr>
                    </g:each>
                    </tbody>
                </table>
            </div>
            <div class="paginateButtons">
                <g:paginate total="${quoteInstanceTotal}" />
            </div>
        </div>
    </body>
</html>
