
<%@ page import="com.innovated.iris.domain.enums.SupplierType" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <title><g:message code="supplierType.create" default="Create SupplierType" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLinkTo(dir: '')}"><g:message code="home" default="Home" /></a></span>
            <span class="menuButton"><g:link class="list" action="list"><g:message code="supplierType.list" default="SupplierType List" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="supplierType.create" default="Create SupplierType" /></h1>
            <g:if test="${flash.message}">
            	<div class="message"><g:message code="${flash.message}" args="${flash.args}" default="${flash.defaultMessage}" /></div>
            </g:if>
            <g:hasErrors bean="${supplierTypeInstance}">
				<div class="errors">
					<g:renderErrors bean="${supplierTypeInstance}" as="list" />
				</div>
            </g:hasErrors>
            <g:form action="save" method="post" >
                <div class="dialog">
                    <fieldset>
                        <legend><g:message code="supplierType.create.legend" default="Enter SupplierType Details"/></legend>
                        
                            <div class="prop mandatory ${hasErrors(bean: supplierTypeInstance, field: 'name', 'error')}">
                                <label for="name">
                                    <g:message code="supplierType.name" default="Name" />
                                    <span class="indicator">*</span>
                                </label>
                                <g:textField name="name" value="${fieldValue(bean: supplierTypeInstance, field: 'name')}" />

                            </div>
                        
                            <div class="prop mandatory ${hasErrors(bean: supplierTypeInstance, field: 'typeCode', 'error')}">
                                <label for="typeCode">
                                    <g:message code="supplierType.typeCode" default="Type Code" />
                                    <span class="indicator">*</span>
                                </label>
                                <g:textField name="typeCode" value="${fieldValue(bean: supplierTypeInstance, field: 'typeCode')}" />

                            </div>
                        
                            <div class="prop mandatory ${hasErrors(bean: supplierTypeInstance, field: 'visible', 'error')}">
                                <label for="visible">
                                    <g:message code="supplierType.visible" default="Visible" />
                                    <span class="indicator">*</span>
                                </label>
                                <g:checkBox name="visible" value="${supplierTypeInstance?.visible}" />

                            </div>
                        
                            <div class="prop mandatory ${hasErrors(bean: supplierTypeInstance, field: 'enabled', 'error')}">
                                <label for="enabled">
                                    <g:message code="supplierType.enabled" default="Enabled" />
                                    <span class="indicator">*</span>
                                </label>
                                <g:checkBox name="enabled" value="${supplierTypeInstance?.enabled}" />

                            </div>
                        
                            <div class="prop mandatory ${hasErrors(bean: supplierTypeInstance, field: 'dateCreated', 'error')}">
                                <label for="dateCreated">
                                    <g:message code="supplierType.dateCreated" default="Date Created" />
                                    <span class="indicator">*</span>
                                </label>
                                <g:datePicker name="dateCreated" value="${supplierTypeInstance?.dateCreated}"  />

                            </div>
                        
                            <div class="prop mandatory ${hasErrors(bean: supplierTypeInstance, field: 'lastUpdated', 'error')}">
                                <label for="lastUpdated">
                                    <g:message code="supplierType.lastUpdated" default="Last Updated" />
                                    <span class="indicator">*</span>
                                </label>
                                <g:datePicker name="lastUpdated" value="${supplierTypeInstance?.lastUpdated}"  />

                            </div>
                        
                    </fieldset>
                </div>
                <div class="buttons">
                    <span class="button"><g:submitButton name="create" class="save" value="${message(code: 'create', 'default': 'Create')}" /></span>
                </div>
            </g:form>
        </div>
    </body>
</html>

