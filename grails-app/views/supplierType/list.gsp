
<%@ page import="com.innovated.iris.domain.enums.SupplierType" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <title><g:message code="supplierType.list" default="SupplierType List" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLinkTo(dir: '')}"><g:message code="home" default="Home" /></a></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="supplierType.new" default="New SupplierType" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="supplierType.list" default="SupplierType List" /></h1>
            <g:if test="${flash.message}">
            <div class="message"><g:message code="${flash.message}" args="${flash.args}" default="${flash.defaultMessage}" /></div>
            </g:if>
            <div class="list">
                <table>
                    <thead>
                        <tr>
                        
                   	    <g:sortableColumn property="id" title="Id" titleKey="supplierType.id" />
                        
                   	    <g:sortableColumn property="name" title="Name" titleKey="supplierType.name" />
                        
                   	    <g:sortableColumn property="typeCode" title="Type Code" titleKey="supplierType.typeCode" />
                        
                   	    <g:sortableColumn property="visible" title="Visible" titleKey="supplierType.visible" />
                        
                   	    <g:sortableColumn property="enabled" title="Enabled" titleKey="supplierType.enabled" />
                        
                   	    <g:sortableColumn property="dateCreated" title="Date Created" titleKey="supplierType.dateCreated" />
                        
                        </tr>
                    </thead>
                    <tbody>
                    <g:each in="${supplierTypeInstanceList}" status="i" var="supplierTypeInstance">
                        <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
                        
                            <td><g:link action="show" id="${supplierTypeInstance.id}">${fieldValue(bean: supplierTypeInstance, field: "id")}</g:link></td>
                        
                            <td>${fieldValue(bean: supplierTypeInstance, field: "name")}</td>
                        
                            <td>${fieldValue(bean: supplierTypeInstance, field: "typeCode")}</td>
                        
                            <td><g:formatBoolean boolean="${supplierTypeInstance.visible}" /></td>
                        
                            <td><g:formatBoolean boolean="${supplierTypeInstance.enabled}" /></td>
                        
                            <td><g:formatDate date="${supplierTypeInstance.dateCreated}" /></td>
                        
                        </tr>
                    </g:each>
                    </tbody>
                </table>
            </div>
            <div class="paginateButtons">
                <g:paginate total="${supplierTypeInstanceTotal}" />
            </div>
        </div>
    </body>
</html>
