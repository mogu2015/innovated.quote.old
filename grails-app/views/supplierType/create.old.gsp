
<%@ page import="com.innovated.iris.domain.enums.SupplierType" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <title><g:message code="supplierType.create" default="Create SupplierType" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLinkTo(dir: '')}"><g:message code="home" default="Home" /></a></span>
            <span class="menuButton"><g:link class="list" action="list"><g:message code="supplierType.list" default="SupplierType List" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="supplierType.create" default="Create SupplierType" /></h1>
            <g:if test="${flash.message}">
            	<div class="message"><g:message code="${flash.message}" args="${flash.args}" default="${flash.defaultMessage}" /></div>
            </g:if>
            <g:hasErrors bean="${supplierTypeInstance}">
		        <div class="errors">
		            <g:renderErrors bean="${supplierTypeInstance}" as="list" />
		        </div>
            </g:hasErrors>
            <g:form action="save" method="post" >
                <div class="dialog">
                    <table>
                        <tbody>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="name"><g:message code="supplierType.name" default="Name" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: supplierTypeInstance, field: 'name', 'errors')}">
                                    <g:textField name="name" value="${fieldValue(bean: supplierTypeInstance, field: 'name')}" />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="typeCode"><g:message code="supplierType.typeCode" default="Type Code" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: supplierTypeInstance, field: 'typeCode', 'errors')}">
                                    <g:textField name="typeCode" value="${fieldValue(bean: supplierTypeInstance, field: 'typeCode')}" />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="visible"><g:message code="supplierType.visible" default="Visible" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: supplierTypeInstance, field: 'visible', 'errors')}">
                                    <g:checkBox name="visible" value="${supplierTypeInstance?.visible}" />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="enabled"><g:message code="supplierType.enabled" default="Enabled" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: supplierTypeInstance, field: 'enabled', 'errors')}">
                                    <g:checkBox name="enabled" value="${supplierTypeInstance?.enabled}" />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="dateCreated"><g:message code="supplierType.dateCreated" default="Date Created" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: supplierTypeInstance, field: 'dateCreated', 'errors')}">
                                    <g:datePicker name="dateCreated" value="${supplierTypeInstance?.dateCreated}"  />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="lastUpdated"><g:message code="supplierType.lastUpdated" default="Last Updated" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: supplierTypeInstance, field: 'lastUpdated', 'errors')}">
                                    <g:datePicker name="lastUpdated" value="${supplierTypeInstance?.lastUpdated}"  />

                                </td>
                            </tr>
                        
                        </tbody>
                    </table>
                </div>
                <div class="buttons">
                    <span class="button"><g:submitButton name="create" class="save" value="${message(code: 'create', 'default': 'Create')}" /></span>
                </div>
            </g:form>
        </div>
    </body>
</html>
