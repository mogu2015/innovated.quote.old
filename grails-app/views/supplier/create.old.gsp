
<%@ page import="com.innovated.iris.domain.Supplier" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <title><g:message code="supplier.create" default="Create Supplier" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLinkTo(dir: '')}"><g:message code="home" default="Home" /></a></span>
            <span class="menuButton"><g:link class="list" action="list"><g:message code="supplier.list" default="Supplier List" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="supplier.create" default="Create Supplier" /></h1>
            <g:if test="${flash.message}">
            	<div class="message"><g:message code="${flash.message}" args="${flash.args}" default="${flash.defaultMessage}" /></div>
            </g:if>
            <g:hasErrors bean="${supplierInstance}">
		        <div class="errors">
		            <g:renderErrors bean="${supplierInstance}" as="list" />
		        </div>
            </g:hasErrors>
            <g:form action="save" method="post" >
                <div class="dialog">
                    <table>
                        <tbody>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="code"><g:message code="supplier.code" default="Code" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: supplierInstance, field: 'code', 'errors')}">
                                    <g:textField name="code" value="${fieldValue(bean: supplierInstance, field: 'code')}" />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="customerLinkCode"><g:message code="supplier.customerLinkCode" default="Customer Link Code" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: supplierInstance, field: 'customerLinkCode', 'errors')}">
                                    <g:textField name="customerLinkCode" value="${fieldValue(bean: supplierInstance, field: 'customerLinkCode')}" />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="entity"><g:message code="supplier.entity" default="Entity" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: supplierInstance, field: 'entity', 'errors')}">
                                    <g:select name="entity.id" from="${com.innovated.iris.domain.Entity.list()}" optionKey="id" value="${supplierInstance?.entity?.id}"  />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="type"><g:message code="supplier.type" default="Type" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: supplierInstance, field: 'type', 'errors')}">
                                    <g:select name="type.id" from="${com.innovated.iris.domain.enums.SupplierType.list()}" optionKey="id" value="${supplierInstance?.type?.id}"  />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="preferred"><g:message code="supplier.preferred" default="Preferred" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: supplierInstance, field: 'preferred', 'errors')}">
                                    <g:checkBox name="preferred" value="${supplierInstance?.preferred}" />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="joined"><g:message code="supplier.joined" default="Joined" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: supplierInstance, field: 'joined', 'errors')}">
                                    <g:datePicker name="joined" value="${supplierInstance?.joined}"  />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="ceased"><g:message code="supplier.ceased" default="Ceased" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: supplierInstance, field: 'ceased', 'errors')}">
                                    <g:datePicker name="ceased" value="${supplierInstance?.ceased}" noSelection="['': '']" />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="abn"><g:message code="supplier.abn" default="Abn" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: supplierInstance, field: 'abn', 'errors')}">
                                    <g:textField name="abn" value="${fieldValue(bean: supplierInstance, field: 'abn')}" />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="dateCreated"><g:message code="supplier.dateCreated" default="Date Created" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: supplierInstance, field: 'dateCreated', 'errors')}">
                                    <g:datePicker name="dateCreated" value="${supplierInstance?.dateCreated}"  />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="lastUpdated"><g:message code="supplier.lastUpdated" default="Last Updated" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: supplierInstance, field: 'lastUpdated', 'errors')}">
                                    <g:datePicker name="lastUpdated" value="${supplierInstance?.lastUpdated}"  />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="onAddComment"><g:message code="supplier.onAddComment" default="On Add Comment" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: supplierInstance, field: 'onAddComment', 'errors')}">
                                    

                                </td>
                            </tr>
                        
                        </tbody>
                    </table>
                </div>
                <div class="buttons">
                    <span class="button"><g:submitButton name="create" class="save" value="${message(code: 'create', 'default': 'Create')}" /></span>
                </div>
            </g:form>
        </div>
    </body>
</html>
