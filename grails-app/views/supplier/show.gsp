
<%@ page import="com.innovated.iris.domain.Supplier" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <title><g:message code="supplier.show" default="Show Supplier" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLinkTo(dir: '')}"><g:message code="home" default="Home" /></a></span>
            <span class="menuButton"><g:link class="list" action="list"><g:message code="supplier.list" default="Supplier List" /></g:link></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="supplier.new" default="New Supplier" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="supplier.show" default="Show Supplier" /></h1>
            <g:if test="${flash.message}">
            <div class="message"><g:message code="${flash.message}" args="${flash.args}" default="${flash.defaultMessage}" /></div>
            </g:if>
            <g:form>
                <g:hiddenField name="id" value="${supplierInstance?.id}" />
                <div class="dialog">
                    <table>
                        <tbody>
                        
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="supplier.id" default="Id" />:</td>
                                
                                <td valign="top" class="value">${fieldValue(bean: supplierInstance, field: "id")}</td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="supplier.code" default="Code" />:</td>
                                
                                <td valign="top" class="value">${fieldValue(bean: supplierInstance, field: "code")}</td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="supplier.customerLinkCode" default="Customer Link Code" />:</td>
                                
                                <td valign="top" class="value">${fieldValue(bean: supplierInstance, field: "customerLinkCode")}</td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="supplier.entity" default="Entity" />:</td>
                                
                                <td valign="top" class="value"><g:link controller="entity" action="show" id="${supplierInstance?.entity?.id}">${supplierInstance?.entity?.encodeAsHTML()}</g:link></td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="supplier.type" default="Type" />:</td>
                                
                                <td valign="top" class="value"><g:link controller="supplierType" action="show" id="${supplierInstance?.type?.id}">${supplierInstance?.type?.encodeAsHTML()}</g:link></td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="supplier.preferred" default="Preferred" />:</td>
                                
                                <td valign="top" class="value"><g:formatBoolean boolean="${supplierInstance?.preferred}" /></td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="supplier.joined" default="Joined" />:</td>
                                
                                <td valign="top" class="value"><g:formatDate date="${supplierInstance?.joined}" /></td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="supplier.ceased" default="Ceased" />:</td>
                                
                                <td valign="top" class="value"><g:formatDate date="${supplierInstance?.ceased}" /></td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="supplier.abn" default="Abn" />:</td>
                                
                                <td valign="top" class="value">${fieldValue(bean: supplierInstance, field: "abn")}</td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="supplier.dateCreated" default="Date Created" />:</td>
                                
                                <td valign="top" class="value"><g:formatDate date="${supplierInstance?.dateCreated}" /></td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="supplier.lastUpdated" default="Last Updated" />:</td>
                                
                                <td valign="top" class="value"><g:formatDate date="${supplierInstance?.lastUpdated}" /></td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="supplier.onAddComment" default="On Add Comment" />:</td>
                                
                                <td valign="top" class="value">${fieldValue(bean: supplierInstance, field: "onAddComment")}</td>
                                
                            </tr>
                            
                        </tbody>
                    </table>
                </div>
                <div class="buttons">
                    <span class="button"><g:actionSubmit class="edit" action="edit" value="${message(code: 'edit', 'default': 'Edit')}" /></span>
                    <span class="button"><g:actionSubmit class="delete" action="delete" value="${message(code: 'delete', 'default': 'Delete')}" onclick="return confirm('${message(code: 'delete.confirm', 'default': 'Are you sure?')}');" /></span>
                </div>
            </g:form>
        </div>
    </body>
</html>
