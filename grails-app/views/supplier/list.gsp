
<%@ page import="com.innovated.iris.domain.Supplier" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <title><g:message code="supplier.list" default="Supplier List" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLinkTo(dir: '')}"><g:message code="home" default="Home" /></a></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="supplier.new" default="New Supplier" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="supplier.list" default="Supplier List" /></h1>
            <g:if test="${flash.message}">
            <div class="message"><g:message code="${flash.message}" args="${flash.args}" default="${flash.defaultMessage}" /></div>
            </g:if>
            <div class="list">
                <table>
                    <thead>
                        <tr>
                        
                   	    <g:sortableColumn property="id" title="Id" titleKey="supplier.id" />
                        
                   	    <g:sortableColumn property="code" title="Code" titleKey="supplier.code" />
                        
                   	    <g:sortableColumn property="customerLinkCode" title="Customer Link Code" titleKey="supplier.customerLinkCode" />
                        
                   	    <th><g:message code="supplier.entity" default="Entity" /></th>
                   	    
                   	    <th><g:message code="supplier.type" default="Type" /></th>
                   	    
                   	    <g:sortableColumn property="preferred" title="Preferred" titleKey="supplier.preferred" />
                        
                        </tr>
                    </thead>
                    <tbody>
                    <g:each in="${supplierInstanceList}" status="i" var="supplierInstance">
                        <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
                        
                            <td><g:link action="show" id="${supplierInstance.id}">${fieldValue(bean: supplierInstance, field: "id")}</g:link></td>
                        
                            <td>${fieldValue(bean: supplierInstance, field: "code")}</td>
                        
                            <td>${fieldValue(bean: supplierInstance, field: "customerLinkCode")}</td>
                        
                            <td>${fieldValue(bean: supplierInstance, field: "entity")}</td>
                        
                            <td>${fieldValue(bean: supplierInstance, field: "type")}</td>
                        
                            <td><g:formatBoolean boolean="${supplierInstance.preferred}" /></td>
                        
                        </tr>
                    </g:each>
                    </tbody>
                </table>
            </div>
            <div class="paginateButtons">
                <g:paginate total="${supplierInstanceTotal}" />
            </div>
        </div>
    </body>
</html>
