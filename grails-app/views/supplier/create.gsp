
<%@ page import="com.innovated.iris.domain.Supplier" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <title><g:message code="supplier.create" default="Create Supplier" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLinkTo(dir: '')}"><g:message code="home" default="Home" /></a></span>
            <span class="menuButton"><g:link class="list" action="list"><g:message code="supplier.list" default="Supplier List" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="supplier.create" default="Create Supplier" /></h1>
            <g:if test="${flash.message}">
            	<div class="message"><g:message code="${flash.message}" args="${flash.args}" default="${flash.defaultMessage}" /></div>
            </g:if>
            <g:hasErrors bean="${supplierInstance}">
				<div class="errors">
					<g:renderErrors bean="${supplierInstance}" as="list" />
				</div>
            </g:hasErrors>
            <g:form action="save" method="post" >
                <div class="dialog">
                    <fieldset>
                        <legend><g:message code="supplier.create.legend" default="Enter Supplier Details"/></legend>
                        
                            <div class="prop mandatory ${hasErrors(bean: supplierInstance, field: 'code', 'error')}">
                                <label for="code">
                                    <g:message code="supplier.code" default="Code" />
                                    <span class="indicator">*</span>
                                </label>
                                <g:textField name="code" value="${fieldValue(bean: supplierInstance, field: 'code')}" />

                            </div>
                        
                            <div class="prop ${hasErrors(bean: supplierInstance, field: 'customerLinkCode', 'error')}">
                                <label for="customerLinkCode">
                                    <g:message code="supplier.customerLinkCode" default="Customer Link Code" />
                                    
                                </label>
                                <g:textField name="customerLinkCode" value="${fieldValue(bean: supplierInstance, field: 'customerLinkCode')}" />

                            </div>
                        
                            <div class="prop mandatory ${hasErrors(bean: supplierInstance, field: 'entity', 'error')}">
                                <label for="entity">
                                    <g:message code="supplier.entity" default="Entity" />
                                    <span class="indicator">*</span>
                                </label>
                                <g:select name="entity.id" from="${com.innovated.iris.domain.Entity.list()}" optionKey="id" value="${supplierInstance?.entity?.id}"  />

                            </div>
                        
                            <div class="prop mandatory ${hasErrors(bean: supplierInstance, field: 'type', 'error')}">
                                <label for="type">
                                    <g:message code="supplier.type" default="Type" />
                                    <span class="indicator">*</span>
                                </label>
                                <g:select name="type.id" from="${com.innovated.iris.domain.enums.SupplierType.list()}" optionKey="id" value="${supplierInstance?.type?.id}"  />

                            </div>
                        
                            <div class="prop mandatory ${hasErrors(bean: supplierInstance, field: 'preferred', 'error')}">
                                <label for="preferred">
                                    <g:message code="supplier.preferred" default="Preferred" />
                                    <span class="indicator">*</span>
                                </label>
                                <g:checkBox name="preferred" value="${supplierInstance?.preferred}" />

                            </div>
                        
                            <div class="prop mandatory ${hasErrors(bean: supplierInstance, field: 'joined', 'error')}">
                                <label for="joined">
                                    <g:message code="supplier.joined" default="Joined" />
                                    <span class="indicator">*</span>
                                </label>
                                <g:datePicker name="joined" value="${supplierInstance?.joined}"  />

                            </div>
                        
                            <div class="prop ${hasErrors(bean: supplierInstance, field: 'ceased', 'error')}">
                                <label for="ceased">
                                    <g:message code="supplier.ceased" default="Ceased" />
                                    
                                </label>
                                <g:datePicker name="ceased" value="${supplierInstance?.ceased}" noSelection="['': '']" />

                            </div>
                        
                            <div class="prop ${hasErrors(bean: supplierInstance, field: 'abn', 'error')}">
                                <label for="abn">
                                    <g:message code="supplier.abn" default="Abn" />
                                    
                                </label>
                                <g:textField name="abn" value="${fieldValue(bean: supplierInstance, field: 'abn')}" />

                            </div>
                        
                            <div class="prop mandatory ${hasErrors(bean: supplierInstance, field: 'dateCreated', 'error')}">
                                <label for="dateCreated">
                                    <g:message code="supplier.dateCreated" default="Date Created" />
                                    <span class="indicator">*</span>
                                </label>
                                <g:datePicker name="dateCreated" value="${supplierInstance?.dateCreated}"  />

                            </div>
                        
                            <div class="prop mandatory ${hasErrors(bean: supplierInstance, field: 'lastUpdated', 'error')}">
                                <label for="lastUpdated">
                                    <g:message code="supplier.lastUpdated" default="Last Updated" />
                                    <span class="indicator">*</span>
                                </label>
                                <g:datePicker name="lastUpdated" value="${supplierInstance?.lastUpdated}"  />

                            </div>
                        
                            <div class="prop ${hasErrors(bean: supplierInstance, field: 'onAddComment', 'error')}">
                                <label for="onAddComment">
                                    <g:message code="supplier.onAddComment" default="On Add Comment" />
                                    
                                </label>
                                

                            </div>
                        
                    </fieldset>
                </div>
                <div class="buttons">
                    <span class="button"><g:submitButton name="create" class="save" value="${message(code: 'create', 'default': 'Create')}" /></span>
                </div>
            </g:form>
        </div>
    </body>
</html>

