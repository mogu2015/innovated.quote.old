<ul>
	<li><a class="returnFalse" href="">Tasks</a>	
		<ul>
			<li><a id="ajaxpageLinkCheck" href="pages/lipsum.html">Ajax/XHR Demo</a></li>
			<li><a id="jsonLink" href="data/json-windows-data.js">Json Demo</a></li>
			<li><a id="youtubeLinkCheck" href="pages/youtube.html">Iframe: YouTube</a></li>
			<li><a id="slideshareLinkCheck" href="pages/slideshare.html">Iframe: Slideshare</a></li>
			<li><a id="accordiantestLinkCheck" href="pages/accordian-demo.html">Accordian</a></li>
			<li><a id="clockLinkCheck" href="plugins/coolclock/">Widget: Clock</a></li>
			<li><a id="parametricsLinkCheck" href="plugins/parametrics/">Window Parametrics</a></li>
			<li class="divider"><a class="returnFalse arrow-right" href="">Tests</a>
				<ul>
					<li><a id="windoweventsLinkCheck" href="pages/events.html">Window Events</a></li>
					<li><a id="containertestLinkCheck" href="pages/lipsum.html">Container Test</a></li>
					<li><a id="iframetestLinkCheck" href="pages/iframetest.html">Iframe Test</a></li>
					<li><a id="noCanvasLinkCheck" href="pages/lipsum.html">No Canvas Body</a></li>
				</ul>
			</li>
			<li class="divider"><a class="returnFalse arrow-right" href="">Starters</a>
				<ul>
					<li><a target="_blank" href="demo-virtual-desktop.html">Virtual Desktop</a></li>
					<li><a target="_blank" href="demo-fixed-width.html">Fixed Width</a></li>
					<li><a target="_blank" href="demo-no-toolbars.html">No Toolbars</a></li>
					<li><a target="_blank" href="demo-no-desktop.html">No Desktop</a></li>
					<li><a target="_blank" href="demo-modal-only.html">Modal Only</a></li>
				</ul>
			</li>
		</ul>
	</li>
	<li><a class="returnFalse" href="">View</a>
		<ul>
			<li><a id="cascadeLink" href="">Cascade Windows</a></li>
			<li><a id="tileLink" href="">Tile Windows</a></li>
			<li><a id="minimizeLink" href="">Minimize All Windows</a></li>
			<li><a id="closeLink" href="">Close All Windows</a></li>
		</ul>
	</li>
	<li><a class="returnFalse" href="">Help</a>
		<ul>
			<li><a id="featuresLinkCheck" href="pages/features.html">Features</a></li>
			<li class="divider"><a target="_blank" href="http://mochaui.com/docs/">Documentation</a></li>
			<li class="divider"><a id="aboutLink" href="pages/about.html">About</a></li>
		</ul>
	</li>
</ul>	

<div class="toolbox divider2">
	<div id="spinnerWrapper"><div id="spinner"></div></div>		
</div>

<div class="toolbox divider2">
	<img src="images/icons/cog.gif" onclick="MochaUI.notification('Do Something');" width="16" height="16" alt="" />
	<img src="images/icons/windows.gif" onclick="MochaUI.notification('Do Something');" width="16" height="16" alt="" />
	<img src="images/icons/sheet.gif" onclick="MochaUI.notification('Do Something');" width="16" height="16" alt="" />
</div>