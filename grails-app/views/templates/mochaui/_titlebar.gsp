<div id="desktopTitlebar">
	<h1 class="applicationTitle"><a href="#" onclick="MochaUI.notification('Do Something');return false;">IRIS</a></h1>
	<h2 class="tagline">IRIS :: <span class="taglineEm">inNovated Reporting &amp; Information System</span></h2>
	<div id="topNav">
		<ul class="menu-right">
			<li>Welcome ${loggedInUserInfo(field:'username')}!</li>
			<li><g:link controller="logout" action="index">Sign Out</g:link></li>
		</ul>
	</div>
</div>