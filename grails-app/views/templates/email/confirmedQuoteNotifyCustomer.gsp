<%@ page contentType="text/html" %>
<p style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 12px;">
    <g:if test="${firstName}">
        Hi <span style="color:maroon;">${firstName}</span>,
    </g:if>
    <g:else>
        To <span style="color:maroon;">whom it may concern</span>,
    </g:else>
</p>
<p style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 12px; line-height : 16px;margin-bottom: 20px;">
    Pricing has been received from several dealers which has enabled the quote to be generated and can be viewed <a href="${uri}">here</a>.
</p>
<p style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 12px; line-height : 16px;margin-bottom: 20px;">
    <span style="font-weight: bolder;"><u>Securing your Vehicle</u></span><br/><br/>
    <span>
        To proceed to secure this vehicle, please review the quotation and select the <span style="color: #0B9100;font-weight: bolder;font-style: italic;">Accept Quote</span> button. If finance has not been applied for, please provide your finance application and once approved, your vehicle can then be ordered when you are ready to do so.
    </span>
</p>

<p style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 12px; line-height : 16px;margin-bottom: 20px;">
    You are very close to driving away in your car!
</p>

<p style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 12px;">
    Best Regards,<br/>
</p>

<p style="margin: 0in; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 12px;"
   lang="en-US">Client Services</p>

<p style="margin:0in;font-family:Verdana, Arial, Helvetica, sans-serif;font-size:12px" lang="en-US"><span
        style="font-weight:bold;color:#305C8F">in</span><span style="font-weight:bold;
color:#AD0027">Novated</span><span style="font-weight:bold;color:#150692">&nbsp;Leasing
Australia</span></p>

<p style="margin:0in;font-family:Verdana, Arial, Helvetica, sans-serif;font-size:12px" lang="en-US"><span
        style="font-weight:bold;color:#920193">Service &amp; Simplicity.</span><span
        style="font-weight:bold;color:#8F0005">&nbsp;</span><span
        style="font-weight: bold; font-style: italic;">Why Settle for Less?</span></p>

<p style="margin:0in;font-family:Verdana, Arial, Helvetica, sans-serif;font-size:12px" lang="en-US"><span
        style="font-weight:bold;color:#140691">Novated Leases,&nbsp;</span><span
        style="font-weight:bold;color:#0B9100;">Salary Packaging&nbsp;</span><span
        style="font-weight: bold;">&amp; Fleet Management</span></p>

<table border="0" cellspacing="0" cellpadding="0"
       style="margin: 0in; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 12px;">
    <tr><td width="20%">Phone :</td><td>1300 787 785</td></tr>
    <tr><td width="20%">Fax :</td><td>1300 667 171</td></tr>
</table>

<p style="margin:0in;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 12px;">
    <a href="mailto:ClientServices@inNovated.com.au">ClientServices@inNovated.com.au</a>
</p>

<p style="margin:0in;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 12px;">
    <a href="http://www.innovated.com.au">www.inNovated.com.au</a>
</p>
