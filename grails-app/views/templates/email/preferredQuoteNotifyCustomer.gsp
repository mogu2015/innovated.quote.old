<%@ page contentType="text/html" %>
<p style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 12px;">
    <g:if test="${firstName}">
        Hi <span style="color:maroon;">${firstName}</span>,
    </g:if>
    <g:else>
        To <span style="color:maroon;">whom it may concern</span>,
    </g:else>
</p>

<p style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 12px; line-height : 16px;margin-bottom: 20px;">
    Great! You're getting closer to getting your <span
        style="font-weight: bolder;color: #8F0005;">${vehicle}</span>. You've started on a journey of greater financial freedom that will assist you to achieve far more than previously possible. The opportunity is now and it's yours.
</p>

<p style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 12px; line-height : 16px;margin-bottom: 20px;">
    What Happens Now? Just 3 simple steps.
</p>

<g:if test="${!priceObtained}">
    <p style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 12px; line-height : 16px;margin-bottom: 20px;">
        <span style="font-weight: bolder;color: #150692;">(1) Dealer Pricing & Quote</span><br/>
        <span>
            We will contact several dealers to bid for your business and you will get the best purchase price available. We will add the anticipated running expenses (fuel, servicing, etc) and then set it up into a simple and affordable <span
                style="background-color: #eded00;font-weight: bolder;">${payCyle}</span> payment, deducted directly from your salary, largely or fully pre-tax. We can then pay for your everyday expenses directly for you and you will then be free from the stress of receiving untimely maintenance, registration and insurance bills.
        </span>
    </p>

    <p style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 12px; line-height : 16px;margin-bottom: 20px;">
        <span style="font-weight: bolder;color: #150692;">(2) Approval & Order</span><br/>
        <span>
            Once you are happy to proceed, your finance application is submitted for approval, if it hasn't been done already. it usually happens the same day or within 24 hours. We then order your car.
        </span>
    </p>

    <p style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 12px; line-height : 16px;margin-bottom: 20px;">
        <span style="font-weight: bolder;color: #150692;">(3) Documentation & Settlement</span><br/>
        <span>
            We then liaise with the dealer to confirm when your car will be ready for delivery. As soon as the dealer forwards the Tax Invoice for your car, we will forward the documentation to sign. When we receive the signed documentation, the financier is instructed to release the funds to the dealer and before you know it, you're driving away in your car!
        </span>
    </p>
</g:if>
<g:else>
    <p style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 12px; line-height : 16px;margin-bottom: 20px;">
        <span style="font-weight: bolder;color: #150692;">(1) Dealer Pricing & Quote</span><br/>
        <span>
            As you have already arranged the vehicle, we will add the anticipated running expenses and then set it up into a simple and affordable <span
                style="background-color: #eded00;font-weight: bolder;">${payCyle}</span> payment, deducted directly from your salary, largely or fully pre-tax. We can then pay for your everyday expenses directly for you and you will then be free from the stress of receiving untimely maintenance, registration and insurance bills.
        </span>
    </p>

    <p style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 12px; line-height : 16px;margin-bottom: 20px;">
        <span style="font-weight: bolder;color: #150692;">(2) Approval & Order</span><br/>
        <span>
            Once you are happy to proceed, your finance application is submitted for approval, if it hasn't been done already. it usually happens the same day or within 24 hours. We then order your car.
        </span>
    </p>

    <p style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 12px; line-height : 16px;margin-bottom: 20px;">
        <span style="font-weight: bolder;color: #150692;">(3) Documentation & Settlement</span><br/>
        <span>
            Once approval is received, usually the same day or within 24 hours, we will forward the documentation to sign. When we receive the signed documentation, the financier is instructed to release the funds to the dealer <span
                style="font-style: italic;">(or '<span
                    style="font-weight: bolder;">owner</span>' if a private sale, or '<span
                    style="font-weight: bolder;">financier</span>' if a re-finance)
        </span> and before you know it, you're driving away in your car!
        </span>
    </p>
</g:else>

<hr/>

<p style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 12px;">
    Best Regards,<br/>
</p>

<p style="margin: 0in; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 12px;"
   lang="en-US">Client Services</p>

<p style="margin:0in;font-family:Verdana, Arial, Helvetica, sans-serif;font-size:12px" lang="en-US"><span
        style="font-weight:bold;color:#305C8F">in</span><span style="font-weight:bold;
color:#AD0027">Novated</span><span style="font-weight:bold;color:#150692">&nbsp;Leasing
Australia</span></p>

<p style="margin:0in;font-family:Verdana, Arial, Helvetica, sans-serif;font-size:12px" lang="en-US"><span
        style="font-weight:bold;color:#920193">Service &amp; Simplicity.</span><span
        style="font-weight:bold;color:#8F0005">&nbsp;</span><span
        style="font-weight: bold; font-style: italic;">Why Settle for Less?</span></p>

<p style="margin:0in;font-family:Verdana, Arial, Helvetica, sans-serif;font-size:12px" lang="en-US"><span
        style="font-weight:bold;color:#140691">Novated Leases,&nbsp;</span><span
        style="font-weight:bold;color:#0B9100;">Salary Packaging&nbsp;</span><span
        style="font-weight: bold;">&amp; Fleet Management</span></p>

<table border="0" cellspacing="0" cellpadding="0"
       style="margin: 0in; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 12px;">
    <tr><td width="20%">Phone :</td><td>1300 787 785</td></tr>
    <tr><td width="20%">Fax :</td><td>1300 667 171</td></tr>
</table>

<p style="margin:0in;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 12px;">
    <a href="mailto:ClientServices@inNovated.com.au">ClientServices@inNovated.com.au</a>
</p>

<p style="margin:0in;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 12px;">
    <a href="http://www.innovated.com.au">www.inNovated.com.au</a>
</p>