<%@ page contentType="text/html" %>
<p style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 12px;">
    <g:if test="${firstName}">
        Hi <span style="color:maroon;">${firstName}</span>,
    </g:if>
    <g:else>
        To <span style="color:maroon;">whom it may concern</span>,
    </g:else>
</p>
<g:if test="${annualSaving}">
    <p style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 12px; line-height : 16px;margin-bottom: 20px;">
        You have received the quote on the <span style="font-weight: bolder;color: #150692;">${vehicle}</span> and in proceeding, you will save approximately
        <span style="font-weight: bolder;color: #150692;">
        ${formatNumber(number: annualSaving.toDouble(), type: 'currency', currencyCode: "AUD", currencySymbol: "\$ ", minFractionDigits: 2, maxFractionDigits: 2)}
        </span>
        every year, which adds up to
        <span style="font-weight: bolder;color: #150692;">
        ${formatNumber(number: overallSaving.toDouble(), type: 'currency', currencyCode: "AUD", currencySymbol: "\$ ", minFractionDigits: 2, maxFractionDigits: 2)}
        </span>
        over the term of the lease.
    </p>
    <p style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 12px; line-height : 16px;margin-bottom: 20px;">
        Just imagine what you could do with that saving! You could save years and a whole lot of interest on your mortgage, or put the money towards a mortgage, pay for annual family holidays, school fees or everyday bills, just by paying for your car pre-tax!
    </p>
</g:if>
<g:else>
    <p style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 12px; line-height : 16px;margin-bottom: 20px;">
        Welcome to inNovated Leasing!
    </p>
</g:else>

<p style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 12px; line-height : 16px;margin-bottom: 20px;">
    Our inNovated approach means its easier for you to get and pay for the vehicle you want. We start by negotiating pricing for you without the hassle. We also assist you in removing the worry of vehicle related financial stress and provide you with more financial choices that improves your life.
</p>
<p style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 12px; line-height : 16px;margin-bottom: 20px;">
    As soon as you decide which vehicle you want, you will be one step closer to experiencing the freedom and excitement that inNovated Leasing clients can enjoy. The opportunity awaits. Its closer than you think.
</p>
<p style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 12px; line-height : 16px;margin-bottom: 20px;">
    Contact us to learn more.
</p>
<!-- (if not additional quotations are generated and no vehicle is selected i.e.: no progression for 5 working days) -->
<!--
<p style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 12px; line-height : 16px;margin-bottom: 20px;">
    We've noticed that you haven't generated any quotes recently or selected a preferred vehicle. We're keen to help you take advantage of the opportunities that are waiting to be taken up, just by paying for your vehicle differently. Don't let the current opportunities become future regrets. We would love to help. Contact us to see how we can help.
</p>
<p style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 12px; line-height : 16px;margin-bottom: 20px;">
    <span style="font-weight: bolder;font-style: italic;"><u>A little known fact</u>
    </span><i>- A new vehicle paid for as a Novated Lease i.e.: pre-tax, often costs less than paying for an existing or older car entirely from take-home salary?</i>
</p>
<p style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 12px; line-height : 16px;margin-bottom: 20px;">
    Contact us to see how you could both upgrade and save.
</p>
-->
<hr/>

<p style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 12px;">
    Best Regards,<br/>
</p>

<p style="margin: 0in; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 12px;"
   lang="en-US">Client Services</p>

<p style="margin:0in;font-family:Verdana, Arial, Helvetica, sans-serif;font-size:12px" lang="en-US"><span
        style="font-weight:bold;color:#305C8F">in</span><span style="font-weight:bold;
color:#AD0027">Novated</span><span style="font-weight:bold;color:#150692">&nbsp;Leasing
Australia</span></p>

<p style="margin:0in;font-family:Verdana, Arial, Helvetica, sans-serif;font-size:12px" lang="en-US"><span
        style="font-weight:bold;color:#920193">Service &amp; Simplicity.</span><span
        style="font-weight:bold;color:#8F0005">&nbsp;</span><span
        style="font-weight: bold; font-style: italic;">Why Settle for Less?</span></p>

<p style="margin:0in;font-family:Verdana, Arial, Helvetica, sans-serif;font-size:12px" lang="en-US"><span
        style="font-weight:bold;color:#140691">Novated Leases,&nbsp;</span><span
        style="font-weight:bold;color:#0B9100;">Salary Packaging&nbsp;</span><span
        style="font-weight: bold;">&amp; Fleet Management</span></p>

<table border="0" cellspacing="0" cellpadding="0"
       style="margin: 0in; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 12px;">
    <tr><td width="20%">Phone :</td><td>1300 787 785</td></tr>
    <tr><td width="20%">Fax :</td><td>1300 667 171</td></tr>
</table>

<p style="margin:0in;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 12px;">
    <a href="mailto:ClientServices@inNovated.com.au">ClientServices@inNovated.com.au</a>
</p>

<p style="margin:0in;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 12px;">
    <a href="http://www.innovated.com.au">www.inNovated.com.au</a>
</p>
 
