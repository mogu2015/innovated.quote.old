<%@ page contentType="text/html" %>
<p style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 12px;">
    To <span style="color:maroon;">whom it may concern</span>,
</p>

<p style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 12px; line-height : 16px;margin-bottom: 20px;">
    A new user has successfully registered for (& confirmed) access to inNovated Leasing's online quoting system.
</p>

<p style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 12px; line-height : 16px;margin-top: 20px;">
<ul style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 12px; line-height : 16px;">
    <li style="margin-bottom: 10px;">
        Username:&nbsp;&nbsp;${userName}
    </li>
    <li style="margin-bottom: 10px;">
        Name:&nbsp;&nbsp;${fullName}
    </li>
    <li style="margin-bottom: 10px;">
        Phone/Mobile:&nbsp;&nbsp;${phone}
    </li>
    <li>
        Email:&nbsp;&nbsp;${email}
    </li>
</ul>
</p>
<hr/>
<p style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 12px;">
    Best Regards,<br/>
</p>

<p style="margin: 0in; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 12px;"
   lang="en-US">Client Services</p>

<p style="margin:0in;font-family:Verdana, Arial, Helvetica, sans-serif;font-size:12px" lang="en-US"><span
        style="font-weight:bold;color:#305C8F">in</span><span style="font-weight:bold;
color:#AD0027">Novated</span><span style="font-weight:bold;color:#150692">&nbsp;Leasing
Australia</span></p>

<p style="margin:0in;font-family:Verdana, Arial, Helvetica, sans-serif;font-size:12px" lang="en-US"><span
        style="font-weight:bold;color:#920193">Service &amp; Simplicity.</span><span
        style="font-weight:bold;color:#8F0005">&nbsp;</span><span
        style="font-weight: bold; font-style: italic;">Why Settle for Less?</span></p>

<p style="margin:0in;font-family:Verdana, Arial, Helvetica, sans-serif;font-size:12px" lang="en-US"><span
        style="font-weight:bold;color:#140691">Novated Leases,&nbsp;</span><span
        style="font-weight:bold;color:#0B9100;">Salary Packaging&nbsp;</span><span
        style="font-weight: bold;">&amp; Fleet Management</span></p>

<table border="0" cellspacing="0" cellpadding="0"
       style="margin: 0in; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 12px;">
    <tr><td width="20%">Phone :</td><td>1300 787 785</td></tr>
    <tr><td width="20%">Fax :</td><td>1300 667 171</td></tr>
</table>

<p style="margin:0in;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 12px;">
    <a href="mailto:ClientServices@inNovated.com.au">ClientServices@inNovated.com.au</a>
</p>

<p style="margin:0in;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 12px;">
    <a href="http://www.innovated.com.au">www.inNovated.com.au</a>
</p>