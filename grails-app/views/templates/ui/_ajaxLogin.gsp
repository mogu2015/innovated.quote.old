
<div id='ajaxLogin' title="IRIS Login">
	<div class='inner'>
		<div class='fheader'>Your session may have timed out. Please Login:</div>
		<form action='${request.contextPath}/j_spring_security_check' method='POST' id='ajaxLoginForm' name='ajaxLoginForm' class='cssform'>
			<input type="hidden" name="spring-security-redirect" value="/login/ajaxSuccess" />
			<p>
				<label for='j_username'>Username</label>
				<input type='text' class='text_' name='j_username' value='' />
			</p>
			<p>
				<label for='j_password'>Password</label>
				<input type='password' class='text_' name='j_password' value='' />
			</p>
			<p>
				<label for='_spring_security_remember_me'>Remember me</label>
				<input type='checkbox' class='chk' name='_spring_security_remember_me'>
			</p>
		</form>
	</div>
	<div style='display:none; text-align:left;' id='loginMessage'></div>
</div>

<script type='text/javascript'>
	function showLogin() { $("#ajaxLogin").dialog("open"); }
	function onSuccessfulLogin(who) { $("#ajaxLogin").hide(); }
	function authAjax() {
		$("#loginMessage").html("Sending request ...").show();
		var form = $("#ajaxLoginForm");
		$.ajax({
			url: form.attr("action"),
			type: "POST",
			data: form.serialize(),
			complete: function(xhr, stat) {
				if (xhr.responseText.success) {
					console.log("data: " + xhr);
					$("#ajaxLogin").dialog("close");
				}
			},
			dataType: "json"
		});
	}
	// init the login dialog...
	$("#ajaxLogin").dialog({
		autoOpen:false, height:300, width:350, modal:true,
		buttons: {
			"Login": function() { authAjax(); },
			Cancel: function() { $(this).dialog("close"); }
		}
	});
</script>