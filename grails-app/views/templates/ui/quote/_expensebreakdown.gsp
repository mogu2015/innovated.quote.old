<h3 class="topPad">Vehicle Expense Breakdown</h3>

<div class="formSet">

    <table id="vehicleExpenseTable" class="grid iris" style="table-layout:auto;">
        <thead>
        <tr>
            <td nowrap class="header sub-total last" style="text-align:left;"><g:message
                    code="quote.breakdown.table.header.category" default="Category"/></td>
            %{--<td class="header sub-total last" style="text-align:center;"><g:message--}%
            %{--code="quote.breakdown.table.header.weekly" default="Weekly"/></td>--}%
            %{--<td class="header sub-total last" style="text-align:center;"><g:message--}%
            %{--code="quote.breakdown.table.header.fortnightly" default="Fortnightly"/></td>--}%
            <td class="header sub-total last" style="text-align:center;"><g:message
                    code="quote.breakdown.table.header.${quoteInstance?.defaultPayFreq?.name}"
                    default="${quoteInstance?.defaultPayFreq?.name}"/></td>
            %{--<td class="header sub-total last" style="text-align:center;"><g:message--}%
                    %{--code="quote.breakdown.table.header.annually" default="Annually"/></td>--}%
            <td nowrap class="header sub-total last" style="text-align:left;"><g:message
                    code="quote.breakdown.table.header.supplier" default="Supplier"/></td>
        </tr>
        </thead>

        <g:if test="${quoteBreakdown.size() > 0}">
            <tbody>
            <g:set var="counter" value="${0}"/>
            <g:each in="${quoteBreakdown}" status="i" var="breakdownItem">
                <tr class="${(counter % 2) == 0 ? 'odd' : 'even'}">
                    <td nowrap class="rowHd left">${breakdownItem.category}</td>
                    <g:if test="${quoteInstance?.defaultPayFreq?.name?.toLowerCase() == 'weekly'}">
                        <td class="rowHd numeric">${formatNumber(number: breakdownItem.weekly, type: 'currency', currencyCode: "AUD", currencySymbol: "\$ ", minFractionDigits: 2, maxFractionDigits: 2)}</td>
                    </g:if>
                    <g:if test="${quoteInstance?.defaultPayFreq?.name?.toLowerCase() == 'fortnightly'}">
                        <td class="rowHd numeric">${formatNumber(number: breakdownItem.fortnightly, type: 'currency', currencyCode: "AUD", currencySymbol: "\$ ", minFractionDigits: 2, maxFractionDigits: 2)}</td>
                    </g:if>
                    <g:if test="${quoteInstance?.defaultPayFreq?.name?.toLowerCase() == 'monthly'}">
                        <td class="rowHd numeric">${formatNumber(number: breakdownItem.monthly, type: 'currency', currencyCode: "AUD", currencySymbol: "\$ ", minFractionDigits: 2, maxFractionDigits: 2)}</td>
                    </g:if>
                    <g:if test="${quoteInstance?.defaultPayFreq?.name?.toLowerCase() == 'bi-monthly'}">
                        <td class="rowHd numeric">${formatNumber(number: breakdownItem.bimonthly, type: 'currency', currencyCode: "AUD", currencySymbol: "\$ ", minFractionDigits: 2, maxFractionDigits: 2)}</td>
                    </g:if>
                    %{--<td class="rowHd numeric">${formatNumber(number: breakdownItem.annually, type: 'currency', currencyCode: "AUD", currencySymbol: "\$ ", minFractionDigits: 2, maxFractionDigits: 2)}</td>--}%
                    <td nowrap class="left">${breakdownItem.supplier}</td>
                </tr>
                <g:set var="counter" value="${counter + 1}"/>
            </g:each>
            </tbody>
        </g:if>

        <g:if test="${quoteBreakdownTotals.size() > 0}">
            <tbody>
            <g:each in="${quoteBreakdownTotals}" status="i" var="breakdownTotal">
                <tr class="${(counter % 2) == 0 ? 'odd' : 'even'}">
                    <td nowrap
                        class="${i == 0 ? "sub-total benefit-amt" : (i == 1) ? "gst" : "grand-total"} rowHd">${breakdownTotal.category}</td>
                    <g:if test="${quoteInstance?.defaultPayFreq?.name?.toLowerCase() == 'weekly'}">
                        <td class="${i == 0 ? "sub-total benefit-amt" : (i == 1) ? "gst" : "grand-total"} rowHd numeric">${formatNumber(number: breakdownTotal.weekly, type: 'currency', currencyCode: "AUD", currencySymbol: "\$ ", minFractionDigits: 2, maxFractionDigits: 2)}</td>
                    </g:if>
                    <g:if test="${quoteInstance?.defaultPayFreq?.name?.toLowerCase() == 'fortnightly'}">
                        <td class="${i == 0 ? "sub-total benefit-amt" : (i == 1) ? "gst" : "grand-total"} rowHd numeric">${formatNumber(number: breakdownTotal.fortnightly, type: 'currency', currencyCode: "AUD", currencySymbol: "\$ ", minFractionDigits: 2, maxFractionDigits: 2)}</td>
                    </g:if>
                    <g:if test="${quoteInstance?.defaultPayFreq?.name?.toLowerCase() == 'monthly'}">
                        <td class="${i == 0 ? "sub-total benefit-amt" : (i == 1) ? "gst" : "grand-total"} rowHd numeric">${formatNumber(number: breakdownTotal.monthly, type: 'currency', currencyCode: "AUD", currencySymbol: "\$ ", minFractionDigits: 2, maxFractionDigits: 2)}</td>
                    </g:if>
                    <g:if test="${quoteInstance?.defaultPayFreq?.name?.toLowerCase() == 'bi-monthly'}">
                        <td class="${i == 0 ? "sub-total benefit-amt" : (i == 1) ? "gst" : "grand-total"} rowHd numeric">${formatNumber(number: breakdownTotal.bimonthly, type: 'currency', currencyCode: "AUD", currencySymbol: "\$ ", minFractionDigits: 2, maxFractionDigits: 2)}</td>
                    </g:if>
                    %{--<td class="${i == 0 ? "sub-total benefit-amt" : (i == 1) ? "gst" : "grand-total"} rowHd numeric">${formatNumber(number: breakdownTotal.annually, type: 'currency', currencyCode: "AUD", currencySymbol: "\$ ", minFractionDigits: 2, maxFractionDigits: 2)}</td>--}%
                    <td nowrap
                        class="${i == 0 ? "sub-total benefit-amt" : (i == 1) ? "gst" : "grand-total"}">${breakdownTotal.supplier}</td>
                </tr>
                <g:set var="counter" value="${counter + 1}"/>
            </g:each>
            </tbody>
        </g:if>

    </table>

</div>