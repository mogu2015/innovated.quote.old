<%@ page import="com.innovated.iris.domain.*" %>
<%@ page import="com.innovated.iris.enums.*" %>

<div>
    <div class="quote-state-list">
        <g:set var="states" value="${[]}"/>
        <% states = QuoteStatus.lookup.collect { it.value }.sort { it.code } %>
        <g:each in="${states}" status="sdx" var="s">
            <g:if test="${s.name != 'Cancelled' && s.name != 'Documentation' && s.name != 'Settled'}">
                <g:if test="${quote.status == s}">
                    <div class="quote-state-wrap">
                        <g:link action="viewquote" id="${quote.id}" title="View quote ${formatNumber(number: quote?.id, format: message(code: 'quote.number.mask'))}">
                            <div class="quote-state quote-state-selected qstat-${s.name.toLowerCase()}">
                                <span>${s.name}</span>
                            </div>
                        </g:link>
                    </div>
                </g:if>
                <g:else>
                    <div class="quote-state-wrap">
                        <div class="quote-state qstat-${s.name.toLowerCase()}-gs">
                            <span>${s.name}</span>
                        </div>
                    </div>
                </g:else>
            </g:if>

        </g:each>
    </div>
</div>
