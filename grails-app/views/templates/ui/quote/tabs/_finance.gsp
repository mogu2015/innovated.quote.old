<script type="text/javascript">
function toggleFinAppl(fadeout,fadein) {
	$(fadeout).fadeOut('fast', function() {
		$(fadein).fadeIn('fast');
	});
}
$(document).ready(function() {
	$("#startFinAppl").click(function(e) {
		e.preventDefault();
		toggleFinAppl("#noFinAppl", "#hasFinAppl");
		return false;
	});
	$("#cancelFinAppl").click(function(e) {
		e.preventDefault();
		toggleFinAppl("#hasFinAppl", "#noFinAppl");
		return false;
	});
});
</script>

<g:if test="${quoteInstance?.finApplications?.size() == 0}">
<div id="noFinAppl" class="emptyListPanel">
	<h1><a id="startFinAppl" href="#"><g:message code="ui.h1.label.noquotefinance" default="Start a New Finance Application" /></a></h1>
	<p><g:message code="ui.p.label.noquotefinance" default="You currently have NO finance application(s) assosciated with this novated budget quote! Click here to commence one..." /></p>
</div>
<div id="hasFinAppl" style="display:none;">
	<g:render template="/templates/ui/finance/finApplTmpl" bean="${emptyFinAppl}" var="appl"/>
</div>
</g:if><g:else>
<div id="hasFinAppl">
	<g:render template="/templates/ui/finance/finApplTmpl" bean="${quoteInstance?.finApplications?.get(0)}" var="appl"/>
</div>
</g:else>

