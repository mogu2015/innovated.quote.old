<%@ page import="com.innovated.iris.domain.*" %>
<%@ page import="com.innovated.iris.enums.*" %>
<g:set var="aqn" value="${formatNumber(number: quoteInstance?.id, format: message(code: 'quote.number.mask'))}"/>
<g:set var="AMT_SCALE" value="${2}"/>
<g:set var="PRCNT_SCALE" value="${1}"/>
<g:set var="show_ecm" value="${false}"/>
<g:set var="best_type" value="${"00000"}"/>
<table id="benefitComparisonTable" class="grid iris">
    <thead>
    <tr>
        <td class="header last"><g:message code="quote.comparison.table.header.category" default=""/></td>
        <g:each in="${quoteInstance.comparisons}" var="hdr">
            %{--<g:if test="${hdr.isBest || hdr.name == 'Not Packaged'}">--}%
                %{--<g:if test="${quoteInstance?.allowsFbtEcm}">--}%
                    %{--<g:set var="show_ecm" value="${true}"/>--}%
                    %{--<g:set var="best_type" value="${hdr.name}"/>--}%
                %{--</g:if>--}%
                %{--<td class="header sub-total last" style="text-align:center;">${hdr.name}</td>--}%
            %{--</g:if>--}%
            %{--<g:else>--}%
                %{--<g:if test="${hdr.name.contains(best_type) && show_ecm}">--}%
                    %{--<td class="header sub-total last" style="text-align:center;">${hdr.name}</td>--}%
                %{--</g:if>--}%
            %{--</g:else>--}%
            <td class="header sub-total last" style="text-align:center;">${hdr.name}</td>
        </g:each>
    </tr>

    <tr>
        <td class="header">&nbsp;</td>
        <g:each in="${quoteInstance.comparisons}" var="defrow">
            <td class="header" style="text-align:center;">
                <g:if test="${defrow.isDefault}"><b><i>(Default)</i></b></g:if>
                <g:else>&nbsp;</g:else>
            </td>
            %{--<g:if test="${defrow.isBest || defrow.name == 'Not Packaged'}">--}%
                %{--<td class="header" style="text-align:center;">--}%
                    %{--<g:if test="${defrow.isDefault}"><b><i>(Default)</i></b></g:if>--}%
                    %{--<g:else>&nbsp;</g:else>--}%
                %{--</td>--}%
            %{--</g:if>--}%
            %{--<g:else>--}%
                %{--<g:if test="${defrow.name.contains(best_type) && show_ecm}">--}%
                    %{--<td class="header" style="text-align:center;">--}%
                        %{--<g:if test="${defrow.isDefault}"><b><i>(Default)</i></b></g:if>--}%
                        %{--<g:else>&nbsp;</g:else>--}%
                    %{--</td>--}%
                %{--</g:if>--}%
            %{--</g:else>--}%
        </g:each>
    </tr>

    </thead>
    <tbody>
    <g:set var="best_col" value="${0}"/>
    <g:each in="${comparisonItemTypes}" var="item" status="cnt">
        <tr>
            <td class="rowHd ${(item.isTotal() || cnt == 0) ? "sub-total" : (item.isSubTotal()) ? "grand-total" : ""}">${item.getName()}</td>
            <g:each in="${quoteInstance.comparisons}" var="comp" status="compIdx">
                <g:if test="${comp.isBest}">
                    <g:set var="best_col" value="${compIdx}"/>
                </g:if>
                <td class="numeric ${(item.isTotal() || cnt == 0) ? "sub-total" : (item.isSubTotal()) ? "grand-total" : ""} ${comp.isBest ? "best" : ""}">
                    <g:set var="compItem" value="${comp.items.find { it.type == item }}"/>
                    <g:if test="${compItem}">
                        ${formatNumber(number: compItem.amount, type: 'currency', currencyCode: "AUD", currencySymbol: "\$ ", minFractionDigits: AMT_SCALE, maxFractionDigits: AMT_SCALE)}
                    </g:if>
                    <g:else>
                        ${formatNumber(number: 0, type: 'currency', currencyCode: "AUD", currencySymbol: "\$ ", minFractionDigits: AMT_SCALE, maxFractionDigits: AMT_SCALE)}
                    </g:else>
                </td>
                %{--<g:else>--}%
                    %{--<g:if test="${comp.name.contains(best_type) && show_ecm}">--}%
                        %{--<td class="numeric ${(item.isTotal() || cnt == 0) ? "sub-total" : (item.isSubTotal()) ? "grand-total" : ""} ${comp.isBest ? "best" : ""}">--}%
                            %{--<g:set var="compItem" value="${comp.items.find { it.type == item }}"/>--}%
                            %{--<g:if test="${compItem}">--}%
                                %{--${formatNumber(number: compItem.amount, type: 'currency', currencyCode: "AUD", currencySymbol: "\$ ", minFractionDigits: AMT_SCALE, maxFractionDigits: AMT_SCALE)}--}%
                            %{--</g:if>--}%
                            %{--<g:else>--}%
                                %{--${formatNumber(number: 0, type: 'currency', currencyCode: "AUD", currencySymbol: "\$ ", minFractionDigits: AMT_SCALE, maxFractionDigits: AMT_SCALE)}--}%
                            %{--</g:else>--}%
                        %{--</td>--}%
                    %{--</g:if>--}%
                %{--</g:else>--}%
            </g:each>
        </tr>
    </g:each>

    <tr>
        <td class="grand-total benefit-amt" style="text-align:right;font-size: 13px;" colspan="2">Annual Benefit Received</td>
        <g:set var="noPackageNet" value="${quoteInstance.comparisons.get(0).items[-1]}"/>
        <g:each in="${quoteInstance.comparisons}" var="diff" status="increase">
            <g:if test="${increase > 0}">
                <g:set var="diffItem" value="${diff.items[-1]}"/>
                <g:set var="diffAmount" value="${(diffItem) ? (diffItem.amount - noPackageNet.amount) : 0}"/>
                <td class="numeric benefit-amt grand-total ${(diffAmount >= 0) ? "" : "negative-amount"} ${(increase == best_col) ? "best" : ""}">
                    ${formatNumber(number: diffAmount, type: 'currency',currencyCode: "AUD", currencySymbol: "\$ ", minFractionDigits: AMT_SCALE, maxFractionDigits: AMT_SCALE)}
                </td>
            </g:if>
            %{--<g:if test="${diff.isBest || diff.name == 'Not Packaged'}">--}%
                %{--<g:if test="${increase > 0}">--}%
                    %{--<g:set var="diffItem" value="${diff.items[-1]}"/>--}%
                    %{--<g:set var="diffAmount" value="${(diffItem) ? (diffItem.amount - noPackageNet.amount) : 0}"/>--}%
                    %{--<td class="numeric benefit-amt grand-total ${(diffAmount >= 0) ? "" : "negative-amount"} ${(increase == best_col) ? "best" : ""}">--}%
                        %{--${formatNumber(number: diffAmount, type: 'currency',currencyCode: "AUD", currencySymbol: "\$ ", minFractionDigits: AMT_SCALE, maxFractionDigits: AMT_SCALE)}--}%
                    %{--</td>--}%
                %{--</g:if>--}%
            %{--</g:if>--}%
            %{--<g:else>--}%
                %{--<g:if test="${diff.name.contains(best_type) && show_ecm}">--}%
                    %{--<g:if test="${increase > 0}">--}%
                        %{--<g:set var="diffItem" value="${diff.items[-1]}"/>--}%
                        %{--<g:set var="diffAmount" value="${(diffItem) ? (diffItem.amount - noPackageNet.amount) : 0}"/>--}%
                        %{--<td class="numeric benefit-amt grand-total ${(diffAmount >= 0) ? "" : "negative-amount"} ${(increase == best_col) ? "best" : ""}">--}%
                            %{--${formatNumber(number: diffAmount, type: 'currency', currencyCode: "AUD", currencySymbol: "\$ ", minFractionDigits: AMT_SCALE, maxFractionDigits: AMT_SCALE)}--}%
                        %{--</td>--}%
                    %{--</g:if>--}%
                %{--</g:if>--}%
            %{--</g:else>--}%
        </g:each>
    </tr>
    <tr>
        <td class="grand-total benefit-prcnt" style="text-align:right;font-size: 13px;"
            colspan="2">% Net Salary Increase</td>
        <g:set var="noPackageNet" value="${quoteInstance.comparisons.get(0).items[-1]}"/>
        <g:each in="${quoteInstance.comparisons}" var="diff" status="increase">
            <g:if test="${increase > 0}">
                <g:set var="diffItem" value="${diff.items[-1]}"/>
                <g:set var="diffAmount" value="${(diffItem) ? (diffItem.amount - noPackageNet.amount) : 0}"/>
                <td class="numeric benefit-prcnt grand-total ${(diffAmount >= 0) ? "" : "negative-amount"} ${(increase == best_col) ? "best" : ""}">
                    ${formatNumber(number: diffAmount / noPackageNet.amount, type: 'percent', minFractionDigits: PRCNT_SCALE, maxFractionDigits: PRCNT_SCALE)}
                </td>
            </g:if>
            %{--<g:if test="${diff.isBest || diff.name == 'Not Packaged'}">--}%
                %{--<g:if test="${increase > 0}">--}%
                    %{--<g:set var="diffItem" value="${diff.items[-1]}"/>--}%
                    %{--<g:set var="diffAmount" value="${(diffItem) ? (diffItem.amount - noPackageNet.amount) : 0}"/>--}%
                    %{--<td class="numeric benefit-prcnt grand-total ${(diffAmount >= 0) ? "" : "negative-amount"} ${(increase == best_col) ? "best" : ""}">--}%
                        %{--${formatNumber(number: diffAmount / noPackageNet.amount, type: 'percent', minFractionDigits: PRCNT_SCALE, maxFractionDigits: PRCNT_SCALE)}--}%
                    %{--</td>--}%
                %{--</g:if>--}%
            %{--</g:if>--}%
            %{--<g:else>--}%
                %{--<g:if test="${diff.name.contains(best_type) && show_ecm}">--}%
                    %{--<g:if test="${increase > 0}">--}%
                        %{--<g:set var="diffItem" value="${diff.items[-1]}"/>--}%
                        %{--<g:set var="diffAmount" value="${(diffItem) ? (diffItem.amount - noPackageNet.amount) : 0}"/>--}%
                        %{--<td class="numeric benefit-prcnt grand-total ${(diffAmount >= 0) ? "" : "negative-amount"} ${(increase == best_col) ? "best" : ""}">--}%
                            %{--${formatNumber(number: diffAmount / noPackageNet.amount, type: 'percent', minFractionDigits: PRCNT_SCALE, maxFractionDigits: PRCNT_SCALE)}--}%
                        %{--</td>--}%
                    %{--</g:if>--}%
                %{--</g:if>--}%
            %{--</g:else>--}%
        </g:each>
    </tr>

    <tr>
        <td colspan="2" class="last"></td>
        <g:each in="${quoteInstance.comparisons}" var="p" status="p_idx">
            <g:if test="${p.isBest}">
                <g:if test="${p_idx > 0}">
                    <td class="print-quote-btn last">
                    %{--<g:link controller="quote" action="export" params="[id: aqn + '-' + p.id + '.pdf']" target="_blank"--}%
                    %{--title="Print this quote using the ${p.name} method">--}%
                    %{--<img alt="Print Quote" src="${resource(dir: 'images', file: 'printBtn.png')}"/>--}%
                    %{--</g:link>--}%
                        <g:ifAnyGranted role="ROLE_SUPER,ROLE_ADMIN,ROLE_USER">
                            <g:if test="${quoteInstance?.status == QuoteStatus.QUOTE_ACTUAL && quoteInstance?.pricingConfirmed != null}">
                                <g:link controller="quote" action="accept" params="[comparison: p.id]"
                                        title="Accept this quote using the ${p.name} method">
                                    <img alt="Accept Quote" src="${resource(dir: 'images', file: 'acceptBtn.png')}"/>
                                </g:link>
                            </g:if>
                        </g:ifAnyGranted>
                        <div class="clearButton"></div>
                    </td>
                </g:if>
            </g:if>
            %{--<g:else>--}%
                %{--<g:if test="${p.name.contains(best_type) && show_ecm}">--}%
                    %{--<g:if test="${p_idx > 0}">--}%
                        %{--<td class="print-quote-btn last">--}%
                        %{--<g:link controller="quote" action="export" params="[id: aqn + '-' + p.id + '.pdf']" target="_blank"--}%
                        %{--title="Print this quote using the ${p.name} method">--}%
                        %{--<img alt="Print Quote" src="${resource(dir: 'images', file: 'printBtn.png')}"/>--}%
                        %{--</g:link>--}%
                            %{--<g:ifAnyGranted role="ROLE_SUPER,ROLE_ADMIN,ROLE_USER">--}%
                                %{--<g:if test="${quoteInstance?.status == QuoteStatus.QUOTE_ACTUAL && quoteInstance?.pricingConfirmed != null}">--}%
                                    %{--<g:link controller="quote" action="accept" params="[comparison: p.id]"--}%
                                            %{--title="Accept this quote using the ${p.name} method">--}%
                                        %{--<img alt="Accept Quote" src="${resource(dir: 'images', file: 'acceptBtn.png')}"/>--}%
                                    %{--</g:link>--}%
                                %{--</g:if>--}%
                            %{--</g:ifAnyGranted>--}%
                            %{--<div class="clearButton"></div>--}%
                        %{--</td>--}%
                    %{--</g:if>--}%
                %{--</g:if>--}%
            %{--</g:else>--}%

        </g:each>
    </tr>
    %{--<tr>--}%
        %{--<td colspan="2" class="last"></td>--}%
        %{--<g:each in="${quoteInstance.comparisons}" var="a" status="a_idx">--}%
            %{--<g:if test="${a_idx > 0}">--}%
                %{--<td class="accept-quote-btn last">--}%
                    %{--<g:link controller="quote" action="accept" params="[id: aqn + '-' + a.id + '.pdf']" target="_blank"--}%
                            %{--title="Accept this quote using the ${a.name} method">--}%
                        %{--<img alt="Accept Quote" src="${resource(dir: 'images', file: 'acceptBtn.png')}"/>--}%
                    %{--</g:link>--}%
                    %{--<div class="clearButton"></div>--}%
                %{--</td>--}%
            %{--</g:if>--}%
        %{--</g:each>--}%
    %{--</tr>--}%
    </tbody>
</table>

