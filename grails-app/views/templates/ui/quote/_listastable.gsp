<%@ page import="com.innovated.iris.domain.*" %>
<%@ page import="com.innovated.iris.enums.*" %>

<script type="text/javascript">
    $(document).ready(function () {
        var setTable = $("#quotes");
        setTable.tablesorter({
            headers: {0: {sorter: false}}
        });
        $("#filter").keyup(function () {
            $.uiTableFilter(setTable, this.value, null, function () {
                // update the table count label
                var visibleRows = setTable.find("tr:visible").length - 1;
                $(".listTotal span").text(visibleRows);
            });
        });
    });
</script>

<g:set var="preferred" value="${[]}"/>
<g:each in="${quoteList}" status="idx" var="q">
    <g:if test="${q.status != QuoteStatus.QUOTE_CREATED && q.status != QuoteStatus.QUOTE_CANCELLED}"><% preferred << q %></g:if>
</g:each>

<g:if test="${preferred?.size() > 0}">
    <div class="formBg" style="margin-top:20px;margin-bottom:20px;">
        <div class="formWrap">
            <g:each in="${preferred}" status="idx" var="q">
                <h3>Preferred Quote:</h3>

                <div class="formSet">
                    <table class="grid">
                        <tbody>
                        <tr>
                            <th>Quote #</th>
                            <td class="special"><g:link action="viewquote" id="${q.id}"
                                                        title="View quote ${formatNumber(number: q?.id, format: message(code: 'quote.number.mask'))}"><g:formatNumber
                                        number="${q?.id}" format="${message(code: 'quote.number.mask')}"/></g:link></td>
                            <td class="spacer">&nbsp;</td>
                            <th>Km's / Annum</th>
                            <td class="special"><g:formatNumber number="${q?.kmPerYear}" type="number" maxFractionDigits="0"/> km</td>
                        </tr>
                        <tr>
                            <th>Vehicle</th>
                            <td>${q?.vehicle?.encodeAsHTML()}</td>
                            <td class="spacer">&nbsp;</td>
                            <th>Lease Term</th>
                            <td>${q?.leaseTerm.encodeAsHTML()} months</td>
                        </tr>
                        <tr>
                            <g:set var="employerTypes"
                                   value="${['BUCOGO', 'ASANDNSW', 'PUBLICNSW', 'REBATABLE', 'PBI0001']}"/>
                            <g:if test="${employerTypes.contains(q?.company?.employerCode)}">
                                <th class="last">Employer Type</th>
                            </g:if>
                            <g:else>
                                <th class="last">Employer</th>
                            </g:else>
                            <td class="last" colspan="4">
                                <g:if test="${q?.employment?.employer?.name}">
                                    ${q?.employment?.employer?.name?.encodeAsHTML()}
                                </g:if><g:else>
                                    ${q?.company?.tradingName?.encodeAsHTML()}
                                </g:else>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>

                <h3>Quotation Status:</h3>

                <div class="formSet" style="padding:10px 0;">
                    <div class="quote-state-list">
                        <g:set var="states" value="${[]}"/>
                        <% states = QuoteStatus.lookup.collect { it.value }.sort { it.code } %>
                        <g:each in="${states}" status="sdx" var="s">
                            <g:if test="${s.name != 'Cancelled' && s.name != 'Documentation' && s.name != 'Settled'}">
                                <g:if test="${q.status == s}">
                                    <div class="quote-state-wrap">
                                    %{--<g:link action="viewquote" id="${q.id}"><span>${s.name}</span></g:link>--}%
                                        <g:link action="viewquote" id="${q.id}"
                                                title="View quote ${formatNumber(number: q?.id, format: message(code: 'quote.number.mask'))}">
                                            <div class="quote-state quote-state-selected qstat-${s.name.toLowerCase()}">
                                                <span>${s.name}</span>
                                            </div>
                                        </g:link>
                                    </div>
                                </g:if>

                                <g:else>
                                    <div class="quote-state-wrap">
                                        <div class="quote-state qstat-${s.name.toLowerCase()}-gs">
                                            <span>${s.name}</span>
                                        </div>
                                    </div>
                                </g:else>
                            </g:if>
                        </g:each>
                    </div>
                </div>
            </g:each>
        </div>
    </div>
</g:if>

<h1 class="contentH1">All My Quotes</h1>

<div class="criteria"><form id="">Filter: <input type="text" id="filter" name="filter" value="" maxlength="30"
                                                 size="30"/></form></div>
<table id="quotes" class="list">
    <thead>
    <tr>
        <th class="hdrLeft"><g:checkBox id="chkAll" name="chkAll" value="${false}" title="Select all rows"/></th>
        <th class="hdrCtr"><g:message code="ui.table.header.quote.aqn" default="AQN"/></th>
        <th class="hdrCtr"><g:message code="ui.table.header.quote.status" default="Status"/></th>
        <th class="hdrCtr" nowrap><g:message code="ui.table.header.quote.vehicle" default="Vehicle"/></th>
        <th class="hdrCtr"><g:message code="ui.table.header.quote.kmsperyr" default="Km's/Year"/></th>
        <th class="hdrCtr"><g:message code="ui.table.header.quote.leaseterm" default="Term"/></th>
        <th class="hdrRight"><g:message code="ui.table.header.quote.lastupdated" default="Last Updated"/></th>
    </tr>
    </thead>
    <tbody>
    <g:each in="${quoteList}" status="idx" var="q">
        <g:set var="driver" value="${(Individual) q?.customer?.entity}"/>
        <g:if test="${q.status == QuoteStatus.QUOTE_CREATED || q.status == QuoteStatus.QUOTE_CANCELLED}">
            <tr id="row_${q.id}" class="${(idx % 2) == 0 ? 'odd' : 'even'}">
                <td class="cellLeft"><g:checkBox class="chkRow" name="chkRow_${q.id}" value="${false}"
                                                 title="Select this row"/></td>
                <td class="cellCtr"><g:link action="viewquote" id="${q.id}"><g:formatNumber number="${q?.id}"
                                                                                            format="${message(code: 'quote.number.mask')}"/></g:link></td>
                <td class="cellCtr">${q?.status?.getName()}</td>
                <td class="cellCtr">${fieldValue(bean: q, field: "vehicle")}</td>
                <td class="cellCtr" nowrap>${fieldValue(bean: q, field: "kmPerYear")} km</td>
                <td class="cellCtr" nowrap>${fieldValue(bean: q, field: "leaseTerm")} month</td>
                <td class="cellRight"><prettytime:display date="${q.lastUpdated}" capitalize="true" showTime="true"
                                                          format="H:mm:ss"/></td>
            </tr>
        </g:if>
    </g:each>
    </tbody>
</table>

<p class="listTotal">(# quotes shown: <span>${quoteList.size() - preferred?.size()}</span>)</p>