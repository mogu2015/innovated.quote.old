<h3 class="topPad">Lease Details</h3>

<div class="formSet">
    <table class="grid">
        <tbody>
        <tr>
            <th><g:message code="quote.show.label.finance.type" default="Finance Type"/></th>
            <td>Novated Lease</td>
            <td class="spacer">&nbsp;</td>
            <th><g:message code="quote.show.label.finance.amt" default="Amount Financed"/><g:if
                    test="${quoteInstance?.insuranceItems?.size() > 0}">&#185;</g:if></th>
            <td><g:formatNumber number="${quoteInstance?.vehFinAmount}" type="currency" currencySymbol="\$ "
                                currencyCode="AUD" locale="en_AU" maxFractionDigits="2"/></td>
        </tr>
        <tr>
            <th class="last"><g:message code="quote.show.label.lease.term" default="Lease Term"/></th>
            <td class="last">${quoteInstance?.leaseTerm} months</td>
            <td class="spacer">&nbsp;</td>
            <th class="last"><g:message code="quote.show.label.residual.combined" default="Residual"/> <g:if
                    test="${quoteInstance?.insuranceItems?.size() > 0}">&#178;</g:if><g:else>&#185;</g:else></th>
            <td class="last">
                <g:formatNumber number="${quoteInstance?.rvValue}" type="currency" currencySymbol="\$ "
                                currencyCode="AUD" locale="en_AU" maxFractionDigits="2"/>&nbsp;(<g:formatNumber
                        number="${quoteInstance?.rvPercentage / 100}" type="percent" maxFractionDigits="2"/>)
            </td>
        </tr>
        </tbody>
    </table>

    <p class="fineprint">
        <span>Please Note:</span>
        <g:if test="${quoteInstance?.insuranceItems?.size() > 0}">
            &#185;The financed amount shown includes: <g:each in="${quoteInstance?.insuranceItems}" var="i"
                                                              status="idx"><g:if test="${idx == 0}"></g:if><g:elseif
                    test="${idx == (quoteInstance?.insuranceItems?.size() - 1)}">&</g:elseif><g:else>,</g:else>${i}</g:each>.
									&#178;The residual dollar amount shown excludes GST.
        </g:if>
        <g:else>
            &#185;The residual dollar amount shown excludes GST.
        </g:else>
    </p>
</div>