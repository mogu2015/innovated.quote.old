<script type="text/javascript">
    $(document).ready(function () {
        // show/hide graph panels
        $(".comparisonToggler").click(function () {
            var rel = $(this).attr("rel");
            $(this).parent().nextAll(".toggleGrp").hide();
            $(this).siblings(".comparisonToggler").removeClass("selectedView");

            $(this).addClass("selectedView");
            $("#" + rel).show();
            return false;
        });

        // build graphs
        <g:render template="/templates/ui/quote/viewquotegraphs" model="[quoteInstance:quoteInstance]" />
    });
</script>

<h3 class="topPad">Benefit Received</h3>

<p class="togglerGrp">
    <b>View:</b>&nbsp;<a class="comparisonToggler selectedView" href="#" rel="comparisonDetailView"
                         title="View the packaging benefit comparison details in tabular form">Details</a>&nbsp;|&nbsp;<b>View:</b>&nbsp;<a
        class="comparisonToggler" href="#" rel="benefitDetailView"
        title="View the packaging benefit details graphically">Benefits</a>&nbsp;|&nbsp;<b>View:</b>&nbsp;<a
        class="comparisonToggler" href="#" rel="costDetailView"
        title="View the packaging cost details graphically">Costs</a>
</p>

<div id="comparisonDetailView" class="formSet toggleGrp">
    <div id="comparisonTableWrapper">
        <g:render template="/templates/ui/quote/packagingcomparisontable"
                  model="[quoteInstance: quoteInstance, comparisonItemTypes: comparisonItemTypes]"/>
    </div>
</div>

<div id="benefitDetailView" class="toggleGrp" style="display:none;">
    <p class="togglerGrp level2">
        <b>View Salary Effect:</b>&nbsp;<a class="comparisonToggler selectedView" href="#" rel="comparisonSalaryView"
                                           title="Graph the effect on your Net Salary Annually">Annually</a>,&nbsp;<a
            class="comparisonToggler" href="#" rel="comparisonSalaryViewPerPay"
            title="Graph the effect on your Net Salary ${quoteInstance?.defaultPayFreq?.getName()}">${quoteInstance?.defaultPayFreq?.getName()}</a>&nbsp;|&nbsp;<b>View Benefit Received:</b>&nbsp;<a
            class="comparisonToggler" href="#" rel="comparisonBenefitView"
            title="Graph the package benefit received Annually">Annually</a>,&nbsp;<a class="comparisonToggler" href="#"
                                                                                      rel="comparisonBenefitViewPerPay"
                                                                                      title="Graph the package benefit received ${quoteInstance?.defaultPayFreq?.getName()}">${quoteInstance?.defaultPayFreq?.getName()}</a>
    </p>

    <div id="comparisonSalaryView" class="formSet toggleGrp">
        <div class="graphDiv" id="comparisonSalaryGraph" style="width:650px; height:400px">salary annual</div>
    </div>

    <div id="comparisonSalaryViewPerPay" class="formSet toggleGrp" style="display:none;">
        <div class="graphDiv" id="comparisonSalaryGraphPerPay" style="width:650px; height:400px">salary freq</div>
    </div>

    <div id="comparisonBenefitView" class="formSet toggleGrp" style="display:none;">
        <div class="graphDiv" id="comparisonBenefitGraph" style="width:650px; height:400px"></div>
    </div>

    <div id="comparisonBenefitViewPerPay" class="formSet toggleGrp" style="display:none;">
        <div class="graphDiv" id="comparisonBenefitGraphPerPay" style="width:650px; height:400px">benefit freq</div>
    </div>
</div>

<div id="costDetailView" class="toggleGrp" style="display:none;">
    <p class="togglerGrp level2">
        <b>View Vehicle Costs:</b>&nbsp;<a class="comparisonToggler selectedView" href="#" rel="comparisonCostView"
                                           title="Graph the package cost incurred Annually">Annually</a>,&nbsp;<a
            class="comparisonToggler" href="#" rel="comparisonCostViewPerPay"
            title="Graph the package cost incurred ${quoteInstance?.defaultPayFreq?.getName()}">${quoteInstance?.defaultPayFreq?.getName()}</a>
    </p>

    <div id="comparisonCostView" class="formSet toggleGrp">
        <div class="graphDiv" id="comparisonCostGraph" style="width:650px; height:400px">cost annual</div>
    </div>

    <div id="comparisonCostViewPerPay" class="formSet toggleGrp" style="display:none;">
        <div class="graphDiv" id="comparisonCostGraphPerPay" style="width:650px; height:400px">cost freq</div>
    </div>
</div>