<%@ page import="com.innovated.iris.domain.*" %>
<%@ page import="com.innovated.iris.enums.*" %>

<script type="text/javascript">
function removeRow(id) {
	var $el = $("#"+id);
	if ($el.find("input:checkbox:checked").length == 1) {
		$el.find("input:checkbox:checked").removeAttr("checked");
		$("#quotes input:checkbox").change();
	}
	$el.fadeOut('slow', function(){
		$(this).remove();
		var rowCount = $("#quotes >tbody >tr").length;
		$(".listTotal span").html(rowCount);
		if (rowCount == 0) {
			// load mask screen?
			location.reload();
		}
	});
}

$(document).ready(function() {
	var setTable = $("#quotes");
	setTable.tablesorter({
		headers: { 0: { sorter:false } }
	});
	$("#filter").keyup(function() {
		$.uiTableFilter(setTable, this.value, null, function() {
			var visibleRows = setTable.find("tr:visible").length - 1;
			$(".listTotal span").text(visibleRows);
		});
	});
	
});
</script>

<div class="criteria"><form id="">Filter: <input type="text" id="filter" name="filter" value="" maxlength="30" size="30"/></form></div>
<table id="quotes" class="list">
<thead>
	<tr>
		<th class="hdrLeft"><g:checkBox id="chkAll" name="chkAll" value="${false}" title="Select all rows" /></th>
		<th class="hdrCtr"><g:message code="ui.table.header.quote.aqn" default="AQN" /></th>
		<th class="hdrCtr"><g:message code="ui.table.header.quote.status" default="Status" /></th>
		<th class="hdrCtr"><g:message code="ui.table.header.quote.client" default="Client" /></th>
		<th class="hdrCtr" nowrap><g:message code="ui.table.header.quote.vehicle" default="Vehicle" /></th>
		<th class="hdrCtr"><g:message code="ui.table.header.quote.kmsperyr" default="Km's/Year" /></th>
		<th class="hdrCtr"><g:message code="ui.table.header.quote.leaseterm" default="Term" /></th>
		<th class="hdrRight"><g:message code="ui.table.header.quote.lastupdated" default="Last Updated" /></th>
	</tr>
</thead>
<tbody>
	<g:each in="${quoteList}" status="idx" var="q">
	<g:set var="driver" value="${(Individual) q?.customer?.entity}" />
	<tr id="row_${q.id}" class="${(idx % 2) == 0 ? 'odd' : 'even'}">
		<td class="cellLeft"><g:checkBox class="chkRow" name="chkRow_${q.id}" value="${false}" title="Select this row" /></td>
		<td class="cellCtr"><g:link controller="tools" action="viewquote" id="${q.id}"><g:formatNumber number="${q?.id}" format="${message(code:'quote.number.mask')}" /></g:link></td>
		<td class="cellCtr">${q?.status?.getName()}</td>
		<td class="cellCtr">${driver?.firstName} ${driver?.lastName} (${q?.employment?.employer})</td>
		<td class="cellCtr">${fieldValue(bean: q, field: "vehicle")}</td>
		<td class="cellCtr" nowrap>${fieldValue(bean: q, field: "kmPerYear")} km</td>
		<td class="cellCtr" nowrap>${fieldValue(bean: q, field: "leaseTerm")} month</td>
		<td class="cellRight"><prettytime:display date="${q.lastUpdated}" capitalize="true" showTime="true" format="H:mm:ss" /></td>
	</tr>
	</g:each>
</tbody>
</table>
<p class="listTotal">(# quotes shown: <span>${quoteList.size()}</span>)</p>

