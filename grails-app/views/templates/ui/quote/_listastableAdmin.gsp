<%@ page import="com.innovated.iris.domain.*" %>
<%@ page import="com.innovated.iris.enums.*" %>

<script type="text/javascript">
function removeRow(id) {
	var $el = $("#"+id);
	if ($el.find("input:checkbox:checked").length == 1) {
		$el.find("input:checkbox:checked").removeAttr("checked");
		$("#quotes input:checkbox").change();
	}
	$el.fadeOut('slow', function(){
		$(this).remove();
		var rowCount = $("#quotes >tbody >tr").length;
		$(".listTotal span").html(rowCount);
		if (rowCount == 0) {
			// load mask screen?
			location.reload();
		}
	});
}

$(document).ready(function() {
	var setTable = $("#quotes");
	setTable.tablesorter({
		headers: { 0: { sorter:false } }
	});
	$("#filter").keyup(function() {
		$.uiTableFilter(setTable, this.value, null, function() {
			// update the table count label
			var visibleRows = setTable.find("tr:visible").length - 1;
			$(".listTotal span").text(visibleRows);
		});
	});

	$(".delete-link").click(function(e){
		e.preventDefault();
		if (confirm("Are you sure you want to delete this quote?")) {
			var id = $(this).attr("rel");
			$.post("${createLink(controller:'quote', action:'deleteAjax')}", { id:id }, function(data) {
				var $mrkp = $("<div class='flashMsg'><p class='" + data.classes + "'>" + data.msg + "</p></div>");
				$mrkp.insertAfter("p.bread");
				$mrkp.delay(5000).fadeOut('slow');
				if (data.success) {
					removeRow('row_' + id);
				}
			}, 'json');
		}
		return false;
	});
	/*jQuery.ajax({
		type:'POST', 
		url:'/iris/quote/deleteAjax/7',
		success:function(data,textStatus){
			jQuery('#messages').html(data);
			removeRow('row_7');;
		},
		error:function(XMLHttpRequest,textStatus,errorThrown){}
	});
	return false;*/
	
});
</script>

<div class="criteria"><form id="">Filter: <input type="text" id="filter" name="filter" value="" maxlength="30" size="30"/></form></div>
<table id="quotes" class="list">
<thead>
	<tr>
		<th class="hdrLeft"><g:checkBox id="chkAll" name="chkAll" value="${false}" title="Select all rows" /></th>
		<th class="hdrCtr"><g:message code="ui.table.header.quote.aqn" default="AQN" /></th>
		<th class="hdrCtr"><g:message code="ui.table.header.quote.status" default="Status" /></th>
		<th class="hdrCtr"><g:message code="ui.table.header.quote.client" default="Client" /></th>
		<th class="hdrCtr">Delete</th>
		<th class="hdrCtr" nowrap><g:message code="ui.table.header.quote.vehicle" default="Vehicle" /></th>
		<th class="hdrCtr"><g:message code="ui.table.header.quote.kmsperyr" default="Km's/Year" /></th>
		<th class="hdrCtr"><g:message code="ui.table.header.quote.leaseterm" default="Term" /></th>
		<th class="hdrRight"><g:message code="ui.table.header.quote.lastupdated" default="Last Updated" /></th>
	</tr>
</thead>
<tbody>
	<g:each in="${quoteList}" status="idx" var="q">
	<g:set var="driver" value="${(Individual) q?.customer?.entity}" />
	<tr id="row_${q.id}" class="${(idx % 2) == 0 ? 'odd' : 'even'}">
		<td class="cellLeft"><g:checkBox class="chkRow" name="chkRow_${q.id}" value="${false}" title="Select this row" /></td>
		<td class="cellCtr"><g:link action="viewquote" id="${q.id}"><g:formatNumber number="${q?.id}" format="${message(code:'quote.number.mask')}" /></g:link></td>
		<td class="cellCtr">${q?.status?.getName()}</td>
		<td class="cellCtr">${driver?.firstName} ${driver?.lastName} (${q?.employment?.employer})</td>
		<!-- <td class="cellCtr"><g:remoteLink controller="quote" action="deleteAjax" id="${q.id}" update="messages" onSuccess="removeRow('row_${q.id}');">delete</g:remoteLink></td> -->
		<td class="cellCtr"><g:link controller="quote" action="deleteAjax" id="${q.id}" class="delete-link" rel="${q.id}">delete</g:link></td>
		<td class="cellCtr">${fieldValue(bean: q, field: "vehicle")}</td>
		<td class="cellCtr" nowrap>${fieldValue(bean: q, field: "kmPerYear")} km</td>
		<td class="cellCtr" nowrap>${fieldValue(bean: q, field: "leaseTerm")} month</td>
		<td class="cellRight"><prettytime:display date="${q.lastUpdated}" capitalize="true" showTime="true" format="H:mm:ss" /></td>
	</tr>
	</g:each>
</tbody>
</table>
<p class="listTotal">(# quotes shown: <span>${quoteList.size()}</span>)</p>