<p class="notice">
    <g:if test="${quoteInstance?.pricingConfirmed == null}">
        <i><b><u>Note</u></b>: The <span class="bad">
            <g:if test="${!quoteInstance?.priceObtained}">
                pricing shown on this quote is indicative</span>. Final pricing is <b>yet to be obtained</b> from relevant 3rd parties.
            </g:if>
            <g:else>
                pricing shown on this quote is the input price</span>.
            </g:else>
        </i>
    </g:if>
    <g:else>
        <i><b><u>Note</u></b>: The <span
                class="good">pricing shown on this quote has been confirmed</span> by all relevant 3rd parties as at: <b><g:formatDate
                date="${quoteInstance?.pricingConfirmed}" format="d MMM yyyy"/></b>.</i>
    </g:else>
</p>

<h3 class="topPad">Quotation Details</h3>

<div class="formSet">
    <table class="grid">
        <tbody>
        <tr>
            <th>Quote #</th>
            <td class="special"><g:formatNumber number="${quoteInstance?.id}" format="${message(code: 'quote.number.mask')}"/></td>
            <td class="spacer">&nbsp;</td>
            <th>Vehicle</th>
            <td>${quoteInstance?.vehicle?.encodeAsHTML()}</td>
        </tr>
        <tr>
            <th>Client Name</th>
            <g:ifAnyGranted role="ROLE_SUPER,ROLE_ADMIN">
                <td><g:link controller="individual" action="show"
                            id="${quoteInstance?.customer?.entity?.id}">${quoteInstance?.customer?.entity?.encodeAsHTML()}</g:link></td>
            </g:ifAnyGranted>
            <g:ifNotGranted role="ROLE_SUPER,ROLE_ADMIN">
                <td>${quoteInstance?.customer?.entity?.encodeAsHTML()}</td>
            </g:ifNotGranted>
            <td class="spacer">&nbsp;</td>
            <th>Engine</th>
            <td>${quoteInstance?.vehicle?.engineCapacity}L</td>
        </tr>
        <tr>
            <th>Client Address</th>
            <g:ifAnyGranted role="ROLE_SUPER,ROLE_ADMIN">
                <td><g:link controller="address" action="show"
                            id="${quoteInstance?.address?.id}">${quoteInstance?.address?.encodeAsHTML()}</g:link></td>
            </g:ifAnyGranted>
            <g:ifNotGranted role="ROLE_SUPER,ROLE_ADMIN">
                <td>${quoteInstance?.address?.encodeAsHTML()}</td>
            </g:ifNotGranted>
            <td class="spacer">&nbsp;</td>
            <th>Fuel</th>
            <td>${quoteInstance?.vehicle?.fueltypestr}</td>
        </tr>
        <tr>
            <g:set var="employerTypes" value="${['BUCOGO','ASANDNSW','PUBLICNSW','REBATABLE','PBI0001']}"/>
            <g:if test="${employerTypes.contains(quoteInstance?.company?.employerCode)}">
                <th>Employer Type</th>
            </g:if>
            <g:else>
                <th>Employer</th>
            </g:else>
            <g:ifAnyGranted role="ROLE_SUPER,ROLE_ADMIN">
                <td><g:link controller="company" action="show"
                            id="${quoteInstance?.company?.id}">${quoteInstance?.company?.name?.encodeAsHTML()}</g:link></td>
            </g:ifAnyGranted>
            <g:ifNotGranted role="ROLE_SUPER,ROLE_ADMIN">
                <td>${quoteInstance?.company?.name?.encodeAsHTML()}</td>
            </g:ifNotGranted>
            <td class="spacer">&nbsp;</td>
            <th>Shape</th>
            <td>${quoteInstance?.vehicle?.shape}</td>
        </tr>
        <tr>
            <th>Gross Salary</th>
            <td><g:formatNumber number="${quoteInstance?.grossAnnualSalary}" type="currency" currencyCode="AUD" currencySymbol="\$ " locale="en_AU" maxFractionDigits="0"/> per annum</td>
            <td class="spacer">&nbsp;</td>
            <th>Transmission</th>
            <td>${quoteInstance?.vehicle?.transmission}</td>
        </tr>

        <tr>
            <th class="last">Pay Cycle</th>
            <td class="last">${quoteInstance?.defaultPayFreq?.getName()}</td>
            <td class="spacer">&nbsp;</td>
            <th>Colour / Trim</th>
            <td>${quoteInstance?.colourPreference}
                <g:if test="${quoteInstance?.vehicle?.shape && quoteInstance?.vehicle?.transmission}"> / </g:if>
                ${quoteInstance?.interiorTrim}
            </td>
        </tr>
        <tr>
            <th>Km's / Annum</th>
            <td class="special"><g:formatNumber number="${quoteInstance?.kmPerYear}" type="number"
                                                maxFractionDigits="0"/> km</td>
            <td class="spacer">&nbsp;</td>
            <th class="last">Options</th>
            <td class="last">${quoteInstance?.optionItems?.join(", ").encodeAsHTML()}</td>
        </tr>
        </tbody>
    </table>
</div>