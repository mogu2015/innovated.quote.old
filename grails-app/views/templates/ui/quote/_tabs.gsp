<g:set var="showAllTabs" value="${false}"/>
<script type="text/javascript">
$(document).ready(function() {
	/*var hash = window.location.hash;
	if (hash) {
		alert("hash: " + hash);
	}*/

	var $tabs = $("#tabs").tabs({
		//cookie: { expires: 30 }
<g:ifAnyGranted role="ROLE_SUPER,ROLE_ADMIN">
		// admin config options
</g:ifAnyGranted><g:ifNotGranted role="ROLE_SUPER,ROLE_ADMIN">
<g:if test="${showAllTabs && quoteInstance?.status?.code <= com.innovated.iris.enums.QuoteStatus.QUOTE_CREATED.getCode()}">
		disabled: [2,3,4,5,6]	// zero-based tab indexes
</g:if>
</g:ifNotGranted>
	});
	/*
	// update URL hash on Tab selection
	$tabs.bind("tabsshow", function(e,ui){
		window.location.hash = ui.tab.hash;
		//window.location = ui.tab;
	});
	*/
	$tabs.tabs('select',"details");
	//$tabs.tabs('select',"finance");
});
</script>
<div id="tabs" class="ui-tabs-hide" style="margin-bottom:12px;">
	<ul>
		<g:if test="${showAllTabs}">
			<li><g:link controller="tools" action="showTab" id="${quoteInstance.id}" params="[tab:'checklist']" title="checklist"><span>Checklist</span></g:link></li>
		</g:if>
			<li><g:link controller="tools" action="showTab" id="${quoteInstance.id}" params="[tab:'details']" title="details"><span>Quote Details</span></g:link></li>
		%{--<li><g:link controller="tools" action="showTab" id="${quoteInstance.id}" params="[tab:'finance']" title="finance"><span>Finance</span></g:link></li>--}%
		<g:if test="${showAllTabs}">
			<li><g:link controller="tools" action="showTab" id="${quoteInstance.id}" params="[tab:'insurance']" title="insurance"><span>Insurance</span></g:link></li>
			<li><g:link controller="tools" action="showTab" id="${quoteInstance.id}" params="[tab:'fuel']" title="fuel"><span>Fuel</span></g:link></li>
			<li><g:link controller="tools" action="showTab" id="${quoteInstance.id}" params="[tab:'other']" title="other"><span>Notes</span></g:link></li>
			<li><g:link controller="tools" action="showTab" id="${quoteInstance.id}" params="[tab:'docs']" title="docs"><span>Documents</span></g:link></li>
		</g:if>
<g:ifAnyGranted role="ROLE_SUPER,ROLE_ADMIN">
		<g:if test="${showAllTabs}"><li><g:link controller="tools" action="showTab" id="${quoteInstance.id}" params="[tab:'vehicle']" title="vehicle"><span>Vehicle Order</span></g:link></li></g:if>
</g:ifAnyGranted>		
	</ul>
</div>
