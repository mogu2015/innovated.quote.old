<%@ page import="com.innovated.iris.server.fbt.PackagingComparisonItemType" %>
<g:set var="notPackagedVal" value="${quoteInstance.comparisons.get(0).items[-1].amount}" />
<g:set var="notPkgPostVal" value="${Math.abs(quoteInstance.comparisons.get(0).items[-2].amount)}" />
<g:set var="bestVal" value="${0}" /><g:set var="worstVal" value="${49999}" /><g:set var="bestCost" value="${49999}" />
<g:each in="${quoteInstance.comparisons}" var="comp" status="i"><g:set var="bestVal" value="${Math.max(bestVal, comp.items[-1].amount)}" /><g:set var="worstVal" value="${Math.min(worstVal, comp.items[-1].amount)}" /><g:set var="bestCost" value="${Math.min(bestCost, notPkgPostVal - (Math.abs(comp.items[-1].amount) - notPackagedVal))}" /></g:each>
var salaryGraph, salaryGraphPerPay, benefitGraph, benefitGraphPerPay, costGraph, costGraphPerPay;
var best = ${bestVal};
var bestCost = ${bestCost};
var worst = ${worstVal};
var notPackaged = ${notPackagedVal};
var notPackagedCost = ${notPkgPostVal};
var notPackagedName = "${quoteInstance.comparisons.get(0).name}";
var payFreq = ${quoteInstance.defaultPayFreq.getCode()};

salaryGraph = new Highcharts.Chart({
	chart:{ renderTo:'comparisonSalaryGraph', defaultSeriesType:'column', borderWidth:1, backgroundColor:'#FFFFFF', margin:[50,50,80,80] },
	title:{
		text:'Net (Take Home) Salary Annually vs. Packaging Method',
		style:{ font:'bold 16px "Lucida Grande", "Lucida Sans Unicode", Verdana, Arial, Helvetica, sans-serif' }
	},
	xAxis:{ title: { text:'Packaging Method', enabled:true, margin:20 }, categories:['&nbsp;'] },
	yAxis:[{
		title:{ text:'Annual (Take Home) Salary', margin:55 }, min:worst-1000,
		plotBands:[{ from:notPackaged, to:best, color:'rgba(68, 170, 213, 0.1)' }],
		labels:{ formatter:function(){ return '$' + Highcharts.numberFormat(this.value/1000, 1) + 'k'; } }
	}],
	series:[<g:each in="${quoteInstance.comparisons}" var="comp" status="i"><g:if test="${i > 0}">, </g:if>{ name: '${comp.name}', data: [${comp.items[-1].amount}], groupPadding:0.001, visible:${(i==0 || comp.isBest) ? true : false} }</g:each>],
	legend:{ layout:'horizontal', style:{left:'10px', bottom:'10px', right:'10px', top:'auto'}, borderWidth:1, backgroundColor:'#FFFEE3' },
	tooltip:{
		formatter:function() {
			var diff = this.y - notPackaged;
			if (this.series.name == notPackagedName) {
				return "<b>" + this.series.name + "</b><br/>Your net salary without packaging = <b>$" + Highcharts.numberFormat(this.y, 0) + "</b>";
			}
			else {
				if (diff == 0) return "<b>" + this.series.name + "</b><br/>Your net salary using this method = <b>$" + Highcharts.numberFormat(this.y, 0) + "</b>";
				else if (diff > 0) return "<b>" + this.series.name + "</b><br/>Your net salary using this method = <b>$" + Highcharts.numberFormat(this.y, 0) + "</b><br/>That's a <b class='increase'>$" + Highcharts.numberFormat(diff, 0) + " increase</b> each year!";
				else return "<b>" + this.series.name + "</b><br/>Your net salary using this method = <b>$" + Highcharts.numberFormat(this.y, 0) + "</b><br/>That's a <b class='decrease'>$" + Highcharts.numberFormat(Math.abs(diff), 0) + " decrease</b> each year.";
			}
		}
	},
	plotOptions:{
		column:{ dataLabels:{ enabled:true, formatter:function(){ return "$" + Highcharts.numberFormat(this.y, 0); } } }
	}
});

salaryGraphPerPay = new Highcharts.Chart({
	chart: {
		renderTo: 'comparisonSalaryGraphPerPay', defaultSeriesType: 'column', borderWidth: 1, backgroundColor:'#FFFFFF', margin: [50, 50, 80, 80]
	},
	title: {
		text: 'Net (Take Home) Salary ${quoteInstance.defaultPayFreq.getName()} vs. Packaging Method',
		style: { font: 'bold 16px "Lucida Grande", "Lucida Sans Unicode", Verdana, Arial, Helvetica, sans-serif' }
	},
	xAxis: {
		title: { text: 'Packaging Method', enabled: true, margin:20 }, categories: ['&nbsp;']
	},
	yAxis: [{
		title: { text:'${quoteInstance.defaultPayFreq.getName()} (Take Home) Salary', margin: 55 }, min: worst/payFreq,
		plotBands: [{ from:notPackaged/payFreq, to:best/payFreq, color:'rgba(68, 170, 213, 0.1)' }],
		labels: { formatter: function() { return '$' + Highcharts.numberFormat(this.value, 0); } }
	}],
	series: [<g:each in="${quoteInstance.comparisons}" var="comp" status="i"><g:if test="${i > 0}">, </g:if>{ name: '${comp.name}', data: [${comp.items[-1].amount / quoteInstance.defaultPayFreq.getCode()}], groupPadding:0.001, visible:${(i==0 || comp.isBest) ? true : false} }</g:each>],
	legend: {
		layout: 'horizontal', style:{ left:'10px', bottom:'10px', right:'10px', top:'auto' }, borderWidth: 1, backgroundColor:'#FFFEE3'
	},
	tooltip: {
		formatter: function() {
			var diff = this.y - Math.round(notPackaged/payFreq);
			if (this.series.name == notPackagedName) {
				return "<b>" + this.series.name + "</b><br/>Your net salary without packaging = <b>$" + Highcharts.numberFormat(this.y, 0) + "</b>";
			}
			else {
				if (diff == 0) return "<b>" + this.series.name + "</b><br/>Your net salary using this method = <b>$" + Highcharts.numberFormat(this.y, 0) + "</b>";
				else if (diff > 0) return "<b>" + this.series.name + "</b><br/>Your net salary using this method = <b>$" + Highcharts.numberFormat(this.y, 0) + "</b><br/>That's a <b class='increase'>$" + Highcharts.numberFormat(diff, 0) + " increase</b> each pay!";
				else return "<b>" + this.series.name + "</b><br/>Your net salary using this method = <b>$" + Highcharts.numberFormat(this.y, 0) + "</b><br/>That's a <b class='decrease'>$" + Highcharts.numberFormat(Math.abs(diff), 0) + " decrease</b> each pay.";
			}
		}
	},
	plotOptions: {
		column: {
			dataLabels: {
				enabled: true, formatter: function() { return "$" + Highcharts.numberFormat(this.y, 0); }
			}
		}
	}
});

benefitGraph = new Highcharts.Chart({
	chart:{ renderTo:'comparisonBenefitGraph', defaultSeriesType:'column', borderWidth:1, backgroundColor:'#FFFFFF', margin:[50,50,80,80] },
	title:{
		text:'Benefit Received Annually vs. Packaging Method',
		style:{ font:'bold 16px "Lucida Grande", "Lucida Sans Unicode", Verdana, Arial, Helvetica, sans-serif' }
	},
	xAxis:{
		title: { text:'Packaging Method', enabled:true, margin:20 }, labels:{ enabled:false },
		categories:[<g:each in="${quoteInstance.comparisons}" var="comp" status="i"><g:if test="${i > 0}">, </g:if>'${comp.name}'</g:each>]
	},
	yAxis:[{
		title:{ text:'Net Benefit Annually', margin:60, enabled:true, style:{ color:'#008000' } },
		labels:{ formatter:function(){ return '$' + Highcharts.numberFormat(this.value, 0); }, style:{ color:'#008000' } }
	}],
	series:[<g:each in="${quoteInstance.comparisons}" var="comp" status="i"><g:if test="${i > 0}">, </g:if>{ name: '${comp.name}', data: [${comp.items[-1].amount - notPackagedVal}], groupPadding:0.001, visible:${(i==0 || comp.isBest) ? true : false} }</g:each>],
	legend:{ layout:'horizontal', style: { left:'10px', bottom:'10px', right:'10px', top:'auto' }, borderWidth:1, backgroundColor:'#FFFEE3' },
	tooltip:{
		formatter:function() {
			if (this.y == 0) return "<b>" + this.series.name + "</b><br/>Your net benefit using this<br/>method = <b>$" + Highcharts.numberFormat(this.y, 0) + "</b>";
			else if (this.y > 0) return "<b>" + this.series.name + "</b><br/>Your net benefit using this<br/>method = <b class='increase'>$" + Highcharts.numberFormat(this.y, 0) + "</b> each year!";
			else return "<b>" + this.series.name + "</b><br/>Your net benefit using this<br/>method = <b class='decrease'>-$" + Highcharts.numberFormat(Math.abs(this.y), 0) + "</b> each year.";
		}
	},
	plotOptions:{
		column:{ dataLabels:{ y:-10, enabled:true, formatter:function(){ return "$" + Highcharts.numberFormat(this.y, 0); } } }
	}
});

benefitGraphPerPay = new Highcharts.Chart({
	chart:{ renderTo:'comparisonBenefitGraphPerPay', defaultSeriesType:'column', borderWidth:1, backgroundColor:'#FFFFFF', margin:[50,50,80,80] },
	title:{
		text:'Benefit Received ${quoteInstance.defaultPayFreq.getName()} vs. Packaging Method',
		style:{ font:'bold 16px "Lucida Grande", "Lucida Sans Unicode", Verdana, Arial, Helvetica, sans-serif' }
	},
	xAxis:{
		title: { text:'Packaging Method', enabled:true, margin:20 }, labels:{ enabled:false },
		categories:[<g:each in="${quoteInstance.comparisons}" var="comp" status="i"><g:if test="${i > 0}">, </g:if>'${comp.name}'</g:each>]
	},
	yAxis:[{
		title:{ text:'Net Benefit ${quoteInstance.defaultPayFreq.getName()}', margin:60, enabled:true, style:{ color:'#008000' } },
		labels:{ formatter:function(){ return '$' + Highcharts.numberFormat(this.value, 0); }, style:{ color:'#008000' } },
		max:(best/payFreq - notPackaged/payFreq)*1.2
	}],
	series:[<g:each in="${quoteInstance.comparisons}" var="comp" status="i"><g:if test="${i > 0}">, </g:if>{ name: '${comp.name}', data: [${(comp.items[-1].amount - notPackagedVal) / quoteInstance.defaultPayFreq.getCode()}], groupPadding:0.001, visible:${(i==0 || comp.isBest) ? true : false} }</g:each>],
	legend:{ layout:'horizontal', style: { left:'10px', bottom:'10px', right:'10px', top:'auto' }, borderWidth:1, backgroundColor:'#FFFEE3' },
	tooltip:{
		formatter:function() {
			if (this.y == 0) return "<b>" + this.series.name + "</b><br/>Your net benefit using this<br/>method = <b>$" + Highcharts.numberFormat(this.y, 0) + "</b>";
			else if (this.y > 0) return "<b>" + this.series.name + "</b><br/>Your net benefit using this<br/>method = <b class='increase'>$" + Highcharts.numberFormat(this.y, 0) + "</b> each pay!";
			else return "<b>" + this.series.name + "</b><br/>Your net benefit using this<br/>method = <b class='decrease'>-$" + Highcharts.numberFormat(Math.abs(this.y), 0) + "</b> each pay.";
		}
	},
	plotOptions:{
		column:{ dataLabels:{ y:-10, enabled:true, formatter:function(){ return "$" + Highcharts.numberFormat(this.y, 0); } } }
	}
});

costGraph = new Highcharts.Chart({
	chart:{ renderTo:'comparisonCostGraph', defaultSeriesType:'column', borderWidth:1, backgroundColor:'#FFFFFF', margin:[50,50,80,80] },
	title:{
		text:'Cost Incurred Annually vs. Packaging Method',
		style:{ font:'bold 16px "Lucida Grande", "Lucida Sans Unicode", Verdana, Arial, Helvetica, sans-serif' }
	},
	xAxis:{
		title: { text:'Packaging Method', enabled:true, margin:20 }, labels:{ enabled:false },
		categories:[<g:each in="${quoteInstance.comparisons}" var="comp" status="i"><g:if test="${i > 0}">, </g:if>'${comp.name}'</g:each>]
	},
	yAxis:[{
		title:{ text:'Cost Incurred Annually', margin:60, enabled:true, style:{ color:'#FF0000' } },
		plotBands:[{ from:bestCost, to:notPackagedCost, color:'rgba(68, 170, 213, 0.1)' }], max:${notPkgPostVal}*1.1,
		labels:{ formatter:function(){ return '$' + Highcharts.numberFormat(this.value, 0); }, style:{ color:'#FF0000' } }
	}],
	series:[<g:each in="${quoteInstance.comparisons}" var="comp" status="i"><g:if test="${i > 0}">, </g:if>{ name: '${comp.name}', data: [${notPkgPostVal - (Math.abs(comp.items[-1].amount) - notPackagedVal)}], groupPadding:0.001, visible:${(i==0 || comp.isBest) ? true : false} }</g:each>],
	legend:{ layout:'horizontal', style: { left:'10px', bottom:'10px', right:'10px', top:'auto' }, borderWidth:1, backgroundColor:'#FFFEE3' },
	tooltip:{
		formatter:function() {
			if (this.series.name == notPackagedName) {
				return "<b>" + this.series.name + "</b><br/>Without packaging, the costs you incur<br/>on this vehicle = <b>$" + Highcharts.numberFormat(this.y, 0) + "</b> each year.";
			}
			else {
				if (this.y == notPackagedCost) return "<b>" + this.series.name + "</b><br/>The total vehicle costs you incur using<br/>this method = <b>$" + Highcharts.numberFormat(this.y, 0) + "</b> each year.";
				else if (this.y > notPackagedCost) return "<b>" + this.series.name + "</b><br/>The total vehicle costs you incur using<br/>this method = <b>$" + Highcharts.numberFormat(this.y, 0) + "</b> each year.";
				else return "<b>" + this.series.name + "</b><br/>The total vehicle costs you incur using<br/>this method = <b>$" + Highcharts.numberFormat(Math.abs(this.y), 0) + "</b>. That's a<br/>saving of <b class='increase'>$" + Highcharts.numberFormat(Math.abs(this.y-notPackagedCost), 0) + "</b> each year!";
			}
		}
	},
	plotOptions:{
		column:{ dataLabels:{ y:-10, enabled:true, formatter:function(){ return "$" + Highcharts.numberFormat(this.y, 0); } } }
	}
});

costGraphPerPay = new Highcharts.Chart({
	chart:{ renderTo:'comparisonCostGraphPerPay', defaultSeriesType:'column', borderWidth:1, backgroundColor:'#FFFFFF', margin:[50,50,80,80] },
	title:{
		text:'Cost Incurred ${quoteInstance.defaultPayFreq.getName()} vs. Packaging Method',
		style:{ font:'bold 16px "Lucida Grande", "Lucida Sans Unicode", Verdana, Arial, Helvetica, sans-serif' }
	},
	xAxis:{
		title: { text:'Packaging Method', enabled:true, margin:20 }, labels:{ enabled:false },
		categories:[<g:each in="${quoteInstance.comparisons}" var="comp" status="i"><g:if test="${i > 0}">, </g:if>'${comp.name}'</g:each>]
	},
	yAxis:[{
		title:{ text:'Cost Incurred ${quoteInstance.defaultPayFreq.getName()}', margin:60, enabled:true, style:{ color:'#FF0000' } },
		plotBands:[{ from:bestCost/payFreq, to:notPackagedCost/payFreq, color:'rgba(68, 170, 213, 0.1)' }], max:(${notPkgPostVal}/payFreq)*1.1,
		labels:{ formatter:function(){ return '$' + Highcharts.numberFormat(this.value, 0); }, style:{ color:'#FF0000' } }
	}],
	series:[<g:each in="${quoteInstance.comparisons}" var="comp" status="i"><g:if test="${i > 0}">, </g:if>{ name: '${comp.name}', data: [${(notPkgPostVal - (Math.abs(comp.items[-1].amount) - notPackagedVal)) / quoteInstance.defaultPayFreq.getCode()}], groupPadding:0.001, visible:${(i==0 || comp.isBest) ? true : false} }</g:each>],
	legend:{ layout:'horizontal', style: { left:'10px', bottom:'10px', right:'10px', top:'auto' }, borderWidth:1, backgroundColor:'#FFFEE3' },
	tooltip:{
		formatter:function() {
			if (this.series.name == notPackagedName) {
				return "<b>" + this.series.name + "</b><br/>Without packaging, the costs you incur<br/>on this vehicle = <b>$" + Highcharts.numberFormat(this.y, 0) + "</b> each pay.";
			}
			else {
				if (this.y == (notPackagedCost/payFreq)) return "<b>" + this.series.name + "</b><br/>The total vehicle costs you incur using<br/>this method = <b>$" + Highcharts.numberFormat(this.y, 0) + "</b> each pay.";
				else if (this.y > (notPackagedCost/payFreq)) return "<b>" + this.series.name + "</b><br/>The total vehicle costs you incur using<br/>this method = <b>$" + Highcharts.numberFormat(this.y, 0) + "</b> each pay.";
				else return "<b>" + this.series.name + "</b><br/>The total vehicle costs you incur using<br/>this method = <b>$" + Highcharts.numberFormat(Math.abs(this.y), 0) + "</b>. That's a<br/>saving of <b class='increase'>$" + Highcharts.numberFormat(Math.abs(this.y-(notPackagedCost/payFreq)), 0) + "</b> each pay!";
			}
		}
	},
	plotOptions:{
		column:{ dataLabels:{ y:-10, enabled:true, formatter:function(){ return "$" + Highcharts.numberFormat(this.y, 0); } } }
	}
});
