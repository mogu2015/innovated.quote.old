<button id="${name}" name="${name}" class="greybutton" type="submit" title="${tip}">
	<span><img width="16" height="16" alt="${text}" src="${src}"/>${text}</span>
</button>