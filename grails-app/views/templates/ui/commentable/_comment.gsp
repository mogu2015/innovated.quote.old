<tr id="row_${comment.id}">
	<td class="cellLeft" width="20"><g:checkBox class="chkRow" name="chkRow_${comment.id}" value="${false}" title="Select this row" /></td>
	<td class="cellCtr" width="60%">${comment.body}</td>
	<td class="cellCtr" nowrap width="60">${comment.poster.username}</td>
	<td class="cellRight" nowrap width="60"><g:formatDate date="${comment.dateCreated}" type="datetime" style="MEDIUM" /></td>
</tr>