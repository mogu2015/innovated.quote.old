<ul class="topnav">
	<li><g:link controller="user" action="home">Home</g:link></li>
<g:ifAnyGranted role="ROLE_EMPLOYER,ROLE_SUPER,ROLE_ADMIN">
	<!-- Employer menu items -->
</g:ifAnyGranted>
<g:ifAnyGranted role="ROLE_USER,ROLE_SUPER,ROLE_ADMIN">
	<li><g:link controller="tools" action="quote">Novated Quote</g:link></li>
	<!-- <li><a href="#">Vehicles</a>
		<ul class="subnav">
			<li><a href="#">Budget Status</a></li>
			<li><a href="#">Odometer Readings</a></li>
			<li><a href="#">Fuel Cards</a></li>
		</ul>
	</li>
	<li><g:link controller="tools">Tools</g:link>
		<ul class="subnav">
			<li><g:link controller="tools" action="quote">Novated Quote</g:link></li>
			<li><a href="#">Km Calculator</a></li>
		</ul>
	</li>
	<li><a href="#">Reports</a>
		<ul class="subnav">
			<li><a href="#">Account Statement</a></li>
			<li><a href="#">My Driver Reports</a></li>
		</ul>
	</li>
	<li><a href="#">Tasks</a>
		<ul class="subnav">
			<li><a href="#">Pay Out My Lease</a></li>
			<li><a href="#">Renewals</a></li>
		</ul>
	</li>
	<li><g:link controller="user" action="mydetails">My Details</g:link></li> -->
</g:ifAnyGranted>
</ul>
<g:ifAnyGranted role="ROLE_SUPER,ROLE_ADMIN">
<!-- Admin specific menu items - aligned right -->
<ul class="topnavRight">
	<li><g:link controller="customer">Customers</g:link></li>
	<li><g:link controller="supplier">Suppliers</g:link></li>
	<li><g:link mapping="contextRoot">System</g:link>
		<ul class="subnav">
			<li><a href="#">Jobs & Events</a></li>
			<li><a href="#">System Settings</a></li>
			<li><a href="#">Bank Reconciliation</a></li>
			<li><a href="#">Invoice Scheduling</a></li>
			<li><a href="#">Generate Payments</a></li>
		</ul>
	</li>
	<li class="menuLastLiRight"><a href="#">Notifications</a></li>
</ul>
</g:ifAnyGranted>