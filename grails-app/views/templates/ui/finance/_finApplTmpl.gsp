<script type="text/javascript">
$(document).ready(function() {
	$("h3 a.expander").click(function(e){
		e.preventDefault();
		var $div = $(this).parent().next("div");
		if ($div.is(":visible")) {
			$div.slideUp();
			$(this).addClass("collapsed").removeClass("expanded");
		} else {
			$div.slideDown();
			$(this).addClass("expanded").removeClass("collapsed");
		}
		return false;
	});

	$("#btnSaveOnly").click(function(e){
		e.preventDefault();
		// do AJAX save...
		
		return false;
	});
	$("#btnSaveSubmit").click(function(e){
		e.preventDefault();
		// do AJAX submit...
		
		return false;
	});

	$(".add-item").click(function(e){
		e.preventDefault();

		var limit = 12;
		var type = $(this).attr("rel");
		var $lastItem = $(this).parent();
		var idx = $lastItem.parent().children().length;
		if (idx <= limit) {
			$.post(
				"${createLink(controller:'tools', action:'getFinApplAnlItem')}",
				{ id:idx, type:type },
				function(data) {
					if (data.success) {
						$(data.html).insertBefore($lastItem);
					}
				}
			);
		}
		else {
			alert('A maximum of ' + limit + ' ' + type + ' items may be added.');
		}
		
		return false;
	});

	$(".remove-item").live("click", function(e){
		e.preventDefault();
		$(this).closest("li").remove();
		return false;
	});
});
</script>
<h3 class="topPad new-fin-app">Finance Application</h3>
<p><u>Current Status</u>: <b>${appl.status.name}</b></p>
<div class="formBg">
	<div class="formWrap">
		<g:form controller="financeApplication" action="save" method="post" >
		
		<h3><a class="expander collapsed" href="#" title="show/hide">Applicant Details</a></h3>
		<div class="formSet" style="display:none;">
			<div class="clearfix ${hasErrors(bean: appl, field: 'applicantTitle', 'error')}">
				<label for="applicantTitle"><g:message code="financeApplication.applicantTitle" default="Title" /></label>
				<g:textField id="applicantTitle" class="shortshort" name="applicantTitle" value="${fieldValue(bean: appl, field: 'applicantTitle')}" tabindex="1" />
			</div>
			<div class="clearfix ${hasErrors(bean: appl, field: 'applicantFirstName', 'error')}">
				<label for="applicantFirstName"><g:message code="financeApplication.applicantFirstName" default="First Name" /></label>
				<g:textField id="applicantFirstName" class="short" name="applicantFirstName" value="${fieldValue(bean: appl, field: 'applicantFirstName')}" tabindex="2" />
			</div>
			<div class="clearfix ${hasErrors(bean: appl, field: 'applicantMiddleName', 'error')}">
				<label for="applicantMiddleName"><g:message code="financeApplication.applicantMiddleName" default="Middle Name" /></label>
				<g:textField id="applicantMiddleName" class="short" name="applicantMiddleName" value="${fieldValue(bean: appl, field: 'applicantMiddleName')}" tabindex="3" />
			</div>
			<div class="clearfix ${hasErrors(bean: appl, field: 'applicantLastName', 'error')}">
				<label for="applicantLastName"><g:message code="financeApplication.applicantLastName" default="Last Name" /></label>
				<g:textField id="applicantLastName" class="short" name="applicantLastName" value="${fieldValue(bean: appl, field: 'applicantLastName')}" tabindex="4" />
			</div>
			<div class="clearfix ${hasErrors(bean: appl, field: 'applicantDateOfBirth', 'error')}">
				<label for="applicantDateOfBirth"><g:message code="financeApplication.applicantDateOfBirth" default="Date of Birth" /></label>
				<g:textField id="applicantDateOfBirth" class="short" name="applicantDateOfBirth" value="${fieldValue(bean: appl, field: 'applicantDateOfBirth')}" tabindex="5" />
			</div>
			<div class="clearfix ${hasErrors(bean: appl, field: 'applicantMaritalStatus', 'error')}">
				<label for="applicantMaritalStatus"><g:message code="financeApplication.applicantMaritalStatus" default="Marital Status" /></label>
				<g:textField id="applicantMaritalStatus" class="short" name="applicantMaritalStatus" value="${fieldValue(bean: appl, field: 'applicantMaritalStatus')}" tabindex="6" />
			</div>
			<div class="clearfix ${hasErrors(bean: appl, field: 'applicantDriversLicenceNum', 'error')}">
				<label for="applicantDriversLicenceNum"><g:message code="financeApplication.applicantDriversLicenceNum" default="Drivers License #" /></label>
				<g:textField id="applicantDriversLicenceNum" class="short" name="applicantDriversLicenceNum" value="${fieldValue(bean: appl, field: 'applicantDriversLicenceNum')}" tabindex="7" />
			</div>
			
			<div class="clearfix ${hasErrors(bean: appl, field: 'applicantEmail', 'error')}">
				<label for="applicantEmail"><g:message code="financeApplication.applicantEmail" default="Email" /></label>
				<g:textField id="applicantEmail" name="applicantEmail" value="${fieldValue(bean: appl, field: 'applicantEmail')}" tabindex="8" />
			</div>
			<div class="clearfix ${hasErrors(bean: appl, field: 'applicantPhoneMobile', 'error')}">
				<label for="applicantPhoneMobile"><g:message code="financeApplication.applicantPhoneMobile" default="Phone (Mobile)" /></label>
				<g:textField id="applicantPhoneMobile" class="short" name="applicantPhoneMobile" value="${fieldValue(bean: appl, field: 'applicantPhoneMobile')}" tabindex="9" />
			</div>
			<div class="clearfix ${hasErrors(bean: appl, field: 'applicantPhoneWork', 'error')}">
				<label for="applicantPhoneWork"><g:message code="financeApplication.applicantPhoneWork" default="Phone (Work)" /></label>
				<g:textField id="applicantPhoneWork" class="short" name="applicantPhoneWork" value="${fieldValue(bean: appl, field: 'applicantPhoneWork')}" tabindex="10" />
			</div>
			<div class="clearfix ${hasErrors(bean: appl, field: 'applicantPhoneHome', 'error')}">
				<label for="applicantPhoneHome"><g:message code="financeApplication.applicantPhoneHome" default="Phone (Home)" /></label>
				<g:textField id="applicantPhoneHome" class="short" name="applicantPhoneHome" value="${fieldValue(bean: appl, field: 'applicantPhoneHome')}" tabindex="11" />
			</div>
		</div>
		
		<h3 class="topPad"><a class="expander collapsed" href="#" title="show/hide">Residency Details</a></h3>
		<div style="display:none;">
		<!-- <p>Select your desired lease term, and the estimated number of kilometres you will travel each year.</p> -->
		<div class="formSet">
			<div class="clearfix ${hasErrors(bean: appl, field: 'residenceCurrStatus', 'error')}">
				<label for="residenceCurrStatus"><g:message code="financeApplication.residenceCurrStatus" default="Residence Status" /></label>
				<g:textField id="residenceCurrStatus" class="short" name="residenceCurrStatus" value="${fieldValue(bean: appl, field: 'residenceCurrStatus')}" tabindex="20" />
			</div>
			<div class="clearfix ${hasErrors(bean: appl, field: 'residenceCurrThirdPartyName', 'error')}">
				<label for="residenceCurrThirdPartyName"><g:message code="financeApplication.residenceCurrThirdPartyName" default="Interested Party" /></label>
				<g:textField id="residenceCurrThirdPartyName" class="short" name="residenceCurrThirdPartyName" value="${fieldValue(bean: appl, field: 'residenceCurrThirdPartyName')}" tabindex="21" />
			</div>
			<div class="clearfix ${hasErrors(bean: appl, field: 'residenceCurrThirdPartyPhone', 'error')}">
				<label for="residenceCurrThirdPartyPhone"><g:message code="financeApplication.residenceCurrThirdPartyPhone" default="Party Phone" /></label>
				<g:textField id="residenceCurrThirdPartyPhone" class="short" name="residenceCurrThirdPartyPhone" value="${fieldValue(bean: appl, field: 'residenceCurrThirdPartyPhone')}" tabindex="22" />
			</div>
			<p class="mid-form">Current Address:</p>
			<div class="clearfix ${hasErrors(bean: appl, field: 'residenceCurrAddr1', 'error')}">
				<label for="residenceCurrAddr1"><g:message code="financeApplication.residenceCurrAddr1" default="Address 1" /></label>
				<g:textField id="residenceCurrAddr1" class="" name="residenceCurrAddr1" value="${fieldValue(bean: appl, field: 'residenceCurrAddr1')}" tabindex="23" />
			</div>
			<div class="clearfix ${hasErrors(bean: appl, field: 'residenceCurrAddr2', 'error')}">
				<label for="residenceCurrAddr2"><g:message code="financeApplication.residenceCurrAddr2" default="Address 2" /></label>
				<g:textField id="residenceCurrAddr2" class="" name="residenceCurrAddr2" value="${fieldValue(bean: appl, field: 'residenceCurrAddr2')}" tabindex="24" />
			</div>
			<div class="clearfix lefty ${hasErrors(bean: appl, field: 'residenceCurrAddr3', 'error')}">
				<label for="residenceCurrAddr3"><g:message code="financeApplication.residenceCurrAddr3" default="Suburb" /></label>
				<g:textField id="residenceCurrAddr3" class="short" name="residenceCurrAddr3" value="${fieldValue(bean: appl, field: 'residenceCurrAddr3')}" tabindex="25" />
			</div>
			<div class="clearfix lefty ${hasErrors(bean: appl, field: 'residenceCurrState', 'error')}">
				<label class="tight" for="residenceCurrState"><g:message code="financeApplication.residenceCurrState" default="State" /></label>
				<g:textField id="residenceCurrState" class="shortshort" name="residenceCurrState" value="${fieldValue(bean: appl, field: 'residenceCurrState')}" tabindex="26" />
			</div>
			<div class="clearfix lefty ${hasErrors(bean: appl, field: 'residenceCurrPostcode', 'error')}">
				<label class="tight" for="residenceCurrPostcode"><g:message code="financeApplication.residenceCurrPostcode" default="Postcode" /></label>
				<g:textField id="residenceCurrPostcode" class="shortshort" name="residenceCurrPostcode" value="${fieldValue(bean: appl, field: 'residenceCurrPostcode')}" tabindex="27" />
			</div>
			<div class="clearlefty"></div>
			<div class="clearfix lefty ${hasErrors(bean: appl, field: 'residenceCurrYears', 'error')}">
				<label for="residenceCurrYears"><g:message code="financeApplication.residenceCurrYears" default="Years" /></label>
				<g:textField id="residenceCurrYears" class="shortshort" name="residenceCurrYears" value="${fieldValue(bean: appl, field: 'residenceCurrYears')}" tabindex="28" />
			</div>
			<div class="clearfix lefty ${hasErrors(bean: appl, field: 'residenceCurrMonths', 'error')}">
				<label class="tight" for="residenceCurrMonths"><g:message code="financeApplication.residenceCurrMonths" default="Months" /></label>
				<g:textField id="residenceCurrMonths" class="shortshort" name="residenceCurrMonths" value="${fieldValue(bean: appl, field: 'residenceCurrMonths')}" tabindex="29" />
			</div>
			<div class="clearlefty"></div>
			
			<div id="prevAddr" style="display:none;">
				<p class="mid-form">Previous Address:</p>
				<div class="clearfix ${hasErrors(bean: appl, field: 'residencePrevAddr1', 'error')}">
					<label for="residencePrevAddr1"><g:message code="financeApplication.residencePrevAddr1" default="Address 1" /></label>
					<g:textField id="residencePrevAddr1" class="" name="residencePrevAddr1" value="${fieldValue(bean: appl, field: 'residencePrevAddr1')}" tabindex="30" />
				</div>
				<div class="clearfix ${hasErrors(bean: appl, field: 'residencePrevAddr2', 'error')}">
					<label for="residencePrevAddr2"><g:message code="financeApplication.residencePrevAddr2" default="Address 2" /></label>
					<g:textField id="residencePrevAddr2" class="" name="residencePrevAddr2" value="${fieldValue(bean: appl, field: 'residencePrevAddr2')}" tabindex="31" />
				</div>
				<div class="clearfix lefty ${hasErrors(bean: appl, field: 'residencePrevAddr3', 'error')}">
					<label for="residencePrevAddr3"><g:message code="financeApplication.residencePrevAddr3" default="Suburb" /></label>
					<g:textField id="residencePrevAddr3" class="short" name="residencePrevAddr3" value="${fieldValue(bean: appl, field: 'residencePrevAddr3')}" tabindex="32" />
				</div>
				<div class="clearfix lefty ${hasErrors(bean: appl, field: 'residencePrevState', 'error')}">
					<label class="tight" for="residencePrevState"><g:message code="financeApplication.residencePrevState" default="State" /></label>
					<g:textField id="residencePrevState" class="shortshort" name="residencePrevState" value="${fieldValue(bean: appl, field: 'residencePrevState')}" tabindex="33" />
				</div>
				<div class="clearfix lefty ${hasErrors(bean: appl, field: 'residencePrevPostcode', 'error')}">
					<label class="tight" for="residencePrevPostcode"><g:message code="financeApplication.residencePrevPostcode" default="Postcode" /></label>
					<g:textField id="residencePrevPostcode" class="shortshort" name="residencePrevPostcode" value="${fieldValue(bean: appl, field: 'residencePrevPostcode')}" tabindex="34" />
				</div>
				<div class="clearlefty"></div>
				<div class="clearfix lefty ${hasErrors(bean: appl, field: 'residencePrevYears', 'error')}">
					<label for="residencePrevYears"><g:message code="financeApplication.residencePrevYears" default="Years" /></label>
					<g:textField id="residencePrevYears" class="shortshort" name="residencePrevYears" value="${fieldValue(bean: appl, field: 'residencePrevYears')}" tabindex="35" />
				</div>
				<div class="clearfix lefty ${hasErrors(bean: appl, field: 'residencePrevMonths', 'error')}">
					<label class="tight" for="residencePrevMonths"><g:message code="financeApplication.residencePrevMonths" default="Months" /></label>
					<g:textField id="residencePrevMonths" class="shortshort" name="residencePrevMonths" value="${fieldValue(bean: appl, field: 'residencePrevMonths')}" tabindex="36" />
				</div>
				<div class="clearlefty"></div>
			</div>
		</div>
		</div>
		
		<h3 class="topPad"><a class="expander collapsed" href="#" title="show/hide">Employment Details</a></h3>
		<div style="display:none;">
			<div class="formSet">
				<div class="clearfix ${hasErrors(bean: appl, field: 'residencePrevMonths', 'error')}">
					<label for="residencePrevMonths"><g:message code="financeApplication.residencePrevMonths" default="Months" /></label>
					<g:textField id="residencePrevMonths" class="shortshort" name="residencePrevMonths" value="${fieldValue(bean: appl, field: 'residencePrevMonths')}" tabindex="40" />
				</div>
			</div>
		</div>
		
		<h3 class="topPad"><a class="expander collapsed" href="#" title="show/hide">Statement of Assets & Liabilities</a></h3>
		<div style="display:none;">
		<!-- <div> -->
			<div class="formSet">
				<div id="anl-wrapper">
					<div id="anl-assets" class="anl-table">
						<div class="anl-table-wrapper">
						<ul>
							<li class="hdr">
								<span>ASSETS</span>
							</li>
							<li class="bdy">
								<ol>
								<g:each in="${appl.assetItems}" var="i" status="idx">
									<g:render template="/templates/ui/finance/anlitems/asset" model="[id:idx, item:i]"/>
								</g:each>
									<li class="last">
										<span id="addAsset" class="add-item" rel="asset"><img alt="Add asset" src="/iris/images/skin/add.png" title="Add asset">&nbsp;Add asset</span>
									</li>
								</ol>
							</li>
							<li class="ftr">
								<span class="total">TOTAL ASSETS</span>
								<g:textField name='anl.total.asset' value="0" />
								<span class="currency-sign">&nbsp;$</span>
								<div class="clear"></div>
							</li>
						</ul>
						</div>
					</div>
					<div id="anl-liabilities" class="anl-table">
						<div class="anl-table-wrapper">
						<ul>
							<li class="hdr">
								<span>LIABILITIES</span>
							</li>
							<li class="bdy">
								<ol>
								<g:each in="${appl.liabilityItems}" var="i" status="idx">
									<g:render template="/templates/ui/finance/anlitems/liability" model="[id:idx, item:i]"/>
								</g:each>
									<li class="last">
										<span id="addLiability" class="add-item" rel="liability"><img alt="Add liability" src="/iris/images/skin/add.png">&nbsp;Add liability</span>
									</li>
								</ol>
							</li>
							<li class="ftr">
								<span class="total">TOTAL LIABILITIES</span>
								<g:textField name='anl.total.liability' value="0" />
								<span class="currency-sign">&nbsp;$</span>
								<div class="clear"></div>
							</li>
						</ul>
						</div>
					</div>
				</div>
				
				<!--  -->
				
				<!--  -->
				<div id="ine-wrapper">
					<div id="ine-income" class="anl-table">
						<div class="anl-table-wrapper">
						<ul>
							<li class="hdr">
								<span>INCOME PER MONTH</span>
							</li>
							<li class="bdy">
								<ol>
								<g:each in="${appl.incomeItems}" var="i" status="idx">
									<g:render template="/templates/ui/finance/anlitems/income" model="[id:idx, item:i]"/>
								</g:each>
									<li class="last">
										<span id="addIncome" class="add-item" rel="income"><img alt="Add income" src="/iris/images/skin/add.png">&nbsp;Add income</span>
									</li>
								</ol>
							</li>
							<li class="ftr">
								<span class="total">TOTAL INCOME</span>
								<g:textField name='anl.total.income' value="0" />
								<span class="currency-sign">&nbsp;$</span>
								<div class="clear"></div>
							</li>
						</ul>
						</div>
					</div>
					<div id="ine-expenses" class="anl-table">
						<div class="anl-table-wrapper">
						<ul>
							<li class="hdr">
								<span>EXPENSES PER MONTH</span>
							</li>
							<li class="bdy">
								<ol>
								<g:each in="${appl.expenseItems}" var="i" status="idx">
									<g:render template="/templates/ui/finance/anlitems/expense" model="[id:idx, item:i]"/>
								</g:each>
									<li class="last">
										<span id="addExpense" class="add-item" rel="expense"><img alt="Add expense" src="/iris/images/skin/add.png">&nbsp;Add expense</span>
									</li>
								</ol>
							</li>
							<li class="ftr">
								<span class="total">TOTAL EXPENSES</span>
								<g:textField name='anl.total.expense' value="0" />
								<span class="currency-sign">&nbsp;$</span>
								<div class="clear"></div>
							</li>
						</ul>
						</div>
					</div>
				</div>
				
				<!--  -->
				
			</div>
		</div>
		
	<g:if test="${appl.status == com.innovated.iris.enums.application.FinanceApplicationStatus.CREATED}">
		<div style="margin-top:20px;">
			<button id="btnSaveOnly" name="btnSaveOnly" class="greybutton" type="submit" tabindex="100">
				<span><img width="16" height="16" alt="" src="${resource(dir:'images/skin',file:'page_save.png')}"/>Save Progress</span>
			</button>
			<button id="btnSaveSubmit" name="btnSaveSubmit" class="greybutton" type="submit" tabindex="101">
				<span><img width="16" height="16" alt="" src="${resource(dir:'images/skin',file:'page_go.png')}"/>Submit for Approval</span>
			</button>
			<!-- <span class="formcancel"> or&nbsp;&nbsp;<a id="cancelFinAppl" href="#" tabindex="102">cancel</a></span> -->
		</div>
	</g:if>
		</g:form>
	</div>
</div>