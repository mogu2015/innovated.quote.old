<li>
	<g:select name='liability.type.${id}' from='${com.innovated.iris.enums.application.FinanceApplicationLiabilityType.lookup}' optionKey='key' optionValue='value' value='${item.type.getCode()}'></g:select>
	<span class="remove-item"><img alt="Remove liability" src="${resource(dir:'images/skin',file:'delete.png')}" title="Remove liability"></span>
	<g:textField name='liability.amount.${id}' value="${item.amount}" />
	<span class="currency-sign">&nbsp;$</span>
	<div class="clear"></div>
</li>