<li>
	<g:select name='expense.type.${id}' from='${com.innovated.iris.enums.application.FinanceApplicationExpenseType.lookup}' optionKey='key' optionValue='value' value='${item.type.getCode()}'></g:select>
	<span class="remove-item"><img alt="Remove expense" src="${resource(dir:'images/skin',file:'delete.png')}" title="Remove expense"></span>
	<g:textField name='expense.amount.${id}' value="${item.amount}" />
	<span class="currency-sign">&nbsp;$</span>
	<div class="clear"></div>
</li>