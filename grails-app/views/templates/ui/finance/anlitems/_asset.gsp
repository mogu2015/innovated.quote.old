<li>
	<g:select name='asset.type.${id}' from='${com.innovated.iris.enums.application.FinanceApplicationAssetType.lookup}' optionKey='key' optionValue='value' value='${item.type.getCode()}'></g:select>
	<span class="remove-item"><img alt="Remove asset" src="${resource(dir:'images/skin',file:'delete.png')}" title="Remove asset"></span>
	<g:textField name='asset.amount.${id}' value="${item.amount}" />
	<span class="currency-sign">&nbsp;$</span>
	<div class="clear"></div>
</li>