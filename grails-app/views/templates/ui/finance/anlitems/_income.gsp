<li>
	<g:select name='income.type.${id}' from='${com.innovated.iris.enums.application.FinanceApplicationIncomeType.lookup}' optionKey='key' optionValue='value' value='${item.type.getCode()}'></g:select>
	<span class="remove-item"><img alt="Remove income" src="${resource(dir:'images/skin',file:'delete.png')}" title="Remove income"></span>
	<g:textField name='income.amount.${id}' value="${item.amount}" />
	<span class="currency-sign">&nbsp;$</span>
	<div class="clear"></div>
</li>