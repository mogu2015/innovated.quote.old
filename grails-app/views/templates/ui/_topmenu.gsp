<g:ifAnyGranted role="ROLE_SUPER,ROLE_ADMIN">
<div class="adminNavs">
	<ul>
		<li><a href="#"><span>Preferences</span></a></li>
	</ul>
</div>
</g:ifAnyGranted>
<div class="adminNavs adminRight">
	<ul>
		<li><a><span>Welcome, <g:loggedInUserInfo field="username"/>!</span></a></li>
		<!-- <li><g:link controller="user" action="settings"><span>Account Settings</span></g:link></li>
		<li><a href="#"><span>Help</span></a></li> -->
		<li><g:link controller="logout" action="index"><span>Logout</span></g:link></li>
	</ul>
</div>
<div class="clear"></div>