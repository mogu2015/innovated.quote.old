<g:if test="${elementId}">
<g:if test="${style}"><div id="${elementId}" class="optGrpMenuItem" style="${style}"></g:if><g:else><div id="${elementId}" class="optGrpMenuItem"></g:else>
</g:if><g:else>
<g:if test="${style}"><div class="optGrpMenuItem" style="${style}"></g:if><g:else><div class="optGrpMenuItem"></g:else>
</g:else>
	<dt>
		<g:if test="${id}">
		<g:link controller="${controller}" action="${action}" id="${id}"><img width="16" height="16" alt="${message(code:titleCode)}" src="${resource(dir:'images/skin',file:icon)}"/></g:link>
		</g:if>
		<g:else>
		<g:link controller="${controller}" action="${action}"><img width="16" height="16" alt="${message(code:titleCode)}" src="${resource(dir:'images/skin',file:icon)}"/></g:link>
		</g:else>
	</dt>
	<dd>
		<g:if test="${id}"><g:link controller="${controller}" action="${action}" id="${id}"><g:message code="${titleCode}" /></g:link></g:if>
		<g:else><g:link controller="${controller}" action="${action}"><g:message code="${titleCode}" /></g:link></g:else>
	</dd>
	<dd class="last"><g:message code="${msgCode}" /></dd>
</div>