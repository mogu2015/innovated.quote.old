<meta name="robots" content="noindex">
		<meta http-equiv="content-type" content="application/xhtml+xml; charset=utf-8">
        <link rel="stylesheet" type="text/css" href="${resource(dir:'css',file:'reset.css')}">
        <link rel="stylesheet" type="text/css" href="${resource(dir:'css/ui-iris',file:'jquery-ui-1.8.2.custom.css')}">
        
        <link rel="stylesheet" type="text/css" href="${resource(dir:'css',file:'prod.css')}">
        <link rel="stylesheet" type="text/css" href="${resource(dir:'css',file:'jquery.gritter.css')}">
        <link rel="stylesheet" type="text/css" href="${resource(dir:'css',file:'tipsy.css')}">
        <link rel="stylesheet" type="text/css" href="${resource(dir:'css',file:'jquery.loadmask.css')}">
        
        <link rel="shortcut icon" href="${resource(dir:'images',file:'favicon.ico')}" type="image/x-icon">
        
        <g:javascript library="jquery/jquery-1.4.2.min"/>
        <!-- <script src="http://cdn.jquerytools.org/1.2.5/full/jquery.tools.min.js"></script> -->
        
        <!-- <g:javascript library="jquery" plugin="jquery"/> -->
        <g:javascript library="jquery/jquery-ui-1.8.2.custom.min"/>
        <g:javascript library="jquery/jquery.cookie"/>
        <g:javascript library="jquery/jquery.formatCurrency-1.4.0.min"/>
        <g:javascript library="jquery/jqbrowser.min"/>
        <g:javascript library="jquery/jquery.bgiframe.min"/>
        <g:javascript library="jquery/jquery.pngFix.pack"/>
        <g:javascript library="highcharts"/>
        <g:javascript library="jquery/jquery.simplemodal.1.4.1.min"/>
        <g:javascript library="jquery/jquery.tablesorter.min"/>
        <g:javascript library="jquery/jquery.uitablefilter"/>
        <g:javascript library="jquery/jquery.gritter.min"/>
        <g:javascript library="jquery/jquery.tipsy"/>
        <g:javascript library="jquery/jquery.loadmask.min"/>
		
        <!--[if IE]><g:javascript library="excanvas.compiled"/><![endif]-->
		
		<!-- IE specific styling -->
		<!--[if IE 6]><link rel="stylesheet" type="text/css" href="${resource(dir:'css',file:'ie6.css')}"><![endif]-->
		<!--[if IE 7]><link rel="stylesheet" type="text/css" href="${resource(dir:'css',file:'ie7.css')}"><![endif]-->
        
        