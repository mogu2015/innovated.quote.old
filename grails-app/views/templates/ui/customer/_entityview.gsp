<!-- _viewentityinfo.gsp -->

<div class="formSet">
	<table class="grid">
		<tbody>
			<tr>
				<th>Entity Name</th><td class="special">${customerInstance.entity.name}</td>
				<td class="spacer">&nbsp;</td>
				<th>Entity Type</th><td class="special">${customerInstance.entity.type}</td>
			</tr>
		<g:if test="${customerInstance.entity.type == 'Company' || customerInstance.entity.type == 'Other'}">
			<g:set var="company" value="${(com.innovated.iris.domain.Company) customerInstance.entity}"/>
			<tr>
				<th>Trading Name</th><td>${company.tradingName}</td>
				<td class="spacer">&nbsp;</td>
				<th>ABN</th><td>${(company.abn) ?: "-"}</td>
			</tr>
			<tr>
				<th>Parent</th><g:set var="parent" value="${company.parent}"/><td>${(parent) ? parent.tradingName : "-"}</td>
				<td class="spacer">&nbsp;</td>
				<th>ACN</th><td>${(company.acn) ?: "-"}</td>
			</tr>
			<tr>
				<th>Employer Code</th><td class="special">${company.employerCode}</td>
				<td class="spacer">&nbsp;</td>
				<th>Pay Frequency</th><td>${company.defaultPayFreq?.getName()}</td>
			</tr>
			<tr>
				<th>FBT Details</th>
				<td colspan="4">
					<!--  -->
					
				</td>
			</tr>
			<tr>
				<th>Claims GST?</th>
				<td>
					<g:if test="${company.canClaimGst}"><img src="${resource(dir:'images/skin',file:'tick.png')}" alt="Yes" title="This company claims GST" /></g:if>
					<g:else><img src="${resource(dir:'images/skin',file:'cross.png')}" alt="No" title="This company does not claim GST" /></g:else>
				</td>
				<td class="spacer">&nbsp;</td>
				<th>&nbsp;</th><td>&nbsp;</td>
			</tr>
		</g:if>
		<g:else>
			<g:set var="individual" value="${(com.innovated.iris.domain.Individual) customerInstance.entity}"/>
			<tr>
				<th>First Name</th><td>${individual.firstName}</td>
				<td class="spacer">&nbsp;</td>
				<th>Preferred Name</th><td>${individual.preferredName}</td>
			</tr>
			<tr>
				<th>Middle Name</th><td>${individual.middleName}</td>
				<td class="spacer">&nbsp;</td>
				<th>Date of Birth</th><g:set var="dob" value="${individual.dateOfBirth}"/>
				<td><span style="padding-right:10px;"><g:formatDate format="d MMM yyyy" date="${dob}"/></span><g:if test="${dob != null}">(${ new Date().year - dob.year } years old)</g:if></td>
			</tr>
			<tr>
				<th>Last Name</th><td>${individual.lastName}</td>
				<td class="spacer">&nbsp;</td>
				<th>Gender</th><td>${individual.gender?.getName()}</td>
			</tr>
		</g:else>
			<tr>
				<th>Addresses</th>
				<td colspan="4">
				<g:if test="${customerInstance.entity.addresses?.size() > 0}">
					<ol>
					<g:each in="${customerInstance.entity.addresses}" var="addr">
						<!-- <li><span style="padding-right:20px;">${addr}</span>(<a class="mapLink" href="#">View on map</a>)</li> -->
						<li><a class="mapLink" href="#" title="View this address on a map">${addr}</a> (${addr.addressType.name})</span></li>
					</g:each>
					</ol>
				</g:if>
				<g:else>
					There are <b>currently no addresses</b> recorded for this customer.
				</g:else>
				</td>
			</tr>
			<tr>
				<th>Emails</th>
				<td colspan="4">
				<g:if test="${customerInstance.entity.emails?.size() > 0}">
					<ol>
					<g:each in="${customerInstance.entity.emails}" var="email">
						<li><a href="mailto:${email}" title="Send an email to: ${email}">${email}</a></li>
					</g:each>
					</ol>
				</g:if>
				<g:else>
					There are <b>currently no email addresses</b> recorded for this customer.
				</g:else>
				</td>
			</tr>
			<tr>
				<th class="last">Phones</th>
				<td class="last" colspan="4">
				<g:if test="${customerInstance.entity.phones?.size() > 0}">
					<ol>
					<g:each in="${customerInstance.entity.phones}" var="phone">
						<li>${phone} (${phone.phoneType.name})</li>
					</g:each>
					</ol>
				</g:if>
				<g:else>
					There are <b>currently no phone numbers</b> recorded for this customer.
				</g:else>
				</td>
			</tr>
		</tbody>
	</table>
</div>