
<style>
textarea {
    width:98%;
}
.textwrapper {
    /*border:1px solid #999999;*/
    margin:5px 0;
    padding:0;
}
</style>

<script type="text/javascript">
/*function removeRow(id) {
	var tbody = $("#"+id).parent();
	$("#"+id).fadeOut('slow', function(){
		$(this).remove();
		var rowCount = $("#comments >tbody >tr").length;
		$(".listTotal span").html(rowCount);
		if (rowCount == 0) location.reload();
	});
}*/
function addRow(data) {
	/*
	var flashMsg = "<div class='flashMsg'><p class='flashOk'>Added note: <b>" + data.comment.body + "</b></p></div>";
	$(".messages").html(flashMsg).fadeIn("fast").delay(5000).fadeOut("slow");
	*/
	var row = $(data.rowMarkup);
	var first = $("table#comments > tbody > tr:first");
	
	var numComments = $("table#comments > tbody").find("tr:visible").length;
	if (numComments == 0)
		row.appendTo("table#comments > tbody").show();
	else
		row.insertBefore(first);
	row.effect("highlight", {}, 3000);
	$("table#comments").trigger("update");
	$(".listTotal span").text($("table#comments > tbody").find("tr:visible").length);
	$.gritter.add({ title:"New Note Created", text:"Successfully added new notation for ${customerInstance.entity.name}" });
}

$(document).ready(function() {
	var setTable = $("#comments");
	setTable.tablesorter({
		headers: { 0: { sorter:false } }
	});
	$("#commentsFilter").keyup(function() {
		$.uiTableFilter(setTable, this.value, null, function() {
			// update the table count label
			$(".listTotal span").text(setTable.find("tbody > tr:visible").length);
		});
	});
	$("#commentsAddBtn").button().click(function() {
		var commentBody = $("#commentsAddFld").val();
		if (commentBody) {
			// convert newlines into linebreaks
			commentBody = commentBody.replace(/\n/g, '<br/>');
			$.post("${createLink(controller:'commentable', action:'add')}", {
					"json":true, "update":"comments", "comment.body": commentBody,
					"commentLink.commentRef": "${customerInstance.id}",
					"commentLink.type": "${customerInstance.class.name}",
					"commentPageURI":"${request.forwardURI}"
				},
				function(data, status, xhr) {
					$("#commentsAddFld").val("");
					addRow(data);
				},
				"json"
			);
		}
		//return false;
	});
});
</script>

<h3 class="topPad">Customer History for <b>${customerInstance.entity.name}</b></h3>
<p>View and/or Add notes to this customers account history.</p>
	<div style="display:block; margin-bottom:20px;">
	    <div class="textwrapper"><textarea name="commentsAddFld" id="commentsAddFld" class="text ui-widget-content ui-corner-all"/></div>
	    <a id="commentsAddBtn" name="commentsAddBtn" href="#addNote">Add This Note...</a>
	</div>


	<div class="criteria" style="padding-top:0;">
		<form>
			<label>Filter:</label>
			<input type="text" id="commentsFilter" name="commentsFilter" value="" maxlength="30" size="30"/>
		</form>
	</div>
	<table id="comments" class="list">
		<thead>
			<tr>
				<th class="hdrLeft" width="20"><g:checkBox id="chkAll" name="chkAll" value="${false}" title="Select all rows" /></th>
				<th class="hdrCtr" nowrap width="60%"><g:message code="ui.table.header.comment.body" default="Notes / Comments" /></th>
				<th class="hdrCtr" nowrap width="60"><g:message code="ui.table.header.comment.poster" default="User" /></th>
				<th class="hdrRight" nowrap width="60"><g:message code="ui.table.header.comment.datecreated" default="Date" /></th>
			</tr>
		</thead>
		<tbody>
			<g:each in="${commentList}" status="idx" var="c">
			<g:render template="/templates/ui/commentable/comment" var="comment" bean="${c}" />
			</g:each>
		</tbody>
	</table>
	<p class="listTotal">(# comments shown: <span>${commentList.size()}</span>)</p>


