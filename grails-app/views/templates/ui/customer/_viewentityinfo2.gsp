<h3 class="topPad">Entity Details</h3>
								<p>View a customer's entity and contact information here. These details can be <a id="entityLink" href="#">updated here</a>.</p>
							<div class="formSet">
								<table class="grid">
									<tbody>
										<tr>
											<th>Entity Name</th>
											<td class="special" width="50%">${customerInstance.entity.name}</td>
											<td class="spacer">&nbsp;</td>
											<th>Entity Type</th>
											<td class="special" width="50%">${customerInstance.entity.type}</td>
										</tr>
										<tr>
											<th>Addresses</th>
											<td>
												<g:if test="${customerInstance.entity.addresses?.size() > 0}">
												<ol>
												<g:each in="${customerInstance.entity.addresses}" var="addr">
													<li>${addr}</li>
												</g:each>
												</ol>
												</g:if>
												<g:else>There are currently no addresses recorded for this customer.</g:else>
											</td>
											<td class="spacer">&nbsp;</td>
											<th class="last" rowspan="2">Phones</th>
											<td class="last" rowspan="2">
												<g:if test="${customerInstance.entity.phones?.size() > 0}">
												<ol>
												<g:each in="${customerInstance.entity.phones}" var="phone">
													<li>${phone} (${phone.phoneType.name})</li>
												</g:each>
												</ol>
												</g:if>
												<g:else>There are currently no phone numbers recorded for this customer.</g:else>
											</td>
										</tr>
										<tr>
											<th class="last">Emails</th>
											<td class="last">
												<g:if test="${customerInstance.entity.emails?.size() > 0}">
												<ol>
												<g:each in="${customerInstance.entity.emails}" var="email">
													<li>${email}</li>
												</g:each>
												</ol>
												</g:if>
												<g:else>There are currently no email addresses recorded for this customer.</g:else>
											</td>
											<td class="last spacer">&nbsp;</td>
											<th class="last">&nbsp;</th>
											<td class="last">&nbsp;</td>
										</tr>
									</tbody>
								</table>
							</div>