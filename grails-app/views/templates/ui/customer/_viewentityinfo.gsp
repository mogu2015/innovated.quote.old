<script type="text/javascript">
$(document).ready(function() {
	$("#entity-tabs").tabs({
		spinner:"",
		idPrefix:"entity-",
		load: function(event, ui) {
			//$('.spinner').fadeOut('slow', function() { $(".formWrap").fadeIn('fast'); });
		}
	});
});
</script>
<h3 class="topPad">Entity Details</h3>
<p>View a customer's entity and contact information here.</p>
<div id="entity-tabs" style="margin-bottom:12px;">
	<ul>
		<li><g:link controller="customer" action="view" id="${customerInstance.id}" params="[tab:'entityView']"><span>View</span></g:link></li>
		<li><g:link controller="customer" action="view" id="${customerInstance.id}" params="[tab:'entityEdit']"><span>Edit</span></g:link></li>
	</ul>
</div>