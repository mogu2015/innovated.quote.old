<g:set var="company" value="${(com.innovated.iris.domain.Company) customerInstance.entity}"/>
<div class="formBg">
	<div class="formWrap">
	
	<script type="text/javascript">
	function updComplete(xhr) {
		var r = $.parseJSON(xhr.responseText);
		$(".messages .errors").remove();
		if (xhr.status != 200) {
			if (r.errors) {
				if ($(".messages .errors").length == 0) {
				    var errorDiv = $("<div class='errors'><ul></ul></div>");
					errorDiv.appendTo(".messages");//.show();
				}
				$.each(r.errors, function(i,v) { 
					$("#"+v.field).parent().addClass("errors");
					// add error list items...
					$("<li>" + v.message + "</li>").appendTo(".messages .errors ul");
				});
			}
		} else {
			$(".formSet .errors").removeClass("errors");
			hidePanel({rel:"entity"});
			$("#tabs").tabs("load", 2);		// refresh ENTITY tab content! (tab #2)
		}
		$.gritter.add({ title:r.title, text:r.text, image:r.img });
		$(window).scrollTop(0);
	}
	</script>
	
	<g:formRemote name="editCompany" update="messages" url="[controller:'company', action:'updateAjax']" onComplete="updComplete(XMLHttpRequest);">
		<g:hiddenField name="id" value="${company?.id}" />
        <g:hiddenField name="version" value="${company?.version}" />
        <g:hiddenField name="returnController" value="${returnController ?: 'customer'}" />
        <g:hiddenField name="returnUrl" value="${createLink(controller:controllerName, action:actionName, params:params)}" />
	
		<h3 class="topPad">Company Information</h3>
		<!-- <p>Update the entity record backing this customer.</p> -->
		<div class="formSet">
			<div class="clearfix">
				<label for="parent.id">Parent</label>
				<g:select class="long" optionKey="id" from="${com.innovated.iris.domain.Company.findAllByIdNotEqual(company?.id)}" name="parent.id" value="${company?.parent?.id}" noSelection="['-1':'']"></g:select>
			</div>
			<div class="clearfix">
				<label for="tradingName">Trading Name</label>
				<g:textField name="tradingName" value="${company.tradingName}" />
			</div>
			<div class="clearfix">
				<label for="abn">ABN</label>
				<g:textField class="short" name="abn" value="${company.abn}" />
			</div>
			<div class="clearfix">
				<label for="acn">ACN</label>
				<g:textField class="short" name="acn" value="${company.acn}" />
			</div>
			<div class="clearfix">
				<label for="defaultPayFreq">Pay Frequency</label>
				<g:select class="short" name="defaultPayFreq" from="${com.innovated.iris.util.PayrollFrequency.lookup}" optionKey="key" optionValue="value" value="${company.defaultPayFreq.code}"></g:select>
			</div>
		</div>
		
		<h3 class="topPad">Employee Access Code</h3>
		<p>The employer code allows individual employees to register for access to IRIS, including the novated budget quote system.</p>
		<div class="formSet">
			<div class="clearfix">
				<label for="employerCode" class="special">Employer Code</label>
				<g:textField class="short" name="employerCode" value="${company.employerCode}" />
			</div>
		</div>
		
		<h3 class="topPad">FBT Related Details</h3>
		<p>FBT details are relavent for Novated Customers, particularly for driving the default values used in the Novated Budget Quote process.</p>
		<div class="formSet">
			<div class="clearfix">
				<label for="canClaimGst">Can Claim GST?</label>
				<g:checkBox name="canClaimGst" value="${company.canClaimGst}" />
			</div>
			<div class="clearfix">
				<label for="allowsFbtEcm">Allows ECM?</label>
				<g:checkBox name="allowsFbtEcm" value="${company.allowsFbtEcm}" />
			</div>
			<div class="clearfix">
				<label for="current_concession">FBT Concession</label>
				<g:select class="long" name="current_concession" from="${concessionMap}" optionKey="key" optionValue="value" value="${currentConcession}"></g:select>
			</div>
			<div class="clearfix">
				<label>Taxable Values</label>
				<g:if test="${taxableValueMap.size() > 0}">
				<table class="tableField">
					<tr>
						<th>Allowed?</th>
						<th>Type</th>
						<th>Default</th>
					</tr>
					<g:each status="i" var="tv" in="${taxableValueMap}">
					<tr>
						<td class="centered"><g:checkBox name="ALLOWED_TV_${tv.key.key}" value="${tv.value}" /></td>
						<td>${tv.key.value.encodeAsHTML()}</td>
						<g:if test="${company.allowedFbtCalcMethods.size() > 0 && tv.key.value == company.allowedFbtCalcMethods.get(0)}">
						<td class="centered"><g:radio id="tv_default_${tv.key.key}" name="tv_default" value="${tv.key.key}" checked="checked"/></td>
						</g:if>
						<g:else>
						<td class="centered"><g:radio id="tv_default_${tv.key.key}" name="tv_default" value="${tv.key.key}"/></td>
						</g:else>
					</tr>
					</g:each>
				</table>
				</g:if>
			</div>
			<div class="clearfix">
				<label for="employerShareFee">Benefit Share Fee *</label>
				<g:textField name="employerShareFee" class="numeric" value="${formatNumber(number:company.employerShareFee, type:'number', minFractionDigits:2, maxFractionDigits:2)}" />&nbsp;(Annually)
			</div>
			<div class="clearfix">
				<label for="employerShareRate">Benefit Share Rate *</label>
				<g:textField name="employerShareRate" class="numeric" value="${formatNumber(number:company.employerShareRate, type:'number', minFractionDigits:0, maxFractionDigits:0)}" />&nbsp;%
			</div>
			<p class="fineprint"><span>Please Note:</span>* The benefit share only applies to certain employers (ie. NSW Health, etc).</p>
		</div>
		
		<button id="btnEntityUpdate" name="btnEntityUpdate" class="greybutton" type="submit" tabindex="34">
			<span><img width="16" height="16" alt="" src="${resource(dir:'images/skin',file:'disk.png')}"/>Update</span>
		</button>
		<span class="formcancel"> or&nbsp;&nbsp;<a rel="entity" class="linkCancel" href="" onclick="hidePanel(this); return false;">cancel</a></span>
		
	</g:formRemote>
	</div>
</div>
