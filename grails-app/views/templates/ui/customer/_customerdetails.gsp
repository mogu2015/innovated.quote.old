<script type="text/javascript">
	$(document).ready(function() {
		$('#joinedDate, #ceasedDate').datepicker({
			changeMonth: true, changeYear: true, dateFormat: 'd M yy', showButtonPanel: true,
			showOtherMonths: true, selectOtherMonths: true, showOn: 'both', buttonImageOnly: true,
			buttonText: 'Select Date...', buttonImage: "${resource(dir:'images/skin',file:'calendar.png')}"
		});
	});
</script>

<h3>Customer Details</h3>
									<p>Update customer details and settings here.</p>
									<div class="formSet">
										<!-- 
										<div class="clearfix ${hasErrors(bean: customerInstance, field: 'code', 'error')}">
											<label for="code"><g:message code="customer.code" default="Customer Code" /></label>
											<g:textField class="short" name="code" readonly="readonly" value="${fieldValue(bean: customerInstance, field: 'code')}" />
										</div>
										 -->
										<div class="clearfix ${hasErrors(bean: customerInstance, field: 'code', 'error')}">
											<label for="code"><g:message code="customer.code" default="Customer Code" /></label>
											<g:textField class="short roInput" name="code" readonly="readonly" value="${fieldValue(bean: customerInstance, field: 'code')}" />
										</div>
										
										<div class="clearfix ${hasErrors(bean: customerInstance, field: 'supplierLinkCode', 'error')}">
											<label for="supplierLinkCode"><g:message code="customer.supplierLinkCode" default="Supplier Link Code" /></label>
											<g:textField class="short" name="supplierLinkCode" value="${fieldValue(bean: customerInstance, field: 'supplierLinkCode')}" />
										</div>
										<div class="clearfix ${hasErrors(bean: customerInstance, field: 'type', 'error')}">
											<label for="type"><g:message code="customer.type" default="Type" /></label>
											<g:select class="short" name="type.id" from="${com.innovated.iris.domain.enums.CustomerType.list()}" optionKey="id" value="${customerInstance?.type?.id}"/>
										</div>
										<div class="clearfix ${hasErrors(bean: customerInstance, field: 'status', 'error')}">
											<label for="status"><g:message code="customer.status" default="Status" /></label>
											<g:select class="short" name="status.id" from="${com.innovated.iris.domain.enums.CustomerStatus.list()}" optionKey="id" value="${customerInstance?.status?.id}"  />
										</div>
										<div class="clearfix ${hasErrors(bean: customerInstance, field: 'joined', 'error')}">
											<label for="joinedDate"><g:message code="customer.joined" default="Joined" /></label>
											<g:textField class="short uneditable" id="joinedDate" name="joinedDate" value="${formatDate(date:customerInstance?.joined, format:'d MMM yyyy')}" />
										</div>
										<div class="clearfix ${hasErrors(bean: customerInstance, field: 'ceased', 'error')}">
											<label for="ceasedDate"><g:message code="customer.ceased" default="Ceased" /></label>
											<g:textField class="short uneditable" id="ceasedDate" name="ceasedDate" value="${formatDate(date:customerInstance?.ceased, format:'d MMM yyyy')}" />
										</div>
									</div>