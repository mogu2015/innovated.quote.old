<script type="text/javascript">
$(document).ready(function() {
	var $tabs = $("#tabs").tabs({
		spinner:"",
		idPrefix:"cust-",
		load: function(event, ui) {
			$('.spinner').fadeOut('slow', function() { $(".formWrap").fadeIn('fast'); });
		},
		select: function(event, ui) {
			if (ui.index == 1) {
				// show the "view" mode whenever selecting the entity tab!
				$("#entity-tabs").tabs("select",0);
			}
		}
	});
});
</script>
<div id="tabs" class="ui-tabs-hide" style="margin-bottom:12px;">
	<ul>
		<li><g:link controller="customer" action="view" id="${customerInstance.id}" params="[tab:'customer']"><span>Customer Details</span></g:link></li>
		<li><g:link controller="customer" action="view" id="${customerInstance.id}" params="[tab:'entity']"><span>Entity Details</span></g:link></li>
	<g:if test="${customerInstance.entity.type == 'Company'}">
		<li><g:link controller="customer" action="view" id="${customerInstance.id}" params="[tab:'employees']"><span>Employees</span></g:link></li>
	</g:if>
		<li><g:link controller="customer" action="view" id="${customerInstance.id}" params="[tab:'assets']"><span>Assets</span></g:link></li>
		<li><g:link controller="customer" action="view" id="${customerInstance.id}" params="[tab:'comments']"><span>Notes</span></g:link></li>
	</ul>
</div>