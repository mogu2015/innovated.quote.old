<g:set var="person" value="${(com.innovated.iris.domain.Individual) customerInstance.entity}"/>
<div class="formBg">
	<div class="formWrap">
	<g:form controller="individual" action="update" method="post" >
		<g:hiddenField name="id" value="${person?.id}" />
        <g:hiddenField name="version" value="${person?.version}" />
        
		<h3 class="topPad">Personal Information</h3>
		<p>Update the entity record backing this customer.</p>
		<div class="formSet">
			<div class="clearfix"><p>Entity information</p></div>
		</div>
		
		<h3 class="topPad">Backing Entity</h3>
		<p>Update the entity record backing this customer.</p>
		<div class="formSet">
			<div class="clearfix">
			<p>Entity information</p>
			</div>
		</div>
		
		<h3 class="topPad">Backing Entity</h3>
		<p>Update the entity record backing this customer.</p>
		<div class="formSet">
			<div class="clearfix">
			<p>Entity information</p>
			</div>
		</div>
		
		<button id="btnEntityUpdate" name="btnEntityUpdate" class="greybutton" type="submit" tabindex="34">
			<span><img width="16" height="16" alt="" src="${resource(dir:'images/skin',file:'disk.png')}"/>Update</span>
		</button>
		<span class="formcancel"> or&nbsp;&nbsp;<a rel="entity" class="linkCancel" href="" onclick="hidePanel(this); return false;">cancel</a></span>
	
	</g:form>
	</div>
</div>
