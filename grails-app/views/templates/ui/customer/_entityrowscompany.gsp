<g:set var="company" value="${(com.innovated.iris.domain.Company) customerInstance.entity}"/>
<tr>
	<th>Trading Name</th>
	<td>${company.tradingName}</td>
	<td class="spacer">&nbsp;</td>
	<th>ABN</th><g:set var="abn" value="${((com.innovated.iris.domain.Company) customerInstance.entity).abn}"/>
	<td>${(abn) ?: "-"}</td>
</tr>
<tr>
	<th>Parent</th><g:set var="parent" value="${((com.innovated.iris.domain.Company) customerInstance.entity).parent}"/>
	<td>${(parent) ? parent.tradingName : "-"}</td>
	<td class="spacer">&nbsp;</td>
	<th>ACN</th><g:set var="acn" value="${((com.innovated.iris.domain.Company) customerInstance.entity).acn}"/>
	<td>${(acn) ?: "-"}</td>
</tr>
<tr>
	<th>Employer Code</th>
	<td class="special">${((com.innovated.iris.domain.Company) customerInstance.entity).employerCode}</td>
	<td class="spacer">&nbsp;</td>
	<th>Pay Frequency</th>
	<td>${((com.innovated.iris.domain.Company) customerInstance.entity).defaultPayFreq?.getName()}</td>
</tr>
<tr>
	<th>FBT Details</th>
	<td colspan="4">
		<!--  -->
		
	</td>
</tr>
<tr>
	<th>Claims GST?</th>
	<td>
		<g:if test="${((com.innovated.iris.domain.Company) customerInstance.entity).canClaimGst}"><img src="${resource(dir:'images/skin',file:'tick.png')}" alt="Yes" title="This company claims GST" /></g:if>
		<g:else><img src="${resource(dir:'images/skin',file:'cross.png')}" alt="No" title="This company does not claim GST" /></g:else>
	</td>
	<td class="spacer">&nbsp;</td>
	<th>&nbsp;</th>
	<td>&nbsp;</td>
</tr>
