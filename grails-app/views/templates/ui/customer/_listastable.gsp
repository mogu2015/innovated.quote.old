<script type="text/javascript">
$(document).ready(function() {
	var table = $('#clients').dataTable({
		"iDisplayLength": 100, "sPaginationType": "full_numbers",
		"iCookieDuration": 3600, "bStateSave": true, "bProcessing": true, "bServerSide": true,
		"sAjaxSource": "${createLink(controller:'customer', action:'listAjax')}",
		//"aLengthMenu": [[10, 25, 50, 100, 250, -1], [10, 25, 50, 100, 250, "All"]],
		"aLengthMenu": [[25, 50, 100, 250, 500, 1000], [25, 50, "100 (Default)", 250, 500, 1000]],
		"aaSorting": [[ 3, "asc" ]],
		"aoColumns": [
			{ "bSortable":false, "sClass":"cellLeft" },
			{ "bSortable":false, "sClass":"cellCtr" },
			{ "sClass":"cellCtr" },
			{ "sClass":"cellCtr" },
			{ "sClass":"cellCtr" },
			{ "sClass":"cellRight" }
		]
	});
	table.fnSetFilteringDelay(300);	// 300 msec after "keyup" the ajax lookup will fire
});
</script>
<!-- 
<div class="criteria"><form id="">Filter: <input type="text" id="filter" name="filter" value="" maxlength="30" size="30"/></form></div>
 -->
<table id="clients" class="list">
<thead>
	<tr>
		<th class="hdrLeft" width="20"><g:checkBox id="chkAll" name="chkAll" value="${false}" title="Select all rows" /></th>
		<th class="hdrCtr" width="16">&nbsp;</th>
		<th class="hdrCtr" nowrap width="40"><g:message code="ui.table.header.customer.code" default="Code" /></th>
		<th class="hdrCtr" nowrap width="50%"><g:message code="ui.table.header.customer.entity" default="Client Name" /></th>
		<th class="hdrCtr" nowrap width="60"><g:message code="ui.table.header.customer.type" default="Type" /></th>
		<th class="hdrRight" nowrap width="60"><g:message code="ui.table.header.customer.status" default="Status" /></th>
	</tr>
</thead>
<tbody>
	<tr>
		<td colspan="6" class="dataTables_empty">Loading data from server</td>
	</tr>
</tbody>
</table>
