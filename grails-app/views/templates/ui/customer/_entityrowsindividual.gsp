<tr>
	<th>First Name</th>
	<td>${((com.innovated.iris.domain.Individual) customerInstance.entity).firstName}</td>
	<td class="spacer">&nbsp;</td>
	<th>Preferred Name</th>
	<td>${((com.innovated.iris.domain.Individual) customerInstance.entity).preferredName}</td>
</tr>
<tr>
	<th>Middle Name</th>
	<td>${((com.innovated.iris.domain.Individual) customerInstance.entity).middleName}</td>
	<td class="spacer">&nbsp;</td>
	<th>Date of Birth</th><g:set var="dob" value="${((com.innovated.iris.domain.Individual) customerInstance.entity).dateOfBirth}"/>
	<td><span style="padding-right:10px;"><g:formatDate format="d MMM yyyy" date="${dob}"/></span><g:if test="${dob != null}">(${ new Date().year - dob.year } years old)</g:if></td>
</tr>
<tr>
	<th>Last Name</th>
	<td>${((com.innovated.iris.domain.Individual) customerInstance.entity).lastName}</td>
	<td class="spacer">&nbsp;</td>
	<th>Gender</th>
	<td>${((com.innovated.iris.domain.Individual) customerInstance.entity).gender?.getName()}</td>
</tr>
