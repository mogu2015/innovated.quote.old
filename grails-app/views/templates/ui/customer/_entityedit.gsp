<div id="entityPanel">
	<p class="bread">
		<g:link controller="customer" action="list"><g:message code="ui.menu.label.customers" default="Customers" /></g:link>
		<span class="breadArrow">&nbsp;</span>
		<a rel="entity" class="linkCancel" href="#"><g:message code="ui.menu.label.customer.edit" default="Edit Customer" /></a>
		<span class="breadArrow">&nbsp;</span>
		<g:message code="ui.menu.label.customer.entity.edit" default="Entity Details" />
	</p>
	
	<g:set var="returnController" value="customer"/>
	<div class="messages"></div>
	
	<h1 class="contentH1">Entity Details: ${customerInstance.entity.name}</h1>
	<g:if test="${customerInstance.entity.type == 'Company'}">company</g:if>
	<g:else>individual</g:else>
</div> <!-- #entityPanel -->