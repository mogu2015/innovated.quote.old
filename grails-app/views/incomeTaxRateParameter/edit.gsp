
<%@ page import="com.innovated.iris.domain.lookup.IncomeTaxRateParameter" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <title><g:message code="incomeTaxRateParameter.edit" default="Edit IncomeTaxRateParameter" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLinkTo(dir: '')}"><g:message code="home" default="Home" /></a></span>
            <span class="menuButton"><g:link class="list" action="list"><g:message code="incomeTaxRateParameter.list" default="IncomeTaxRateParameter List" /></g:link></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="incomeTaxRateParameter.new" default="New IncomeTaxRateParameter" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="incomeTaxRateParameter.edit" default="Edit IncomeTaxRateParameter" /></h1>
            <g:if test="${flash.message}">
            <div class="message"><g:message code="${flash.message}" args="${flash.args}" default="${flash.defaultMessage}" /></div>
            </g:if>
            <g:hasErrors bean="${incomeTaxRateParameterInstance}">
            <div class="errors">
                <g:renderErrors bean="${incomeTaxRateParameterInstance}" as="list" />
            </div>
            </g:hasErrors>
            <g:form method="post" >
                <g:hiddenField name="id" value="${incomeTaxRateParameterInstance?.id}" />
                <g:hiddenField name="version" value="${incomeTaxRateParameterInstance?.version}" />
                <div class="dialog">
                    <table>
                        <tbody>
                        	<tr class="prop">
                                <td valign="top" class="name">
                                    <label for="dateFrom"><g:message code="incomeTaxRateParameter.dateFrom" default="Date From" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: incomeTaxRateParameterInstance, field: 'dateFrom', 'errors')}">
                                    <g:datePicker name="dateFrom" value="${incomeTaxRateParameterInstance?.dateFrom}" precision="day" />
                                </td>
                            </tr>                            
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="lowerValue"><g:message code="incomeTaxRateParameter.lowerValue" default="Lower Value" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: incomeTaxRateParameterInstance, field: 'lowerValue', 'errors')}">
                                    <g:textField name="lowerValue" value="${fieldValue(bean: incomeTaxRateParameterInstance, field: 'lowerValue')}" />
                                </td>
                            </tr>
                        	<tr class="prop">
                                <td valign="top" class="name">
                                    <label for="upperValue"><g:message code="incomeTaxRateParameter.upperValue" default="Upper Value" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: incomeTaxRateParameterInstance, field: 'upperValue', 'errors')}">
                                    <g:textField name="upperValue" value="${fieldValue(bean: incomeTaxRateParameterInstance, field: 'upperValue')}" />
                                </td>
                            </tr>
                        	<tr class="prop">
                                <td valign="top" class="name">
                                    <label for="amount"><g:message code="incomeTaxRateParameter.amount" default="Amount" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: incomeTaxRateParameterInstance, field: 'amount', 'errors')}">
                                    <g:textField name="amount" value="${fieldValue(bean: incomeTaxRateParameterInstance, field: 'amount')}" />
                                </td>
                            </tr>
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="rate"><g:message code="incomeTaxRateParameter.rate" default="Rate" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: incomeTaxRateParameterInstance, field: 'rate', 'errors')}">
                                    <g:textField name="rate" value="${fieldValue(bean: incomeTaxRateParameterInstance, field: 'rate')}" />
                                </td>
                            </tr>
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="highestRate"><g:message code="incomeTaxRateParameter.highestRate" default="Highest Rate" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: incomeTaxRateParameterInstance, field: 'highestRate', 'errors')}">
                                    <g:checkBox name="highestRate" value="${incomeTaxRateParameterInstance?.highestRate}" />
                                </td>
                            </tr>
                        <!-- 
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="lastUpdated"><g:message code="incomeTaxRateParameter.lastUpdated" default="Last Updated" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: incomeTaxRateParameterInstance, field: 'lastUpdated', 'errors')}">
                                    <g:datePicker name="lastUpdated" value="${incomeTaxRateParameterInstance?.lastUpdated}"  />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="dateCreated"><g:message code="incomeTaxRateParameter.dateCreated" default="Date Created" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: incomeTaxRateParameterInstance, field: 'dateCreated', 'errors')}">
                                    <g:datePicker name="dateCreated" value="${incomeTaxRateParameterInstance?.dateCreated}"  />
                                </td>
                            </tr>
                         -->
                        </tbody>
                    </table>
                </div>
                <div class="buttons">
                    <span class="button"><g:actionSubmit class="save" action="update" value="${message(code: 'update', 'default': 'Update')}" /></span>
                    <span class="button"><g:actionSubmit class="delete" action="delete" value="${message(code: 'delete', 'default': 'Delete')}" onclick="return confirm('${message(code: 'delete.confirm', 'default': 'Are you sure?')}');" /></span>
                </div>
            </g:form>
        </div>
    </body>
</html>
