
<%@ page import="com.innovated.iris.domain.lookup.IncomeTaxRateParameter" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <title><g:message code="incomeTaxRateParameter.show" default="Show IncomeTaxRateParameter" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLinkTo(dir: '')}"><g:message code="home" default="Home" /></a></span>
            <span class="menuButton"><g:link class="list" action="list"><g:message code="incomeTaxRateParameter.list" default="IncomeTaxRateParameter List" /></g:link></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="incomeTaxRateParameter.new" default="New IncomeTaxRateParameter" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="incomeTaxRateParameter.show" default="Show IncomeTaxRateParameter" /></h1>
            <g:if test="${flash.message}">
            <div class="message"><g:message code="${flash.message}" args="${flash.args}" default="${flash.defaultMessage}" /></div>
            </g:if>
            <g:form>
                <g:hiddenField name="id" value="${incomeTaxRateParameterInstance?.id}" />
                <div class="dialog">
                    <table>
                        <tbody>
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="incomeTaxRateParameter.id" default="Id" />:</td>
                                <td valign="top" class="value">${fieldValue(bean: incomeTaxRateParameterInstance, field: "id")}</td>
                            </tr>
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="incomeTaxRateParameter.dateFrom" default="Date From" />:</td>
                                <td valign="top" class="value"><g:formatDate format="d MMM yyyy" date="${incomeTaxRateParameterInstance?.dateFrom}" /></td>
                            </tr>
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="incomeTaxRateParameter.lowerValue" default="Lower Value" />:</td>
                                <td valign="top" class="value">${formatNumber(number:incomeTaxRateParameterInstance.lowerValue, type:'currency', minFractionDigits:0, maxFractionDigits:0)}</td>
                            </tr>
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="incomeTaxRateParameter.upperValue" default="Upper Value" />:</td>
                                <td valign="top" class="value">${formatNumber(number:incomeTaxRateParameterInstance.upperValue, type:'currency', minFractionDigits:0, maxFractionDigits:0)}</td>
                            </tr>
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="incomeTaxRateParameter.amount" default="Amount" />:</td>
                                <td valign="top" class="value">${formatNumber(number:incomeTaxRateParameterInstance.amount, type:'currency', minFractionDigits:0, maxFractionDigits:0)}</td>
                            </tr>
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="incomeTaxRateParameter.rate" default="Rate" />:</td>
                                <td valign="top" class="value">${formatNumber(number:incomeTaxRateParameterInstance.rate, type:'percent', minFractionDigits:0, maxFractionDigits:0)}</td>
                            </tr>
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="incomeTaxRateParameter.highestRate" default="Highest Rate" />:</td>
                                <td valign="top" class="value"><g:formatBoolean boolean="${incomeTaxRateParameterInstance?.highestRate}" /></td>
                            </tr>

                            <!-- 
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="incomeTaxRateParameter.lastUpdated" default="Last Updated" />:</td>
                                
                                <td valign="top" class="value"><g:formatDate date="${incomeTaxRateParameterInstance?.lastUpdated}" /></td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="incomeTaxRateParameter.dateCreated" default="Date Created" />:</td>
                                
                                <td valign="top" class="value"><g:formatDate date="${incomeTaxRateParameterInstance?.dateCreated}" /></td>
                                
                            </tr>
                            
                             -->
                            
                        </tbody>
                    </table>
                </div>
                <div class="buttons">
                    <span class="button"><g:actionSubmit class="edit" action="edit" value="${message(code: 'edit', 'default': 'Edit')}" /></span>
                    <span class="button"><g:actionSubmit class="delete" action="delete" value="${message(code: 'delete', 'default': 'Delete')}" onclick="return confirm('${message(code: 'delete.confirm', 'default': 'Are you sure?')}');" /></span>
                </div>
            </g:form>
        </div>
    </body>
</html>
