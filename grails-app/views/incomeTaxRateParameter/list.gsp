
<%@ page import="com.innovated.iris.domain.lookup.IncomeTaxRateParameter" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <title><g:message code="incomeTaxRateParameter.list" default="IncomeTaxRateParameter List" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLinkTo(dir: '')}"><g:message code="home" default="Home" /></a></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="incomeTaxRateParameter.new" default="New IncomeTaxRateParameter" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="incomeTaxRateParameter.list" default="IncomeTaxRateParameter List" /></h1>
            <g:if test="${flash.message}">
            <div class="message"><g:message code="${flash.message}" args="${flash.args}" default="${flash.defaultMessage}" /></div>
            </g:if>
            <div class="list">
                <table>
                    <thead>
                        <tr>
	                   	    <g:sortableColumn property="id" title="ID" titleKey="incomeTaxRateParameter.id" />
	                   	    <g:sortableColumn property="dateFrom" title="Date From" titleKey="incomeTaxRateParameter.dateFrom" />
	                   	    <g:sortableColumn class="numeric" property="lowerValue" title="Lower Value" titleKey="incomeTaxRateParameter.lowerValue" />
	                   	    <g:sortableColumn class="numeric" property="upperValue" title="Upper Value" titleKey="incomeTaxRateParameter.upperValue" />
	                   	    <g:sortableColumn class="numeric" property="amount" title="Tax Amount" titleKey="incomeTaxRateParameter.amount" />
	                   	    <g:sortableColumn class="numeric" property="rate" title="Tax Rate" titleKey="incomeTaxRateParameter.rate" />
	                   	    <g:sortableColumn property="highestRate" title="Highest Rate" titleKey="incomeTaxRateParameter.highestRate" />
                        </tr>
                    </thead>
                    <tbody>
                    <g:each in="${incomeTaxRateParameterInstanceList}" status="i" var="incomeTaxRateParameterInstance">
                        <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
                            <td><g:link action="show" id="${incomeTaxRateParameterInstance.id}">${fieldValue(bean: incomeTaxRateParameterInstance, field: "id")}</g:link></td>
                            <td><g:formatDate format="d MMM yyyy" date="${incomeTaxRateParameterInstance.dateFrom}" /></td>
                            <td class="numeric">${formatNumber(number:incomeTaxRateParameterInstance.lowerValue, type:'currency', minFractionDigits:0, maxFractionDigits:0)}</td>
                            <td class="numeric">${formatNumber(number:incomeTaxRateParameterInstance.upperValue, type:'currency', minFractionDigits:0, maxFractionDigits:0)}</td>
                            <td class="numeric">${formatNumber(number:incomeTaxRateParameterInstance.amount, type:'currency', minFractionDigits:0, maxFractionDigits:0)}</td>
                            <td class="numeric">${formatNumber(number:incomeTaxRateParameterInstance.rate, type:'percent', minFractionDigits:0, maxFractionDigits:0)}</td>
                            <td><g:formatBoolean boolean="${incomeTaxRateParameterInstance.highestRate}" /></td>
                        </tr>
                    </g:each>
                    </tbody>
                </table>
            </div>
            <div class="paginateButtons">
                <g:paginate total="${incomeTaxRateParameterInstanceTotal}" />
            </div>
        </div>
    </body>
</html>
