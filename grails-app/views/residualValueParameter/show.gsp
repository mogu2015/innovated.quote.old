
<%@ page import="com.innovated.iris.domain.lookup.ResidualValueParameter" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <title><g:message code="residualValueParameter.show" default="Show ResidualValueParameter" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLinkTo(dir: '')}"><g:message code="home" default="Home" /></a></span>
            <span class="menuButton"><g:link class="list" action="list"><g:message code="residualValueParameter.list" default="ResidualValueParameter List" /></g:link></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="residualValueParameter.new" default="New ResidualValueParameter" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="residualValueParameter.show" default="Show ResidualValueParameter" /></h1>
            <g:if test="${flash.message}">
            <div class="message"><g:message code="${flash.message}" args="${flash.args}" default="${flash.defaultMessage}" /></div>
            </g:if>
            <g:form>
                <g:hiddenField name="id" value="${residualValueParameterInstance?.id}" />
                <div class="dialog">
                    <table>
                        <tbody>
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="residualValueParameter.id" default="ID" />:</td>
                                <td valign="top" class="value">${fieldValue(bean: residualValueParameterInstance, field: "id")}</td>
                            </tr>
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="residualValueParameter.dateFrom" default="Date From" />:</td>
                                <td valign="top" class="value"><g:formatDate format="d MMM yyyy" date="${residualValueParameterInstance?.dateFrom}"/></td>
                            </tr>
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="residualValueParameter.customerCode" default="Customer Code" />:</td>
                                <td valign="top" class="value">${fieldValue(bean: residualValueParameterInstance, field: "customerCode")}</td>
                            </tr>
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="residualValueParameter.term" default="Lease Term" />:</td>
                                <td valign="top" class="value">${fieldValue(bean: residualValueParameterInstance, field: "term")} months</td>
                            </tr>
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="residualValueParameter.minHiKm" default="Minimum (High KM)" />:</td>
                                <td valign="top" class="value">${formatNumber(number:residualValueParameterInstance.minHiKm, type:'percent', minFractionDigits:2, maxFractionDigits:8)}</td>
                            </tr>
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="residualValueParameter.min" default="Minimum" />:</td>
                                <td valign="top" class="value">${formatNumber(number:residualValueParameterInstance.min, type:'percent', minFractionDigits:2, maxFractionDigits:8)}</td>
                            </tr>
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="residualValueParameter.max" default="Maximum" />:</td>
                                <td valign="top" class="value">${formatNumber(number:residualValueParameterInstance.max, type:'percent', minFractionDigits:2, maxFractionDigits:8)}</td>
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="residualValueParameter.hiKmThreshold" default="High KM Threshold" />:</td>
                                <td valign="top" class="value">${fieldValue(bean: residualValueParameterInstance, field: "hiKmThreshold")} km</td>
                            </tr>
                            <!--
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="residualValueParameter.lastUpdated" default="Last Updated" />:</td>
                                <td valign="top" class="value"><g:formatDate date="${residualValueParameterInstance?.lastUpdated}" /></td>
                            </tr>
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="residualValueParameter.dateCreated" default="Date Created" />:</td>
                                <td valign="top" class="value"><g:formatDate date="${residualValueParameterInstance?.dateCreated}" /></td>
                            </tr>
                            -->
                        </tbody>
                    </table>
                </div>
                <div class="buttons">
                    <span class="button"><g:actionSubmit class="edit" action="edit" value="${message(code: 'edit', 'default': 'Edit')}" /></span>
                    <span class="button"><g:actionSubmit class="delete" action="delete" value="${message(code: 'delete', 'default': 'Delete')}" onclick="return confirm('${message(code: 'delete.confirm', 'default': 'Are you sure?')}');" /></span>
                </div>
            </g:form>
        </div>
    </body>
</html>
