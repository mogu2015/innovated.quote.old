
<%@ page import="com.innovated.iris.domain.lookup.ResidualValueParameter" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <title><g:message code="residualValueParameter.create" default="Create ResidualValueParameter" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLinkTo(dir: '')}"><g:message code="home" default="Home" /></a></span>
            <span class="menuButton"><g:link class="list" action="list"><g:message code="residualValueParameter.list" default="ResidualValueParameter List" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="residualValueParameter.create" default="Create ResidualValueParameter" /></h1>
            <g:if test="${flash.message}">
            <div class="message"><g:message code="${flash.message}" args="${flash.args}" default="${flash.defaultMessage}" /></div>
            </g:if>
            <g:hasErrors bean="${residualValueParameterInstance}">
            <div class="errors">
                <g:renderErrors bean="${residualValueParameterInstance}" as="list" />
            </div>
            </g:hasErrors>
            <g:form action="save" method="post" >
                <div class="dialog">
                    <table>
                        <tbody>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="dateFrom"><g:message code="residualValueParameter.dateFrom" default="Date From" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: residualValueParameterInstance, field: 'dateFrom', 'errors')}">
                                    <g:datePicker name="dateFrom" value="${residualValueParameterInstance?.dateFrom}" precision="day" />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="customerCode"><g:message code="residualValueParameter.customerCode" default="Customer Code" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: residualValueParameterInstance, field: 'customerCode', 'errors')}">
                                    <g:select name="term" from="${customerCodes}" value="${residualValueParameterInstance?.customerCode}"  />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="term"><g:message code="residualValueParameter.term" default="Term" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: residualValueParameterInstance, field: 'term', 'errors')}">
                                    <g:select name="term" from="${[6,12,18,24,30,36,42,48,54,60]}" value="${residualValueParameterInstance?.term}"  />
                                </td>
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="minHiKm"><g:message code="residualValueParameter.minHiKm" default="Min (High KM)" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: residualValueParameterInstance, field: 'minHiKm', 'errors')}">
                                    <g:textField name="minHiKm" class="numeric" value="${formatNumber(number:residualValueParameterInstance.minHiKm, type:'number', minFractionDigits:2, maxFractionDigits:8)}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="min"><g:message code="residualValueParameter.min" default="Min" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: residualValueParameterInstance, field: 'min', 'errors')}">
                                    <g:textField name="min" class="numeric" value="${formatNumber(number:residualValueParameterInstance.min, type:'number', minFractionDigits:2, maxFractionDigits:8)}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="max"><g:message code="residualValueParameter.max" default="Max" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: residualValueParameterInstance, field: 'max', 'errors')}">
                                    <g:textField name="max" class="numeric" value="${formatNumber(number:residualValueParameterInstance.max, type:'number', minFractionDigits:2, maxFractionDigits:8)}" />
                                </td>
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="hiKmThreshold"><g:message code="residualValueParameter.hiKmThreshold" default="High KM Threshold" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: residualValueParameterInstance, field: 'hiKmThreshold', 'errors')}">
                                    <g:textField class="numeric" name="hiKmThreshold" value="${formatNumber(number:residualValueParameterInstance.hiKmThreshold, type:'number')}" />
                                </td>
                            </tr>
                        <!--
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="lastUpdated"><g:message code="residualValueParameter.lastUpdated" default="Last Updated" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: residualValueParameterInstance, field: 'lastUpdated', 'errors')}">
                                    <g:datePicker name="lastUpdated" value="${residualValueParameterInstance?.lastUpdated}"  />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="dateCreated"><g:message code="residualValueParameter.dateCreated" default="Date Created" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: residualValueParameterInstance, field: 'dateCreated', 'errors')}">
                                    <g:datePicker name="dateCreated" value="${residualValueParameterInstance?.dateCreated}"  />

                                </td>
                            </tr>
                        -->
                        </tbody>
                    </table>
                </div>
                <div class="buttons">
                    <span class="button"><g:submitButton name="create" class="save" value="${message(code: 'create', 'default': 'Create')}" /></span>
                </div>
            </g:form>
        </div>
    </body>
</html>
