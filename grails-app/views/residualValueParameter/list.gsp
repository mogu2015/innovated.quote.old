
<%@ page import="com.innovated.iris.domain.lookup.ResidualValueParameter" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <title><g:message code="residualValueParameter.list" default="ResidualValueParameter List" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLinkTo(dir: '')}"><g:message code="home" default="Home" /></a></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="residualValueParameter.new" default="New ResidualValueParameter" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="residualValueParameter.list" default="ResidualValueParameter List" /></h1>
            <g:if test="${flash.message}">
            <div class="message"><g:message code="${flash.message}" args="${flash.args}" default="${flash.defaultMessage}" /></div>
            </g:if>
            <div class="list">
                <table>
                    <thead>
                        <tr>
                   	    <g:sortableColumn property="id" title="ID" titleKey="residualValueParameter.id" />
                   	    <g:sortableColumn property="dateFrom" title="Date From" titleKey="residualValueParameter.dateFrom" />
                   	    <g:sortableColumn property="customerCode" title="Customer Code" titleKey="residualValueParameter.customerCode" />
                   	    <g:sortableColumn property="term" title="Term" titleKey="residualValueParameter.term" />
                   	    <g:sortableColumn property="minHighKm" title="Min (High KM)" titleKey="residualValueParameter.minHiKm" />
                   	    <g:sortableColumn property="min" title="Min" titleKey="residualValueParameter.min" />
                   	    <g:sortableColumn property="max" title="Max" titleKey="residualValueParameter.max" />
                   	    <g:sortableColumn property="hiKmThreshold" title="High KM Threshold" titleKey="residualValueParameter.hiKmThreshold" />
                        </tr>
                    </thead>
                    <tbody>
                    <g:each in="${residualValueParameterInstanceList}" status="i" var="residualValueParameterInstance">
                        <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
                            <td><g:link action="show" id="${residualValueParameterInstance.id}">${fieldValue(bean: residualValueParameterInstance, field: "id")}</g:link></td>
                            <td><g:formatDate format="d MMM yyyy" date="${residualValueParameterInstance?.dateFrom}"/></td>
                            <td>${fieldValue(bean: residualValueParameterInstance, field: "customerCode")}</td>
                            <td class="numeric">${fieldValue(bean: residualValueParameterInstance, field: "term")}</td>
                            <td class="numeric">${formatNumber(number:residualValueParameterInstance.minHiKm, type:'percent', minFractionDigits:2, maxFractionDigits:8)}</td>
                            <td class="numeric">${formatNumber(number:residualValueParameterInstance.min, type:'percent', minFractionDigits:2, maxFractionDigits:8)}</td>
                            <td class="numeric">${formatNumber(number:residualValueParameterInstance.max, type:'percent', minFractionDigits:2, maxFractionDigits:8)}</td>
                            <td class="numeric">${fieldValue(bean: residualValueParameterInstance, field: "hiKmThreshold")} km</td>
                        </tr>
                    </g:each>
                    </tbody>
                </table>
            </div>
            <div class="paginateButtons">
                <g:paginate total="${residualValueParameterInstanceTotal}" />
            </div>
        </div>
    </body>
</html>
