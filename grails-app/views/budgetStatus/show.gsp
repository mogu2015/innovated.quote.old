
<%@ page import="com.innovated.iris.domain.enums.BudgetStatus" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <title><g:message code="budgetStatus.show" default="Show BudgetStatus" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLinkTo(dir: '')}"><g:message code="home" default="Home" /></a></span>
            <span class="menuButton"><g:link class="list" action="list"><g:message code="budgetStatus.list" default="BudgetStatus List" /></g:link></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="budgetStatus.new" default="New BudgetStatus" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="budgetStatus.show" default="Show BudgetStatus" /></h1>
            <g:if test="${flash.message}">
            <div class="message"><g:message code="${flash.message}" args="${flash.args}" default="${flash.defaultMessage}" /></div>
            </g:if>
            <g:form>
                <g:hiddenField name="id" value="${budgetStatusInstance?.id}" />
                <div class="dialog">
                    <table>
                        <tbody>
                        
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="budgetStatus.id" default="Id" />:</td>
                                
                                <td valign="top" class="value">${fieldValue(bean: budgetStatusInstance, field: "id")}</td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="budgetStatus.name" default="Name" />:</td>
                                
                                <td valign="top" class="value">${fieldValue(bean: budgetStatusInstance, field: "name")}</td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="budgetStatus.statusCode" default="Status Code" />:</td>
                                
                                <td valign="top" class="value">${fieldValue(bean: budgetStatusInstance, field: "statusCode")}</td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="budgetStatus.visible" default="Visible" />:</td>
                                
                                <td valign="top" class="value"><g:formatBoolean boolean="${budgetStatusInstance?.visible}" /></td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="budgetStatus.enabled" default="Enabled" />:</td>
                                
                                <td valign="top" class="value"><g:formatBoolean boolean="${budgetStatusInstance?.enabled}" /></td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="budgetStatus.dateCreated" default="Date Created" />:</td>
                                
                                <td valign="top" class="value"><g:formatDate date="${budgetStatusInstance?.dateCreated}" /></td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="budgetStatus.lastUpdated" default="Last Updated" />:</td>
                                
                                <td valign="top" class="value"><g:formatDate date="${budgetStatusInstance?.lastUpdated}" /></td>
                                
                            </tr>
                            
                        </tbody>
                    </table>
                </div>
                <div class="buttons">
                    <span class="button"><g:actionSubmit class="edit" action="edit" value="${message(code: 'edit', 'default': 'Edit')}" /></span>
                    <span class="button"><g:actionSubmit class="delete" action="delete" value="${message(code: 'delete', 'default': 'Delete')}" onclick="return confirm('${message(code: 'delete.confirm', 'default': 'Are you sure?')}');" /></span>
                </div>
            </g:form>
        </div>
    </body>
</html>
