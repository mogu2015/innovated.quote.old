
<%@ page import="com.innovated.iris.domain.enums.BudgetStatus" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <title><g:message code="budgetStatus.edit" default="Edit BudgetStatus" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLinkTo(dir: '')}"><g:message code="home" default="Home" /></a></span>
            <span class="menuButton"><g:link class="list" action="list"><g:message code="budgetStatus.list" default="BudgetStatus List" /></g:link></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="budgetStatus.new" default="New BudgetStatus" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="budgetStatus.edit" default="Edit BudgetStatus" /></h1>
            <g:if test="${flash.message}">
            <div class="message"><g:message code="${flash.message}" args="${flash.args}" default="${flash.defaultMessage}" /></div>
            </g:if>
            <g:hasErrors bean="${budgetStatusInstance}">
            <div class="errors">
                <g:renderErrors bean="${budgetStatusInstance}" as="list" />
            </div>
            </g:hasErrors>
            <g:form method="post" >
                <g:hiddenField name="id" value="${budgetStatusInstance?.id}" />
                <g:hiddenField name="version" value="${budgetStatusInstance?.version}" />
                <div class="dialog">
                    <table>
                        <tbody>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="name"><g:message code="budgetStatus.name" default="Name" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: budgetStatusInstance, field: 'name', 'errors')}">
                                    <g:textField name="name" value="${fieldValue(bean: budgetStatusInstance, field: 'name')}" />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="statusCode"><g:message code="budgetStatus.statusCode" default="Status Code" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: budgetStatusInstance, field: 'statusCode', 'errors')}">
                                    <g:textField name="statusCode" value="${fieldValue(bean: budgetStatusInstance, field: 'statusCode')}" />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="visible"><g:message code="budgetStatus.visible" default="Visible" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: budgetStatusInstance, field: 'visible', 'errors')}">
                                    <g:checkBox name="visible" value="${budgetStatusInstance?.visible}" />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="enabled"><g:message code="budgetStatus.enabled" default="Enabled" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: budgetStatusInstance, field: 'enabled', 'errors')}">
                                    <g:checkBox name="enabled" value="${budgetStatusInstance?.enabled}" />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="dateCreated"><g:message code="budgetStatus.dateCreated" default="Date Created" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: budgetStatusInstance, field: 'dateCreated', 'errors')}">
                                    <g:datePicker name="dateCreated" value="${budgetStatusInstance?.dateCreated}"  />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="lastUpdated"><g:message code="budgetStatus.lastUpdated" default="Last Updated" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: budgetStatusInstance, field: 'lastUpdated', 'errors')}">
                                    <g:datePicker name="lastUpdated" value="${budgetStatusInstance?.lastUpdated}"  />

                                </td>
                            </tr>
                        
                        </tbody>
                    </table>
                </div>
                <div class="buttons">
                    <span class="button"><g:actionSubmit class="save" action="update" value="${message(code: 'update', 'default': 'Update')}" /></span>
                    <span class="button"><g:actionSubmit class="delete" action="delete" value="${message(code: 'delete', 'default': 'Delete')}" onclick="return confirm('${message(code: 'delete.confirm', 'default': 'Are you sure?')}');" /></span>
                </div>
            </g:form>
        </div>
    </body>
</html>
