
<%@ page import="com.innovated.iris.domain.enums.BudgetStatus" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <title><g:message code="budgetStatus.list" default="BudgetStatus List" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLinkTo(dir: '')}"><g:message code="home" default="Home" /></a></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="budgetStatus.new" default="New BudgetStatus" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="budgetStatus.list" default="BudgetStatus List" /></h1>
            <g:if test="${flash.message}">
            <div class="message"><g:message code="${flash.message}" args="${flash.args}" default="${flash.defaultMessage}" /></div>
            </g:if>
            <div class="list">
                <table>
                    <thead>
                        <tr>
                        
                   	    <g:sortableColumn property="id" title="Id" titleKey="budgetStatus.id" />
                        
                   	    <g:sortableColumn property="name" title="Name" titleKey="budgetStatus.name" />
                        
                   	    <g:sortableColumn property="statusCode" title="Status Code" titleKey="budgetStatus.statusCode" />
                        
                   	    <g:sortableColumn property="visible" title="Visible" titleKey="budgetStatus.visible" />
                        
                   	    <g:sortableColumn property="enabled" title="Enabled" titleKey="budgetStatus.enabled" />
                        
                   	    <g:sortableColumn property="dateCreated" title="Date Created" titleKey="budgetStatus.dateCreated" />
                        
                        </tr>
                    </thead>
                    <tbody>
                    <g:each in="${budgetStatusInstanceList}" status="i" var="budgetStatusInstance">
                        <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
                        
                            <td><g:link action="show" id="${budgetStatusInstance.id}">${fieldValue(bean: budgetStatusInstance, field: "id")}</g:link></td>
                        
                            <td>${fieldValue(bean: budgetStatusInstance, field: "name")}</td>
                        
                            <td>${fieldValue(bean: budgetStatusInstance, field: "statusCode")}</td>
                        
                            <td><g:formatBoolean boolean="${budgetStatusInstance.visible}" /></td>
                        
                            <td><g:formatBoolean boolean="${budgetStatusInstance.enabled}" /></td>
                        
                            <td><g:formatDate date="${budgetStatusInstance.dateCreated}" /></td>
                        
                        </tr>
                    </g:each>
                    </tbody>
                </table>
            </div>
            <div class="paginateButtons">
                <g:paginate total="${budgetStatusInstanceTotal}" />
            </div>
        </div>
    </body>
</html>
