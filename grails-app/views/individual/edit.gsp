
<%@ page import="com.innovated.iris.domain.Individual" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <title><g:message code="individual.edit" default="Edit Individual" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLinkTo(dir: '')}"><g:message code="home" default="Home" /></a></span>
            <span class="menuButton"><g:link class="list" action="list"><g:message code="individual.list" default="Individual List" /></g:link></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="individual.new" default="New Individual" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="individual.edit" default="Edit Individual" /></h1>
            <g:if test="${flash.message}">
            	<div class="message"><g:message code="${flash.message}" args="${flash.args}" default="${flash.defaultMessage}" /></div>
            </g:if>
            <g:hasErrors bean="${individualInstance}">
				<div class="errors">
					<g:renderErrors bean="${individualInstance}" as="list" />
				</div>
            </g:hasErrors>
            <g:form method="post" >
                <g:hiddenField name="id" value="${individualInstance?.id}" />
                <g:hiddenField name="version" value="${individualInstance?.version}" />
                <div class="dialog">
                    <fieldset>
                        <legend><g:message code="individual.edit.legend" default="Update Individual Details"/></legend>
                        
                        <div class="prop mandatory ${hasErrors(bean: individualInstance, field: 'name', 'error')}">
                            <label for="name">
                                <g:message code="individual.name" default="Name" />
                                <span class="indicator">*</span>
                            </label>
                            <input type="text" id="name" name="name" value="${fieldValue(bean:individualInstance,field:'name')}"/>
                        </div>
                        
                        <div class="prop mandatory ${hasErrors(bean: individualInstance, field: 'addresses', 'error')}">
                            <label for="addresses">
                                <g:message code="individual.addresses" default="Addresses" />
                                <span class="indicator">*</span>
                            </label>
                            
<ul>
<g:each var="a" in="${individualInstance?.addresses?}">
    <li><g:link controller="address" action="show" id="${a.id}">${a?.encodeAsHTML()}</g:link></li>
</g:each>
</ul>
<g:link controller="address" params="['entity.id':individualInstance?.id]" action="create">Add Address</g:link>

                        </div>
                        
                        <div class="prop mandatory ${hasErrors(bean: individualInstance, field: 'phones', 'error')}">
                            <label for="phones">
                                <g:message code="individual.phones" default="Phones" />
                                <span class="indicator">*</span>
                            </label>
                            
<ul>
<g:each var="p" in="${individualInstance?.phones?}">
    <li><g:link controller="phone" action="show" id="${p.id}">${p?.encodeAsHTML()}</g:link></li>
</g:each>
</ul>
<g:link controller="phone" params="['entity.id':individualInstance?.id]" action="create">Add Phone</g:link>

                        </div>
                        
                        <div class="prop mandatory ${hasErrors(bean: individualInstance, field: 'emails', 'error')}">
                            <label for="emails">
                                <g:message code="individual.emails" default="Emails" />
                                <span class="indicator">*</span>
                            </label>
                            
<ul>
<g:each var="e" in="${individualInstance?.emails?}">
    <li><g:link controller="email" action="show" id="${e.id}">${e?.encodeAsHTML()}</g:link></li>
</g:each>
</ul>
<g:link controller="email" params="['entity.id':individualInstance?.id]" action="create">Add Email</g:link>

                        </div>
                        
                        <div class="prop mandatory ${hasErrors(bean: individualInstance, field: 'firstName', 'error')}">
                            <label for="firstName">
                                <g:message code="individual.firstName" default="First Name" />
                                <span class="indicator">*</span>
                            </label>
                            <input type="text" id="firstName" name="firstName" value="${fieldValue(bean:individualInstance,field:'firstName')}"/>
                        </div>
                        
                        <div class="prop ${hasErrors(bean: individualInstance, field: 'middleName', 'error')}">
                            <label for="middleName">
                                <g:message code="individual.middleName" default="Middle Name" />
                                
                            </label>
                            <input type="text" id="middleName" name="middleName" value="${fieldValue(bean:individualInstance,field:'middleName')}"/>
                        </div>
                        
                        <div class="prop mandatory ${hasErrors(bean: individualInstance, field: 'lastName', 'error')}">
                            <label for="lastName">
                                <g:message code="individual.lastName" default="Last Name" />
                                <span class="indicator">*</span>
                            </label>
                            <input type="text" id="lastName" name="lastName" value="${fieldValue(bean:individualInstance,field:'lastName')}"/>
                        </div>
                        
                        <div class="prop mandatory ${hasErrors(bean: individualInstance, field: 'preferredName', 'error')}">
                            <label for="preferredName">
                                <g:message code="individual.preferredName" default="Preferred Name" />
                                <span class="indicator">*</span>
                            </label>
                            <input type="text" id="preferredName" name="preferredName" value="${fieldValue(bean:individualInstance,field:'preferredName')}"/>
                        </div>
                        
                        <div class="prop ${hasErrors(bean: individualInstance, field: 'dateOfBirth', 'error')}">
                            <label for="dateOfBirth">
                                <g:message code="individual.dateOfBirth" default="Date Of Birth" />
                                
                            </label>
                            <g:datePicker name="dateOfBirth" value="${individualInstance?.dateOfBirth}" precision="minute" noSelection="['':'']"></g:datePicker>
                        </div>
                        
                        <div class="prop mandatory ${hasErrors(bean: individualInstance, field: 'dateCreated', 'error')}">
                            <label for="dateCreated">
                                <g:message code="individual.dateCreated" default="Date Created" />
                                <span class="indicator">*</span>
                            </label>
                            <g:datePicker name="dateCreated" value="${individualInstance?.dateCreated}" precision="minute" ></g:datePicker>
                        </div>
                        
                        <div class="prop mandatory ${hasErrors(bean: individualInstance, field: 'lastUpdated', 'error')}">
                            <label for="lastUpdated">
                                <g:message code="individual.lastUpdated" default="Last Updated" />
                                <span class="indicator">*</span>
                            </label>
                            <g:datePicker name="lastUpdated" value="${individualInstance?.lastUpdated}" precision="minute" ></g:datePicker>
                        </div>
                        
                    </fieldset>
                </div>
                <div class="buttons">
                    <span class="button"><g:actionSubmit class="save" action="update" value="${message(code: 'update', 'default': 'Update')}" /></span>
                    <span class="button"><g:actionSubmit class="delete" action="delete" value="${message(code: 'delete', 'default': 'Delete')}" onclick="return confirm('${message(code: 'delete.confirm', 'default': 'Are you sure?')}');" /></span>
                </div>
            </g:form>
        </div>
    </body>
</html>

