
<%@ page import="com.innovated.iris.domain.Individual" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <title><g:message code="individual.create" default="Create Individual" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLinkTo(dir: '')}"><g:message code="home" default="Home" /></a></span>
            <span class="menuButton"><g:link class="list" action="list"><g:message code="individual.list" default="Individual List" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="individual.create" default="Create Individual" /></h1>
            <g:if test="${flash.message}">
            	<div class="message"><g:message code="${flash.message}" args="${flash.args}" default="${flash.defaultMessage}" /></div>
            </g:if>
            <g:hasErrors bean="${individualInstance}">
				<div class="errors">
					<g:renderErrors bean="${individualInstance}" as="list" />
				</div>
            </g:hasErrors>
            <g:form action="save" method="post" >
                <div class="dialog">
                    <fieldset>
                        <legend><g:message code="individual.create.legend" default="Enter Individual Details"/></legend>
                        
                            <div class="prop mandatory ${hasErrors(bean: individualInstance, field: 'name', 'error')}">
                                <label for="name">
                                    <g:message code="individual.name" default="Name" />
                                    <span class="indicator">*</span>
                                </label>
                                <input type="text" id="name" name="name" value="${fieldValue(bean:individualInstance,field:'name')}"/>
                            </div>
                        
                            <div class="prop mandatory ${hasErrors(bean: individualInstance, field: 'firstName', 'error')}">
                                <label for="firstName">
                                    <g:message code="individual.firstName" default="First Name" />
                                    <span class="indicator">*</span>
                                </label>
                                <input type="text" id="firstName" name="firstName" value="${fieldValue(bean:individualInstance,field:'firstName')}"/>
                            </div>
                        
                            <div class="prop ${hasErrors(bean: individualInstance, field: 'middleName', 'error')}">
                                <label for="middleName">
                                    <g:message code="individual.middleName" default="Middle Name" />
                                    
                                </label>
                                <input type="text" id="middleName" name="middleName" value="${fieldValue(bean:individualInstance,field:'middleName')}"/>
                            </div>
                        
                            <div class="prop mandatory ${hasErrors(bean: individualInstance, field: 'lastName', 'error')}">
                                <label for="lastName">
                                    <g:message code="individual.lastName" default="Last Name" />
                                    <span class="indicator">*</span>
                                </label>
                                <input type="text" id="lastName" name="lastName" value="${fieldValue(bean:individualInstance,field:'lastName')}"/>
                            </div>
                        
                            <div class="prop mandatory ${hasErrors(bean: individualInstance, field: 'preferredName', 'error')}">
                                <label for="preferredName">
                                    <g:message code="individual.preferredName" default="Preferred Name" />
                                    <span class="indicator">*</span>
                                </label>
                                <input type="text" id="preferredName" name="preferredName" value="${fieldValue(bean:individualInstance,field:'preferredName')}"/>
                            </div>
                        <!-- 
                            <div class="prop ${hasErrors(bean: individualInstance, field: 'dateOfBirth', 'error')}">
                                <label for="dateOfBirth">
                                    <g:message code="individual.dateOfBirth" default="Date Of Birth" />
                                </label>
                                <g:datePicker name="dateOfBirth" value="${individualInstance?.dateOfBirth}" precision="day" noSelection="['':'']" default="none"></g:datePicker>
                            </div>
                             -->
                        <!--
                            <div class="prop mandatory ${hasErrors(bean: individualInstance, field: 'dateCreated', 'error')}">
                                <label for="dateCreated">
                                    <g:message code="individual.dateCreated" default="Date Created" />
                                    <span class="indicator">*</span>
                                </label>
                                <g:datePicker name="dateCreated" value="${individualInstance?.dateCreated}" precision="minute" ></g:datePicker>
                            </div>
                        
                            <div class="prop mandatory ${hasErrors(bean: individualInstance, field: 'lastUpdated', 'error')}">
                                <label for="lastUpdated">
                                    <g:message code="individual.lastUpdated" default="Last Updated" />
                                    <span class="indicator">*</span>
                                </label>
                                <g:datePicker name="lastUpdated" value="${individualInstance?.lastUpdated}" precision="minute" ></g:datePicker>
                            </div>
                        -->
                    </fieldset>
                </div>
                <div class="buttons">
                    <span class="button"><g:submitButton name="create" class="save" value="${message(code: 'create', 'default': 'Create')}" /></span>
                </div>
            </g:form>
        </div>
    </body>
</html>

