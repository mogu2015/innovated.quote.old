
<%@ page import="com.innovated.iris.domain.lookup.VehicleRegistrationParameter" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <title><g:message code="vehicleRegistrationParameter.show" default="Show VehicleRegistrationParameter" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLinkTo(dir: '')}"><g:message code="home" default="Home" /></a></span>
            <span class="menuButton"><g:link class="list" action="list"><g:message code="vehicleRegistrationParameter.list" default="VehicleRegistrationParameter List" /></g:link></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="vehicleRegistrationParameter.new" default="New VehicleRegistrationParameter" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="vehicleRegistrationParameter.show" default="Show VehicleRegistrationParameter" /></h1>
            <g:if test="${flash.message}">
            <div class="message"><g:message code="${flash.message}" args="${flash.args}" default="${flash.defaultMessage}" /></div>
            </g:if>
            <g:form>
                <g:hiddenField name="id" value="${vehicleRegistrationParameterInstance?.id}" />
                <div class="dialog">
                    <table>
                        <tbody>
                        
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="vehicleRegistrationParameter.id" default="Id" />:</td>
                                
                                <td valign="top" class="value">${fieldValue(bean: vehicleRegistrationParameterInstance, field: "id")}</td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="vehicleRegistrationParameter.dateFrom" default="Date From" />:</td>
                                
                                <td valign="top" class="value"><g:formatDate date="${vehicleRegistrationParameterInstance?.dateFrom}" /></td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="vehicleRegistrationParameter.customerCode" default="Customer Code" />:</td>
                                
                                <td valign="top" class="value">${fieldValue(bean: vehicleRegistrationParameterInstance, field: "customerCode")}</td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="vehicleRegistrationParameter.state" default="State" />:</td>
                                
                                <td valign="top" class="value">${fieldValue(bean: vehicleRegistrationParameterInstance, field: "state")}</td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="vehicleRegistrationParameter.regoSupplier" default="Rego Supplier" />:</td>
                                
                                <td valign="top" class="value"><g:link controller="supplier" action="show" id="${vehicleRegistrationParameterInstance?.regoSupplier?.id}">${vehicleRegistrationParameterInstance?.regoSupplier?.encodeAsHTML()}</g:link></td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="vehicleRegistrationParameter.ctpSupplier" default="Ctp Supplier" />:</td>
                                
                                <td valign="top" class="value"><g:link controller="supplier" action="show" id="${vehicleRegistrationParameterInstance?.ctpSupplier?.id}">${vehicleRegistrationParameterInstance?.ctpSupplier?.encodeAsHTML()}</g:link></td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="vehicleRegistrationParameter.regoCostTwelveMths" default="Rego Cost Twelve Mths" />:</td>
                                
                                <td valign="top" class="value">${fieldValue(bean: vehicleRegistrationParameterInstance, field: "regoCostTwelveMths")}</td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="vehicleRegistrationParameter.regoCostSixMths" default="Rego Cost Six Mths" />:</td>
                                
                                <td valign="top" class="value">${fieldValue(bean: vehicleRegistrationParameterInstance, field: "regoCostSixMths")}</td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="vehicleRegistrationParameter.regoIncludesGst" default="Rego Includes Gst" />:</td>
                                
                                <td valign="top" class="value"><g:formatBoolean boolean="${vehicleRegistrationParameterInstance?.regoIncludesGst}" /></td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="vehicleRegistrationParameter.ctpCostTwelveMths" default="Ctp Cost Twelve Mths" />:</td>
                                
                                <td valign="top" class="value">${fieldValue(bean: vehicleRegistrationParameterInstance, field: "ctpCostTwelveMths")}</td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="vehicleRegistrationParameter.ctpCostSixMths" default="Ctp Cost Six Mths" />:</td>
                                
                                <td valign="top" class="value">${fieldValue(bean: vehicleRegistrationParameterInstance, field: "ctpCostSixMths")}</td>
                                
                            </tr>
                            
                        </tbody>
                    </table>
                </div>
                <div class="buttons">
                    <span class="button"><g:actionSubmit class="edit" action="edit" value="${message(code: 'edit', 'default': 'Edit')}" /></span>
                    <span class="button"><g:actionSubmit class="delete" action="delete" value="${message(code: 'delete', 'default': 'Delete')}" onclick="return confirm('${message(code: 'delete.confirm', 'default': 'Are you sure?')}');" /></span>
                </div>
            </g:form>
        </div>
    </body>
</html>
