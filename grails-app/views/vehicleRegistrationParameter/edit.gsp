
<%@ page import="com.innovated.iris.domain.lookup.VehicleRegistrationParameter" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <title><g:message code="vehicleRegistrationParameter.edit" default="Edit VehicleRegistrationParameter" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLinkTo(dir: '')}"><g:message code="home" default="Home" /></a></span>
            <span class="menuButton"><g:link class="list" action="list"><g:message code="vehicleRegistrationParameter.list" default="VehicleRegistrationParameter List" /></g:link></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="vehicleRegistrationParameter.new" default="New VehicleRegistrationParameter" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="vehicleRegistrationParameter.edit" default="Edit VehicleRegistrationParameter" /></h1>
            <g:if test="${flash.message}">
            	<div class="message"><g:message code="${flash.message}" args="${flash.args}" default="${flash.defaultMessage}" /></div>
            </g:if>
            <g:hasErrors bean="${vehicleRegistrationParameterInstance}">
				<div class="errors">
					<g:renderErrors bean="${vehicleRegistrationParameterInstance}" as="list" />
				</div>
            </g:hasErrors>
            <g:form method="post" >
                <g:hiddenField name="id" value="${vehicleRegistrationParameterInstance?.id}" />
                <g:hiddenField name="version" value="${vehicleRegistrationParameterInstance?.version}" />
                <div class="dialog">
                    <fieldset>
                        <legend><g:message code="vehicleRegistrationParameter.edit.legend" default="Update VehicleRegistrationParameter Details"/></legend>
                        
                        <div class="prop mandatory ${hasErrors(bean: vehicleRegistrationParameterInstance, field: 'dateFrom', 'error')}">
                            <label for="dateFrom">
                                <g:message code="vehicleRegistrationParameter.dateFrom" default="Date From" />
                                <span class="indicator">*</span>
                            </label>
                            <g:datePicker name="dateFrom" value="${vehicleRegistrationParameterInstance?.dateFrom}"  />

                        </div>
                        
                        <div class="prop mandatory ${hasErrors(bean: vehicleRegistrationParameterInstance, field: 'customerCode', 'error')}">
                            <label for="customerCode">
                                <g:message code="vehicleRegistrationParameter.customerCode" default="Customer Code" />
                                <span class="indicator">*</span>
                            </label>
                            <g:textField name="customerCode" value="${fieldValue(bean: vehicleRegistrationParameterInstance, field: 'customerCode')}" />

                        </div>
                        
                        <div class="prop mandatory ${hasErrors(bean: vehicleRegistrationParameterInstance, field: 'state', 'error')}">
                            <label for="state">
                                <g:message code="vehicleRegistrationParameter.state" default="State" />
                                <span class="indicator">*</span>
                            </label>
                            <g:textField name="state" value="${fieldValue(bean: vehicleRegistrationParameterInstance, field: 'state')}" />

                        </div>
                        
                        <div class="prop mandatory ${hasErrors(bean: vehicleRegistrationParameterInstance, field: 'regoSupplier', 'error')}">
                            <label for="regoSupplier">
                                <g:message code="vehicleRegistrationParameter.regoSupplier" default="Rego Supplier" />
                                <span class="indicator">*</span>
                            </label>
                            <g:select name="regoSupplier.id" from="${com.innovated.iris.domain.Supplier.list()}" optionKey="id" value="${vehicleRegistrationParameterInstance?.regoSupplier?.id}"  />

                        </div>
                        
                        <div class="prop mandatory ${hasErrors(bean: vehicleRegistrationParameterInstance, field: 'ctpSupplier', 'error')}">
                            <label for="ctpSupplier">
                                <g:message code="vehicleRegistrationParameter.ctpSupplier" default="Ctp Supplier" />
                                <span class="indicator">*</span>
                            </label>
                            <g:select name="ctpSupplier.id" from="${com.innovated.iris.domain.Supplier.list()}" optionKey="id" value="${vehicleRegistrationParameterInstance?.ctpSupplier?.id}"  />

                        </div>
                        
                        <div class="prop mandatory ${hasErrors(bean: vehicleRegistrationParameterInstance, field: 'regoCostTwelveMths', 'error')}">
                            <label for="regoCostTwelveMths">
                                <g:message code="vehicleRegistrationParameter.regoCostTwelveMths" default="Rego Cost Twelve Mths" />
                                <span class="indicator">*</span>
                            </label>
                            <g:textField name="regoCostTwelveMths" value="${fieldValue(bean: vehicleRegistrationParameterInstance, field: 'regoCostTwelveMths')}" />

                        </div>
                        
                        <div class="prop ${hasErrors(bean: vehicleRegistrationParameterInstance, field: 'regoCostSixMths', 'error')}">
                            <label for="regoCostSixMths">
                                <g:message code="vehicleRegistrationParameter.regoCostSixMths" default="Rego Cost Six Mths" />
                                
                            </label>
                            <g:textField name="regoCostSixMths" value="${fieldValue(bean: vehicleRegistrationParameterInstance, field: 'regoCostSixMths')}" />

                        </div>
                        
                        <div class="prop mandatory ${hasErrors(bean: vehicleRegistrationParameterInstance, field: 'regoIncludesGst', 'error')}">
                            <label for="regoIncludesGst">
                                <g:message code="vehicleRegistrationParameter.regoIncludesGst" default="Rego Includes Gst" />
                                <span class="indicator">*</span>
                            </label>
                            <g:checkBox name="regoIncludesGst" value="${vehicleRegistrationParameterInstance?.regoIncludesGst}" />

                        </div>
                        
                        <div class="prop mandatory ${hasErrors(bean: vehicleRegistrationParameterInstance, field: 'ctpCostTwelveMths', 'error')}">
                            <label for="ctpCostTwelveMths">
                                <g:message code="vehicleRegistrationParameter.ctpCostTwelveMths" default="Ctp Cost Twelve Mths" />
                                <span class="indicator">*</span>
                            </label>
                            <g:textField name="ctpCostTwelveMths" value="${fieldValue(bean: vehicleRegistrationParameterInstance, field: 'ctpCostTwelveMths')}" />

                        </div>
                        
                        <div class="prop ${hasErrors(bean: vehicleRegistrationParameterInstance, field: 'ctpCostSixMths', 'error')}">
                            <label for="ctpCostSixMths">
                                <g:message code="vehicleRegistrationParameter.ctpCostSixMths" default="Ctp Cost Six Mths" />
                                
                            </label>
                            <g:textField name="ctpCostSixMths" value="${fieldValue(bean: vehicleRegistrationParameterInstance, field: 'ctpCostSixMths')}" />

                        </div>
                        
                    </fieldset>
                </div>
                <div class="buttons">
                    <span class="button"><g:actionSubmit class="save" action="update" value="${message(code: 'update', 'default': 'Update')}" /></span>
                    <span class="button"><g:actionSubmit class="delete" action="delete" value="${message(code: 'delete', 'default': 'Delete')}" onclick="return confirm('${message(code: 'delete.confirm', 'default': 'Are you sure?')}');" /></span>
                </div>
            </g:form>
        </div>
    </body>
</html>

