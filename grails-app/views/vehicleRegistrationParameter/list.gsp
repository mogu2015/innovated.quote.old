
<%@ page import="com.innovated.iris.domain.lookup.VehicleRegistrationParameter" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <title><g:message code="vehicleRegistrationParameter.list" default="VehicleRegistrationParameter List" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLinkTo(dir: '')}"><g:message code="home" default="Home" /></a></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="vehicleRegistrationParameter.new" default="New VehicleRegistrationParameter" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="vehicleRegistrationParameter.list" default="VehicleRegistrationParameter List" /></h1>
            <g:if test="${flash.message}">
            <div class="message"><g:message code="${flash.message}" args="${flash.args}" default="${flash.defaultMessage}" /></div>
            </g:if>
            <div class="list">
                <table>
                    <thead>
                        <tr>
                        
                   	    <g:sortableColumn property="id" title="Id" titleKey="vehicleRegistrationParameter.id" />
                        
                   	    <g:sortableColumn property="dateFrom" title="Date From" titleKey="vehicleRegistrationParameter.dateFrom" />
                        
                   	    <g:sortableColumn property="customerCode" title="Customer Code" titleKey="vehicleRegistrationParameter.customerCode" />
                        
                   	    <g:sortableColumn property="state" title="State" titleKey="vehicleRegistrationParameter.state" />
                        
                   	    <th><g:message code="vehicleRegistrationParameter.regoSupplier" default="Rego Supplier" /></th>
                   	    
                   	    <th><g:message code="vehicleRegistrationParameter.ctpSupplier" default="Ctp Supplier" /></th>
                   	    
                        </tr>
                    </thead>
                    <tbody>
                    <g:each in="${vehicleRegistrationParameterInstanceList}" status="i" var="vehicleRegistrationParameterInstance">
                        <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
                        
                            <td><g:link action="show" id="${vehicleRegistrationParameterInstance.id}">${fieldValue(bean: vehicleRegistrationParameterInstance, field: "id")}</g:link></td>
                        
                            <td><g:formatDate date="${vehicleRegistrationParameterInstance.dateFrom}" /></td>
                        
                            <td>${fieldValue(bean: vehicleRegistrationParameterInstance, field: "customerCode")}</td>
                        
                            <td>${fieldValue(bean: vehicleRegistrationParameterInstance, field: "state")}</td>
                        
                            <td>${fieldValue(bean: vehicleRegistrationParameterInstance, field: "regoSupplier")}</td>
                        
                            <td>${fieldValue(bean: vehicleRegistrationParameterInstance, field: "ctpSupplier")}</td>
                        
                        </tr>
                    </g:each>
                    </tbody>
                </table>
            </div>
            <div class="paginateButtons">
                <g:paginate total="${vehicleRegistrationParameterInstanceTotal}" />
            </div>
        </div>
    </body>
</html>
