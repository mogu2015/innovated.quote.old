
<%@ page import="com.innovated.iris.domain.lookup.LuxuryCarTaxParameter" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <title><g:message code="luxuryCarTaxParameter.show" default="Show LuxuryCarTaxParameter" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLinkTo(dir: '')}"><g:message code="home" default="Home" /></a></span>
            <span class="menuButton"><g:link class="list" action="list"><g:message code="luxuryCarTaxParameter.list" default="LuxuryCarTaxParameter List" /></g:link></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="luxuryCarTaxParameter.new" default="New LuxuryCarTaxParameter" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="luxuryCarTaxParameter.show" default="Show LuxuryCarTaxParameter" /></h1>
            <g:if test="${flash.message}">
            <div class="message"><g:message code="${flash.message}" args="${flash.args}" default="${flash.defaultMessage}" /></div>
            </g:if>
            <g:form>
                <g:hiddenField name="id" value="${luxuryCarTaxParameterInstance?.id}" />
                <div class="dialog">
                    <table>
                        <tbody>
                        
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="luxuryCarTaxParameter.id" default="Id" />:</td>
                                
                                <td valign="top" class="value">${fieldValue(bean: luxuryCarTaxParameterInstance, field: "id")}</td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="luxuryCarTaxParameter.dateFrom" default="Date From" />:</td>
                                
                                <td valign="top" class="value"><g:formatDate date="${luxuryCarTaxParameterInstance?.dateFrom}" /></td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="luxuryCarTaxParameter.customerCode" default="Customer Code" />:</td>
                                
                                <td valign="top" class="value">${fieldValue(bean: luxuryCarTaxParameterInstance, field: "customerCode")}</td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="luxuryCarTaxParameter.rate" default="Rate" />:</td>
                                
                                <td valign="top" class="value">${fieldValue(bean: luxuryCarTaxParameterInstance, field: "rate")}</td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="luxuryCarTaxParameter.threshold" default="Threshold" />:</td>
                                
                                <td valign="top" class="value">${fieldValue(bean: luxuryCarTaxParameterInstance, field: "threshold")}</td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="luxuryCarTaxParameter.fuelConsumption" default="Fuel Consumption" />:</td>
                                
                                <td valign="top" class="value">${fieldValue(bean: luxuryCarTaxParameterInstance, field: "fuelConsumption")}</td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="luxuryCarTaxParameter.fuelEfficientRate" default="Fuel Efficient Rate" />:</td>
                                
                                <td valign="top" class="value">${fieldValue(bean: luxuryCarTaxParameterInstance, field: "fuelEfficientRate")}</td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="luxuryCarTaxParameter.fuelEfficientThreshold" default="Fuel Efficient Threshold" />:</td>
                                
                                <td valign="top" class="value">${fieldValue(bean: luxuryCarTaxParameterInstance, field: "fuelEfficientThreshold")}</td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="luxuryCarTaxParameter.creditThreshold" default="Credit Threshold" />:</td>
                                
                                <td valign="top" class="value">${fieldValue(bean: luxuryCarTaxParameterInstance, field: "creditThreshold")}</td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="luxuryCarTaxParameter.lastUpdated" default="Last Updated" />:</td>
                                
                                <td valign="top" class="value"><g:formatDate date="${luxuryCarTaxParameterInstance?.lastUpdated}" /></td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="luxuryCarTaxParameter.dateCreated" default="Date Created" />:</td>
                                
                                <td valign="top" class="value"><g:formatDate date="${luxuryCarTaxParameterInstance?.dateCreated}" /></td>
                                
                            </tr>
                            
                        </tbody>
                    </table>
                </div>
                <div class="buttons">
                    <span class="button"><g:actionSubmit class="edit" action="edit" value="${message(code: 'edit', 'default': 'Edit')}" /></span>
                    <span class="button"><g:actionSubmit class="delete" action="delete" value="${message(code: 'delete', 'default': 'Delete')}" onclick="return confirm('${message(code: 'delete.confirm', 'default': 'Are you sure?')}');" /></span>
                </div>
            </g:form>
        </div>
    </body>
</html>
