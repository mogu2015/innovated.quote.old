
<%@ page import="com.innovated.iris.domain.lookup.LuxuryCarTaxParameter" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <title><g:message code="luxuryCarTaxParameter.list" default="LuxuryCarTaxParameter List" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLinkTo(dir: '')}"><g:message code="home" default="Home" /></a></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="luxuryCarTaxParameter.new" default="New LuxuryCarTaxParameter" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="luxuryCarTaxParameter.list" default="LuxuryCarTaxParameter List" /></h1>
            <g:if test="${flash.message}">
            <div class="message"><g:message code="${flash.message}" args="${flash.args}" default="${flash.defaultMessage}" /></div>
            </g:if>
            <div class="list">
                <table>
                    <thead>
                        <tr>
                        
                   	    <g:sortableColumn property="id" title="Id" titleKey="luxuryCarTaxParameter.id" />
                        
                   	    <g:sortableColumn property="dateFrom" title="Date From" titleKey="luxuryCarTaxParameter.dateFrom" />
                        
                   	    <g:sortableColumn property="customerCode" title="Customer Code" titleKey="luxuryCarTaxParameter.customerCode" />
                        
                   	    <g:sortableColumn property="rate" title="Rate" titleKey="luxuryCarTaxParameter.rate" />
                        
                   	    <g:sortableColumn property="threshold" title="Threshold" titleKey="luxuryCarTaxParameter.threshold" />
                        
                   	    <g:sortableColumn property="fuelConsumption" title="Fuel Consumption" titleKey="luxuryCarTaxParameter.fuelConsumption" />
                        
                        </tr>
                    </thead>
                    <tbody>
                    <g:each in="${luxuryCarTaxParameterInstanceList}" status="i" var="luxuryCarTaxParameterInstance">
                        <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
                        
                            <td><g:link action="show" id="${luxuryCarTaxParameterInstance.id}">${fieldValue(bean: luxuryCarTaxParameterInstance, field: "id")}</g:link></td>
                        
                            <td><g:formatDate date="${luxuryCarTaxParameterInstance.dateFrom}" /></td>
                        
                            <td>${fieldValue(bean: luxuryCarTaxParameterInstance, field: "customerCode")}</td>
                        
                            <td>${fieldValue(bean: luxuryCarTaxParameterInstance, field: "rate")}</td>
                        
                            <td>${fieldValue(bean: luxuryCarTaxParameterInstance, field: "threshold")}</td>
                        
                            <td>${fieldValue(bean: luxuryCarTaxParameterInstance, field: "fuelConsumption")}</td>
                        
                        </tr>
                    </g:each>
                    </tbody>
                </table>
            </div>
            <div class="paginateButtons">
                <g:paginate total="${luxuryCarTaxParameterInstanceTotal}" />
            </div>
        </div>
    </body>
</html>
