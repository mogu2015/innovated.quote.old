
<%@ page import="com.innovated.iris.domain.lookup.LuxuryCarTaxParameter" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <title><g:message code="luxuryCarTaxParameter.edit" default="Edit LuxuryCarTaxParameter" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLinkTo(dir: '')}"><g:message code="home" default="Home" /></a></span>
            <span class="menuButton"><g:link class="list" action="list"><g:message code="luxuryCarTaxParameter.list" default="LuxuryCarTaxParameter List" /></g:link></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="luxuryCarTaxParameter.new" default="New LuxuryCarTaxParameter" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="luxuryCarTaxParameter.edit" default="Edit LuxuryCarTaxParameter" /></h1>
            <g:if test="${flash.message}">
            <div class="message"><g:message code="${flash.message}" args="${flash.args}" default="${flash.defaultMessage}" /></div>
            </g:if>
            <g:hasErrors bean="${luxuryCarTaxParameterInstance}">
            <div class="errors">
                <g:renderErrors bean="${luxuryCarTaxParameterInstance}" as="list" />
            </div>
            </g:hasErrors>
            <g:form method="post" >
                <g:hiddenField name="id" value="${luxuryCarTaxParameterInstance?.id}" />
                <g:hiddenField name="version" value="${luxuryCarTaxParameterInstance?.version}" />
                <div class="dialog">
                    <table>
                        <tbody>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="dateFrom"><g:message code="luxuryCarTaxParameter.dateFrom" default="Date From" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: luxuryCarTaxParameterInstance, field: 'dateFrom', 'errors')}">
                                    <g:datePicker name="dateFrom" value="${luxuryCarTaxParameterInstance?.dateFrom}" precision="day" />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="customerCode"><g:message code="luxuryCarTaxParameter.customerCode" default="Customer Code" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: luxuryCarTaxParameterInstance, field: 'customerCode', 'errors')}">
                                    <g:textField name="customerCode" value="${fieldValue(bean: luxuryCarTaxParameterInstance, field: 'customerCode')}" />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="rate"><g:message code="luxuryCarTaxParameter.rate" default="Rate" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: luxuryCarTaxParameterInstance, field: 'rate', 'errors')}">
                                    <g:textField name="rate" value="${fieldValue(bean: luxuryCarTaxParameterInstance, field: 'rate')}" />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="threshold"><g:message code="luxuryCarTaxParameter.threshold" default="Threshold" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: luxuryCarTaxParameterInstance, field: 'threshold', 'errors')}">
                                    <g:textField name="threshold" value="${fieldValue(bean: luxuryCarTaxParameterInstance, field: 'threshold')}" />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="fuelConsumption"><g:message code="luxuryCarTaxParameter.fuelConsumption" default="Fuel Consumption" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: luxuryCarTaxParameterInstance, field: 'fuelConsumption', 'errors')}">
                                    <g:textField name="fuelConsumption" value="${fieldValue(bean: luxuryCarTaxParameterInstance, field: 'fuelConsumption')}" />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="fuelEfficientRate"><g:message code="luxuryCarTaxParameter.fuelEfficientRate" default="Fuel Efficient Rate" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: luxuryCarTaxParameterInstance, field: 'fuelEfficientRate', 'errors')}">
                                    <g:textField name="fuelEfficientRate" value="${fieldValue(bean: luxuryCarTaxParameterInstance, field: 'fuelEfficientRate')}" />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="fuelEfficientThreshold"><g:message code="luxuryCarTaxParameter.fuelEfficientThreshold" default="Fuel Efficient Threshold" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: luxuryCarTaxParameterInstance, field: 'fuelEfficientThreshold', 'errors')}">
                                    <g:textField name="fuelEfficientThreshold" value="${fieldValue(bean: luxuryCarTaxParameterInstance, field: 'fuelEfficientThreshold')}" />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="creditThreshold"><g:message code="luxuryCarTaxParameter.creditThreshold" default="Credit Threshold" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: luxuryCarTaxParameterInstance, field: 'creditThreshold', 'errors')}">
                                    <g:textField name="creditThreshold" value="${fieldValue(bean: luxuryCarTaxParameterInstance, field: 'creditThreshold')}" />

                                </td>
                            </tr>
                        <!--
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="lastUpdated"><g:message code="luxuryCarTaxParameter.lastUpdated" default="Last Updated" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: luxuryCarTaxParameterInstance, field: 'lastUpdated', 'errors')}">
                                    <g:datePicker name="lastUpdated" value="${luxuryCarTaxParameterInstance?.lastUpdated}"  />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="dateCreated"><g:message code="luxuryCarTaxParameter.dateCreated" default="Date Created" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: luxuryCarTaxParameterInstance, field: 'dateCreated', 'errors')}">
                                    <g:datePicker name="dateCreated" value="${luxuryCarTaxParameterInstance?.dateCreated}"  />

                                </td>
                            </tr>
                        -->
                        </tbody>
                    </table>
                </div>
                <div class="buttons">
                    <span class="button"><g:actionSubmit class="save" action="update" value="${message(code: 'update', 'default': 'Update')}" /></span>
                    <span class="button"><g:actionSubmit class="delete" action="delete" value="${message(code: 'delete', 'default': 'Delete')}" onclick="return confirm('${message(code: 'delete.confirm', 'default': 'Are you sure?')}');" /></span>
                </div>
            </g:form>
        </div>
    </body>
</html>
