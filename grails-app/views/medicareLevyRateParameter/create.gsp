
<%@ page import="com.innovated.iris.domain.lookup.MedicareLevyRateParameter" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <title><g:message code="medicareLevyRateParameter.create" default="Create MedicareLevyRateParameter" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLinkTo(dir: '')}"><g:message code="home" default="Home" /></a></span>
            <span class="menuButton"><g:link class="list" action="list"><g:message code="medicareLevyRateParameter.list" default="MedicareLevyRateParameter List" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="medicareLevyRateParameter.create" default="Create MedicareLevyRateParameter" /></h1>
            <g:if test="${flash.message}">
            <div class="message"><g:message code="${flash.message}" args="${flash.args}" default="${flash.defaultMessage}" /></div>
            </g:if>
            <g:hasErrors bean="${medicareLevyRateParameterInstance}">
            <div class="errors">
                <g:renderErrors bean="${medicareLevyRateParameterInstance}" as="list" />
            </div>
            </g:hasErrors>
            <g:form action="save" method="post" >
                <div class="dialog">
                    <table>
                        <tbody>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="dateFrom"><g:message code="medicareLevyRateParameter.dateFrom" default="Date From" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: medicareLevyRateParameterInstance, field: 'dateFrom', 'errors')}">
                                    <g:datePicker name="dateFrom" value="${medicareLevyRateParameterInstance?.dateFrom}" precision="day" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="rate"><g:message code="medicareLevyRateParameter.rate" default="Rate" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: medicareLevyRateParameterInstance, field: 'rate', 'errors')}">
                                    <g:textField name="rate" class="numeric" value="${formatNumber(number:medicareLevyRateParameterInstance.rate, type:'number', minFractionDigits:2, maxFractionDigits:8)}" />
                                </td>
                            </tr>
                        <!-- 
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="lastUpdated"><g:message code="medicareLevyRateParameter.lastUpdated" default="Last Updated" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: medicareLevyRateParameterInstance, field: 'lastUpdated', 'errors')}">
                                    <g:datePicker name="lastUpdated" value="${medicareLevyRateParameterInstance?.lastUpdated}"  />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="dateCreated"><g:message code="medicareLevyRateParameter.dateCreated" default="Date Created" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: medicareLevyRateParameterInstance, field: 'dateCreated', 'errors')}">
                                    <g:datePicker name="dateCreated" value="${medicareLevyRateParameterInstance?.dateCreated}"  />

                                </td>
                            </tr>
                         -->
                        </tbody>
                    </table>
                </div>
                <div class="buttons">
                    <span class="button"><g:submitButton name="create" class="save" value="${message(code: 'create', 'default': 'Create')}" /></span>
                </div>
            </g:form>
        </div>
    </body>
</html>
