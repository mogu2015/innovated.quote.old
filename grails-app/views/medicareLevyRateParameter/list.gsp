
<%@ page import="com.innovated.iris.domain.lookup.MedicareLevyRateParameter" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <title><g:message code="medicareLevyRateParameter.list" default="MedicareLevyRateParameter List" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLinkTo(dir: '')}"><g:message code="home" default="Home" /></a></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="medicareLevyRateParameter.new" default="New MedicareLevyRateParameter" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="medicareLevyRateParameter.list" default="MedicareLevyRateParameter List" /></h1>
            <g:if test="${flash.message}">
            <div class="message"><g:message code="${flash.message}" args="${flash.args}" default="${flash.defaultMessage}" /></div>
            </g:if>
            <div class="list">
                <table>
                    <thead>
                        <tr>
	                   	    <g:sortableColumn property="id" title="ID" titleKey="medicareLevyRateParameter.id" />
	                   	    <g:sortableColumn property="dateFrom" title="Date From" titleKey="medicareLevyRateParameter.dateFrom" />
	                   	    <g:sortableColumn property="rate" title="Rate" titleKey="medicareLevyRateParameter.rate" />
	                   	    <g:sortableColumn property="lastUpdated" title="Last Updated" titleKey="medicareLevyRateParameter.lastUpdated" />
	                   	    <g:sortableColumn property="dateCreated" title="Date Created" titleKey="medicareLevyRateParameter.dateCreated" />
                        </tr>
                    </thead>
                    <tbody>
                    <g:each in="${medicareLevyRateParameterInstanceList}" status="i" var="medicareLevyRateParameterInstance">
                        <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
                            <td><g:link action="show" id="${medicareLevyRateParameterInstance.id}">${fieldValue(bean: medicareLevyRateParameterInstance, field: "id")}</g:link></td>
                            <td><g:formatDate format="d MMM yyyy" date="${medicareLevyRateParameterInstance.dateFrom}" /></td>
                            <td>${formatNumber(number:medicareLevyRateParameterInstance.rate, type:'percent', minFractionDigits:2, maxFractionDigits:8)}</td>
                            <td><g:formatDate date="${medicareLevyRateParameterInstance.lastUpdated}" /></td>
                            <td><g:formatDate date="${medicareLevyRateParameterInstance.dateCreated}" /></td>
                        </tr>
                    </g:each>
                    </tbody>
                </table>
            </div>
            <div class="paginateButtons">
                <g:paginate total="${medicareLevyRateParameterInstanceTotal}" />
            </div>
        </div>
    </body>
</html>
