
<%@ page import="com.innovated.iris.domain.lookup.MedicareLevyRateParameter" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <title><g:message code="medicareLevyRateParameter.show" default="Show MedicareLevyRateParameter" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLinkTo(dir: '')}"><g:message code="home" default="Home" /></a></span>
            <span class="menuButton"><g:link class="list" action="list"><g:message code="medicareLevyRateParameter.list" default="MedicareLevyRateParameter List" /></g:link></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="medicareLevyRateParameter.new" default="New MedicareLevyRateParameter" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="medicareLevyRateParameter.show" default="Show MedicareLevyRateParameter" /></h1>
            <g:if test="${flash.message}">
            <div class="message"><g:message code="${flash.message}" args="${flash.args}" default="${flash.defaultMessage}" /></div>
            </g:if>
            <g:form>
                <g:hiddenField name="id" value="${medicareLevyRateParameterInstance?.id}" />
                <div class="dialog">
                    <table>
                        <tbody>
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="medicareLevyRateParameter.id" default="Id" />:</td>
                                <td valign="top" class="value">${fieldValue(bean: medicareLevyRateParameterInstance, field: "id")}</td>
                            </tr>
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="medicareLevyRateParameter.dateFrom" default="Date From" />:</td>
                                <td valign="top" class="value"><g:formatDate format="d MMM yyyy" date="${medicareLevyRateParameterInstance?.dateFrom}" /></td>
                            </tr>
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="medicareLevyRateParameter.rate" default="Rate" />:</td>
                                <td valign="top" class="value">${formatNumber(number:medicareLevyRateParameterInstance.rate, type:'percent', minFractionDigits:2, maxFractionDigits:8)}</td>
                            </tr>
                            <!-- 
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="medicareLevyRateParameter.lastUpdated" default="Last Updated" />:</td>
                                <td valign="top" class="value"><g:formatDate date="${medicareLevyRateParameterInstance?.lastUpdated}" /></td>
                            </tr>
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="medicareLevyRateParameter.dateCreated" default="Date Created" />:</td>
                                <td valign="top" class="value"><g:formatDate date="${medicareLevyRateParameterInstance?.dateCreated}" /></td>
                            </tr>
                             -->
                        </tbody>
                    </table>
                </div>
                <div class="buttons">
                    <span class="button"><g:actionSubmit class="edit" action="edit" value="${message(code: 'edit', 'default': 'Edit')}" /></span>
                    <span class="button"><g:actionSubmit class="delete" action="delete" value="${message(code: 'delete', 'default': 'Delete')}" onclick="return confirm('${message(code: 'delete.confirm', 'default': 'Are you sure?')}');" /></span>
                </div>
            </g:form>
        </div>
    </body>
</html>
