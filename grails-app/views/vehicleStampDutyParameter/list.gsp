<%@
page import="com.innovated.iris.domain.lookup.*"
page import="com.innovated.iris.domain.enums.*"
page import="com.innovated.iris.domain.*"
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <title><g:message code="vehicleStampDutyParameter.list" default="PSD Lookup Value List" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLinkTo(dir: '')}"><g:message code="home" default="Home" /></a></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="vehicleStampDutyParameter.new" default="New PSD Lookup Value" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="vehicleStampDutyParameter.list" default="PSD Lookup Value List" /></h1>
            <g:if test="${flash.message}">
            <div class="message"><g:message code="${flash.message}" args="${flash.args}" default="${flash.defaultMessage}" /></div>
            </g:if>
            <div class="list">
                <table>
                    <thead>
                        <tr>
		               	    <g:sortableColumn property="id" title="Id" titleKey="vehicleStampDutyParameter.id" />
		               	    <g:sortableColumn property="dateFrom" title="Date From" titleKey="vehicleStampDutyParameter.dateFrom" />
		               	    <g:sortableColumn property="customerCode" title="Customer Code" titleKey="vehicleStampDutyParameter.customerCode" />
		               	    <g:sortableColumn property="state" title="State" titleKey="vehicleStampDutyParameter.state" />
		               	    <g:sortableColumn property="lowValue" title="Low Value" titleKey="vehicleStampDutyParameter.lowValue" />
		               	    <g:sortableColumn property="highValue" title="High Value" titleKey="vehicleStampDutyParameter.highValue" />
		               	    <g:sortableColumn property="thresholdValue" title="Threshold Value" titleKey="vehicleStampDutyParameter.thresholdValue" />
                        </tr>
                    </thead>
                    <tbody>
                    <g:each in="${vehicleStampDutyParameterInstanceList}" status="i" var="vehicleStampDutyParameterInstance">
                        <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
                            <td><g:link action="edit" id="${vehicleStampDutyParameterInstance.id}">${fieldValue(bean: vehicleStampDutyParameterInstance, field: "id")}</g:link></td>
                            <td><g:formatDate date="${vehicleStampDutyParameterInstance.dateFrom}" format="d MMM yyyy" /></td>
                            <g:if test="${!Customer.BASE_CUST_CODE.equals(vehicleStampDutyParameterInstance.customerCode)}">
                            <td><g:link controller="customer" action="show" params="[code:vehicleStampDutyParameterInstance.customerCode]" title="Show customer">${fieldValue(bean: vehicleStampDutyParameterInstance, field: "customerCode")}</g:link></td>
                            </g:if>
                            <g:else>
                            <td>${fieldValue(bean: vehicleStampDutyParameterInstance, field: "customerCode")}</td>
                            </g:else>
                            <td>${fieldValue(bean: vehicleStampDutyParameterInstance, field: "state")}</td>
                            <td class="numeric"><g:formatNumber number="${vehicleStampDutyParameterInstance.lowValue}" format="#,##0.00 %" /></td>
                        	<td class="numeric"><g:formatNumber number="${vehicleStampDutyParameterInstance.highValue}" format="#,##0.00 %" /></td>
                            <td class="numeric"><g:formatNumber number="${vehicleStampDutyParameterInstance.thresholdValue}" format="\$ ###,##0" /></td>
                        </tr>
                    </g:each>
                    </tbody>
                </table>
            </div>
            <div class="paginateButtons">
                <g:paginate total="${vehicleStampDutyParameterInstanceTotal}" />
            </div>
        </div>
    </body>
</html>
