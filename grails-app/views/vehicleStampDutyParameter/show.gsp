
<%@ page import="com.innovated.iris.domain.lookup.VehicleStampDutyParameter" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <title><g:message code="vehicleStampDutyParameter.show" default="Show VehicleStampDutyParameter" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLinkTo(dir: '')}"><g:message code="home" default="Home" /></a></span>
            <span class="menuButton"><g:link class="list" action="list"><g:message code="vehicleStampDutyParameter.list" default="VehicleStampDutyParameter List" /></g:link></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="vehicleStampDutyParameter.new" default="New VehicleStampDutyParameter" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="vehicleStampDutyParameter.show" default="Show VehicleStampDutyParameter" /></h1>
            <g:if test="${flash.message}">
            <div class="message"><g:message code="${flash.message}" args="${flash.args}" default="${flash.defaultMessage}" /></div>
            </g:if>
            <g:form>
                <g:hiddenField name="id" value="${vehicleStampDutyParameterInstance?.id}" />
                <div class="dialog">
                    <table>
                        <tbody>
                        
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="vehicleStampDutyParameter.id" default="Id" />:</td>
                                
                                <td valign="top" class="value">${fieldValue(bean: vehicleStampDutyParameterInstance, field: "id")}</td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="vehicleStampDutyParameter.dateFrom" default="Date From" />:</td>
                                
                                <td valign="top" class="value"><g:formatDate date="${vehicleStampDutyParameterInstance?.dateFrom}" /></td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="vehicleStampDutyParameter.customerCode" default="Customer Code" />:</td>
                                
                                <td valign="top" class="value">${fieldValue(bean: vehicleStampDutyParameterInstance, field: "customerCode")}</td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="vehicleStampDutyParameter.state" default="State" />:</td>
                                
                                <td valign="top" class="value">${fieldValue(bean: vehicleStampDutyParameterInstance, field: "state")}</td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="vehicleStampDutyParameter.lowValue" default="Low Value" />:</td>
                                
                                <td valign="top" class="value">${fieldValue(bean: vehicleStampDutyParameterInstance, field: "lowValue")}</td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="vehicleStampDutyParameter.highValue" default="High Value" />:</td>
                                
                                <td valign="top" class="value">${fieldValue(bean: vehicleStampDutyParameterInstance, field: "highValue")}</td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="vehicleStampDutyParameter.thresholdValue" default="Threshold Value" />:</td>
                                
                                <td valign="top" class="value">${fieldValue(bean: vehicleStampDutyParameterInstance, field: "thresholdValue")}</td>
                                
                            </tr>
                            
                        </tbody>
                    </table>
                </div>
                <div class="buttons">
                    <span class="button"><g:actionSubmit class="edit" action="edit" value="${message(code: 'edit', 'default': 'Edit')}" /></span>
                    <span class="button"><g:actionSubmit class="delete" action="delete" value="${message(code: 'delete', 'default': 'Delete')}" onclick="return confirm('${message(code: 'delete.confirm', 'default': 'Are you sure?')}');" /></span>
                </div>
            </g:form>
        </div>
    </body>
</html>
