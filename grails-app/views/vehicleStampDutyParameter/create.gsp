<%@
page import="com.innovated.iris.domain.lookup.*"
page import="com.innovated.iris.domain.enums.*"
page import="com.innovated.iris.domain.*"
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <title><g:message code="vehicleStampDutyParameter.create" default="Create PSD Lookup Value" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLinkTo(dir: '')}"><g:message code="home" default="Home" /></a></span>
            <span class="menuButton"><g:link class="list" action="list"><g:message code="vehicleStampDutyParameter.list" default="PSD Lookup Value List" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="vehicleStampDutyParameter.create" default="Create PSD Lookup Value" /></h1>
            <g:if test="${flash.message}">
            	<div class="message"><g:message code="${flash.message}" args="${flash.args}" default="${flash.defaultMessage}" /></div>
            </g:if>
            <g:hasErrors bean="${vehicleStampDutyParameterInstance}">
				<div class="errors">
					<g:renderErrors bean="${vehicleStampDutyParameterInstance}" as="list" />
				</div>
            </g:hasErrors>
            <g:form action="save" method="post" >
                <div class="dialog">
                    <fieldset>
                        <legend><g:message code="vehicleStampDutyParameter.create.legend" default="Enter PSD Lookup Value Details"/></legend>
                        
                            <div class="prop mandatory ${hasErrors(bean: vehicleStampDutyParameterInstance, field: 'dateFrom', 'error')}">
                                <label for="dateFrom">
                                    <g:message code="vehicleStampDutyParameter.dateFrom" default="Date From" />
                                    <span class="indicator">*</span>
                                </label>
                                <g:datePicker name="dateFrom" value="${vehicleStampDutyParameterInstance?.dateFrom}" precision="day" />

                            </div>
                        
                            <div class="prop mandatory ${hasErrors(bean: vehicleStampDutyParameterInstance, field: 'customerCode', 'error')}">
                                <label for="customerCode">
                                    <g:message code="vehicleStampDutyParameter.customerCode" default="Customer Code" />
                                    <span class="indicator">*</span>
                                </label>
                                <g:select name="customerCode" from="${Customer.list()}"
                                	optionKey="code" optionValue="${{it.code}}"
                                	value="${vehicleStampDutyParameterInstance?.customerCode}"
                                	noSelection="${[(defaultCode):'DEFAULT']}" />
                            </div>
                        
                            <div class="prop mandatory ${hasErrors(bean: vehicleStampDutyParameterInstance, field: 'state', 'error')}">
                                <label for="state">
                                    <g:message code="vehicleStampDutyParameter.state" default="State" />
                                    <span class="indicator">*</span>
                                </label>
                                <g:select name="state" from="${AustralianState.list()}"
                                	optionKey="abbreviation" optionValue="${{it.abbreviation}}"
                                	value="${vehicleStampDutyParameterInstance?.state}" />
                            </div>
                        
                            <div class="prop mandatory ${hasErrors(bean: vehicleStampDutyParameterInstance, field: 'lowValue', 'error')}">
                                <label for="lowValue">
                                    <g:message code="vehicleStampDutyParameter.lowValue" default="Low Value" />
                                    <span class="indicator">*</span>
                                </label>
                                <g:textField name="lowValue" value="${fieldValue(bean: vehicleStampDutyParameterInstance, field: 'lowValue')}" />

                            </div>
                        
                            <div class="prop ${hasErrors(bean: vehicleStampDutyParameterInstance, field: 'highValue', 'error')}">
                                <label for="highValue">
                                    <g:message code="vehicleStampDutyParameter.highValue" default="High Value" />
                                    
                                </label>
                                <g:textField name="highValue" value="${fieldValue(bean: vehicleStampDutyParameterInstance, field: 'highValue')}" />

                            </div>
                        
                            <div class="prop ${hasErrors(bean: vehicleStampDutyParameterInstance, field: 'thresholdValue', 'error')}">
                                <label for="thresholdValue">
                                    <g:message code="vehicleStampDutyParameter.thresholdValue" default="Threshold Value" />
                                    
                                </label>
                                <g:textField name="thresholdValue" value="${fieldValue(bean: vehicleStampDutyParameterInstance, field: 'thresholdValue')}" />

                            </div>
                        
                    </fieldset>
                </div>
                <div class="buttons">
                    <span class="button"><g:submitButton name="create" class="save" value="${message(code: 'create', 'default': 'Create')}" /></span>
                </div>
            </g:form>
        </div>
    </body>
</html>

