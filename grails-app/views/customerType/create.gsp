
<%@ page import="com.innovated.iris.domain.enums.CustomerType" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <title><g:message code="customerType.create" default="Create CustomerType" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLinkTo(dir: '')}"><g:message code="home" default="Home" /></a></span>
            <span class="menuButton"><g:link class="list" action="list"><g:message code="customerType.list" default="CustomerType List" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="customerType.create" default="Create CustomerType" /></h1>
            <g:if test="${flash.message}">
            	<div class="message"><g:message code="${flash.message}" args="${flash.args}" default="${flash.defaultMessage}" /></div>
            </g:if>
            <g:hasErrors bean="${customerTypeInstance}">
				<div class="errors">
					<g:renderErrors bean="${customerTypeInstance}" as="list" />
				</div>
            </g:hasErrors>
            <g:form action="save" method="post" >
                <div class="dialog">
                    <fieldset>
                        <legend><g:message code="customerType.create.legend" default="Enter CustomerType Details"/></legend>
                        
                            <div class="prop mandatory ${hasErrors(bean: customerTypeInstance, field: 'name', 'error')}">
                                <label for="name">
                                    <g:message code="customerType.name" default="Name" />
                                    <span class="indicator">*</span>
                                </label>
                                <input type="text" id="name" name="name" value="${fieldValue(bean:customerTypeInstance,field:'name')}"/>
                            </div>
                        
                            <div class="prop mandatory ${hasErrors(bean: customerTypeInstance, field: 'typeCode', 'error')}">
                                <label for="typeCode">
                                    <g:message code="customerType.typeCode" default="Type Code" />
                                    <span class="indicator">*</span>
                                </label>
                                <input type="text" id="typeCode" name="typeCode" value="${fieldValue(bean:customerTypeInstance,field:'typeCode')}"/>
                            </div>
                        
                            <div class="prop mandatory ${hasErrors(bean: customerTypeInstance, field: 'visible', 'error')}">
                                <label for="visible">
                                    <g:message code="customerType.visible" default="Visible" />
                                    <span class="indicator">*</span>
                                </label>
                                <g:checkBox name="visible" value="${customerTypeInstance?.visible}" ></g:checkBox>
                            </div>
                        
                            <div class="prop mandatory ${hasErrors(bean: customerTypeInstance, field: 'enabled', 'error')}">
                                <label for="enabled">
                                    <g:message code="customerType.enabled" default="Enabled" />
                                    <span class="indicator">*</span>
                                </label>
                                <g:checkBox name="enabled" value="${customerTypeInstance?.enabled}" ></g:checkBox>
                            </div>
                        
                            <div class="prop mandatory ${hasErrors(bean: customerTypeInstance, field: 'dateCreated', 'error')}">
                                <label for="dateCreated">
                                    <g:message code="customerType.dateCreated" default="Date Created" />
                                    <span class="indicator">*</span>
                                </label>
                                <g:datePicker name="dateCreated" value="${customerTypeInstance?.dateCreated}" precision="minute" ></g:datePicker>
                            </div>
                        
                            <div class="prop mandatory ${hasErrors(bean: customerTypeInstance, field: 'lastUpdated', 'error')}">
                                <label for="lastUpdated">
                                    <g:message code="customerType.lastUpdated" default="Last Updated" />
                                    <span class="indicator">*</span>
                                </label>
                                <g:datePicker name="lastUpdated" value="${customerTypeInstance?.lastUpdated}" precision="minute" ></g:datePicker>
                            </div>
                        
                    </fieldset>
                </div>
                <div class="buttons">
                    <span class="button"><g:submitButton name="create" class="save" value="${message(code: 'create', 'default': 'Create')}" /></span>
                </div>
            </g:form>
        </div>
    </body>
</html>

