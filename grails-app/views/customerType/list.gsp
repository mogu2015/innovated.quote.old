
<%@ page import="com.innovated.iris.domain.enums.CustomerType" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta name="layout" content="main" />
        <title>CustomerType List</title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${resource(dir:'')}">Home</a></span>
            <span class="menuButton"><g:link class="create" action="create">New CustomerType</g:link></span>
        </div>
        <div class="body">
            <h1>CustomerType List</h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <div class="list">
                <table>
                    <thead>
                        <tr>
                        
                   	        <g:sortableColumn property="id" title="Id" />
                        
                   	        <g:sortableColumn property="name" title="Name" />
                        
                   	        <g:sortableColumn property="typeCode" title="Type Code" />
                        
                   	        <g:sortableColumn property="visible" title="Visible" />
                        
                   	        <g:sortableColumn property="enabled" title="Enabled" />
                        
                   	        <g:sortableColumn property="dateCreated" title="Date Created" />
                        
                        </tr>
                    </thead>
                    <tbody>
                    <g:each in="${customerTypeInstanceList}" status="i" var="customerTypeInstance">
                        <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
                        
                            <td><g:link action="show" id="${customerTypeInstance.id}">${fieldValue(bean:customerTypeInstance, field:'id')}</g:link></td>
                        
                            <td>${fieldValue(bean:customerTypeInstance, field:'name')}</td>
                        
                            <td>${fieldValue(bean:customerTypeInstance, field:'typeCode')}</td>
                        
                            <td>${fieldValue(bean:customerTypeInstance, field:'visible')}</td>
                        
                            <td>${fieldValue(bean:customerTypeInstance, field:'enabled')}</td>
                        
                            <td>${fieldValue(bean:customerTypeInstance, field:'dateCreated')}</td>
                        
                        </tr>
                    </g:each>
                    </tbody>
                </table>
            </div>
            <div class="paginateButtons">
                <g:paginate total="${customerTypeInstanceTotal}" />
            </div>
        </div>
    </body>
</html>
