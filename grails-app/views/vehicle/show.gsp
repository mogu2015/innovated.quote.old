<%@ page import="com.innovated.iris.domain.Vehicle" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <title><g:message code="vehicle.show" default="Show Vehicle" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLinkTo(dir: '')}"><g:message code="home" default="Home" /></a></span>
            <span class="menuButton"><g:link class="list" action="list"><g:message code="vehicle.list" default="Vehicle List" /></g:link></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="vehicle.new" default="New Vehicle" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="vehicle.show" default="Show Vehicle" /></h1>
            <g:if test="${flash.message}">
            <div class="message"><g:message code="${flash.message}" args="${flash.args}" default="${flash.defaultMessage}" /></div>
            </g:if>
            <g:form>
                <g:hiddenField name="id" value="${vehicleInstance?.id}" />
                <div class="dialog">
                    <table>
                        <tbody>
                        
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="vehicle.id" default="Id" />:</td>
                                
                                <td valign="top" class="value">${fieldValue(bean: vehicleInstance, field: "id")}</td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="vehicle.assetCode" default="Asset Code" />:</td>
                                
                                <td valign="top" class="value">${fieldValue(bean: vehicleInstance, field: "assetCode")}</td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="vehicle.owner" default="Owner" />:</td>
                                
                                <td valign="top" class="value"><g:link controller="customer" action="show" id="${vehicleInstance?.owner?.id}">${vehicleInstance?.owner?.encodeAsHTML()}</g:link></td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="vehicle.acquisitionDate" default="Acquisition Date" />:</td>
                                
                                <td valign="top" class="value"><g:formatDate date="${vehicleInstance?.acquisitionDate}" /></td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="vehicle.acquisitionPrice" default="Acquisition Price" />:</td>
                                
                                <td valign="top" class="value"><g:formatNumber number="${vehicleInstance?.acquisitionPrice}" /></td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="vehicle.disposalDate" default="Disposal Date" />:</td>
                                
                                <td valign="top" class="value"><g:formatDate date="${vehicleInstance?.disposalDate}" /></td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="vehicle.disposalPrice" default="Disposal Price" />:</td>
                                
                                <td valign="top" class="value"><g:formatNumber number="${vehicleInstance?.disposalPrice}" /></td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="vehicle.information" default="Information" />:</td>
                                
                                <td valign="top" class="value">${fieldValue(bean: vehicleInstance, field: "information")}</td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="vehicle.budgets" default="Budgets" />:</td>
                                
                                <td  valign="top" style="text-align: left;" class="value">
                                    <ul>
                                    <g:each in="${vehicleInstance?.budgets}" var="budgetInstance">
                                        <li><g:link controller="budget" action="show" id="${budgetInstance.id}">${budgetInstance.encodeAsHTML()}</g:link></li>
                                    </g:each>
                                    </ul>
                                </td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="vehicle.driver" default="Driver" />:</td>
                                
                                <td valign="top" class="value">${fieldValue(bean: vehicleInstance, field: "driver")}</td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="vehicle.vin" default="Vin" />:</td>
                                
                                <td valign="top" class="value">${fieldValue(bean: vehicleInstance, field: "vin")}</td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="vehicle.engineNumber" default="Engine Number" />:</td>
                                
                                <td valign="top" class="value">${fieldValue(bean: vehicleInstance, field: "engineNumber")}</td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="vehicle.complianceDate" default="Compliance Date" />:</td>
                                
                                <td valign="top" class="value"><g:formatDate date="${vehicleInstance?.complianceDate}" /></td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="vehicle.colour" default="Colour" />:</td>
                                
                                <td valign="top" class="value">${fieldValue(bean: vehicleInstance, field: "colour")}</td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="vehicle.trim" default="Trim" />:</td>
                                
                                <td valign="top" class="value">${fieldValue(bean: vehicleInstance, field: "trim")}</td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="vehicle.photo" default="Photo" />:</td>
                                
                                <td valign="top" class="value">
                                	<!--${fieldValue(bean: vehicleInstance, field: "photo")}-->
                                	<img class="vehicle_image" src="${createLink(controller:'vehicle', action:'vehicle_image', id:vehicleInstance?.id)}" />
                                </td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="vehicle.registration" default="Registration" />:</td>
                                
                                <td valign="top" class="value"><g:link controller="registration" action="show" id="${vehicleInstance?.registration?.id}">${vehicleInstance?.registration?.encodeAsHTML()}</g:link></td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="vehicle.makeModel" default="Make Model" />:</td>
                                
                                <td valign="top" class="value"><g:link controller="makeModel" action="show" id="${vehicleInstance?.makeModel?.id}">${vehicleInstance?.makeModel?.encodeAsHTML()}</g:link></td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="vehicle.dateCreated" default="Date Created" />:</td>
                                
                                <td valign="top" class="value"><g:formatDate date="${vehicleInstance?.dateCreated}" /></td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="vehicle.lastUpdated" default="Last Updated" />:</td>
                                
                                <td valign="top" class="value"><g:formatDate date="${vehicleInstance?.lastUpdated}" /></td>
                                
                            </tr>
                            
                        </tbody>
                    </table>
                </div>
                <div class="buttons">
                    <span class="button"><g:actionSubmit class="edit" action="edit" value="${message(code: 'edit', 'default': 'Edit')}" /></span>
                    <span class="button"><g:actionSubmit class="delete" action="delete" value="${message(code: 'delete', 'default': 'Delete')}" onclick="return confirm('${message(code: 'delete.confirm', 'default': 'Are you sure?')}');" /></span>
                </div>
            </g:form>
        </div>
    </body>
</html>
