<%@ page import="com.innovated.iris.domain.Vehicle" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <title><g:message code="vehicle.edit" default="Edit Vehicle" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLinkTo(dir: '')}"><g:message code="home" default="Home" /></a></span>
            <span class="menuButton"><g:link class="list" action="list"><g:message code="vehicle.list" default="Vehicle List" /></g:link></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="vehicle.new" default="New Vehicle" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="vehicle.edit" default="Edit Vehicle" /></h1>
            <g:if test="${flash.message}">
            	<div class="message"><g:message code="${flash.message}" args="${flash.args}" default="${flash.defaultMessage}" /></div>
            </g:if>
            <g:hasErrors bean="${vehicleInstance}">
				<div class="errors">
					<g:renderErrors bean="${vehicleInstance}" as="list" />
				</div>
            </g:hasErrors>
            <g:form method="post"  enctype="multipart/form-data">
                <g:hiddenField name="id" value="${vehicleInstance?.id}" />
                <g:hiddenField name="version" value="${vehicleInstance?.version}" />
                <div class="dialog">
                    <fieldset>
                        <legend><g:message code="vehicle.edit.legend" default="Update Vehicle Details"/></legend>
                        
                        <div class="prop ${hasErrors(bean: vehicleInstance, field: 'assetCode', 'error')}">
                            <label for="assetCode">
                                <g:message code="vehicle.assetCode" default="Asset Code" />
                                
                            </label>
                            <g:textField name="assetCode" value="${fieldValue(bean: vehicleInstance, field: 'assetCode')}" />

                        </div>
                        
                        <div class="prop mandatory ${hasErrors(bean: vehicleInstance, field: 'owner', 'error')}">
                            <label for="owner">
                                <g:message code="vehicle.owner" default="Owner" />
                                <span class="indicator">*</span>
                            </label>
                            <g:select name="owner.id" from="${com.innovated.iris.domain.Customer.list()}" optionKey="id" value="${vehicleInstance?.owner?.id}"  />

                        </div>
                        
                        <div class="prop mandatory ${hasErrors(bean: vehicleInstance, field: 'acquisitionDate', 'error')}">
                            <label for="acquisitionDate">
                                <g:message code="vehicle.acquisitionDate" default="Acquisition Date" />
                                <span class="indicator">*</span>
                            </label>
                            <g:datePicker name="acquisitionDate" value="${vehicleInstance?.acquisitionDate}"  />

                        </div>
                        
                        <div class="prop mandatory ${hasErrors(bean: vehicleInstance, field: 'acquisitionPrice', 'error')}">
                            <label for="acquisitionPrice">
                                <g:message code="vehicle.acquisitionPrice" default="Acquisition Price" />
                                <span class="indicator">*</span>
                            </label>
                            <g:textField name="acquisitionPrice" value="${fieldValue(bean: vehicleInstance, field: 'acquisitionPrice')}" />

                        </div>
                        
                        <div class="prop ${hasErrors(bean: vehicleInstance, field: 'disposalDate', 'error')}">
                            <label for="disposalDate">
                                <g:message code="vehicle.disposalDate" default="Disposal Date" />
                                
                            </label>
                            <g:datePicker name="disposalDate" value="${vehicleInstance?.disposalDate}" noSelection="['': '']" />

                        </div>
                        
                        <div class="prop ${hasErrors(bean: vehicleInstance, field: 'disposalPrice', 'error')}">
                            <label for="disposalPrice">
                                <g:message code="vehicle.disposalPrice" default="Disposal Price" />
                                
                            </label>
                            <g:textField name="disposalPrice" value="${fieldValue(bean: vehicleInstance, field: 'disposalPrice')}" />

                        </div>
                        
                        <div class="prop ${hasErrors(bean: vehicleInstance, field: 'information', 'error')}">
                            <label for="information">
                                <g:message code="vehicle.information" default="Information" />
                                
                            </label>
                            <g:textField name="information" value="${fieldValue(bean: vehicleInstance, field: 'information')}" />

                        </div>
                        
                        <div class="prop mandatory ${hasErrors(bean: vehicleInstance, field: 'budgets', 'error')}">
                            <label for="budgets">
                                <g:message code="vehicle.budgets" default="Budgets" />
                                <span class="indicator">*</span>
                            </label>
                            <g:select name="budgets"
from="${com.innovated.iris.domain.Budget.list()}"
size="5" multiple="yes" optionKey="id"
value="${vehicleInstance?.budgets}" />


                        </div>
                        
                        <div class="prop mandatory ${hasErrors(bean: vehicleInstance, field: 'driver', 'error')}">
                            <label for="driver">
                                <g:message code="vehicle.driver" default="Driver" />
                                <span class="indicator">*</span>
                            </label>
                            <g:textField name="driver" value="${fieldValue(bean: vehicleInstance, field: 'driver')}" />

                        </div>
                        
                        <div class="prop ${hasErrors(bean: vehicleInstance, field: 'vin', 'error')}">
                            <label for="vin">
                                <g:message code="vehicle.vin" default="Vin" />
                                
                            </label>
                            <g:textField name="vin" value="${fieldValue(bean: vehicleInstance, field: 'vin')}" />

                        </div>
                        
                        <div class="prop ${hasErrors(bean: vehicleInstance, field: 'engineNumber', 'error')}">
                            <label for="engineNumber">
                                <g:message code="vehicle.engineNumber" default="Engine Number" />
                                
                            </label>
                            <g:textField name="engineNumber" value="${fieldValue(bean: vehicleInstance, field: 'engineNumber')}" />

                        </div>
                        
                        <div class="prop mandatory ${hasErrors(bean: vehicleInstance, field: 'complianceDate', 'error')}">
                            <label for="complianceDate">
                                <g:message code="vehicle.complianceDate" default="Compliance Date" />
                                <span class="indicator">*</span>
                            </label>
                            <g:datePicker name="complianceDate" value="${vehicleInstance?.complianceDate}"  />

                        </div>
                        
                        <div class="prop ${hasErrors(bean: vehicleInstance, field: 'colour', 'error')}">
                            <label for="colour">
                                <g:message code="vehicle.colour" default="Colour" />
                                
                            </label>
                            <g:textField name="colour" value="${fieldValue(bean: vehicleInstance, field: 'colour')}" />

                        </div>
                        
                        <div class="prop ${hasErrors(bean: vehicleInstance, field: 'trim', 'error')}">
                            <label for="trim">
                                <g:message code="vehicle.trim" default="Trim" />
                                
                            </label>
                            <g:textField name="trim" value="${fieldValue(bean: vehicleInstance, field: 'trim')}" />

                        </div>
                        
                        <div class="prop mandatory ${hasErrors(bean: vehicleInstance, field: 'photo', 'error')}">
                            <label for="photo">
                                <g:message code="vehicle.photo" default="Vehicle Photo" />
                                <span class="indicator">*</span>
                            </label>
                            <img class="vehicle_image" src="${createLink(controller:'vehicle', action:'vehicle_image', id:vehicleInstance?.id)}" />
                            
                            <input type="file" id="photo" name="photo" value="Change?" />

                        </div>
                        
                        <div class="prop ${hasErrors(bean: vehicleInstance, field: 'registration', 'error')}">
                            <label for="registration">
                                <g:message code="vehicle.registration" default="Registration" />
                                
                            </label>
                            <g:select name="registration.id" from="${com.innovated.iris.domain.Registration.list()}" optionKey="id" value="${vehicleInstance?.registration?.id}" noSelection="['-1': '']" />

                        </div>
                        
                        <div class="prop mandatory ${hasErrors(bean: vehicleInstance, field: 'makeModel', 'error')}">
                            <label for="makeModel">
                                <g:message code="vehicle.makeModel" default="Make Model" />
                                <span class="indicator">*</span>
                            </label>
                            <g:select name="makeModel.id" from="${com.innovated.iris.domain.MakeModel.list()}" optionKey="id" value="${vehicleInstance?.makeModel?.id}"  />

                        </div>
                        
                        <div class="prop mandatory ${hasErrors(bean: vehicleInstance, field: 'dateCreated', 'error')}">
                            <label for="dateCreated">
                                <g:message code="vehicle.dateCreated" default="Date Created" />
                                <span class="indicator">*</span>
                            </label>
                            <g:datePicker name="dateCreated" value="${vehicleInstance?.dateCreated}"  />

                        </div>
                        
                        <div class="prop mandatory ${hasErrors(bean: vehicleInstance, field: 'lastUpdated', 'error')}">
                            <label for="lastUpdated">
                                <g:message code="vehicle.lastUpdated" default="Last Updated" />
                                <span class="indicator">*</span>
                            </label>
                            <g:datePicker name="lastUpdated" value="${vehicleInstance?.lastUpdated}"  />

                        </div>
                        
                    </fieldset>
                </div>
                <div class="buttons">
                    <span class="button"><g:actionSubmit class="save" action="update" value="${message(code: 'update', 'default': 'Update')}" /></span>
                    <span class="button"><g:actionSubmit class="delete" action="delete" value="${message(code: 'delete', 'default': 'Delete')}" onclick="return confirm('${message(code: 'delete.confirm', 'default': 'Are you sure?')}');" /></span>
                </div>
            </g:form>
        </div>
    </body>
</html>

