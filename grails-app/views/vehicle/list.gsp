<%@ page import="com.innovated.iris.domain.Vehicle" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <title><g:message code="vehicle.list" default="Vehicle List" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLinkTo(dir: '')}"><g:message code="home" default="Home" /></a></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="vehicle.new" default="New Vehicle" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="vehicle.list" default="Vehicle List" /></h1>
            <g:if test="${flash.message}">
            <div class="message"><g:message code="${flash.message}" args="${flash.args}" default="${flash.defaultMessage}" /></div>
            </g:if>
            <div class="list">
                <table>
                    <thead>
                        <tr>
                        
                   	    <g:sortableColumn property="id" title="Id" titleKey="vehicle.id" />
                        
                   	    <g:sortableColumn property="assetCode" title="Asset Code" titleKey="vehicle.assetCode" />
                        
                   	    <th><g:message code="vehicle.owner" default="Owner" /></th>
                   	    
                   	    <g:sortableColumn property="acquisitionDate" title="Acquisition Date" titleKey="vehicle.acquisitionDate" />
                        
                   	    <g:sortableColumn property="acquisitionPrice" title="Acquisition Price" titleKey="vehicle.acquisitionPrice" />
                        
                   	    <g:sortableColumn property="disposalDate" title="Disposal Date" titleKey="vehicle.disposalDate" />
                        
                        </tr>
                    </thead>
                    <tbody>
                    <g:each in="${vehicleInstanceList}" status="i" var="vehicleInstance">
                        <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
                        
                            <td><g:link action="show" id="${vehicleInstance.id}">${fieldValue(bean: vehicleInstance, field: "id")}</g:link></td>
                        
                            <td>${fieldValue(bean: vehicleInstance, field: "assetCode")}</td>
                        
                            <td>${fieldValue(bean: vehicleInstance, field: "owner")}</td>
                        
                            <td><g:formatDate date="${vehicleInstance.acquisitionDate}" /></td>
                        
                            <td><g:formatNumber number="${vehicleInstance.acquisitionPrice}" /></td>
                        
                            <td><g:formatDate date="${vehicleInstance.disposalDate}" /></td>
                        
                        </tr>
                    </g:each>
                    </tbody>
                </table>
            </div>
            <div class="paginateButtons">
                <g:paginate total="${vehicleInstanceTotal}" />
            </div>
        </div>
    </body>
</html>
