
<%@ page import="com.innovated.iris.domain.lookup.GstRateParameter" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <title><g:message code="gstRateParameter.list" default="GstRateParameter List" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLinkTo(dir: '')}"><g:message code="home" default="Home" /></a></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="gstRateParameter.new" default="New GstRateParameter" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="gstRateParameter.list" default="GstRateParameter List" /></h1>
            <g:if test="${flash.message}">
            <div class="message"><g:message code="${flash.message}" args="${flash.args}" default="${flash.defaultMessage}" /></div>
            </g:if>
            <div class="list">
                <table>
                    <thead>
                        <tr>
	                   	    <g:sortableColumn property="id" title="ID" titleKey="gstRateParameter.id" />
	                   	    <g:sortableColumn property="dateFrom" title="Date From" titleKey="gstRateParameter.dateFrom" />
	                   	    <g:sortableColumn property="rate" title="Rate" titleKey="gstRateParameter.rate" />
	                   	    <g:sortableColumn property="lastUpdated" title="Last Updated" titleKey="gstRateParameter.lastUpdated" />
	                   	    <g:sortableColumn property="dateCreated" title="Date Created" titleKey="gstRateParameter.dateCreated" />
                        </tr>
                    </thead>
                    <tbody>
                    <g:each in="${gstRateParameterInstanceList}" status="i" var="gstRateParameterInstance">
                        <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
                            <td><g:link action="show" id="${gstRateParameterInstance.id}">${fieldValue(bean: gstRateParameterInstance, field: "id")}</g:link></td>
                            <td><g:formatDate format="d MMM yyyy" date="${gstRateParameterInstance.dateFrom}" /></td>
                            <td>${formatNumber(number:gstRateParameterInstance.rate, type:'percent', minFractionDigits:2, maxFractionDigits:8)}</td>
                            <td><g:formatDate date="${gstRateParameterInstance.lastUpdated}" /></td>
                            <td><g:formatDate date="${gstRateParameterInstance.dateCreated}" /></td>
                        </tr>
                    </g:each>
                    </tbody>
                </table>
            </div>
            <div class="paginateButtons">
                <g:paginate total="${gstRateParameterInstanceTotal}" />
            </div>
        </div>
    </body>
</html>
