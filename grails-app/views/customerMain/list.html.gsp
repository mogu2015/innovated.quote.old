<html>
    <head>
        <meta name="layout" content="prod">
        <title><g:message code="ui.bread.clients" default="Clients" /></title>
        <g:javascript library="jquery/jquery.dataTables.min" />
        <g:javascript library="jquery/dataTables.fnSetFilteringDelay" />
    </head>
	<body>
		<div class="twocol">
			<div id="content">
				<div id="leftcol">
					<p class="bread">
						<g:message code="ui.bread.clients" default="Clients" />
					</p>
					
					<g:if test="${flash.message}">
					<div class="flashMsg">
						<p><g:message code="${flash.message}" args="${flash.args}" default="${flash.defaultMessage}" /></p>
					</div>
					</g:if>
					
					<p>Count = ${count}</p>
					
					<h1 class="contentH1"><g:message code="ui.h1.list.clients" default="Clients" /></h1>
					<g:if test="${customerInstanceList?.size() > 0}">
					<div class="alt_pagination">
						<g:render template="/templates/ui/customer/listastable" />
					</div>
					</g:if>
					<g:else>
					<div class="emptyListPanel">
						<h1><g:link controller="customer" action="create"><g:message code="ui.h1.label.noclients" default="Add a new Client" /></g:link></h1>
						<p><g:message code="ui.p.label.noclients" default="Client records will show here." /></p>
					</div>
					</g:else>
				</div>
				<div id="rightcol">
					<div id="options">
						<div class="optGrpHdr"><h3 class="optHdr">Current Options</h3></div>
						<dl class="optGrpMenu">
							<iris:sidebarItem controller="customer" action="newCompany" icon="building_add.png" titleCode="ui.opt.title.addcompany" msgCode="ui.opt.msg.addcompany" />
							<iris:sidebarItem controller="customer" action="newIndividual" icon="user_add.png" titleCode="ui.opt.title.addindiv" msgCode="ui.opt.msg.addindiv" />
						</dl>
					</div>
				</div>
				<div class="clear"></div>
			</div>
		</div>
	</body>
</html>

