<%@ page import="com.innovated.iris.domain.auth.User" %>
<html>
	<head>
		<meta name='layout' content='prod.login'>
		<title><g:message code="user.register.confirm" default="Confirm Registration" /></title>
	</head>
	
	<body>
		<div id="additional"></div>
		<div id="formWrapper">
			<div id="formCasing">
			<div id="formBody">
				<div class="logoDiv" style="text-align: center;"><img id="irisLogo" src="${resource(dir:'images',file:'iris_logo.png')}" alt="IRIS" title="IRIS :: inNovated Leasing Australia" /></div>
				%{--<h1><g:message code="user.register.confirm" default="Confirm Registration" /></h1>--}%
				<!-- content -->
				<div class="emptyListPanel">
					<p>Congratulations!</p>
					<p class="success"><b>You've successfully registered for online access.</b></p>
		            <p>Your login details have been sent to the email address you provided. Please click on the confirmation link of that email to complete your registration.</p>
		            <p>Thanks,</p>
		            <p>inNovated Client Services</p>
				</div>
				
			</div>
			</div>
			<div id="formFooter"></div>
		</div>
	</body>
</html>

