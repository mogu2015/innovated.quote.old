<%@ page import="com.innovated.iris.domain.auth.User" %>
<html>
	<head>
		<meta name='layout' content='prod.login'>
		<title><g:message code="user.changePassword" default="Change Password" /></title>
		<script type="text/javascript">
			$(document).ready(function(){
				if ($.browser.msie())
					$("#firstName").focus();
				else
			  		$("#firstName").select();
			});
		</script>
		<style type="text/css">
			form dt { width: 140px; }
			form dd { margin-left: 140px; }
			form dd input { width: 120px; }
			form dd input#email, input#jobRole { width: 225px; }
			.formcancel { width: 400px; }
		</style>
	</head>
	
	<body>
		<noscript>
		    <div class="login_js_alert">For this application to work properly you'll need to <strong>enable JavaScript</strong>. <a href="http://www.google.com/support/websearch/bin/answer.py?hl=en&answer=23852">Find out how</a>.</div>
		</noscript>
		<div id="additional"></div>
		<div id="formWrapper">
			<div id="formCasing">
			<div id="formBody">
				<div class="logoDiv" style="text-align: center;"><img id="irisLogo" src="${resource(dir:'images',file:'iris_logo.png')}" alt="IRIS" title="IRIS :: inNovated Leasing Australia" /></div>
				<h1 style="text-align: center;"><g:message code="user.changePassword" default="Change Password" /></h1>
				<g:if test='${flash.message}'>
					<div class="errors">
						<p class="error" style="padding: 0 5px; text-align: center">${flash.message}</p>
					</div>
				</g:if>
				<g:hasErrors bean="${cmd}">
					<div class="errors">
						<g:renderErrors bean="${cmd}" as="list" />
					</div>
				</g:hasErrors>
				
				<div id="loginForm">
					<g:form controller="user" action="updatePassword" method="post" >
					  	<g:hiddenField name="id" value="${cmd?.id}" />
					  	<dl class="inputForm">
							<!-- 
							<dt><label class="${hasErrors(bean: cmd, field: 'passwordOld', 'error')}" for="passwordOld"><g:message code="changePassword.label.passwordOld" default="Old Password" /><span class="indicator">*</span></label></dt>
							<dd><g:passwordField class="input ${hasErrors(bean: cmd, field: 'passwordOld', 'error')}" tabindex="2" name="passwordOld" value="${fieldValue(bean: cmd, field: 'passwordOld')}" /></dd>
							 -->
							<dt><label class="${hasErrors(bean: cmd, field: 'passwordNew', 'error')}" for="passwordNew"><g:message code="changePassword.label.passwordNew" default="New Password" /><span class="indicator">*</span></label></dt>
							<dd><g:passwordField class="input ${hasErrors(bean: cmd, field: 'passwordNew', 'error')}" tabindex="8" name="passwordNew" value="${fieldValue(bean: cmd, field: 'passwordNew')}" /></dd>
                            
                            <dt><label class="${hasErrors(bean: cmd, field: 'passwordConfirm', 'error')}" for="passwordConfirm"><g:message code="registerUserCommand.label.passwordConfirm" default="passwordConfirm" /><span class="indicator">*</span></label></dt>
							<dd><g:passwordField class="input ${hasErrors(bean: cmd, field: 'passwordConfirm', 'error')}" tabindex="9" name="passwordConfirm" value="${fieldValue(bean: cmd, field: 'passwordConfirm')}" /></dd>
                            
                        	<dd class="buttons">
								<button id="btnChangePwd" name="btnChangePwd" class="greybutton" type="submit" tabindex="10">
					       			<span><img width="16" height="16" alt="" src="${resource(dir:'images/skin',file:'user_add.png')}"/>Change</span>
					    		</button>
					    		<span class="formcancel"> or&nbsp;&nbsp;<g:link controller="user" action="show" id="${cmd?.id}">Cancel</g:link></span>
					    	</dd>
						</dl>
				  	</g:form>
				</div>
			</div>
			</div>
			<div id="formFooter"></div>
		</div>
	</body>
</html>
