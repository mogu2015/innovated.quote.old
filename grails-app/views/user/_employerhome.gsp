<html>
    <head>
        <meta name="layout" content="prod">
        <title><g:message code="user.home.welcome" default="Welcome" /></title>
    </head>
	<body>
		<div class="twocol">
			<div id="content">
				<div id="leftcol">
					<p class="bread">
						<g:message code="ui.menu.label.home" default="Home" />
					</p>
					
					<div id="messages"></div>
					<g:if test="${flash.message}">
					<div class="flashMsg">
						<g:if test="${flash.classes}"><p class="${flash.classes}"></g:if><g:else><p></g:else><g:message code="${flash.message}" args="${flash.args}" default="${flash.defaultMessage}" /></p>
					</div>
					</g:if>
					
					<h1 class="contentH1"><g:message code="ui.h1.label.employerbudgetquotes" default="Current Novated Lease Budget Quotes" /></h1>
					<g:if test="${employer}"><h2 style="margin:10px 0;">${employer.name}</h2></g:if>
					
					<g:if test="${quoteList?.size() > 0}">
					<div>
						<g:ifAnyGranted role="ROLE_SUPER,ROLE_ADMIN,ROLE_EMPLOYER">
							<g:render template="/templates/ui/quote/listastableEmployer" />
						</g:ifAnyGranted>
						<g:ifNotGranted role="ROLE_SUPER,ROLE_ADMIN,ROLE_EMPLOYER">
							<p>You don't have sufficient security access to view this page.</p>
						</g:ifNotGranted>
					</div>
					</g:if>
					<g:else>
					<div class="emptyListPanel">
						<h1><g:message code="ui.h1.label.noemployeequotes" default="No Budget Quotes" /></h1>
						<p><g:message code="ui.p.label.noemployeequotes" default="There are currently NO novated budget quotes recorded for any of your employees." /></p>
					</div>
					</g:else>
				</div>
				<div id="rightcol">
					<div id="options">
						<!-- 
						<div class="optGrpHdr"><h3 class="optHdr">What do you want to do?...</h3></div>
						<dl class="optGrpMenu">
							<iris:sidebarItem controller="tools" action="createquote" icon="page_white_add.png" titleCode="ui.opt.title.newquote" msgCode="ui.opt.msg.newquote" />
							<iris:sidebarItem elementId="select-quote" style="display:none;" controller="tools" action="selectquote" icon="page_white_gear.png" titleCode="ui.opt.title.selectquote" msgCode="ui.opt.msg.selectquote" />
						</dl>
						 -->
					</div>
				</div>
				<div class="clear"></div>
			</div>
		</div>
	</body>
</html>

