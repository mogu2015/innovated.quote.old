<%@ page import="com.innovated.iris.domain.auth.User" %>
<html>
<head>
    <meta name='layout' content='prod.login'>
    <title><g:message code="user.register.confirm" default="Confirm Registration" /></title>

    <style type="text/css">
        .content-box {
            margin: 15px 0;
            padding: 5px 15px 18px;
            text-align: center;
            font-family: arial, helvetica, sans-serif;
        }
        p {
            font-size: 16px !important;
        }


    </style>
</head>

<body>
<div id="additional"></div>
<div id="formWrapper">
    <div id="formCasing">
        <div id="formBody">
            <div class="logoDiv" style="text-align: center;"><img id="irisLogo" src="${resource(dir:'images',file:'iris_logo.png')}" alt="IRIS" title="IRIS :: inNovated Leasing Australia" /></div>
            %{--<h1><g:message code="user.register.confirm" default="Confirm Registration" /></h1>--}%
            <!-- content -->
            <div class="content-box">
                <p class="success"><b>Your account has already been confirmed.</b></p>
                <p>Please select <a href="/iris">here</a> to sign in.</p>
                <p>Thanks,</p>
                <p>inNovated Client Services</p>
            </div>
        </div>
    </div>
    <div id="formFooter"></div>
</div>
</body>
</html>

