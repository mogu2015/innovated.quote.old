<%@ page import="com.innovated.iris.domain.auth.User" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <title><g:message code="user.register.welcome" default="Welcome" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLinkTo(dir: '')}"><g:message code="home" default="Home" /></a></span>
        </div>
        <div class="body">
            <h1><g:message code="user.register.welcome" default="Welcome" /></h1>
            <p>Welcome!</p>	
        </div>
    </body>
</html>

