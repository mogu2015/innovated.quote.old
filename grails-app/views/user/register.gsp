<%@ page import="com.innovated.iris.domain.auth.User" %>
<html>
	<head>
		<meta name='layout' content='prod.login'>
		<title><g:message code="user.register" default="User Registration" /></title>
		<script type="text/javascript">
			$(document).ready(function(){
				if ($.browser.msie())
					$("#firstName").focus();
				else
			  		$("#firstName").select();
			});
		</script>
		<style type="text/css">
			form dt { width: 140px; }
			form dd { margin-left: 140px; }
			form dd input { width: 120px; }
			form dd input#email, input#jobRole { width: 225px; }
			.formcancel { width: 400px; }
		</style>
	</head>
	
	<body>
		<noscript>
		    <div class="login_js_alert">For this application to work properly you'll need to <strong>enable JavaScript</strong>. <a href="http://www.google.com/support/websearch/bin/answer.py?hl=en&answer=23852">Find out how</a>.</div>
		</noscript>
		<div id="additional"></div>
		<div id="formWrapper">
			<div id="formCasing">
			<div id="formBody">
				<div class="logoDiv" style="text-align: center;"><img id="irisLogo" src="${resource(dir:'images',file:'iris_logo.png')}" alt="IRIS" title="IRIS :: inNovated Leasing Australia" /></div>
				<h1>New User Registration</h1>
				<!-- <p>Please enter your details to register for access to IRIS:</p> -->
				<g:if test='${flash.message}'>
					<div class="errors">
						<p class="error" style="padding: 0 5px; text-align: center">${flash.message}</p>
					</div>
				</g:if>
				<g:hasErrors bean="${cmd}">
					<div class="errors">
						<g:renderErrors bean="${cmd}" as="list" />
					</div>
				</g:hasErrors>
				
				<div id="loginForm">
					<g:form action="registerUser" method="post" >
					  	<dl class="inputForm">
							<dt><label class="${hasErrors(bean: cmd, field: 'firstName', 'error')}" for="firstName"><g:message code="registerUserCommand.label.firstName" default="firstName" /><span class="indicator">*</span></label></dt>
                            <dd><g:textField id="firstName" class="input ${hasErrors(bean: cmd, field: 'firstName', 'error')}" tabindex="1" name="firstName" value="${fieldValue(bean: cmd, field: 'firstName')}" /></dd>
							
							<dt><label class="${hasErrors(bean: cmd, field: 'lastName', 'error')}" for="lastName"><g:message code="registerUserCommand.label.lastName" default="lastName" /><span class="indicator">*</span></label></dt>
							<dd><g:textField class="input ${hasErrors(bean: cmd, field: 'lastName', 'error')}" tabindex="2" name="lastName" value="${fieldValue(bean: cmd, field: 'lastName')}" /></dd>
							
							<dt><label class="${hasErrors(bean: cmd, field: 'email', 'error')}" for="email"><g:message code="registerUserCommand.label.email" default="email" /><span class="indicator">*</span></label></dt>
							<dd><g:textField class="input ${hasErrors(bean: cmd, field: 'email', 'error')}" tabindex="3" name="email" value="${fieldValue(bean: cmd, field: 'email')}" /></dd>
							
							<dt><label class="${hasErrors(bean: cmd, field: 'mobile', 'error')}" for="mobile"><g:message code="registerUserCommand.label.mobile" default="mobile" /><span class="indicator">*</span></label></dt>
							<dd><g:textField class="input ${hasErrors(bean: cmd, field: 'mobile', 'error')}" tabindex="4" name="mobile" value="${fieldValue(bean: cmd, field: 'mobile')}" /></dd>
                            
                            %{--<dt><label class="${hasErrors(bean: cmd, field: 'employerCode', 'error')}" for="employerCode"><g:message code="registerUserCommand.label.employerCode" default="employerCode" /><span class="indicator">*</span></label></dt>--}%
							%{--<dd><g:textField class="input ${hasErrors(bean: cmd, field: 'employerCode', 'error')}" tabindex="5" name="employerCode" value="${fieldValue(bean: cmd, field: 'employerCode')}" /></dd>--}%
                            %{----}%
                            %{--<dt><label class="${hasErrors(bean: cmd, field: 'jobRole', 'error')}" for="jobRole"><g:message code="registerUserCommand.label.jobRole" default="jobRole" /><span class="indicator">*</span></label></dt>--}%
							%{--<dd><g:textField class="input ${hasErrors(bean: cmd, field: 'jobRole', 'error')}" tabindex="6" name="jobRole" value="${cmd?.jobRole}" /></dd>--}%
                            %{----}%
                            <dt><label class="${hasErrors(bean: cmd, field: 'userName', 'error')}" for="userName"><g:message code="registerUserCommand.label.userName" default="userName" /><span class="indicator">*</span></label></dt>
							<dd><g:textField class="input ${hasErrors(bean: cmd, field: 'userName', 'error')}" tabindex="7" name="userName" value="${fieldValue(bean: cmd, field: 'userName')}" /></dd>
                        
                        	<dt><label class="${hasErrors(bean: cmd, field: 'password', 'error')}" for="password"><g:message code="registerUserCommand.label.password" default="password" /><span class="indicator">*</span></label></dt>
							<dd><g:passwordField class="input ${hasErrors(bean: cmd, field: 'password', 'error')}" tabindex="8" name="password" value="${fieldValue(bean: cmd, field: 'password')}" /></dd>
                            
                            <dt><label class="${hasErrors(bean: cmd, field: 'passwordConfirm', 'error')}" for="passwordConfirm"><g:message code="registerUserCommand.label.passwordConfirm" default="passwordConfirm" /><span class="indicator">*</span></label></dt>
							<dd><g:passwordField class="input ${hasErrors(bean: cmd, field: 'passwordConfirm', 'error')}" tabindex="9" name="passwordConfirm" value="${fieldValue(bean: cmd, field: 'passwordConfirm')}" /></dd>
                            
                        	<dd class="buttons">
								<button id="btnRegisterUser" name="btnRegisterUser" class="greybutton" type="submit" tabindex="10">
					       			<span><img width="16" height="16" alt="" src="${resource(dir:'images/skin',file:'user_add.png')}"/>Register</span>
					    		</button>
					    		<span class="formcancel"> or&nbsp;&nbsp;<g:link controller="login" action="auth">Cancel</g:link></span>
					    	</dd>
						</dl>
				  	</g:form>
				</div>
			</div>
			</div>
			<div id="formFooter"></div>
		</div>
	</body>
</html>
