<%@ page import="com.innovated.iris.domain.auth.User" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <title><g:message code="user.register" default="User Registration" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLinkTo(dir: '')}"><g:message code="home" default="Home" /></a></span>
        </div>
        <div class="body">
            <h1><g:message code="user.register" default="User Registration" /></h1>
            <g:if test="${flash.message}">
            	<div class="message"><g:message code="${flash.message}" args="${flash.args}" default="${flash.defaultMessage}" /></div>
            </g:if>
            <g:hasErrors bean="${cmd}">
				<div class="errors">
					<g:renderErrors bean="${cmd}" as="list" />
				</div>
            </g:hasErrors>
            <g:form action="registerUser" method="post" >
                <div class="dialog">
                    <fieldset>
                        <legend><g:message code="user.register.legend" default="New User Registration"/></legend>
                        
                            <div class="prop mandatory ${hasErrors(bean: cmd, field: 'firstName', 'error')}">
                                <label for="firstName">
                                    <g:message code="registerUserCommand.label.firstName" default="firstName" />
                                    <span class="indicator">*</span>
                                </label>
                                <g:textField name="firstName" value="${fieldValue(bean: cmd, field: 'firstName')}" />
                            </div>
                            <div class="prop mandatory ${hasErrors(bean: cmd, field: 'lastName', 'error')}">
                                <label for="lastName">
                                    <g:message code="registerUserCommand.label.lastName" default="lastName" />
                                    <span class="indicator">*</span>
                                </label>
                                <g:textField name="lastName" value="${fieldValue(bean: cmd, field: 'lastName')}" />
                            </div>
                            <div class="prop mandatory ${hasErrors(bean: cmd, field: 'email', 'error')}">
                                <label for="email">
                                    <g:message code="registerUserCommand.label.email" default="email" />
                                    <span class="indicator">*</span>
                                </label>
                                <g:textField name="email" value="${fieldValue(bean: cmd, field: 'email')}" />
                            </div>
                            <div class="prop mandatory ${hasErrors(bean: cmd, field: 'mobile', 'error')}">
                                <label for="mobile">
                                    <g:message code="registerUserCommand.label.mobile" default="mobile" />
                                    <span class="indicator">*</span>
                                </label>
                                <g:textField name="mobile" value="${fieldValue(bean: cmd, field: 'mobile')}" />
                            </div>
                            
                            <div class="prop mandatory ${hasErrors(bean: cmd, field: 'employerCode', 'error')}">
                                <label for="employerCode">
                                    <g:message code="registerUserCommand.label.employerCode" default="employerCode" />
                                    <span class="indicator">*</span>
                                </label>
                                <g:textField name="employerCode" value="${fieldValue(bean: cmd, field: 'employerCode')}" />
                            </div>
                            <div class="prop mandatory ${hasErrors(bean: cmd, field: 'jobRole', 'error')}">
                                <label for="jobRole">
                                    <g:message code="registerUserCommand.label.jobRole" default="jobRole" />
                                    <span class="indicator">*</span>
                                </label>
                                <g:textField name="jobRole" value="${fieldValue(bean: cmd, field: 'jobRole')}" />
                            </div>
                        
                        	<div class="prop mandatory ${hasErrors(bean: cmd, field: 'userName', 'error')}">
                                <label for="userName">
                                    <g:message code="registerUserCommand.label.userName" default="userName" />
                                    <span class="indicator">*</span>
                                </label>
                                <g:textField name="userName" value="${fieldValue(bean: cmd, field: 'userName')}" />
                            </div>
                            <div class="prop mandatory ${hasErrors(bean: cmd, field: 'password', 'error')}">
                                <label for="password">
                                    <g:message code="registerUserCommand.label.password" default="password" />
                                    <span class="indicator">*</span>
                                </label>
                                <g:passwordField name="password" value="${fieldValue(bean: cmd, field: 'password')}" />
                            </div>
                        	<div class="prop mandatory ${hasErrors(bean: cmd, field: 'passwordConfirm', 'error')}">
                                <label for="passwordConfirm">
                                    <g:message code="registerUserCommand.label.passwordConfirm" default="passwordConfirm" />
                                    <span class="indicator">*</span>
                                </label>
                                <g:passwordField name="passwordConfirm" value="${fieldValue(bean: cmd, field: 'passwordConfirm')}" />
                            </div>
                            
                    </fieldset>
                </div>
                <div class="buttons">
                    <span class="button"><g:submitButton name="create" class="save" value="${message(code: 'register', 'default': 'Register')}" /></span>
                </div>
            </g:form>
        </div>
    </body>
</html>

