
<%@ page import="com.innovated.iris.domain.auth.User" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <title><g:message code="user.create" default="Create User" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLinkTo(dir: '')}"><g:message code="home" default="Home" /></a></span>
            <span class="menuButton"><g:link class="list" action="list"><g:message code="user.list" default="User List" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="user.create" default="Create User" /></h1>
            <g:if test="${flash.message}">
            	<div class="message"><g:message code="${flash.message}" args="${flash.args}" default="${flash.defaultMessage}" /></div>
            </g:if>
            <g:hasErrors bean="${userInstance}">
				<div class="errors">
					<g:renderErrors bean="${userInstance}" as="list" />
				</div>
            </g:hasErrors>
            <g:form action="save" method="post" >
                <div class="dialog">
                    <fieldset>
                        <legend><g:message code="user.create.legend" default="Enter User Details"/></legend>
                        
                            <div class="prop mandatory ${hasErrors(bean: userInstance, field: 'username', 'error')}">
                                <label for="username">
                                    <g:message code="user.username" default="Username" />
                                    <span class="indicator">*</span>
                                </label>
                                <g:textField name="username" value="${fieldValue(bean: userInstance, field: 'username')}" />

                            </div>
                        
                            <div class="prop mandatory ${hasErrors(bean: userInstance, field: 'userRealName', 'error')}">
                                <label for="userRealName">
                                    <g:message code="user.userRealName" default="User Real Name" />
                                    <span class="indicator">*</span>
                                </label>
                                <g:textField name="userRealName" value="${fieldValue(bean: userInstance, field: 'userRealName')}" />

                            </div>
                        
                            <div class="prop mandatory ${hasErrors(bean: userInstance, field: 'passwd', 'error')}">
                                <label for="passwd">
                                    <g:message code="user.passwd" default="Passwd" />
                                    <span class="indicator">*</span>
                                </label>
                                <g:passwordField name="passwd" value="${fieldValue(bean: userInstance, field: 'passwd')}" />

                            </div>
                        
                            <div class="prop mandatory ${hasErrors(bean: userInstance, field: 'enabled', 'error')}">
                                <label for="enabled">
                                    <g:message code="user.enabled" default="Enabled" />
                                    <span class="indicator">*</span>
                                </label>
                                <g:checkBox name="enabled" value="${userInstance?.enabled}" />

                            </div>
                        
                            <div class="prop mandatory ${hasErrors(bean: userInstance, field: 'dateCreated', 'error')}">
                                <label for="dateCreated">
                                    <g:message code="user.dateCreated" default="Date Created" />
                                    <span class="indicator">*</span>
                                </label>
                                <g:datePicker name="dateCreated" value="${userInstance?.dateCreated}"  />

                            </div>
                        
                            <div class="prop mandatory ${hasErrors(bean: userInstance, field: 'lastUpdated', 'error')}">
                                <label for="lastUpdated">
                                    <g:message code="user.lastUpdated" default="Last Updated" />
                                    <span class="indicator">*</span>
                                </label>
                                <g:datePicker name="lastUpdated" value="${userInstance?.lastUpdated}"  />

                            </div>
                        
                            <div class="prop ${hasErrors(bean: userInstance, field: 'customer', 'error')}">
                                <label for="customer">
                                    <g:message code="user.customer" default="Customer" />
                                    
                                </label>
                                <g:select name="customer.id" from="${com.innovated.iris.domain.Customer.list()}" optionKey="id" value="${userInstance?.customer?.id}" noSelection="['null': '']" />

                            </div>
                        
                            <div class="prop ${hasErrors(bean: userInstance, field: 'description', 'error')}">
                                <label for="description">
                                    <g:message code="user.description" default="Description" />
                                    
                                </label>
                                <g:textField name="description" value="${fieldValue(bean: userInstance, field: 'description')}" />

                            </div>
                        
                            <div class="prop ${hasErrors(bean: userInstance, field: 'email', 'error')}">
                                <label for="email">
                                    <g:message code="user.email" default="Email" />
                                    
                                </label>
                                <g:textField name="email" value="${fieldValue(bean: userInstance, field: 'email')}" />

                            </div>
                        
                            <div class="prop mandatory ${hasErrors(bean: userInstance, field: 'emailShow', 'error')}">
                                <label for="emailShow">
                                    <g:message code="user.emailShow" default="Email Show" />
                                    <span class="indicator">*</span>
                                </label>
                                <g:checkBox name="emailShow" value="${userInstance?.emailShow}" />

                            </div>
                        <!--
                            <div class="prop ${hasErrors(bean: userInstance, field: 'pass', 'error')}">
                                <label for="pass">
                                    <g:message code="user.pass" default="Pass" />
                                    
                                </label>
                                <g:textField name="pass" value="${fieldValue(bean: userInstance, field: 'pass')}" />

                            </div>
                        -->
                    </fieldset>
                </div>
                <div class="buttons">
                    <span class="button"><g:submitButton name="create" class="save" value="${message(code: 'create', 'default': 'Create')}" /></span>
                </div>
            </g:form>
        </div>
    </body>
</html>

