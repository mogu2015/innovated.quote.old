
<%@ page import="com.innovated.iris.domain.auth.User" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <title><g:message code="user.list" default="User List" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLinkTo(dir: '')}"><g:message code="home" default="Home" /></a></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="user.new" default="New User" /></g:link></span>
            <span class="menuButton"><g:link class="create" action="register"><g:message code="user.register" default="Register User" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="user.list" default="User List" /></h1>
            <g:if test="${flash.message}">
            <div class="message"><g:message code="${flash.message}" args="${flash.args}" default="${flash.defaultMessage}" /></div>
            </g:if>
            <div class="list">
                <table>
                    <thead>
                        <tr>
                        
                   	    <g:sortableColumn property="id" title="Id" titleKey="user.id" />
                        
                   	    <g:sortableColumn property="username" title="Username" titleKey="user.username" />
                        
                   	    <g:sortableColumn property="userRealName" title="User Real Name" titleKey="user.userRealName" />
                        
                   	    <g:sortableColumn property="passwd" title="Passwd" titleKey="user.passwd" />
                        
                   	    <g:sortableColumn property="enabled" title="Enabled" titleKey="user.enabled" />
                        
                   	    <g:sortableColumn property="dateCreated" title="Date Created" titleKey="user.dateCreated" />
                        
                        </tr>
                    </thead>
                    <tbody>
                    <g:each in="${userInstanceList}" status="i" var="userInstance">
                        <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
                        
                            <td><g:link action="show" id="${userInstance.id}">${fieldValue(bean: userInstance, field: "id")}</g:link></td>
                        
                            <td>${fieldValue(bean: userInstance, field: "username")}</td>
                        
                            <td>${fieldValue(bean: userInstance, field: "userRealName")}</td>
                        
                            <td>${fieldValue(bean: userInstance, field: "passwd")}</td>
                        
                            <td><g:formatBoolean boolean="${userInstance.enabled}" /></td>
                        
                            <td><g:formatDate date="${userInstance.dateCreated}" /></td>
                        
                        </tr>
                    </g:each>
                    </tbody>
                </table>
            </div>
            <div class="paginateButtons">
                <g:paginate total="${userInstanceTotal}" />
            </div>
        </div>
    </body>
</html>
