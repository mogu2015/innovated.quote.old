
<%@ page import="com.innovated.iris.domain.Company" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta name="layout" content="main" />
        <title>Show Company</title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${resource(dir:'')}">Home</a></span>
            <span class="menuButton"><g:link class="list" action="list">Company List</g:link></span>
            <span class="menuButton"><g:link class="create" action="create">New Company</g:link></span>
        </div>
        <div class="body">
            <h1>Show Company</h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <div class="dialog">
                <table>
                    <tbody>
                        <tr class="prop">
                            <td valign="top" class="name">Name:</td>
                            <td valign="top" class="value">${fieldValue(bean:companyInstance, field:'name')}</td>
                        </tr>
                        <tr class="prop">
                            <td valign="top" class="name">Trading Name:</td>
                            <td valign="top" class="value">${fieldValue(bean:companyInstance, field:'tradingName')}</td>
                        </tr>
                        <tr class="prop">
                            <td valign="top" class="name">Abn:</td>
                            <td valign="top" class="value">${fieldValue(bean:companyInstance, field:'abn')}</td>
                        </tr>
                        <tr class="prop">
                            <td valign="top" class="name">Acn:</td>
                            <td valign="top" class="value">${fieldValue(bean:companyInstance, field:'acn')}</td>
                        </tr>
                        <tr class="prop">
                            <td valign="top" class="name">Employer Code:</td>
                            <td valign="top" class="value">${fieldValue(bean:companyInstance, field:'employerCode')}</td>
                        </tr>
                        <tr class="prop">
                            <td valign="top" class="name">Pay Frequency:</td>
                            <td valign="top" class="value">${fieldValue(bean:companyInstance, field:'defaultPayFreq')}</td>
                        </tr>
                        
                        
                        <tr class="prop">
                            <td valign="top" class="name">Addresses:</td>
                            <td  valign="top" style="text-align:left;" class="value">
                                <ul>
                                <g:each var="a" in="${companyInstance.addresses}">
                                    <li><g:link controller="address" action="show" id="${a.id}">${a?.encodeAsHTML()}</g:link></li>
                                </g:each>
                                </ul>
                            </td>
                        </tr>
                        <tr class="prop">
                            <td valign="top" class="name">Phones:</td>
                            <td  valign="top" style="text-align:left;" class="value">
                                <ul>
                                <g:each var="p" in="${companyInstance.phones}">
                                    <li><g:link controller="phone" action="show" id="${p.id}">${p?.encodeAsHTML()}</g:link></li>
                                </g:each>
                                </ul>
                            </td>
                        </tr>
                        <tr class="prop">
                            <td valign="top" class="name">Emails:</td>
                            <td  valign="top" style="text-align:left;" class="value">
                                <ul>
                                <g:each var="e" in="${companyInstance.emails}">
                                    <li><g:link controller="email" action="show" id="${e.id}">${e?.encodeAsHTML()}</g:link></li>
                                </g:each>
                                </ul>
                            </td>
                        </tr>
                        
                        
                        
                        <tr class="prop">
                            <td valign="top" class="name">Valid FBT TV Calc Methods:</td>
                            <td  valign="top" style="text-align:left;" class="value">
                                <ul>
                                <g:each status="i" var="e" in="${taxableValueMap}">
                                	<!--
                                	<li>${e.key.value}</li>
                                	-->
                                	<g:if test="${companyInstance.allowedFbtCalcMethods.contains(e.key.value)}">
                                    <li>${e.key.value}<g:if test="${e.key.value == companyInstance.allowedFbtCalcMethods.get(0)}">&nbsp;<b><i>(Default)</i></b></g:if></li>
                                    </g:if>
                                    
                                </g:each>
                                </ul>
                            </td>
                        </tr>
                        
                        <tr class="prop">
                            <td valign="top" class="name">Current Concession:</td>
                            <td valign="top" class="value">${ currentConcession?.type }</td>
                        </tr>
                        <tr class="prop">
                            <td valign="top" class="name">FBT - Allows ECM?:</td>
                            <td valign="top" class="value">${ ((companyInstance.allowsFbtEcm) ? "Yes" : "No").encodeAsHTML() }</td>
                        </tr>
                        <tr class="prop">
                            <td valign="top" class="name">Can Claim GST?:</td>
                            <td valign="top" class="value">${ ((companyInstance.canClaimGst) ? "Yes" : "No").encodeAsHTML() }</td>
                        </tr>
                        <tr class="prop">
                            <td valign="top" class="name">Parent:</td>
                            <td valign="top" class="value"><g:link controller="company" action="show" id="${companyInstance?.parent?.id}">${companyInstance?.parent?.encodeAsHTML()}</g:link></td>
                        </tr>
                        <tr class="prop">
                            <td valign="top" class="name">Children:</td>
                            <td  valign="top" style="text-align:left;" class="value">
                                <ul>
                                <g:each var="c" in="${companyInstance.children}">
                                    <li><g:link controller="company" action="show" id="${c.id}">${c?.encodeAsHTML()}</g:link></li>
                                </g:each>
                                </ul>
                            </td>
                        </tr>
                        <tr class="prop">
                            <td valign="top" class="name">Contacts:</td>
                            <td  valign="top" style="text-align:left;" class="value">
                                <ul>
                                <g:each var="c" in="${companyInstance.contacts}">
                                    <li><g:link controller="companyContact" action="show" id="${c.id}">${c?.encodeAsHTML()}</g:link></li>
                                </g:each>
                                </ul>
                            </td>
                        </tr>
                        <tr class="prop">
                            <td valign="top" class="name">Date Created:</td>
                            <td valign="top" class="value">${fieldValue(bean:companyInstance, field:'dateCreated')}</td>
                        </tr>
                        <tr class="prop">
                            <td valign="top" class="name">Last Updated:</td>
                            <td valign="top" class="value">${fieldValue(bean:companyInstance, field:'lastUpdated')}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="buttons">
                <g:form>
                    <input type="hidden" name="id" value="${companyInstance?.id}" />
                    <span class="button"><g:actionSubmit class="edit" value="Edit" /></span>
                    <span class="button"><g:actionSubmit class="delete" onclick="return confirm('Are you sure?');" value="Delete" /></span>
                </g:form>
            </div>
        </div>
    </body>
</html>
