<%@

page import="com.innovated.iris.domain.Company"

%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <title><g:message code="company.edit" default="Edit Company" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLinkTo(dir: '')}"><g:message code="home" default="Home" /></a></span>
            <span class="menuButton"><g:link class="list" action="list"><g:message code="company.list" default="Company List" /></g:link></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="company.new" default="New Company" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="company.edit" default="Edit Company" /></h1>
            <g:if test="${flash.message}">
            	<div class="message"><g:message code="${flash.message}" args="${flash.args}" default="${flash.defaultMessage}" /></div>
            </g:if>
            <g:hasErrors bean="${companyInstance}">
				<div class="errors">
					<g:renderErrors bean="${companyInstance}" as="list" />
				</div>
            </g:hasErrors>
            <g:form method="post" >
                <g:hiddenField name="id" value="${companyInstance?.id}" />
                <g:hiddenField name="version" value="${companyInstance?.version}" />
                <div class="dialog">
                    <fieldset>
                        <legend><g:message code="company.edit.legend" default="Update Company Details"/></legend>
                        
                        <div class="prop mandatory ${hasErrors(bean: companyInstance, field: 'name', 'error')}">
                            <label for="name">
                                <g:message code="company.name" default="Name" />
                                <span class="indicator">*</span>
                            </label>
                            <input type="text" id="name" name="name" value="${fieldValue(bean:companyInstance,field:'name')}"/>
                        </div>
                        
                        <div class="prop mandatory ${hasErrors(bean: companyInstance, field: 'employerCode', 'error')}">
                            <label for="employerCode">
                                <g:message code="company.employerCode" default="Employer Code" />
                                <span class="indicator">*</span>
                            </label>
                            <input type="text" id="employerCode" name="employerCode" value="${fieldValue(bean:companyInstance,field:'employerCode')}"/>
                        </div>
                        
                        <div class="prop mandatory ${hasErrors(bean: companyInstance, field: 'defaultPayFreq', 'error')}">
							<label for="defaultPayFreq">
                            	<g:message code="company.defaultPayFreq" default="Pay Freq" />
                                <span class="indicator">*</span>
                            </label>
                            <g:select name="defaultPayFreq.code" from="${com.innovated.iris.util.PayrollFrequency.lookup}" optionKey="key" optionValue="value" value="${companyInstance?.defaultPayFreq?.code}"></g:select>
                        </div>
                        
                        <div class="prop mandatory ${hasErrors(bean: companyInstance, field: 'addresses', 'error')}">
                            <label for="addresses">
                                <g:message code="company.addresses" default="Addresses" />
                                <span class="indicator">*</span>
                            </label>
                            
<ul>
<g:each var="a" in="${companyInstance?.addresses?}">
    <li><g:link controller="address" action="show" id="${a.id}">${a?.encodeAsHTML()}</g:link></li>
</g:each>
</ul>
<g:link controller="address" params="['company.id':companyInstance?.id]" action="create">Add Address</g:link>

                        </div>
                        
                        <div class="prop mandatory ${hasErrors(bean: companyInstance, field: 'phones', 'error')}">
                            <label for="phones">
                                <g:message code="company.phones" default="Phones" />
                                <span class="indicator">*</span>
                            </label>
                            
<ul>
<g:each var="p" in="${companyInstance?.phones?}">
    <li><g:link controller="phone" action="show" id="${p.id}">${p?.encodeAsHTML()}</g:link></li>
</g:each>
</ul>
<g:link controller="phone" params="['company.id':companyInstance?.id]" action="create">Add Phone</g:link>

                        </div>
                        
                        <div class="prop mandatory ${hasErrors(bean: companyInstance, field: 'emails', 'error')}">
                            <label for="emails">
                                <g:message code="company.emails" default="Emails" />
                                <span class="indicator">*</span>
                            </label>
                            
<ul>
<g:each var="e" in="${companyInstance?.emails?}">
    <li><g:link controller="email" action="show" id="${e.id}">${e?.encodeAsHTML()}</g:link></li>
</g:each>
</ul>
<g:link controller="email" params="['company.id':companyInstance?.id]" action="create">Add Email</g:link>

                        </div>
                        
                        <div class="prop mandatory ${hasErrors(bean: companyInstance, field: 'tradingName', 'error')}">
                            <label for="tradingName">
                                <g:message code="company.tradingName" default="Trading Name" />
                                <span class="indicator">*</span>
                            </label>
                            <input type="text" id="tradingName" name="tradingName" value="${fieldValue(bean:companyInstance,field:'tradingName')}"/>
                        </div>
                        
                        <div class="prop ${hasErrors(bean: companyInstance, field: 'abn', 'error')}">
                            <label for="abn">
                                <g:message code="company.abn" default="Abn" />
                                
                            </label>
                            <input type="text" id="abn" name="abn" value="${fieldValue(bean:companyInstance,field:'abn')}"/>
                        </div>
                        
                        <div class="prop ${hasErrors(bean: companyInstance, field: 'acn', 'error')}">
                            <label for="acn">
                                <g:message code="company.acn" default="Acn" />
                                
                            </label>
                            <input type="text" id="acn" name="acn" value="${fieldValue(bean:companyInstance,field:'acn')}"/>
                        </div>
                        
                        <!--
                        <div class="prop ${hasErrors(bean: companyInstance, field: 'allowedFbtCalcMethods', 'error')}">
		                    <label for="allowedFbtCalcMethods">
		                        <g:message code="company.allowedFbtCalcMethods" default="FBT - Allowed TV Types" />
		                    </label>
		                    <g:if test="${taxableValueMap.size() > 0}">
		                    <ul>
		                    <g:each status="i" var="tv" in="${taxableValueMap}">
		                    	<li><g:checkBox name="ALLOWED_TV_${tv.key.key}" value="${tv.value}" />${tv.key.value.encodeAsHTML() + ":" + tv.value}</li>
		                    </g:each>
		                    </ul>
		                    </g:if>
		                </div>
                        
                        <div class="prop ${hasErrors(bean: companyInstance, field: 'allowsFbtEcm', 'error')}">
                            <label for="allowsFbtEcm">
                                <g:message code="company.allowsFbtEcm" default="FBT - Allows ECM?" />
                            </label>
                            <g:checkBox name="allowsFbtEcm" value="${companyInstance.allowsFbtEcm}" />
                        </div>
                        -->
                        
                        <div class="prop ${hasErrors(bean: companyInstance, field: 'canClaimGst', 'error')}">
                            <label for="canClaimGst">
                                <g:message code="company.canClaimGst" default="Can claim GST?" />
                            </label>
                            <g:checkBox name="canClaimGst" value="${companyInstance.canClaimGst}" />
                        </div>
                        
                        <div class="prop ${hasErrors(bean: companyInstance, field: 'parent', 'error')}">
                            <label for="parent">
                                <g:message code="company.parent" default="Parent" />
                                
                            </label>
                            <g:select optionKey="id" from="${com.innovated.iris.domain.Company.findAllByIdNotEqual(companyInstance?.id)}" name="parent.id" value="${companyInstance?.parent?.id}" noSelection="['-1':'']"></g:select>
                        </div>
                        
                        <div class="prop ${hasErrors(bean: companyInstance, field: 'children', 'error')}">
                            <label for="children">
                                <g:message code="company.children" default="Children" />
                                
                            </label>
                            
<ul>
<g:each var="c" in="${companyInstance?.children?}">
    <li><g:link controller="company" action="show" id="${c.id}">${c?.encodeAsHTML()}</g:link></li>
</g:each>
</ul>
<g:link controller="company" params="['parent.id':companyInstance?.id]" action="create">Add Company</g:link>

                        </div>
                        
                        <div class="prop ${hasErrors(bean: companyInstance, field: 'contacts', 'error')}">
                            <label for="contacts">
                                <g:message code="company.contacts" default="Contacts" />
                                
                            </label>
                            
<ul>
<g:each var="c" in="${companyInstance?.contacts?}">
    <li><g:link controller="companyContact" action="show" id="${c.id}">${c?.encodeAsHTML()}</g:link></li>
</g:each>
</ul>
<g:link controller="companyContact" params="['company.id':companyInstance?.id]" action="create">Add CompanyContact</g:link>

                        </div>
                        <!--
                        <div class="prop mandatory ${hasErrors(bean: companyInstance, field: 'dateCreated', 'error')}">
                            <label for="dateCreated">
                                <g:message code="company.dateCreated" default="Date Created" />
                                <span class="indicator">*</span>
                            </label>
                            <g:datePicker name="dateCreated" value="${companyInstance?.dateCreated}" precision="minute" ></g:datePicker>
                        </div>
                        
                        <div class="prop mandatory ${hasErrors(bean: companyInstance, field: 'lastUpdated', 'error')}">
                            <label for="lastUpdated">
                                <g:message code="company.lastUpdated" default="Last Updated" />
                                <span class="indicator">*</span>
                            </label>
                            <g:datePicker name="lastUpdated" value="${companyInstance?.lastUpdated}" precision="minute" ></g:datePicker>
                        </div>
                        -->
                    </fieldset>
                    <fieldset>
						<legend><g:message code="company.fbt.edit.legend" default="Update Company FBT Preferences"/></legend>
							<div class="prop ${hasErrors(bean: companyInstance, field: 'allowsFbtEcm', 'error')}">
		                        <label for="allowsFbtEcm">
		                            <g:message code="company.allowsFbtEcm" default="ECM Preferred?" />
		                        </label>
		                        <g:checkBox name="allowsFbtEcm" value="${companyInstance.allowsFbtEcm}" />
		                    </div>
		                    <div class="prop ${hasErrors(bean: companyInstance, field: 'allowedFbtCalcMethods', 'error')}">
		                        <h3>Preferred Taxable Value Types</h3>
		                        <g:if test="${taxableValueMap.size() > 0}">
		                        <table>
		                        	<tr>
		                        		<td>Allowed?</td>
		                        		<td>Type</td>
		                        		<td>Default</td>
		                        	</tr>
		                        	<g:each status="i" var="tv" in="${taxableValueMap}">
				                    <tr>
		                        		<td><g:checkBox name="ALLOWED_TV_${tv.key.key}" value="${tv.value}" /></td>
		                        		<td>${tv.key.value.encodeAsHTML()}</td>
		                        		<g:if test="${companyInstance.allowedFbtCalcMethods.size() > 0 && tv.key.value == companyInstance.allowedFbtCalcMethods.get(0)}">
		                        		<td><g:radio id="tv_default_${tv.key.key}" name="tv_default" value="${tv.key.key}" checked="checked"/></td>
		                        		</g:if>
		                        		<g:else>
		                        		<td><g:radio id="tv_default_${tv.key.key}" name="tv_default" value="${tv.key.key}"/></td>
		                        		</g:else>
		                        	</tr>
				                    </g:each>
		                        </table>
		                        </g:if>
		                    </div>
		                    <div class="prop ${hasErrors(bean: companyInstance, field: 'concessions', 'error')}">
                                <label for="parent">
                                    <g:message code="company.concessions" default="Current FBT Concession" />
                                </label>
                                <g:select name="current_concession" from="${concessionMap}" optionKey="key" optionValue="value" value="${currentConcession.type.code.encodeAsHTML()}"></g:select>
                            </div>
					</fieldset>
                </div>
                <div class="buttons">
                    <span class="button"><g:actionSubmit class="save" action="update" value="${message(code: 'update', 'default': 'Update')}" /></span>
                    <span class="button"><g:actionSubmit class="delete" action="delete" value="${message(code: 'delete', 'default': 'Delete')}" onclick="return confirm('${message(code: 'delete.confirm', 'default': 'Are you sure?')}');" /></span>
                </div>
            </g:form>
        </div>
    </body>
</html>

