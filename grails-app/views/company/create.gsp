
<%@ page import="com.innovated.iris.domain.Company" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <title><g:message code="company.create" default="Create Company" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLinkTo(dir: '')}"><g:message code="home" default="Home" /></a></span>
            <span class="menuButton"><g:link class="list" action="list"><g:message code="company.list" default="Company List" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="company.create" default="Create Company" /></h1>
            <g:if test="${flash.message}">
            	<div class="message"><g:message code="${flash.message}" args="${flash.args}" default="${flash.defaultMessage}" /></div>
            </g:if>
            <g:hasErrors bean="${companyInstance}">
				<div class="errors">
					<g:renderErrors bean="${companyInstance}" as="list" />
				</div>
            </g:hasErrors>
            <g:form action="save" method="post" >
                <div class="dialog">
                    <fieldset>
                        <legend><g:message code="company.create.legend" default="Enter Company Details"/></legend>
                        
                            <div class="prop mandatory ${hasErrors(bean: companyInstance, field: 'name', 'error')}">
                                <label for="name">
                                    <g:message code="company.name" default="Name" />
                                    <span class="indicator">*</span>
                                </label>
                                <input type="text" id="name" name="name" value="${fieldValue(bean:companyInstance,field:'name')}"/>
                            </div>
                        
                            <div class="prop mandatory ${hasErrors(bean: companyInstance, field: 'tradingName', 'error')}">
                                <label for="tradingName">
                                    <g:message code="company.tradingName" default="Trading Name" />
                                    <span class="indicator">*</span>
                                </label>
                                <input type="text" id="tradingName" name="tradingName" value="${fieldValue(bean:companyInstance,field:'tradingName')}"/>
                            </div>
                        
                            <div class="prop ${hasErrors(bean: companyInstance, field: 'abn', 'error')}">
                                <label for="abn">
                                    <g:message code="company.abn" default="Abn" />
                                    
                                </label>
                                <input type="text" id="abn" name="abn" value="${fieldValue(bean:companyInstance,field:'abn')}"/>
                            </div>
                        
                            <div class="prop ${hasErrors(bean: companyInstance, field: 'acn', 'error')}">
                                <label for="acn">
                                    <g:message code="company.acn" default="Acn" />
                                    
                                </label>
                                <input type="text" id="acn" name="acn" value="${fieldValue(bean:companyInstance,field:'acn')}"/>
                            </div>
                            
                            
		                    <div class="prop ${hasErrors(bean: companyInstance, field: 'canClaimGst', 'error')}">
		                        <label for="canClaimGst">
		                            <g:message code="company.canClaimGst" default="Can claim GST?" />
		                        </label>
		                        <g:checkBox name="canClaimGst" value="${companyInstance.canClaimGst}" />
		                    </div>
                        
                            <div class="prop ${hasErrors(bean: companyInstance, field: 'parent', 'error')}">
                                <label for="parent">
                                    <g:message code="company.parent" default="Parent" />
                                </label>
                                <g:select optionKey="id" from="${com.innovated.iris.domain.Company.list()}" name="parent.id" value="${companyInstance?.parent?.id}" noSelection="['-1':'']"></g:select>
                            </div>
                            
                            <div class="prop ${hasErrors(bean: companyInstance, field: 'employerCode', 'error')}">
                                <label for="employerCode"><g:message code="company.employerCode" default="Employer Code" /></label>
                                <input type="text" id="employerCode" name="employerCode" value="${fieldValue(bean:companyInstance,field:'employerCode')}"/>
                            </div>
                            
                            <div class="prop ${hasErrors(bean: companyInstance, field: 'defaultPayFreq', 'error')}">
                                <label for="defaultPayFreq"><g:message code="company.defaultPayFreq" default="Parent" /></label>
                                <g:select name="defaultPayFreq.code" from="${com.innovated.iris.util.PayrollFrequency.lookup}" optionKey="key" optionValue="value" value="${companyInstance?.defaultPayFreq?.code}"></g:select>
                            </div>
                            
                        	<!--
                            <div class="prop mandatory ${hasErrors(bean: companyInstance, field: 'dateCreated', 'error')}">
                                <label for="dateCreated">
                                    <g:message code="company.dateCreated" default="Date Created" />
                                    <span class="indicator">*</span>
                                </label>
                                <g:datePicker name="dateCreated" value="${companyInstance?.dateCreated}" precision="minute" ></g:datePicker>
                            </div>
                            <div class="prop mandatory ${hasErrors(bean: companyInstance, field: 'lastUpdated', 'error')}">
                                <label for="lastUpdated">
                                    <g:message code="company.lastUpdated" default="Last Updated" />
                                    <span class="indicator">*</span>
                                </label>
                                <g:datePicker name="lastUpdated" value="${companyInstance?.lastUpdated}" precision="minute" ></g:datePicker>
                            </div>
                            -->
                    </fieldset>
                    <fieldset>
						<legend><g:message code="company.fbt.create.legend" default="Company FBT Preferences"/></legend>
							<div class="prop ${hasErrors(bean: companyInstance, field: 'allowsFbtEcm', 'error')}">
		                        <label for="allowsFbtEcm">
		                            <g:message code="company.allowsFbtEcm" default="ECM Preferred?" />
		                        </label>
		                        <g:checkBox name="allowsFbtEcm" value="${companyInstance.allowsFbtEcm}" />
		                    </div>
		                    <div class="prop ${hasErrors(bean: companyInstance, field: 'allowedFbtCalcMethods', 'error')}">
		                        <h3>Preferred Taxable Value Types</h3>
		                        <g:if test="${taxableValueMap.size() > 0}">
		                        <table>
		                        	<tr>
		                        		<td>Allowed?</td>
		                        		<td>Type</td>
		                        		<td>Default</td>
		                        	</tr>
		                        	<g:each status="i" var="tv" in="${taxableValueMap}">
				                    <tr>
		                        		<td><g:checkBox name="ALLOWED_TV_${tv.key.key}" value="${tv.value}" /></td>
		                        		<td>${tv.key.value.encodeAsHTML()}</td>
		                        		<g:if test="${tv.key.value == companyInstance.allowedFbtCalcMethods.get(0)}">
		                        		<td><g:radio id="tv_default_${tv.key.key}" name="tv_default" value="${tv.key.key}" checked="checked"/></td>
		                        		</g:if>
		                        		<g:else>
		                        		<td><g:radio id="tv_default_${tv.key.key}" name="tv_default" value="${tv.key.key}"/></td>
		                        		</g:else>
		                        	</tr>
				                    </g:each>
		                        </table>
		                        </g:if>
		                    </div>
		                    <div class="prop ${hasErrors(bean: companyInstance, field: 'concessions', 'error')}">
                                <label for="parent">
                                    <g:message code="company.concessions" default="Current FBT Concession" />
                                </label>
                                <g:select name="current_concession" from="${concessionMap}" optionKey="key" optionValue="value"></g:select>
                            </div>
					</fieldset>
                </div>
                <div class="buttons">
                    <span class="button"><g:submitButton name="create" class="save" value="${message(code: 'create', 'default': 'Create')}" /></span>
                </div>
            </g:form>
        </div>
    </body>
</html>

