
<%@ page import="com.innovated.iris.domain.enums.AustralianState" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <title><g:message code="australianState.edit" default="Edit AustralianState" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLinkTo(dir: '')}"><g:message code="home" default="Home" /></a></span>
            <span class="menuButton"><g:link class="list" action="list"><g:message code="australianState.list" default="AustralianState List" /></g:link></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="australianState.new" default="New AustralianState" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="australianState.edit" default="Edit AustralianState" /></h1>
            <g:if test="${flash.message}">
            <div class="message"><g:message code="${flash.message}" args="${flash.args}" default="${flash.defaultMessage}" /></div>
            </g:if>
            <g:hasErrors bean="${australianStateInstance}">
            <div class="errors">
                <g:renderErrors bean="${australianStateInstance}" as="list" />
            </div>
            </g:hasErrors>
            <g:form method="post" >
                <g:hiddenField name="id" value="${australianStateInstance?.id}" />
                <g:hiddenField name="version" value="${australianStateInstance?.version}" />
                <div class="dialog">
                    <table>
                        <tbody>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="name"><g:message code="australianState.name" default="Name" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: australianStateInstance, field: 'name', 'errors')}">
                                    <g:textField name="name" value="${fieldValue(bean: australianStateInstance, field: 'name')}" />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="abbreviation"><g:message code="australianState.abbreviation" default="Abbreviation" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: australianStateInstance, field: 'abbreviation', 'errors')}">
                                    <g:textField name="abbreviation" value="${fieldValue(bean: australianStateInstance, field: 'abbreviation')}" />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="capitalCity"><g:message code="australianState.capitalCity" default="Capital City" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: australianStateInstance, field: 'capitalCity', 'errors')}">
                                    <g:textField name="capitalCity" value="${fieldValue(bean: australianStateInstance, field: 'capitalCity')}" />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="dateCreated"><g:message code="australianState.dateCreated" default="Date Created" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: australianStateInstance, field: 'dateCreated', 'errors')}">
                                    <g:datePicker name="dateCreated" value="${australianStateInstance?.dateCreated}"  />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="lastUpdated"><g:message code="australianState.lastUpdated" default="Last Updated" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: australianStateInstance, field: 'lastUpdated', 'errors')}">
                                    <g:datePicker name="lastUpdated" value="${australianStateInstance?.lastUpdated}"  />

                                </td>
                            </tr>
                        
                        </tbody>
                    </table>
                </div>
                <div class="buttons">
                    <span class="button"><g:actionSubmit class="save" action="update" value="${message(code: 'update', 'default': 'Update')}" /></span>
                    <span class="button"><g:actionSubmit class="delete" action="delete" value="${message(code: 'delete', 'default': 'Delete')}" onclick="return confirm('${message(code: 'delete.confirm', 'default': 'Are you sure?')}');" /></span>
                </div>
            </g:form>
        </div>
    </body>
</html>
