
<%@ page import="com.innovated.iris.domain.enums.AustralianState" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <title><g:message code="australianState.list" default="AustralianState List" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLinkTo(dir: '')}"><g:message code="home" default="Home" /></a></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="australianState.new" default="New AustralianState" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="australianState.list" default="AustralianState List" /></h1>
            <g:if test="${flash.message}">
            <div class="message"><g:message code="${flash.message}" args="${flash.args}" default="${flash.defaultMessage}" /></div>
            </g:if>
            <div class="list">
                <table>
                    <thead>
                        <tr>
                        
                   	    <g:sortableColumn property="id" title="Id" titleKey="australianState.id" />
                        
                   	    <g:sortableColumn property="name" title="Name" titleKey="australianState.name" />
                        
                   	    <g:sortableColumn property="abbreviation" title="Abbreviation" titleKey="australianState.abbreviation" />
                        
                   	    <g:sortableColumn property="capitalCity" title="Capital City" titleKey="australianState.capitalCity" />
                        
                   	    <g:sortableColumn property="dateCreated" title="Date Created" titleKey="australianState.dateCreated" />
                        
                   	    <g:sortableColumn property="lastUpdated" title="Last Updated" titleKey="australianState.lastUpdated" />
                        
                        </tr>
                    </thead>
                    <tbody>
                    <g:each in="${australianStateInstanceList}" status="i" var="australianStateInstance">
                        <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
                        
                            <td><g:link action="show" id="${australianStateInstance.id}">${fieldValue(bean: australianStateInstance, field: "id")}</g:link></td>
                        
                            <td>${fieldValue(bean: australianStateInstance, field: "name")}</td>
                        
                            <td>${fieldValue(bean: australianStateInstance, field: "abbreviation")}</td>
                        
                            <td>${fieldValue(bean: australianStateInstance, field: "capitalCity")}</td>
                        
                            <td><g:formatDate date="${australianStateInstance.dateCreated}" /></td>
                        
                            <td><g:formatDate date="${australianStateInstance.lastUpdated}" /></td>
                        
                        </tr>
                    </g:each>
                    </tbody>
                </table>
            </div>
            <div class="paginateButtons">
                <g:paginate total="${australianStateInstanceTotal}" />
            </div>
        </div>
    </body>
</html>
