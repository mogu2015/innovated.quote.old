
<%@ page import="com.innovated.iris.domain.enums.RegistrationUsageType" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <title><g:message code="registrationUsageType.list" default="RegistrationUsageType List" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLinkTo(dir: '')}"><g:message code="home" default="Home" /></a></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="registrationUsageType.new" default="New RegistrationUsageType" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="registrationUsageType.list" default="RegistrationUsageType List" /></h1>
            <g:if test="${flash.message}">
            <div class="message"><g:message code="${flash.message}" args="${flash.args}" default="${flash.defaultMessage}" /></div>
            </g:if>
            <div class="list">
                <table>
                    <thead>
                        <tr>
                        
                   	    <g:sortableColumn property="id" title="Id" titleKey="registrationUsageType.id" />
                        
                   	    <g:sortableColumn property="name" title="Name" titleKey="registrationUsageType.name" />
                        
                   	    <g:sortableColumn property="typeCode" title="Type Code" titleKey="registrationUsageType.typeCode" />
                        
                   	    <g:sortableColumn property="visible" title="Visible" titleKey="registrationUsageType.visible" />
                        
                   	    <g:sortableColumn property="enabled" title="Enabled" titleKey="registrationUsageType.enabled" />
                        
                   	    <g:sortableColumn property="dateCreated" title="Date Created" titleKey="registrationUsageType.dateCreated" />
                        
                        </tr>
                    </thead>
                    <tbody>
                    <g:each in="${registrationUsageTypeInstanceList}" status="i" var="registrationUsageTypeInstance">
                        <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
                        
                            <td><g:link action="show" id="${registrationUsageTypeInstance.id}">${fieldValue(bean: registrationUsageTypeInstance, field: "id")}</g:link></td>
                        
                            <td>${fieldValue(bean: registrationUsageTypeInstance, field: "name")}</td>
                        
                            <td>${fieldValue(bean: registrationUsageTypeInstance, field: "typeCode")}</td>
                        
                            <td><g:formatBoolean boolean="${registrationUsageTypeInstance.visible}" /></td>
                        
                            <td><g:formatBoolean boolean="${registrationUsageTypeInstance.enabled}" /></td>
                        
                            <td><g:formatDate date="${registrationUsageTypeInstance.dateCreated}" /></td>
                        
                        </tr>
                    </g:each>
                    </tbody>
                </table>
            </div>
            <div class="paginateButtons">
                <g:paginate total="${registrationUsageTypeInstanceTotal}" />
            </div>
        </div>
    </body>
</html>
