
<%@ page import="com.innovated.iris.domain.enums.RegistrationUsageType" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <title><g:message code="registrationUsageType.create" default="Create RegistrationUsageType" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLinkTo(dir: '')}"><g:message code="home" default="Home" /></a></span>
            <span class="menuButton"><g:link class="list" action="list"><g:message code="registrationUsageType.list" default="RegistrationUsageType List" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="registrationUsageType.create" default="Create RegistrationUsageType" /></h1>
            <g:if test="${flash.message}">
            	<div class="message"><g:message code="${flash.message}" args="${flash.args}" default="${flash.defaultMessage}" /></div>
            </g:if>
            <g:hasErrors bean="${registrationUsageTypeInstance}">
				<div class="errors">
					<g:renderErrors bean="${registrationUsageTypeInstance}" as="list" />
				</div>
            </g:hasErrors>
            <g:form action="save" method="post" >
                <div class="dialog">
                    <fieldset>
                        <legend><g:message code="registrationUsageType.create.legend" default="Enter RegistrationUsageType Details"/></legend>
                        
                            <div class="prop mandatory ${hasErrors(bean: registrationUsageTypeInstance, field: 'name', 'error')}">
                                <label for="name">
                                    <g:message code="registrationUsageType.name" default="Name" />
                                    <span class="indicator">*</span>
                                </label>
                                <g:textField name="name" value="${fieldValue(bean: registrationUsageTypeInstance, field: 'name')}" />

                            </div>
                        
                            <div class="prop mandatory ${hasErrors(bean: registrationUsageTypeInstance, field: 'typeCode', 'error')}">
                                <label for="typeCode">
                                    <g:message code="registrationUsageType.typeCode" default="Type Code" />
                                    <span class="indicator">*</span>
                                </label>
                                <g:textField name="typeCode" value="${fieldValue(bean: registrationUsageTypeInstance, field: 'typeCode')}" />

                            </div>
                        
                            <div class="prop mandatory ${hasErrors(bean: registrationUsageTypeInstance, field: 'visible', 'error')}">
                                <label for="visible">
                                    <g:message code="registrationUsageType.visible" default="Visible" />
                                    <span class="indicator">*</span>
                                </label>
                                <g:checkBox name="visible" value="${registrationUsageTypeInstance?.visible}" />

                            </div>
                        
                            <div class="prop mandatory ${hasErrors(bean: registrationUsageTypeInstance, field: 'enabled', 'error')}">
                                <label for="enabled">
                                    <g:message code="registrationUsageType.enabled" default="Enabled" />
                                    <span class="indicator">*</span>
                                </label>
                                <g:checkBox name="enabled" value="${registrationUsageTypeInstance?.enabled}" />

                            </div>
                        
                            <div class="prop mandatory ${hasErrors(bean: registrationUsageTypeInstance, field: 'dateCreated', 'error')}">
                                <label for="dateCreated">
                                    <g:message code="registrationUsageType.dateCreated" default="Date Created" />
                                    <span class="indicator">*</span>
                                </label>
                                <g:datePicker name="dateCreated" value="${registrationUsageTypeInstance?.dateCreated}"  />

                            </div>
                        
                            <div class="prop mandatory ${hasErrors(bean: registrationUsageTypeInstance, field: 'lastUpdated', 'error')}">
                                <label for="lastUpdated">
                                    <g:message code="registrationUsageType.lastUpdated" default="Last Updated" />
                                    <span class="indicator">*</span>
                                </label>
                                <g:datePicker name="lastUpdated" value="${registrationUsageTypeInstance?.lastUpdated}"  />

                            </div>
                        
                    </fieldset>
                </div>
                <div class="buttons">
                    <span class="button"><g:submitButton name="create" class="save" value="${message(code: 'create', 'default': 'Create')}" /></span>
                </div>
            </g:form>
        </div>
    </body>
</html>

