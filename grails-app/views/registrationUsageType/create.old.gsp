
<%@ page import="com.innovated.iris.domain.enums.RegistrationUsageType" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <title><g:message code="registrationUsageType.create" default="Create RegistrationUsageType" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLinkTo(dir: '')}"><g:message code="home" default="Home" /></a></span>
            <span class="menuButton"><g:link class="list" action="list"><g:message code="registrationUsageType.list" default="RegistrationUsageType List" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="registrationUsageType.create" default="Create RegistrationUsageType" /></h1>
            <g:if test="${flash.message}">
            	<div class="message"><g:message code="${flash.message}" args="${flash.args}" default="${flash.defaultMessage}" /></div>
            </g:if>
            <g:hasErrors bean="${registrationUsageTypeInstance}">
		        <div class="errors">
		            <g:renderErrors bean="${registrationUsageTypeInstance}" as="list" />
		        </div>
            </g:hasErrors>
            <g:form action="save" method="post" >
                <div class="dialog">
                    <table>
                        <tbody>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="name"><g:message code="registrationUsageType.name" default="Name" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: registrationUsageTypeInstance, field: 'name', 'errors')}">
                                    <g:textField name="name" value="${fieldValue(bean: registrationUsageTypeInstance, field: 'name')}" />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="typeCode"><g:message code="registrationUsageType.typeCode" default="Type Code" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: registrationUsageTypeInstance, field: 'typeCode', 'errors')}">
                                    <g:textField name="typeCode" value="${fieldValue(bean: registrationUsageTypeInstance, field: 'typeCode')}" />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="visible"><g:message code="registrationUsageType.visible" default="Visible" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: registrationUsageTypeInstance, field: 'visible', 'errors')}">
                                    <g:checkBox name="visible" value="${registrationUsageTypeInstance?.visible}" />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="enabled"><g:message code="registrationUsageType.enabled" default="Enabled" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: registrationUsageTypeInstance, field: 'enabled', 'errors')}">
                                    <g:checkBox name="enabled" value="${registrationUsageTypeInstance?.enabled}" />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="dateCreated"><g:message code="registrationUsageType.dateCreated" default="Date Created" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: registrationUsageTypeInstance, field: 'dateCreated', 'errors')}">
                                    <g:datePicker name="dateCreated" value="${registrationUsageTypeInstance?.dateCreated}"  />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="lastUpdated"><g:message code="registrationUsageType.lastUpdated" default="Last Updated" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: registrationUsageTypeInstance, field: 'lastUpdated', 'errors')}">
                                    <g:datePicker name="lastUpdated" value="${registrationUsageTypeInstance?.lastUpdated}"  />

                                </td>
                            </tr>
                        
                        </tbody>
                    </table>
                </div>
                <div class="buttons">
                    <span class="button"><g:submitButton name="create" class="save" value="${message(code: 'create', 'default': 'Create')}" /></span>
                </div>
            </g:form>
        </div>
    </body>
</html>
