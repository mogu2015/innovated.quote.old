
<%@ page import="com.innovated.iris.domain.Email" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta name="layout" content="main" />
        <title>Email List</title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${resource(dir:'')}">Home</a></span>
            <span class="menuButton"><g:link class="create" action="create">New Email</g:link></span>
        </div>
        <div class="body">
            <h1>Email List</h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <div class="list">
                <table>
                    <thead>
                        <tr>
                        
                   	        <g:sortableColumn property="id" title="Id" />
                        
                   	        <g:sortableColumn property="defaultType" title="Default Type" />
                        
                   	        <g:sortableColumn property="address" title="Address" />
                        
                   	        <g:sortableColumn property="allowsHtml" title="Allows Html" />
                        
                   	        <g:sortableColumn property="dateCreated" title="Date Created" />
                        
                   	        <g:sortableColumn property="lastUpdated" title="Last Updated" />
                        
                        </tr>
                    </thead>
                    <tbody>
                    <g:each in="${emailInstanceList}" status="i" var="emailInstance">
                        <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
                        
                            <td><g:link action="show" id="${emailInstance.id}">${fieldValue(bean:emailInstance, field:'id')}</g:link></td>
                        
                            <td>${fieldValue(bean:emailInstance, field:'defaultType')}</td>
                        
                            <td>${fieldValue(bean:emailInstance, field:'address')}</td>
                        
                            <td>${fieldValue(bean:emailInstance, field:'allowsHtml')}</td>
                        
                            <td>${fieldValue(bean:emailInstance, field:'dateCreated')}</td>
                        
                            <td>${fieldValue(bean:emailInstance, field:'lastUpdated')}</td>
                        
                        </tr>
                    </g:each>
                    </tbody>
                </table>
            </div>
            <div class="paginateButtons">
                <g:paginate total="${emailInstanceTotal}" />
            </div>
        </div>
    </body>
</html>
