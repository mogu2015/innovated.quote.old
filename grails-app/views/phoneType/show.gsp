
<%@ page import="com.innovated.iris.domain.enums.PhoneType" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <g:ifAllGranted role="ROLE_SUPER"><meta name="layout" content="super" /></g:ifAllGranted>
        <g:ifNotGranted role="ROLE_SUPER"><meta name="layout" content="main" /></g:ifNotGranted>
        <title>Show PhoneType</title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${resource(dir:'')}">Home</a></span>
            <span class="menuButton"><g:link class="list" action="list">PhoneType List</g:link></span>
            <span class="menuButton"><g:link class="create" action="create">New PhoneType</g:link></span>
        </div>
        <div class="body">
            <h1>Show PhoneType</h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <div class="dialog">
                <table>
                    <tbody>

                    
                        <tr class="prop">
                            <td valign="top" class="name">Id:</td>
                            
                            <td valign="top" class="value">${fieldValue(bean:phoneTypeInstance, field:'id')}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name">Name:</td>
                            
                            <td valign="top" class="value">${fieldValue(bean:phoneTypeInstance, field:'name')}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name">Display Format:</td>
                            
                            <td valign="top" class="value">${fieldValue(bean:phoneTypeInstance, field:'displayFormat')}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name">Visible:</td>
                            
                            <td valign="top" class="value">${fieldValue(bean:phoneTypeInstance, field:'visible')}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name">Enabled:</td>
                            
                            <td valign="top" class="value">${fieldValue(bean:phoneTypeInstance, field:'enabled')}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name">Date Created:</td>
                            
                            <td valign="top" class="value">${fieldValue(bean:phoneTypeInstance, field:'dateCreated')}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name">Last Updated:</td>
                            
                            <td valign="top" class="value">${fieldValue(bean:phoneTypeInstance, field:'lastUpdated')}</td>
                            
                        </tr>
                    
                    </tbody>
                </table>
            </div>
            <div class="buttons">
                <g:form>
                    <input type="hidden" name="id" value="${phoneTypeInstance?.id}" />
                    <span class="button"><g:actionSubmit class="edit" value="Edit" /></span>
                    <span class="button"><g:actionSubmit class="delete" onclick="return confirm('Are you sure?');" value="Delete" /></span>
                </g:form>
            </div>
        </div>
    </body>
</html>
