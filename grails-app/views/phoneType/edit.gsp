
<%@ page import="com.innovated.iris.domain.enums.PhoneType" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <g:ifAllGranted role="ROLE_SUPER"><meta name="layout" content="super" /></g:ifAllGranted>
        <g:ifNotGranted role="ROLE_SUPER"><meta name="layout" content="main" /></g:ifNotGranted>
        <title>Edit PhoneType</title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${resource(dir:'')}">Home</a></span>
            <span class="menuButton"><g:link class="list" action="list">PhoneType List</g:link></span>
            <span class="menuButton"><g:link class="create" action="create">New PhoneType</g:link></span>
        </div>
        <div class="body">
            <h1>Edit PhoneType</h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <g:hasErrors bean="${phoneTypeInstance}">
            <div class="errors">
                <g:renderErrors bean="${phoneTypeInstance}" as="list" />
            </div>
            </g:hasErrors>
            <g:form method="post" >
                <input type="hidden" name="id" value="${phoneTypeInstance?.id}" />
                <input type="hidden" name="version" value="${phoneTypeInstance?.version}" />
                <div class="dialog">
                    <table>
                        <tbody>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="name">Name:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean:phoneTypeInstance,field:'name','errors')}">
                                    <input type="text" id="name" name="name" value="${fieldValue(bean:phoneTypeInstance,field:'name')}"/>
                                </td>
                            </tr> 
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="displayFormat">Display Format:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean:phoneTypeInstance,field:'displayFormat','errors')}">
                                    <input type="text" id="displayFormat" name="displayFormat" value="${fieldValue(bean:phoneTypeInstance,field:'displayFormat')}"/>
                                </td>
                            </tr> 
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="visible">Visible:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean:phoneTypeInstance,field:'visible','errors')}">
                                    <g:checkBox name="visible" value="${phoneTypeInstance?.visible}" ></g:checkBox>
                                </td>
                            </tr> 
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="enabled">Enabled:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean:phoneTypeInstance,field:'enabled','errors')}">
                                    <g:checkBox name="enabled" value="${phoneTypeInstance?.enabled}" ></g:checkBox>
                                </td>
                            </tr> 
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="dateCreated">Date Created:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean:phoneTypeInstance,field:'dateCreated','errors')}">
                                    <g:datePicker name="dateCreated" value="${phoneTypeInstance?.dateCreated}" precision="minute" ></g:datePicker>
                                </td>
                            </tr> 
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="lastUpdated">Last Updated:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean:phoneTypeInstance,field:'lastUpdated','errors')}">
                                    <g:datePicker name="lastUpdated" value="${phoneTypeInstance?.lastUpdated}" precision="minute" ></g:datePicker>
                                </td>
                            </tr> 
                        
                        </tbody>
                    </table>
                </div>
                <div class="buttons">
                    <span class="button"><g:actionSubmit class="save" value="Update" /></span>
                    <span class="button"><g:actionSubmit class="delete" onclick="return confirm('Are you sure?');" value="Delete" /></span>
                </div>
            </g:form>
        </div>
    </body>
</html>
