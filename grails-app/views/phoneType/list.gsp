
<%@ page import="com.innovated.iris.domain.enums.PhoneType" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <g:ifAllGranted role="ROLE_SUPER"><meta name="layout" content="super" /></g:ifAllGranted>
        <g:ifNotGranted role="ROLE_SUPER"><meta name="layout" content="main" /></g:ifNotGranted>
        <title>PhoneType List</title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${resource(dir:'')}">Home</a></span>
            <span class="menuButton"><g:link class="create" action="create">New PhoneType</g:link></span>
        </div>
        <div class="body">
            <h1>PhoneType List</h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <div class="list">
                <table>
                    <thead>
                        <tr>
                        
                   	        <g:sortableColumn property="id" title="Id" />
                        
                   	        <g:sortableColumn property="name" title="Name" />
                        
                   	        <g:sortableColumn property="displayFormat" title="Display Format" />
                        
                   	        <g:sortableColumn property="visible" title="Visible" />
                        
                   	        <g:sortableColumn property="enabled" title="Enabled" />
                        
                   	        <g:sortableColumn property="dateCreated" title="Date Created" />
                        
                        </tr>
                    </thead>
                    <tbody>
                    <g:each in="${phoneTypeInstanceList}" status="i" var="phoneTypeInstance">
                        <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
                        
                            <td><g:link action="show" id="${phoneTypeInstance.id}">${fieldValue(bean:phoneTypeInstance, field:'id')}</g:link></td>
                        
                            <td>${fieldValue(bean:phoneTypeInstance, field:'name')}</td>
                        
                            <td>${fieldValue(bean:phoneTypeInstance, field:'displayFormat')}</td>
                        
                            <td>${fieldValue(bean:phoneTypeInstance, field:'visible')}</td>
                        
                            <td>${fieldValue(bean:phoneTypeInstance, field:'enabled')}</td>
                        
                            <td>${fieldValue(bean:phoneTypeInstance, field:'dateCreated')}</td>
                        
                        </tr>
                    </g:each>
                    </tbody>
                </table>
            </div>
            <div class="paginateButtons">
                <g:paginate total="${phoneTypeInstanceTotal}" />
            </div>
        </div>
    </body>
</html>
