<%@ page import="com.innovated.iris.domain.*" %>
<%@ page import="com.innovated.iris.enums.*" %>
<html>
<head>
    <meta name="layout" content="prod">
    <title><g:message code="ui.menu.label.novatedquote" default="Quote"/></title>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#select-quote a").click(function (e) {
                e.preventDefault();
                // get id of selected quote

                var id = "${quoteInstance?.id}";
                var href = $(this).attr("href");
                var params = {id: id};
                $.post("${createLink(action:'selectquoteAjax')}", params, function (data) {
                    var doSelect = false;
                    if (data.alreadySelected) {
                        if (data.selectedId != id) {
                            var choice = confirm("You already have quote " + data.selectedAqn + " selected as preferred!\n\nDo you want to change this prefernce?")
                            if (choice) {
                                doSelect = true;
                                window.location = href + "/" + id;
                            }
                        }
                        else {
                            alert("You already have quote " + data.selectedAqn + " selected as preferred!");
                        }
                    }
                    else {
                        doSelect = true;
                        var confirmSelect = confirm("Do you confirm to have this quote as the preferred one ?");
                        if(confirmSelect){
                            window.location = href + "/" + id;
                        }
                    }
//                    if (doSelect) {
//                            window.location = href + "/" + id;
//                    }
                });
            });

            $("#accept-quote a").click(function (e) {
                e.preventDefault();
                //var url =
                $('#acceptWindow').modal({
                    autoResize: true,
                    onOpen: function (dialog) {
                        dialog.overlay.fadeIn('fast', function () {
                            dialog.container.slideDown('fast', function () {
                                dialog.data.fadeIn('fast', function () {
                                    dialog.container.find('input').first().focus();
                                });
                            });
                        });
                    },
                    onClose: function (dialog) {
                        dialog.data.fadeOut('fast', function () {
                            dialog.container.slideUp('fast', function () {
                                dialog.overlay.fadeOut('fast', function () {
                                    $.modal.close(); // must call this!
                                });
                            });
                        });
                    }
                });
                return false;
            });
        });
    </script>
</head>

<body>
<div class="twocol">
    <div id="content">
        <div id="leftcol">
            <p class="bread">
                <g:ifAnyGranted role="ROLE_EMPLOYER">
                    <g:link controller="user" action="home"><g:message code="ui.menu.label.home"
                                                                       default="Home"/></g:link>
                    <g:if test="${quoteInstance}">
                        <span class="breadArrow">&nbsp;</span>
                        <g:formatNumber number="${quoteInstance?.id}" format="${message(code: 'quote.number.mask')}"/>
                    </g:if>
                </g:ifAnyGranted>
                <g:ifNotGranted role="ROLE_EMPLOYER">
                    <!-- <g:link controller="tools"><g:message code="ui.menu.label.tools" default="Tools"/></g:link> -->
                    <g:message code="ui.menu.label.tools" default="Tools"/>
                    <span class="breadArrow">&nbsp;</span>
                    <g:link controller="tools" action="quote"><g:message code="ui.menu.label.novatedquote"
                                                                         default="Quote"/></g:link>
                    <g:if test="${quoteInstance}">
                        <span class="breadArrow">&nbsp;</span>
                        <g:formatNumber number="${quoteInstance?.id}" format="${message(code: 'quote.number.mask')}"/>
                    </g:if>
                </g:ifNotGranted>
            </p>

            <g:if test="${flash.message}">
                <div class="flashMsg">
                    <g:if test="${flash.classes}"><p class="${flash.classes}"></g:if><g:else><p></g:else><g:message
                        code="${flash.message}" args="${flash.args}" default="${flash.defaultMessage}"/></p>
                </div>
            </g:if>

            <g:hasErrors bean="${quoteInstance}">
                <div class="errors">
                    <ul>
                        <g:eachError bean="${quoteInstance}">
                            <li><g:message error="${it}"/></li>
                        </g:eachError>
                    </ul>
                </div>
            </g:hasErrors>

            <h1 class="contentH1"><g:message code="ui.h1.label.viewquote" default="View Quote"/> <g:formatNumber
                    number="${quoteInstance?.id}" format="${message(code: 'quote.number.mask')}"/></h1>
            <!-- <p>Quotation created: <b><g:formatDate date="${quoteInstance?.dateCreated}"
                                                        format="d MMM yyyy"/></b>.</p> -->
            <p>
                Quotation Status: <b>${quoteInstance?.status.getName()}</b>
                <g:render template="/templates/ui/quote/quotestatus" bean="${quoteInstance}" var="quote"/>
            </p>

            <div class="formBg">
                <div class="formWrap">
                    <g:render template="/templates/ui/quote/tabs"/>
                </div>
            </div>

        </div> <!-- #contentMain -->
        <div id="rightcol">
            <div id="options">
                <g:ifAnyGranted role="ROLE_SUPER,ROLE_ADMIN">
                    <div class="optGrpHdr"><h3 class="optHdr">Quote Tasks - ADMIN</h3></div>
                    <dl class="optGrpMenu">
                        <iris:sidebarItem controller="quote" action="edit" id="${quoteInstance?.id}"
                                          icon="page_white_edit.png" titleCode="ui.opt.title.editquote"
                                          msgCode="ui.opt.msg.editquote"/>
                    </dl>
                </g:ifAnyGranted>
                <div class="optGrpHdr"><h3 class="optHdr">Current Options</h3></div>
                <dl class="optGrpMenu">
                    <g:ifAnyGranted role="ROLE_SUPER,ROLE_ADMIN,ROLE_EMPLOYER">
                        <g:if test="${quoteInstance?.status == QuoteStatus.QUOTE_ACCEPTED}">
                            <iris:sidebarItem controller="tools" action="approvequote" id="${quoteInstance?.id}"
                                              icon="accept.png" titleCode="ui.opt.title.employerapprove"
                                              msgCode="ui.opt.msg.employerapprove"/>
                        </g:if>
                    </g:ifAnyGranted>
                    <g:ifAnyGranted role="ROLE_EMPLOYER">
                        <iris:sidebarItem controller="user" action="home" icon="page_white_stack.png"
                                          titleCode="ui.opt.title.listquotesemployer"
                                          msgCode="ui.opt.msg.listquotesemployer"/>
                    </g:ifAnyGranted>
                    <g:ifAnyGranted role="ROLE_SUPER,ROLE_ADMIN,ROLE_USER">
                        <g:if test="${quoteInstance?.status == QuoteStatus.QUOTE_CREATED}">
                            <iris:sidebarItem elementId="select-quote" controller="tools" action="selectquote"
                                              icon="page_white_gear.png" titleCode="ui.opt.title.selectquote"
                                              msgCode="ui.opt.msg.selectquote"/>
                        </g:if>
                        <!-- <g:if
                            test="${quoteInstance?.status == QuoteStatus.QUOTE_CONFIRMED && quoteInstance?.pricingConfirmed != null}">
                        <iris:sidebarItem elementId="accept-quote" controller="quote" action="accept"
                                          icon="page_white_go.png" titleCode="ui.opt.title.acceptquote"
                                          msgCode="ui.opt.msg.acceptquote"/>
                    </g:if> -->
                        <iris:sidebarItem controller="tools" action="quote" icon="page_white_stack.png"
                                          titleCode="ui.opt.title.listquotes" msgCode="ui.opt.msg.listquotes"/>
                        <iris:sidebarItem controller="tools" action="createquote" icon="page_white_add.png"
                                          titleCode="ui.opt.title.newquote" msgCode="ui.opt.msg.newquote"/>
                    </g:ifAnyGranted>
                </dl>
            </div>
        </div>

        <div class="clear"></div>
    </div>
</div>
<!-- "Accept Quote" window/form -->
<div id="acceptWindow" style="display:none;">
    <span class="window-title"></span>

    <h3>Add a new address</h3>

    <div id="acceptMsg"></div>

    <div class="formSet">
        <div class="clearfix mandatory">
            <label for="street"><g:message code="address.street" default="Street"/></label>
            <g:textField id="street" class="short" name="street" value=""/>
        </div>

        <div class="clearfix mandatory">
            <label for="suburb"><g:message code="address.suburb" default="Suburb"/></label>
            <g:textField id="suburb" class="short" name="suburb" value=""/>
        </div>
    </div>

    <button id="btnCreate" name="btnCreate" class="greybutton" type="submit">
        <span><img width="16" height="16" alt=""
                   src="${resource(dir: 'images/skin', file: 'page_white_go.png')}"/>Accept</span>
    </button>
    <span class="formcancel">or&nbsp;&nbsp;<a class="simplemodal-close" href="#closeAccept"><g:message code="cancel"
                                                                                                       default="cancel"/></a>
    </span>

</div>

</body>
</html>

