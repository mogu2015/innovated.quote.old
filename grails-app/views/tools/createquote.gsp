<%@
page import="java.util.Calendar" page import="com.innovated.iris.domain.enums.*" %>
<html>
<head>
    <meta name="layout" content="prod">
    <title><g:message code="ui.menu.label.novatedquote" default="Quote"/></title>

    <link rel="stylesheet" href="${resource(dir: 'css', file: 'iris.css')}"/>

    <script type="text/javascript">
        function toggleDivs(fadeout, fadein) {
            $(fadeout).fadeOut('fast', function () {
                $(fadein).fadeIn('fast');
            });
        }

        function handleOpCostChange(evt) {
            var tgt = $(evt.target);
            //alert("click target: " + tgt.attr("name") + ", value=" + tgt.val());
            if (tgt.val() == 1)
                tgt.parent().next(".showHide").show();
            else
                tgt.parent().next(".showHide").hide();
        }

        function selectClientMessage() {
            alert("Please select a client first from the drop-down list above.");
            return false;
        }

        function autoCompleteSource(elId, request, response) {
            var makeId = elId.split("Select")[0] + "Make";
            var modelId = elId.split("Select")[0] + "Model";
            if ($(makeId).val() == "null") {
                alert("Please select the Vehicle Make first!");
                $(elId).val("");
                response({});
                return;
            }
            else {
                request.makeId = $(makeId).val();
                request.modelId = $(modelId).val();
                request.newUsed = $("#newUsedOption").val();
                request.variant = $("#vehicleVariant").find("option:selected").text();
                $.post(
                        "${createLink(controller:'makeModel', action:'listAjax')}", request,
                        function (data, status, xhr) {
                            if (data.length == 0) $(elId).parent().addClass('error');
                            else $(elId).parent().removeClass('error');
                            response(data);
                        }, "json"
                );
            }
        }
        function autoCompleteSelect(elId, event, ui) {
            $('#vehicle_val').val(ui.item.id);
//            var currentDate = new Date();
            var displayPrice = $("#newUsedOption").val() == 1 ? ui.item.rrp : 0;
            if($("#newUsedOption").val() == 1){
                $("#rrpField").show();
                $("#ownPriceLink").show();
                $(".ownPriceSection").hide();
                $("#cancelOwnPriceLink").hide();
            }else{
                $(".ownPriceSection").show();
                $("#rrpField").hide();
                $("#ownPriceLink").hide();
                $("#cancelOwnPriceLink").hide();
            }
            $("#indicativeRrp").val(displayPrice);
            $("#indicativeRrpDisplay").val(displayPrice).formatCurrency(({symbol: "\$ ", roundToDecimalPlace: 0}));
            $("[name='vehicleYear']").val(ui.item.year);
            // pre-select body & trans if we can!
            $.post(
                    "${createLink(controller:'makeModel', action:'getModelAjax')}", {"id": ui.item.id},
                    function (data, status, xhr) {
                        if (data.model && data.model != "") {
                            $("#quoteNewCarModel").val(data.model.id);
                        }
                    },
                    "json"
            );
        }
        function matchTransmission(abbrTransmission) {
            if ("Automatic".indexOf(abbrTransmission) != -1) {
                return "Automatic";
            } else if ("Manual".indexOf(abbrTransmission) != -1) {
                return "Manual";
            } else if ("CVT".indexOf(abbrTransmission) != -1) {
                return "CVT";
            } else if ("Sequential".indexOf(abbrTransmission) != -1) {
                return "Sequential";
            } else {
                return "Other";
            }
        }
        function autoCompleteRender(elId, ul, item) {
            var $mkp = $("<li></li>").data("item.autocomplete", item);
            var label = "<a><b>" + item.label + "</b></a>";
            var currentDate = new Date();
            if (elId == "#quoteNewCarSelect") {
                var rrp = Highcharts.numberFormat(item.rrp, 0);
                if ($("#newUsedOption").val() == 1) {
                    label = "<a><b>" + item.label + "</b> (" + item.year + ")<br/>Indicative RRP: <i>$" + rrp + "</i></a>";
                } else {
                    label = "<a><b>" + item.label + "</b> (" + item.year + ")<br/><i>(Used Vehicle)</i></a>";
                }

            }
            return $mkp.append(label).appendTo(ul);
            //return $("<li></li>").data("item.autocomplete", item).append("<a><b>" + item.label + "</b><br/>Indicative RRP: <i>$" + rrp + "</i></a>").appendTo(ul);
        }

        function searchEmployerHandler() {
            $("#enterEmployerInfo").hide();
            $("#searchEmployerInfo").show();
            $("#employerEnable").val("YES");
            $("#selectPayFreq").val("null");
        }

        function cancelSearchHandler() {
            $("#searchEmployerInfo").hide();
            $("#enterEmployerInfo").show();
            $("#employerEnable").val("NO");
            $("#selectPayFreq").val("null");
        }

        function inputOwnPriceHandler(){
            $("#rrpField").hide();
            $(".ownPriceSection").show();
            $("#ownPriceLink").hide();
            $("#cancelOwnPriceLink").show();
            $("#priceObtained").val("true");
            $("#indicativeRrp").val($("#ownPriceDisplay").asNumber());
        }

        function calcelOwnPriceHandler(){
            $("#rrpField").show();
            $(".ownPriceSection").hide();
            $("#ownPriceLink").show();
            $("#cancelOwnPriceLink").hide();
            $("#priceObtained").val("false");
            $("#indicativeRrp").val($("#indicativeRrpDisplay").asNumber());
        }

        $(document).ready(function () {
            $("#btnLogin").click(function (e) {
                // show a load mask...
                $("#body").mask("Loading...");
            });

            $("#grossAnnualSalaryDisplay").blur(function () {
                $("#grossAnnualSalaryDisplay").formatCurrency({symbol: "\$ "});
                var originalVal = $("#grossAnnualSalaryDisplay").asNumber();
                $("#grossAnnualSalary").val(originalVal);
            });
            $("#indicativeRrpDisplay").blur(function () {
                $("#indicativeRrpDisplay").formatCurrency({symbol: "\$ ", roundToDecimalPlace: 0});
                var originalVal = $("#indicativeRrpDisplay").asNumber();
                $("#indicativeRrp").val(originalVal);
            });

            $("#ownPriceDisplay").blur(function () {
                $("#ownPriceDisplay").formatCurrency({symbol: "\$ ", roundToDecimalPlace: 0});
                var originalVal = $("#ownPriceDisplay").asNumber();
                $("#indicativeRrp").val(originalVal);
            });

            $("#kmPerYear").blur(function () {
                $("#kmPerYear").formatCurrency({symbol: "", roundToDecimalPlace: 0});
            });

            $("#quoteNewCarMake").change(function () {
                $("#quoteNewCarSelect").val("");
                $("#vehicle_val").val("");

                $("#quoteNewCarModel").empty();
                $("#vehicleVariant").empty().append("<option value='-1'>Select vehicle type...</option>");

                var makeId = $("#quoteNewCarMake").val();
                $.post(
                        "${createLink(controller:'vehicleModel', action:'getModelAjax')}", {
                            'id': makeId
                        },
                        function (data, status, xhr) {
                            if (data.length > 0) {
                                $("#quoteNewCarModel").append("<option value=''>Select vehicle model...</option>");
                                $.each(data, function (i, n) {
                                    $("#quoteNewCarModel").append("<option value='" + n.id + "'>" + n.name + "</option>");
                                });
//                                $("#quoteNewCarModel").trigger("change");
                            }
                        }, "json"
                );
            });
            $("#quoteNewCarModel").change(function () {
                $("#vehicleVariant").empty();
                $.post(
                        "${createLink(controller:'vehicleModel', action:'getVariantByModelAjax')}", {
                            'id': $("#quoteNewCarModel").val()
                        },
                        function (data, status, xhr) {
                            if (data.length > 0) {
                                $("#vehicleVariant").append("<option value=''>Select vehicle type...</option>");
                                $.each(data, function (i, n) {
                                    $("#vehicleVariant").append("<option value='" + n + "'>" + n + "</option>");
                                });
//                                $("#vehicleVariant").trigger("change");
                            } else {
                                $("#vehicleVariant").trigger("change");
                            }
                        }, "json"
                );

            });
            $("#vehicleVariant").change(function () {
                $("#quoteNewCarSelect").val("");
                var makeValue = $("#quoteNewCarMake").find("option:selected").text();
                var modelValue = $("#quoteNewCarModel").find("option:selected").text();
                var variant = $("#vehicleVariant").find("option:selected").text();
                $("#quoteNewCarSelect").autocomplete("search", makeValue + " " + modelValue + " " + variant);
            });
            $("#quoteUsedCarMake").change(function () {
                $("#quoteUsedCarSelect").val("");
                $("#vehicle_val").val("");
            });

            $("#quoteNewCarSelect").autocomplete({
                delay: 300, //minLength: 1,
                source: function (request, response) {
                    autoCompleteSource("#quoteNewCarSelect", request, response);
                },
                select: function (event, ui) {
                    autoCompleteSelect("#quoteNewCarSelect", event, ui);
                }
            }).data("autocomplete")._renderItem = function (ul, item) {
                return autoCompleteRender("#quoteNewCarSelect", ul, item);
            };

            var employerSearch = $("#employerSearch");
            if (employerSearch.length > 0) {
                $("#employerSearch").autocomplete({
                    delay: 300,
                    source: function (request, response) {
                        $.post(
                                "${createLink(controller:'company', action:'listAjax')}", request,
                                function (data, status, xhr) {
                                    if (data.length == 0) $("#employerSearch").parent().addClass('error');
                                    else $("#employerSearch").parent().removeClass('error');
                                    response(data);
                                }, "json"
                        );
                    },
                    select: function (event, ui) {
                        $('#employerSearch').val(ui.item.tradingName);
                        $('#employerId').val(ui.item.id);
                        $('#selectPayFreq').val(ui.item.payCyle);
                    }
                }).data("autocomplete")._renderItem = function (ul, item) {
                    var $mkp = $("<li></li>").data("item.autocomplete", item);
                    var label = "<a><b>" + item.tradingName + "</b></a>";

                    return $mkp.append(label).appendTo(ul);
                };
            }

            $("#newUsed input[type='radio']").click(function () {
                $("#newUsedOption").val($(this).val());
                // clear any selections
                if ($("#quoteNewCarMake").val() != "null" && $("#quoteNewCarMake").val() != "") {
                    $("#quoteNewCarSelect").val("");
                    $("#quoteNewCarMake").val("");
                    $("#quoteNewCarModel").empty().append("<option value='-1'>Select vehicle model...</option>");
                    $("#vehicleVariant").empty().append("<option value='-1'>Select vehicle type...</option>");
                    $("#vehicle_val").val("");
                }

            });

            <g:if test="${enableUsed}">
            $("#quoteUsedCarSelect").autocomplete({
                delay: 300, //minLength: 1,
                source: function (request, response) {
                    autoCompleteSource("#quoteUsedCarSelect", request, response);
                },
                select: function (event, ui) {
                    autoCompleteSelect("#quoteUsedCarSelect", event, ui);
                }
            }).data("autocomplete")._renderItem = function (ul, item) {
                return autoCompleteRender("#quoteUsedCarSelect", ul, item);
            };

            // handle new/used selection changes
            $("#newUsed input[type='radio']").click(function () {
                //toggle the visibility of the new/used vehicle selection div's
                if ($(this).val() == 1) {	// show new - hide used
//                    toggleDivs("#vehicleSelectUsed", "#vehicleSelectNew");
                }
                else {		// show used - hide new
//                    toggleDivs("#vehicleSelectNew", "#vehicleSelectUsed");
                }
                // clear any selections
                $("#quoteNewCarSelect").val("");
                $("#quoteUsedCarSelect").val("");
                $("#vehicle_val").val("");
            });
            </g:if>

            $("#missingVehicle").click(function () {
                //alert("missing vehicle clicked");
                return false;
            });

            $("#addAddress").click(function () {
                if ($("#customerId").val() == "null") {
                    return selectClientMessage();
                }
                else {
                    // clear the input fields first!
                    $("#street").val("");
                    $("#suburb").val("");
                    $("#postcode").val("");
                    $("#contentMain").hide();
                    $("#changeAddress").show();
                }
            });
            $("#addAddressCancel").click(function () {
                $("#changeAddress").hide();
                $("#contentMain").show();
            });
            $("#btnAddAddress").click(function () {
                //alert('save clicked');
                var customerId = $("#customerId").val();
                var type = $("#addressTypeId").val();
                var street = $("#street").val();
                var suburb = $("#suburb").val();
                var state = $("#stateId").val();
                var postcode = $("#postcode").val();

                if (street == '' || suburb == '' || state == '' || postcode == '') {
                    alert("All address fields are required.");
                    return false;
                }
                else {
                    $.post(
                            "${createLink(controller:'address', action:'saveAjax')}", {
                                'entity.id':${(quoteInstance?.customer) ? quoteInstance?.customer?.entity?.id : 'null'},
                                'customer.id': customerId, 'addressType.id': type, street: street,
                                suburb: suburb, 'state.id': state, postcode: postcode
                            },
                            function (data, status, xhr) {
                                if (data.success) {
                                    // add new address to drop-down and show it!
                                    $('#addressSelect').append($("<option></option>").attr("value", data.addr.id).text(data.addrStr));
                                    $('#addressSelect option:last').attr("selected", "selected");
                                    $("#changeAddress").hide();
                                    $("#contentMain").show();
                                }
                                else {
                                    alert("All address fields are required. Please try again...");
                                }
                            }, "json"
                    );
                }
            });

            // setup opcost radio button change, and trigger it on initial page load!
            $("input[name=hasLogBook]").click(function (e) {
                handleOpCostChange(e);
            });

            <g:ifAnyGranted role="ROLE_SUPER,ROLE_ADMIN">
            // update fields on customer selection change...
            var salaryCache = {};
            $("#customerId").change(function () {
                var cin = ($(this).val() == "null") ? null : $(this).val();
                if (cin) {
                    $.post(
                            "${createLink(controller:'customer', action:'personalDetailsAjax')}", {id: cin},
                            function (data, status, xhr) {
                                if (data.success) {
                                    var updateMap = {
                                        '#addressSelect': data.addresses,
                                        '#mobileSelect': data.mobiles,
                                        '#emailSelect': data.emails,
                                        '#jobSelect': data.jobs
                                    }
                                    salaryCache = data.salaries;
                                    $.each(updateMap, function (k, v) {
                                        $(k + ' option:not(:first)').remove();	//remove all options first!
                                        $.each(v, function (idx, val) {
                                            $(k).append($("<option></option>").attr("value", val.value).text(val.text));
                                            $(k + ' option:first').next().attr("selected", "selected");
                                            if (k == '#jobSelect') {
                                                $(k).trigger("change");
                                            }
                                        });
                                    });
                                }
                                else {
                                    alert(data.msg);
                                }
                            }, "json"
                    );
                }
            });
//            $("#jobSelect").change(function () {
//                var jobId = $(this).val();
//                $.each(salaryCache, function (idx, val) {
//                    if (jobId == val.value) {
//                        $("#grossAnnualSalary").val(val.text);
//                    }
//                });
//            });
            </g:ifAnyGranted>
        });
    </script>
</head>

<body>
<div class="twocol">
    <div id="content">
        <div id="leftcol">
            <div id="contentMain">
                <p class="bread">
                    <!-- <g:link controller="tools"><g:message code="ui.menu.label.tools" default="Tools"/></g:link> -->
                    <g:message code="ui.menu.label.tools" default="Tools"/>
                    <span class="breadArrow">&nbsp;</span>
                    <g:link controller="tools" action="quote"><g:message code="ui.menu.label.novatedquote"
                                                                         default="Quote"/></g:link>
                    <span class="breadArrow">&nbsp;</span>
                    <g:message code="ui.menu.label.createquote" default="Get a new quote"/>
                </p>

                <g:if test="${flash.message}">
                    <div class="flashMsg">
                        <p><g:message code="${flash.message}" args="${flash.args}"
                                      default="${flash.defaultMessage}"/></p>
                    </div>
                </g:if>
                <g:hasErrors bean="${quoteInstance}">
                    <div class="errors">
                        <ul>
                            <g:eachError bean="${quoteInstance}">
                                <li><g:message error="${it}"/></li>
                            </g:eachError>
                        </ul>
                    </div>
                </g:hasErrors>

                <h1 class="contentH1"><g:message code="ui.h1.label.createquote"
                                                 default="Obtain Novated Lease Budget Quote"/></h1>

                <div class="formBg">
                    <g class="formWrap">
                        <g:form controller="tools" action="savequote" method="post">

                            <g:ifAnyGranted role="ROLE_SUPER,ROLE_ADMIN">
                                <h3>Quotation Details</h3>

                                <p>For admin/staff users, the intended client must be selected, as well as the date at which the quote will be generated.</p>

                                <div class="formSet">
                                    <div class="clearfix ${hasErrors(bean: quoteInstance, field: 'customer', 'error')}">
                                        <label for="fromDate"><g:message code="quote.fromDate"
                                                                         default="Quote as at"/></label>
                                        <g:datePicker name="fromDate" value="${quoteInstance?.fromDate}" precision="day"
                                                      years="${yearRange}" tabindex="1"/>
                                    </div>

                                    <div class="clearfix ${hasErrors(bean: quoteInstance, field: 'customer', 'error')}">
                                        <label for="customer"><g:message code="quote.customer"
                                                                         default="For Client"/></label>
                                        <g:select id="customerId" name="customer.id" class="long" tabindex="2"
                                                  from="${clients}" optionKey="id"
                                                  value="${quoteInstance?.customer?.id}"
                                                  noSelection="${['null': 'Select a client for this quote...']}"/>
                                    </div>
                                </div>
                            </g:ifAnyGranted>
                            <g:ifNotGranted role="ROLE_SUPER,ROLE_ADMIN">
                                <g:hiddenField name="fromDate" value="date.struct"/>
                                <g:hiddenField name="fromDate_year"
                                               value="${quoteInstance?.fromDate.getAt(Calendar.YEAR)}"/>
                                <g:hiddenField name="fromDate_month"
                                               value="${quoteInstance?.fromDate.getAt(Calendar.MONTH) + 1}"/>
                                <g:hiddenField name="fromDate_day"
                                               value="${quoteInstance?.fromDate.getAt(Calendar.DATE)}"/>
                                <g:hiddenField name="fromDate_minute" value="0"/>
                                <g:hiddenField name="fromDate_second" value="0"/>
                                <g:hiddenField id="customerId" name="customer.id"
                                               value="${quoteInstance?.customer?.id}"/>
                            </g:ifNotGranted>

                            <h3>Personal Details</h3>

                            <p>Please enter / update your personal details.</p>

                            <div class="formSet">
                                <div class="clearfix mandatory ${hasErrors(bean: quoteInstance, field: 'address', 'error')}">
                                    <label for="address"><g:message code="quote.address" default="Address"/></label>
                                    <g:select id="addressSelect" name="address.id" class="long" tabindex="3"
                                              from="${addresses}" optionKey="id" value="${quoteInstance?.address?.id}"
                                              noSelection="${['null': 'Select your street address...']}"/> (<a
                                        id="addAddress" class="fieldLinkRight" href="#"
                                        tabindex="0">add a new address</a>)
                                </div>

                                <div class="clearfix ${hasErrors(bean: quoteInstance, field: 'email', 'error')}">
                                    <label for="email.id"><g:message code="quote.email" default="Email"/></label>
                                    <g:select id="emailSelect" name="email.id" class="long" tabindex="4"
                                              from="${emails}" optionKey="id" value="${quoteInstance?.email?.id}"
                                              noSelection="${['null': 'Select your email address...']}"/> <!-- (<a class="fieldLinkRight" href="#" tabindex="0">add a new email</a>) -->
                                </div>

                                <div class="clearfix ${hasErrors(bean: quoteInstance, field: 'mobile', 'error')}">
                                    <label for="mobile.id"><g:message code="quote.mobile" default="Mobile"/></label>
                                    <g:select id="mobileSelect" name="mobile.id" class="short" tabindex="5"
                                              from="${mobiles}" optionKey="id" value="${quoteInstance?.mobile?.id}"
                                              noSelection="${['null': 'Select your mobile...']}" style="width: 13.3em;"/> <!-- (<a class="fieldLinkRight" href="#" tabindex="0">add a new mobile</a>) -->
                                </div>


                                <div class="clearfix mandatory ${hasErrors(bean: quoteInstance, field: 'grossAnnualSalary', 'error')}">
                                    <label for="grossAnnualSalaryDisplay"><g:message code="quote.grossAnnualSalary" default="Salary"/></label>
                                    <g:textField id="grossAnnualSalaryDisplay" class="short"
                                                 name="grossAnnualSalaryDisplay"
                                                 value="\$ ${fieldValue(bean: quoteInstance, field: 'grossAnnualSalary')}" style="width: 149px;"/>
                                    <span> per </span>
                                    <select name="salaryPeriod" class="short" id="salaryPeriod">
                                        <option value="1">annum</option>
                                        <option value="12">month</option>
                                        <option value="26">fortnight</option>
                                        <option value="24">bi-month</option>
                                        <option value="52">week</option>

                                    </select>
                                    %{--<g:select class="short" name="salaryPeriod"--}%
                                              %{--optionKey="${{ it.value?.getCode() }}"--}%
                                              %{--optionValue="value"--}%
                                              %{--from="${com.innovated.iris.util.PayrollFrequency.lookup}"--}%
                                              %{--value="${selectPayFreq == null ? 12 : selectPayFreq}"/>--}%
                                </div>

                                <g:hiddenField id="grossAnnualSalary" name="grossAnnualSalary"
                                               value="${fieldValue(bean: quoteInstance, field: 'grossAnnualSalary')}"
                                               tabindex="7"/>
                            </div>


                            <h3>Employer Details</h3>

                            <p>Please select / enter your employer details.</p>

                            %{--<g:if test="${employers.size() > 0}">--}%
                                %{--<div class="formSet">--}%
                                    %{--<g:hiddenField name="employerEnable" value="YES"/>--}%
                                    %{--<g:hiddenField name="employerMatched" value="YES"/>--}%
                                    %{--<div class="clearfix ${hasErrors(bean: quoteInstance, field: 'company', 'error')}">--}%
                                        %{--<label for="company.id"><g:message code="quote.company"--}%
                                                                           %{--default="Employer"/></label>--}%
                                        %{--<g:select id="employerSelect" name="company.id" class="long" tabindex="6"--}%
                                                  %{--from="${employers}"--}%
                                                  %{--value="${employers.size() == 1 ? employers.get(0)?.id : quoteInstance?.companyId}"--}%
                                                  %{--optionKey="id"--}%
                                                  %{--noSelection="${['null': 'Select your employer...']}"/>--}%
                                    %{--</div>--}%
                                %{--</div>--}%
                            %{--</g:if>--}%

                            %{--<g:if test="${employers.size() == 0}">--}%

                                <g:hiddenField id="employerId" name="company.id" value="${quoteInstance?.company?.id}"/>
                                <g:hiddenField id="employerEnable" name="employerEnable" value="${employerEnable == null ? 'YES' : (employerEnable.booleanValue() ? 'YES' : 'NO')}"/>


                                <div class="formSet">
                                    <div id="enterEmployerInfo" style="display: ${employerEnable == null ? 'none' : (employerEnable.booleanValue() ? 'none' : 'block')};">
                                        <div class="clearfix mandatory ${hasErrors(bean: quoteInstance, field: 'company', 'error')}">
                                            <label for="employerType">Employer Type</label>
                                            <g:select class="long" name="employerType"
                                                      optionKey="${{ it.value?.name() }}"
                                                      optionValue="value"
                                                      from="${com.innovated.iris.util.EmployerType.lookup}"
                                                      value="${employerType}"
                                                      noSelection="${['null': 'Select your employer type...']}"/>
                                        </div>
                                        <div class="clearfix">
                                            <label></label>
                                            <span class="formcancel">or&nbsp;&nbsp;<a style="color: #551A8B;"
                                                                                      id="searchEmployer"
                                                                                      href="javascript:void(0);"
                                                                                      onclick="searchEmployerHandler()">input your employer name/code</a>
                                            </span>
                                        </div>
                                    </div>
                                    <div id="searchEmployerInfo" style="display: ${employerEnable == null ? 'block' : (employerEnable.booleanValue() ? 'block' : 'none')};">
                                        <div class="clearfix mandatory ${hasErrors(bean: quoteInstance, field: 'company', 'error')}">
                                            <label for="employerSearch">Employer</label>
                                            <g:textField id="employerSearch" class="employerAutoComplete" name="companyName"
                                                         value="${quoteInstance?.company?.tradingName}"/>
                                            (<span id="searchEmployerNote"
                                                   class="fieldLinkRight">enter the <b>employer name</b> or <b>employer code</b> to search the employers!
                                        </span>)
                                        </div>

                                        <div class="clearfix">
                                            <label></label>
                                            <span class="formcancel">or&nbsp;&nbsp;<a id="cancelSearch"
                                                                                      style="color: #551A8B;"
                                                                                      href="javascript:void(0);"
                                                                                      onclick="cancelSearchHandler()">select if your employer is not displayed</a>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="clearfix mandatory ${hasErrors(bean: quoteInstance, field: 'defaultPayFreq', 'error')}">
                                        <label for="selectPayFreq"><g:message code="quote.defaultPayFreq"
                                                                              default="Payroll Frequency"/></label>
                                        <g:select class="short" name="selectPayFreq"
                                                  optionKey="${{ it.value?.getCode() }}"
                                                  optionValue="value"
                                                  from="${com.innovated.iris.util.PayrollFrequency.lookup}"
                                                  value="${selectPayFreq}"
                                                  noSelection="${['null': 'Select the payroll frequency...']}"/>
                                        %{--<a class="fieldLinkRight">(select another if need)</a>--}%
                                    </div>
                                </div>

                            %{--</g:if>--}%
                            <h3 class="topPad">Lease Term & Vehicle Usage</h3>

                            <p>Select your desired lease term, and the estimated number of kilometres you will travel each year.</p>

                            <div class="formSet">
                                <div class="clearfix mandatory ${hasErrors(bean: quoteInstance, field: 'leaseTerm', 'error')}">
                                    <label for="leaseTerm"><g:message code="quote.leaseTerm"
                                                                      default="Lease Term"/></label>
                                    <g:select class="short" name="leaseTerm" tabindex="8" from="${[12, 24, 36, 48, 60]}"
                                              value="${fieldValue(bean: quoteInstance, field: 'leaseTerm')}"
                                              optionValue="${{ it + ' months' }}"
                                              noSelection="${['null': 'Select lease term...']}"/>
                                </div>

                                <div class="clearfix mandatory ${hasErrors(bean: quoteInstance, field: 'kmPerYear', 'error')}">
                                    <label for="kmPerYear"><g:message code="quote.kmPerYear"
                                                                      default="Km's Per Year"/></label>
                                    <g:textField class="short" tabindex="9" name="kmPerYear"
                                                 value="${fieldValue(bean: quoteInstance, field: 'kmPerYear')}"/> <a
                                        class="fieldLinkRight">input km's/annum, (input 15,000) if unsure</a>
                                </div>
                            </div>

                            <h3 class="topPad">Vehicle Selection</h3>

                            <p>Please select the make and model of your preferred vehicle.</p>

                            <div class="formSet">
                                <g:hiddenField id="vehicle_val" name="vehicle.id"
                                               value="${quoteInstance?.vehicle?.id}"/>
                                <g:hiddenField id="newUsedOption" name="newUsedOption" value="1"/>
                                %{--<g:if test="${enableUsed}">--}%
                                %{--<div class="clearfix">--}%
                                %{--<label for="newUsed">&nbsp;</label>--}%

                                %{--<div id="newUsed">--}%
                                %{--<span><input class="radio" type="radio" name="newUsed" value="1"--}%
                                %{--tabindex="11" checked/> New Cars</span>--}%
                                %{--<span><input class="radio" type="radio" name="newUsed" value="0"--}%
                                %{--tabindex="12"/> Used Cars (or Sale & Leaseback)</span>--}%
                                %{--</div>--}%
                                %{--</div>--}%
                                %{--</g:if>--}%
                                %{--<g:else>--}%
                                %{--<g:hiddenField name="newUsed" value="1"/>--}%
                                %{--</g:else>--}%
                                <div class="clearfix">
                                    <label for="newUsed">&nbsp;</label>

                                    <div id="newUsed">
                                        <span>
                                            <g:radio name="newUsed" value="1"
                                                     checked="${newVehicle != null ? newVehicle.booleanValue() : true}"/> New Cars
                                        </span>
                                        <span>
                                            <g:radio name="newUsed" value="0"
                                                     checked="${newVehicle != null ? !(newVehicle.booleanValue()) : false}"/> Used Cars (or Sale & Leaseback)
                                        </span>
                                    </div>
                                </div>

                                <div id="vehicleSelectNew">
                                    <div class="clearfix" style="display: none;">
                                        <label for="vehicleYear">Year Model</label>
                                        %{--<g:select class="short" name="vehicleYearNew" tabindex="15" from="${usedYears}"--}%
                                        %{--value="${quoteInstance?.vehicleYear}"--}%
                                        %{--noSelection="[null: 'Select Year Model']"/>--}%
                                        <g:textField name="vehicleYear" value="${quoteInstance?.vehicleYear}"/>
                                    </div>

                                    <div class="clearfix">
                                        <label for="nu.newMake">Make</label>
                                        <g:select id="quoteNewCarMake" class="short" name="nu.newMake" tabindex="13"
                                                  from="${makes}" value="${nu?.newMake}"
                                                  optionKey="id" noSelection="${['': 'Select vehicle make...']}"/>
                                    </div>

                                    <div class="clearfix">
                                        <label for="nu.newModel">Model</label>
                                        <g:select id="quoteNewCarModel" class="short" name="nu.newModel"
                                                  from="${models}" value="${nu?.newModel}"
                                                  optionKey="id" noSelection="${['': 'Select vehicle model...']}"/>
                                    </div>

                                    <div class="clearfix">
                                        <label for="nu.variant">Type</label>
                                        <g:select id="vehicleVariant" name="nu.variant" class="short" from="${variants}"
                                                  value="${nu?.variant}"
                                                  noSelection="${['': 'Select vehicle variant...']}"/>
                                    </div>

                                    <div class="clearfix mandatory ${hasErrors(bean: quoteInstance, field: 'vehicle', 'error')}">
                                        <label for="nu.newMakeModel">Vehicle</label>
                                        <g:textField id="quoteNewCarSelect" class="makeModelAutoComplete"
                                                     name="nu.newMakeModel" value="${nu?.newMakeModel}"
                                                     tabindex="13"/> <!-- (<a id="missingVehicle" class="fieldLinkRight" href="#" tabindex="0">the vehicle I want isn't on this list!</a>) -->
                                    </div>

                                    <div class="clearfix mandatory ${hasErrors(bean: quoteInstance, field: 'listPrice', 'error')}" id="rrpField">
                                        <label for="indicativeRrpDisplay"><g:message code="quote.indicativeRrp" default="RRP"/></label>
                                        <g:textField id="indicativeRrpDisplay" class="short"
                                                     name="indicativeRrpDisplay"
                                                     value="\$ ${fieldValue(bean: quoteInstance, field: 'listPrice')}" readonly="readonly"/>
                                        <a class="fieldLinkRight"> (An indicative quote will be provided. Once the vehicle required is decided upon, dealer pricing will be obtained for an exact price)</a>
                                    </div>

                                    <div class="clearfix" id="ownPriceLink">
                                        <label></label>
                                        <span class="formcancel">or&nbsp;&nbsp;
                                            <a style="color: #551A8B;" href="javascript:void(0);" onclick="inputOwnPriceHandler()">
                                                select if you have your own price
                                            </a>
                                        </span>
                                    </div>

                                    <div class="clearfix mandatory ${hasErrors(bean: quoteInstance, field: 'listPrice', 'error')} ownPriceSection" style="display: none;" id="ownPriceField">
                                        <label for="ownPriceDisplay"><g:message code="quote.indicativeRrp" default="Own Price"/></label>
                                        <g:textField id="ownPriceDisplay" class="short"
                                                     name="ownPriceDisplay"
                                                     value="\$ ${fieldValue(bean: quoteInstance, field: 'listPrice')}"/><a
                                            class="fieldLinkRight"> enter price (incl GST) if dealer pricing already obtained</a>
                                    </div>

                                    <div class="clearfix" style="display: none;" id="cancelOwnPriceLink">
                                        <label></label>
                                        <span class="formcancel">or&nbsp;&nbsp;
                                            <a style="color: #551A8B;" href="javascript:void(0);" onclick="calcelOwnPriceHandler()">
                                                select if use RRP
                                            </a>
                                        </span>
                                    </div>
                                    <g:hiddenField name="priceObtained" id="priceObtained" value="${fieldValue(bean: quoteInstance, field: 'priceObtained')}"/>

                                    <g:hiddenField id="indicativeRrp" name="listPrice"
                                                   value="${fieldValue(bean: quoteInstance, field: 'listPrice')}"/>
                                </div>
                                <g:if test="${enableUsed}">
                                    <div id="vehicleSelectUsed" style="display:none;">
                                        <div class="clearfix">
                                            <label for="vehicleYearUsed">Year Model</label>
                                            <g:select class="short" name="vehicleYearUsed" tabindex="15"
                                                      from="${usedYears}" value="${quoteInstance?.vehicleYear}"
                                                      noSelection="[null: 'Select Year Model']"/>
                                            <!-- <g:select class="short" name="nu.usedYear" tabindex="15"
                                                           from="${usedYears}" value="${nu?.usedYear}"
                                                           noSelection="[null: 'Select Year Model']"/> -->
                                        </div>
                                        <!--
										<div class="clearfix">
											<label for="nu.usedMakeModel">Make / Model</label>
											<g:textField name="nu.usedMakeModel" value="${nu?.usedMakeModel}"
                                                         tabindex="14"/> <a class="help-tip" href="#" title="${message(code: 'quote.nu.usedMakeModel.tip', default: 'Type in the desired make & model')}">&nbsp;</a>
										</div>
										 -->
                                        <div class="clearfix">
                                            <label for="nu.usedMake">Make</label>
                                            <g:select id="quoteUsedCarMake" class="short" name="nu.usedMake"
                                                      tabindex="13" from="${makes}" value="${nu?.usedMake}"
                                                      optionKey="id"
                                                      noSelection="${['null': 'Select vehicle make...']}"/>
                                        </div>

                                        <div class="clearfix ${hasErrors(bean: quoteInstance, field: 'vehicle', 'error')}">
                                            <label for="nu.usedMakeModel">Model</label>
                                            <g:textField id="quoteUsedCarSelect" class="makeModelAutoComplete"
                                                         name="nu.usedMakeModel" value="${nu?.usedMakeModel}"
                                                         tabindex="13"/> <!-- (<a id="missingVehicle" class="fieldLinkRight" href="#" tabindex="0">the vehicle I want isn't on this list!</a>) -->
                                        </div>

                                        <div class="clearfix">
                                            <label for="listPrice">Price</label>
                                            <g:textField class="short" name="listPrice"
                                                         value="${quoteInstance?.listPrice}" tabindex="16"/> <a
                                                class="fieldLinkRight">(enter the purchase price)</a>  <a
                                                class="help-tip" href="#"
                                                title="${message(code: 'quote.nu.usedPrice.tip', default: 'Also include GST, where applicable')}">&nbsp;</a>
                                            <!-- <g:textField class="short" name="nu.usedPrice" value="${nu?.usedPrice}"
                                                              tabindex="16"/> <a class="fieldLinkRight">(enter the purchase price)</a>  <a class="help-tip" href="#" title="${message(code: 'quote.nu.usedPrice.tip', default: 'Also include GST, where applicable')}">&nbsp;</a> -->
                                        </div>
                                    </div>
                                </g:if>
                            </div>

                            <h3 class="topPad">Vehicle Preferences & Options</h3>

                            <p>Please enter the details of your vehicle preferences and options.</p>

                            <div class="formSet">
                                %{--<div class="clearfix ${hasErrors(bean: quoteInstance, field: 'shape', 'error')}">--}%
                                %{--<label for="shape"><g:message code="quote.shape" default="Shape"/></label>--}%
                                %{--<g:select class="short" tabindex="17" id="shape" name="shape"--}%
                                %{--from="${quoteInstance.constraints.shape.inList}"--}%
                                %{--value="${quoteInstance.shape}" valueMessagePrefix="quote.shape"/>--}%
                                %{--</div>--}%

                                %{--<div class="clearfix ${hasErrors(bean: quoteInstance, field: 'transmission', 'error')}">--}%
                                %{--<label for="transmission"><g:message code="quote.transmission"--}%
                                %{--default="Transmission"/></label>--}%
                                %{--<g:select class="short" tabindex="18" id="transmission" name="transmission"--}%
                                %{--from="${quoteInstance.constraints.transmission.inList}"--}%
                                %{--value="${quoteInstance.transmission}"--}%
                                %{--valueMessagePrefix="quote.transmission"/>--}%
                                %{--</div>--}%

                                <div class="clearfix ${hasErrors(bean: quoteInstance, field: 'colourPreference', 'error')}">
                                    <label for="colourPreference"><g:message code="quote.colourPreference"
                                                                             default="Colour Preference"/></label>
                                    <g:textField class="short" tabindex="19" name="colourPreference"
                                                 value="${fieldValue(bean: quoteInstance, field: 'colourPreference')}"/>
                                </div>

                                <div class="clearfix ${hasErrors(bean: quoteInstance, field: 'interiorTrim', 'error')}">
                                    <label for="interiorTrim"><g:message code="quote.interiorTrim"
                                                                         default="Interior Trim"/></label>
                                    <g:textField class="short" tabindex="20" name="interiorTrim"
                                                 value="${fieldValue(bean: quoteInstance, field: 'interiorTrim')}"/>
                                </div>

                                <div class="clearfix ${hasErrors(bean: quoteInstance, field: 'options', 'error')}">
                                    <label for="options"><g:message code="quote.options" default="Options"/></label>
                                    <g:textField tabindex="21" name="options"
                                                 value="${fieldValue(bean: quoteInstance, field: 'options')}"/> <a
                                        class="fieldLinkRight">(separate with commas)</a>
                                </div>
                            </div>
                            <!--
                            <h3 class="topPad">Operating Cost (Log-Book) Method</h3>
                            <p>Do you have a current / valid log-book? <b>AND</b> Will you be using the Operating Cost Method of FBT calculation?<br><i>(If you're unsure what this means, you can safely answer <b>no</b>.)</i></p>
                            <div class="formSet">
                                <div class="clearfix">
                                    <label for="opCostChoice">&nbsp;</label>
                            <g:hiddenField name="opCostChoice"
                                           value=""/><input tabindex="22" type="radio" name="hasLogBook" value="0" "${(quoteInstance.hasLogBook == 0) ? 'checked=checked' : ''}" /> No <input tabindex="23" type="radio" name="hasLogBook" value="1" "${(quoteInstance?.hasLogBook == 1) ? 'checked=checked' : ''}" /> Yes
									</div>
                            <g:if test="${quoteInstance.hasLogBook == 0}"><div class="showHide" style="display:none;"></g:if><g:else><div class="showHide"></g:else>
                            <div class="clearfix ${hasErrors(bean: quoteInstance, field: 'logBookStart', 'error')}">
										<label for="logBookStart">Logbook Start Date</label>
                            <g:datePicker tabindex="24" name="logBookStart" value="${quoteInstance?.logBookStart}"
                                          precision="day" years="${usedYears}" tabindex="1"/>
                            </div>
                            <div class="clearfix ${hasErrors(bean: quoteInstance, field: 'businessUsage', 'error')}">
										<label for="businessUsage"><g:message code="quote.businessUsage"
                                                                              default="Business Use %"/></label>
                            <g:textField class="short" tabindex="25" name="businessUsage"
                                         value="${fieldValue(bean: quoteInstance, field: 'businessUsage')}"/>
                            </div>
                            </div>
                        </div>
                         -->
                            <button id="btnLogin" name="btnLogin" class="greybutton" type="submit" tabindex="26">
                                <span><img width="16" height="16" alt=""
                                           src="${resource(dir: 'images/skin', file: 'page_white_go.png')}"/>Get Quote
                                </span>
                            </button>
                            <span class="formcancel">or&nbsp;&nbsp;<g:link controller="tools" action="quote"
                                                                           tabindex="27">cancel</g:link></span>

                        </g:form>
                </div>
            </div>

            <div id="changeAddress" style="display:none;">
                <!-- addr changes -->
                <p class="bread">
                    <!-- <g:link controller="tools"><g:message code="ui.menu.label.tools" default="Tools"/></g:link> -->
                    <g:message code="ui.menu.label.tools" default="Tools"/>
                    <span class="breadArrow">&nbsp;</span>
                    <g:link controller="tools" action="quote"><g:message code="ui.menu.label.novatedquote"
                                                                         default="Quote"/></g:link>
                    <span class="breadArrow">&nbsp;</span>
                    <g:message code="ui.menu.label.createquote" default="Get a new quote"/>
                    <span class="breadArrow">&nbsp;</span>Add Address
                </p>

                <h1 class="contentH1">Add a New Address</h1>

                <div class="formBg">
                    <div class="formWrap">
                        <h3 class="topPad">Address Details</h3>

                        <p>Please enter your new address details. This address is used to lookup defaults for vehicle registration, insurance, etc.</p>

                        <div class="formSet">
                            <div class="clearfix">
                                <label for="addressType.id">Address Type</label>
                                <g:select class="short" tabindex="40" id="addressTypeId" name="addressType.id"
                                          from="${AddressType.list()}" optionKey="id" value="11"/>
                            </div>

                            <div class="clearfix">
                                <label for="street">Street</label>
                                <g:textField tabindex="41" id="street" name="street" value=""/>
                            </div>

                            <div class="clearfix">
                                <label for="suburb">Suburb</label>
                                <g:textField class="short" tabindex="42" id="suburb" name="suburb" value=""/>
                            </div>

                            <div class="clearfix">
                                <label for="state.id">State</label>
                                <g:select class="short" tabindex="43" id="stateId" name="state.id"
                                          from="${AustralianState.list()}" optionKey="id" value=""/>
                            </div>

                            <div class="clearfix">
                                <label for="postcode">Postcode</label>
                                <g:textField class="short" tabindex="44" id="postcode" name="postcode" value=""/>
                            </div>
                        </div>

                        <button id="btnAddAddress" name="btnAddAddress" class="greybutton" type="submit" tabindex="50">
                            <span><img width="16" height="16" alt=""
                                       src="${resource(dir: 'images/skin', file: 'disk.png')}"/>Save Address</span>
                        </button>
                        <span class="formcancel">or&nbsp;&nbsp;<a id="addAddressCancel" href="#"
                                                                  tabindex="51">cancel</a></span>
                    </div>
                </div>
            </div> <!-- #changeAddress -->
        </div>
    </div> <!-- #contentMain -->


    <div id="rightcol">
        <div id="options">
            <div class="optGrpHdr"><h3 class="optHdr">Current Options</h3></div>
            <dl class="optGrpMenu">
                <iris:sidebarItem controller="tools" action="quote" icon="page_white_stack.png"
                                  titleCode="ui.opt.title.listquotes" msgCode="ui.opt.msg.listquotes"/>
            </dl>
        </div>
    </div>

    <div class="clear"></div>
</div>
</div>
</body>
</html>

