<html>
<head>
    <meta name="layout" content="prod">
    <title><g:message code="ui.menu.label.novatedquote" default="Quote"/></title>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#quotes input:checkbox").click(function () {
                var num = $("#quotes tbody input:checkbox:checked").length;

                // show/hide the "Select Quote" sidebar link
                if (num == 1) $("#select-quote").fadeIn('fast');
                else $("#select-quote").fadeOut('slow');
            });

            $("#select-quote a").click(function (e) {
                e.preventDefault();
                if ($("#quotes tbody input:checkbox:checked").length == 1) {
                    // get id of selected quote
                    var id = $("#quotes tbody input:checkbox:checked").first().attr("id").split("_")[1];
                    var href = $(this).attr("href");
                    var params = {id: id};
                    $.post("${createLink(action:'selectquoteAjax')}", params, function (data) {
                        var doSelect = false;
                        if (data.alreadySelected) {
                            if (data.selectedId != id) {
                                var choice = confirm("You already have quote " + data.selectedAqn + " selected as preferred!\n\nDo you want to change this prefernce?")
                                if (choice) {
                                    doSelect = true;
                                }
                            }
                            else {
                                alert("You already have quote " + data.selectedAqn + " selected as preferred!");
                            }
                        }
                        else {
                            doSelect = true;
                        }
                        if (doSelect) {
                            window.location = href + "/" + id;
                            //alert("doing selection...");
                        }
                    }, 'json');
                }
                else {
                    alert("Only 1 budget quote can be selected as your preferred at any given time!");
                }
            });
        });
    </script>
</head>

<body>
<div class="twocol">
    <div id="content">
        <div id="leftcol">
            <p class="bread">
                <!-- <g:link controller="tools"><g:message code="ui.menu.label.tools" default="Tools"/></g:link> -->
                <g:message code="ui.menu.label.tools" default="Tools"/>
                <span class="breadArrow">&nbsp;</span>
                <g:message code="ui.menu.label.novatedquote" default="Quote"/>
            </p>

            <div id="messages"></div>

            <g:if test="${flash.message}">
                <div class="flashMsg">
                    <g:if test="${flash.classes}"><p class="${flash.classes}"></g:if><g:else><p></g:else><g:message
                        code="${flash.message}" args="${flash.args}" default="${flash.defaultMessage}"/></p>
                </div>
            </g:if>

            <h1 class="contentH1"><g:message code="ui.h1.label.novatedbudgetquote" default="Quote"/></h1>
            <g:if test="${quoteList?.size() > 0}">
                <div>
                    <g:ifAnyGranted role="ROLE_SUPER,ROLE_ADMIN">
                        <g:render template="/templates/ui/quote/listastableAdmin"/>
                    </g:ifAnyGranted>
                    <g:ifNotGranted role="ROLE_SUPER,ROLE_ADMIN">
                        <g:render template="/templates/ui/quote/listastable"/>
                    </g:ifNotGranted>
                </div>
            </g:if>
            <g:else>
                <div class="emptyListPanel">
                    <h1><g:link controller="tools" action="createquote"><g:message code="ui.h1.label.nonovatedquote"
                                                                                   default="Generate a new quote"/></g:link></h1>

                    <p><g:message code="ui.p.label.nonovatedquote" default="Your completed quotes will show here."/></p>
                </div>
            </g:else>
        </div>

        <div id="rightcol">
            <div id="options">
                <div class="optGrpHdr"><h3 class="optHdr">What do you want to do?...</h3></div>
                <dl class="optGrpMenu">
                    <iris:sidebarItem controller="tools" action="createquote" icon="page_white_add.png"
                                      titleCode="ui.opt.title.newquote" msgCode="ui.opt.msg.newquote"/>
                    <iris:sidebarItem elementId="select-quote" style="display:none;" controller="tools"
                                      action="selectquote" icon="page_white_gear.png"
                                      titleCode="ui.opt.title.selectquote" msgCode="ui.opt.msg.selectquote"/>
                </dl>
            </div>
        </div>

        <div class="clear"></div>
    </div>
</div>
</body>
</html>

