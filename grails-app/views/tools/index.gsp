<html>
    <head>
        <meta name="layout" content="prod">
        <title><g:message code="ui.menu.label.tools" default="Tools" /></title>
    </head>
	<body>
		<div class="twocol">
			<div id="content">
				<div id="leftcol">
					<p class="bread">
						<g:message code="ui.menu.label.tools" default="Tools" />
					</p>
					<h1 class="contentH1"><g:message code="ui.h1.label.tools" default="Client Tools" /></h1>
					<div>
						
					</div>
				</div>
				<div id="rightcol">
					<!-- RIGHT COLUMN content here... -->
					client tools
				</div>
				<div class="clear"></div>
			</div>
		</div>
	</body>
</html>

