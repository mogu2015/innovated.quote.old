
<%@ page import="com.innovated.iris.domain.QuoteItemInsurance" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <title><g:message code="quoteItemInsurance.list" default="QuoteItemInsurance List" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLinkTo(dir: '')}"><g:message code="home" default="Home" /></a></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="quoteItemInsurance.new" default="New QuoteItemInsurance" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="quoteItemInsurance.list" default="QuoteItemInsurance List" /></h1>
            <g:if test="${flash.message}">
            <div class="message"><g:message code="${flash.message}" args="${flash.args}" default="${flash.defaultMessage}" /></div>
            </g:if>
            <div class="list">
                <table>
                    <thead>
                        <tr>
                        
                   	    <g:sortableColumn property="id" title="Id" titleKey="quoteItemInsurance.id" />
                        
                   	    <g:sortableColumn property="pageKey" title="Page Key" titleKey="quoteItemInsurance.pageKey" />
                        
                   	    <g:sortableColumn property="name" title="Name" titleKey="quoteItemInsurance.name" />
                        
                   	    <g:sortableColumn property="value" title="Value" titleKey="quoteItemInsurance.value" />
                        
                   	    <th><g:message code="quoteItemInsurance.quote" default="Quote" /></th>
                   	    
                        </tr>
                    </thead>
                    <tbody>
                    <g:each in="${quoteItemInsuranceInstanceList}" status="i" var="quoteItemInsuranceInstance">
                        <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
                        
                            <td><g:link action="show" id="${quoteItemInsuranceInstance.id}">${fieldValue(bean: quoteItemInsuranceInstance, field: "id")}</g:link></td>
                        
                            <td>${fieldValue(bean: quoteItemInsuranceInstance, field: "pageKey")}</td>
                        
                            <td>${fieldValue(bean: quoteItemInsuranceInstance, field: "name")}</td>
                        
                            <td>${fieldValue(bean: quoteItemInsuranceInstance, field: "value")}</td>
                        
                            <td>${fieldValue(bean: quoteItemInsuranceInstance, field: "quote")}</td>
                        
                        </tr>
                    </g:each>
                    </tbody>
                </table>
            </div>
            <div class="paginateButtons">
                <g:paginate total="${quoteItemInsuranceInstanceTotal}" />
            </div>
        </div>
    </body>
</html>
