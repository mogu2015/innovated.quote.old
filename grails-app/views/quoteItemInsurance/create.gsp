
<%@ page import="com.innovated.iris.domain.QuoteItemInsurance" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <title><g:message code="quoteItemInsurance.create" default="Create QuoteItemInsurance" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLinkTo(dir: '')}"><g:message code="home" default="Home" /></a></span>
            <span class="menuButton"><g:link class="list" action="list"><g:message code="quoteItemInsurance.list" default="QuoteItemInsurance List" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="quoteItemInsurance.create" default="Create QuoteItemInsurance" /></h1>
            <g:if test="${flash.message}">
            <div class="message"><g:message code="${flash.message}" args="${flash.args}" default="${flash.defaultMessage}" /></div>
            </g:if>
            <g:hasErrors bean="${quoteItemInsuranceInstance}">
            <div class="errors">
                <g:renderErrors bean="${quoteItemInsuranceInstance}" as="list" />
            </div>
            </g:hasErrors>
            <g:form action="save" method="post" >
                <div class="dialog">
                    <table>
                        <tbody>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="pageKey"><g:message code="quoteItemInsurance.pageKey" default="Page Key" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: quoteItemInsuranceInstance, field: 'pageKey', 'errors')}">
                                    <g:textField name="pageKey" value="${fieldValue(bean: quoteItemInsuranceInstance, field: 'pageKey')}" />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="name"><g:message code="quoteItemInsurance.name" default="Name" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: quoteItemInsuranceInstance, field: 'name', 'errors')}">
                                    <g:textField name="name" value="${fieldValue(bean: quoteItemInsuranceInstance, field: 'name')}" />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="value"><g:message code="quoteItemInsurance.value" default="Value" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: quoteItemInsuranceInstance, field: 'value', 'errors')}">
                                    <g:textField name="value" value="${fieldValue(bean: quoteItemInsuranceInstance, field: 'value')}" />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="quote"><g:message code="quoteItemInsurance.quote" default="Quote" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: quoteItemInsuranceInstance, field: 'quote', 'errors')}">
                                    <g:select name="quote.id" from="${com.innovated.iris.domain.Quote.list()}" optionKey="id" value="${quoteItemInsuranceInstance?.quote?.id}"  />

                                </td>
                            </tr>
                        
                        </tbody>
                    </table>
                </div>
                <div class="buttons">
                    <span class="button"><g:submitButton name="create" class="save" value="${message(code: 'create', 'default': 'Create')}" /></span>
                </div>
            </g:form>
        </div>
    </body>
</html>
