
<%@ page import="com.innovated.iris.domain.QuoteItemInsurance" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <title><g:message code="quoteItemInsurance.show" default="Show QuoteItemInsurance" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLinkTo(dir: '')}"><g:message code="home" default="Home" /></a></span>
            <span class="menuButton"><g:link class="list" action="list"><g:message code="quoteItemInsurance.list" default="QuoteItemInsurance List" /></g:link></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="quoteItemInsurance.new" default="New QuoteItemInsurance" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="quoteItemInsurance.show" default="Show QuoteItemInsurance" /></h1>
            <g:if test="${flash.message}">
            <div class="message"><g:message code="${flash.message}" args="${flash.args}" default="${flash.defaultMessage}" /></div>
            </g:if>
            <g:form>
                <g:hiddenField name="id" value="${quoteItemInsuranceInstance?.id}" />
                <div class="dialog">
                    <table>
                        <tbody>
                        
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="quoteItemInsurance.id" default="Id" />:</td>
                                
                                <td valign="top" class="value">${fieldValue(bean: quoteItemInsuranceInstance, field: "id")}</td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="quoteItemInsurance.pageKey" default="Page Key" />:</td>
                                
                                <td valign="top" class="value">${fieldValue(bean: quoteItemInsuranceInstance, field: "pageKey")}</td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="quoteItemInsurance.name" default="Name" />:</td>
                                
                                <td valign="top" class="value">${fieldValue(bean: quoteItemInsuranceInstance, field: "name")}</td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="quoteItemInsurance.value" default="Value" />:</td>
                                
                                <td valign="top" class="value">${fieldValue(bean: quoteItemInsuranceInstance, field: "value")}</td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="quoteItemInsurance.quote" default="Quote" />:</td>
                                
                                <td valign="top" class="value"><g:link controller="quote" action="show" id="${quoteItemInsuranceInstance?.quote?.id}">${quoteItemInsuranceInstance?.quote?.encodeAsHTML()}</g:link></td>
                                
                            </tr>
                            
                        </tbody>
                    </table>
                </div>
                <div class="buttons">
                    <span class="button"><g:actionSubmit class="edit" action="edit" value="${message(code: 'edit', 'default': 'Edit')}" /></span>
                    <span class="button"><g:actionSubmit class="delete" action="delete" value="${message(code: 'delete', 'default': 'Delete')}" onclick="return confirm('${message(code: 'delete.confirm', 'default': 'Are you sure?')}');" /></span>
                </div>
            </g:form>
        </div>
    </body>
</html>
