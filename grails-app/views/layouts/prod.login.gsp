<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
	<head>
		<title><g:layoutTitle default="IRIS" /></title>
        <g:render template="/templates/ui/htmlheadinc" />
        <style type="text/css">
			html { background: #FFF; }
			body { background: #FFF; }
			a:link, a:visited, a:hover, a:active { color: #015793; }
		</style>
        <g:layoutHead />
        <g:javascript library="application" />	
	</head>
	<body style="min-width:0px;">
		<g:layoutBody />
	</body>
</html>

