<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<!-- <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> -->
<html>
	<head>
		<title><g:layoutTitle default="IRIS" /></title>
        <g:render template="/templates/ui/htmlheadinc" />
        <g:javascript library="application" />
        <g:layoutHead />
	</head>
	<body>
		<g:render template='/templates/ui/ajaxLogin'/>
		<div id="body">
			<div id="adminNavsWrapper">
				<g:render template="/templates/ui/topmenu" />
			</div>
			<div class="clear"></div>
			<div id="case">
				<div class="shadowHeaderWrap">
					<div class="shadowHeader">
						<div class="navheaderleft">
							<div class="shadowMidLeft">
								<div id="logoSpacer"></div>
							</div>
						</div>
					</div>
				</div>
				<div class="shadowWrap2">
					<div class="shadowWrap">
						<div id="navheader" class="navheader">
							<g:render template="/templates/ui/mainmenu" />
						</div>
						<div class="clear"></div>		
						<div class="shadowMidLeft">
							<div class="shadowMidContent">
								<g:layoutBody />
							</div>
						</div>
					</div>
				</div>
				<div class="shadowBottomLeft">
					<div class="shadowMidLeft"></div>
				</div>
			</div>
		</div>
		
	</body>
</html>

