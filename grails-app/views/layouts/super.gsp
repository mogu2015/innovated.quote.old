<html>
    <head>
        <title><g:layoutTitle default="IRIS" /></title>
        <link rel="stylesheet" type="text/css" href="http://yui.yahooapis.com/2.8.0r4/build/reset/reset-min.css">
        <link rel="stylesheet" href="${resource(dir:'css',file:'main.css')}" />
        <link rel="stylesheet" href="${resource(dir:'css',file:'forms.css')}" />
        <link rel="stylesheet" href="${resource(dir:'css',file:'iris.css')}" />
        <link rel="shortcut icon" href="${resource(dir:'images',file:'favicon.ico')}" type="image/x-icon" />
        <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js"></script>
        <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.7.2/jquery-ui.min.js"></script>
        <!--<g:javascript library="jquery/jquery-1.3.2.min" />-->
        <!--<g:javascript library="jquery/jquery.editable-1.3.3.min" />-->
        <g:javascript library="jquery/jqbrowser.min"/>
        <!--<g:javascript library="jquery/jquery.highlight"/>-->
        <g:layoutHead />
        <g:javascript library="application" />	
        <!--<calendar:resources lang="en" theme="blue"/>-->
    </head>
    <body>
        <div id="spinner" class="spinner" style="display:none;">
            <img src="${resource(dir:'images',file:'spinner.gif')}" alt="Spinner" />
        </div>	
        <div class="logo">
    	    <a href="${resource(dir:'')}">
    		    <img src="${resource(dir:'images',file:'iris_logo.png')}" alt="IRIS" title="IRIS Home" />
	        </a>
	        <span>SUPER Layout</span>
        </div>
        <div class="status">
        	<g:isLoggedIn>Welcome ${loggedInUserInfo(field:'username')}! | <g:link controller="logout" action="index">Logout</g:link></g:isLoggedIn>
        	<g:isNotLoggedIn><g:link controller="login" action="auth">Login Here</g:link></g:isNotLoggedIn>
        </div>
        <g:layoutBody />		
    </body>	
</html>
