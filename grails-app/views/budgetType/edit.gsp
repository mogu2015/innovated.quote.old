
<%@ page import="com.innovated.iris.domain.enums.BudgetType" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <title><g:message code="budgetType.edit" default="Edit BudgetType" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLinkTo(dir: '')}"><g:message code="home" default="Home" /></a></span>
            <span class="menuButton"><g:link class="list" action="list"><g:message code="budgetType.list" default="BudgetType List" /></g:link></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="budgetType.new" default="New BudgetType" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="budgetType.edit" default="Edit BudgetType" /></h1>
            <g:if test="${flash.message}">
            	<div class="message"><g:message code="${flash.message}" args="${flash.args}" default="${flash.defaultMessage}" /></div>
            </g:if>
            <g:hasErrors bean="${budgetTypeInstance}">
				<div class="errors">
					<g:renderErrors bean="${budgetTypeInstance}" as="list" />
				</div>
            </g:hasErrors>
            <g:form method="post" >
                <g:hiddenField name="id" value="${budgetTypeInstance?.id}" />
                <g:hiddenField name="version" value="${budgetTypeInstance?.version}" />
                <div class="dialog">
                    <fieldset>
                        <legend><g:message code="budgetType.edit.legend" default="Update BudgetType Details"/></legend>
                        
                        <div class="prop mandatory ${hasErrors(bean: budgetTypeInstance, field: 'name', 'error')}">
                            <label for="name">
                                <g:message code="budgetType.name" default="Name" />
                                <span class="indicator">*</span>
                            </label>
                            <g:textField name="name" value="${fieldValue(bean: budgetTypeInstance, field: 'name')}" />

                        </div>
                        
                        <div class="prop mandatory ${hasErrors(bean: budgetTypeInstance, field: 'visible', 'error')}">
                            <label for="visible">
                                <g:message code="budgetType.visible" default="Visible" />
                                <span class="indicator">*</span>
                            </label>
                            <g:checkBox name="visible" value="${budgetTypeInstance?.visible}" />

                        </div>
                        
                        <div class="prop mandatory ${hasErrors(bean: budgetTypeInstance, field: 'enabled', 'error')}">
                            <label for="enabled">
                                <g:message code="budgetType.enabled" default="Enabled" />
                                <span class="indicator">*</span>
                            </label>
                            <g:checkBox name="enabled" value="${budgetTypeInstance?.enabled}" />

                        </div>
                        
                        <div class="prop mandatory ${hasErrors(bean: budgetTypeInstance, field: 'dateCreated', 'error')}">
                            <label for="dateCreated">
                                <g:message code="budgetType.dateCreated" default="Date Created" />
                                <span class="indicator">*</span>
                            </label>
                            <g:datePicker name="dateCreated" value="${budgetTypeInstance?.dateCreated}"  />

                        </div>
                        
                        <div class="prop mandatory ${hasErrors(bean: budgetTypeInstance, field: 'lastUpdated', 'error')}">
                            <label for="lastUpdated">
                                <g:message code="budgetType.lastUpdated" default="Last Updated" />
                                <span class="indicator">*</span>
                            </label>
                            <g:datePicker name="lastUpdated" value="${budgetTypeInstance?.lastUpdated}"  />

                        </div>
                        
                    </fieldset>
                </div>
                <div class="buttons">
                    <span class="button"><g:actionSubmit class="save" action="update" value="${message(code: 'update', 'default': 'Update')}" /></span>
                    <span class="button"><g:actionSubmit class="delete" action="delete" value="${message(code: 'delete', 'default': 'Delete')}" onclick="return confirm('${message(code: 'delete.confirm', 'default': 'Are you sure?')}');" /></span>
                </div>
            </g:form>
        </div>
    </body>
</html>

