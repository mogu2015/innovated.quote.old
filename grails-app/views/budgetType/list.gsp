
<%@ page import="com.innovated.iris.domain.enums.BudgetType" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <title><g:message code="budgetType.list" default="BudgetType List" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLinkTo(dir: '')}"><g:message code="home" default="Home" /></a></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="budgetType.new" default="New BudgetType" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="budgetType.list" default="BudgetType List" /></h1>
            <g:if test="${flash.message}">
            <div class="message"><g:message code="${flash.message}" args="${flash.args}" default="${flash.defaultMessage}" /></div>
            </g:if>
            <div class="list">
                <table>
                    <thead>
                        <tr>
                        
                   	    <g:sortableColumn property="id" title="Id" titleKey="budgetType.id" />
                        
                   	    <g:sortableColumn property="name" title="Name" titleKey="budgetType.name" />
                        
                   	    <g:sortableColumn property="visible" title="Visible" titleKey="budgetType.visible" />
                        
                   	    <g:sortableColumn property="enabled" title="Enabled" titleKey="budgetType.enabled" />
                        
                   	    <g:sortableColumn property="dateCreated" title="Date Created" titleKey="budgetType.dateCreated" />
                        
                   	    <g:sortableColumn property="lastUpdated" title="Last Updated" titleKey="budgetType.lastUpdated" />
                        
                        </tr>
                    </thead>
                    <tbody>
                    <g:each in="${budgetTypeInstanceList}" status="i" var="budgetTypeInstance">
                        <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
                        
                            <td><g:link action="show" id="${budgetTypeInstance.id}">${fieldValue(bean: budgetTypeInstance, field: "id")}</g:link></td>
                        
                            <td>${fieldValue(bean: budgetTypeInstance, field: "name")}</td>
                        
                            <td><g:formatBoolean boolean="${budgetTypeInstance.visible}" /></td>
                        
                            <td><g:formatBoolean boolean="${budgetTypeInstance.enabled}" /></td>
                        
                            <td><g:formatDate date="${budgetTypeInstance.dateCreated}" /></td>
                        
                            <td><g:formatDate date="${budgetTypeInstance.lastUpdated}" /></td>
                        
                        </tr>
                    </g:each>
                    </tbody>
                </table>
            </div>
            <div class="paginateButtons">
                <g:paginate total="${budgetTypeInstanceTotal}" />
            </div>
        </div>
    </body>
</html>
