
<%@ page import="com.innovated.iris.domain.application.FinanceApplication" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="prod" />
        <title><g:message code="financeApplication.create" default="Create FinanceApplication" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLinkTo(dir: '')}"><g:message code="home" default="Home" /></a></span>
            <span class="menuButton"><g:link class="list" action="list"><g:message code="financeApplication.list" default="FinanceApplication List" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="financeApplication.create" default="Create FinanceApplication" /></h1>
            <g:if test="${flash.message}">
            <div class="message"><g:message code="${flash.message}" args="${flash.args}" default="${flash.defaultMessage}" /></div>
            </g:if>
            <g:hasErrors bean="${financeApplicationInstance}">
            <div class="errors">
                <g:renderErrors bean="${financeApplicationInstance}" as="list" />
            </div>
            </g:hasErrors>
            <g:form action="save" method="post" >
                <div class="dialog">
                    <table>
                        <tbody>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="applicantDateOfBirth"><g:message code="financeApplication.applicantDateOfBirth" default="Applicant Date Of Birth" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: financeApplicationInstance, field: 'applicantDateOfBirth', 'errors')}">
                                    <g:datePicker name="applicantDateOfBirth" value="${financeApplicationInstance?.applicantDateOfBirth}"  />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="applicantDriversLicenceNum"><g:message code="financeApplication.applicantDriversLicenceNum" default="Applicant Drivers Licence Num" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: financeApplicationInstance, field: 'applicantDriversLicenceNum', 'errors')}">
                                    <g:textField name="applicantDriversLicenceNum" value="${fieldValue(bean: financeApplicationInstance, field: 'applicantDriversLicenceNum')}" />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="applicantEmail"><g:message code="financeApplication.applicantEmail" default="Applicant Email" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: financeApplicationInstance, field: 'applicantEmail', 'errors')}">
                                    <g:textField name="applicantEmail" value="${fieldValue(bean: financeApplicationInstance, field: 'applicantEmail')}" />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="applicantFirstName"><g:message code="financeApplication.applicantFirstName" default="Applicant First Name" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: financeApplicationInstance, field: 'applicantFirstName', 'errors')}">
                                    <g:textField name="applicantFirstName" value="${fieldValue(bean: financeApplicationInstance, field: 'applicantFirstName')}" />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="applicantLastName"><g:message code="financeApplication.applicantLastName" default="Applicant Last Name" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: financeApplicationInstance, field: 'applicantLastName', 'errors')}">
                                    <g:textField name="applicantLastName" value="${fieldValue(bean: financeApplicationInstance, field: 'applicantLastName')}" />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="applicantMaritalStatus"><g:message code="financeApplication.applicantMaritalStatus" default="Applicant Marital Status" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: financeApplicationInstance, field: 'applicantMaritalStatus', 'errors')}">
                                    <g:textField name="applicantMaritalStatus" value="${fieldValue(bean: financeApplicationInstance, field: 'applicantMaritalStatus')}" />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="applicantMiddleName"><g:message code="financeApplication.applicantMiddleName" default="Applicant Middle Name" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: financeApplicationInstance, field: 'applicantMiddleName', 'errors')}">
                                    <g:textField name="applicantMiddleName" value="${fieldValue(bean: financeApplicationInstance, field: 'applicantMiddleName')}" />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="applicantPhoneHome"><g:message code="financeApplication.applicantPhoneHome" default="Applicant Phone Home" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: financeApplicationInstance, field: 'applicantPhoneHome', 'errors')}">
                                    <g:textField name="applicantPhoneHome" value="${fieldValue(bean: financeApplicationInstance, field: 'applicantPhoneHome')}" />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="applicantPhoneMobile"><g:message code="financeApplication.applicantPhoneMobile" default="Applicant Phone Mobile" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: financeApplicationInstance, field: 'applicantPhoneMobile', 'errors')}">
                                    <g:textField name="applicantPhoneMobile" value="${fieldValue(bean: financeApplicationInstance, field: 'applicantPhoneMobile')}" />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="applicantPhoneWork"><g:message code="financeApplication.applicantPhoneWork" default="Applicant Phone Work" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: financeApplicationInstance, field: 'applicantPhoneWork', 'errors')}">
                                    <g:textField name="applicantPhoneWork" value="${fieldValue(bean: financeApplicationInstance, field: 'applicantPhoneWork')}" />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="applicantTitle"><g:message code="financeApplication.applicantTitle" default="Applicant Title" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: financeApplicationInstance, field: 'applicantTitle', 'errors')}">
                                    <g:textField name="applicantTitle" value="${fieldValue(bean: financeApplicationInstance, field: 'applicantTitle')}" />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="employCurrContact"><g:message code="financeApplication.employCurrContact" default="Employ Curr Contact" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: financeApplicationInstance, field: 'employCurrContact', 'errors')}">
                                    <g:textField name="employCurrContact" value="${fieldValue(bean: financeApplicationInstance, field: 'employCurrContact')}" />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="employCurrMonths"><g:message code="financeApplication.employCurrMonths" default="Employ Curr Months" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: financeApplicationInstance, field: 'employCurrMonths', 'errors')}">
                                    <g:textField name="employCurrMonths" value="${fieldValue(bean: financeApplicationInstance, field: 'employCurrMonths')}" />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="employCurrName"><g:message code="financeApplication.employCurrName" default="Employ Curr Name" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: financeApplicationInstance, field: 'employCurrName', 'errors')}">
                                    <g:textField name="employCurrName" value="${fieldValue(bean: financeApplicationInstance, field: 'employCurrName')}" />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="employCurrPhone"><g:message code="financeApplication.employCurrPhone" default="Employ Curr Phone" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: financeApplicationInstance, field: 'employCurrPhone', 'errors')}">
                                    <g:textField name="employCurrPhone" value="${fieldValue(bean: financeApplicationInstance, field: 'employCurrPhone')}" />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="employCurrPosition"><g:message code="financeApplication.employCurrPosition" default="Employ Curr Position" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: financeApplicationInstance, field: 'employCurrPosition', 'errors')}">
                                    <g:textField name="employCurrPosition" value="${fieldValue(bean: financeApplicationInstance, field: 'employCurrPosition')}" />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="employCurrYears"><g:message code="financeApplication.employCurrYears" default="Employ Curr Years" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: financeApplicationInstance, field: 'employCurrYears', 'errors')}">
                                    <g:textField name="employCurrYears" value="${fieldValue(bean: financeApplicationInstance, field: 'employCurrYears')}" />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="employPrevContact"><g:message code="financeApplication.employPrevContact" default="Employ Prev Contact" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: financeApplicationInstance, field: 'employPrevContact', 'errors')}">
                                    <g:textField name="employPrevContact" value="${fieldValue(bean: financeApplicationInstance, field: 'employPrevContact')}" />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="employPrevMonths"><g:message code="financeApplication.employPrevMonths" default="Employ Prev Months" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: financeApplicationInstance, field: 'employPrevMonths', 'errors')}">
                                    <g:textField name="employPrevMonths" value="${fieldValue(bean: financeApplicationInstance, field: 'employPrevMonths')}" />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="employPrevName"><g:message code="financeApplication.employPrevName" default="Employ Prev Name" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: financeApplicationInstance, field: 'employPrevName', 'errors')}">
                                    <g:textField name="employPrevName" value="${fieldValue(bean: financeApplicationInstance, field: 'employPrevName')}" />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="employPrevPhone"><g:message code="financeApplication.employPrevPhone" default="Employ Prev Phone" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: financeApplicationInstance, field: 'employPrevPhone', 'errors')}">
                                    <g:textField name="employPrevPhone" value="${fieldValue(bean: financeApplicationInstance, field: 'employPrevPhone')}" />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="employPrevYears"><g:message code="financeApplication.employPrevYears" default="Employ Prev Years" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: financeApplicationInstance, field: 'employPrevYears', 'errors')}">
                                    <g:textField name="employPrevYears" value="${fieldValue(bean: financeApplicationInstance, field: 'employPrevYears')}" />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="quote"><g:message code="financeApplication.quote" default="Quote" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: financeApplicationInstance, field: 'quote', 'errors')}">
                                    <g:select name="quote.id" from="${com.innovated.iris.domain.Quote.list()}" optionKey="id" value="${financeApplicationInstance?.quote?.id}"  />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="residenceCurrAddr1"><g:message code="financeApplication.residenceCurrAddr1" default="Residence Curr Addr1" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: financeApplicationInstance, field: 'residenceCurrAddr1', 'errors')}">
                                    <g:textField name="residenceCurrAddr1" value="${fieldValue(bean: financeApplicationInstance, field: 'residenceCurrAddr1')}" />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="residenceCurrAddr2"><g:message code="financeApplication.residenceCurrAddr2" default="Residence Curr Addr2" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: financeApplicationInstance, field: 'residenceCurrAddr2', 'errors')}">
                                    <g:textField name="residenceCurrAddr2" value="${fieldValue(bean: financeApplicationInstance, field: 'residenceCurrAddr2')}" />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="residenceCurrAddr3"><g:message code="financeApplication.residenceCurrAddr3" default="Residence Curr Addr3" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: financeApplicationInstance, field: 'residenceCurrAddr3', 'errors')}">
                                    <g:textField name="residenceCurrAddr3" value="${fieldValue(bean: financeApplicationInstance, field: 'residenceCurrAddr3')}" />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="residenceCurrMonths"><g:message code="financeApplication.residenceCurrMonths" default="Residence Curr Months" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: financeApplicationInstance, field: 'residenceCurrMonths', 'errors')}">
                                    <g:textField name="residenceCurrMonths" value="${fieldValue(bean: financeApplicationInstance, field: 'residenceCurrMonths')}" />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="residenceCurrPostcode"><g:message code="financeApplication.residenceCurrPostcode" default="Residence Curr Postcode" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: financeApplicationInstance, field: 'residenceCurrPostcode', 'errors')}">
                                    <g:textField name="residenceCurrPostcode" value="${fieldValue(bean: financeApplicationInstance, field: 'residenceCurrPostcode')}" />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="residenceCurrState"><g:message code="financeApplication.residenceCurrState" default="Residence Curr State" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: financeApplicationInstance, field: 'residenceCurrState', 'errors')}">
                                    <g:textField name="residenceCurrState" value="${fieldValue(bean: financeApplicationInstance, field: 'residenceCurrState')}" />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="residenceCurrStatus"><g:message code="financeApplication.residenceCurrStatus" default="Residence Curr Status" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: financeApplicationInstance, field: 'residenceCurrStatus', 'errors')}">
                                    <g:select name="residenceCurrStatus" from="${com.innovated.iris.enums.HomeOwnershipStatus?.lookup}" optionKey="key" optionValue="value" value="${financeApplicationInstance?.residenceCurrStatus?.code}"/>

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="residenceCurrThirdPartyName"><g:message code="financeApplication.residenceCurrThirdPartyName" default="Residence Curr Third Party Name" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: financeApplicationInstance, field: 'residenceCurrThirdPartyName', 'errors')}">
                                    <g:textField name="residenceCurrThirdPartyName" value="${fieldValue(bean: financeApplicationInstance, field: 'residenceCurrThirdPartyName')}" />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="residenceCurrThirdPartyPhone"><g:message code="financeApplication.residenceCurrThirdPartyPhone" default="Residence Curr Third Party Phone" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: financeApplicationInstance, field: 'residenceCurrThirdPartyPhone', 'errors')}">
                                    <g:textField name="residenceCurrThirdPartyPhone" value="${fieldValue(bean: financeApplicationInstance, field: 'residenceCurrThirdPartyPhone')}" />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="residenceCurrYears"><g:message code="financeApplication.residenceCurrYears" default="Residence Curr Years" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: financeApplicationInstance, field: 'residenceCurrYears', 'errors')}">
                                    <g:textField name="residenceCurrYears" value="${fieldValue(bean: financeApplicationInstance, field: 'residenceCurrYears')}" />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="residencePrevAddr1"><g:message code="financeApplication.residencePrevAddr1" default="Residence Prev Addr1" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: financeApplicationInstance, field: 'residencePrevAddr1', 'errors')}">
                                    <g:textField name="residencePrevAddr1" value="${fieldValue(bean: financeApplicationInstance, field: 'residencePrevAddr1')}" />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="residencePrevAddr2"><g:message code="financeApplication.residencePrevAddr2" default="Residence Prev Addr2" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: financeApplicationInstance, field: 'residencePrevAddr2', 'errors')}">
                                    <g:textField name="residencePrevAddr2" value="${fieldValue(bean: financeApplicationInstance, field: 'residencePrevAddr2')}" />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="residencePrevAddr3"><g:message code="financeApplication.residencePrevAddr3" default="Residence Prev Addr3" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: financeApplicationInstance, field: 'residencePrevAddr3', 'errors')}">
                                    <g:textField name="residencePrevAddr3" value="${fieldValue(bean: financeApplicationInstance, field: 'residencePrevAddr3')}" />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="residencePrevMonths"><g:message code="financeApplication.residencePrevMonths" default="Residence Prev Months" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: financeApplicationInstance, field: 'residencePrevMonths', 'errors')}">
                                    <g:textField name="residencePrevMonths" value="${fieldValue(bean: financeApplicationInstance, field: 'residencePrevMonths')}" />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="residencePrevPostcode"><g:message code="financeApplication.residencePrevPostcode" default="Residence Prev Postcode" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: financeApplicationInstance, field: 'residencePrevPostcode', 'errors')}">
                                    <g:textField name="residencePrevPostcode" value="${fieldValue(bean: financeApplicationInstance, field: 'residencePrevPostcode')}" />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="residencePrevState"><g:message code="financeApplication.residencePrevState" default="Residence Prev State" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: financeApplicationInstance, field: 'residencePrevState', 'errors')}">
                                    <g:textField name="residencePrevState" value="${fieldValue(bean: financeApplicationInstance, field: 'residencePrevState')}" />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="residencePrevYears"><g:message code="financeApplication.residencePrevYears" default="Residence Prev Years" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: financeApplicationInstance, field: 'residencePrevYears', 'errors')}">
                                    <g:textField name="residencePrevYears" value="${fieldValue(bean: financeApplicationInstance, field: 'residencePrevYears')}" />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="status"><g:message code="financeApplication.status" default="Status" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: financeApplicationInstance, field: 'status', 'errors')}">
                                    <g:select name="status" from="${com.innovated.iris.enums.application.FinanceApplicationStatus?.lookup}" optionKey="key" optionValue="value" value="${financeApplicationInstance?.status?.code}" />
                                </td>
                            </tr>
                        
                        </tbody>
                    </table>
                </div>
                <div class="buttons">
                    <span class="button"><g:submitButton name="create" class="save" value="${message(code: 'create', 'default': 'Create')}" /></span>
                </div>
            </g:form>
        </div>
    </body>
</html>
