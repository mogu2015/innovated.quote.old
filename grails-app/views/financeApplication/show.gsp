
<%@ page import="com.innovated.iris.domain.application.FinanceApplication" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="prod" />
        <title><g:message code="financeApplication.show" default="Show FinanceApplication" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLinkTo(dir: '')}"><g:message code="home" default="Home" /></a></span>
            <span class="menuButton"><g:link class="list" action="list"><g:message code="financeApplication.list" default="FinanceApplication List" /></g:link></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="financeApplication.new" default="New FinanceApplication" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="financeApplication.show" default="Show FinanceApplication" /></h1>
            <g:if test="${flash.message}">
            <div class="message"><g:message code="${flash.message}" args="${flash.args}" default="${flash.defaultMessage}" /></div>
            </g:if>
            <g:form>
                <g:hiddenField name="id" value="${financeApplicationInstance?.id}" />
                <div class="dialog">
                    <table>
                        <tbody>
                        
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="financeApplication.id" default="Id" />:</td>
                                
                                <td valign="top" class="value">${fieldValue(bean: financeApplicationInstance, field: "id")}</td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="financeApplication.applicantDateOfBirth" default="Applicant Date Of Birth" />:</td>
                                
                                <td valign="top" class="value"><g:formatDate date="${financeApplicationInstance?.applicantDateOfBirth}" /></td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="financeApplication.applicantDriversLicenceNum" default="Applicant Drivers Licence Num" />:</td>
                                
                                <td valign="top" class="value">${fieldValue(bean: financeApplicationInstance, field: "applicantDriversLicenceNum")}</td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="financeApplication.applicantEmail" default="Applicant Email" />:</td>
                                
                                <td valign="top" class="value">${fieldValue(bean: financeApplicationInstance, field: "applicantEmail")}</td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="financeApplication.applicantFirstName" default="Applicant First Name" />:</td>
                                
                                <td valign="top" class="value">${fieldValue(bean: financeApplicationInstance, field: "applicantFirstName")}</td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="financeApplication.applicantLastName" default="Applicant Last Name" />:</td>
                                
                                <td valign="top" class="value">${fieldValue(bean: financeApplicationInstance, field: "applicantLastName")}</td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="financeApplication.applicantMaritalStatus" default="Applicant Marital Status" />:</td>
                                
                                <td valign="top" class="value">${fieldValue(bean: financeApplicationInstance, field: "applicantMaritalStatus")}</td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="financeApplication.applicantMiddleName" default="Applicant Middle Name" />:</td>
                                
                                <td valign="top" class="value">${fieldValue(bean: financeApplicationInstance, field: "applicantMiddleName")}</td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="financeApplication.applicantPhoneHome" default="Applicant Phone Home" />:</td>
                                
                                <td valign="top" class="value">${fieldValue(bean: financeApplicationInstance, field: "applicantPhoneHome")}</td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="financeApplication.applicantPhoneMobile" default="Applicant Phone Mobile" />:</td>
                                
                                <td valign="top" class="value">${fieldValue(bean: financeApplicationInstance, field: "applicantPhoneMobile")}</td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="financeApplication.applicantPhoneWork" default="Applicant Phone Work" />:</td>
                                
                                <td valign="top" class="value">${fieldValue(bean: financeApplicationInstance, field: "applicantPhoneWork")}</td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="financeApplication.applicantTitle" default="Applicant Title" />:</td>
                                
                                <td valign="top" class="value">${fieldValue(bean: financeApplicationInstance, field: "applicantTitle")}</td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="financeApplication.assetItems" default="Asset Items" />:</td>
                                
                                <td  valign="top" style="text-align: left;" class="value">
                                    <ul>
                                    <g:each in="${financeApplicationInstance?.assetItems}" var="financeApplicationAssetInstance">
                                        <li><g:link controller="financeApplicationAsset" action="show" id="${financeApplicationAssetInstance.id}">${financeApplicationAssetInstance.encodeAsHTML()}</g:link></li>
                                    </g:each>
                                    </ul>
                                </td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="financeApplication.employCurrContact" default="Employ Curr Contact" />:</td>
                                
                                <td valign="top" class="value">${fieldValue(bean: financeApplicationInstance, field: "employCurrContact")}</td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="financeApplication.employCurrMonths" default="Employ Curr Months" />:</td>
                                
                                <td valign="top" class="value">${fieldValue(bean: financeApplicationInstance, field: "employCurrMonths")}</td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="financeApplication.employCurrName" default="Employ Curr Name" />:</td>
                                
                                <td valign="top" class="value">${fieldValue(bean: financeApplicationInstance, field: "employCurrName")}</td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="financeApplication.employCurrPhone" default="Employ Curr Phone" />:</td>
                                
                                <td valign="top" class="value">${fieldValue(bean: financeApplicationInstance, field: "employCurrPhone")}</td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="financeApplication.employCurrPosition" default="Employ Curr Position" />:</td>
                                
                                <td valign="top" class="value">${fieldValue(bean: financeApplicationInstance, field: "employCurrPosition")}</td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="financeApplication.employCurrYears" default="Employ Curr Years" />:</td>
                                
                                <td valign="top" class="value">${fieldValue(bean: financeApplicationInstance, field: "employCurrYears")}</td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="financeApplication.employPrevContact" default="Employ Prev Contact" />:</td>
                                
                                <td valign="top" class="value">${fieldValue(bean: financeApplicationInstance, field: "employPrevContact")}</td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="financeApplication.employPrevMonths" default="Employ Prev Months" />:</td>
                                
                                <td valign="top" class="value">${fieldValue(bean: financeApplicationInstance, field: "employPrevMonths")}</td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="financeApplication.employPrevName" default="Employ Prev Name" />:</td>
                                
                                <td valign="top" class="value">${fieldValue(bean: financeApplicationInstance, field: "employPrevName")}</td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="financeApplication.employPrevPhone" default="Employ Prev Phone" />:</td>
                                
                                <td valign="top" class="value">${fieldValue(bean: financeApplicationInstance, field: "employPrevPhone")}</td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="financeApplication.employPrevYears" default="Employ Prev Years" />:</td>
                                
                                <td valign="top" class="value">${fieldValue(bean: financeApplicationInstance, field: "employPrevYears")}</td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="financeApplication.expenseItems" default="Expense Items" />:</td>
                                
                                <td  valign="top" style="text-align: left;" class="value">
                                    <ul>
                                    <g:each in="${financeApplicationInstance?.expenseItems}" var="financeApplicationExpenseInstance">
                                        <li><g:link controller="financeApplicationExpense" action="show" id="${financeApplicationExpenseInstance.id}">${financeApplicationExpenseInstance.encodeAsHTML()}</g:link></li>
                                    </g:each>
                                    </ul>
                                </td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="financeApplication.incomeItems" default="Income Items" />:</td>
                                
                                <td  valign="top" style="text-align: left;" class="value">
                                    <ul>
                                    <g:each in="${financeApplicationInstance?.incomeItems}" var="financeApplicationIncomeInstance">
                                        <li><g:link controller="financeApplicationIncome" action="show" id="${financeApplicationIncomeInstance.id}">${financeApplicationIncomeInstance.encodeAsHTML()}</g:link></li>
                                    </g:each>
                                    </ul>
                                </td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="financeApplication.liabilityItems" default="Liability Items" />:</td>
                                
                                <td  valign="top" style="text-align: left;" class="value">
                                    <ul>
                                    <g:each in="${financeApplicationInstance?.liabilityItems}" var="financeApplicationLiabilityInstance">
                                        <li><g:link controller="financeApplicationLiability" action="show" id="${financeApplicationLiabilityInstance.id}">${financeApplicationLiabilityInstance.encodeAsHTML()}</g:link></li>
                                    </g:each>
                                    </ul>
                                </td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="financeApplication.quote" default="Quote" />:</td>
                                
                                <td valign="top" class="value"><g:link controller="quote" action="show" id="${financeApplicationInstance?.quote?.id}">${financeApplicationInstance?.quote?.encodeAsHTML()}</g:link></td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="financeApplication.residenceCurrAddr1" default="Residence Curr Addr1" />:</td>
                                
                                <td valign="top" class="value">${fieldValue(bean: financeApplicationInstance, field: "residenceCurrAddr1")}</td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="financeApplication.residenceCurrAddr2" default="Residence Curr Addr2" />:</td>
                                
                                <td valign="top" class="value">${fieldValue(bean: financeApplicationInstance, field: "residenceCurrAddr2")}</td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="financeApplication.residenceCurrAddr3" default="Residence Curr Addr3" />:</td>
                                
                                <td valign="top" class="value">${fieldValue(bean: financeApplicationInstance, field: "residenceCurrAddr3")}</td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="financeApplication.residenceCurrMonths" default="Residence Curr Months" />:</td>
                                
                                <td valign="top" class="value">${fieldValue(bean: financeApplicationInstance, field: "residenceCurrMonths")}</td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="financeApplication.residenceCurrPostcode" default="Residence Curr Postcode" />:</td>
                                
                                <td valign="top" class="value">${fieldValue(bean: financeApplicationInstance, field: "residenceCurrPostcode")}</td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="financeApplication.residenceCurrState" default="Residence Curr State" />:</td>
                                
                                <td valign="top" class="value">${fieldValue(bean: financeApplicationInstance, field: "residenceCurrState")}</td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="financeApplication.residenceCurrStatus" default="Residence Curr Status" />:</td>
                                
                                <td valign="top" class="value">${financeApplicationInstance?.residenceCurrStatus?.encodeAsHTML()}</td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="financeApplication.residenceCurrThirdPartyName" default="Residence Curr Third Party Name" />:</td>
                                
                                <td valign="top" class="value">${fieldValue(bean: financeApplicationInstance, field: "residenceCurrThirdPartyName")}</td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="financeApplication.residenceCurrThirdPartyPhone" default="Residence Curr Third Party Phone" />:</td>
                                
                                <td valign="top" class="value">${fieldValue(bean: financeApplicationInstance, field: "residenceCurrThirdPartyPhone")}</td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="financeApplication.residenceCurrYears" default="Residence Curr Years" />:</td>
                                
                                <td valign="top" class="value">${fieldValue(bean: financeApplicationInstance, field: "residenceCurrYears")}</td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="financeApplication.residencePrevAddr1" default="Residence Prev Addr1" />:</td>
                                
                                <td valign="top" class="value">${fieldValue(bean: financeApplicationInstance, field: "residencePrevAddr1")}</td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="financeApplication.residencePrevAddr2" default="Residence Prev Addr2" />:</td>
                                
                                <td valign="top" class="value">${fieldValue(bean: financeApplicationInstance, field: "residencePrevAddr2")}</td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="financeApplication.residencePrevAddr3" default="Residence Prev Addr3" />:</td>
                                
                                <td valign="top" class="value">${fieldValue(bean: financeApplicationInstance, field: "residencePrevAddr3")}</td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="financeApplication.residencePrevMonths" default="Residence Prev Months" />:</td>
                                
                                <td valign="top" class="value">${fieldValue(bean: financeApplicationInstance, field: "residencePrevMonths")}</td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="financeApplication.residencePrevPostcode" default="Residence Prev Postcode" />:</td>
                                
                                <td valign="top" class="value">${fieldValue(bean: financeApplicationInstance, field: "residencePrevPostcode")}</td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="financeApplication.residencePrevState" default="Residence Prev State" />:</td>
                                
                                <td valign="top" class="value">${fieldValue(bean: financeApplicationInstance, field: "residencePrevState")}</td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="financeApplication.residencePrevYears" default="Residence Prev Years" />:</td>
                                
                                <td valign="top" class="value">${fieldValue(bean: financeApplicationInstance, field: "residencePrevYears")}</td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="financeApplication.status" default="Status" />:</td>
                                
                                <td valign="top" class="value">${financeApplicationInstance?.status?.encodeAsHTML()}</td>
                                
                            </tr>
                            
                        </tbody>
                    </table>
                </div>
                <div class="buttons">
                    <span class="button"><g:actionSubmit class="edit" action="edit" value="${message(code: 'edit', 'default': 'Edit')}" /></span>
                    <span class="button"><g:actionSubmit class="delete" action="delete" value="${message(code: 'delete', 'default': 'Delete')}" onclick="return confirm('${message(code: 'delete.confirm', 'default': 'Are you sure?')}');" /></span>
                </div>
            </g:form>
        </div>
    </body>
</html>
