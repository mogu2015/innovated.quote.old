
<%@ page import="com.innovated.iris.domain.application.FinanceApplication" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="prod" />
        <title><g:message code="financeApplication.list" default="FinanceApplication List" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLinkTo(dir: '')}"><g:message code="home" default="Home" /></a></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="financeApplication.new" default="New FinanceApplication" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="financeApplication.list" default="FinanceApplication List" /></h1>
            <g:if test="${flash.message}">
            <div class="message"><g:message code="${flash.message}" args="${flash.args}" default="${flash.defaultMessage}" /></div>
            </g:if>
            <div class="list">
                <table>
                    <thead>
                        <tr>
                        
                   	    <g:sortableColumn property="id" title="Id" titleKey="financeApplication.id" />
                        
                   	    <g:sortableColumn property="applicantDateOfBirth" title="Applicant Date Of Birth" titleKey="financeApplication.applicantDateOfBirth" />
                        
                   	    <g:sortableColumn property="applicantDriversLicenceNum" title="Applicant Drivers Licence Num" titleKey="financeApplication.applicantDriversLicenceNum" />
                        
                   	    <g:sortableColumn property="applicantEmail" title="Applicant Email" titleKey="financeApplication.applicantEmail" />
                        
                   	    <g:sortableColumn property="applicantFirstName" title="Applicant First Name" titleKey="financeApplication.applicantFirstName" />
                        
                   	    <g:sortableColumn property="applicantLastName" title="Applicant Last Name" titleKey="financeApplication.applicantLastName" />
                   	    
                   	    <g:sortableColumn property="quote" title="Quote" titleKey="financeApplication.quote" />
                        
                        </tr>
                    </thead>
                    <tbody>
                    <g:each in="${financeApplicationInstanceList}" status="i" var="financeApplicationInstance">
                        <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
                        
                            <td><g:link action="show" id="${financeApplicationInstance.id}">${fieldValue(bean: financeApplicationInstance, field: "id")}</g:link></td>
                        
                            <td><g:formatDate date="${financeApplicationInstance.applicantDateOfBirth}" /></td>
                        
                            <td>${fieldValue(bean: financeApplicationInstance, field: "applicantDriversLicenceNum")}</td>
                        
                            <td>${fieldValue(bean: financeApplicationInstance, field: "applicantEmail")}</td>
                        
                            <td>${fieldValue(bean: financeApplicationInstance, field: "applicantFirstName")}</td>
                        
                            <td>${fieldValue(bean: financeApplicationInstance, field: "applicantLastName")}</td>
                            
                            <td><g:formatNumber number="${financeApplicationInstance?.quote?.id}" format="${message(code:'quote.number.mask')}" /></td>
                        
                        </tr>
                    </g:each>
                    </tbody>
                </table>
            </div>
            <div class="paginateButtons">
                <g:paginate total="${financeApplicationInstanceTotal}" />
            </div>
        </div>
    </body>
</html>
