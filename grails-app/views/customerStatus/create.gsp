
<%@ page import="com.innovated.iris.domain.enums.CustomerStatus" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <title><g:message code="customerStatus.create" default="Create CustomerStatus" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLinkTo(dir: '')}"><g:message code="home" default="Home" /></a></span>
            <span class="menuButton"><g:link class="list" action="list"><g:message code="customerStatus.list" default="CustomerStatus List" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="customerStatus.create" default="Create CustomerStatus" /></h1>
            <g:if test="${flash.message}">
            	<div class="message"><g:message code="${flash.message}" args="${flash.args}" default="${flash.defaultMessage}" /></div>
            </g:if>
            <g:hasErrors bean="${customerStatusInstance}">
				<div class="errors">
					<g:renderErrors bean="${customerStatusInstance}" as="list" />
				</div>
            </g:hasErrors>
            <g:form action="save" method="post" >
                <div class="dialog">
                    <fieldset>
                        <legend><g:message code="customerStatus.create.legend" default="Enter CustomerStatus Details"/></legend>
                        
                            <div class="prop mandatory ${hasErrors(bean: customerStatusInstance, field: 'name', 'error')}">
                                <label for="name">
                                    <g:message code="customerStatus.name" default="Name" />
                                    <span class="indicator">*</span>
                                </label>
                                <input type="text" id="name" name="name" value="${fieldValue(bean:customerStatusInstance,field:'name')}"/>
                            </div>
                        
                            <div class="prop mandatory ${hasErrors(bean: customerStatusInstance, field: 'statusCode', 'error')}">
                                <label for="statusCode">
                                    <g:message code="customerStatus.statusCode" default="Status Code" />
                                    <span class="indicator">*</span>
                                </label>
                                <input type="text" id="statusCode" name="statusCode" value="${fieldValue(bean:customerStatusInstance,field:'statusCode')}"/>
                            </div>
                        
                            <div class="prop mandatory ${hasErrors(bean: customerStatusInstance, field: 'visible', 'error')}">
                                <label for="visible">
                                    <g:message code="customerStatus.visible" default="Visible" />
                                    <span class="indicator">*</span>
                                </label>
                                <g:checkBox name="visible" value="${customerStatusInstance?.visible}" ></g:checkBox>
                            </div>
                        
                            <div class="prop mandatory ${hasErrors(bean: customerStatusInstance, field: 'enabled', 'error')}">
                                <label for="enabled">
                                    <g:message code="customerStatus.enabled" default="Enabled" />
                                    <span class="indicator">*</span>
                                </label>
                                <g:checkBox name="enabled" value="${customerStatusInstance?.enabled}" ></g:checkBox>
                            </div>
                        
                            <div class="prop mandatory ${hasErrors(bean: customerStatusInstance, field: 'dateCreated', 'error')}">
                                <label for="dateCreated">
                                    <g:message code="customerStatus.dateCreated" default="Date Created" />
                                    <span class="indicator">*</span>
                                </label>
                                <g:datePicker name="dateCreated" value="${customerStatusInstance?.dateCreated}" precision="minute" ></g:datePicker>
                            </div>
                        
                            <div class="prop mandatory ${hasErrors(bean: customerStatusInstance, field: 'lastUpdated', 'error')}">
                                <label for="lastUpdated">
                                    <g:message code="customerStatus.lastUpdated" default="Last Updated" />
                                    <span class="indicator">*</span>
                                </label>
                                <g:datePicker name="lastUpdated" value="${customerStatusInstance?.lastUpdated}" precision="minute" ></g:datePicker>
                            </div>
                        
                    </fieldset>
                </div>
                <div class="buttons">
                    <span class="button"><g:submitButton name="create" class="save" value="${message(code: 'create', 'default': 'Create')}" /></span>
                </div>
            </g:form>
        </div>
    </body>
</html>

