
<%@ page import="com.innovated.iris.domain.lookup.FbtCentsPerKmMethodRateParameter" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <title><g:message code="fbtCentsPerKmMethodRateParameter.list" default="FbtCentsPerKmMethodRateParameter List" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLinkTo(dir: '')}"><g:message code="home" default="Home" /></a></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="fbtCentsPerKmMethodRateParameter.new" default="New FbtCentsPerKmMethodRateParameter" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="fbtCentsPerKmMethodRateParameter.list" default="FbtCentsPerKmMethodRateParameter List" /></h1>
            <g:if test="${flash.message}">
            <div class="message"><g:message code="${flash.message}" args="${flash.args}" default="${flash.defaultMessage}" /></div>
            </g:if>
            <div class="list">
                <table>
                    <thead>
                        <tr>
                        
                   	    <g:sortableColumn property="id" title="Id" titleKey="fbtCentsPerKmMethodRateParameter.id" />
                        
                   	    <g:sortableColumn property="dateFrom" title="Date From" titleKey="fbtCentsPerKmMethodRateParameter.dateFrom" />
                        
                   	    <g:sortableColumn property="customerCode" title="Customer Code" titleKey="fbtCentsPerKmMethodRateParameter.customerCode" />
                        
                   	    <g:sortableColumn property="taxableValueTypeCode" title="Taxable Value Type Code" titleKey="fbtCentsPerKmMethodRateParameter.taxableValueTypeCode" />
                        
                   	    <g:sortableColumn property="centsPerKm" title="Cents Per Km" titleKey="fbtCentsPerKmMethodRateParameter.centsPerKm" />
                        
                   	    <g:sortableColumn property="lastUpdated" title="Last Updated" titleKey="fbtCentsPerKmMethodRateParameter.lastUpdated" />
                        
                        </tr>
                    </thead>
                    <tbody>
                    <g:each in="${fbtCentsPerKmMethodRateParameterInstanceList}" status="i" var="fbtCentsPerKmMethodRateParameterInstance">
                        <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
                        
                            <td><g:link action="show" id="${fbtCentsPerKmMethodRateParameterInstance.id}">${fieldValue(bean: fbtCentsPerKmMethodRateParameterInstance, field: "id")}</g:link></td>
                        
                            <td><g:formatDate date="${fbtCentsPerKmMethodRateParameterInstance.dateFrom}" /></td>
                        
                            <td>${fieldValue(bean: fbtCentsPerKmMethodRateParameterInstance, field: "customerCode")}</td>
                        
                            <td>${fieldValue(bean: fbtCentsPerKmMethodRateParameterInstance, field: "taxableValueTypeCode")}</td>
                        
                            <td>${fieldValue(bean: fbtCentsPerKmMethodRateParameterInstance, field: "centsPerKm")}</td>
                        
                            <td><g:formatDate date="${fbtCentsPerKmMethodRateParameterInstance.lastUpdated}" /></td>
                        
                        </tr>
                    </g:each>
                    </tbody>
                </table>
            </div>
            <div class="paginateButtons">
                <g:paginate total="${fbtCentsPerKmMethodRateParameterInstanceTotal}" />
            </div>
        </div>
    </body>
</html>
