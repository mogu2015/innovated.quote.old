
<%@ page import="com.innovated.iris.domain.lookup.FbtCentsPerKmMethodRateParameter" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <title><g:message code="fbtCentsPerKmMethodRateParameter.show" default="Show FbtCentsPerKmMethodRateParameter" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLinkTo(dir: '')}"><g:message code="home" default="Home" /></a></span>
            <span class="menuButton"><g:link class="list" action="list"><g:message code="fbtCentsPerKmMethodRateParameter.list" default="FbtCentsPerKmMethodRateParameter List" /></g:link></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="fbtCentsPerKmMethodRateParameter.new" default="New FbtCentsPerKmMethodRateParameter" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="fbtCentsPerKmMethodRateParameter.show" default="Show FbtCentsPerKmMethodRateParameter" /></h1>
            <g:if test="${flash.message}">
            <div class="message"><g:message code="${flash.message}" args="${flash.args}" default="${flash.defaultMessage}" /></div>
            </g:if>
            <g:form>
                <g:hiddenField name="id" value="${fbtCentsPerKmMethodRateParameterInstance?.id}" />
                <div class="dialog">
                    <table>
                        <tbody>
                        
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="fbtCentsPerKmMethodRateParameter.id" default="Id" />:</td>
                                
                                <td valign="top" class="value">${fieldValue(bean: fbtCentsPerKmMethodRateParameterInstance, field: "id")}</td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="fbtCentsPerKmMethodRateParameter.dateFrom" default="Date From" />:</td>
                                
                                <td valign="top" class="value"><g:formatDate date="${fbtCentsPerKmMethodRateParameterInstance?.dateFrom}" /></td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="fbtCentsPerKmMethodRateParameter.customerCode" default="Customer Code" />:</td>
                                
                                <td valign="top" class="value">${fieldValue(bean: fbtCentsPerKmMethodRateParameterInstance, field: "customerCode")}</td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="fbtCentsPerKmMethodRateParameter.taxableValueTypeCode" default="Taxable Value Type Code" />:</td>
                                
                                <td valign="top" class="value">${fieldValue(bean: fbtCentsPerKmMethodRateParameterInstance, field: "taxableValueTypeCode")}</td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="fbtCentsPerKmMethodRateParameter.centsPerKm" default="Cents Per Km" />:</td>
                                
                                <td valign="top" class="value">${fieldValue(bean: fbtCentsPerKmMethodRateParameterInstance, field: "centsPerKm")}</td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="fbtCentsPerKmMethodRateParameter.lastUpdated" default="Last Updated" />:</td>
                                
                                <td valign="top" class="value"><g:formatDate date="${fbtCentsPerKmMethodRateParameterInstance?.lastUpdated}" /></td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="fbtCentsPerKmMethodRateParameter.dateCreated" default="Date Created" />:</td>
                                
                                <td valign="top" class="value"><g:formatDate date="${fbtCentsPerKmMethodRateParameterInstance?.dateCreated}" /></td>
                                
                            </tr>
                            
                        </tbody>
                    </table>
                </div>
                <div class="buttons">
                    <span class="button"><g:actionSubmit class="edit" action="edit" value="${message(code: 'edit', 'default': 'Edit')}" /></span>
                    <span class="button"><g:actionSubmit class="delete" action="delete" value="${message(code: 'delete', 'default': 'Delete')}" onclick="return confirm('${message(code: 'delete.confirm', 'default': 'Are you sure?')}');" /></span>
                </div>
            </g:form>
        </div>
    </body>
</html>
