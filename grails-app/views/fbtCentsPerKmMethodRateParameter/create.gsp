
<%@ page import="com.innovated.iris.domain.lookup.FbtCentsPerKmMethodRateParameter" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <title><g:message code="fbtCentsPerKmMethodRateParameter.create" default="Create FbtCentsPerKmMethodRateParameter" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLinkTo(dir: '')}"><g:message code="home" default="Home" /></a></span>
            <span class="menuButton"><g:link class="list" action="list"><g:message code="fbtCentsPerKmMethodRateParameter.list" default="FbtCentsPerKmMethodRateParameter List" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="fbtCentsPerKmMethodRateParameter.create" default="Create FbtCentsPerKmMethodRateParameter" /></h1>
            <g:if test="${flash.message}">
            <div class="message"><g:message code="${flash.message}" args="${flash.args}" default="${flash.defaultMessage}" /></div>
            </g:if>
            <g:hasErrors bean="${fbtCentsPerKmMethodRateParameterInstance}">
            <div class="errors">
                <g:renderErrors bean="${fbtCentsPerKmMethodRateParameterInstance}" as="list" />
            </div>
            </g:hasErrors>
            <g:form action="save" method="post" >
                <div class="dialog">
                    <table>
                        <tbody>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="dateFrom"><g:message code="fbtCentsPerKmMethodRateParameter.dateFrom" default="Date From" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: fbtCentsPerKmMethodRateParameterInstance, field: 'dateFrom', 'errors')}">
                                    <g:datePicker name="dateFrom" value="${fbtCentsPerKmMethodRateParameterInstance?.dateFrom}"  />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="customerCode"><g:message code="fbtCentsPerKmMethodRateParameter.customerCode" default="Customer Code" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: fbtCentsPerKmMethodRateParameterInstance, field: 'customerCode', 'errors')}">
                                    <g:textField name="customerCode" value="${fieldValue(bean: fbtCentsPerKmMethodRateParameterInstance, field: 'customerCode')}" />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="taxableValueTypeCode"><g:message code="fbtCentsPerKmMethodRateParameter.taxableValueTypeCode" default="Taxable Value Type Code" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: fbtCentsPerKmMethodRateParameterInstance, field: 'taxableValueTypeCode', 'errors')}">
                                    <g:textField name="taxableValueTypeCode" value="${fieldValue(bean: fbtCentsPerKmMethodRateParameterInstance, field: 'taxableValueTypeCode')}" />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="centsPerKm"><g:message code="fbtCentsPerKmMethodRateParameter.centsPerKm" default="Cents Per Km" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: fbtCentsPerKmMethodRateParameterInstance, field: 'centsPerKm', 'errors')}">
                                    <g:select name="centsPerKm" from="${1.0..100.0}" value="${fbtCentsPerKmMethodRateParameterInstance?.centsPerKm}"  />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="lastUpdated"><g:message code="fbtCentsPerKmMethodRateParameter.lastUpdated" default="Last Updated" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: fbtCentsPerKmMethodRateParameterInstance, field: 'lastUpdated', 'errors')}">
                                    <g:datePicker name="lastUpdated" value="${fbtCentsPerKmMethodRateParameterInstance?.lastUpdated}"  />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="dateCreated"><g:message code="fbtCentsPerKmMethodRateParameter.dateCreated" default="Date Created" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: fbtCentsPerKmMethodRateParameterInstance, field: 'dateCreated', 'errors')}">
                                    <g:datePicker name="dateCreated" value="${fbtCentsPerKmMethodRateParameterInstance?.dateCreated}"  />

                                </td>
                            </tr>
                        
                        </tbody>
                    </table>
                </div>
                <div class="buttons">
                    <span class="button"><g:submitButton name="create" class="save" value="${message(code: 'create', 'default': 'Create')}" /></span>
                </div>
            </g:form>
        </div>
    </body>
</html>
