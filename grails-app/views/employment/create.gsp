
<%@ page import="com.innovated.iris.domain.Employment" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <title><g:message code="employment.create" default="Create Employment" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLinkTo(dir: '')}"><g:message code="home" default="Home" /></a></span>
            <span class="menuButton"><g:link class="list" action="list"><g:message code="employment.list" default="Employment List" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="employment.create" default="Create Employment" /></h1>
            <g:if test="${flash.message}">
            	<div class="message"><g:message code="${flash.message}" args="${flash.args}" default="${flash.defaultMessage}" /></div>
            </g:if>
            <g:hasErrors bean="${employmentInstance}">
				<div class="errors">
					<g:renderErrors bean="${employmentInstance}" as="list" />
				</div>
            </g:hasErrors>
            <g:form action="save" method="post" >
                <div class="dialog">
                    <fieldset>
                        <legend><g:message code="employment.create.legend" default="Enter Employment Details"/></legend>
                        
                            <div class="prop mandatory ${hasErrors(bean: employmentInstance, field: 'employer', 'error')}">
                                <label for="employer">
                                    <g:message code="employment.employer" default="Employer" />
                                    <span class="indicator">*</span>
                                </label>
                                <g:select name="employer.id" from="${com.innovated.iris.domain.Company.list()}" optionKey="id" value="${employmentInstance?.employer?.id}"  />

                            </div>
                        
                            <div class="prop mandatory ${hasErrors(bean: employmentInstance, field: 'employee', 'error')}">
                                <label for="employee">
                                    <g:message code="employment.employee" default="Employee" />
                                    <span class="indicator">*</span>
                                </label>
                                <g:select name="employee.id" from="${com.innovated.iris.domain.Individual.list()}" optionKey="id" value="${employmentInstance?.employee?.id}"  />

                            </div>
                        
                            <div class="prop mandatory ${hasErrors(bean: employmentInstance, field: 'jobTitle', 'error')}">
                                <label for="jobTitle">
                                    <g:message code="employment.jobTitle" default="Job Title" />
                                    <span class="indicator">*</span>
                                </label>
                                <g:textField name="jobTitle" value="${fieldValue(bean: employmentInstance, field: 'jobTitle')}" />

                            </div>
                        
                            <div class="prop ${hasErrors(bean: employmentInstance, field: 'jobDescription', 'error')}">
                                <label for="jobDescription">
                                    <g:message code="employment.jobDescription" default="Job Description" />
                                    
                                </label>
                                <g:textField name="jobDescription" value="${fieldValue(bean: employmentInstance, field: 'jobDescription')}" />

                            </div>
                        
                            <div class="prop ${hasErrors(bean: employmentInstance, field: 'staffNumber', 'error')}">
                                <label for="staffNumber">
                                    <g:message code="employment.staffNumber" default="Staff Number" />
                                    
                                </label>
                                <g:textField name="staffNumber" value="${fieldValue(bean: employmentInstance, field: 'staffNumber')}" />

                            </div>
                        
                            <div class="prop mandatory ${hasErrors(bean: employmentInstance, field: 'startedEmployment', 'error')}">
                                <label for="startedEmployment">
                                    <g:message code="employment.startedEmployment" default="Started Employment" />
                                    <span class="indicator">*</span>
                                </label>
                                <g:datePicker name="startedEmployment" value="${employmentInstance?.startedEmployment}" precision="day" />

                            </div>
                        
                            <div class="prop ${hasErrors(bean: employmentInstance, field: 'ceasedEmployment', 'error')}">
                                <label for="ceasedEmployment">
                                    <g:message code="employment.ceasedEmployment" default="Ceased Employment" />
                                    
                                </label>
                                <g:datePicker name="ceasedEmployment" value="${employmentInstance?.ceasedEmployment}" noSelection="['': '']" precision="day" />

                            </div>
                        
                            <div class="prop mandatory ${hasErrors(bean: employmentInstance, field: 'grossSalary', 'error')}">
                                <label for="grossSalary">
                                    <g:message code="employment.grossSalary" default="Gross Salary" />
                                    <span class="indicator">*</span>
                                </label>
                                <g:textField name="grossSalary" value="${fieldValue(bean: employmentInstance, field: 'grossSalary')}" />

                            </div>
                        <!--
                            <div class="prop mandatory ${hasErrors(bean: employmentInstance, field: 'dateCreated', 'error')}">
                                <label for="dateCreated">
                                    <g:message code="employment.dateCreated" default="Date Created" />
                                    <span class="indicator">*</span>
                                </label>
                                <g:datePicker name="dateCreated" value="${employmentInstance?.dateCreated}"  />

                            </div>
                        
                            <div class="prop mandatory ${hasErrors(bean: employmentInstance, field: 'lastUpdated', 'error')}">
                                <label for="lastUpdated">
                                    <g:message code="employment.lastUpdated" default="Last Updated" />
                                    <span class="indicator">*</span>
                                </label>
                                <g:datePicker name="lastUpdated" value="${employmentInstance?.lastUpdated}"  />

                            </div>
                        -->
                    </fieldset>
                </div>
                <div class="buttons">
                    <span class="button"><g:submitButton name="create" class="save" value="${message(code: 'create', 'default': 'Create')}" /></span>
                </div>
            </g:form>
        </div>
    </body>
</html>

