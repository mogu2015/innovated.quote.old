
<%@ page import="com.innovated.iris.domain.Employment" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <title><g:message code="employment.edit" default="Edit Employment" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLinkTo(dir: '')}"><g:message code="home" default="Home" /></a></span>
            <span class="menuButton"><g:link class="list" action="list"><g:message code="employment.list" default="Employment List" /></g:link></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="employment.new" default="New Employment" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="employment.edit" default="Edit Employment" /></h1>
            <g:if test="${flash.message}">
            <div class="message"><g:message code="${flash.message}" args="${flash.args}" default="${flash.defaultMessage}" /></div>
            </g:if>
            <g:hasErrors bean="${employmentInstance}">
            <div class="errors">
                <g:renderErrors bean="${employmentInstance}" as="list" />
            </div>
            </g:hasErrors>
            <g:form method="post" >
                <g:hiddenField name="id" value="${employmentInstance?.id}" />
                <g:hiddenField name="version" value="${employmentInstance?.version}" />
                <div class="dialog">
                    <table>
                        <tbody>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="employer"><g:message code="employment.employer" default="Employer" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: employmentInstance, field: 'employer', 'errors')}">
                                    <g:select name="employer.id" from="${com.innovated.iris.domain.Company.list()}" optionKey="id" value="${employmentInstance?.employer?.id}"  />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="employee"><g:message code="employment.employee" default="Employee" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: employmentInstance, field: 'employee', 'errors')}">
                                    <g:select name="employee.id" from="${com.innovated.iris.domain.Individual.list()}" optionKey="id" value="${employmentInstance?.employee?.id}"  />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="jobTitle"><g:message code="employment.jobTitle" default="Job Title" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: employmentInstance, field: 'jobTitle', 'errors')}">
                                    <g:textField name="jobTitle" value="${fieldValue(bean: employmentInstance, field: 'jobTitle')}" />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="jobDescription"><g:message code="employment.jobDescription" default="Job Description" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: employmentInstance, field: 'jobDescription', 'errors')}">
                                    <g:textField name="jobDescription" value="${fieldValue(bean: employmentInstance, field: 'jobDescription')}" />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="staffNumber"><g:message code="employment.staffNumber" default="Staff Number" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: employmentInstance, field: 'staffNumber', 'errors')}">
                                    <g:textField name="staffNumber" value="${fieldValue(bean: employmentInstance, field: 'staffNumber')}" />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="startedEmployment"><g:message code="employment.startedEmployment" default="Started Employment" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: employmentInstance, field: 'startedEmployment', 'errors')}">
                                    <g:datePicker name="startedEmployment" value="${employmentInstance?.startedEmployment}"  />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="ceasedEmployment"><g:message code="employment.ceasedEmployment" default="Ceased Employment" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: employmentInstance, field: 'ceasedEmployment', 'errors')}">
                                    <g:datePicker name="ceasedEmployment" value="${employmentInstance?.ceasedEmployment}" noSelection="['': '']" />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="grossSalary"><g:message code="employment.grossSalary" default="Gross Salary" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: employmentInstance, field: 'grossSalary', 'errors')}">
                                    <g:textField name="grossSalary" value="${fieldValue(bean: employmentInstance, field: 'grossSalary')}" />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="dateCreated"><g:message code="employment.dateCreated" default="Date Created" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: employmentInstance, field: 'dateCreated', 'errors')}">
                                    <g:datePicker name="dateCreated" value="${employmentInstance?.dateCreated}"  />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="lastUpdated"><g:message code="employment.lastUpdated" default="Last Updated" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: employmentInstance, field: 'lastUpdated', 'errors')}">
                                    <g:datePicker name="lastUpdated" value="${employmentInstance?.lastUpdated}"  />

                                </td>
                            </tr>
                        
                        </tbody>
                    </table>
                </div>
                <div class="buttons">
                    <span class="button"><g:actionSubmit class="save" action="update" value="${message(code: 'update', 'default': 'Update')}" /></span>
                    <span class="button"><g:actionSubmit class="delete" action="delete" value="${message(code: 'delete', 'default': 'Delete')}" onclick="return confirm('${message(code: 'delete.confirm', 'default': 'Are you sure?')}');" /></span>
                </div>
            </g:form>
        </div>
    </body>
</html>
