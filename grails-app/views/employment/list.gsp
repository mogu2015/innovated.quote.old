
<%@ page import="com.innovated.iris.domain.Employment" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <title><g:message code="employment.list" default="Employment List" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLinkTo(dir: '')}"><g:message code="home" default="Home" /></a></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="employment.new" default="New Employment" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="employment.list" default="Employment List" /></h1>
            <g:if test="${flash.message}">
            <div class="message"><g:message code="${flash.message}" args="${flash.args}" default="${flash.defaultMessage}" /></div>
            </g:if>
            <div class="list">
                <table>
                    <thead>
                        <tr>
                        
                   	    <g:sortableColumn property="id" title="Id" titleKey="employment.id" />
                        
                   	    <th><g:message code="employment.employer" default="Employer" /></th>
                   	    
                   	    <th><g:message code="employment.employee" default="Employee" /></th>
                   	    
                   	    <g:sortableColumn property="jobTitle" title="Job Title" titleKey="employment.jobTitle" />
                        
                   	    <g:sortableColumn property="jobDescription" title="Job Description" titleKey="employment.jobDescription" />
                        
                   	    <g:sortableColumn property="staffNumber" title="Staff Number" titleKey="employment.staffNumber" />
                        
                        </tr>
                    </thead>
                    <tbody>
                    <g:each in="${employmentInstanceList}" status="i" var="employmentInstance">
                        <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
                        
                            <td><g:link action="show" id="${employmentInstance.id}">${fieldValue(bean: employmentInstance, field: "id")}</g:link></td>
                        
                            <td>${fieldValue(bean: employmentInstance, field: "employer")}</td>
                        
                            <td>${fieldValue(bean: employmentInstance, field: "employee")}</td>
                        
                            <td>${fieldValue(bean: employmentInstance, field: "jobTitle")}</td>
                        
                            <td>${fieldValue(bean: employmentInstance, field: "jobDescription")}</td>
                        
                            <td>${fieldValue(bean: employmentInstance, field: "staffNumber")}</td>
                        
                        </tr>
                    </g:each>
                    </tbody>
                </table>
            </div>
            <div class="paginateButtons">
                <g:paginate total="${employmentInstanceTotal}" />
            </div>
        </div>
    </body>
</html>
