
<%@ page import="com.innovated.iris.domain.Employment" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <title><g:message code="employment.show" default="Show Employment" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLinkTo(dir: '')}"><g:message code="home" default="Home" /></a></span>
            <span class="menuButton"><g:link class="list" action="list"><g:message code="employment.list" default="Employment List" /></g:link></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="employment.new" default="New Employment" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="employment.show" default="Show Employment" /></h1>
            <g:if test="${flash.message}">
            <div class="message"><g:message code="${flash.message}" args="${flash.args}" default="${flash.defaultMessage}" /></div>
            </g:if>
            <g:form>
                <g:hiddenField name="id" value="${employmentInstance?.id}" />
                <div class="dialog">
                    <table>
                        <tbody>
                        
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="employment.id" default="Id" />:</td>
                                
                                <td valign="top" class="value">${fieldValue(bean: employmentInstance, field: "id")}</td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="employment.employer" default="Employer" />:</td>
                                
                                <td valign="top" class="value"><g:link controller="company" action="show" id="${employmentInstance?.employer?.id}">${employmentInstance?.employer?.encodeAsHTML()}</g:link></td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="employment.employee" default="Employee" />:</td>
                                
                                <td valign="top" class="value"><g:link controller="individual" action="show" id="${employmentInstance?.employee?.id}">${employmentInstance?.employee?.encodeAsHTML()}</g:link></td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="employment.jobTitle" default="Job Title" />:</td>
                                
                                <td valign="top" class="value">${fieldValue(bean: employmentInstance, field: "jobTitle")}</td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="employment.jobDescription" default="Job Description" />:</td>
                                
                                <td valign="top" class="value">${fieldValue(bean: employmentInstance, field: "jobDescription")}</td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="employment.staffNumber" default="Staff Number" />:</td>
                                
                                <td valign="top" class="value">${fieldValue(bean: employmentInstance, field: "staffNumber")}</td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="employment.startedEmployment" default="Started Employment" />:</td>
                                
                                <td valign="top" class="value"><g:formatDate date="${employmentInstance?.startedEmployment}" /></td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="employment.ceasedEmployment" default="Ceased Employment" />:</td>
                                
                                <td valign="top" class="value"><g:formatDate date="${employmentInstance?.ceasedEmployment}" /></td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="employment.grossSalary" default="Gross Salary" />:</td>
                                
                                <td valign="top" class="value"><g:formatNumber number="${employmentInstance?.grossSalary}" /></td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="employment.dateCreated" default="Date Created" />:</td>
                                
                                <td valign="top" class="value"><g:formatDate date="${employmentInstance?.dateCreated}" /></td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="employment.lastUpdated" default="Last Updated" />:</td>
                                
                                <td valign="top" class="value"><g:formatDate date="${employmentInstance?.lastUpdated}" /></td>
                                
                            </tr>
                            
                        </tbody>
                    </table>
                </div>
                <div class="buttons">
                    <span class="button"><g:actionSubmit class="edit" action="edit" value="${message(code: 'edit', 'default': 'Edit')}" /></span>
                    <span class="button"><g:actionSubmit class="delete" action="delete" value="${message(code: 'delete', 'default': 'Delete')}" onclick="return confirm('${message(code: 'delete.confirm', 'default': 'Are you sure?')}');" /></span>
                </div>
            </g:form>
        </div>
    </body>
</html>
