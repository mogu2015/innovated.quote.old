
<%@ page import="com.innovated.iris.domain.Phone" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <title><g:message code="phone.create" default="Create Phone" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLinkTo(dir: '')}"><g:message code="home" default="Home" /></a></span>
            <span class="menuButton"><g:link class="list" action="list"><g:message code="phone.list" default="Phone List" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="phone.create" default="Create Phone" /></h1>
            <g:if test="${flash.message}">
            	<div class="message"><g:message code="${flash.message}" args="${flash.args}" default="${flash.defaultMessage}" /></div>
            </g:if>
            <g:hasErrors bean="${phoneInstance}">
				<div class="errors">
					<g:renderErrors bean="${phoneInstance}" as="list" />
				</div>
            </g:hasErrors>
            <g:form action="save" method="post" >
                <div class="dialog">
                    <fieldset>
                        <legend><g:message code="phone.create.legend" default="Enter Phone Details"/></legend>
                        
                            <div class="prop mandatory ${hasErrors(bean: phoneInstance, field: 'phoneType', 'error')}">
                                <label for="phoneType">
                                    <g:message code="phone.phoneType" default="Phone Type" />
                                    <span class="indicator">*</span>
                                </label>
                                <g:select optionKey="id" from="${com.innovated.iris.domain.enums.PhoneType.list()}" name="phoneType.id" value="${phoneInstance?.phoneType?.id}" ></g:select>
                            </div>
                        
                            <div class="prop mandatory ${hasErrors(bean: phoneInstance, field: 'number', 'error')}">
                                <label for="number">
                                    <g:message code="phone.number" default="Number" />
                                    <span class="indicator">*</span>
                                </label>
                                <input type="text" id="number" name="number" value="${fieldValue(bean:phoneInstance,field:'number')}"/>
                            </div>
                        
                            <div class="prop mandatory ${hasErrors(bean: phoneInstance, field: 'dateCreated', 'error')}">
                                <label for="dateCreated">
                                    <g:message code="phone.dateCreated" default="Date Created" />
                                    <span class="indicator">*</span>
                                </label>
                                <g:datePicker name="dateCreated" value="${phoneInstance?.dateCreated}" precision="minute" ></g:datePicker>
                            </div>
                        
                            <div class="prop mandatory ${hasErrors(bean: phoneInstance, field: 'lastUpdated', 'error')}">
                                <label for="lastUpdated">
                                    <g:message code="phone.lastUpdated" default="Last Updated" />
                                    <span class="indicator">*</span>
                                </label>
                                <g:datePicker name="lastUpdated" value="${phoneInstance?.lastUpdated}" precision="minute" ></g:datePicker>
                            </div>
                        
                            <div class="prop mandatory ${hasErrors(bean: phoneInstance, field: 'entity', 'error')}">
                                <label for="entity">
                                    <g:message code="phone.entity" default="Entity" />
                                    <span class="indicator">*</span>
                                </label>
                                <g:select optionKey="id" from="${com.innovated.iris.domain.Entity.list()}" name="entity.id" value="${phoneInstance?.entity?.id}" ></g:select>
                            </div>
                        
                    </fieldset>
                </div>
                <div class="buttons">
                    <span class="button"><g:submitButton name="create" class="save" value="${message(code: 'create', 'default': 'Create')}" /></span>
                </div>
            </g:form>
        </div>
    </body>
</html>

