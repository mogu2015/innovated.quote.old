
<%@ page import="com.innovated.iris.domain.CompanyContact" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta name="layout" content="main" />
        <title>CompanyContact List</title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${resource(dir:'')}">Home</a></span>
            <span class="menuButton"><g:link class="create" action="create">New CompanyContact</g:link></span>
        </div>
        <div class="body">
            <h1>CompanyContact List</h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <div class="list">
                <table>
                    <thead>
                        <tr>
                        
                   	        <g:sortableColumn property="id" title="Id" />
                        
                   	        <g:sortableColumn property="name" title="Name" />
                        
                   	        <th>Addresses</th>
                   	    
                   	        <th>Phones</th>
                   	    
                   	        <th>Emails</th>
                   	    
                   	        <th>Company</th>
                   	    
                        </tr>
                    </thead>
                    <tbody>
                    <g:each in="${companyContactInstanceList}" status="i" var="companyContactInstance">
                        <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
                        
                            <td><g:link action="show" id="${companyContactInstance.id}">${fieldValue(bean:companyContactInstance, field:'id')}</g:link></td>
                        
                            <td>${fieldValue(bean:companyContactInstance, field:'name')}</td>
                        
                            <td>${fieldValue(bean:companyContactInstance, field:'addresses')}</td>
                        
                            <td>${fieldValue(bean:companyContactInstance, field:'phones')}</td>
                        
                            <td>${fieldValue(bean:companyContactInstance, field:'emails')}</td>
                        
                            <td>${fieldValue(bean:companyContactInstance, field:'company')}</td>
                        
                        </tr>
                    </g:each>
                    </tbody>
                </table>
            </div>
            <div class="paginateButtons">
                <g:paginate total="${companyContactInstanceTotal}" />
            </div>
        </div>
    </body>
</html>
