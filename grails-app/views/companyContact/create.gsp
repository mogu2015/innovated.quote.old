
<%@ page import="com.innovated.iris.domain.CompanyContact" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <title><g:message code="companyContact.create" default="Create CompanyContact" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLinkTo(dir: '')}"><g:message code="home" default="Home" /></a></span>
            <span class="menuButton"><g:link class="list" action="list"><g:message code="companyContact.list" default="CompanyContact List" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="companyContact.create" default="Create CompanyContact" /></h1>
            <g:if test="${flash.message}">
            	<div class="message"><g:message code="${flash.message}" args="${flash.args}" default="${flash.defaultMessage}" /></div>
            </g:if>
            <g:hasErrors bean="${companyContactInstance}">
				<div class="errors">
					<g:renderErrors bean="${companyContactInstance}" as="list" />
				</div>
            </g:hasErrors>
            <g:form action="save" method="post" >
                <div class="dialog">
                    <fieldset>
                        <legend><g:message code="companyContact.create.legend" default="Enter CompanyContact Details"/></legend>
                        
                             <div class="prop mandatory ${hasErrors(bean: companyContactInstance, field: 'company', 'error')}">
                                <label for="company">
                                    <g:message code="companyContact.company" default="Company" />
                                    <span class="indicator">*</span>
                                </label>
                                <g:select optionKey="id" from="${com.innovated.iris.domain.Company.list()}" name="company.id" value="${companyContactInstance?.company?.id}" ></g:select>
                            </div>
                            
                            <div class="prop mandatory ${hasErrors(bean: companyContactInstance, field: 'name', 'error')}">
                                <label for="name">
                                    <g:message code="companyContact.name" default="Name" />
                                    <span class="indicator">*</span>
                                </label>
                                <input type="text" id="name" name="name" value="${fieldValue(bean:companyContactInstance,field:'name')}"/>
                            </div>
                        
                           <!--
                        
                            <div class="prop mandatory ${hasErrors(bean: companyContactInstance, field: 'dateCreated', 'error')}">
                                <label for="dateCreated">
                                    <g:message code="companyContact.dateCreated" default="Date Created" />
                                    <span class="indicator">*</span>
                                </label>
                                <g:datePicker name="dateCreated" value="${companyContactInstance?.dateCreated}" precision="minute" ></g:datePicker>
                            </div>
                        
                            <div class="prop mandatory ${hasErrors(bean: companyContactInstance, field: 'lastUpdated', 'error')}">
                                <label for="lastUpdated">
                                    <g:message code="companyContact.lastUpdated" default="Last Updated" />
                                    <span class="indicator">*</span>
                                </label>
                                <g:datePicker name="lastUpdated" value="${companyContactInstance?.lastUpdated}" precision="minute" ></g:datePicker>
                            </div>
                        -->
                            <div class="prop ${hasErrors(bean: companyContactInstance, field: 'role', 'error')}">
                                <label for="role">
                                    <g:message code="companyContact.role" default="Role" />
                                    
                                </label>
                                <input type="text" id="role" name="role" value="${fieldValue(bean:companyContactInstance,field:'role')}"/>
                            </div>
                        
                    </fieldset>
                </div>
                <div class="buttons">
                    <span class="button"><g:submitButton name="create" class="save" value="${message(code: 'create', 'default': 'Create')}" /></span>
                </div>
            </g:form>
        </div>
    </body>
</html>

