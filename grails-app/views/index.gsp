<g:ifNotGranted role="ROLE_SUPER,ROLE_ADMIN">
	${ response.sendRedirect("user/home") }
</g:ifNotGranted>

<html>
    <head>
        <title>Welcome to IRIS</title>
        <meta name="layout" content="main" />
        
    </head>
    <body>
        <h1 style="margin-left:20px;">Welcome to IRIS! (inNovated Reporting & Information System)</h1>
        <p style="margin-left:20px;width:80%">
        	Access Controllers here:
        </p><br/>
        <div class="dialog" style="margin-left:20px;width:60%;">
            <h3>HOME:</h3>
            <ul>
            	<li class="controller"><g:link controller="user" action="home">User Home/Dashboard Page</g:link></li>
            </ul>
            <br/>
            
            <g:ifAnyGranted role="ROLE_SUPER,ROLE_ADMIN">
            <!-- Super & admins only -->
            <h3>Codes & Enums</h3>
            <ul>
            	<li class="controller"><g:link controller="user">IRIS Users</g:link></li>
            	<li class="controller"><g:link controller="role">User Roles</g:link></li>
            	<br/>
            	<li class="controller"><g:link controller="addressType">Address Types</g:link></li>
            	<li class="controller"><g:link controller="phoneType">Phone Types</g:link></li>
            	<li class="controller"><g:link controller="customerType">Customer Types</g:link></li>
            	<li class="controller"><g:link controller="customerStatus">Customer Statuses</g:link></li>
            	<li class="controller"><g:link controller="nonVehicleType">Non-Vehicle Asset Types</g:link></li>
            	<li class="controller"><g:link controller="supplierType">Supplier Types</g:link></li>
            	<li class="controller"><g:link controller="budgetType">Budget Types</g:link></li>
            	<li class="controller"><g:link controller="budgetStatus">Budget Statuses</g:link></li>
            	<li class="controller"><g:link controller="registrationUsageType">Rego Types</g:link></li>
            	<li class="controller"><g:link controller="australianState">Australian States</g:link></li>
            	<br/>
            	<li class="controller"><g:link controller="vehicleManufacturer">Vehicle Makes</g:link></li>
            	<li class="controller"><g:link controller="vehicleModel">Vehicle Models</g:link></li>
			</ul>
            <br/>
            
            <h3>Data Migration/Import</h3>
            <ul>
            	<li class="controller"><g:link controller="vehicleDataImport">Vehicle Data</g:link> <i>(from GreenVehicleGuide.gov.au)</i></li>
            </ul>
            <br/>
            
			<h3>Lookup Parameters</h3>
            <ul>
            	<li class="controller"><g:link controller="vehicleStampDutyParameter">Purchase Stamp Duty</g:link></li>
            	<li class="controller"><g:link controller="luxuryCarTaxParameter">Luxury Car Tax Parameters</g:link></li>
            	<li class="controller"><g:link controller="residualValueParameter">Residual Value Parameters</g:link></li>
            	<li class="controller"><g:link controller="financierParameter">Financier Parameters</g:link></li>
            	<li class="controller"><g:link controller="maintenanceParameter">Maintenance Parameters</g:link></li>
            	<li class="controller"><g:link controller="tyreParameter">Tyre Parameters</g:link></li>
            	<li class="controller"><g:link controller="fuelParameter">Fuel Parameters</g:link></li>
            	<li class="controller"><g:link controller="vehicleRegistrationParameter">Registration Parameters</g:link></li>
            	<li class="controller"><g:link controller="roadsideParameter">Roadside Parameters</g:link></li>
            	<li class="controller"><g:link controller="feesBrokerageParameter">Management Fee Parameters</g:link></li>
            	<br/>
            	<li class="controller"><g:link controller="gstRateParameter">GST Rate Parameters</g:link></li>
            	<li class="controller"><g:link controller="incomeTaxRateParameter">Income Tax Rate Parameters</g:link></li>
            	<li class="controller"><g:link controller="medicareLevyRateParameter">Medicare Levy Rate Parameters</g:link></li>
            	<li class="controller"><g:link controller="hecsHelpRateParameter">HECS/HELP Rate Parameters</g:link></li>
            	<br/>
            	<li class="controller"><g:link controller="fbtStatutoryMethodRateParameter">FBT Statutory Method Rate Parameters</g:link></li>
            	<br/>
            	<li class="controller"><g:link controller="quoteExportTemplateParameter">Quote Export Templates</g:link></li>
            </ul>
            <br/>
            
            <h3>Domain Classes</h3>
            <ul>
            	<li class="controller"><g:link controller="numberSequence">Number Sequences</g:link></li>
            	<br/>
            	<li class="controller"><g:link controller="address">Addresses</g:link></li>
            	<li class="controller"><g:link controller="email">Emails</g:link></li>
            	<li class="controller"><g:link controller="phone">Phones</g:link></li>
            	<br/>
            	<li class="controller"><g:link controller="entity">Entities</g:link></li>
            	<li class="controller"><g:link controller="company">Companies</g:link></li>
            	<li class="controller"><g:link controller="companyContact">Company Contacts</g:link></li>
            	<li class="controller"><g:link controller="individual">Individuals</g:link></li>
            	<li class="controller"><g:link controller="customer">Customers</g:link></li>
            	<li class="controller"><g:link controller="employment">Employment Record</g:link></li>
            	<li class="controller"><g:link controller="supplier">Suppliers</g:link></li>
            	<br/>
            	<li class="controller"><g:link controller="quote">Quotes</g:link></li>
            	<li class="controller"><g:link controller="budget">Budgets</g:link></li>
            	<br/>
            	<li class="controller"><g:link controller="asset">Assets</g:link></li>
            	<li class="controller"><g:link controller="nonVehicle">Non-Vehicles</g:link></li>
            	<li class="controller"><g:link controller="vehicle">Vehicles</g:link></li>
            	<li class="controller"><g:link controller="registration">Vehicle Registrations</g:link></li>
            	<br/>
            	<li class="controller"><g:link controller="makeModel">Make/Models</g:link></li>
            </ul>
            <br/>
            
            </g:ifAnyGranted>
            
            <g:ifNotGranted role="ROLE_SUPER,ROLE_ADMIN">
            <!-- non-admin users -->
            <h3>Domain Classes</h3>
            <ul>
            	<li class="controller"><g:link controller="address">Addresses</g:link></li>
            	<li class="controller"><g:link controller="email">Emails</g:link></li>
            	<li class="controller"><g:link controller="phone">Phones</g:link></li>
            	<br/>
            	<li class="controller"><g:link controller="company">Companies</g:link></li>
            	<li class="controller"><g:link controller="companyContact">Company Contacts</g:link></li>
            	<li class="controller"><g:link controller="individual">Individuals</g:link></li>
            	<li class="controller"><g:link controller="customer">Customers</g:link></li>
            	<li class="controller"><g:link controller="employment">Employment Record</g:link></li>
            	<br/>
            	<li class="controller"><g:link controller="quote">Quotes</g:link></li>
            </ul>
            </g:ifNotGranted>
        </div>
    </body>
</html>
