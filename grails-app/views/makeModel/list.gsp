
<%@ page import="com.innovated.iris.domain.MakeModel" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <title><g:message code="makeModel.list" default="Make/Model List" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLinkTo(dir: '')}"><g:message code="home" default="Home" /></a></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="makeModel.new" default="New Make/Model" /></g:link></span>
            <span class="menuButton"><g:link class="save" action="importFile"><g:message code="makeModel.import" default="Import Make/Model List" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="makeModel.list" default="Make/Model List" /></h1>
            <g:if test="${flash.message}">
            <div class="message"><g:message code="${flash.message}" args="${flash.args}" default="${flash.defaultMessage}" /></div>
            </g:if>
            <div class="list">
                <table>
                    <thead>
                        <tr>
                        
                   	    <g:sortableColumn property="id" title="Id" titleKey="makeModel.id" />
                        
                   	    <th><g:message code="makeModel.model" default="Model" /></th>
                   	    
                   	    <g:sortableColumn property="variant" title="Variant" titleKey="makeModel.variant" />
                        
                   	    <g:sortableColumn property="redbookCode" title="Redbook Code" titleKey="makeModel.redbookCode" />
                        
                   	    <g:sortableColumn property="buildDate" title="Build Date" titleKey="makeModel.buildDate" />
                        
                   	    <g:sortableColumn property="engineCapacity" title="Engine Capacity" titleKey="makeModel.engineCapacity" />
                        
                        </tr>
                    </thead>
                    <tbody>
                    <g:each in="${makeModelInstanceList}" status="i" var="makeModelInstance">
                        <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
                        
                            <td><g:link action="show" id="${makeModelInstance.id}">${fieldValue(bean: makeModelInstance, field: "id")}</g:link></td>
                        
                            <td>${fieldValue(bean: makeModelInstance, field: "model")}</td>
                        
                            <td>${fieldValue(bean: makeModelInstance, field: "variant")}</td>
                        
                            <td>${fieldValue(bean: makeModelInstance, field: "redbookCode")}</td>
                        
                            <td><g:formatDate date="${makeModelInstance.buildDate}" /></td>
                        
                            <td>${fieldValue(bean: makeModelInstance, field: "engineCapacity")}</td>
                        
                        </tr>
                    </g:each>
                    </tbody>
                </table>
            </div>
            <div class="paginateButtons">
                <g:paginate total="${makeModelInstanceTotal}" />
            </div>
        </div>
    </body>
</html>
