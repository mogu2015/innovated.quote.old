<%@
page import="com.innovated.iris.domain.MakeModel"
page import="com.innovated.iris.domain.enums.FuelType"
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <title><g:message code="makeModel.create" default="Create MakeModel" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLinkTo(dir: '')}"><g:message code="home" default="Home" /></a></span>
            <span class="menuButton"><g:link class="list" action="list"><g:message code="makeModel.list" default="MakeModel List" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="makeModel.create" default="Create MakeModel" /></h1>
            <g:if test="${flash.message}">
            	<div class="message"><g:message code="${flash.message}" args="${flash.args}" default="${flash.defaultMessage}" /></div>
            </g:if>
            <g:hasErrors bean="${makeModelInstance}">
				<div class="errors">
					<g:renderErrors bean="${makeModelInstance}" as="list" />
				</div>
            </g:hasErrors>
            <g:form action="save" method="post" >
                <div class="dialog">
                    <fieldset>
                        <legend><g:message code="makeModel.create.legend" default="Enter MakeModel Details"/></legend>
                        
                            <div class="prop mandatory ${hasErrors(bean: makeModelInstance, field: 'model', 'error')}">
                                <label for="model">
                                    <g:message code="makeModel.model" default="Model" />
                                    <span class="indicator">*</span>
                                </label>
                                <g:select name="model.id" from="${com.innovated.iris.domain.enums.VehicleModel.list()}" optionKey="id" value="${makeModelInstance?.model?.id}"  />

                            </div>
                        
                            <div class="prop mandatory ${hasErrors(bean: makeModelInstance, field: 'variant', 'error')}">
                                <label for="variant">
                                    <g:message code="makeModel.variant" default="Variant" />
                                    <span class="indicator">*</span>
                                </label>
                                <g:textField name="variant" value="${fieldValue(bean: makeModelInstance, field: 'variant')}" />

                            </div>
                            
                            <div class="prop mandatory ${hasErrors(bean: makeModelInstance, field: 'description', 'error')}">
                                <label for="description">
                                    <g:message code="makeModel.description" default="Description" />
                                    <span class="indicator">*</span>
                                </label>
                                <g:textField name="description" value="${fieldValue(bean: makeModelInstance, field: 'description')}" />
							</div>
                        
                            <div class="prop ${hasErrors(bean: makeModelInstance, field: 'redbookCode', 'error')}">
                                <label for="redbookCode">
                                    <g:message code="makeModel.redbookCode" default="RedBook Code" />
                                </label>
                                <g:textField name="redbookCode" value="${fieldValue(bean: makeModelInstance, field: 'redbookCode')}" />
                            </div>
                            <div class="prop ${hasErrors(bean: makeModelInstance, field: 'glassCode', 'error')}">
                                <label for="glassCode">
                                    <g:message code="makeModel.glassCode" default="GlassesGuide Code" />
                                </label>
                                <g:textField name="glassCode" value="${fieldValue(bean: makeModelInstance, field: 'glassCode')}" />
                            </div>
                            
                            <div class="prop mandatory ${hasErrors(bean: makeModelInstance, field: 'insuranceClass', 'error')}">
                                <label for="insuranceClass">
                                    <g:message code="makeModel.insuranceClass" default="Insurance Class" />
                                    <span class="indicator">*</span>
                                </label>
                                <g:select name="insuranceClass" from="${insuranceClasses}" valueMessagePrefix="makeModel.insuranceClasses" value="${makeModelInstance?.insuranceClass}"  />
                            </div>
                            
                            <div class="prop mandatory ${hasErrors(bean: makeModelInstance, field: 'fuelType', 'error')}">
                                <label for="fuelType">
                                    <g:message code="makeModel.fuelType" default="Fuel Type" />
                                    <span class="indicator">*</span>
                                </label>
                                <g:select name="fuelType.id" from="${com.innovated.iris.domain.enums.FuelType.list()}" optionKey="id" value="${makeModelInstance?.fuelType?.id}"  />
                            </div>
                            
                            <div class="prop mandatory ${hasErrors(bean: makeModelInstance, field: 'tyreSize', 'error')}">
                                <label for="tyreSize">
                                    <g:message code="makeModel.tyreSize" default="Tyre Size" />
                                    <span class="indicator">*</span>
                                </label>
                                <g:textField name="tyreSize" value="${fieldValue(bean: makeModelInstance, field: 'tyreSize')}" />
                            </div>
                            
                            <div class="prop mandatory ${hasErrors(bean: makeModelInstance, field: 'retailPrice', 'error')}">
                                <label for="retailPrice">
                                    <g:message code="makeModel.retailPrice" default="\$ RRP" />
                                    <span class="indicator">*</span>
                                </label>
                                <g:textField name="retailPrice" value="${fieldValue(bean: makeModelInstance, field: 'retailPrice')}" />
                            </div>
                        
                            <div class="prop mandatory ${hasErrors(bean: makeModelInstance, field: 'buildDate', 'error')}">
                                <label for="buildDate">
                                    <g:message code="makeModel.buildDate" default="Build Date" />
                                    <span class="indicator">*</span>
                                </label>
                                <g:datePicker name="buildDate" value="${makeModelInstance?.buildDate}" precision="month" years="${minYear..maxYear}" />
                            </div>
                        
                            <div class="prop mandatory ${hasErrors(bean: makeModelInstance, field: 'engineCapacity', 'error')}">
                                <label for="engineCapacity">
                                    <g:message code="makeModel.engineCapacity" default="Engine Capacity" />
                                    <span class="indicator">*</span>
                                </label>
                                <g:textField name="engineCapacity" value="${fieldValue(bean: makeModelInstance, field: 'engineCapacity')}" />

                            </div>
                            
                            <div class="prop ${hasErrors(bean: makeModelInstance, field: 'fuelConsumptionCombined', 'error')}">
		                        <label for="fuelConsumptionCombined">
		                            <g:message code="makeModel.fuelConsumptionCombined" default="L/100 km (Combined)" />
		                            
		                        </label>
		                        <g:textField name="fuelConsumptionCombined" value="${fieldValue(bean: makeModelInstance, field: 'fuelConsumptionCombined')}" />

		                    </div>
                        
                            <div class="prop mandatory ${hasErrors(bean: makeModelInstance, field: 'cylinders', 'error')}">
                                <label for="cylinders">
                                    <g:message code="makeModel.cylinders" default="Cylinders" />
                                    <span class="indicator">*</span>
                                </label>
                                <g:select name="cylinders" from="${1..16}" value="${makeModelInstance?.cylinders}"  />

                            </div>
                        
                            <div class="prop mandatory ${hasErrors(bean: makeModelInstance, field: 'doors', 'error')}">
                                <label for="doors">
                                    <g:message code="makeModel.doors" default="Doors" />
                                    <span class="indicator">*</span>
                                </label>
                                <g:select name="doors" from="${0..5}" value="${makeModelInstance?.doors}"  />

                            </div>
                        
                            <div class="prop ${hasErrors(bean: makeModelInstance, field: 'shape', 'error')}">
                                <label for="shape">
                                    <g:message code="makeModel.shape" default="Shape" />
                                </label>
                            	<g:textField name="shape" value="${fieldValue(bean: makeModelInstance, field: 'shape')}" />
                            </div>
                            <div class="prop ${hasErrors(bean: makeModelInstance, field: 'transmission', 'error')}">
                                <label for="transmission">
                                    <g:message code="makeModel.transmission" default="Transmission" />
                                </label>
                            	<g:textField name="transmission" value="${fieldValue(bean: makeModelInstance, field: 'transmission')}" />
                            </div>
                        
                            <div class="prop mandatory ${hasErrors(bean: makeModelInstance, field: 'serviceIntervalKms', 'error')}">
                                <label for="serviceIntervalKms">
                                    <g:message code="makeModel.serviceIntervalKms" default="Service Interval Kms" />
                                    <span class="indicator">*</span>
                                </label>
                                <g:select name="serviceIntervalKms" from="${[5000,10000,15000,20000,25000,30000]}" value="${makeModelInstance?.serviceIntervalKms}"  />

                            </div>
                        
                            <div class="prop mandatory ${hasErrors(bean: makeModelInstance, field: 'serviceIntervalMths', 'error')}">
                                <label for="serviceIntervalMths">
                                    <g:message code="makeModel.serviceIntervalMths" default="Service Interval Mths" />
                                    <span class="indicator">*</span>
                                </label>
                                <g:select name="serviceIntervalMths" from="${1..12}" value="${makeModelInstance?.serviceIntervalMths}"  />

                            </div>
                        
                    </fieldset>
                </div>
                <div class="buttons">
                    <span class="button"><g:submitButton name="create" class="save" value="${message(code: 'create', 'default': 'Create')}" /></span>
                </div>
            </g:form>
        </div>
    </body>
</html>

