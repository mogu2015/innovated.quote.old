
<%@ page import="com.innovated.iris.domain.MakeModel" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <title><g:message code="makeModel.show" default="Show MakeModel" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLinkTo(dir: '')}"><g:message code="home" default="Home" /></a></span>
            <span class="menuButton"><g:link class="list" action="list"><g:message code="makeModel.list" default="MakeModel List" /></g:link></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="makeModel.new" default="New MakeModel" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="makeModel.show" default="Show MakeModel" /></h1>
            <g:if test="${flash.message}">
            <div class="message"><g:message code="${flash.message}" args="${flash.args}" default="${flash.defaultMessage}" /></div>
            </g:if>
            <g:form>
                <g:hiddenField name="id" value="${makeModelInstance?.id}" />
                <div class="dialog">
                    <table>
                        <tbody>
                        
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="makeModel.id" default="Id" />:</td>
                                
                                <td valign="top" class="value">${fieldValue(bean: makeModelInstance, field: "id")}</td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="makeModel.model" default="Model" />:</td>
                                
                                <td valign="top" class="value"><g:link controller="vehicleModel" action="show" id="${makeModelInstance?.model?.id}">${makeModelInstance?.model?.encodeAsHTML()}</g:link></td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="makeModel.variant" default="Variant" />:</td>
                                
                                <td valign="top" class="value">${fieldValue(bean: makeModelInstance, field: "variant")}</td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="makeModel.redbookCode" default="Redbook Code" />:</td>
                                
                                <td valign="top" class="value">${fieldValue(bean: makeModelInstance, field: "redbookCode")}</td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="makeModel.insuranceClass" default="Insurance Class" />:</td>
                                <td valign="top" class="value">${fieldValue(bean: makeModelInstance, field: "insuranceClass")}</td>
                            </tr>
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="makeModel.tyreSize" default="Tyre Size" />:</td>
                                <td valign="top" class="value">${fieldValue(bean: makeModelInstance, field: "tyreSize")}</td>
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="makeModel.retailPrice" default="\$ RRP" />:</td>
                                
                                <td valign="top" class="value">${fieldValue(bean: makeModelInstance, field: "retailPrice")}</td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="makeModel.buildDate" default="Build Date" />:</td>
                                
                                <td valign="top" class="value"><g:formatDate date="${makeModelInstance?.buildDate}" /></td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="makeModel.engineCapacity" default="Engine Capacity" />:</td>
                                
                                <td valign="top" class="value">${fieldValue(bean: makeModelInstance, field: "engineCapacity")}</td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="makeModel.cylinders" default="Cylinders" />:</td>
                                
                                <td valign="top" class="value">${fieldValue(bean: makeModelInstance, field: "cylinders")}</td>
                                
                            </tr>
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="makeModel.doors" default="Doors" />:</td>
                                <td valign="top" class="value">${fieldValue(bean: makeModelInstance, field: "doors")}</td>
                            </tr>
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="makeModel.shape" default="Shape" />:</td>
                                <td valign="top" class="value">${fieldValue(bean: makeModelInstance, field: "shape")}</td>
                            </tr>
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="makeModel.transmission" default="Transmission" />:</td>
                                <td valign="top" class="value">${fieldValue(bean: makeModelInstance, field: "transmission")}</td>
                            </tr>
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="makeModel.serviceIntervalKms" default="Service Interval Kms" />:</td>
                                <td valign="top" class="value">${fieldValue(bean: makeModelInstance, field: "serviceIntervalKms")}</td>
                            </tr>
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="makeModel.serviceIntervalMths" default="Service Interval Mths" />:</td>
                                <td valign="top" class="value">${fieldValue(bean: makeModelInstance, field: "serviceIntervalMths")}</td>
                            </tr>
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="makeModel.fuelConsumptionCombined" default="L/100 km (Combined)" />:</td>
                                <td valign="top" class="value">${fieldValue(bean: makeModelInstance, field: "fuelConsumptionCombined")}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="buttons">
                    <span class="button"><g:actionSubmit class="edit" action="edit" value="${message(code: 'edit', 'default': 'Edit')}" /></span>
                    <span class="button"><g:actionSubmit class="delete" action="delete" value="${message(code: 'delete', 'default': 'Delete')}" onclick="return confirm('${message(code: 'delete.confirm', 'default': 'Are you sure?')}');" /></span>
                </div>
            </g:form>
        </div>
    </body>
</html>
