
<%@ page import="com.innovated.iris.domain.MakeModel" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <title><g:message code="makeModel.edit" default="Edit MakeModel" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLinkTo(dir: '')}"><g:message code="home" default="Home" /></a></span>
            <span class="menuButton"><g:link class="list" action="list"><g:message code="makeModel.list" default="MakeModel List" /></g:link></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="makeModel.new" default="New MakeModel" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="makeModel.edit" default="Edit MakeModel" /></h1>
            <g:if test="${flash.message}">
            <div class="message"><g:message code="${flash.message}" args="${flash.args}" default="${flash.defaultMessage}" /></div>
            </g:if>
            <g:hasErrors bean="${makeModelInstance}">
            <div class="errors">
                <g:renderErrors bean="${makeModelInstance}" as="list" />
            </div>
            </g:hasErrors>
            <g:form method="post" >
                <g:hiddenField name="id" value="${makeModelInstance?.id}" />
                <g:hiddenField name="version" value="${makeModelInstance?.version}" />
                <div class="dialog">
                    <table>
                        <tbody>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="model"><g:message code="makeModel.model" default="Model" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: makeModelInstance, field: 'model', 'errors')}">
                                    <g:select name="model.id" from="${com.innovated.iris.domain.enums.VehicleModel.list()}" optionKey="id" value="${makeModelInstance?.model?.id}"  />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="variant"><g:message code="makeModel.variant" default="Variant" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: makeModelInstance, field: 'variant', 'errors')}">
                                    <g:textField name="variant" value="${fieldValue(bean: makeModelInstance, field: 'variant')}" />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="redbookCode"><g:message code="makeModel.redbookCode" default="Redbook Code" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: makeModelInstance, field: 'redbookCode', 'errors')}">
                                    <g:textField name="redbookCode" value="${fieldValue(bean: makeModelInstance, field: 'redbookCode')}" />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="buildDate"><g:message code="makeModel.buildDate" default="Build Date" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: makeModelInstance, field: 'buildDate', 'errors')}">
                                    <g:datePicker name="buildDate" value="${makeModelInstance?.buildDate}"  />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="engineCapacity"><g:message code="makeModel.engineCapacity" default="Engine Capacity" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: makeModelInstance, field: 'engineCapacity', 'errors')}">
                                    <g:textField name="engineCapacity" value="${fieldValue(bean: makeModelInstance, field: 'engineCapacity')}" />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="cylinders"><g:message code="makeModel.cylinders" default="Cylinders" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: makeModelInstance, field: 'cylinders', 'errors')}">
                                    <g:select name="cylinders" from="${1..16}" value="${makeModelInstance?.cylinders}"  />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="doors"><g:message code="makeModel.doors" default="Doors" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: makeModelInstance, field: 'doors', 'errors')}">
                                    <g:select name="doors" from="${0..5}" value="${makeModelInstance?.doors}"  />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="shape"><g:message code="makeModel.shape" default="Shape" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: makeModelInstance, field: 'shape', 'errors')}">
                                    <g:select name="shape" from="${makeModelInstance.constraints.shape.inList}" value="${makeModelInstance.shape}" valueMessagePrefix="makeModel.shape"  />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="serviceIntervalKms"><g:message code="makeModel.serviceIntervalKms" default="Service Interval Kms" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: makeModelInstance, field: 'serviceIntervalKms', 'errors')}">
                                    <g:select name="serviceIntervalKms" from="${1000..30000}" value="${makeModelInstance?.serviceIntervalKms}"  />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="serviceIntervalMths"><g:message code="makeModel.serviceIntervalMths" default="Service Interval Mths" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: makeModelInstance, field: 'serviceIntervalMths', 'errors')}">
                                    <g:select name="serviceIntervalMths" from="${1..12}" value="${makeModelInstance?.serviceIntervalMths}"  />

                                </td>
                            </tr>
                        
                        </tbody>
                    </table>
                </div>
                <div class="buttons">
                    <span class="button"><g:actionSubmit class="save" action="update" value="${message(code: 'update', 'default': 'Update')}" /></span>
                    <span class="button"><g:actionSubmit class="delete" action="delete" value="${message(code: 'delete', 'default': 'Delete')}" onclick="return confirm('${message(code: 'delete.confirm', 'default': 'Are you sure?')}');" /></span>
                </div>
            </g:form>
        </div>
    </body>
</html>
