
<%@ page import="com.innovated.iris.domain.lookup.RoadsideParameter" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <title><g:message code="roadsideParameter.edit" default="Edit RoadsideParameter" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLinkTo(dir: '')}"><g:message code="home" default="Home" /></a></span>
            <span class="menuButton"><g:link class="list" action="list"><g:message code="roadsideParameter.list" default="RoadsideParameter List" /></g:link></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="roadsideParameter.new" default="New RoadsideParameter" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="roadsideParameter.edit" default="Edit RoadsideParameter" /></h1>
            <g:if test="${flash.message}">
            <div class="message"><g:message code="${flash.message}" args="${flash.args}" default="${flash.defaultMessage}" /></div>
            </g:if>
            <g:hasErrors bean="${roadsideParameterInstance}">
            <div class="errors">
                <g:renderErrors bean="${roadsideParameterInstance}" as="list" />
            </div>
            </g:hasErrors>
            <g:form method="post" >
                <g:hiddenField name="id" value="${roadsideParameterInstance?.id}" />
                <g:hiddenField name="version" value="${roadsideParameterInstance?.version}" />
                <div class="dialog">
                    <table>
                        <tbody>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="dateFrom"><g:message code="roadsideParameter.dateFrom" default="Date From" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: roadsideParameterInstance, field: 'dateFrom', 'errors')}">
                                    <g:datePicker name="dateFrom" value="${roadsideParameterInstance?.dateFrom}"  />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="customerCode"><g:message code="roadsideParameter.customerCode" default="Customer Code" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: roadsideParameterInstance, field: 'customerCode', 'errors')}">
                                    <g:textField name="customerCode" value="${fieldValue(bean: roadsideParameterInstance, field: 'customerCode')}" />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="state"><g:message code="roadsideParameter.state" default="State" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: roadsideParameterInstance, field: 'state', 'errors')}">
                                    <g:textField name="state" value="${fieldValue(bean: roadsideParameterInstance, field: 'state')}" />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="defaultSupplier"><g:message code="roadsideParameter.defaultSupplier" default="Default Supplier" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: roadsideParameterInstance, field: 'defaultSupplier', 'errors')}">
                                    <g:select name="defaultSupplier.id" from="${com.innovated.iris.domain.Supplier.list()}" optionKey="id" value="${roadsideParameterInstance?.defaultSupplier?.id}"  />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="joiningFee"><g:message code="roadsideParameter.joiningFee" default="Joining Fee" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: roadsideParameterInstance, field: 'joiningFee', 'errors')}">
                                    <g:textField name="joiningFee" value="${fieldValue(bean: roadsideParameterInstance, field: 'joiningFee')}" />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="annualFee"><g:message code="roadsideParameter.annualFee" default="Annual Fee" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: roadsideParameterInstance, field: 'annualFee', 'errors')}">
                                    <g:textField name="annualFee" value="${fieldValue(bean: roadsideParameterInstance, field: 'annualFee')}" />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="lastUpdated"><g:message code="roadsideParameter.lastUpdated" default="Last Updated" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: roadsideParameterInstance, field: 'lastUpdated', 'errors')}">
                                    <g:datePicker name="lastUpdated" value="${roadsideParameterInstance?.lastUpdated}"  />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="dateCreated"><g:message code="roadsideParameter.dateCreated" default="Date Created" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: roadsideParameterInstance, field: 'dateCreated', 'errors')}">
                                    <g:datePicker name="dateCreated" value="${roadsideParameterInstance?.dateCreated}"  />

                                </td>
                            </tr>
                        
                        </tbody>
                    </table>
                </div>
                <div class="buttons">
                    <span class="button"><g:actionSubmit class="save" action="update" value="${message(code: 'update', 'default': 'Update')}" /></span>
                    <span class="button"><g:actionSubmit class="delete" action="delete" value="${message(code: 'delete', 'default': 'Delete')}" onclick="return confirm('${message(code: 'delete.confirm', 'default': 'Are you sure?')}');" /></span>
                </div>
            </g:form>
        </div>
    </body>
</html>
