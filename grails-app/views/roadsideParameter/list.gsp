
<%@ page import="com.innovated.iris.domain.lookup.RoadsideParameter" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <title><g:message code="roadsideParameter.list" default="RoadsideParameter List" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLinkTo(dir: '')}"><g:message code="home" default="Home" /></a></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="roadsideParameter.new" default="New RoadsideParameter" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="roadsideParameter.list" default="RoadsideParameter List" /></h1>
            <g:if test="${flash.message}">
            <div class="message"><g:message code="${flash.message}" args="${flash.args}" default="${flash.defaultMessage}" /></div>
            </g:if>
            <div class="list">
                <table>
                    <thead>
                        <tr>
                   	    <g:sortableColumn property="id" title="ID" titleKey="roadsideParameter.id" />
                   	    <g:sortableColumn property="dateFrom" title="Date From" titleKey="roadsideParameter.dateFrom" />
                   	    <g:sortableColumn property="customerCode" title="Customer Code" titleKey="roadsideParameter.customerCode" />
                   	    <g:sortableColumn property="state" title="State" titleKey="roadsideParameter.state" />
                   	    <th><g:message code="roadsideParameter.defaultSupplier" default="Default Supplier" /></th>
                   	    <g:sortableColumn property="annualFee" title="Annual Fee" titleKey="roadsideParameter.annualFee" />
                   	    <g:sortableColumn property="joiningFee" title="Joining Fee" titleKey="roadsideParameter.joiningFee" />
                        </tr>
                    </thead>
                    <tbody>
                    <g:each in="${roadsideParameterInstanceList}" status="i" var="roadsideParameterInstance">
                        <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
                            <td><g:link action="show" id="${roadsideParameterInstance.id}">${fieldValue(bean: roadsideParameterInstance, field: "id")}</g:link></td>
                            <td><g:formatDate date="${roadsideParameterInstance.dateFrom}" format="d MMM yyyy" /></td>
                            <td>${fieldValue(bean: roadsideParameterInstance, field: "customerCode")}</td>
                            <td>${fieldValue(bean: roadsideParameterInstance, field: "state")}</td>
                            <td>${fieldValue(bean: roadsideParameterInstance, field: "defaultSupplier")}</td>
                            <td class="numeric">${formatNumber(number:roadsideParameterInstance.annualFee, type:'currency')}</td>
                            <td class="numeric">${formatNumber(number:roadsideParameterInstance.joiningFee, type:'currency')}</td>
                            
                        </tr>
                    </g:each>
                    </tbody>
                </table>
            </div>
            <div class="paginateButtons">
                <g:paginate total="${roadsideParameterInstanceTotal}" />
            </div>
        </div>
    </body>
</html>
