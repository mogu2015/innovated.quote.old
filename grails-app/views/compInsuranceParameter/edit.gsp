
<%@ page import="com.innovated.iris.domain.lookup.CompInsuranceParameter" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <title><g:message code="compInsuranceParameter.edit" default="Edit CompInsuranceParameter" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLinkTo(dir: '')}"><g:message code="home" default="Home" /></a></span>
            <span class="menuButton"><g:link class="list" action="list"><g:message code="compInsuranceParameter.list" default="CompInsuranceParameter List" /></g:link></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="compInsuranceParameter.new" default="New CompInsuranceParameter" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="compInsuranceParameter.edit" default="Edit CompInsuranceParameter" /></h1>
            <g:if test="${flash.message}">
            <div class="message"><g:message code="${flash.message}" args="${flash.args}" default="${flash.defaultMessage}" /></div>
            </g:if>
            <g:hasErrors bean="${compInsuranceParameterInstance}">
            <div class="errors">
                <g:renderErrors bean="${compInsuranceParameterInstance}" as="list" />
            </div>
            </g:hasErrors>
            <g:form method="post" >
                <g:hiddenField name="id" value="${compInsuranceParameterInstance?.id}" />
                <g:hiddenField name="version" value="${compInsuranceParameterInstance?.version}" />
                <div class="dialog">
                    <table>
                        <tbody>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="dateFrom"><g:message code="compInsuranceParameter.dateFrom" default="Date From" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: compInsuranceParameterInstance, field: 'dateFrom', 'errors')}">
                                    <g:datePicker name="dateFrom" value="${compInsuranceParameterInstance?.dateFrom}"  />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="customerCode"><g:message code="compInsuranceParameter.customerCode" default="Customer Code" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: compInsuranceParameterInstance, field: 'customerCode', 'errors')}">
                                    <g:textField name="customerCode" value="${fieldValue(bean: compInsuranceParameterInstance, field: 'customerCode')}" />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="defaultSupplier"><g:message code="compInsuranceParameter.defaultSupplier" default="Default Supplier" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: compInsuranceParameterInstance, field: 'defaultSupplier', 'errors')}">
                                    <g:select name="defaultSupplier.id" from="${com.innovated.iris.domain.Supplier.list()}" optionKey="id" value="${compInsuranceParameterInstance?.defaultSupplier?.id}"  />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="state"><g:message code="compInsuranceParameter.state" default="State" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: compInsuranceParameterInstance, field: 'state', 'errors')}">
                                    <g:textField name="state" value="${fieldValue(bean: compInsuranceParameterInstance, field: 'state')}" />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="type"><g:message code="compInsuranceParameter.type" default="Type" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: compInsuranceParameterInstance, field: 'type', 'errors')}">
                                    <g:textField name="type" value="${fieldValue(bean: compInsuranceParameterInstance, field: 'type')}" />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="metro"><g:message code="compInsuranceParameter.metro" default="Metro" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: compInsuranceParameterInstance, field: 'metro', 'errors')}">
                                    <g:textField name="metro" value="${fieldValue(bean: compInsuranceParameterInstance, field: 'metro')}" />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="regional"><g:message code="compInsuranceParameter.regional" default="Regional" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: compInsuranceParameterInstance, field: 'regional', 'errors')}">
                                    <g:textField name="regional" value="${fieldValue(bean: compInsuranceParameterInstance, field: 'regional')}" />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="under25Metro"><g:message code="compInsuranceParameter.under25Metro" default="Under25 Metro" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: compInsuranceParameterInstance, field: 'under25Metro', 'errors')}">
                                    <g:textField name="under25Metro" value="${fieldValue(bean: compInsuranceParameterInstance, field: 'under25Metro')}" />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="under25Regional"><g:message code="compInsuranceParameter.under25Regional" default="Under25 Regional" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: compInsuranceParameterInstance, field: 'under25Regional', 'errors')}">
                                    <g:textField name="under25Regional" value="${fieldValue(bean: compInsuranceParameterInstance, field: 'under25Regional')}" />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="excess"><g:message code="compInsuranceParameter.excess" default="Excess" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: compInsuranceParameterInstance, field: 'excess', 'errors')}">
                                    <g:textField name="excess" value="${fieldValue(bean: compInsuranceParameterInstance, field: 'excess')}" />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="lastUpdated"><g:message code="compInsuranceParameter.lastUpdated" default="Last Updated" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: compInsuranceParameterInstance, field: 'lastUpdated', 'errors')}">
                                    <g:datePicker name="lastUpdated" value="${compInsuranceParameterInstance?.lastUpdated}"  />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="dateCreated"><g:message code="compInsuranceParameter.dateCreated" default="Date Created" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: compInsuranceParameterInstance, field: 'dateCreated', 'errors')}">
                                    <g:datePicker name="dateCreated" value="${compInsuranceParameterInstance?.dateCreated}"  />

                                </td>
                            </tr>
                        
                        </tbody>
                    </table>
                </div>
                <div class="buttons">
                    <span class="button"><g:actionSubmit class="save" action="update" value="${message(code: 'update', 'default': 'Update')}" /></span>
                    <span class="button"><g:actionSubmit class="delete" action="delete" value="${message(code: 'delete', 'default': 'Delete')}" onclick="return confirm('${message(code: 'delete.confirm', 'default': 'Are you sure?')}');" /></span>
                </div>
            </g:form>
        </div>
    </body>
</html>
