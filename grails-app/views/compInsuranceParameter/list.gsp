
<%@ page import="com.innovated.iris.domain.lookup.CompInsuranceParameter" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <title><g:message code="compInsuranceParameter.list" default="CompInsuranceParameter List" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLinkTo(dir: '')}"><g:message code="home" default="Home" /></a></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="compInsuranceParameter.new" default="New CompInsuranceParameter" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="compInsuranceParameter.list" default="CompInsuranceParameter List" /></h1>
            <g:if test="${flash.message}">
            <div class="message"><g:message code="${flash.message}" args="${flash.args}" default="${flash.defaultMessage}" /></div>
            </g:if>
            <div class="list">
                <table>
                    <thead>
                        <tr>
                        
                   	    <g:sortableColumn property="id" title="Id" titleKey="compInsuranceParameter.id" />
                        
                   	    <g:sortableColumn property="dateFrom" title="Date From" titleKey="compInsuranceParameter.dateFrom" />
                        
                   	    <g:sortableColumn property="customerCode" title="Customer Code" titleKey="compInsuranceParameter.customerCode" />
                        
                   	    <th><g:message code="compInsuranceParameter.defaultSupplier" default="Default Supplier" /></th>
                   	    
                   	    <g:sortableColumn property="state" title="State" titleKey="compInsuranceParameter.state" />
                        
                   	    <g:sortableColumn property="type" title="Type" titleKey="compInsuranceParameter.type" />
                        
                        </tr>
                    </thead>
                    <tbody>
                    <g:each in="${compInsuranceParameterInstanceList}" status="i" var="compInsuranceParameterInstance">
                        <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
                        
                            <td><g:link action="show" id="${compInsuranceParameterInstance.id}">${fieldValue(bean: compInsuranceParameterInstance, field: "id")}</g:link></td>
                        
                            <td><g:formatDate date="${compInsuranceParameterInstance.dateFrom}" /></td>
                        
                            <td>${fieldValue(bean: compInsuranceParameterInstance, field: "customerCode")}</td>
                        
                            <td>${fieldValue(bean: compInsuranceParameterInstance, field: "defaultSupplier")}</td>
                        
                            <td>${fieldValue(bean: compInsuranceParameterInstance, field: "state")}</td>
                        
                            <td>${fieldValue(bean: compInsuranceParameterInstance, field: "type")}</td>
                        
                        </tr>
                    </g:each>
                    </tbody>
                </table>
            </div>
            <div class="paginateButtons">
                <g:paginate total="${compInsuranceParameterInstanceTotal}" />
            </div>
        </div>
    </body>
</html>
