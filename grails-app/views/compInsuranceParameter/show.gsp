
<%@ page import="com.innovated.iris.domain.lookup.CompInsuranceParameter" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <title><g:message code="compInsuranceParameter.show" default="Show CompInsuranceParameter" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLinkTo(dir: '')}"><g:message code="home" default="Home" /></a></span>
            <span class="menuButton"><g:link class="list" action="list"><g:message code="compInsuranceParameter.list" default="CompInsuranceParameter List" /></g:link></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="compInsuranceParameter.new" default="New CompInsuranceParameter" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="compInsuranceParameter.show" default="Show CompInsuranceParameter" /></h1>
            <g:if test="${flash.message}">
            <div class="message"><g:message code="${flash.message}" args="${flash.args}" default="${flash.defaultMessage}" /></div>
            </g:if>
            <g:form>
                <g:hiddenField name="id" value="${compInsuranceParameterInstance?.id}" />
                <div class="dialog">
                    <table>
                        <tbody>
                        
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="compInsuranceParameter.id" default="Id" />:</td>
                                
                                <td valign="top" class="value">${fieldValue(bean: compInsuranceParameterInstance, field: "id")}</td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="compInsuranceParameter.dateFrom" default="Date From" />:</td>
                                
                                <td valign="top" class="value"><g:formatDate date="${compInsuranceParameterInstance?.dateFrom}" /></td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="compInsuranceParameter.customerCode" default="Customer Code" />:</td>
                                
                                <td valign="top" class="value">${fieldValue(bean: compInsuranceParameterInstance, field: "customerCode")}</td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="compInsuranceParameter.defaultSupplier" default="Default Supplier" />:</td>
                                
                                <td valign="top" class="value"><g:link controller="supplier" action="show" id="${compInsuranceParameterInstance?.defaultSupplier?.id}">${compInsuranceParameterInstance?.defaultSupplier?.encodeAsHTML()}</g:link></td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="compInsuranceParameter.state" default="State" />:</td>
                                
                                <td valign="top" class="value">${fieldValue(bean: compInsuranceParameterInstance, field: "state")}</td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="compInsuranceParameter.type" default="Type" />:</td>
                                
                                <td valign="top" class="value">${fieldValue(bean: compInsuranceParameterInstance, field: "type")}</td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="compInsuranceParameter.metro" default="Metro" />:</td>
                                
                                <td valign="top" class="value">${fieldValue(bean: compInsuranceParameterInstance, field: "metro")}</td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="compInsuranceParameter.regional" default="Regional" />:</td>
                                
                                <td valign="top" class="value">${fieldValue(bean: compInsuranceParameterInstance, field: "regional")}</td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="compInsuranceParameter.under25Metro" default="Under25 Metro" />:</td>
                                
                                <td valign="top" class="value">${fieldValue(bean: compInsuranceParameterInstance, field: "under25Metro")}</td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="compInsuranceParameter.under25Regional" default="Under25 Regional" />:</td>
                                
                                <td valign="top" class="value">${fieldValue(bean: compInsuranceParameterInstance, field: "under25Regional")}</td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="compInsuranceParameter.excess" default="Excess" />:</td>
                                
                                <td valign="top" class="value">${fieldValue(bean: compInsuranceParameterInstance, field: "excess")}</td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="compInsuranceParameter.lastUpdated" default="Last Updated" />:</td>
                                
                                <td valign="top" class="value"><g:formatDate date="${compInsuranceParameterInstance?.lastUpdated}" /></td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="compInsuranceParameter.dateCreated" default="Date Created" />:</td>
                                
                                <td valign="top" class="value"><g:formatDate date="${compInsuranceParameterInstance?.dateCreated}" /></td>
                                
                            </tr>
                            
                        </tbody>
                    </table>
                </div>
                <div class="buttons">
                    <span class="button"><g:actionSubmit class="edit" action="edit" value="${message(code: 'edit', 'default': 'Edit')}" /></span>
                    <span class="button"><g:actionSubmit class="delete" action="delete" value="${message(code: 'delete', 'default': 'Delete')}" onclick="return confirm('${message(code: 'delete.confirm', 'default': 'Are you sure?')}');" /></span>
                </div>
            </g:form>
        </div>
    </body>
</html>
