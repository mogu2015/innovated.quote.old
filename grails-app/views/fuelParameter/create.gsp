
<%@ page import="com.innovated.iris.domain.lookup.FuelParameter" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <title><g:message code="fuelParameter.create" default="Create FuelParameter" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLinkTo(dir: '')}"><g:message code="home" default="Home" /></a></span>
            <span class="menuButton"><g:link class="list" action="list"><g:message code="fuelParameter.list" default="FuelParameter List" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="fuelParameter.create" default="Create FuelParameter" /></h1>
            <g:if test="${flash.message}">
            <div class="message"><g:message code="${flash.message}" args="${flash.args}" default="${flash.defaultMessage}" /></div>
            </g:if>
            <g:hasErrors bean="${fuelParameterInstance}">
            <div class="errors">
                <g:renderErrors bean="${fuelParameterInstance}" as="list" />
            </div>
            </g:hasErrors>
            <g:form action="save" method="post" >
                <div class="dialog">
                    <table>
                        <tbody>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="dateFrom"><g:message code="fuelParameter.dateFrom" default="Date From" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: fuelParameterInstance, field: 'dateFrom', 'errors')}">
                                    <g:datePicker name="dateFrom" value="${fuelParameterInstance?.dateFrom}"  />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="customerCode"><g:message code="fuelParameter.customerCode" default="Customer Code" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: fuelParameterInstance, field: 'customerCode', 'errors')}">
                                    <g:textField name="customerCode" value="${fieldValue(bean: fuelParameterInstance, field: 'customerCode')}" />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="state"><g:message code="fuelParameter.state" default="State" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: fuelParameterInstance, field: 'state', 'errors')}">
                                    <g:textField name="state" value="${fieldValue(bean: fuelParameterInstance, field: 'state')}" />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="defaultSupplier"><g:message code="fuelParameter.defaultSupplier" default="Default Supplier" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: fuelParameterInstance, field: 'defaultSupplier', 'errors')}">
                                    <g:select name="defaultSupplier.id" from="${com.innovated.iris.domain.Supplier.list()}" optionKey="id" value="${fuelParameterInstance?.defaultSupplier?.id}"  />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="type"><g:message code="fuelParameter.type" default="Type" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: fuelParameterInstance, field: 'type', 'errors')}">
                                    <g:select name="type.id" from="${com.innovated.iris.domain.enums.FuelType.list()}" optionKey="id" value="${fuelParameterInstance?.type?.id}"  />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="dollarsPerLitre"><g:message code="fuelParameter.dollarsPerLitre" default="Dollars Per Litre" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: fuelParameterInstance, field: 'dollarsPerLitre', 'errors')}">
                                    <g:select name="dollarsPerLitre" from="${0.01..2.0}" value="${fuelParameterInstance?.dollarsPerLitre}"  />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="dollarsPerLitreRegional"><g:message code="fuelParameter.dollarsPerLitreRegional" default="Dollars Per Litre Regional" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: fuelParameterInstance, field: 'dollarsPerLitreRegional', 'errors')}">
                                    <g:select name="dollarsPerLitreRegional" from="${0.01..2.0}" value="${fuelParameterInstance?.dollarsPerLitreRegional}"  />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="lastUpdated"><g:message code="fuelParameter.lastUpdated" default="Last Updated" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: fuelParameterInstance, field: 'lastUpdated', 'errors')}">
                                    <g:datePicker name="lastUpdated" value="${fuelParameterInstance?.lastUpdated}"  />

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="dateCreated"><g:message code="fuelParameter.dateCreated" default="Date Created" />:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: fuelParameterInstance, field: 'dateCreated', 'errors')}">
                                    <g:datePicker name="dateCreated" value="${fuelParameterInstance?.dateCreated}"  />

                                </td>
                            </tr>
                        
                        </tbody>
                    </table>
                </div>
                <div class="buttons">
                    <span class="button"><g:submitButton name="create" class="save" value="${message(code: 'create', 'default': 'Create')}" /></span>
                </div>
            </g:form>
        </div>
    </body>
</html>
