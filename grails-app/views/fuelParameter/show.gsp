
<%@ page import="com.innovated.iris.domain.lookup.FuelParameter" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <title><g:message code="fuelParameter.show" default="Show FuelParameter" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLinkTo(dir: '')}"><g:message code="home" default="Home" /></a></span>
            <span class="menuButton"><g:link class="list" action="list"><g:message code="fuelParameter.list" default="FuelParameter List" /></g:link></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="fuelParameter.new" default="New FuelParameter" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="fuelParameter.show" default="Show FuelParameter" /></h1>
            <g:if test="${flash.message}">
            <div class="message"><g:message code="${flash.message}" args="${flash.args}" default="${flash.defaultMessage}" /></div>
            </g:if>
            <g:form>
                <g:hiddenField name="id" value="${fuelParameterInstance?.id}" />
                <div class="dialog">
                    <table>
                        <tbody>
                        
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="fuelParameter.id" default="Id" />:</td>
                                
                                <td valign="top" class="value">${fieldValue(bean: fuelParameterInstance, field: "id")}</td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="fuelParameter.dateFrom" default="Date From" />:</td>
                                
                                <td valign="top" class="value"><g:formatDate date="${fuelParameterInstance?.dateFrom}" /></td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="fuelParameter.customerCode" default="Customer Code" />:</td>
                                
                                <td valign="top" class="value">${fieldValue(bean: fuelParameterInstance, field: "customerCode")}</td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="fuelParameter.state" default="State" />:</td>
                                
                                <td valign="top" class="value">${fieldValue(bean: fuelParameterInstance, field: "state")}</td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="fuelParameter.defaultSupplier" default="Default Supplier" />:</td>
                                
                                <td valign="top" class="value"><g:link controller="supplier" action="show" id="${fuelParameterInstance?.defaultSupplier?.id}">${fuelParameterInstance?.defaultSupplier?.encodeAsHTML()}</g:link></td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="fuelParameter.type" default="Type" />:</td>
                                
                                <td valign="top" class="value"><g:link controller="fuelType" action="show" id="${fuelParameterInstance?.type?.id}">${fuelParameterInstance?.type?.encodeAsHTML()}</g:link></td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="fuelParameter.dollarsPerLitre" default="Dollars Per Litre" />:</td>
                                
                                <td valign="top" class="value">${fieldValue(bean: fuelParameterInstance, field: "dollarsPerLitre")}</td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="fuelParameter.dollarsPerLitreRegional" default="Dollars Per Litre Regional" />:</td>
                                
                                <td valign="top" class="value">${fieldValue(bean: fuelParameterInstance, field: "dollarsPerLitreRegional")}</td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="fuelParameter.lastUpdated" default="Last Updated" />:</td>
                                
                                <td valign="top" class="value"><g:formatDate date="${fuelParameterInstance?.lastUpdated}" /></td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="fuelParameter.dateCreated" default="Date Created" />:</td>
                                
                                <td valign="top" class="value"><g:formatDate date="${fuelParameterInstance?.dateCreated}" /></td>
                                
                            </tr>
                            
                        </tbody>
                    </table>
                </div>
                <div class="buttons">
                    <span class="button"><g:actionSubmit class="edit" action="edit" value="${message(code: 'edit', 'default': 'Edit')}" /></span>
                    <span class="button"><g:actionSubmit class="delete" action="delete" value="${message(code: 'delete', 'default': 'Delete')}" onclick="return confirm('${message(code: 'delete.confirm', 'default': 'Are you sure?')}');" /></span>
                </div>
            </g:form>
        </div>
    </body>
</html>
