
<%@ page import="com.innovated.iris.domain.lookup.FuelParameter" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <title><g:message code="fuelParameter.list" default="FuelParameter List" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLinkTo(dir: '')}"><g:message code="home" default="Home" /></a></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="fuelParameter.new" default="New FuelParameter" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="fuelParameter.list" default="FuelParameter List" /></h1>
            <g:if test="${flash.message}">
            <div class="message"><g:message code="${flash.message}" args="${flash.args}" default="${flash.defaultMessage}" /></div>
            </g:if>
            <div class="list">
                <table>
                    <thead>
                        <tr>
                        
                   	    <g:sortableColumn property="id" title="Id" titleKey="fuelParameter.id" />
                        
                   	    <g:sortableColumn property="dateFrom" title="Date From" titleKey="fuelParameter.dateFrom" />
                        
                   	    <g:sortableColumn property="customerCode" title="Customer Code" titleKey="fuelParameter.customerCode" />
                        
                   	    <g:sortableColumn property="state" title="State" titleKey="fuelParameter.state" />
                        
                   	    <th><g:message code="fuelParameter.defaultSupplier" default="Default Supplier" /></th>
                   	    
                   	    <th><g:message code="fuelParameter.type" default="Type" /></th>
                   	    
                        </tr>
                    </thead>
                    <tbody>
                    <g:each in="${fuelParameterInstanceList}" status="i" var="fuelParameterInstance">
                        <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
                        
                            <td><g:link action="show" id="${fuelParameterInstance.id}">${fieldValue(bean: fuelParameterInstance, field: "id")}</g:link></td>
                        
                            <td><g:formatDate date="${fuelParameterInstance.dateFrom}" /></td>
                        
                            <td>${fieldValue(bean: fuelParameterInstance, field: "customerCode")}</td>
                        
                            <td>${fieldValue(bean: fuelParameterInstance, field: "state")}</td>
                        
                            <td>${fieldValue(bean: fuelParameterInstance, field: "defaultSupplier")}</td>
                        
                            <td>${fieldValue(bean: fuelParameterInstance, field: "type")}</td>
                        
                        </tr>
                    </g:each>
                    </tbody>
                </table>
            </div>
            <div class="paginateButtons">
                <g:paginate total="${fuelParameterInstanceTotal}" />
            </div>
        </div>
    </body>
</html>
