
<%@ page import="com.innovated.iris.domain.enums.VehicleModel" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <title><g:message code="vehicleModel.list" default="Vehicle Model List" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLinkTo(dir: '')}"><g:message code="home" default="Home" /></a></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="vehicleModel.new" default="New Vehicle Model" /></g:link></span>
            <span class="menuButton"><g:link class="save" action="importFile"><g:message code="vehicleModel.import" default="Import Model List" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="vehicleModel.list" default="Vehicle Model List" /></h1>
            <g:if test="${flash.message}">
            <div class="message"><g:message code="${flash.message}" args="${flash.args}" default="${flash.defaultMessage}" /></div>
            </g:if>
            <div class="list">
                <table>
                    <thead>
                        <tr>
                        
                   	    <g:sortableColumn property="id" title="Id" titleKey="vehicleModel.id" />
                        
                   	    <g:sortableColumn property="name" title="Name" titleKey="vehicleModel.name" />
                        
                   	    <th><g:message code="vehicleModel.variants" default="Variants" /></th>
                   	    
                   	    <g:sortableColumn property="firstReleaseDate" title="First Release Date" titleKey="vehicleModel.firstReleaseDate" />
                        
                   	    <g:sortableColumn property="vehicleClass" title="Vehicle Class" titleKey="vehicleModel.vehicleClass" />
                        
                   	    <th><g:message code="vehicleModel.make" default="Make" /></th>
                   	    
                        </tr>
                    </thead>
                    <tbody>
                    <g:each in="${vehicleModelInstanceList}" status="i" var="vehicleModelInstance">
                        <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
                        
                            <td><g:link action="show" id="${vehicleModelInstance.id}">${fieldValue(bean: vehicleModelInstance, field: "id")}</g:link></td>
                        
                            <td>${fieldValue(bean: vehicleModelInstance, field: "name")}</td>
                        
                            <td>${fieldValue(bean: vehicleModelInstance, field: "variants")}</td>
                        
                            <td><g:formatDate date="${vehicleModelInstance.firstReleaseDate}" /></td>
                        
                            <td>${fieldValue(bean: vehicleModelInstance, field: "vehicleClass")}</td>
                        
                            <td>${fieldValue(bean: vehicleModelInstance, field: "make")}</td>
                        
                        </tr>
                    </g:each>
                    </tbody>
                </table>
            </div>
            <div class="paginateButtons">
                <g:paginate total="${vehicleModelInstanceTotal}" />
            </div>
        </div>
    </body>
</html>
