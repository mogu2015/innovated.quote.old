
<%@ page import="com.innovated.iris.domain.enums.VehicleModel" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <title><g:message code="vehicleModel.create" default="Create VehicleModel" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLinkTo(dir: '')}"><g:message code="home" default="Home" /></a></span>
            <span class="menuButton"><g:link class="list" action="list"><g:message code="vehicleModel.list" default="VehicleModel List" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="vehicleModel.create" default="Create VehicleModel" /></h1>
            <g:if test="${flash.message}">
            	<div class="message"><g:message code="${flash.message}" args="${flash.args}" default="${flash.defaultMessage}" /></div>
            </g:if>
            <g:hasErrors bean="${vehicleModelInstance}">
				<div class="errors">
					<g:renderErrors bean="${vehicleModelInstance}" as="list" />
				</div>
            </g:hasErrors>
            <g:form action="save" method="post" >
                <div class="dialog">
                    <fieldset>
                        <legend><g:message code="vehicleModel.create.legend" default="Enter VehicleModel Details"/></legend>
                        
                            <div class="prop mandatory ${hasErrors(bean: vehicleModelInstance, field: 'make', 'error')}">
                                <label for="make">
                                    <g:message code="vehicleModel.make" default="Make" />
                                    <span class="indicator">*</span>
                                </label>
                                <g:select name="make.id" from="${com.innovated.iris.domain.enums.VehicleManufacturer.list()}" optionKey="id" value="${vehicleModelInstance?.make?.id}"  />

                            </div>
                            
                            <div class="prop mandatory ${hasErrors(bean: vehicleModelInstance, field: 'name', 'error')}">
                                <label for="name">
                                    <g:message code="vehicleModel.name" default="Model Name" />
                                    <span class="indicator">*</span>
                                </label>
                                <g:textField name="name" value="${fieldValue(bean: vehicleModelInstance, field: 'name')}" />

                            </div>
                            
                            <div class="prop ${hasErrors(bean: vehicleModelInstance, field: 'vehicleClass', 'error')}">
                                <label for="vehicleClass">
                                    <g:message code="vehicleModel.vehicleClass" default="Vehicle Class" />
                                    
                                </label>
                                <g:select name="vehicleClass" from="${vehicleModelInstance.constraints.vehicleClass.inList}" value="${vehicleModelInstance.vehicleClass}" valueMessagePrefix="vehicleModel.vehicleClass"  />

                            </div>
                        
                            <div class="prop ${hasErrors(bean: vehicleModelInstance, field: 'firstReleaseDate', 'error')}">
                                <label for="firstReleaseDate">
                                    <g:message code="vehicleModel.firstReleaseDate" default="First Release Date" />
                                    
                                </label>
                                <g:datePicker name="firstReleaseDate" value="${vehicleModelInstance?.firstReleaseDate}" noSelection="['': '']" precision="day" />

                            </div>
                        
                    </fieldset>
                </div>
                <div class="buttons">
                    <span class="button"><g:submitButton name="create" class="save" value="${message(code: 'create', 'default': 'Create')}" /></span>
                </div>
            </g:form>
        </div>
    </body>
</html>

