
<%@ page import="com.innovated.iris.domain.enums.VehicleModel" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <title><g:message code="vehicleModel.edit" default="Edit VehicleModel" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLinkTo(dir: '')}"><g:message code="home" default="Home" /></a></span>
            <span class="menuButton"><g:link class="list" action="list"><g:message code="vehicleModel.list" default="VehicleModel List" /></g:link></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="vehicleModel.new" default="New VehicleModel" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="vehicleModel.edit" default="Edit VehicleModel" /></h1>
            <g:if test="${flash.message}">
            	<div class="message"><g:message code="${flash.message}" args="${flash.args}" default="${flash.defaultMessage}" /></div>
            </g:if>
            <g:hasErrors bean="${vehicleModelInstance}">
				<div class="errors">
					<g:renderErrors bean="${vehicleModelInstance}" as="list" />
				</div>
            </g:hasErrors>
            <g:form method="post" >
                <g:hiddenField name="id" value="${vehicleModelInstance?.id}" />
                <g:hiddenField name="version" value="${vehicleModelInstance?.version}" />
                <div class="dialog">
                    <fieldset>
                        <legend><g:message code="vehicleModel.edit.legend" default="Update VehicleModel Details"/></legend>
                        
                        <div class="prop mandatory ${hasErrors(bean: vehicleModelInstance, field: 'name', 'error')}">
                            <label for="name">
                                <g:message code="vehicleModel.name" default="Name" />
                                <span class="indicator">*</span>
                            </label>
                            <g:textField name="name" value="${fieldValue(bean: vehicleModelInstance, field: 'name')}" />

                        </div>
                        
                        <div class="prop mandatory ${hasErrors(bean: vehicleModelInstance, field: 'variants', 'error')}">
                            <label for="variants">
                                <g:message code="vehicleModel.variants" default="Variants" />
                                <span class="indicator">*</span>
                            </label>
                            
<ul>
<g:each in="${vehicleModelInstance?.variants}" var="makeModelInstance">
    <li><g:link controller="makeModel" action="show" id="${makeModelInstance.id}">${makeModelInstance?.encodeAsHTML()}</g:link></li>
</g:each>
</ul>
<g:link controller="makeModel" params="['model.id': vehicleModelInstance?.id]" action="create"><g:message code="makeModel.new" default="New MakeModel" /></g:link>


                        </div>
                        
                        <div class="prop ${hasErrors(bean: vehicleModelInstance, field: 'firstReleaseDate', 'error')}">
                            <label for="firstReleaseDate">
                                <g:message code="vehicleModel.firstReleaseDate" default="First Release Date" />
                                
                            </label>
                            <g:datePicker name="firstReleaseDate" value="${vehicleModelInstance?.firstReleaseDate}" noSelection="['': '']" />

                        </div>
                        
                        <div class="prop ${hasErrors(bean: vehicleModelInstance, field: 'vehicleClass', 'error')}">
                            <label for="vehicleClass">
                                <g:message code="vehicleModel.vehicleClass" default="Vehicle Class" />
                                
                            </label>
                            <g:select name="vehicleClass" from="${vehicleModelInstance.constraints.vehicleClass.inList}" value="${vehicleModelInstance.vehicleClass}" valueMessagePrefix="vehicleModel.vehicleClass"  />

                        </div>
                        
                        <div class="prop mandatory ${hasErrors(bean: vehicleModelInstance, field: 'make', 'error')}">
                            <label for="make">
                                <g:message code="vehicleModel.make" default="Make" />
                                <span class="indicator">*</span>
                            </label>
                            <g:select name="make.id" from="${com.innovated.iris.domain.enums.VehicleManufacturer.list()}" optionKey="id" value="${vehicleModelInstance?.make?.id}"  />

                        </div>
                        
                    </fieldset>
                </div>
                <div class="buttons">
                    <span class="button"><g:actionSubmit class="save" action="update" value="${message(code: 'update', 'default': 'Update')}" /></span>
                    <span class="button"><g:actionSubmit class="delete" action="delete" value="${message(code: 'delete', 'default': 'Delete')}" onclick="return confirm('${message(code: 'delete.confirm', 'default': 'Are you sure?')}');" /></span>
                </div>
            </g:form>
        </div>
    </body>
</html>

