
<%@ page import="com.innovated.iris.domain.enums.VehicleManufacturer" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <title><g:message code="vehicleManufacturer.edit" default="Edit VehicleManufacturer" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLinkTo(dir: '')}"><g:message code="home" default="Home" /></a></span>
            <span class="menuButton"><g:link class="list" action="list"><g:message code="vehicleManufacturer.list" default="VehicleManufacturer List" /></g:link></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="vehicleManufacturer.new" default="New VehicleManufacturer" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="vehicleManufacturer.edit" default="Edit VehicleManufacturer" /></h1>
            <g:if test="${flash.message}">
            	<div class="message"><g:message code="${flash.message}" args="${flash.args}" default="${flash.defaultMessage}" /></div>
            </g:if>
            <g:hasErrors bean="${vehicleManufacturerInstance}">
				<div class="errors">
					<g:renderErrors bean="${vehicleManufacturerInstance}" as="list" />
				</div>
            </g:hasErrors>
            <g:form method="post" >
                <g:hiddenField name="id" value="${vehicleManufacturerInstance?.id}" />
                <g:hiddenField name="version" value="${vehicleManufacturerInstance?.version}" />
                <div class="dialog">
                    <fieldset>
                        <legend><g:message code="vehicleManufacturer.edit.legend" default="Update VehicleManufacturer Details"/></legend>
                        
                        <div class="prop mandatory ${hasErrors(bean: vehicleManufacturerInstance, field: 'name', 'error')}">
                            <label for="name">
                                <g:message code="vehicleManufacturer.name" default="Name" />
                                <span class="indicator">*</span>
                            </label>
                            <g:textField name="name" value="${fieldValue(bean: vehicleManufacturerInstance, field: 'name')}" />

                        </div>
                        
                        <div class="prop mandatory ${hasErrors(bean: vehicleManufacturerInstance, field: 'models', 'error')}">
                            <label for="models">
                                <g:message code="vehicleManufacturer.models" default="Models" />
                                <span class="indicator">*</span>
                            </label>
                            
<ul>
<g:each in="${vehicleManufacturerInstance?.models}" var="vehicleModelInstance">
    <li><g:link controller="vehicleModel" action="show" id="${vehicleModelInstance.id}">${vehicleModelInstance?.encodeAsHTML()}</g:link></li>
</g:each>
</ul>
<g:link controller="vehicleModel" params="['make.id': vehicleManufacturerInstance?.id]" action="create"><g:message code="vehicleModel.new" default="New VehicleModel" /></g:link>


                        </div>
                        
                    </fieldset>
                </div>
                <div class="buttons">
                    <span class="button"><g:actionSubmit class="save" action="update" value="${message(code: 'update', 'default': 'Update')}" /></span>
                    <span class="button"><g:actionSubmit class="delete" action="delete" value="${message(code: 'delete', 'default': 'Delete')}" onclick="return confirm('${message(code: 'delete.confirm', 'default': 'Are you sure?')}');" /></span>
                </div>
            </g:form>
        </div>
    </body>
</html>

