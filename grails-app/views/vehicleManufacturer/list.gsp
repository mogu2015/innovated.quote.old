
<%@ page import="com.innovated.iris.domain.enums.VehicleManufacturer" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <title><g:message code="vehicleManufacturer.list" default="VehicleManufacturer List" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLinkTo(dir: '')}"><g:message code="home" default="Home" /></a></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="vehicleManufacturer.new" default="New VehicleManufacturer" /></g:link></span>
            <span class="menuButton"><g:link class="save" action="importFile"><g:message code="vehicleManufacturer.import" default="Import Make List" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="vehicleManufacturer.list" default="VehicleManufacturer List" /></h1>
            <g:if test="${flash.message}">
            <div class="message"><g:message code="${flash.message}" args="${flash.args}" default="${flash.defaultMessage}" /></div>
            </g:if>
            <div class="list">
                <table>
                    <thead>
                        <tr>
                        
                   	    <g:sortableColumn property="id" title="Id" titleKey="vehicleManufacturer.id" />
                        
                   	    <g:sortableColumn property="name" title="Name" titleKey="vehicleManufacturer.name" />
                        
                   	    <th><g:message code="vehicleManufacturer.models" default="Models" /></th>
                   	    
                        </tr>
                    </thead>
                    <tbody>
                    <g:each in="${vehicleManufacturerInstanceList}" status="i" var="vehicleManufacturerInstance">
                        <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
                        
                            <td><g:link action="show" id="${vehicleManufacturerInstance.id}">${fieldValue(bean: vehicleManufacturerInstance, field: "id")}</g:link></td>
                        
                            <td>${fieldValue(bean: vehicleManufacturerInstance, field: "name")}</td>
                        
                            <td>
                            	<ul>
                            	<g:each in="${vehicleManufacturerInstance.models}" var="model">
                            		<li>${model.name}</li>
                            	</g:each>
                            	</ul>
                            </td>
                        
                        </tr>
                    </g:each>
                    </tbody>
                </table>
            </div>
            <div class="paginateButtons">
                <g:paginate total="${vehicleManufacturerInstanceTotal}" />
            </div>
        </div>
    </body>
</html>
