
<%@ page import="com.innovated.iris.domain.enums.VehicleManufacturer" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <title><g:message code="vehicleManufacturer.create" default="Create VehicleManufacturer" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLinkTo(dir: '')}"><g:message code="home" default="Home" /></a></span>
            <span class="menuButton"><g:link class="list" action="list"><g:message code="vehicleManufacturer.list" default="VehicleManufacturer List" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="vehicleManufacturer.create" default="Create VehicleManufacturer" /></h1>
            <g:if test="${flash.message}">
            	<div class="message"><g:message code="${flash.message}" args="${flash.args}" default="${flash.defaultMessage}" /></div>
            </g:if>
            <g:hasErrors bean="${vehicleManufacturerInstance}">
				<div class="errors">
					<g:renderErrors bean="${vehicleManufacturerInstance}" as="list" />
				</div>
            </g:hasErrors>
            <g:form action="save" method="post" >
                <div class="dialog">
                    <fieldset>
                        <legend><g:message code="vehicleManufacturer.create.legend" default="Enter VehicleManufacturer Details"/></legend>
                        
                            <div class="prop mandatory ${hasErrors(bean: vehicleManufacturerInstance, field: 'name', 'error')}">
                                <label for="name">
                                    <g:message code="vehicleManufacturer.name" default="Name" />
                                    <span class="indicator">*</span>
                                </label>
                                <g:textField name="name" value="${fieldValue(bean: vehicleManufacturerInstance, field: 'name')}" />

                            </div>
                        
                    </fieldset>
                </div>
                <div class="buttons">
                    <span class="button"><g:submitButton name="create" class="save" value="${message(code: 'create', 'default': 'Create')}" /></span>
                </div>
            </g:form>
        </div>
    </body>
</html>

