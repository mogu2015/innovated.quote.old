<%@
page import="com.innovated.iris.domain.enums.VehicleManufacturer"
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <title><g:message code="vehicleManufacturer.import" default="Import Vehicle Makes" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLinkTo(dir: '')}"><g:message code="home" default="Home" /></a></span>
            <span class="menuButton"><g:link class="list" action="list"><g:message code="vehicleManufacturer.list" default="VehicleManufacturer List" /></g:link></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="vehicleManufacturer.new" default="New VehicleManufacturer" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="vehicleManufacturer.import" default="Import Vehicle Makes" /></h1>
            <g:if test="${flash.message}">
            <div class="message"><g:message code="${flash.message}" args="${flash.args}" default="${flash.defaultMessage}" /></div>
            </g:if>
            <div class="dialog">
                <g:uploadForm name="xlsUpload" action="saveXls">
					<label for="importFile">Select XLS File:</label>  
					<input type="file" name="importFile" />
					<div class="buttons"><span class="button"><input class="save" type="submit" value="Import"/></span></div> 
				</g:uploadForm>
            </div>
        </div>
    </body>
</html>
