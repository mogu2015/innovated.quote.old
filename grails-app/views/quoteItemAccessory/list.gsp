
<%@ page import="com.innovated.iris.domain.QuoteItemAccessory" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <title><g:message code="quoteItemAccessory.list" default="QuoteItemAccessory List" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLinkTo(dir: '')}"><g:message code="home" default="Home" /></a></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="quoteItemAccessory.new" default="New QuoteItemAccessory" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="quoteItemAccessory.list" default="QuoteItemAccessory List" /></h1>
            <g:if test="${flash.message}">
            <div class="message"><g:message code="${flash.message}" args="${flash.args}" default="${flash.defaultMessage}" /></div>
            </g:if>
            <div class="list">
                <table>
                    <thead>
                        <tr>
                        
                   	    <g:sortableColumn property="id" title="Id" titleKey="quoteItemAccessory.id" />
                        
                   	    <g:sortableColumn property="pageKey" title="Page Key" titleKey="quoteItemAccessory.pageKey" />
                        
                   	    <g:sortableColumn property="name" title="Name" titleKey="quoteItemAccessory.name" />
                        
                   	    <g:sortableColumn property="value" title="Value" titleKey="quoteItemAccessory.value" />
                        
                   	    <th><g:message code="quoteItemAccessory.quote" default="Quote" /></th>
                   	    
                        </tr>
                    </thead>
                    <tbody>
                    <g:each in="${quoteItemAccessoryInstanceList}" status="i" var="quoteItemAccessoryInstance">
                        <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
                        
                            <td><g:link action="show" id="${quoteItemAccessoryInstance.id}">${fieldValue(bean: quoteItemAccessoryInstance, field: "id")}</g:link></td>
                        
                            <td>${fieldValue(bean: quoteItemAccessoryInstance, field: "pageKey")}</td>
                        
                            <td>${fieldValue(bean: quoteItemAccessoryInstance, field: "name")}</td>
                        
                            <td>${fieldValue(bean: quoteItemAccessoryInstance, field: "value")}</td>
                        
                            <td>${fieldValue(bean: quoteItemAccessoryInstance, field: "quote")}</td>
                        
                        </tr>
                    </g:each>
                    </tbody>
                </table>
            </div>
            <div class="paginateButtons">
                <g:paginate total="${quoteItemAccessoryInstanceTotal}" />
            </div>
        </div>
    </body>
</html>
