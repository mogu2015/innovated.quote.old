
<%@ page import="com.innovated.iris.domain.QuoteItemAccessory" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <title><g:message code="quoteItemAccessory.show" default="Show QuoteItemAccessory" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLinkTo(dir: '')}"><g:message code="home" default="Home" /></a></span>
            <span class="menuButton"><g:link class="list" action="list"><g:message code="quoteItemAccessory.list" default="QuoteItemAccessory List" /></g:link></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="quoteItemAccessory.new" default="New QuoteItemAccessory" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="quoteItemAccessory.show" default="Show QuoteItemAccessory" /></h1>
            <g:if test="${flash.message}">
            <div class="message"><g:message code="${flash.message}" args="${flash.args}" default="${flash.defaultMessage}" /></div>
            </g:if>
            <g:form>
                <g:hiddenField name="id" value="${quoteItemAccessoryInstance?.id}" />
                <div class="dialog">
                    <table>
                        <tbody>
                        
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="quoteItemAccessory.id" default="Id" />:</td>
                                
                                <td valign="top" class="value">${fieldValue(bean: quoteItemAccessoryInstance, field: "id")}</td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="quoteItemAccessory.pageKey" default="Page Key" />:</td>
                                
                                <td valign="top" class="value">${fieldValue(bean: quoteItemAccessoryInstance, field: "pageKey")}</td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="quoteItemAccessory.name" default="Name" />:</td>
                                
                                <td valign="top" class="value">${fieldValue(bean: quoteItemAccessoryInstance, field: "name")}</td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="quoteItemAccessory.value" default="Value" />:</td>
                                
                                <td valign="top" class="value">${fieldValue(bean: quoteItemAccessoryInstance, field: "value")}</td>
                                
                            </tr>
                            
                            <tr class="prop">
                                <td valign="top" class="name"><g:message code="quoteItemAccessory.quote" default="Quote" />:</td>
                                
                                <td valign="top" class="value"><g:link controller="quote" action="show" id="${quoteItemAccessoryInstance?.quote?.id}">${quoteItemAccessoryInstance?.quote?.encodeAsHTML()}</g:link></td>
                                
                            </tr>
                            
                        </tbody>
                    </table>
                </div>
                <div class="buttons">
                    <span class="button"><g:actionSubmit class="edit" action="edit" value="${message(code: 'edit', 'default': 'Edit')}" /></span>
                    <span class="button"><g:actionSubmit class="delete" action="delete" value="${message(code: 'delete', 'default': 'Delete')}" onclick="return confirm('${message(code: 'delete.confirm', 'default': 'Are you sure?')}');" /></span>
                </div>
            </g:form>
        </div>
    </body>
</html>
