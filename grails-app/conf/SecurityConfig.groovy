security {
	// see DefaultSecurityConfig.groovy for all settable/overridable properties
	active = true

	loginUserDomainClass = "com.innovated.iris.domain.auth.User"
	authorityDomainClass = "com.innovated.iris.domain.auth.Role"
	
	defaultTargetUrl = "/login"
	
	useRequestMapDomainClass = false
	
	// user session tracking
	//useHttpSessionEventPublisher = true
	
	useControllerAnnotations = true
	controllerAnnotationsRejectIfNoRule = false
	controllerAnnotationStaticRules = [
		//'/*': ['IS_AUTHENTICATED_FULLY'],
		//'/**': ['IS_AUTHENTICATED_FULLY'],
		'/js/**': ['IS_AUTHENTICATED_ANONYMOUSLY'],
		'/css/**': ['IS_AUTHENTICATED_ANONYMOUSLY'],
		'/images/**': ['IS_AUTHENTICATED_ANONYMOUSLY'],
		'/confirm/**': ['IS_AUTHENTICATED_ANONYMOUSLY'],
		'/j_spring_security_check': ['IS_AUTHENTICATED_ANONYMOUSLY'],
		'/login/authAjaxFull': ['IS_AUTHENTICATED_ANONYMOUSLY']
	]
}
