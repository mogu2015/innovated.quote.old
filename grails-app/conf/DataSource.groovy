dataSource {
	pooled = true
	driverClassName = "com.mysql.jdbc.Driver"
	dialect = "org.hibernate.dialect.MySQL5InnoDBDialect"
//	username = "username"
//	password = "password"
}
hibernate {
	cache.use_second_level_cache = true
	cache.use_query_cache = true
	cache.provider_class = 'net.sf.ehcache.hibernate.EhCacheProvider'
}
// environment specific settings
environments {
	development {
		dataSource {
			dbCreate = "none" // one of 'create', 'create-drop','update'
			url = "jdbc:mysql://localhost/iris_prod"
			username = "tamati"
			password = "kelly79"
			//logSql = true
		}
	}
	test {
		dataSource {
			dbCreate = "none"
			url = "jdbc:mysql://localhost/iris_test"
			username = "innov_quote"
			password = "svCZ7tDNaJSu8JjA"
		}
	}
	production {
		dataSource {
			dbCreate = "none" // one of 'create', 'create-drop','update'
			url = "jdbc:mysql://localhost/iris_prod"
			username = "tamati"
			password = "kelly79"
		}
	}
}
