import java.util.regex.Pattern.First;

import org.apache.commons.lang.RandomStringUtils;
import org.codehaus.groovy.grails.commons.ApplicationAttributes;

import grails.util.*
import com.innovated.iris.util.PayrollFrequency;
import com.innovated.iris.domain.*
import com.innovated.iris.domain.auth.*
import com.innovated.iris.domain.enums.*
import com.innovated.iris.domain.lookup.*
import com.innovated.iris.server.*
import com.innovated.iris.server.fbt.*
import com.innovated.iris.services.*

class BootStrap {
	//def authenticationManager
	def sessionController
	def authenticateService
	def emailConfirmationService
	def returnToSenderService

	private static final DB_CONN_TEST_INTERVAL = 1000 * 60 * 15	// run DB connection test every 15mins!

	def init = { servletContext ->
		// user session tracking
		//authenticationManager.sessionController = sessionController 

		// fix stale DB connections!!!
		def ctx = servletContext.getAttribute(ApplicationAttributes.APPLICATION_CONTEXT)
		def ds = ctx.dataSourceUnproxied
		ds.with { d ->
			d.setMinEvictableIdleTimeMillis(DB_CONN_TEST_INTERVAL)
			d.setTimeBetweenEvictionRunsMillis(DB_CONN_TEST_INTERVAL)
			d.setNumTestsPerEvictionRun(3)
			d.setTestOnBorrow(true)
			d.setTestWhileIdle(true)
			d.setTestOnReturn(true)
			d.setValidationQuery('select 1')
			d.setMinIdle(1)
			d.setMaxActive(16)
			d.setInitialSize(8)
		}
		if (log.infoEnabled) {
			log.info "Configured Datasource properties:"
			ds.properties.findAll{ k, v -> !k.contains('password') }.each { p ->
				log.info "  $p"
			}
		}

		//email confirmation
		emailConfirmationService.onConfirmation = { email, uid ->
			return returnToSenderService.confirmed(email, uid)
		}
		emailConfirmationService.onInvalid = { uid ->
			return returnToSenderService.invalid(uid)
		}
		emailConfirmationService.onTimeout = { email, uid ->
			returnToSenderService.timedOut(email, uid)
		}
		emailConfirmationService.maxAge = 1000 * 60 * 60 * 24 * 3L // confirmations valid for 3 days


		if (Environment.current == Environment.PRODUCTION) {
			bootstrapProd()
		}
		else if (Environment.current == Environment.TEST) {
			bootstrapTest()
		}
		else {
//			bootstrapDev()
//			if (!Role.findByAuthority('TEST'))
//			new Role(authority:'TEST',description:"test").save()
		}
	}

	def destroy = {
	}

	private def createDefaultRoles() {
		if (!Role.findByAuthority('ROLE_ADMIN'))
			new Role(authority:'ROLE_ADMIN',description:"Administrator").save()

		if (!Role.findByAuthority('ROLE_SUPER'))
			new Role(authority:'ROLE_SUPER',description:"Super User").save()

		if (!Role.findByAuthority('ROLE_USER'))
			new Role(authority:'ROLE_USER',description:"User").save()

		if (!Role.findByAuthority('ROLE_EMPLOYER'))
			new Role(authority:'ROLE_EMPLOYER',description:"Employer role").save()
	}

	private def createDefaultUsers() {
		def admin = User.findByUsername('test')
		if (!admin) {
			admin = new User(username:'test', userRealName:'Test Admin User',
					passwd:authenticateService.passwordEncoder('test'),
					email:'super@example.com', enabled:true)
			admin.save()
		}

		def user = User.findByUsername('user')
		if (user == null) {
			user = new User(username:'user',userRealName:'Normal User',
					passwd:authenticateService.passwordEncoder('user'),
					email:'user@example.com', enabled:true)
			user.save()
		}

		def employer = User.findByUsername('employer')
		if (employer == null) {
			employer = new User(username:'employer',userRealName:'employer User',
					passwd:authenticateService.passwordEncoder('employer'),
					email:'employer@example.com', enabled:true)
			employer.save()
		}

		Role.findByAuthority('ROLE_ADMIN').addToPeople(admin)
		Role.findByAuthority('ROLE_SUPER').addToPeople(admin)
		Role.findByAuthority('ROLE_USER').addToPeople(user)
		Role.findByAuthority('ROLE_EMPLOYER').addToPeople(employer)
	}

	private def bootstrapDev() {
		//
		// Date used for parameters...
		def calendar = new GregorianCalendar(2009, Calendar.NOVEMBER, 1)
		def asAtDate = calendar.time

		createDefaultRoles()
		createDefaultUsers()

		//=========== Add some default enum values... ===============//
		// Phone types
		def wkPh = new PhoneType(name:"Work Phone", visible:true, enabled:true, displayFormat:"(##) ####-####").save()
		def wkFax = new PhoneType(name:"Work Fax", visible:true, enabled:true, displayFormat:"(##) ####-####").save()
		def hmPh = new PhoneType(name:"Home Phone", visible:true, enabled:true, displayFormat:"(##) ####-####").save()
		new PhoneType(name:"Home Fax", visible:true, enabled:true, displayFormat:"(##) ####-####").save()
		def mobPh = new PhoneType(name:"Mobile", visible:true, enabled:true, displayFormat:"#### ### ###").save()

		// Address types
		def postalAddrType = new AddressType(name:"Postal Address", visible:true, enabled:true).save()
		def streetAddrType = new AddressType(name:"Street Address", visible:true, enabled:true).save()
		new AddressType(name:"Delivery Address", visible:true, enabled:true).save()

		// Customer types
		def custType = new CustomerType(name:"Novated", typeCode:"NOV", visible:true, enabled:true).save()
		new CustomerType(name:"Fleet", typeCode:"FLT", visible:true, enabled:true).save()
		new CustomerType(name:"Managed", typeCode:"MNG", visible:true, enabled:true).save()

		// Supplier types
		def suppType = new SupplierType(name:"Financier", typeCode:"FIN", visible:true, enabled:true).save()
		def regoSupp = new SupplierType(name:"Registration Authority", typeCode:"REGO", visible:true, enabled:true).save()
		new SupplierType(name:"Vehicle Dealer", typeCode:"VDLR", visible:true, enabled:true).save()
		new SupplierType(name:"Vehicle Repairer", typeCode:"VRPR", visible:true, enabled:true).save()
		def roadsideSupp = new SupplierType(name:"Roadside/Motor Club", typeCode:"RDSIDE", visible:true, enabled:true).save()

		// Customer Statuses
		new CustomerStatus(name:"Prospective", statusCode:"PC", visible:true, enabled:true).save()
		def custStatus = new CustomerStatus(name:"Customer", statusCode:"CC", visible:true, enabled:true).save()
		new CustomerStatus(name:"Ex-Customer", statusCode:"EC", visible:true, enabled:true).save()

		// Non-Vehicle type assets
		new NonVehicleType(name:"Laptop Computer", typeCode:"LPT", visible:true, enabled:true).save()
		new NonVehicleType(name:"Gym Membership", typeCode:"GYM", visible:true, enabled:true).save()
		new NonVehicleType(name:"Superannuation Contribution", typeCode:"SPR", visible:true, enabled:true).save()

		// Budget types
		new BudgetType(name:"Novated", typeCode:"BDG_NOV", visible:true, enabled:true).save()
		new BudgetType(name:"Fleet",   typeCode:"BDG_FLT", visible:true, enabled:true).save()
		new BudgetType(name:"Managed", typeCode:"BDG_MNG", visible:true, enabled:true).save()

		// Budget Statuses
		new BudgetStatus(name:"Quotation", statusCode:"BDG_QUOT", visible:true, enabled:true).save()
		new BudgetStatus(name:"Activated", statusCode:"BDG_LIVE", visible:true, enabled:true).save()
		new BudgetStatus(name:"Suspended", statusCode:"BDG_SUSP", visible:true, enabled:true).save()
		new BudgetStatus(name:"Finalized", statusCode:"BDG_FINL", visible:true, enabled:true).save()

		// Rego types
		new RegistrationUsageType(name:"Private",  typeCode:"PRIV", visible:true, enabled:true).save()
		new RegistrationUsageType(name:"Business", typeCode:"BUS",  visible:true, enabled:true).save()

		// Fuel types
		def ulpType = new FuelType(name:"Unleaded Petrol", abbreviation:"ULP", visible:true, enabled:true, economyFactor:1.0, ron:91).save()
		def pulpType = new FuelType(name:"Premium Unleaded Petrol", abbreviation:"PULP", visible:true, enabled:true, economyFactor:1.0, ron:95).save()
		def ron98Type = new FuelType(name:"Premium Unleaded Petrol 98RON", abbreviation:"PULP98", visible:true, enabled:true, economyFactor:1.0, ron:98).save()
		def dieselType = new FuelType(name:"Diesel", abbreviation:"DIESEL", visible:true, enabled:true, economyFactor:1.0, ron:null).save()
		def lpgType = new FuelType(name:"LPG", abbreviation:"LPG", visible:true, enabled:true, economyFactor:1.35, ron:null).save()

		// States
		new AustralianState(name:"Australian Capital Territory", abbreviation:"ACT", capitalCity:"Canberra").save()
		def nsw = new AustralianState(name:"New South Wales", abbreviation:"NSW", capitalCity:"Sydney").save()
		new AustralianState(name:"Victoria", abbreviation:"VIC", capitalCity:"Melbourne").save()
		new AustralianState(name:"Queensland", abbreviation:"QLD", capitalCity:"Brisbane").save()
		new AustralianState(name:"South Australia", abbreviation:"SA", capitalCity:"Adelaide").save()
		new AustralianState(name:"Tasmania", abbreviation:"TAS", capitalCity:"Hobart").save()
		new AustralianState(name:"Western Australia", abbreviation:"WA", capitalCity:"Perth").save()
		new AustralianState(name:"Northern Territory", abbreviation:"NT", capitalCity:"Darwin").save()

		// Vehicle Makes
		def mazda  = new VehicleManufacturer(name:"Mazda").save()
		def ford   = new VehicleManufacturer(name:"Ford").save()
		def holden = new VehicleManufacturer(name:"Holden").save()

		// vehicle Models
		def mazda_3   = new VehicleModel(name:"3", make:mazda, firstReleaseDate:new Date(), vehicleClass:"Passenger")
		def mazda_6   = new VehicleModel(name:"6", make:mazda, firstReleaseDate:new Date(), vehicleClass:"Passenger")
		def mazda_cx7 = new VehicleModel(name:"CX-7", make:mazda, firstReleaseDate:new Date(), vehicleClass:"Passenger")
		mazda.addToModels(mazda_3)
		mazda.addToModels(mazda_6)
		mazda.addToModels(mazda_cx7)
		mazda.save()

		// Make/Models
		def mz3_v = new MakeModel(description:"Mazda 3 MPS Turbo Manual Hatch", model:mazda_3, variant:"MPS Turbo", buildDate:new Date(), shape:"Hatch", transmission:"Manual", redbookCode:"RDB001", glassCode:"GG001", retailPrice:36950.0, tyreSize:"225/40R18", fuelType:pulpType)
		mazda_3.addToVariants(mz3_v)
		mz3_v = new MakeModel(description:"Mazda 3 MPS Luxury Turbo Manual Hatch", model:mazda_3, variant:"MPS Turbo Luxury", buildDate:new Date(), shape:"Hatch", transmission:"Manual", redbookCode:"RDB002", glassCode:"GG002", retailPrice:39950.0, tyreSize:"225/40R18", fuelType:pulpType)
		mazda_3.addToVariants(mz3_v)

		/*mz3_v = new MakeModel(model:mazda_3, variant:"MPS Luxury Turbo", buildDate:new Date(), transmission:"Automatic", redbookCode:"RDB002", glassCode:"GG002",
			retailPrice:39354.55, engineCapacity:2.3, cylinders:4, doors:4, shape:"Hatch", fuelConsumptionCombined:9.9, tyreSize:"225/40R18", fuelType:pulpType)
		mazda_3.addToVariants(mz3_v)
		mz3_v = new MakeModel(model:mazda_3, variant:"SP25", buildDate:new Date(), shape:"Sedan", transmission:"Automatic", redbookCode:"RDB003", glassCode:"GG003", retailPrice:32950.0, tyreSize:"205/50R17", fuelType:pulpType)
		mazda_3.addToVariants(mz3_v)
		mz3_v = new MakeModel(model:mazda_3, variant:"SP25 Luxury", buildDate:new Date(), shape:"Sedan", transmission:"Automatic", redbookCode:"RDB004", glassCode:"GG004", retailPrice:35950.0, tyreSize:"205/50R17", fuelType:pulpType)
		mazda_3.addToVariants(mz3_v)*/
		mazda_3.save()

		/*def mzcx7_v = new MakeModel(model:mazda_cx7, variant:"Luxury Sports ER Series 2", buildDate:new Date(),
			transmission:"Automatic", redbookCode:"RDB005", glassCode:"GG005", retailPrice:45990.0, tyreSize:"235/55R19",
			engineCapacity:2.3, cylinders:4, doors:5, shape:"Wagon", insuranceClass:CompInsuranceParameter.CLASS_4WD,
			fuelConsumptionCombined:11.5, fuelType:pulpType
		)
		mazda_cx7.addToVariants(mzcx7_v)
		mazda_cx7.save()*/

		//==================================== Dummy Data here! ====================================//

		def individual = new Individual(name:"Campbell, Neil", firstName:"Neil", middleName:"", lastName:"Campbell", preferredName:"Neil Campbell")
		individual.addToAddresses(new Address(addressType:streetAddrType, street:"19 Applecross Ave", suburb:"Castle Hill", state:nsw, postcode:"2000"))
		individual.addToPhones(new Phone(phoneType:mobPh, number:"0412 345 678"))
		individual.addToPhones(new Phone(phoneType:wkPh, number:"1300 787 785"))
		individual.addToEmails(new Email(defaultType:"To", address:"neilc@innovated.com.au"))
		individual.save()

		def company = new Company(name:"inNovated Leasing Australia Pty Ltd", tradingName:"inNovated Leasing", abn:"53 004 085 616", acn:"004 999 982", parent:null, employerCode:"INNOV01XYZ")
		def contact = new CompanyContact(name:"Neil Campbell", role:"Director")
		contact.addToPhones(new Phone(phoneType:mobPh, number:"0412 345 678"))
		company.addToContacts(contact)
		company.addToPhones(new Phone(phoneType:wkPh, number:"1300 787 785"))
		company.addToPhones(new Phone(phoneType:wkFax, number:"1300 667 131"))
		company.addToEmails(new Email(defaultType:"To", address:"accounts@innovated.com.au"))
		company.addToAddresses(new Address(addressType:postalAddrType, street:"PO Box 1150", suburb:"Castle Hill", state:nsw, postcode:"1765"))
		company.addToAllowedFbtCalcMethods(TaxableValueType.STATUTORY)
		company.addToAllowedFbtCalcMethods(TaxableValueType.OP_COST)
		company.addToConcessions(new CompanyFbtConcession(dateFrom:asAtDate, type:FbtConcessionType.NO_CONCESSION))
		company.save(flush:true)

		def nswHealth = new Company(name:"NSW Health Department", tradingName:"NSW Health", abn:"abn123", acn:"acn123", parent:null, employerCode:"NSWHEALTH02XYZ")

		def sesiahs = new Company(name:"South Eastern Sydney & Illawarra Area Health Service", employerCode:"SESIAHS03XYZ",
				tradingName:"SESIAHS", abn:"abn123", acn:"acn123", defaultPayFreq:PayrollFrequency.FORTNIGHTLY,
				employerShareFee:110.0, employerShareRate:50.0, allowsFbtEcm:false, canClaimGst:false)
		sesiahs.addToConcessions(new CompanyFbtConcession(dateFrom:asAtDate, type:FbtConcessionType.EXEMPT_17K_CAP))
		sesiahs.addToAllowedFbtCalcMethods(TaxableValueType.STATUTORY)

		def sswahs = new Company(name:"Sydney South West Area Health Service", tradingName:"SSWAHS", employerCode:"SSWAHS04XYZ",
				abn:"abn123", acn:"acn123", defaultPayFreq:PayrollFrequency.FORTNIGHTLY, employerShareFee:110.0,
				employerShareRate:50.0, allowsFbtEcm:false, canClaimGst:false)
		sswahs.addToConcessions(new CompanyFbtConcession(dateFrom:asAtDate, type:FbtConcessionType.EXEMPT_17K_CAP))
		sswahs.addToAllowedFbtCalcMethods(TaxableValueType.STATUTORY)

		def asnsw = new Company(name:"Ambulance Service of NSW", tradingName:"ASNSW", employerCode:"AMBO123",
				abn:"abn123", acn:"acn123", defaultPayFreq:PayrollFrequency.FORTNIGHTLY, employerShareFee:110.0,
				employerShareRate:50.0, allowsFbtEcm:false, canClaimGst:false)
		asnsw.addToConcessions(new CompanyFbtConcession(dateFrom:asAtDate, type:FbtConcessionType.EXEMPT_17K_CAP))
		asnsw.addToAllowedFbtCalcMethods(TaxableValueType.STATUTORY)

		nswHealth.addToChildren(sesiahs)
		nswHealth.addToChildren(sswahs)
		nswHealth.addToChildren(asnsw)
		nswHealth.addToConcessions(new CompanyFbtConcession(dateFrom:asAtDate, type:FbtConcessionType.NO_CONCESSION))
		nswHealth.save()

		def sesiahsCust = new Customer(code:"SESIAHS001", entity:sesiahs, type:custType, status:custStatus, joined:new Date()).save()
		def sswahsCust = new Customer(code:"SSWAHS001", entity:sswahs, type:custType, status:custStatus, joined:new Date()).save()
		def asnswCust = new Customer(code:"CASNSW001", entity:asnsw, type:custType, status:custStatus, joined:new Date()).save()

		def employee = new Customer(code:"NC001", entity:individual, type:custType, status:custStatus, joined:new Date()).save()
		def employer = new Customer(code:"INNOV001", entity:company, type:custType, status:custStatus, joined:new Date()).save()

		/*normalUser.customer = employee
		normalUser.save()*/

		def user = User.findByUsername('user')
		user.customer = employee
		user.save()

		user = User.findByUsername('employer')
		user.customer = employer
		user.save()

		def job = new Employment(employer:company, employee:individual, jobTitle:"Director", startedEmployment:new Date(), grossSalary:80000.0D).save()
		def job2 = new Employment(employer:sesiahs, employee:individual, jobTitle:"Director", startedEmployment:new Date(), grossSalary:80000.0D).save()
		def job3 = new Employment(employer:asnsw, employee:individual, jobTitle:"Director", startedEmployment:new Date(), grossSalary:80000.0D).save()

		//println "<< JOB >>: " + job.toString()

		// add a default finance provider
		def eCode = RandomStringUtils.randomAlphabetic(6);
		def macLease = new Company(name:"Macquarie Leasing Pty Ltd", tradingName:"Macquarie Leasing", abn:"123456789123", acn:"123456789", parent:null, employerCode:eCode/*"MACQ01XYZ"*/).save()
		def macqlSupp = new Supplier(code:"MACLEASE", customerLinkCode:"", entity:macLease, abn:"123456789", type:suppType, joined:new Date(), ceased:null).save()

		// add a default rego provider (NSW)
		def rta = new Company(name:"NSW Roads & Traffic Authority", tradingName:"Roads & Traffic Authority", abn:"123456789123", acn:"123456789", parent:null, employerCode:"RTA01XYZ").save()
		rta.addToConcessions(new CompanyFbtConcession(dateFrom:asAtDate, type:FbtConcessionType.NO_CONCESSION))
		def rtaSupp = new Supplier(code:"RTA001", customerLinkCode:"RTA001", entity:rta, abn:"123456789", type:regoSupp, joined:new Date(), ceased:null).save()

		// Roadside suppliers
		def suppNrma = new Supplier(code:"NRMA001", customerLinkCode:"NRMA001", entity:rta, abn:"123456789", type:roadsideSupp, joined:new Date(), ceased:null).save()
		def suppRaa = new Supplier(code:"RAA001", customerLinkCode:"RAA001", entity:rta, abn:"123456789", type:roadsideSupp, joined:new Date(), ceased:null).save()
		def suppRacq = new Supplier(code:"RACQ001", customerLinkCode:"RACQ001", entity:rta, abn:"123456789", type:roadsideSupp, joined:new Date(), ceased:null).save()
		def suppRact = new Supplier(code:"RACT001", customerLinkCode:"RACT001", entity:rta, abn:"123456789", type:roadsideSupp, joined:new Date(), ceased:null).save()
		def suppRacv = new Supplier(code:"RACV001", customerLinkCode:"RACV001", entity:rta, abn:"123456789", type:roadsideSupp, joined:new Date(), ceased:null).save()
		def suppRacwa = new Supplier(code:"RACWA001", customerLinkCode:"RACWA001", entity:rta, abn:"123456789", type:roadsideSupp, joined:new Date(), ceased:null).save()

		// Fee's & Brokerage Parameters
		new FeesBrokerageParameter(dateFrom:asAtDate, customerCode:Customer.BASE_CUST_CODE, brokeragePercent:3.4, annualFeeFltMng:84.0, annualFeeFbtRep:36.0, annualFeeAdmin:396.0, annualFeeBankChg:36.0).save()
		new FeesBrokerageParameter(dateFrom:asAtDate, customerCode:"SESIAHS001", brokeragePercent:2.5, annualFeeFltMng:60.06, annualFeeFbtRep:0.0, annualFeeAdmin:300.04, annualFeeBankChg:36.14).save()
		new FeesBrokerageParameter(dateFrom:asAtDate, customerCode:"CASNSW001", brokeragePercent:3.0, annualFeeFltMng:60.06, annualFeeFbtRep:0.0, annualFeeAdmin:300.04, annualFeeBankChg:36.14).save()

		new FinancierParameter(dateFrom:asAtDate, customerCode:Customer.BASE_CUST_CODE, defaultSupplier:macqlSupp, interestRate:0.098, documentationFee:250.0, paymentsMadeInAdvance:false).save()

		new VehicleStampDutyParameter(dateFrom:asAtDate, customerCode:Customer.BASE_CUST_CODE, state:"NSW", lowValue:0.03, highValue:0.05, thresholdValue:45000).save()
		new VehicleStampDutyParameter(dateFrom:asAtDate, customerCode:Customer.BASE_CUST_CODE, state:"ACT", lowValue:0.03).save()
		new VehicleStampDutyParameter(dateFrom:asAtDate, customerCode:Customer.BASE_CUST_CODE, state:"VIC", lowValue:0.025).save()
		new VehicleStampDutyParameter(dateFrom:asAtDate, customerCode:Customer.BASE_CUST_CODE, state:"QLD", lowValue:0.02).save()
		new VehicleStampDutyParameter(dateFrom:asAtDate, customerCode:Customer.BASE_CUST_CODE, state:"TAS", lowValue:0.03).save()
		new VehicleStampDutyParameter(dateFrom:asAtDate, customerCode:Customer.BASE_CUST_CODE, state:"WA", lowValue:0.03).save()
		new VehicleStampDutyParameter(dateFrom:asAtDate, customerCode:Customer.BASE_CUST_CODE, state:"SA", lowValue:0.038).save()
		new VehicleStampDutyParameter(dateFrom:asAtDate, customerCode:Customer.BASE_CUST_CODE, state:"NT", lowValue:0.03).save()

		// residual values for quotes
		new ResidualValueParameter(dateFrom:asAtDate, customerCode:Customer.BASE_CUST_CODE, term:6,  min:0.70, minHiKm:0.70, max:0.75).save()
		new ResidualValueParameter(dateFrom:asAtDate, customerCode:Customer.BASE_CUST_CODE, term:12, min:0.65, minHiKm:0.60, max:0.70).save()
		new ResidualValueParameter(dateFrom:asAtDate, customerCode:Customer.BASE_CUST_CODE, term:18, min:0.60, minHiKm:0.60, max:0.65).save()
		new ResidualValueParameter(dateFrom:asAtDate, customerCode:Customer.BASE_CUST_CODE, term:24, min:0.55, minHiKm:0.50, max:0.60).save()
		new ResidualValueParameter(dateFrom:asAtDate, customerCode:Customer.BASE_CUST_CODE, term:30, min:0.50, minHiKm:0.50, max:0.55).save()
		new ResidualValueParameter(dateFrom:asAtDate, customerCode:Customer.BASE_CUST_CODE, term:36, min:0.45, minHiKm:0.30, max:0.50).save()
		new ResidualValueParameter(dateFrom:asAtDate, customerCode:Customer.BASE_CUST_CODE, term:42, min:0.40, minHiKm:0.25, max:0.45).save()
		new ResidualValueParameter(dateFrom:asAtDate, customerCode:Customer.BASE_CUST_CODE, term:48, min:0.35, minHiKm:0.20, max:0.45).save()
		new ResidualValueParameter(dateFrom:asAtDate, customerCode:Customer.BASE_CUST_CODE, term:54, min:0.30, minHiKm:0.15, max:0.35).save()
		new ResidualValueParameter(dateFrom:asAtDate, customerCode:Customer.BASE_CUST_CODE, term:60, min:0.25, minHiKm:0.15, max:0.35).save()
		// SESIAHS RV's
		new ResidualValueParameter(dateFrom:asAtDate, customerCode:"SESIAHS001", term:6,  min:0.7000, minHiKm:0.70, max:0.75).save()
		new ResidualValueParameter(dateFrom:asAtDate, customerCode:"SESIAHS001", term:12, min:0.6563, minHiKm:0.60, max:0.70).save()
		new ResidualValueParameter(dateFrom:asAtDate, customerCode:"SESIAHS001", term:18, min:0.6000, minHiKm:0.60, max:0.65).save()
		new ResidualValueParameter(dateFrom:asAtDate, customerCode:"SESIAHS001", term:24, min:0.5625, minHiKm:0.50, max:0.60).save()
		new ResidualValueParameter(dateFrom:asAtDate, customerCode:"SESIAHS001", term:30, min:0.5000, minHiKm:0.50, max:0.55).save()
		new ResidualValueParameter(dateFrom:asAtDate, customerCode:"SESIAHS001", term:36, min:0.4688, minHiKm:0.30, max:0.50).save()
		new ResidualValueParameter(dateFrom:asAtDate, customerCode:"SESIAHS001", term:42, min:0.4000, minHiKm:0.25, max:0.45).save()
		new ResidualValueParameter(dateFrom:asAtDate, customerCode:"SESIAHS001", term:48, min:0.3750, minHiKm:0.20, max:0.45).save()
		new ResidualValueParameter(dateFrom:asAtDate, customerCode:"SESIAHS001", term:54, min:0.3000, minHiKm:0.15, max:0.35).save()
		new ResidualValueParameter(dateFrom:asAtDate, customerCode:"SESIAHS001", term:60, min:0.2813, minHiKm:0.15, max:0.35).save()

		new LuxuryCarTaxParameter(dateFrom:new GregorianCalendar(2000, Calendar.JULY, 1).time, customerCode:Customer.BASE_CUST_CODE, rate:0.25, threshold:57180.0, fuelConsumption:7.0, fuelEfficientRate:0.25, fuelEfficientThreshold:75000.0, creditThreshold:3000.0).save()
		new LuxuryCarTaxParameter(dateFrom:new GregorianCalendar(2008, Calendar.JULY, 1).time, customerCode:Customer.BASE_CUST_CODE, rate:0.33, threshold:57180.0, fuelConsumption:7.0, fuelEfficientRate:0.33, fuelEfficientThreshold:75000.0, creditThreshold:3000.0).save()
		new LuxuryCarTaxParameter(dateFrom:new GregorianCalendar(2008, Calendar.OCTOBER, 3).time, customerCode:Customer.BASE_CUST_CODE, rate:0.33, threshold:57180.0, fuelConsumption:7.0, fuelEfficientRate:0.33, fuelEfficientThreshold:75000.0, creditThreshold:3000.0).save()

		new VehicleRegistrationParameter(dateFrom:asAtDate, customerCode:Customer.BASE_CUST_CODE, state:"NSW", regoSupplier:rtaSupp, ctpSupplier:rtaSupp, regoCostTwelveMths:396.0, regoCostSixMths:192.0, regoIncludesGst:false, ctpCostTwelveMths:405.0, ctpCostSixMths:202.5).save()
		new VehicleRegistrationParameter(dateFrom:asAtDate, customerCode:Customer.BASE_CUST_CODE, state:"ACT", regoSupplier:rtaSupp, ctpSupplier:rtaSupp, regoCostTwelveMths:384.0, regoCostSixMths:192.0, regoIncludesGst:false, ctpCostTwelveMths:395.0, ctpCostSixMths:197.5).save()
		new VehicleRegistrationParameter(dateFrom:asAtDate, customerCode:Customer.BASE_CUST_CODE, state:"VIC", regoSupplier:rtaSupp, ctpSupplier:rtaSupp, regoCostTwelveMths:184.0, regoCostSixMths: 92.0, regoIncludesGst:false, ctpCostTwelveMths:429.0, ctpCostSixMths:214.5).save()
		new VehicleRegistrationParameter(dateFrom:asAtDate, customerCode:Customer.BASE_CUST_CODE, state:"QLD", regoSupplier:rtaSupp, ctpSupplier:rtaSupp, regoCostTwelveMths:395.0, regoCostSixMths:197.5, regoIncludesGst:false, ctpCostTwelveMths:380.0, ctpCostSixMths:190.0).save()
		new VehicleRegistrationParameter(dateFrom:asAtDate, customerCode:Customer.BASE_CUST_CODE, state:"TAS", regoSupplier:rtaSupp, ctpSupplier:rtaSupp, regoCostTwelveMths:230.0, regoCostSixMths:115.0, regoIncludesGst:false, ctpCostTwelveMths:338.0, ctpCostSixMths:169.0).save()
		new VehicleRegistrationParameter(dateFrom:asAtDate, customerCode:Customer.BASE_CUST_CODE, state:"WA",  regoSupplier:rtaSupp, ctpSupplier:rtaSupp, regoCostTwelveMths:276.0, regoCostSixMths:138.0, regoIncludesGst:false, ctpCostTwelveMths:245.0, ctpCostSixMths:122.5).save()
		new VehicleRegistrationParameter(dateFrom:asAtDate, customerCode:Customer.BASE_CUST_CODE, state:"SA",  regoSupplier:rtaSupp, ctpSupplier:rtaSupp, regoCostTwelveMths:295.0, regoCostSixMths:147.5, regoIncludesGst:false, ctpCostTwelveMths:420.0, ctpCostSixMths:210.0).save()
		new VehicleRegistrationParameter(dateFrom:asAtDate, customerCode:Customer.BASE_CUST_CODE, state:"NT",  regoSupplier:rtaSupp, ctpSupplier:rtaSupp, regoCostTwelveMths:201.0, regoCostSixMths:100.5, regoIncludesGst:false, ctpCostTwelveMths:465.0, ctpCostSixMths:232.5).save()

		new RoadsideParameter(dateFrom:asAtDate, customerCode:Customer.BASE_CUST_CODE, state:"NSW", defaultSupplier:suppNrma, joiningFee:49.0, annualFee:81.6).save()
		new RoadsideParameter(dateFrom:asAtDate, customerCode:Customer.BASE_CUST_CODE, state:"ACT", defaultSupplier:suppNrma, joiningFee:49.0, annualFee:81.6).save()
		new RoadsideParameter(dateFrom:asAtDate, customerCode:Customer.BASE_CUST_CODE, state:"VIC", defaultSupplier:suppRacv, joiningFee:37.0, annualFee:64.0).save()
		new RoadsideParameter(dateFrom:asAtDate, customerCode:Customer.BASE_CUST_CODE, state:"QLD", defaultSupplier:suppRacq, joiningFee:33.0, annualFee:58.0).save()
		new RoadsideParameter(dateFrom:asAtDate, customerCode:Customer.BASE_CUST_CODE, state:"TAS", defaultSupplier:suppRact, joiningFee: 0.0, annualFee:72.0).save()
		new RoadsideParameter(dateFrom:asAtDate, customerCode:Customer.BASE_CUST_CODE, state:"WA",  defaultSupplier:suppRacwa, joiningFee:34.0, annualFee:62.0).save()
		new RoadsideParameter(dateFrom:asAtDate, customerCode:Customer.BASE_CUST_CODE, state:"SA",  defaultSupplier:suppRaa, joiningFee:30.0, annualFee:65.0).save()
		new RoadsideParameter(dateFrom:asAtDate, customerCode:Customer.BASE_CUST_CODE, state:"NT",  defaultSupplier:suppRaa, joiningFee:30.0, annualFee:69.0).save()

		//new MaintenanceParameter(dateFrom:asAtDate, make:mazda,  model:null,    variant:null,  over0km:32.5, over20000km:35.0, over30000km:37.5, over40000km:40.0).save()
		//new MaintenanceParameter(dateFrom:asAtDate, make:mazda,  model:mazda_3, variant:null,  over0km:32.5, over20000km:35.0, over30000km:37.5, over40000km:40.0).save()
		//new MaintenanceParameter(dateFrom:asAtDate, make:mazda,  model:mazda_3, variant:mz3_v, over0km:32.5, over20000km:35.0, over30000km:37.5, over40000km:40.0).save()

		new CompInsuranceParameter(dateFrom:asAtDate, customerCode:Customer.BASE_CUST_CODE, state:"NSW", defaultSupplier:suppRaa, type:CompInsuranceParameter.CLASS_4CYL, metro: 989.45, regional: 838.15, excess:400.0, under25Metro:1266.87, under25Regional:1154.18, under25Excess:1000.0).save()
		new CompInsuranceParameter(dateFrom:asAtDate, customerCode:Customer.BASE_CUST_CODE, state:"NSW", defaultSupplier:suppRaa, type:CompInsuranceParameter.CLASS_6CYL, metro:1082.07, regional: 913.96, excess:400.0, under25Metro:1266.87, under25Regional:1154.18, under25Excess:1000.0).save()
		new CompInsuranceParameter(dateFrom:asAtDate, customerCode:Customer.BASE_CUST_CODE, state:"NSW", defaultSupplier:suppRaa, type:CompInsuranceParameter.CLASS_4WD,  metro:1313.78, regional:1054.36, excess:500.0, under25Metro:1699.31, under25Regional:1353.82, under25Excess:1100.0).save()
		new CompInsuranceParameter(dateFrom:asAtDate, customerCode:Customer.BASE_CUST_CODE, state:"NSW", defaultSupplier:suppRaa, type:CompInsuranceParameter.CLASS_V8PR, metro:1544.40, regional:1256.18, excess:500.0, under25Metro:1801.31, under25Regional:1455.82, under25Excess:1100.0).save()

		// Fuel parameters
		new FuelParameter(dateFrom:asAtDate, customerCode:Customer.BASE_CUST_CODE, state:"NSW", type:lpgType,    dollarsPerLitre:0.55, dollarsPerLitreRegional:1.289, defaultSupplier:suppRaa).save()
		new FuelParameter(dateFrom:asAtDate, customerCode:Customer.BASE_CUST_CODE, state:"NSW", type:dieselType, dollarsPerLitre:1.25, dollarsPerLitreRegional:1.289, defaultSupplier:suppRaa).save()
		new FuelParameter(dateFrom:asAtDate, customerCode:Customer.BASE_CUST_CODE, state:"NSW", type:ulpType,    dollarsPerLitre:1.20, dollarsPerLitreRegional:1.289, defaultSupplier:suppRaa).save()
		new FuelParameter(dateFrom:asAtDate, customerCode:Customer.BASE_CUST_CODE, state:"NSW", type:pulpType,   dollarsPerLitre:1.30, dollarsPerLitreRegional:1.289, defaultSupplier:suppRaa).save()
		new FuelParameter(dateFrom:asAtDate, customerCode:Customer.BASE_CUST_CODE, state:"NSW", type:ron98Type,  dollarsPerLitre:1.35, dollarsPerLitreRegional:1.289, defaultSupplier:suppRaa).save()
		new FuelParameter(dateFrom:asAtDate, customerCode:Customer.BASE_CUST_CODE, state:"ACT", type:lpgType,    dollarsPerLitre:0.60, dollarsPerLitreRegional:1.289, defaultSupplier:suppRaa).save()
		new FuelParameter(dateFrom:asAtDate, customerCode:Customer.BASE_CUST_CODE, state:"ACT", type:dieselType, dollarsPerLitre:1.32, dollarsPerLitreRegional:1.289, defaultSupplier:suppRaa).save()
		new FuelParameter(dateFrom:asAtDate, customerCode:Customer.BASE_CUST_CODE, state:"ACT", type:ulpType,    dollarsPerLitre:1.27, dollarsPerLitreRegional:1.289, defaultSupplier:suppRaa).save()
		new FuelParameter(dateFrom:asAtDate, customerCode:Customer.BASE_CUST_CODE, state:"ACT", type:pulpType,   dollarsPerLitre:1.35, dollarsPerLitreRegional:1.289, defaultSupplier:suppRaa).save()
		new FuelParameter(dateFrom:asAtDate, customerCode:Customer.BASE_CUST_CODE, state:"ACT", type:ron98Type,  dollarsPerLitre:1.40, dollarsPerLitreRegional:1.289, defaultSupplier:suppRaa).save()
		new FuelParameter(dateFrom:asAtDate, customerCode:Customer.BASE_CUST_CODE, state:"NT",  type:lpgType,    dollarsPerLitre:0.95, dollarsPerLitreRegional:1.289, defaultSupplier:suppRaa).save()
		new FuelParameter(dateFrom:asAtDate, customerCode:Customer.BASE_CUST_CODE, state:"NT",  type:dieselType, dollarsPerLitre:1.35, dollarsPerLitreRegional:1.289, defaultSupplier:suppRaa).save()
		new FuelParameter(dateFrom:asAtDate, customerCode:Customer.BASE_CUST_CODE, state:"NT",  type:ulpType,    dollarsPerLitre:1.30, dollarsPerLitreRegional:1.289, defaultSupplier:suppRaa).save()
		new FuelParameter(dateFrom:asAtDate, customerCode:Customer.BASE_CUST_CODE, state:"NT",  type:pulpType,   dollarsPerLitre:1.40, dollarsPerLitreRegional:1.289, defaultSupplier:suppRaa).save()
		new FuelParameter(dateFrom:asAtDate, customerCode:Customer.BASE_CUST_CODE, state:"NT",  type:ron98Type,  dollarsPerLitre:1.45, dollarsPerLitreRegional:1.289, defaultSupplier:suppRaa).save()
		new FuelParameter(dateFrom:asAtDate, customerCode:Customer.BASE_CUST_CODE, state:"QLD", type:lpgType,    dollarsPerLitre:0.60, dollarsPerLitreRegional:1.289, defaultSupplier:suppRaa).save()
		new FuelParameter(dateFrom:asAtDate, customerCode:Customer.BASE_CUST_CODE, state:"QLD", type:dieselType, dollarsPerLitre:1.28, dollarsPerLitreRegional:1.289, defaultSupplier:suppRaa).save()
		new FuelParameter(dateFrom:asAtDate, customerCode:Customer.BASE_CUST_CODE, state:"QLD", type:ulpType,    dollarsPerLitre:1.23, dollarsPerLitreRegional:1.289, defaultSupplier:suppRaa).save()
		new FuelParameter(dateFrom:asAtDate, customerCode:Customer.BASE_CUST_CODE, state:"QLD", type:pulpType,   dollarsPerLitre:1.33, dollarsPerLitreRegional:1.289, defaultSupplier:suppRaa).save()
		new FuelParameter(dateFrom:asAtDate, customerCode:Customer.BASE_CUST_CODE, state:"QLD", type:ron98Type,  dollarsPerLitre:1.38, dollarsPerLitreRegional:1.289, defaultSupplier:suppRaa).save()
		new FuelParameter(dateFrom:asAtDate, customerCode:Customer.BASE_CUST_CODE, state:"SA",  type:lpgType,    dollarsPerLitre:0.60, dollarsPerLitreRegional:1.289, defaultSupplier:suppRaa).save()
		new FuelParameter(dateFrom:asAtDate, customerCode:Customer.BASE_CUST_CODE, state:"SA",  type:dieselType, dollarsPerLitre:1.25, dollarsPerLitreRegional:1.289, defaultSupplier:suppRaa).save()
		new FuelParameter(dateFrom:asAtDate, customerCode:Customer.BASE_CUST_CODE, state:"SA",  type:ulpType,    dollarsPerLitre:1.20, dollarsPerLitreRegional:1.289, defaultSupplier:suppRaa).save()
		new FuelParameter(dateFrom:asAtDate, customerCode:Customer.BASE_CUST_CODE, state:"SA",  type:pulpType,   dollarsPerLitre:1.30, dollarsPerLitreRegional:1.289, defaultSupplier:suppRaa).save()
		new FuelParameter(dateFrom:asAtDate, customerCode:Customer.BASE_CUST_CODE, state:"SA",  type:ron98Type,  dollarsPerLitre:1.35, dollarsPerLitreRegional:1.289, defaultSupplier:suppRaa).save()
		new FuelParameter(dateFrom:asAtDate, customerCode:Customer.BASE_CUST_CODE, state:"TAS", type:lpgType,    dollarsPerLitre:0.67, dollarsPerLitreRegional:1.289, defaultSupplier:suppRaa).save()
		new FuelParameter(dateFrom:asAtDate, customerCode:Customer.BASE_CUST_CODE, state:"TAS", type:dieselType, dollarsPerLitre:1.32, dollarsPerLitreRegional:1.289, defaultSupplier:suppRaa).save()
		new FuelParameter(dateFrom:asAtDate, customerCode:Customer.BASE_CUST_CODE, state:"TAS", type:ulpType,    dollarsPerLitre:1.27, dollarsPerLitreRegional:1.289, defaultSupplier:suppRaa).save()
		new FuelParameter(dateFrom:asAtDate, customerCode:Customer.BASE_CUST_CODE, state:"TAS", type:pulpType,   dollarsPerLitre:1.35, dollarsPerLitreRegional:1.289, defaultSupplier:suppRaa).save()
		new FuelParameter(dateFrom:asAtDate, customerCode:Customer.BASE_CUST_CODE, state:"TAS", type:ron98Type,  dollarsPerLitre:1.40, dollarsPerLitreRegional:1.289, defaultSupplier:suppRaa).save()
		new FuelParameter(dateFrom:asAtDate, customerCode:Customer.BASE_CUST_CODE, state:"VIC", type:lpgType,    dollarsPerLitre:0.50, dollarsPerLitreRegional:1.289, defaultSupplier:suppRaa).save()
		new FuelParameter(dateFrom:asAtDate, customerCode:Customer.BASE_CUST_CODE, state:"VIC", type:dieselType, dollarsPerLitre:1.25, dollarsPerLitreRegional:1.289, defaultSupplier:suppRaa).save()
		new FuelParameter(dateFrom:asAtDate, customerCode:Customer.BASE_CUST_CODE, state:"VIC", type:ulpType,    dollarsPerLitre:1.20, dollarsPerLitreRegional:1.289, defaultSupplier:suppRaa).save()
		new FuelParameter(dateFrom:asAtDate, customerCode:Customer.BASE_CUST_CODE, state:"VIC", type:pulpType,   dollarsPerLitre:1.30, dollarsPerLitreRegional:1.289, defaultSupplier:suppRaa).save()
		new FuelParameter(dateFrom:asAtDate, customerCode:Customer.BASE_CUST_CODE, state:"VIC", type:ron98Type,  dollarsPerLitre:1.35, dollarsPerLitreRegional:1.289, defaultSupplier:suppRaa).save()
		new FuelParameter(dateFrom:asAtDate, customerCode:Customer.BASE_CUST_CODE, state:"WA",  type:lpgType,    dollarsPerLitre:0.57, dollarsPerLitreRegional:1.289, defaultSupplier:suppRaa).save()
		new FuelParameter(dateFrom:asAtDate, customerCode:Customer.BASE_CUST_CODE, state:"WA",  type:dieselType, dollarsPerLitre:1.29, dollarsPerLitreRegional:1.289, defaultSupplier:suppRaa).save()
		new FuelParameter(dateFrom:asAtDate, customerCode:Customer.BASE_CUST_CODE, state:"WA",  type:ulpType,    dollarsPerLitre:1.24, dollarsPerLitreRegional:1.289, defaultSupplier:suppRaa).save()
		new FuelParameter(dateFrom:asAtDate, customerCode:Customer.BASE_CUST_CODE, state:"WA",  type:pulpType,   dollarsPerLitre:1.34, dollarsPerLitreRegional:1.289, defaultSupplier:suppRaa).save()
		new FuelParameter(dateFrom:asAtDate, customerCode:Customer.BASE_CUST_CODE, state:"WA",  type:ron98Type,  dollarsPerLitre:1.39, dollarsPerLitreRegional:1.289, defaultSupplier:suppRaa).save()

		// FBT related parameters
		def firstJan1990       = new GregorianCalendar(1990, Calendar.JANUARY, 1).time
		def ninthMay2011       = new GregorianCalendar(2011, Calendar.MAY,     9).time
		def tenthMay2011       = new GregorianCalendar(2011, Calendar.MAY,    10).time
		def thirtyFirstMar2012 = new GregorianCalendar(2012, Calendar.MARCH,  31).time
		def firstApr2012       = new GregorianCalendar(2012, Calendar.APRIL,   1).time
		def thirtyFirstMar2013 = new GregorianCalendar(2013, Calendar.MARCH,  31).time
		def firstApr2013       = new GregorianCalendar(2013, Calendar.APRIL,   1).time
		def thirtyFirstMar2014 = new GregorianCalendar(2014, Calendar.MARCH,  31).time
		def firstApr2014       = new GregorianCalendar(2014, Calendar.APRIL,   1).time
		def thirtyFirstMar2999 = new GregorianCalendar(2999, Calendar.MARCH,  31).time

		new FbtStatutoryMethodRateParameter(dateFrom:firstJan1990, commenceStart:firstJan1990, commenceEnd:ninthMay2011, customerCode:Customer.BASE_CUST_CODE, taxableValueTypeCode:TaxableValueType.STATUTORY.getCode(), lowerValue:    0, upperValue: 14999, rate:0.26).save()
		new FbtStatutoryMethodRateParameter(dateFrom:firstJan1990, commenceStart:firstJan1990, commenceEnd:ninthMay2011, customerCode:Customer.BASE_CUST_CODE, taxableValueTypeCode:TaxableValueType.STATUTORY.getCode(), lowerValue:15000, upperValue: 24999, rate:0.20).save()
		new FbtStatutoryMethodRateParameter(dateFrom:firstJan1990, commenceStart:firstJan1990, commenceEnd:ninthMay2011, customerCode:Customer.BASE_CUST_CODE, taxableValueTypeCode:TaxableValueType.STATUTORY.getCode(), lowerValue:25000, upperValue: 40000, rate:0.11).save()
		new FbtStatutoryMethodRateParameter(dateFrom:firstJan1990, commenceStart:firstJan1990, commenceEnd:ninthMay2011, customerCode:Customer.BASE_CUST_CODE, taxableValueTypeCode:TaxableValueType.STATUTORY.getCode(), lowerValue:40001, upperValue:999999, rate:0.07).save()

		new FbtStatutoryMethodRateParameter(dateFrom:tenthMay2011, commenceStart:tenthMay2011, commenceEnd:thirtyFirstMar2012, customerCode:Customer.BASE_CUST_CODE, taxableValueTypeCode:TaxableValueType.STATUTORY.getCode(), lowerValue:    0, upperValue: 14999, rate:0.20).save()
		new FbtStatutoryMethodRateParameter(dateFrom:tenthMay2011, commenceStart:tenthMay2011, commenceEnd:thirtyFirstMar2012, customerCode:Customer.BASE_CUST_CODE, taxableValueTypeCode:TaxableValueType.STATUTORY.getCode(), lowerValue:15000, upperValue: 24999, rate:0.20).save()
		new FbtStatutoryMethodRateParameter(dateFrom:tenthMay2011, commenceStart:tenthMay2011, commenceEnd:thirtyFirstMar2012, customerCode:Customer.BASE_CUST_CODE, taxableValueTypeCode:TaxableValueType.STATUTORY.getCode(), lowerValue:25000, upperValue: 40000, rate:0.14).save()
		new FbtStatutoryMethodRateParameter(dateFrom:tenthMay2011, commenceStart:tenthMay2011, commenceEnd:thirtyFirstMar2012, customerCode:Customer.BASE_CUST_CODE, taxableValueTypeCode:TaxableValueType.STATUTORY.getCode(), lowerValue:40001, upperValue:999999, rate:0.10).save()

		new FbtStatutoryMethodRateParameter(dateFrom:tenthMay2011, commenceStart:firstApr2012, commenceEnd:thirtyFirstMar2013, customerCode:Customer.BASE_CUST_CODE, taxableValueTypeCode:TaxableValueType.STATUTORY.getCode(), lowerValue:    0, upperValue: 14999, rate:0.20).save()
		new FbtStatutoryMethodRateParameter(dateFrom:tenthMay2011, commenceStart:firstApr2012, commenceEnd:thirtyFirstMar2013, customerCode:Customer.BASE_CUST_CODE, taxableValueTypeCode:TaxableValueType.STATUTORY.getCode(), lowerValue:15000, upperValue: 24999, rate:0.20).save()
		new FbtStatutoryMethodRateParameter(dateFrom:tenthMay2011, commenceStart:firstApr2012, commenceEnd:thirtyFirstMar2013, customerCode:Customer.BASE_CUST_CODE, taxableValueTypeCode:TaxableValueType.STATUTORY.getCode(), lowerValue:25000, upperValue: 40000, rate:0.17).save()
		new FbtStatutoryMethodRateParameter(dateFrom:tenthMay2011, commenceStart:firstApr2012, commenceEnd:thirtyFirstMar2013, customerCode:Customer.BASE_CUST_CODE, taxableValueTypeCode:TaxableValueType.STATUTORY.getCode(), lowerValue:40001, upperValue:999999, rate:0.13).save()

		new FbtStatutoryMethodRateParameter(dateFrom:tenthMay2011, commenceStart:firstApr2013, commenceEnd:thirtyFirstMar2014, customerCode:Customer.BASE_CUST_CODE, taxableValueTypeCode:TaxableValueType.STATUTORY.getCode(), lowerValue:    0, upperValue: 14999, rate:0.20).save()
		new FbtStatutoryMethodRateParameter(dateFrom:tenthMay2011, commenceStart:firstApr2013, commenceEnd:thirtyFirstMar2014, customerCode:Customer.BASE_CUST_CODE, taxableValueTypeCode:TaxableValueType.STATUTORY.getCode(), lowerValue:15000, upperValue: 24999, rate:0.20).save()
		new FbtStatutoryMethodRateParameter(dateFrom:tenthMay2011, commenceStart:firstApr2013, commenceEnd:thirtyFirstMar2014, customerCode:Customer.BASE_CUST_CODE, taxableValueTypeCode:TaxableValueType.STATUTORY.getCode(), lowerValue:25000, upperValue: 40000, rate:0.20).save()
		new FbtStatutoryMethodRateParameter(dateFrom:tenthMay2011, commenceStart:firstApr2013, commenceEnd:thirtyFirstMar2014, customerCode:Customer.BASE_CUST_CODE, taxableValueTypeCode:TaxableValueType.STATUTORY.getCode(), lowerValue:40001, upperValue:999999, rate:0.17).save()

		new FbtStatutoryMethodRateParameter(dateFrom:tenthMay2011, commenceStart:firstApr2014, commenceEnd:thirtyFirstMar2999, customerCode:Customer.BASE_CUST_CODE, taxableValueTypeCode:TaxableValueType.STATUTORY.getCode(), lowerValue:    0, upperValue: 14999, rate:0.20).save()
		new FbtStatutoryMethodRateParameter(dateFrom:tenthMay2011, commenceStart:firstApr2014, commenceEnd:thirtyFirstMar2999, customerCode:Customer.BASE_CUST_CODE, taxableValueTypeCode:TaxableValueType.STATUTORY.getCode(), lowerValue:15000, upperValue: 24999, rate:0.20).save()
		new FbtStatutoryMethodRateParameter(dateFrom:tenthMay2011, commenceStart:firstApr2014, commenceEnd:thirtyFirstMar2999, customerCode:Customer.BASE_CUST_CODE, taxableValueTypeCode:TaxableValueType.STATUTORY.getCode(), lowerValue:25000, upperValue: 40000, rate:0.20).save()
		new FbtStatutoryMethodRateParameter(dateFrom:tenthMay2011, commenceStart:firstApr2014, commenceEnd:thirtyFirstMar2999, customerCode:Customer.BASE_CUST_CODE, taxableValueTypeCode:TaxableValueType.STATUTORY.getCode(), lowerValue:40001, upperValue:999999, rate:0.20).save()




		new FbtCentsPerKmMethodRateParameter(dateFrom:asAtDate, customerCode:Customer.BASE_CUST_CODE, taxableValueTypeCode:TaxableValueType.CENTS_PER_KM_MC.getCode(),       centsPerKm:13.0).save()
		new FbtCentsPerKmMethodRateParameter(dateFrom:asAtDate, customerCode:Customer.BASE_CUST_CODE, taxableValueTypeCode:TaxableValueType.CENTS_PER_UPTO_2500CC.getCode(), centsPerKm:42.0).save()
		new FbtCentsPerKmMethodRateParameter(dateFrom:asAtDate, customerCode:Customer.BASE_CUST_CODE, taxableValueTypeCode:TaxableValueType.CENTS_PER_OVER_2500CC.getCode(), centsPerKm:51.0).save()

		new GstRateParameter(dateFrom:new GregorianCalendar(2000, Calendar.JULY, 1).time, rate:0.1).save()
		new MedicareLevyRateParameter(dateFrom:new GregorianCalendar(2000, Calendar.JULY, 1).time, rate:0.015).save()

		// Personal income tax rates & HECS/HELP rates
		def firstJul2008 = new GregorianCalendar(2008, Calendar.JULY, 1).time
		def firstJul2009 = new GregorianCalendar(2009, Calendar.JULY, 1).time
		def firstJul2010 = new GregorianCalendar(2010, Calendar.JULY, 1).time
		new IncomeTaxRateParameter(dateFrom:firstJul2008, lowerValue:     1, upperValue:  6000, rate:0.00, amount:    0.0, highestRate:false).save()
		new IncomeTaxRateParameter(dateFrom:firstJul2008, lowerValue:  6001, upperValue: 34000, rate:0.15, amount:    0.0, highestRate:false).save()
		new IncomeTaxRateParameter(dateFrom:firstJul2008, lowerValue: 34001, upperValue: 80000, rate:0.30, amount: 4200.0, highestRate:false).save()
		new IncomeTaxRateParameter(dateFrom:firstJul2008, lowerValue: 80001, upperValue:180000, rate:0.40, amount:18000.0, highestRate:false).save()
		new IncomeTaxRateParameter(dateFrom:firstJul2008, lowerValue:180001, upperValue:999999, rate:0.45, amount:58000.0, highestRate:true).save()
		new IncomeTaxRateParameter(dateFrom:firstJul2009, lowerValue:     1, upperValue:  6000, rate:0.00, amount:    0.0, highestRate:false).save()
		new IncomeTaxRateParameter(dateFrom:firstJul2009, lowerValue:  6001, upperValue: 35000, rate:0.15, amount:    0.0, highestRate:false).save()
		new IncomeTaxRateParameter(dateFrom:firstJul2009, lowerValue: 35001, upperValue: 80000, rate:0.30, amount: 4350.0, highestRate:false).save()
		new IncomeTaxRateParameter(dateFrom:firstJul2009, lowerValue: 80001, upperValue:180000, rate:0.38, amount:17850.0, highestRate:false).save()
		new IncomeTaxRateParameter(dateFrom:firstJul2009, lowerValue:180001, upperValue:999999, rate:0.45, amount:55850.0, highestRate:true).save()
		new IncomeTaxRateParameter(dateFrom:firstJul2010, lowerValue:     1, upperValue:  6000, rate:0.00, amount:    0.0, highestRate:false).save()
		new IncomeTaxRateParameter(dateFrom:firstJul2010, lowerValue:  6001, upperValue: 37000, rate:0.15, amount:    0.0, highestRate:false).save()
		new IncomeTaxRateParameter(dateFrom:firstJul2010, lowerValue: 37001, upperValue: 80000, rate:0.30, amount: 4650.0, highestRate:false).save()
		new IncomeTaxRateParameter(dateFrom:firstJul2010, lowerValue: 80001, upperValue:180000, rate:0.37, amount:17550.0, highestRate:false).save()
		new IncomeTaxRateParameter(dateFrom:firstJul2010, lowerValue:180001, upperValue:999999, rate:0.45, amount:54550.0, highestRate:true).save()

		// 1 Jul 2008 - 30 Jun 2009
		new HecsHelpRateParameter(dateFrom:firstJul2008, lowerValue:    1, upperValue: 41594, rate:0.000).save()
		new HecsHelpRateParameter(dateFrom:firstJul2008, lowerValue:41595, upperValue: 46333, rate:0.040).save()
		new HecsHelpRateParameter(dateFrom:firstJul2008, lowerValue:46334, upperValue: 51070, rate:0.045).save()
		new HecsHelpRateParameter(dateFrom:firstJul2008, lowerValue:51071, upperValue: 53754, rate:0.050).save()
		new HecsHelpRateParameter(dateFrom:firstJul2008, lowerValue:53755, upperValue: 57782, rate:0.055).save()
		new HecsHelpRateParameter(dateFrom:firstJul2008, lowerValue:57783, upperValue: 62579, rate:0.060).save()
		new HecsHelpRateParameter(dateFrom:firstJul2008, lowerValue:62580, upperValue: 65873, rate:0.065).save()
		new HecsHelpRateParameter(dateFrom:firstJul2008, lowerValue:65874, upperValue: 72492, rate:0.070).save()
		new HecsHelpRateParameter(dateFrom:firstJul2008, lowerValue:72493, upperValue: 77247, rate:0.075).save()
		new HecsHelpRateParameter(dateFrom:firstJul2008, lowerValue:77248, upperValue:999999, rate:0.080).save()

		// 1 Jul 2009 - 30 Jun 2010
		new HecsHelpRateParameter(dateFrom:firstJul2009, lowerValue:    1, upperValue: 43150, rate:0.000).save()
		new HecsHelpRateParameter(dateFrom:firstJul2009, lowerValue:43151, upperValue: 48066, rate:0.040).save()
		new HecsHelpRateParameter(dateFrom:firstJul2009, lowerValue:48067, upperValue: 52980, rate:0.045).save()
		new HecsHelpRateParameter(dateFrom:firstJul2009, lowerValue:52981, upperValue: 55764, rate:0.050).save()
		new HecsHelpRateParameter(dateFrom:firstJul2009, lowerValue:55765, upperValue: 59943, rate:0.055).save()
		new HecsHelpRateParameter(dateFrom:firstJul2009, lowerValue:59944, upperValue: 64919, rate:0.060).save()
		new HecsHelpRateParameter(dateFrom:firstJul2009, lowerValue:64920, upperValue: 68336, rate:0.065).save()
		new HecsHelpRateParameter(dateFrom:firstJul2009, lowerValue:68337, upperValue: 75203, rate:0.070).save()
		new HecsHelpRateParameter(dateFrom:firstJul2009, lowerValue:75204, upperValue: 80136, rate:0.075).save()
		new HecsHelpRateParameter(dateFrom:firstJul2009, lowerValue:80137, upperValue:999999, rate:0.080).save()

		new BenefitShareParameter(dateFrom:firstJul2008, customerCode:Customer.BASE_CUST_CODE, shareRate:0.0, adminFee:0.0).save()
		new BenefitShareParameter(dateFrom:firstJul2008, customerCode:"SESIAHS001", shareRate:0.5, adminFee:110.0).save()
		new BenefitShareParameter(dateFrom:firstJul2008, customerCode:"SSWAHS001", shareRate:0.5, adminFee:110.0).save()
		new BenefitShareParameter(dateFrom:firstJul2008, customerCode:"CASNSW001", shareRate:0.5, adminFee:110.0).save()

		// Luxury Allowance params
		new LuxuryAllowanceParameter(dateFrom:firstJul2008, companyTaxRate:0.30, dvThreshold:30000.0, dvRateLow:0.25, dvRateHigh:0.40, slRate:0.125).save()

		// terms & conditions showing on the exported quotes...
		def termsConditions = new QuoteTermsParameter(dateFrom:asAtDate, customerCode:Customer.BASE_CUST_CODE)
		termsConditions.addToTerms(new QuoteItemTerms(category:"Quotations", text:"The provision of a Novated Budget and Novated Lease contract... testing a really long condition. testing a really long condition. testing a really long condition. testing a really long condition. testing a really long condition. testing a really long condition. testing a really long condition. testing a really long condition. testing a really long condition. testing a really long condition. testing a really long condition. testing a really long condition. testing a really long condition. testing a really long condition. testing a really long condition. testing a really long condition. testing a really long condition. testing a really long condition. testing a really long condition. testing a really long condition."))
		termsConditions.addToTerms(new QuoteItemTerms(category:"Quotations", text:"Vehicle pricing is received by inNovated Leasing and..."))
		termsConditions.addToTerms(new QuoteItemTerms(category:"Quotations", text:"The Novated Budget quotation provided is valid at the time of issue..."))
		termsConditions.addToTerms(new QuoteItemTerms(category:"Ordering", text:"The acceptance of this Novated Budget indicates you acceptance of the..."))
		termsConditions.addToTerms(new QuoteItemTerms(category:"Ordering", text:"inNovated Leasing will, in arranging the purchase of any vehicle..."))
		termsConditions.addToTerms(new QuoteItemTerms(category:"Ordering", text:"This contract constitutes a binding order and hereby authorises inNovated..."))
		termsConditions.addToTerms(new QuoteItemTerms(category:"Customer Responsibilities", text:"inNovated Leasing will not be held liable under any..."))
		termsConditions.addToTerms(new QuoteItemTerms(category:"Customer Responsibilities", text:"Fringe Benefits Tax (FBT) liability calculations..."))
		termsConditions.addToTerms(new QuoteItemTerms(category:"Customer Responsibilities", text:"Annual registration, <b>compulsory third party (CTP)</b>, comprehensive..."))
		termsConditions.save(flush:true)

		// quote export template(s)
		new QuoteExportTemplateParameter(dateFrom:asAtDate, customerCode:Customer.BASE_CUST_CODE, templateName:QuoteExportTemplateParameter.BASE_TMPL).save()
		new QuoteExportTemplateParameter(dateFrom:asAtDate, customerCode:"CASNSW001", templateName:"quote/asnsw/quote-export-asnsw").save()

	}

	private def bootstrapTest() {
		// test bootstrap settings
	}

	private def bootstrapProd() {
		// prod bootstrap settings
	}
} 
