// Set config defaults for IRIS

grails.server.port.http=8081
grails.project.class.dir = "target/classes"
grails.project.war.file = "target/iris.war"

grails.project.dependency.resolution = {
	inherits "global"
	log "warn"
	repositories {
		//inherits true
		
		grailsPlugins()
		grailsHome()
		grailsCentral()
		
		mavenLocal()
		mavenCentral()
	}  
	dependencies {
		//runtime 'org.apache.activemq:activemq-core:5.3.2'
		runtime 'libphonenumber:libphonenumber:7.2.7'
	}
}