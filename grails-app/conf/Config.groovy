// locations to search for config files that get merged into the main config
// config files can either be Java properties files or ConfigSlurper scripts

// grails.config.locations = [ "classpath:${appName}-config.properties",
//                             "classpath:${appName}-config.groovy",
//                             "file:${userHome}/.grails/${appName}-config.properties",
//                             "file:${userHome}/.grails/${appName}-config.groovy"]

// if(System.properties["${appName}.config.location"]) {
//    grails.config.locations << "file:" + System.properties["${appName}.config.location"]
// }
grails.mime.file.extensions = true // enables the parsing of file extensions from URLs into the request format
grails.mime.use.accept.header = false
grails.mime.types = [html         : ['text/html', 'application/xhtml+xml'],
                     xml          : ['text/xml', 'application/xml'],
                     text         : 'text/plain',
                     js           : 'text/javascript',
                     rss          : 'application/rss+xml',
                     atom         : 'application/atom+xml',
                     css          : 'text/css',
                     csv          : 'text/csv',
                     all          : '*/*',
                     json         : ['application/json', 'text/json'],
                     form         : 'application/x-www-form-urlencoded',
                     multipartForm: 'multipart/form-data'
]
// The default codec used to encode data with ${}
grails.views.default.codec = "none" // none, html, base64
grails.views.gsp.encoding = "UTF-8"
grails.converters.encoding = "UTF-8"

//
grails.views.javascript.library = "jquery"

// enabled native2ascii conversion of i18n properties files
grails.enable.native2ascii = true

// Comments Plugin
grails.commentable.poster.evaluator = { getAuthUserDomain() }

// compiling jasper-reports source files
reports.src.location = "src/java"
reports.package.path = "com/innovated/iris/server/report"

// ActiveMQ Settings
grails.plugins.activemq.active = true
grails.plugins.activemq.port = 61616
grails.plugins.activemq.useJmx = false
grails.plugins.activemq.persistent = true

// set per-environment serverURL stem for creating absolute links
environments {
    production {
        grails.serverURL = "http://www.innovated.com.au:8080/iris"
        auditLog.enabled = true
        jasper.dir.reports = "WEB-INF/classes/com/innovated/iris/server/report"
    }
    development {
        grails.serverURL = "http://localhost:8081/${appName}"
        auditLog.enabled = true
        jasper.dir.reports = "WEB-INF/classes/com/innovated/iris/server/report"
    }
    test {
        grails.serverURL = "http://qinmoxi.cn:8080/iris"
        auditLog.enabled = false
        jasper.dir.reports = "WEB-INF/classes/com/innovated/iris/server/report"
    }
}

// log4j configuration
/*log4j = { root ->
    root.level = org.apache.log4j.Level.DEBUG
}*/

log4j = {
    // Example of changing the log pattern for the default console
    // appender:
    //
//    appenders {
//        console name:'stdout', layout:pattern(conversionPattern: '%c{2} %m%n')
//    }
//    appenders {
//        rollingFile name:'stacktrace',
//                maxFileSize:"5MB",
//                maxBackupIndex: 10,
//                file:"/var/log/tomcat6/${appName}_stacktrace.log",
//                'append':true,
//                threshold:org.apache.log4j.Level.ALL
//    }

    error 'org.codehaus.groovy.grails.web.servlet',  //  controllers
            'org.codehaus.groovy.grails.web.pages', //  GSP
            'org.codehaus.groovy.grails.web.sitemesh', //  layouts
            'org.codehaus.groovy.grails.web.mapping.filter', // URL mapping
            'org.codehaus.groovy.grails.web.mapping', // URL mapping
            'org.codehaus.groovy.grails.commons', // core / classloading
            'org.codehaus.groovy.grails.plugins', // plugins
            'org.codehaus.groovy.grails.orm.hibernate', // hibernate integration
            'org.springframework',
            'org.hibernate'

    warn 'org.mortbay.log'

    //trace 'org.hibernate.SQL'
}

/*
	Custom AuditLog configuration for project
	see Environment settings above for per/env configuration
*/
auditLog {
    //enabled = true	<= configured above!!
    serverId = "IRIS"
    verbose = true
    //actorKey =
    //username =
}

tenant {
    mode = "multiTenant"
}

// Grails MAIL plugin settings
grails {
    mail {
        host = "smtp.gmail.com"
        port = 587
        username = "ClientServices@inNovated.com.au"
        password = "apostle11"
        props = [
                "mail.smtp.port"           : "587",
                "mail.smtp.auth"           : "true",
                "mail.smtp.from"           : "ClientServices@inNovated.com.au",
                "mail.smtp.user"           : "ClientServices@inNovated.com.au",
                "mail.smtp.starttls.enable": "true",
                "mail.smtp.quitwait"       : "false"
        ]
    }
}

//grails {
//    mail {
//        host = "smtp.qq.com"
//        port = 25
//        username = "452166191@qq.com"
//        password = "MOGU6133756"
////        from = "452166191@qq.com"
//
//        props = [
//                "mail.smtp.port"                  : "25",
//                "mail.smtp.auth"                  : "true",
//                "mail.smtp.user"                  : "452166191@qq.com",
//                "mail.smtp.from"                  : "452166191@qq.com",
//                "mail.smtp.starttls.enable"       : "true",
//                "mail.smtp.quitwait"              : "false"
//        ]
//    }
//}

// IRIS specific configurations here!...
iris {
    geocoder {
        enabled = false
        url = "http://tinygeocoder.com/create-api.php?q="
    }
}

//log4j.logger.org.springframework.security='off,stdout'
