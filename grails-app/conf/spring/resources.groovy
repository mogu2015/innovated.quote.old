import org.apache.activemq.ActiveMQConnectionFactory
import org.codehaus.groovy.grails.commons.ConfigurationHolder;
import org.springframework.jms.connection.CachingConnectionFactory
import org.springframework.security.concurrent.ConcurrentSessionControllerImpl;
import org.springframework.security.concurrent.SessionRegistryImpl;

import com.innovated.iris.domain.CustomPropertyEditorRegistrar;

// Place your Spring DSL code here
beans = {
	// configure JMS provider (Apache ActiveMQ) - ONLY when we DON'T want to embed it! 
	if (!ConfigurationHolder.config.grails.plugins.activemq.active) {
		jmsConnectionFactory(CachingConnectionFactory) {
			targetConnectionFactory = { ActiveMQConnectionFactory cf ->
				brokerURL = "tcp://localhost:61616"	// use standalone ActiveMQ server on the local host
				useAsyncSend = true
			}
		}
	}
	
	// setup user session tracking
	/*
	sessionRegistry(SessionRegistryImpl)
	sessionController(ConcurrentSessionControllerImpl) {
		maximumSessions = -1
		sessionRegistry = ref('sessionRegistry')
	}
	*/
	
	customPropertyEditorRegistrar(CustomPropertyEditorRegistrar) {
		messageSource = ref('messageSource')
	}
}