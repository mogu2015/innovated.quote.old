class UrlMappings {
	static mappings = {
		"/$controller/$action?/$id?"{
			constraints {
				// apply constraints here
			}
		}
		
		"500"(view:'/error')
		"503"(view:'/error')

		name contextRoot: "/" {
			view = "/index"
		}
		
		// Map URL's for new customer creation (companies & individuals)
		"/customer/company/create" {
			controller = "customer"
			action = "newCompany"
		}
		"/customer/individual/create" {
			controller = "customer"
			action = "newIndividual"
		}
	}
}
