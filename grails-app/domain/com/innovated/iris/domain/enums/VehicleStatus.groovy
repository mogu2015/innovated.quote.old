package com.innovated.iris.domain.enums

class VehicleStatus {
	
	Date dateCreated
	Date lastUpdated
	
	static mapping = {
		table "enum_vehicle_status"
	}

    static constraints = {
    }
}
