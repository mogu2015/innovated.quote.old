package com.innovated.iris.domain.enums

class AustralianState {
	String name
	String abbreviation
	String capitalCity
	
	Date dateCreated
	Date lastUpdated
	
    static constraints = {
    	name(blank:false, unique:true)
    	abbreviation(blank:false, unique:true)
    	capitalCity(blank:false, unique:true)
    }
    
    static mapping = {
		table "enum_states"
	}
    
    String toString() { return name }
}
