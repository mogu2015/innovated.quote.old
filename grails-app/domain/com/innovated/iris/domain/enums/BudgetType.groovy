package com.innovated.iris.domain.enums

class BudgetType {
	String name
	boolean visible
	boolean enabled
	
	Date dateCreated
	Date lastUpdated

    static constraints = {
    	name(blank:false, unique:true)
    	visible()
    	enabled()
    	dateCreated()
    	lastUpdated()
    }
    
    static mapping = {
		table "enum_budget_type"
	}
    
    String toString() { return name }
}
