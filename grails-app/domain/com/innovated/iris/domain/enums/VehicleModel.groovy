package com.innovated.iris.domain.enums

import com.innovated.iris.domain.*

class VehicleModel {
	String name
	Date firstReleaseDate
	String vehicleClass = "Passenger"
	
	List variants = new ArrayList()
	static hasMany = [variants:MakeModel]
	static belongsTo = [make:VehicleManufacturer]
	
    static constraints = {
    	name(blank:false)
    	variants(nullable:false)
    	firstReleaseDate(nullable:true)
    	vehicleClass(inList:["Passenger","Motorcycle","Light Commercial","Heavy","Plant","Other"])
    }
    
    String toString() {
		return "${name}"
	}
}
