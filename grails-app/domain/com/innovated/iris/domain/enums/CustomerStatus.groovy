package com.innovated.iris.domain.enums

class CustomerStatus {
	String name
	String statusCode
	boolean visible
	boolean enabled
	
	Date dateCreated
	Date lastUpdated
	
    static constraints = {
    	name(blank:false, unique:true)
    	statusCode(blank:false, unique:true)
    	visible()
    	enabled()
    }
    
    static mapping = {
		table "enum_customer_status"
	}
	
	String toString() { return name }
}
