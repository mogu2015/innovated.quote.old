package com.innovated.iris.domain.enums

class PhoneType {
	String name
	boolean visible
	boolean enabled
	String displayFormat
	
	Date dateCreated
	Date lastUpdated
	
    static constraints = {
    	name(blank:false, unique:true)
    	displayFormat(blank:false)
    	visible()
    	enabled()
    }
    
    static mapping = {
		table "enum_phone_type"
	}
    
    String toString() { return name }
}
