package com.innovated.iris.domain.enums

class CustomerType {
	String name
	String typeCode
	boolean visible
	boolean enabled
	
	Date dateCreated
	Date lastUpdated
	
    static constraints = {
    	name(blank:false, unique:true)
    	typeCode(blank:false, unique:true)
    	visible()
    	enabled()
    }
    
    static mapping = {
		table "enum_customer_type"
	}
    
    String toString() { return name }
}
