package com.innovated.iris.domain.enums

class FuelType {
	String name
	String abbreviation
	boolean visible
	boolean enabled
	Double economyFactor = 1.0
	Integer ron
	
	Date dateCreated
	Date lastUpdated
	
    static constraints = {
    	name(nullable:false, blank:false, unique:true)
    	abbreviation(nullable:true, blank:true)
    	economyFactor(nullable:false, range:1.0..2.0)
    	ron(nullable:true)
    	visible()
    	enabled()
    }
    
    static mapping = {
		table "enum_fuel_type"
	}
    
    String toString() { return name }
}
