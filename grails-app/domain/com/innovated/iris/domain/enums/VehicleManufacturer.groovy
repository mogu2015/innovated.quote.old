package com.innovated.iris.domain.enums

class VehicleManufacturer {
	String name
	
	List models = new ArrayList()
	static hasMany = [models:VehicleModel]
	
    static constraints = {
    	name(blank:false)
    	models(nullable:false)
    }
    
    String toString() {
    	return name
    }
}
