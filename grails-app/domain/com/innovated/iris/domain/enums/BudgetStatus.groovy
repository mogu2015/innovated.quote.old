package com.innovated.iris.domain.enums

class BudgetStatus {
	String name
	String statusCode
	boolean visible
	boolean enabled
	
	Date dateCreated
	Date lastUpdated
	
    static constraints = {
    	name(blank:false, unique:true)
    	statusCode(blank:false, unique:true)
    	visible()
    	enabled()
    }
    
    static mapping = {
		table "enum_budget_status"
	}
	
	String toString() { return name }
}
