package com.innovated.iris.domain.enums

class VehicleType {
	String name
	boolean visible
	boolean enabled
	
	Date dateCreated
	Date lastUpdated
	
    static constraints = {
    	name(blank:false, unique:true)
    	visible()
    	enabled()
    }
    
    static mapping = {
		table "enum_vehicle_type"
	}
    
    String toString() { return name }
}
