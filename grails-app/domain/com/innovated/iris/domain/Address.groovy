package com.innovated.iris.domain

import com.innovated.iris.domain.enums.*

class Address {
	//static auditable = true
	
	AddressType addressType
	String street
	//String street2
	String suburb
	AustralianState state// = AustralianState.findByAbbreviation("NSW")
	String postcode
	
	// we geocode the address if we can!
	String lat
	String lng
	
	Date dateCreated
	Date lastUpdated
	
	static belongsTo = [entity:Entity]
	
    static constraints = {
		addressType(nullable:false)
		street(blank:false)
		//street2(blank:false)
		suburb(blank:false)
		state(nullable:false)
		postcode(blank:false)
		lat(blank:true, nullable:true)
		lng(blank:true, nullable:true)
    }
    
    static mapping = {
		table "entity_address"
	}
	
	String toString() {
		StringBuffer sb = new StringBuffer(street)
		sb.append(", ")
		//sb.append(street2)
		//sb.append(", ")
		sb.append(suburb)
		sb.append(" ")
		sb.append(state?.abbreviation)
		sb.append(" ")
		sb.append(postcode)
		return sb.toString()
	}
	
	int compareTo(addr) {
       this.toString().compareTo(addr.toString())
	}
}
