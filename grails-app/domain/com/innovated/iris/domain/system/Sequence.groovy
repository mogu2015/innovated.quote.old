package com.innovated.iris.domain.system

import java.util.Date;

class Sequence {
	NumberSequence numberSequence
	Long number
	String userCreated
	Date dateCreated
	
    static constraints = {
		numberSequence(nullable:false)
		number(nullable:false)
		userCreated(nullable:false, blank:false)
    }
}
