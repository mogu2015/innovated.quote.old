package com.innovated.iris.domain.system

import java.util.Date;

class NumberSequence {
	String code
	String name
	String format
	Date dateCreated
	Date lastUpdated
	
    static constraints = {
		code(nullable:false, blank:false, size:1..5)
		name(nullable:false, blank:false)
		format(nullable:false, blank:false)
    }
	
	String toString() {
		return "${code} - ${name}"
	}
}
