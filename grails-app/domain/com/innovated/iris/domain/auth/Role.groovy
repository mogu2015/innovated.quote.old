package com.innovated.iris.domain.auth

import com.innovated.iris.domain.auth.User

/**
 * Authority domain class.
 */
class Role {

	static hasMany = [people: User]

	String description
	String authority
	boolean enabled
	Date dateCreated
	Date lastUpdated

	static constraints = {
		authority(blank: false, unique: true)
		description()
		enabled()
		dateCreated()
		lastUpdated()
	}
	
	String toString() {
		return this.authority[5..-1]
	}
}
