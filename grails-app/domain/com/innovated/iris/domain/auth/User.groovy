package com.innovated.iris.domain.auth

import com.innovated.iris.domain.auth.Role
import com.innovated.iris.domain.*
import com.innovated.iris.domain.enums.*

/**
 * User domain class.
 */
class User {
	static transients = ['pass']
	static hasMany = [authorities: Role]
	static belongsTo = Role

	String username
	String userRealName
	String passwd
	boolean enabled
	String email
	boolean emailShow
	String description = ''
	String pass
	
	Customer customer
	
	Date dateCreated
	Date lastUpdated

	static constraints = {
		username(blank: false, unique: true)
		userRealName(blank: false)
		passwd(blank: false)
		enabled()
		dateCreated()
		lastUpdated()
		customer(nullable:true)
	}
	
	String toString() {
		return username
	}
}
