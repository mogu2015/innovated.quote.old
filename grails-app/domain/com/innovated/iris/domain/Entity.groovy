package com.innovated.iris.domain

class Entity {
	public static final String COMPANY = "Company"
	public static final String INDIVIDUAL = "Individual"
	
	String name
	String type
	
	List addresses = new ArrayList()
	List phones = new ArrayList()
	List emails = new ArrayList()
	
	Date dateCreated
	Date lastUpdated
	
	static hasMany = [addresses:Address, phones:Phone, emails:Email]
	
    static constraints = {
    	name(/*unique:true, */nullable:false, blank:false)
    	type(nullable:false, blank:false)
    	addresses(nullable:false)
    	phones(nullable:false)
    	emails(nullable:false)
    }
    
    static mapping = {
        tablePerHierarchy false
    }
    
    String toString() {
    	return name
    }
}
