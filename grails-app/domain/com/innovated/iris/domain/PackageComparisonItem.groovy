package com.innovated.iris.domain

import com.innovated.iris.server.fbt.FbtConcessionType;
import com.innovated.iris.server.fbt.PackagingComparisonItemType;
import com.innovated.iris.server.fbt.TaxableValueType;

class PackageComparisonItem {
	PackagingComparisonItemType type = PackagingComparisonItemType.GROSS_SALARY
	Double amount = 0.0
	
	static belongsTo = [comparison:PackageComparison]
	
	static constraints = {
	}
	
	String toString() {
		return type.name
	}
}
