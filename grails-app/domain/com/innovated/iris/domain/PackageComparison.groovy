package com.innovated.iris.domain

import com.innovated.iris.util.*
import com.innovated.iris.domain.auth.*
import com.innovated.iris.domain.enums.*
import com.innovated.iris.domain.lookup.*
import com.innovated.iris.services.*
import com.innovated.iris.server.fbt.*

class PackageComparison {
	boolean isDefault = false
	boolean noVehicle = false
	boolean comparisonOnly = true
	boolean usingEcm = false
	boolean isBest = false
	boolean selected = false
	
	TaxableValueType tvType = TaxableValueType.FBT_EXEMPT
	FbtConcessionType fcType = FbtConcessionType.NO_CONCESSION
	String name = "Not Packaged"
	List items = new ArrayList<PackageComparisonItem>()

	static hasMany = [items:PackageComparisonItem]
	static belongsTo = [quote:Quote]
	
	static constraints = {
		//
	}
    
    String toString() {
    	return name + (isDefault ? " (Default)" : "")
    }
}
