package com.innovated.iris.domain.importExport

class VehicleDataImport {
	Integer publishedVehicleID
    Integer vehicleID
	String make
	String modelReleaseVersion
    String marketingModel
    String variant
    String makeModelVariant
    String bodyStyle
    Integer seatingCapacity
    String bodySeats
    Double engineDisplacement
    String engineConfiguration
    String induction
    String engine
    String transmissionType
    Integer fwdGearsNo
    String trans
    String fuelType
    String drivingWheels
    String vehicleClass
    //Integer gvgStarRating
    //Integer overallRating
    //Integer airPollutionRating
    //Integer greenhouseRating
    Double fuelConsumption
    Double fuelConsumptionUrban
    Double fuelConsumptionExtraUrban
    //Integer co2Emissions
    //Integer co2EmissionsUrban
    //Integer co2EmissionsExtraUrban
    Integer modelReleaseYear
    String currentModel				// Yes OR No
    //Integer maxResultSize			// records in this result set
    //Integer starRatingID
    //Integer airRatingID
    //Integer greenRatingID
    //Integer maxStarRatingID
    //Integer maxAirRatingID
    //Integer maxGreenRatingID
    //Integer stationaryNoiseData
    //Integer testSpeed
    //String electronicStabilityControl
    //String frontalAirbagDriver
    //String frontalAirbagPassenger
    //String sideProtectionAirbagTorso
    //String sideProtectionAirbagHead
    //String seatbeltReminder
    //String directInjectionPetrol	// Yes OR No
	
	Date dateCreated
	Date lastUpdated
	
    static constraints = {
    	publishedVehicleID(nullable:false)
		vehicleID(nullable:false)
		make(nullable:false, blank:false)
		modelReleaseVersion(nullable:true, blank:true)
		marketingModel(nullable:false, blank:false)
		variant(nullable:true, blank:true)
		makeModelVariant(nullable:true, blank:true)
		bodyStyle(nullable:true, blank:true)
		seatingCapacity(nullable:true)
		bodySeats(nullable:true, blank:true)
		engineDisplacement(nullable:true)
		engineConfiguration(nullable:true, blank:true)
		induction(nullable:true, blank:true)
		engine(nullable:true, blank:true)
		transmissionType(nullable:true, blank:true)
		fwdGearsNo(nullable:true)
		trans(nullable:true, blank:true)
		fuelType(nullable:true, blank:true)
		drivingWheels(nullable:true, blank:true)
		vehicleClass(nullable:true, blank:true)
		fuelConsumption(nullable:false)
		fuelConsumptionUrban(nullable:true)
		fuelConsumptionExtraUrban(nullable:true)
		modelReleaseYear(nullable:false)
		currentModel(nullable:false, blank:false)
    }
}
