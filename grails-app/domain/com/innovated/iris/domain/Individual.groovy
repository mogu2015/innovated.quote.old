package com.innovated.iris.domain

import com.innovated.iris.enums.Gender;

class Individual extends Entity {
	String firstName
	String middleName
	String lastName
	String preferredName
	Date dateOfBirth
	String type = "Individual"
	Gender gender = Gender.UNKNOWN
	
	Date dateCreated
	Date lastUpdated
	
    static constraints = {
    	firstName(nullable:false, blank:false)
    	middleName(nullable:true, blank:true)
    	lastName(nullable:false, blank:false)
    	preferredName(nullable:false, blank:false)
    	dateOfBirth(nullable:true)
		gender(nullable:false)
    }
    
    String toString() {
    	return name
    }
}
