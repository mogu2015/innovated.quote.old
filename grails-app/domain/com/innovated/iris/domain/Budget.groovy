package com.innovated.iris.domain

import org.grails.comments.*
import com.innovated.iris.domain.enums.*

class Budget implements Commentable {
	BudgetType type
	BudgetStatus status
	
    static constraints = {
    	type(nullable:false)
    	status(nullable:false)
    }
    
    def onAddComment = { comment ->
        // post processing logic for newly added comment
    }
}
