package com.innovated.iris.domain

import com.innovated.iris.domain.enums.*
import com.innovated.iris.domain.lookup.*

class QuoteItemTerms {
	String category = "Condition Category"
	String text = "Condition description"
	
	static belongsTo = [quoteTermsParameter:QuoteTermsParameter]
	
    static constraints = {
		category(blank:false,nullable:false)
		text(blank:false,nullable:false)
    }

	static mapping = {
		text type:'text'
	}
    
    String toString() {
    	return "${category}:${text}"
    }
}
