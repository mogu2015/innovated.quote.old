package com.innovated.iris.domain

import java.util.Calendar;

import org.grails.comments.Commentable;

import com.innovated.iris.util.*;
import com.innovated.iris.enums.*;
import com.innovated.iris.domain.application.FinanceApplication;
import com.innovated.iris.domain.enums.*
import com.innovated.iris.domain.lookup.*
import com.innovated.iris.server.fbt.*

class Quote implements Commentable {
	QuoteStatus status = QuoteStatus.QUOTE_CREATED
	Date fromDate = new Date()
	Date pricingConfirmed
	
	Customer customer
	Employment employment
	Company company
	Address address
	Boolean isRegional = false
	Phone mobile
	Email email
	
	MakeModel vehicle
	Boolean vehicleNew = true
	Integer vehicleYear = Calendar.instance.get(Calendar.YEAR)
	String transmission = ""
	String shape = ""
	String colourPreference = "Metallic Paint"
	String interiorTrim = "Standard Cloth Trim"
	String options = ""
	
	Integer kmPerYear = 0
	Integer leaseTerm = 0		// in months
	
	// calculated and "looked-up" values
	String stateString = ""
	String addressString = ""
	Double grossAnnualSalary = 0.0
	Double businessUsage = 0.0	// as a percentage (not a rate). [ie. 91.5% = 91.5 => (NOT 0.915!)]
	
	Long vehSupplierId = 0
	Double listPrice = 0.0
	List optionItems = new ArrayList<QuoteItemOption>()
	List dealerItems = new ArrayList<QuoteItemDealer>()
	Double purchasePrice = 0.0
	Double purchasePriceGst = 0.0
	Double purchasePriceGstCalc = 0.0
	Double purchasePriceGstAdj = 0.0
	Double luxuryTax = 0.0
	Double luxuryTaxCalc = 0.0
	Double luxuryTaxAdj = 0.0
	List accessoryItems = new ArrayList<QuoteItemAccessory>()
	Double fbtBaseValue = 0.0
	List inclGstItems = new ArrayList<QuoteItemInclGst>()
	List noGstItems = new ArrayList<QuoteItemNoGst>()
	Double driveAwayPrice = 0.0
	Boolean vehFinAmountInclGst = false
	Double vehFinAmount = 0.0
	List insuranceItems = new ArrayList<QuoteItemInsurance>()
	Double insuranceItemsTotal = 0.0
	
	Double annualRegoCost = 0.0
	Double annualCtpCost = 0.0
	Long regoSupplierId = 0
	Long ctpSupplierId = 0
	Boolean regoIncludesGst = false
	
	Double maintCostPer1000km = 0.0
	Double maintKmAdj = 0.0
	Double annualMaintenance = 0.0
	
	Integer numTyres = 4
	Integer tyreChoice = 0
	Double costPerTyreStandard = 0.0
	Double costPerTyrePremium = 0.0
	Double annualTyres = 0.0
	
	Long fuelSupplierId = 0
	FuelType fuelTypeUsed
	Double fuelEconomyBuffer = 1.0		// Set "Realistic" fuel economy to 100% of CombinedFuelEconomy figure
	Double fuelEconomyFactor = 1.0		// Fuel usage factor (only used for LPG at the moment -> 135%). Defaults to 100%
	Double fuelCostPerLitre = 0.0
	Double fuelCostPerLitreMetro = 0.0
	Double fuelCostPerLitreRegional = 0.0
	Double annualFuel = 0.0
	
	Long compInsurSupplierId = 0
	Boolean compInsurIsUnder25 = false
	Double compInsurMetro = 0.0
	Double compInsurRegional = 0.0
	Double compInsurUnder25Metro = 0.0
	Double compInsurUnder25Regional = 0.0
	Double annualCompInsur = 0.0
	
	Long roadsideSupplierId = 0
	Double roadsideJoiningFee = 0.0
	Double roadsideAnnualFee = 0.0
	Double annualRoadside = 0.0
	
	Double annualDetailWash = 0.0
	Double annualOther = 0.0
	
	Double psdRate = 0.0
	Double psdRateLow = 0.0
	Double psdRateHigh = 0.0
	Double psdThreshold = 0.0
	Double psdAmount = 0.0
	
	Double lctRate = 0.0
	Double lctThreshold = 0.0
	Double lctFuelConsumption = 0.0
	Double lctFuelEfficientRate = 0.0
	Double lctFuelEfficientThreshold = 0.0
	Double lctCreditThreshold = 0.0
	Double lctAmount = 0.0
	
	Double luxCompanyTaxRate = 0.0
	Double luxDvThreshold = 0.0
	Double luxDvRateLow = 0.0
	Double luxDvRateHigh = 0.0
	Double luxSlRate = 0.0
	Boolean luxUseDv = true
	
	Integer rvChoice = 0
	Double rvChoiceValue = 0.0
	Double rvMin = 0.0
	Double rvMax = 0.0
	Double rvMinHiKm = 0.0
	Integer rvHiKmThreshold = 0
	Double rvRate = 0.0
	Double rvPercentage = 0.0
	Double rvValue = 0.0
	Double rvAltValue = 0.0
	Boolean rvUseAltValue = false
	
	Long   finSupplierId = 0
	Double finInterestRate = 0.0
	Double finDocFee = 0.0
	Double finAdjAmt = 0.0
	Boolean finPmtInAdvance = false
	Integer finNumDeferredPayments = 0
	Double finMthlyPymt = 0.0
	Double finMthlyPymtAdj = 0.0
	Double finMthlyPymtGst = 0.0
	Double finInsurMthlyPymt = 0.0
	Double finVehMthlyPymt = 0.0
	Double finAmount = 0.0
	List finApplications = new ArrayList<FinanceApplication>()
	
	Double annualFinanceVehicle = 0.0
	Double annualFinanceInsur = 0.0
	Double annualFinanceGst = 0.0
	
	Integer brokerageChoice = 0
	Double brokerageVehicle = 0.0
	Double brokerageOther = 0.0
	Double brokerageTotal = 0.0
	
	Double brokeragePercent = 0.0
	Double annualFeeFltMng = 0.0
	Double annualFeeFbtRep = 0.0
	Double annualFeeAdmin = 0.0
	Double annualFeeBankChg = 0.0
	
	Double annualRunningCosts = 0.0
	Double annualTotalCosts = 0.0
	
	// opcost/logbook details
	Integer hasLogBook = 0	// 0 = false, 1 = true
	Date logBookStart = new Date()
	
	// drawn from employer defaults...
	List allowedFbtCalcMethods = new ArrayList<TaxableValueType>()
	FbtConcessionType concession = FbtConcessionType.NO_CONCESSION
	Boolean allowsFbtEcm = true
	Boolean canClaimGst = true
	List comparisons = new ArrayList<PackageComparison>()
	PayrollFrequency defaultPayFreq = PayrollFrequency.MONTHLY

	// Employer Benefit Share (as used by NSW Health)
	Double employerShareFee = 0.0
	Double employerShareRate = 0.0		// this is stored as a percentage - not a rate!!! ie. 50% = 50... not 0.5!
	Double employerShareAmount = 0.0
	
	Date dateCreated
	Date lastUpdated

	Boolean priceObtained = false
	
	// temp list for display purposes only (for now...)
	List breakdown = new ArrayList()
	static transients = ['breakdown']
	
	static mapping = {
		allowedFbtCalcMethods joinTable:[name:'quote_fbt_tv_types']
    }
	
	static hasMany = [
		optionItems:QuoteItemOption,
		dealerItems:QuoteItemDealer,
		accessoryItems:QuoteItemAccessory,
		inclGstItems:QuoteItemInclGst,
		noGstItems:QuoteItemNoGst,
		insuranceItems:QuoteItemInsurance,
		allowedFbtCalcMethods:TaxableValueType,
		comparisons:PackageComparison,
		finApplications:FinanceApplication
	]
	
	static constraints = {
		status(nullable:false)
		pricingConfirmed(nullable:true)
    	fromDate(nullable:false)
    	customer(nullable:false)
    	employment(nullable:true)
		company(nullable: true)
    	grossAnnualSalary(nullable:false, range:10000.0..999999.0)
    	businessUsage(nullable:true, range:0.0..100.0)
    	employerShareRate(nullable:false, range:0.0..100.0)
    	employerShareFee(nullable:false, range:0.0..1000.0)
    	address(nullable:false)
    	mobile(nullable:false)
    	email(nullable:false)
    	vehicle(nullable:false)
//    	transmission(inList:MakeModel.VALID_TRANS)	// ["Automatic","Manual"]
//    	shape(inList:MakeModel.VALID_SHAPES)		// ["Sedan","Hatch","Wagon","Utility","Dual-Cab","Coupe","Other"]
    	colourPreference(nullable:true, blank:true)
    	interiorTrim(nullable:true, blank:true)
    	options(nullable:true, blank:true)
    	kmPerYear(range:1..75000)
    	leaseTerm(range:1..60)
    	stateString(nullable:false, blank:true)
    	addressString(nullable:false, blank:true)
    	fuelTypeUsed(nullable:true)
    }

}
