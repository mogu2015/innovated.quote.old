package com.innovated.iris.domain

import org.grails.comments.*
import com.innovated.iris.domain.enums.*

class Asset implements Commentable {
	String assetCode
	Customer owner
	
	Date acquisitionDate
	BigDecimal acquisitionPrice
	Date disposalDate
	BigDecimal disposalPrice
	
	String information
	List budgets = new ArrayList()
	
	Date dateCreated
	Date lastUpdated
	
	static belongsTo = [owner:Customer]
	static hasMany = [budgets:Budget]
	//static transients = ['currentBudget']

    static constraints = {
    	assetCode(unique:true, blank:false)
    	owner(nullable:false)
    	acquisitionDate(nullable:false)
    	acquisitionPrice(nullable:false)
    	disposalDate(nullable:true)
    	disposalPrice(nullable:true)
    	information(nullable:true)
    	budgets(nullable:false)
    }
    
    static mapping = {
        tablePerHierarchy false
    }
    
    /*def onAddComment = { comment ->
        // post processing logic for newly added comment
    }*/
    
    /**
     * Retrieve the current budget (IF it exists!)
     */
    /*Budget getCurrentBudget() {
    	try {
    		return Collections.max(budgets)
    	} catch (Exception e) {
    		return null
    	}
    }*/
}
