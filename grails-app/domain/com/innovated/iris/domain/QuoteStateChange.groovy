package com.innovated.iris.domain

import java.util.Date;

import com.innovated.iris.enums.QuoteStatus;

class QuoteStateChange {

	Long quoteId
	QuoteStatus oldState
	QuoteStatus newState
	String changedBy
	String comment
	
	Date dateCreated
	Date lastUpdated
	
    static constraints = {
		quoteId(nullable:false)
		oldState(nullable:true)
		newState(nullable:false)
		changedBy(nullable:false, blank:false)
		comment(nullable:true, blank:true)
    }
}
