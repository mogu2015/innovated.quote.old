package com.innovated.iris.domain

import com.innovated.iris.domain.enums.*

class Registration {
	String plate
	Date expiryDate
	RegistrationUsageType usage
	AustralianState state
	Vehicle vehicle
	Date paymentDate
	BigDecimal paymentTotal
	
	//static belongsTo = [vehicle:Vehicle]
	
    static constraints = {
    	vehicle(nullable:false)
    	plate(blank:false, size:1..10)
    	expiryDate(nullable:false)
    	usage(nullable:false)
    	state(nullable:false)
    	paymentDate(nullable:true)
    	paymentTotal(nullable:true)
    }
    
    String toString() {
    	return "${plate} (${state.abbreviation})"
    }
}
