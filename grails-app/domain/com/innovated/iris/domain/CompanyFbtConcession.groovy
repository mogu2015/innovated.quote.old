package com.innovated.iris.domain

import com.innovated.iris.domain.enums.*
import com.innovated.iris.server.fbt.*

class CompanyFbtConcession {
	Date dateFrom = new Date()
	FbtConcessionType type = FbtConcessionType.NO_CONCESSION
	Date dateCreated
	Date lastUpdated
	
	static belongsTo = [company:Company]
	
    static constraints = {
    	dateFrom(nullable:false)
    	type(nullable:false)
    }
}
