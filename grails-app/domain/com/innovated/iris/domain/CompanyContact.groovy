package com.innovated.iris.domain

class CompanyContact extends Entity {
	String role
	String type = "Individual"
	
	Date dateCreated
	Date lastUpdated
	
	static belongsTo = [company:Company]
	
    static constraints = {
    }
    
    String toString() {
    	return name
    }
}
