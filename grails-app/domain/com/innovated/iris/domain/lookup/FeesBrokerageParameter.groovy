package com.innovated.iris.domain.lookup

import com.innovated.iris.domain.enums.*
import com.innovated.iris.domain.*

class FeesBrokerageParameter {
	Date dateFrom = new Date()
	String customerCode = Customer.BASE_CUST_CODE
	
	Double brokeragePercent = 3.4
	Double annualFeeFltMng  = 84.0		//  $7 p/mth
	Double annualFeeFbtRep  = 36.0		//  $3 p/mth
	Double annualFeeAdmin   = 396.0		// $33 p/mth
	Double annualFeeBankChg = 36.0		//  $3 p/mth
	
	Date dateCreated
	Date lastUpdated
	
    static constraints = {
    	dateFrom(nullable:false)
    	customerCode(nullable:false, blank:false)
    	brokeragePercent(nullable:false, range:0.0..5.0)
    	annualFeeFltMng(nullable:false, range:0.0..100.0)
    	annualFeeFbtRep(nullable:false, range:0.0..50.0)
    	annualFeeAdmin(nullable:false, range:0.0..500.0)
    	annualFeeBankChg(nullable:false, range:0.0..50.0)
    }
    
    static mapping = {
		table "lookup_fees_brokerage"
	}
}
