package com.innovated.iris.domain.lookup

import com.innovated.iris.domain.enums.*
import com.innovated.iris.domain.*

class VehicleRegistrationParameter {
	Date dateFrom = new Date()
	String customerCode = Customer.BASE_CUST_CODE
	String state = "NSW"
	Supplier regoSupplier
	Supplier ctpSupplier
	Double regoCostTwelveMths = 384.0
	Double regoCostSixMths = 192.0
	boolean regoIncludesGst = false
	Double ctpCostTwelveMths = 384.0
	Double ctpCostSixMths = 192.0
	
    static constraints = {
    	dateFrom(nullable:false)
    	customerCode(nullable:false, blank:false)
    	state(nullable:false, blank:false)
    	regoSupplier(nullable:false)
    	ctpSupplier(nullable:false)
    	regoCostTwelveMths(nullable:false)
    	regoCostSixMths(nullable:true)
    	regoIncludesGst()
    	ctpCostTwelveMths(nullable:false)
    	ctpCostSixMths(nullable:true)
    }
    
    static mapping = {
		table "lookup_rego"
	}
}
