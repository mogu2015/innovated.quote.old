package com.innovated.iris.domain.lookup

import com.innovated.iris.domain.enums.*
import com.innovated.iris.domain.*

class LuxuryCarTaxParameter {
	Date dateFrom = new Date()
	String customerCode = Customer.BASE_CUST_CODE
	
	Double rate = 0.33
	Double threshold = 57180.0
	Double fuelConsumption = 7.0
	Double fuelEfficientRate = 0.33
	Double fuelEfficientThreshold = 75000.0
	Double creditThreshold = 3000.0
	
	Date dateCreated
	Date lastUpdated
	
    static constraints = {
    	dateFrom(nullable:false)
    	customerCode(nullable:false, blank:false)
    	
    	rate(nullable:false, range:0.0001..1.0)
    	threshold(nullable:false)
    	fuelConsumption(nullable:true)
    	fuelEfficientRate(nullable:true)
    	fuelEfficientThreshold(nullable:true)
    	creditThreshold(nullable:true)
    }
    
    static mapping = {
		table "lookup_lct"
	}
}
