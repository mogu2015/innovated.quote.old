package com.innovated.iris.domain.lookup

class RatesParameter {
	Date dateFrom = new Date()
	Date dateTo = new Date()

	String rateType
	String rateDesc
	Float percentage = 0
	Double value = 0
	
	Date dateCreated
	Date lastUpdated
	
	static constraints = {
		dateFrom(nullable:false)
		dateTo(nullable: false)
		rateType(nullable: false, blank: false)
		rateDesc(nullable: true, blank: true)
		percentage(nullable: true)
		value(nullable: true)
	}
    
    static mapping = {
		table "lookup_rates"
	}
}
