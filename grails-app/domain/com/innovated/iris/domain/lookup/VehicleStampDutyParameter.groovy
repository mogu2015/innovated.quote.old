package com.innovated.iris.domain.lookup

import com.innovated.iris.domain.enums.*
import com.innovated.iris.domain.*

class VehicleStampDutyParameter {
	Date dateFrom = new Date()
	String customerCode = Customer.BASE_CUST_CODE
	String state = "NSW"
	Double lowValue = 0.03
	Double highValue
	Double thresholdValue
	
	Date dateCreated
	Date lastUpdated
		
    static constraints = {
    	dateFrom(nullable:false)
    	customerCode(nullable:false, blank:false)
    	state(nullable:false, blank:false)
    	lowValue(nullable:false, range:0.001..10.0)
    	highValue(nullable:true)
    	thresholdValue(nullable:true)
    }
    
    static mapping = {
		table "lookup_stamp_duty"
	}
}
