package com.innovated.iris.domain.lookup

import com.innovated.iris.domain.enums.*
import com.innovated.iris.domain.*

class RoadsideParameter {
	Date dateFrom = new Date()
	String customerCode = Customer.BASE_CUST_CODE
	String state = "NSW"
	Supplier defaultSupplier
	Double joiningFee = 0.0
	Double annualFee = 81.6
	Date dateCreated
	Date lastUpdated
		
    static constraints = {
    	dateFrom(nullable:false)
    	customerCode(nullable:false, blank:false)
    	state(nullable:false, blank:false)
    	defaultSupplier(nullable:false)
    	joiningFee(nullable:false)
    	annualFee(nullable:false)
    }
    
    static mapping = {
		table "lookup_roadside"
	}
}
