package com.innovated.iris.domain.lookup

import java.util.Date;

class MedicareLevyRateParameter {
	Date dateFrom = new Date()
	Double rate = 0.015		// default = 1.5%
	
	Date dateCreated
	Date lastUpdated
	
    static constraints = {
		dateFrom(nullable:false)
		rate(nullable:false, range:0.001..0.3)
    }
    
    static mapping = {
		table "lookup_mcare_levy"
	}
}
