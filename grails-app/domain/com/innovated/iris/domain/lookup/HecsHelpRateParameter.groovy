package com.innovated.iris.domain.lookup

import java.util.Date;

class HecsHelpRateParameter {
	Date dateFrom = new Date()
	
	Double lowerValue = 1.0
	Double upperValue = 43151.0
	Double rate = 0.0
	
	Date dateCreated
	Date lastUpdated
	
    static constraints = {
    }
    
    static mapping = {
		table "lookup_hecs_help"
	}
}
