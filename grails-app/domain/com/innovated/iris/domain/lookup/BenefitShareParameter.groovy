package com.innovated.iris.domain.lookup

import java.util.Date;
import com.innovated.iris.domain.enums.*
import com.innovated.iris.domain.*

class BenefitShareParameter {
	Date dateFrom = new Date()
	String customerCode = Customer.BASE_CUST_CODE
	Double shareRate = 0.0		// percentage rate of the tax saving that's shared by the employer
	Double adminFee = 0.0		// annual admin fee added on by the employer
	
	Date dateCreated
	Date lastUpdated
	
	static constraints = {
		dateFrom(nullable:false)
		customerCode(nullable:false, blank:false)
		shareRate(nullable:false, range:0.0..1.0)
		adminFee(nullable:false, range:0.0..1000.0)
	}
	
	static mapping = {
		table "lookup_benefit_share"
	}
}
