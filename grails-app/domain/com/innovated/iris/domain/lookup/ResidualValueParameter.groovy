package com.innovated.iris.domain.lookup

import com.innovated.iris.domain.enums.*
import com.innovated.iris.domain.*

class ResidualValueParameter {
	Date dateFrom = new Date()
	String customerCode = Customer.BASE_CUST_CODE
	
	Integer term = 36	// lease term in months
	Double min = 0.45
	Double max = 0.50
	Double minHiKm = 0.30
	Integer hiKmThreshold = 30000
	
	Date dateCreated
	Date lastUpdated
	
    static constraints = {
    	dateFrom(nullable:false)
    	customerCode(nullable:false, blank:false)
    	term(nullable:false, range:1..60)
    	min(nullable:false, range:0.0001..1.0)
    	minHiKm(nullable:false, range:0.0001..1.0)
    	max(nullable:false, range:0.0001..1.0)
    	hiKmThreshold(nullable:false)
    }
    
    static mapping = {
		table "lookup_residual"
	}
}
