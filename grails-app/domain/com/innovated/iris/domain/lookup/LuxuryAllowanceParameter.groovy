package com.innovated.iris.domain.lookup

import java.util.Date;

class LuxuryAllowanceParameter {
	Date dateFrom = new Date()
	Double companyTaxRate = 0.30
	Double dvThreshold = 30000.0
	Double dvRateLow = 0.25
	Double dvRateHigh = 0.40
	Double slRate = 0.125
	
	Date dateCreated
	Date lastUpdated
	
	static constraints = {
		dateFrom(nullable:false)
		companyTaxRate(range:0.1..0.9)
		dvThreshold(range:10000.0..50000.0)
		dvRateLow(range:0.01..0.99)
		dvRateHigh(range:0.01..0.99)
		slRate(range:0.01..0.99)
	}
	
	static mapping = {
		table "lookup_lux"
	}
}
