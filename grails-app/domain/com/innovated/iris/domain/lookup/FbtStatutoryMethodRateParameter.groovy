package com.innovated.iris.domain.lookup

import com.innovated.iris.domain.*
import com.innovated.iris.domain.enums.*
import com.innovated.iris.server.fbt.*

class FbtStatutoryMethodRateParameter {
	Date dateFrom = new Date()
	Date commenceStart = new Date()
	Date commenceEnd = new Date()
	String customerCode = Customer.BASE_CUST_CODE
	
	Integer taxableValueTypeCode = TaxableValueType.STATUTORY.getCode()
	Double lowerValue = 0.0
	Double upperValue = 14999.0
	Double rate = 0.26
	
	Date dateCreated
	Date lastUpdated
	
    static constraints = {
    	dateFrom(nullable:false)
		commenceStart(nullable:false)
		commenceEnd(nullable:false)
    	customerCode(nullable:false, blank:false)
    	taxableValueTypeCode(nullable:false)
    	lowerValue(nullable:false, range:0..999999)
    	upperValue(nullable:false, range:0..999999)
    	rate(nullable:false, range:0.01..1.00)
    }
    
    static mapping = {
		table "lookup_fbt_tv_statutory"
	}
}
