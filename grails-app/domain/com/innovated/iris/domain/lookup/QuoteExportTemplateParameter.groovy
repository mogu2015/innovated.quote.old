package com.innovated.iris.domain.lookup

import com.innovated.iris.domain.enums.*
import com.innovated.iris.domain.*

class QuoteExportTemplateParameter {
	public static final String BASE_TMPL = "quote/standard/quote-export"
	
	Date dateFrom = new Date()
	String customerCode = Customer.BASE_CUST_CODE
	String templateName = BASE_TMPL
	
	Date dateCreated
	Date lastUpdated
	
	static constraints = {
    	dateFrom(nullable:false)
    	customerCode(nullable:false, blank:false)
		templateName(nullable:false, blank:false)
    }
    
    static mapping = {
		table "lookup_quote_export_template"
	}
}
