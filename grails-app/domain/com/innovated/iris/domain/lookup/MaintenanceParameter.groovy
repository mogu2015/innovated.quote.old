package com.innovated.iris.domain.lookup

import com.innovated.iris.domain.enums.*
import com.innovated.iris.domain.*

class MaintenanceParameter {
	Date dateFrom = new Date()
	
	VehicleManufacturer make
	VehicleModel model
	MakeModel variant
	
	Double over0km
	Double over20000km
	Double over30000km
	Double over40000km
	
	Date dateCreated
	Date lastUpdated
	
    static constraints = {
    	dateFrom(nullable:false)
    	make(nullable:false)
    	model(nullable:true)
    	variant(nullable:true)
    	over0km(nullable:false)
    	over20000km(nullable:false)
    	over30000km(nullable:false)
    	over40000km(nullable:false)
    }
    
    static mapping = {
		table "lookup_maintenance"
	}
}
