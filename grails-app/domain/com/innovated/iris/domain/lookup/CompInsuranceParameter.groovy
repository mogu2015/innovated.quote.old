package com.innovated.iris.domain.lookup

import com.innovated.iris.domain.enums.*
import com.innovated.iris.domain.*

class CompInsuranceParameter {
	public static final String CLASS_4CYL = "000_4CYL"
	public static final String CLASS_6CYL = "001_6CYL"
	public static final String CLASS_4WD  = "002_4WD"
	public static final String CLASS_V8PR = "003_V8PRESTIGE"
	
	Date dateFrom = new Date()
	String customerCode = Customer.BASE_CUST_CODE
	Supplier defaultSupplier
	String state = "NSW"
	
	// these prices are excluding GST!!
	String type = CLASS_6CYL
	Double metro = 1082.07
	Double regional = 913.96
	Double under25Metro = 1266.87
	Double under25Regional = 1154.18
	
	// these prices are including GST!!
	Double excess = 400.0
	Double under25Excess = 1000.0
	
	Date dateCreated
	Date lastUpdated
	
    static constraints = {
    	dateFrom(nullable:false)
    	customerCode(nullable:false, blank:false)
    	defaultSupplier(nullable:false)
    	state(nullable:false, blank:false)
    	type(nullable:false, blank:false)
    	metro(nullable:false)
    	regional(nullable:false)
    	excess(nullable:false)
    	under25Metro(nullable:false)
    	under25Regional(nullable:false)
    	under25Excess(nullable:false)
    }
    
    static mapping = {
		table "lookup_insurance"
	}
}
