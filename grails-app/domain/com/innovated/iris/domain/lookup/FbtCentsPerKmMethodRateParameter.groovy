package com.innovated.iris.domain.lookup

import com.innovated.iris.domain.*
import com.innovated.iris.domain.enums.*
import com.innovated.iris.server.fbt.*

class FbtCentsPerKmMethodRateParameter {
	Date dateFrom = new Date()
	String customerCode = Customer.BASE_CUST_CODE
	
	Integer taxableValueTypeCode = TaxableValueType.CENTS_PER_KM_MC.getCode()
	Double centsPerKm = 13.0
	
	Date dateCreated
	Date lastUpdated
	
    static constraints = {
    	dateFrom(nullable:false)
    	customerCode(nullable:false, blank:false)
    	taxableValueTypeCode(nullable:false)
    	centsPerKm(nullable:false, range:1.0..100.0)
    }
    
    static mapping = {
		table "lookup_fbt_tv_cents_per_km"
	}
}
