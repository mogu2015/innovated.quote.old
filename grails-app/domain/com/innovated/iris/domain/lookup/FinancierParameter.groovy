package com.innovated.iris.domain.lookup

import com.innovated.iris.domain.enums.*
import com.innovated.iris.domain.*

class FinancierParameter {
	Date dateFrom = new Date()
	String customerCode = Customer.BASE_CUST_CODE
	Supplier defaultSupplier
	Double interestRate = 0.1015
	Double documentationFee = 250.0
	boolean paymentsMadeInAdvance = false
	
	Date dateCreated
	Date lastUpdated
	
    static constraints = {
    	dateFrom(nullable:false)
    	customerCode(nullable:false, blank:false)
    	defaultSupplier(nullable:false)
    	interestRate(nullable:false)
    	documentationFee(nullable:false)
    }
    
    static mapping = {
		table "lookup_financier"
	}
}
