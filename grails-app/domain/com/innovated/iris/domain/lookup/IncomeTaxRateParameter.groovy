package com.innovated.iris.domain.lookup

import java.util.Date;

class IncomeTaxRateParameter {
	Date dateFrom = new Date()
	
	Double lowerValue = 1.0
	Double upperValue = 6000.0
	Double rate = 0.0
	Double amount = 0.0
	Boolean highestRate = false
	
	Date dateCreated
	Date lastUpdated
	
    static constraints = {
    }
    
    static mapping = {
		table "lookup_income_tax"
	}
}
