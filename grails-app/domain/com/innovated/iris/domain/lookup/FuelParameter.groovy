package com.innovated.iris.domain.lookup

import com.innovated.iris.domain.enums.*
import com.innovated.iris.domain.*

class FuelParameter {
	Date dateFrom = new Date()
	String customerCode = Customer.BASE_CUST_CODE
	String state = "NSW"
	Supplier defaultSupplier
	FuelType type
	Double dollarsPerLitre
	Double dollarsPerLitreRegional
	
	Date dateCreated
	Date lastUpdated
	
    static constraints = {
    	dateFrom(nullable:false)
    	customerCode(nullable:false, blank:false)
    	state(nullable:false, blank:false)
    	defaultSupplier(nullable:false)
    	type(nullable:false)
    	dollarsPerLitre(nullable:false, range:0.01..2.0)
    	dollarsPerLitreRegional(nullable:false, range:0.01..2.0)
    }
    
    static mapping = {
		table "lookup_fuel"
	}
}
