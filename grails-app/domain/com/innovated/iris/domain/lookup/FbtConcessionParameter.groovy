package com.innovated.iris.domain.lookup

class FbtConcessionParameter {

    static constraints = {
    }
    
    static mapping = {
		table "lookup_fbt_concession"
	}
}
