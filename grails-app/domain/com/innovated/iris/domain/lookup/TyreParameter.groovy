
package com.innovated.iris.domain.lookup

import com.innovated.iris.domain.enums.*
import com.innovated.iris.domain.*

import java.util.regex.*

class TyreParameter {
	public static final String SIZE_REGEX = "[A-Z]?(\\d{1,3})/(\\d{1,3})\\s?[A-Z]{1,2}(\\d{1,}|\\d{1,}\\.\\d{1,2})(\\s.*)?"
	public static final int GRP_WIDTH = 1
	public static final int GRP_HEIGHT = 2
	public static final int GRP_DIAMETER = 3
	
	Date dateFrom = new Date()
	//String customerCode = Customer.BASE_CUST_CODE
	VehicleManufacturer make
	VehicleModel model
	MakeModel variant
	
	String size
	Double standardPrice
	Double premiumPrice
	Double getWidth()    { extractDimension(size, GRP_WIDTH) }
	Double getHeight()   { extractDimension(size, GRP_HEIGHT) }
	Double getDiameter() { extractDimension(size, GRP_DIAMETER) }
	
	static transients = ['width', 'height', 'diameter']
	
    static constraints = {
    	dateFrom(nullable:false)
    	//customerCode(nullable:false, blank:false)
		make(nullable:false)
		model(nullable:true)
		variant(nullable:true)
    	size(nullable:true, blank:true)
    	standardPrice(nullable:false)
    	premiumPrice(nullable:false)
    }
    
    static mapping = {
		table "lookup_tyres"
	}
    
    private Double extractDimension(String sizeString, int group) {
    	Double result = -1
    	Pattern p = Pattern.compile(SIZE_REGEX)
		Matcher m = p.matcher(sizeString)
    	if (m.matches()) {
			try {
				result = Double.parseDouble(m.group(group))
			} catch (NumberFormatException nfe) {
				result = -1
			}
    	}
    	return result
    }
}
