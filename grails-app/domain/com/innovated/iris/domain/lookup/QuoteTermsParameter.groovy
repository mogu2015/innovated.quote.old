package com.innovated.iris.domain.lookup

import com.innovated.iris.domain.enums.*
import com.innovated.iris.domain.*

class QuoteTermsParameter {
	Date dateFrom = new Date()
	String customerCode = Customer.BASE_CUST_CODE
	
	List<QuoteItemTerms> terms = new ArrayList<QuoteItemTerms>()
	
	Date dateCreated
	Date lastUpdated
	
	static hasMany = [terms:QuoteItemTerms]
	
    static constraints = {
    	dateFrom(nullable:false)
    	customerCode(nullable:false, blank:false)
		terms(nullable:false)
    }
    
    static mapping = {
		table "lookup_quote_terms_conditions"
	}
}
