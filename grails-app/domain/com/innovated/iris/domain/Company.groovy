package com.innovated.iris.domain

import com.innovated.iris.server.fbt.*
import com.innovated.iris.util.*

class Company extends Entity {
	String type = "Company"
	
	String tradingName
	String abn
	String acn
	Company parent
	String employerCode		// unique text string to identify employers when employees register for a user account
	List children = new ArrayList<Company>()
	List contacts = new ArrayList<CompanyContact>()

	// properties specific to quoting
	List allowedFbtCalcMethods = new ArrayList<TaxableValueType>()	// which FBT taxable value calculation methods does this company allow for employees?
	List concessions = new ArrayList<CompanyFbtConcession>()		// track FBT concession history for this company/employer
	Boolean allowsFbtEcm = true		// does this company allows employees to use ECM for FBT?
	Boolean canClaimGst = true		// can this company claim GST credits?
	
	PayrollFrequency defaultPayFreq = PayrollFrequency.MONTHLY		// what this employers pay cycle frequency?
	Double employerShareFee = 0.0		// Employer benefit share defaults (only for AHS's)
	Double employerShareRate = 0.0		// this is stored as a percentage - not a rate!!! ie. 50% = 50... not 0.5!
	
	Date dateCreated
	Date lastUpdated
	
	static hasMany = [
		contacts:CompanyContact, children:Company,
		allowedFbtCalcMethods:TaxableValueType,
		concessions:CompanyFbtConcession
	]
	
	static mapping = {
		allowedFbtCalcMethods joinTable:[name:'company_fbt_tv_types']
    }
	
    static constraints = {
    	tradingName(blank:false)
    	abn(blank:true)
    	acn(blank:true)
    	employerCode(nullable:false, unique:true, blank:false, size:6..32)
    	parent(nullable:true)
    	/*concessions(validator: { val, obj ->
    		if (val.size() < 1) {
    			def now = new Date()
    			println "Company has no concessions identified! Adding a NO_CONCESSION default, dated as at: ${now}"
    			obj.concessions << new CompanyFbtConcession(dateFrom:now, type:FbtConcessionType.NO_CONCESSION)
    		}
    		return true
    	})*/
    }
    
    String toString() {
    	return tradingName
    }
}
