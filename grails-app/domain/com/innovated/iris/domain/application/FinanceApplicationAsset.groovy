package com.innovated.iris.domain.application

import com.innovated.iris.enums.application.FinanceApplicationAssetType;

class FinanceApplicationAsset implements Comparable<FinanceApplicationAsset> {
	FinanceApplicationAssetType type = FinanceApplicationAssetType.CASH_DEPOSIT
	String name = ""	// name is blank by default!
	Double amount = 0.0
	
    static constraints = {
		type(nullable:false)
		name(nullable:false, blank:true)
		amount(nullable:false)
    }
	
	int compareTo(FinanceApplicationAsset asset) {
		int i = this.type.code.compareTo(asset.type.code)
		if (i == 0) {
			return this.name.compareTo(asset.name)
		} else {
			return i
		}
	}
}
