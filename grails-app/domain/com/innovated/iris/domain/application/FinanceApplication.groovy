package com.innovated.iris.domain.application

import java.util.List;

import org.grails.comments.Commentable;

import com.innovated.iris.domain.Quote;
import com.innovated.iris.enums.HomeOwnershipStatus;
import com.innovated.iris.enums.application.FinanceApplicationStatus;

class FinanceApplication implements Commentable {
	// constraints constants
	public static final List<Integer> VALID_YEARS  = 0..99
	public static final List<Integer> VALID_MONTHS = 0..11
	
	// Link to the desired Quote
	Quote quote
	FinanceApplicationStatus status = FinanceApplicationStatus.CREATED
	
	// Applicant specific details
	String applicantTitle = ""
	String applicantFirstName = ""
	String applicantMiddleName = ""
	String applicantLastName = ""
	Date   applicantDateOfBirth
	String applicantMaritalStatus = ""
	String applicantDriversLicenceNum = ""
	String applicantEmail = ""
	String applicantPhoneMobile = ""
	String applicantPhoneWork = ""
	String applicantPhoneHome = ""
	
	// Residential Details
	HomeOwnershipStatus  residenceCurrStatus = HomeOwnershipStatus.MORTGAGE	// "Own", "Mortgage" or "Rent"
	String  residenceCurrThirdPartyName = ""			// Mortage Provider, Landlord, Real Estate or N/A
	String  residenceCurrThirdPartyPhone = ""		// Phone number of party (see above)
	String  residenceCurrAddr1 = ""
	String  residenceCurrAddr2 = ""
	String  residenceCurrAddr3 = ""		// ie. suburb
	String  residenceCurrState = ""
	String  residenceCurrPostcode = ""
	Integer residenceCurrYears = 0		// 0 to 99 years
	Integer residenceCurrMonths = 0		// 0 to 11 months
	String  residencePrevAddr1 = ""
	String  residencePrevAddr2 = ""
	String  residencePrevAddr3 = ""		// ie. suburb
	String  residencePrevState = ""
	String  residencePrevPostcode = ""
	Integer residencePrevYears = 0		// 0 to 99 years
	Integer residencePrevMonths = 0		// 0 to 11 months
	
	// Employment Details
	String  employCurrPosition = ""
	String  employCurrName = ""
	String  employCurrContact = ""
	String  employCurrPhone = ""
	Integer employCurrYears = 0
	Integer employCurrMonths = 0
	String  employPrevName = ""
	String  employPrevContact = ""
	String  employPrevPhone = ""
	Integer employPrevYears = 0
	Integer employPrevMonths = 0
	
	// Financed Asset Details (ie. vehicle or equipment!)
	
	// Quote Details ??
	
	// A&L and I&E Details
	List<FinanceApplicationAsset> assetItems = new ArrayList<FinanceApplicationAsset>()
	List<FinanceApplicationLiability> liabilityItems = new ArrayList<FinanceApplicationLiability>()
	List<FinanceApplicationIncome> incomeItems = new ArrayList<FinanceApplicationIncome>()
	List<FinanceApplicationExpense> expenseItems = new ArrayList<FinanceApplicationExpense>()
	
	static belongsTo = [quote:Quote]
	
	static hasMany = [
		assetItems:     FinanceApplicationAsset,
		liabilityItems: FinanceApplicationLiability,
		incomeItems:    FinanceApplicationIncome,
		expenseItems:   FinanceApplicationExpense
	]
	
    static constraints = {
		quote(nullable:false)
		applicantDateOfBirth(nullable:true)
    }
}
