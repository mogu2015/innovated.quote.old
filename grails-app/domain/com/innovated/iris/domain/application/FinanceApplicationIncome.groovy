package com.innovated.iris.domain.application

import com.innovated.iris.enums.application.FinanceApplicationIncomeType;

class FinanceApplicationIncome implements Comparable<FinanceApplicationIncome> {
	FinanceApplicationIncomeType type = FinanceApplicationIncomeType.NET_SALARY
	String name = ""	// name is blank by default!
	Double amount = 0.0
	
	static constraints = {
		type(nullable:false)
		name(nullable:false, blank:true)
		amount(nullable:false)
	}
	
	int compareTo(FinanceApplicationIncome income) {
		int i = this.type.code.compareTo(income.type.code)
		if (i == 0) {
			return this.name.compareTo(income.name)
		} else {
			return i
		}
	}
}
