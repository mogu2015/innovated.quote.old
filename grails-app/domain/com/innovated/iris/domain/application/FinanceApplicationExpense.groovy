package com.innovated.iris.domain.application

import com.innovated.iris.enums.application.FinanceApplicationExpenseType;

class FinanceApplicationExpense implements Comparable<FinanceApplicationExpense> {
	FinanceApplicationExpenseType type = FinanceApplicationExpenseType.MORTGAGE_RENT
	String name = ""	// name is blank by default!
	Double amount = 0.0
	
	static constraints = {
		type(nullable:false)
		name(nullable:false, blank:true)
		amount(nullable:false)
	}
	
	int compareTo(FinanceApplicationExpense expense) {
		int i = this.type.code.compareTo(expense.type.code)
		if (i == 0) {
			return this.name.compareTo(expense.name)
		} else {
			return i
		}
	}
}
