package com.innovated.iris.domain.application

import com.innovated.iris.enums.application.FinanceApplicationLiabilityType;

class FinanceApplicationLiability implements Comparable<FinanceApplicationLiability> {
	FinanceApplicationLiabilityType type = FinanceApplicationLiabilityType.BANK_LOAN
	String name = ""	// name is blank by default!
	Double amount = 0.0
	
	static constraints = {
		type(nullable:false)
		name(nullable:false, blank:true)
		amount(nullable:false)
	}
	
	int compareTo(FinanceApplicationLiability liability) {
		int i = this.type.code.compareTo(liability.type.code)
		if (i == 0) {
			return this.name.compareTo(liability.name)
		} else {
			return i
		}
	}
}
