package com.innovated.iris.domain

import org.grails.comments.*
import com.innovated.iris.domain.enums.*

class Customer implements Commentable {
	public static final String BASE_CUST_CODE = "000_DEFAULT"
	
	String code
	String supplierLinkCode
	Entity entity	// -> links us back to the "Company" or "Individual"
	
	CustomerType type
	CustomerStatus status
	Date joined = new Date()
	Date ceased
	List assets = new ArrayList()
	
	Date dateCreated
	Date lastUpdated
	
	static hasMany = [assets:Asset]
	
    static constraints = {
    	code(unique:true,nullable:false,blank:false)
    	supplierLinkCode(nullable:true)
    	entity(nullable:false)
    	type(nullable:false)
    	status(nullable:false)
    	joined(nullable:false)
		ceased(nullable:true)
    }
    
    String toString() {
    	return "${code} - ${entity?.name}"
    }
    
    /*def onAddComment = { comment ->
        // post processing logic for newly added comment
    }*/
}
