package com.innovated.iris.domain

import com.innovated.iris.domain.auth.*
import com.innovated.iris.domain.enums.*

class Vehicle extends Asset {
	String driver = "Driver Unknown"
	String vin
	String engineNumber
	Date complianceDate
	
	String colour
	String trim
	
	byte[] photo
	String photoMimeType = "image/png"
	
	Registration registration
	MakeModel makeModel
		
	Date dateCreated
	Date lastUpdated
	
	//static hasOne = [registration:Registration]

    static constraints = {
    	driver(blank:false)
    	vin(blank:true)
    	engineNumber(blank:true)
    	complianceDate(nullable:false)
    	colour(blank:true)
    	trim(blank:true)
    	photo(size:0..10000000)		// 10MB limit on the vehicle photo
    	registration(nullable:true)
    	makeModel(nullable:false)
    }
    
    String toString() {
    	StringBuffer sb = new StringBuffer()
    	if (registration != null) {
    		sb.append("${registration}, ")
    	}
    	sb.append("${driver}, ${makeModel}")
    	return sb.toString()
    }
}
