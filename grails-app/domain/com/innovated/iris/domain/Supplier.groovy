package com.innovated.iris.domain

import org.grails.comments.*
import com.innovated.iris.domain.enums.*

class Supplier implements Commentable {
	String code
	String customerLinkCode
	Entity entity
	String abn
	
	SupplierType type
	Boolean preferred = true	// defaults to TRUE on creation
	Date joined
	Date ceased
	
	Date dateCreated
	Date lastUpdated
	
    static constraints = {
    	code(unique:true,blank:false)
    	customerLinkCode(blank:true)
    	entity(nullable:false)
    	type(nullable:false)
    	preferred()
    	joined(nullable:false)
		ceased(nullable:true)
    }
    
    String toString() {
    	return "${entity?.name}"
    }
    
    def onAddComment = { comment ->
        // post processing logic for newly added comment
    }
}
