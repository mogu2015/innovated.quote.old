package com.innovated.iris.domain

class Email {
	String defaultType
	String address
	boolean allowsHtml = true
	
	Date dateCreated
	Date lastUpdated
	
	static belongsTo = [entity:Entity]
	
    static constraints = {
    	defaultType(blank:false, inList:["To","Cc","Bcc"/*,"Reply-To","Newsgroup","Followup-To"*/])
    	address(blank:false, email:true)
    }
    
    static mapping = {
		table "entity_email"
	}
	
	int compareTo(email) {
		this.address.compareTo(email.address)
	}
	
	String toString() {
    	return address
    }
}
