package com.innovated.iris.domain.finance

import java.util.Date;

import com.domainlanguage.money.Money;
import com.innovated.iris.enums.finance.ModuleType;
import com.innovated.iris.enums.finance.PeriodCode;
import com.innovated.iris.enums.finance.PostingType;

class LedgerTransaction {
	
	// information from source transaction
	String journalNumber
	Long   journalLineId
	String voucherNumber
	
	Date   transactionDate
	String transactionReference
	String transactionText
	Date   documentDate		// can be different from trans date. ie invoice date <=> invoice posting date
	
	Integer accountNumber
	Money   amount
	Boolean crediting = false
	
	// dimension(s)
	
	
	// posting information
	Date postingDate			// basically the "received date", as it applies to costs, etc
	ModuleType moduleType		// identifies which finance module created this transaction
	PostingType postingType		// identifies which finance process posted this transaction
	PeriodCode periodCode
	
	String tenantCode	// a short id/code identifying the "owning entity" of this transaction (when we move to multiple tenants!)
	
	Date dateCreated
	Date lastUpdated
	
    static constraints = {
    }
}
