package com.innovated.iris.domain

import java.text.DecimalFormat;
import java.util.List;
import org.grails.comments.*
import com.innovated.iris.domain.enums.*
import com.innovated.iris.domain.lookup.*

class MakeModel {
    public static
    final List<String> VALID_SHAPES = ["Sedan", "Hatch", "Wagon", "Utility", "Dual-Cab", "Coupe", "Cabriolet", "Convertible", "Van", "Other"]
    public static final List<String> VALID_TRANS = ["Automatic", "Manual", "Sequential", "CVT", "Other"]

    String variant
    String redbookCode = ""
    String glassCode = ""
    String description = ""

    Date buildDate = new Date()
    double engineCapacity = 2.0
    Integer cylinders = 4
    Integer doors = 4
    String shape = ""
    String transmission = ""
    String insuranceClass = CompInsuranceParameter.CLASS_4CYL
    String tyreSize = ""

    FuelType fuelType
    //double fuelConsumptionCity = 10.0
    //double fuelConsumptionHighway = 10.0
    double fuelConsumptionCombined = 10.0

    double retailPrice = 20000.0

    Integer serviceIntervalKms = 10000
    Integer serviceIntervalMths = 6

    String engineConfiguration = ""
    String drivingWheels = ""
    String fueltypestr = ""
    String yearModel = ""

    // List Standard features and Options here...

    String printEngineFuelString() {
        DecimalFormat df = new DecimalFormat("0.0");
        return "${df.format(engineCapacity)}L ${cylinders}cyl ${fuelType.abbreviation}"
    }

    static belongsTo = [model: VehicleModel]

    static constraints = {
        model(nullable: false)
        description(nullable: false, blank: false, unique: true)
        // let's keep these descriptions unique! ie. de-normalized - mainly to help lookups
        variant(blank: true)
        fuelType(nullable: false)
        insuranceClass(nullable: true, blank: true)
        buildDate(nullable: true)
        fuelConsumptionCombined(range: 1.0..20.0)
        retailPrice(blank: false, range: 1.0..1000000.0)    // $RRP
        engineConfiguration(blank: true, nullable: true)

        redbookCode(nullable: true, blank: true)
        glassCode(nullable: true, blank: true)

        engineCapacity(range: 1.0..10.0)
        cylinders(range: 1..16, nullable: true)
        doors(range: 0..7, nullable: true)

        serviceIntervalKms(range: 1000..30000, nullable: true)
        serviceIntervalMths(range: 1..12, nullable: true)
        yearModel(nullable: true)

//    	shape(validator: { val, obj ->
//			if (val == null) return false
//			def input = val.tokenize(" ,")
//			if (input.size() > 0) return input.findAll{ return !MakeModel.VALID_SHAPES.contains(it) }.size() == 0
//			else return true
//		})
//    	transmission(validator: { val, obj ->
//			if (val == null) return false
//			def input = val.tokenize(" ,")
//			if (input.size() > 0) return input.findAll{ return !MakeModel.VALID_TRANS.contains(it) }.size() == 0
//			else return true
//		})
        tyreSize(validator: { val, obj ->
            if ("".equals(val) || val == null) return true
            else return val.matches(TyreParameter.SIZE_REGEX)
        }, nullable: true)
    }

    static mapping = {
        table "make_model"
    }

    void biuldDescription() {
        def combine = []
        def desc = ""
        combine << model?.make?.name << model?.name << variant << drivingWheels << engineConfiguration << engineCapacity << fuelType?.name << transmission << shape
        combine.eachWithIndex { item, index ->
            if (item != "" && item != null) {
                desc += item + " "
            }
        }
        description = desc.substring(0, desc.length() - 1)
    }

    public String getDescription() {
        this.biuldDescription()
        return description
    }

    String toString() {
        return description
        //Make - Model - Type/Variant - Driving Wheels - engine configration - engine Capacity - Fuel Type - Transmission - Shape (vehicle class)
    }
}
