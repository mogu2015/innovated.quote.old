package com.innovated.iris.domain

class Employment {
	Company employer
	Individual employee
	String jobTitle = ""
	String jobDescription = ""
	String staffNumber = ""
	Date startedEmployment
	Date ceasedEmployment
	Double grossSalary
	
	Date dateCreated
	Date lastUpdated
	
    static constraints = {
    	employer(nullable:false)
    	employee(nullable:false)
    	jobTitle(blank:false)
    	jobDescription(blank:true)
    	staffNumber(blank:true)
    	startedEmployment(nullable:true)
    	ceasedEmployment(nullable:true)
    	grossSalary(nullable:true)
    }
    
    String toString() {
    	return "${jobTitle} (${employer})"
    }
}
