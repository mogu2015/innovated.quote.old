package com.innovated.iris.domain

import java.text.*
import com.innovated.iris.domain.enums.*
import com.innovated.iris.domain.lookup.*

class QuoteItemDealer {
	String pageKey = ""
	String name = ""
	Double value = 0.0
	
	static belongsTo = [quote:Quote]
	
    static constraints = {
    	pageKey(blank:true,nullable:false)
    	name(blank:false,nullable:false)
    	value(nullable:false)
    }
    
    String toString() {
    	return "${name}"
    }
}
