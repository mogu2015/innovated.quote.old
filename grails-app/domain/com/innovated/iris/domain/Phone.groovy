package com.innovated.iris.domain

import com.innovated.iris.domain.enums.*

class Phone {
	PhoneType phoneType
	String number
	
	Date dateCreated
	Date lastUpdated
	
	static belongsTo = [entity:Entity]

    static constraints = {
    	phoneType(nullable:false)
    	number(blank:false)
    	dateCreated()
    	lastUpdated()
    }
    
    static mapping = {
		table "entity_phone"
	}
	
	int compareTo(phone) {
		this.number.compareTo(phone.number)
	}
	
	String toString() {
    	return number
    }
}
