package iris

class PopularDomain {

    String domain

    Date dateCreated
    Date lastUpdated


    static mapping = {
        table "popular_domain"
    }

    static constraints = {
        domain (blank: false, nullable: false, unique: true)
    }
}
