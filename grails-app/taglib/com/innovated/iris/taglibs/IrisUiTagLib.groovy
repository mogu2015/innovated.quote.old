package com.innovated.iris.taglibs

import org.grails.comments.Comment;

import com.innovated.iris.domain.Individual;
import com.innovated.iris.enums.Gender;

class IrisUiTagLib {
	static namespace = 'iris'
	
	def sidebarItem = { attrs ->
		attrs.titleCode = attrs.titleCode ?: "default.rcol.title"
		attrs.msgCode = attrs.msgCode ?: "default.rcol.msg"
		out << render(template:"/templates/ui/sidebaritem", model:attrs)
	}
	
	def customerIcon = { attrs ->
		def alt = "Customer record"
		def title = "Customer record"
		def file = "group_error.png"
		
		def customer = attrs.customer
		if (customer) {
			alt = customer.entity.type
			title = customer.entity.type
			if ("Individual" == customer.entity.type) {
				// do additional filtering based on gender
				Individual indiv = (Individual) customer.entity
				if (indiv.gender == Gender.MALE) {
					file = "user.png"
					title = "Male"
				}
				else if (indiv.gender == Gender.FEMALE) {
					file = "user_female.png"
					title = "Female"
				}
				else {
					file = "group_error.png"
					title = "Gender unknown"
				}
			}
			else if ("Company" == customer.entity.type) {
				file = "building.png"
			}
			else {
				title = "Unknown customer class"
			}
		}
		
		file = g.resource(dir:'images/skin', file:file)
		out << "<img width='16' height='16' alt='${alt}' src='${file}' title='${title}'/>"
	}
	
	def renderComment = { attrs ->
		attrs.comment = attrs.comment ?: new Comment()
		out << render(template:"/templates/ui/commentable/comment", model:attrs)
	}
	
	def greyButton = { attrs, body ->
		def model = [
			name: attrs.name ?: "button",
			src: attrs.src ?: g.resource(dir:'images/skin', file:'cog.png'),
			text: attrs.text ?: "Button",
			tip: body() ?: "Click here"
		]
		out << render(template:"/templates/ui/greybutton", model:model)
	}
}
