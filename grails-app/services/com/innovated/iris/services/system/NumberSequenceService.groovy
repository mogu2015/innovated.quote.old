package com.innovated.iris.services.system

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Date;

import org.springframework.transaction.annotation.Transactional;

import com.innovated.iris.domain.system.NumberSequence;
import com.innovated.iris.domain.system.Sequence;
import com.innovated.iris.domain.system.SequenceNext;

class NumberSequenceService {
	def authenticateService
	
    boolean transactional = false
	private Object lock = new Object()
	
	@Transactional
	public String newSequence() {
		//
	}
	
	@Transactional
    public String getNext(String sequenceCode) {
		synchronized (lock) {
			def numSeq = NumberSequence.findByCode(sequenceCode)
			if (numSeq) {
				def principal = authenticateService.principal()
	
				// get the ID of the sequence we're dealing with...
				def tmpSeq = SequenceNext.findByNumberSequence(numSeq)
				if (!tmpSeq) {		// currently no numbers in this sequence, so create the initial one, and obtain lock
					tmpSeq = new SequenceNext(numberSequence:numSeq, number:0, userUpdated:principal.username)
					tmpSeq.save(flush:true)
				}
				def seqId = tmpSeq.id
				//tmpSeq.discard()	// evict nextSequence from session?
				
				// lock the next_sequence counter for this number sequence
				def seqNext = SequenceNext.lock(seqId)
				Long nextNumber = seqNext.number + 1
				seqNext.number = nextNumber
				seqNext.userUpdated = principal.username
				seqNext.save(flush:true)
	
				// save the new number generated
				def seq = new Sequence(numberSequence:numSeq, number:nextNumber, userCreated:principal.username)
				seq.save(flush:true)
	
				// output the generated number sequence code
				DecimalFormat formatter = new DecimalFormat(numSeq.format)
				return formatter.format(nextNumber)
			}
			else {
				throw new RuntimeException("Number Sequence not found for code: ${sequenceCode}")
			}
		}
    }
}
