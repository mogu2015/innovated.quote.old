package com.innovated.iris.services

import grails.plugin.jms.Queue;

class NotificationService {
	static expose = ['jms']

    boolean transactional = false
    def mailService
	def emailConfirmationService

	@Queue
	def email(message) {
    	//println "got EMAIL message: " + message
		try {
			mailService.sendMail {
				to message.to
				from message.from
				subject message.subject
				body(view: message.view, model: message.model)
			}
			
			// notify of successfully sent messages?!?
		}
		catch (Exception e) {
			log.error("Failed to send email ${message}", e)
		}
		return null
    }

	@Queue
	def confirmEmail(message) {
    	try {
			emailConfirmationService.sendConfirmation(message.to, message.subject, message.model, message.token)
			// notify of successfully sent messages?!?
		}
		catch (Exception e) {
			log.error("Failed to send confirmation email ${message}", e)
		}
		return null
    }
	
    @Queue
	def sms(message) {
    	println "TODO: SMS notifications Not Yet Implemented: " + message
		return null
    }
}
