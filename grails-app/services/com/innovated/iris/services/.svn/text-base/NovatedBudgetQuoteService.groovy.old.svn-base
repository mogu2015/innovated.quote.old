package com.innovated.iris.services

import com.innovated.iris.util.*
import com.innovated.iris.domain.*
import com.innovated.iris.domain.enums.*
import com.innovated.iris.domain.lookup.*

class NovatedBudgetQuoteService {
	def parameterLookupService
	
	boolean transactional = true
	
	def lookupParam(clazz, hql, hqlParams) {
		return clazz.find(hql, hqlParams)
	}
	
	def create(params) {
		ParameterLookup lookup = new ParameterLookup()
		def quote = new Quote(params)
		
		if (!quote.hasErrors() && quote.save(flush:true)) {
			//========== Set default date ==========//
			if (quote?.fromDate == null) {
				quote.fromDate = new Date()
			}
			
			//========== DRIVER ADDRESS ==========//
			if (quote?.address == null) {
				//check employee addresses for a street address!
				def employeeAddresses = quote?.employment?.employee?.addresses
				quote.address = employeeAddresses.find {
					"Street Address".equals(it.addressType.name)
				}
			}
			if (quote.address != null) {
				quote.addressString = quote.address.toString()
				quote.stateString = quote.address.state.abbreviation
			}
			
			Date now = (Date) quote.fromDate
			String state = (String) quote.stateString
			
			def hql = "from VehicleRegistrationParameter as v where v.dateFrom<=? and v.customerCode=? and v.state=? order by v.dateFrom desc"
			def lkp = [now, Customer.BASE_CUST_CODE, state]
			//def regoCtp = lookup.lookupParam(VehicleRegistrationParameter, hql, lkp)
			def regoCtp = VehicleRegistrationParameter.find(hql, lkp)
			println "regoCtp = " + regoCtp
			
			quote.save()
			return quote
    	}
    	else {
    		return null
    	}
	}
	
	/**
     *
     *
     */
    def lookupParameters(q) {
    	ParameterLookup lookup = new ParameterLookup()
    	
    	def quote = q
    	
    	// Set default date
    	if (quote?.fromDate == null) {
    		quote.fromDate = new Date()
    	}
    	
    	//==== DRIVER ADDRESS ====//
		if (quote?.address == null) {
			//check employee addresses for a street address!
			def employeeAddresses = quote?.employment?.employee?.addresses
			quote.address = employeeAddresses.find {
				"Street Address".equals(it.addressType.name)
			}
		}
		if (quote.address != null) {
			quote.addressString = quote.address.toString()
			quote.stateString = quote.address.state.abbreviation
		}
		
		//==== CHECK CUSTOMER RECORDS ====//
		def custEmployee = Customer.findWhere(entity: quote?.employment?.employee)
		if (custEmployee == null) {
			quote.errors.reject("quote.employee.not.a.customer", "This employee has no customer record! Please create one first.")
		} else {
			//quote.employeeCode = custEmployee?.code
		}
		def custEmployer = Customer.findWhere(entity: quote?.employment?.employer)
		if (custEmployer == null) {
			quote.errors.reject("quote.employer.not.a.customer", "This employer has no customer record! Please create one first.")
		} else {
			//quote.employerCode = custEmployer?.code
		}
		//println "employer " + custEmployer
		
		//==== VEHICLE RETAIL PRICE ====//
		def retailPrice = quote.vehicle?.retailPrice
		if (retailPrice) {
			quote.purchasePrice = retailPrice
		} else {
			quote.errors.reject("quote.lookup.vehicle.price", "Vehicle retail price lookup failed!")
		}
		
		//==== REGO & CTP ====//
		//def regoCtp = lookupRegoAndCtp(quote, custEmployee, custEmployer)
		/*
		def hql = "from VehicleRegistrationParameter as v where v.dateFrom <= :date and v.customerCode = :code and v.state = :state order by v.dateFrom desc"
		def regoCtp = VehicleRegistrationParameter.find(hql, [date:quote.fromDate, code:Customer.BASE_CUST_CODE, state:quote.stateString])
		*/
		
		
		def hql = "from VehicleRegistrationParameter as v where v.dateFrom<=? and v.customerCode=? and v.state=? order by v.dateFrom desc"
		def lkp = [quote.fromDate, Customer.BASE_CUST_CODE, quote.stateString]
		def regoCtp = lookup.lookupParam(VehicleRegistrationParameter, hql, lkp)
		println "regoCtp = " + regoCtp
		
		if (regoCtp) {
			quote.properties = [
    			annualRegoCost:regoCtp.regoCostTwelveMths, annualCtpCost:regoCtp.ctpCostTwelveMths,
    			regoSupplierId:regoCtp.regoSupplier.id, ctpSupplierId:regoCtp.ctpSupplier.id,
    			regoIncludesGst:regoCtp.regoIncludesGst
    		]
		} else {
			quote.errors.reject("quote.lookup.regoCtp", "Lookup Values for Registration and CTP could not be retrieved!")
		}
		
		/*
		
		//==== FINANCE ====//
    	def fin = lookupFinance(quote, custEmployee, custEmployer)
    	if (fin) {
    		quote.properties = [
    			finSupplierId:fin.defaultSupplier.id, finInterestRate:fin.interestRate,
    			finDocFee:fin.documentationFee, finAdjAmt:0.0, finPmtInAdvance:false, finAmount:0.0
    		]
    	} else {
    		quote.errors.reject("quote.lookup.finance", "Lookup Values for finance calculation could not be retrieved!")
    	}
    	
    	// lookup PSD values/rates
    	def psd = lookupPurchaseStampDuty(quote, custEmployee, custEmployer)
    	if (psd) {
    		quote.properties = [psdRate:psd.lowValue, psdRateLow:psd.lowValue, psdRateHigh:psd.highValue, psdThreshold:psd.thresholdValue, psdAmount:0.0]
    	} else {
    		quote.errors.reject("quote.lookup.psd", "Lookup Values for purchase stamp duty could not be retrieved!")
    	}
    	
    	*/
    	
    	return quote
    }
    
    def lookupRegoAndCtp(date, custEmployee, custEmployer) {
		def lookupDate = quote.fromDate
		if (!lookupDate) { lookupDate = new Date() }
		
		def clos = { params ->
			def hql = "from VehicleRegistrationParameter as v where v.dateFrom <= :date and v.customerCode = :code and v.state = :state order by v.dateFrom desc"
			return VehicleRegistrationParameter.find(hql, params)
		}
		
		//println "inside lookupRegoAndCtp: " + custEmployee
		
		return lookupParameter(clos,
			[date:lookupDate, code:custEmployee.code, state:quote.stateString],
			[date:lookupDate, code:custEmployer.code, state:quote.stateString],
			[date:lookupDate, code:Customer.BASE_CUST_CODE, state:quote.stateString]
		)
	}
    
    private FinancierParameter lookupFinance(quote, custEmployee, custEmployer) {
		def lookupDate = quote.fromDate
		if (!lookupDate) { lookupDate = new Date() }
		
		def clos = { params ->
			def hql = "from FinancierParameter as v where v.dateFrom <= :date and v.customerCode = :code order by v.dateFrom desc"
			return FinancierParameter.find(hql, params)
		}
		
		return lookupParameter(clos,
			[date:lookupDate, code:custEmployee.code],
			[date:lookupDate, code:custEmployer.code],
			[date:lookupDate, code:Customer.BASE_CUST_CODE]
		)
	}
    
    private VehicleStampDutyParameter lookupPurchaseStampDuty(quote, custEmployee, custEmployer) {
		def lookupDate = quote.fromDate
		if (!lookupDate) { lookupDate = new Date() }
		
		def clos = { params ->
			def hql = "from VehicleStampDutyParameter as v where v.dateFrom <= :date and v.customerCode = :code and v.state = :state order by v.dateFrom desc"
			return VehicleStampDutyParameter.find(hql, params)
		}
		
		return lookupParameter(clos,
			[date:lookupDate, code:custEmployee.code, state:quote.stateString],
			[date:lookupDate, code:custEmployer.code, state:quote.stateString],
			[date:lookupDate, code:Customer.BASE_CUST_CODE, state:quote.stateString]
		)
	}
    
    /**
	 * Lookup inherited parameter values based on the criteria passed into this
	 * method.
	 */
    private lookupParameter(closure, p1, p2, p3) {
    	//println "inside lookupParameter: " + p1
    	def result = closure.call(p1)
    	if (!result) {
    		result = closure.call(p2)
    		if (!result) {
    			result = closure.call(p3)
    		}
    	}
    	//println "result = " + result
    	return result
    }
    
}


