/**
 * 
 */
package com.innovated.iris.services.importExport

import com.innovated.iris.domain.application.FinanceApplication;
import com.innovated.iris.services.application.ExportResponse;
import com.innovated.iris.services.application.IFinanceApplicationExporter;

/**
 * @author tamati
 *
 */
class DefaultFinanceApplExporter implements IFinanceApplicationExporter {

	OutputStream out
	String fileName
	
	public DefaultFinanceApplExporter(OutputStream out, String fileName) {
		this.out = out
		this.fileName = fileName
	}
	
	/* (non-Javadoc)
	 * @see com.innovated.iris.services.application.IFinanceApplicationExporter#export(com.innovated.iris.domain.application.FinanceApplication)
	 */
	public ExportResponse export(FinanceApplication application) {
		ExportResponse resp = new ExportResponse(success:true, code:200, message:"OK")
		
		// TODO: do some real processing here!?!...
		 
		return resp;
	}

}
