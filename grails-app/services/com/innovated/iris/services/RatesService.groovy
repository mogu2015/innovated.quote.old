package com.innovated.iris.services

import com.innovated.iris.domain.lookup.RatesParameter

class RatesService {

    boolean transactional = true

	def parameterLookupService


	def getFbtRate(Date date) {
		RatesParameter fbtRate = parameterLookupService.lookupFbtRate(date)
		if (fbtRate) {
			return fbtRate.value
		} else {
			return 0.47		// return 47% as a default!
		}
	}

	def getRebateRate(Date date){
		RatesParameter rebateRate = parameterLookupService.lookupRebateRate(date)
		if (rebateRate) {
			return rebateRate.percentage
		} else {
			return 0.47		// return 47% as a default!
		}
	}

}
