package com.innovated.iris.services

import com.innovated.iris.domain.Company;
import com.innovated.iris.domain.Customer;
import com.innovated.iris.domain.Entity;
import com.innovated.iris.domain.Individual;
import com.innovated.iris.server.fbt.TaxableValueType;

class CustomerService {

    boolean transactional = true
	
	def list(params) {
		log.debug("Listing customers")
		params.max = params.max ? params.max.toInteger() : 10
		return Customer.list(params)
	}
	
	// Factory method for creating new customer instances
    def newCustomer(String type, Entity entity) {
		Customer c = new Customer(joined:new Date())
		if (!entity) {
			if (type == Entity.COMPANY) {
				entity = new Company(allowsFbtEcm:false, canClaimGst:false)
				
				// add default "accepted" FBT calc methods for this company
				((Company) entity).allowedFbtCalcMethods.with {
					add(TaxableValueType.STATUTORY)
					add(TaxableValueType.OP_COST)
					add(TaxableValueType.FBT_EXEMPT)
					add(TaxableValueType.CENTS_PER_KM_MC)
				}
			}
			else {
				entity = new Individual()
			}
		}
		c.entity = entity
    	return c
    }
	
	def save(customer, params) {
		def entity = customer.entity
		if (entity.validate()) {	// validate against domain constraints for the entity (ie. name & type)
			if (!customer.hasErrors()) {	// check for data binding errors
				def custCode = generateCustomerCode(customer?.entity?.name)
				customer.code = custCode
				
				// only proceed to save if both objects validate against domain constraints
				if (customer.validate() && entity.validate()) {
					println "customer valid"
					
					// if the entity is a company, get the parent & save it
					def savedEntity = entity
					if (entity?.type == Entity.COMPANY) {
						//check for a parent company first - it needs to be saved if it exists!
						def parent = Company.get(params.entity.parent.id)
						if (parent) {
							parent.addToChildren(entity)
							savedEntity = parent
						}
					}
					
					// save the entity
					if (!savedEntity.save(flush:true)) {
						log.error("Unable to save linked entity for customer, aborting customer creation")
						throw new RuntimeException("Unable to save linked entity for customer, aborting customer creation")
					}
					
					// save the customer now
					//customer.entity = savedEntity
					if (!customer.save(flush:true)) {
						log.error("Unable to save this customer, aborting customer creation")
						throw new RuntimeException("Unable to save this customer, aborting customer creation")
					}
					
					return customer
				}
			}
		}
		
		// Validation or save errors ocured
		log.debug("Submitted details for new customer account are invalid")
		customer.errors.each {
			log.debug it
		}
		return customer
	}
	
	/*def save(customer, params) {
		//println params
		println "type:${customer?.entity?.type}, name:${customer?.entity?.name}"
		
		// create the entity record first if required!
		def entity = customer?.entity
		if (entity?.id == null) {
			// save the entity first
			println "no entity - saving"
			if (entity?.type == Entity.COMPANY) {
				Company company = (Company) entity
				//check for a parent company first - it needs to be saved if it exists!
				def parent = Company.get(params.entity.parent.id)
				if (parent) {
					parent.addToChildren(company)
					if (company.validate() && parent.save(flush:true)) {
						flash.message = "Company ${companyInstance.id} updated"
						redirect(action:edit, id:companyInstance.id)
					}
					else {
						render(view:'create', model:buildCompanyEditModel(companyInstance))
					}
				}
				else {
					
				}
			}
			else {
				
			}
			//entity.save(flush:true)
		}
		
		//
		
		return true
	}*/
	
	/**
	 * get a valid (and unique) customer code based on <code>name</code>
	 * @param name
	 * @return a unique customer code string
	 */
	public String generateCustomerCode(String name) {
		if (name) {
			def tokens = name.tokenize(' ,;:-_\t\n\r')
			def base = tokens.join().toUpperCase()
			if (base.length() >= 4) {
				base = base.substring(0, 4)
			}
			int i = 1
			String code = "C${base}${i.toString().padLeft(3, '0')}"
			while (Customer.findByCode(code)) {
				i++
				code = "C${base}${i.toString().padLeft(3, '0')}"
			}
			return code
		}
		else {
			return null
		}
	}
}
