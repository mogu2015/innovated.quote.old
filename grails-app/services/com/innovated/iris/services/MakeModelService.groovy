package com.innovated.iris.services

import groovy.sql.Sql;

import java.io.InputStream;
import java.text.DecimalFormat;
import java.text.NumberFormat;

import jxl.Sheet;
import jxl.Workbook;

import org.springframework.web.multipart.MultipartFile;

import com.innovated.iris.domain.*;
import com.innovated.iris.domain.enums.*;
import com.innovated.iris.domain.lookup.*;

class MakeModelService {
	private static final NumberFormat fileSizeFmt = new DecimalFormat("#,###,##0")
	private static final NumberFormat timeTakenFmt = new DecimalFormat("0.000")
    
    boolean transactional = true
	
	def dataSource
	def sessionFactory
	def propertyInstanceMap = org.codehaus.groovy.grails.plugins.DomainClassGrailsPlugin.PROPERTY_INSTANCE_MAP

    def importFile2(MultipartFile file) {
		def firstRowIsHeader = true		// we could eventually pass this in via parameter
		def result = [:]
		
		println "Importing File: " + file.getOriginalFilename()
		println "     Size = " + fileSizeFmt.format(file.getSize()) + " bytes"
		println "     Type = " + file.getContentType()

		long total = 0
		long inserted = 0
		
		long time = System.currentTimeMillis()
		InputStream fis = file.getInputStream()
		Workbook workbook = Workbook.getWorkbook(fis)

		if (workbook.getNumberOfSheets() > 0) {
			// assume the 1st sheet is the one we need
			Sheet sheet = workbook.getSheet(0)

			//def db = new Sql(dataSource)
			def fuelTypes = FuelType.list()
			/*fuelTypes.each {
				println "fuelTypes to check: ${it}"
			}*/

			//def cache = [:]
			long iter = 0
			//(1..100).each {

			int numRows = sheet.getRows()
			int r = (firstRowIsHeader) ? 1 : 0
			for (r; r < numRows; r++) {
				total++
				iter++
				// get our fields
				def makeName = sheet.getCell(2, r).contents
				def modelName = sheet.getCell(3, r).contents
				def description = sheet.getCell(1, r).contents
				
				def exists = MakeModel.findByDescription(description)	// description is unique!
				//def exists = db.firstRow("SELECT m.description FROM make_model AS m WHERE m.description=${description}")
				if (exists) {
					println "${description} already exists, skipping..."
				}
				else {
					println "adding new make model: ${description}"
					
					def model = VehicleModel.find("from VehicleModel as v where v.name=:model and v.make.name=:make", [model:modelName, make:makeName])
					def retailPrice = sheet.getCell(23, r).contents.toDouble()
					
					def fuelType = fuelTypes.find{ it.abbreviation == sheet.getCell(17, r).contents }
					def props = [
						model:model, variant:sheet.getCell(4, r).contents, description:description, shape:sheet.getCell(5, r).contents,
						engineCapacity:sheet.getCell(10, r).contents.toDouble(), cylinders:sheet.getCell(9, r).contents.toInteger(),
						doors:sheet.getCell(6, r).contents.toInteger(), transmission:sheet.getCell(14, r).contents, fuelType:fuelType,
						fuelConsumptionCombined:sheet.getCell(18, r).contents.toDouble(), retailPrice:retailPrice
					]
					
					def v = new MakeModel(props)
					//v.properties = props
					
					model.addToVariants(v)
					if (model.save()) {
						println "VALID: ${description}"
						inserted++
					}
					else {
						println "INVALID: ${description}, ERRORS: ${v.errors}"
					}
				}
				if (r % 10 == 0) cleanUpGorm()		// batch 50 then flush
			}
			//} //temp
			println "iterations: ${iter}"
		}
		workbook.close()
		fis.close()

		def timeTaken = (double) (System.currentTimeMillis() - time) / 1000.0
		def flash = [
			msg:"Total: ${fileSizeFmt.format(total)}, Inserted: ${fileSizeFmt.format(inserted)}, Time taken: ${timeTakenFmt.format(timeTaken)} seconds",
			title:"some title", image:"some image"
		]
		result.flash = flash

		
		return result
    }

	
	private cleanUpGorm() {
		def session = sessionFactory.currentSession
		session.flush()
		session.clear()
		propertyInstanceMap.get().clear()
	}
}
