
package com.innovated.iris.services

import com.innovated.iris.domain.Address
import com.innovated.iris.domain.Entity
import com.innovated.iris.domain.auth.Role;
import com.innovated.iris.domain.auth.User;
import com.innovated.iris.domain.enums.CustomerStatus;
import com.innovated.iris.domain.enums.CustomerType;
import com.innovated.iris.domain.enums.PhoneType;

import com.innovated.iris.domain.Company;
import com.innovated.iris.domain.Customer;
import com.innovated.iris.domain.Email;
import com.innovated.iris.domain.Employment;
import com.innovated.iris.domain.Individual;
import com.innovated.iris.domain.Phone;
import com.innovated.iris.domain.RegisterUserCommand
import iris.PopularDomain
import org.codehaus.groovy.grails.commons.ApplicationHolder
import org.codehaus.groovy.grails.commons.ConfigurationHolder;

class UserService {

    boolean transactional = true

    def authenticateService

    def registerNewUser(RegisterUserCommand cmd, Company employer) {
        // create individual
        Individual i = new Individual(
                name: "${cmd.lastName}, ${cmd.firstName}", firstName: cmd.firstName, middleName: "",
                lastName: cmd.lastName, preferredName: "${cmd.firstName} ${cmd.lastName}")

        i.addToPhones(new Phone(phoneType: PhoneType.findByName("Mobile"), number: "${cmd.mobile}"))
        i.addToEmails(new Email(defaultType: "To", address: "${cmd.email}", allowsHtml: true))

        if (i.validate() && i.save(flush: true)) {
            // create employment record!
            if (employer) {
                def job = new Employment(employer: employer, employee: i, jobTitle: "${cmd.jobRole}").save()
            }

            // create customer
            String cCode = getCustomerCode(i.name)
            CustomerType cType = CustomerType.findByTypeCode("NOV")
            CustomerStatus cStatus = CustomerStatus.findByStatusCode("PC")
            Customer c = new Customer(code: cCode, entity: i, type: cType, status: cStatus, joined: new Date())

            if (c.validate() && c.save(flush: true)) {
                // create user - initially disabled
                def user = new User(username: cmd.userName, userRealName: i.preferredName,
                        passwd: authenticateService.passwordEncoder(cmd.password),
                        email: cmd.email, enabled: false, customer: c,
                        description: "Online Registration - initiated by user")
                if (user.validate() && user.save(flush: true)) {
                    Role.findByAuthority('ROLE_USER').addToPeople(user)
                } else {
                    cmd.errors.reject('registerUserCommand.creation.user', 'Error creating record for new user')
                    throw new RuntimeException("Error creating User record")
                }
            } else {
                cmd.errors.reject('registerUserCommand.creation.customer', 'Error creating record for customer')
                throw new RuntimeException("Error creating Customer record")
            }
        } else {
            cmd.errors.reject('registerUserCommand.creation.individual', 'Error creating record for individual')
            throw new RuntimeException("Error creating Individual record")
        }
        return cmd
    }

    /**
     * get a valid (and unique) customer code based on <code>name</code>
     * @param name
     * @return a unique customer code string
     */
    public String getCustomerCode(String name) {
        def base = name.tokenize(' ,;:-_\t\n\r').join().toUpperCase().substring(0, 4)
        int i = 1
        String code = "C${base}${i.toString().padLeft(3, '0')}"

        while (Customer.findByCode(code)) {
            i++
            code = "C${base}${i.toString().padLeft(3, '0')}"
        }

        return code
    }

    public List getEmployersByCustomerEmail(Customer customer) {
        def result = []

        if (customer) {
            User user = User.findByCustomer(customer)

            result = getEmployersByCustomerEmail(user.email)
        }

        return result
    }

    public List<Company> getEmployersByCustomerEmail(String email) {
        def result = []

        String emailSuffix = email.substring(email.indexOf('@') + 1, email.length())

        def popularDomain = PopularDomain.findByDomain(emailSuffix)

        if(!popularDomain){
            def companyEmailList = Email.findAll("from Email as ee, Entity as e where ee.entity=e and ee.address!='no_email_recorded_on_system@innovated.com.au' and e.type='Company' and ee.address like :description", [description: "%" + emailSuffix])
            companyEmailList.each { objList ->
                for (Object obj : objList) {
                    if (obj instanceof Email) {
                        Company company = Company.get(obj.entity?.id)
                        if (company) {
                            result << company
                        }
                    }
                }
            }
        }
        return result.unique()

    }

    public String makeUri() {
        def grailsApplication = ApplicationHolder.application
        def baseUrl = ConfigurationHolder.config.grails.serverURL
        def serverURL = baseUrl ?: baseUrl + '/' + grailsApplication.metadata.'app.name'
        return serverURL
    }

}
