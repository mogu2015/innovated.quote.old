package com.innovated.iris.services

import com.innovated.iris.util.*
import com.innovated.iris.domain.*
import com.innovated.iris.domain.enums.*
import com.innovated.iris.domain.lookup.*
import com.innovated.iris.server.fbt.*

import com.domainlanguage.base.Ratio;
import com.domainlanguage.money.Money;

class FbtService {
	private static final int GST_RATE = 0
	private static final int FBT_RATE = 1
	private static final int FBT_REBATE_RATE = 2
	
	boolean transactional = true
	
	def parameterLookupService
	def ratesService
	//def taxService
	
	//====== Main service method to return the "FBT Payable" =================//
	def fbtPayable(Date date, FbtConcessionType concessionType, Money taxableValue, boolean canClaimGst) {
		Ratio gstRate = lookupRate(GST_RATE, date)
		Ratio fbtRate = lookupRate(FBT_RATE, date)
		
		GrossUpRate gu = new GrossUpRate(fbtRate, gstRate);
		double gur = (canClaimGst) ? gu.type1() : gu.type2()
		Ratio grossUpRate = Ratio.of(BigDecimal.valueOf(gur));
		
		Money concession = lookupConcessionAmount(concessionType, date)
		
		if (concessionType.isRebatable()) {
			return calculateFbtRebatable(taxableValue, grossUpRate, concession, fbtRate, lookupRate(FBT_REBATE_RATE, date))
		} else {
			return calculateFbt(taxableValue, grossUpRate, concession, fbtRate)
		}
	}
	
	def fbtPayable(Date date, FbtConcessionType concessionType, double taxableValue, boolean canClaimGst) {
		def a = fbtPayable(date, concessionType, Money.valueOf(taxableValue, FbtUtils.DEFAULT_CURRENCY), canClaimGst)
		return a.breachEncapsulationOfAmount().doubleValue()
	}
	
	def postTaxContribution(Date date, Money fbtPayable, boolean canClaimGst) {
		Ratio gstRate = lookupRate(GST_RATE, date)
		Ratio fbtRate = lookupRate(FBT_RATE, date)
		double fbtr = fbtRate.decimalValue(8, FbtUtils.DEFAULT_ROUNDING)

		GrossUpRate gu = new GrossUpRate(fbtRate, gstRate)
		double gur = (canClaimGst) ? gu.type1() : gu.type2()

		return Money.valueOf(fbtPayable.breachEncapsulationOfAmount() / fbtr / gur, FbtUtils.DEFAULT_CURRENCY)
	}
	
	def taxableValue(date, taxableValueType, params) {
		def m = new FbtExemptTaxableValueCalculator()
		switch (taxableValueType) {
			case TaxableValueType.STATUTORY:
				// lookup statutory method percentage
				def tenthMay2011 = new GregorianCalendar(2011, Calendar.MAY, 10).time
				def rate = (params.commenceDate >= tenthMay2011) ? 0.20 : 0.26
				def statRate = parameterLookupService.lookupStatutoryMethodRate(date, params.commenceDate, params.totalKms)
				if (statRate) {
					rate = statRate.rate
				} else {
					println "ERROR looking up StatutoryMethodRateParameter... using default = ${rate}"
				}
				m = new StatutoryMethodTaxableValueCalculator(params.baseValue, rate, params.daysAvailable, params.daysInFbtYear, params.contributions)
				break;
				
			case TaxableValueType.OP_COST:
				m = new OperatingCostMethodTaxableValueCalculator(params.operatingCosts, params.privateUseRate, params.contributions)
				break;
				
			case TaxableValueType.CENTS_PER_KM_MC:
				// lookup cents/km method value
				def centsPerKm = parameterLookupService.lookupCentsPerKmMethodRate(date, taxableValueType)
				def cents = 14.0
				if (centsPerKm)
					cents = centsPerKm.centsPerKm
				else
					println "ERROR looking up FbtCentsPerKmMethodRateParameter... using default = 14.0"
				m = new CentsPerKmTaxableValueCalculator(cents/100, params.totalKms, params.contributions)
				break;
				
			case TaxableValueType.CENTS_PER_UPTO_2500CC:
				// lookup cents/km method value
				def centsPerKm = parameterLookupService.lookupCentsPerKmMethodRate(date, taxableValueType)
				def cents = 42.0
				if (centsPerKm)
					cents = centsPerKm.centsPerKm
				else
					println "ERROR looking up FbtCentsPerKmMethodRateParameter... using default = 42"
				m = new CentsPerKmTaxableValueCalculator(cents/100, params.totalKms, params.contribution)
				break;
				
			case TaxableValueType.CENTS_PER_OVER_2500CC:
				// lookup cents/km method value
				def centsPerKm = parameterLookupService.lookupCentsPerKmMethodRate(date, taxableValueType)
				def cents = 51.0
				if (centsPerKm)
					cents = centsPerKm.centsPerKm
				else
					println "ERROR looking up FbtCentsPerKmMethodRateParameter... using default = 51"
				m = new CentsPerKmTaxableValueCalculator(cents/100, params.totalKms, params.contribution)
				break;
				
			default:	// TaxableValueType.FBT_EXEMPT
				break;
		}
		return m.calculate()
	}
	//========================================================================//
	
	private Ratio lookupRate(int type, Date date) {
		Ratio rate = null
		switch (type) {
			case FBT_REBATE_RATE:
				rate = Ratio.of(ratesService.getRebateRate(date), 1);
				break;
			case FBT_RATE:
				rate = Ratio.of(ratesService.getFbtRate(date), 1);
				break;
			default:	// default is GST_RATE
				rate = Ratio.of(10, 100);
				break;
		}
		return rate
	}
	
	private Money lookupConcessionAmount(FbtConcessionType concessionType, Date date) {
		Money m = Money.valueOf(concessionType.getCap(), FbtUtils.DEFAULT_CURRENCY)
		// lookup from DB...
		return m
	}
	
	private Money calculateFbtRebatable(Money taxableValue, Ratio grossUpRate, Money concessionAmount, Ratio fbtRate, Ratio multiplier) {
		Money grossTax = calculateFbt(taxableValue, grossUpRate, FbtUtils.ZERO_MONEY_AMT, fbtRate)
		Money nonRebatableAmount = calculateFbt(taxableValue, grossUpRate, concessionAmount, fbtRate)
		
		Ratio daysAsRebatableEmployer = Ratio.of(1, 1)
		Money fbtRebate = (grossTax.minus(nonRebatableAmount)).applying(multiplier, 2, FbtUtils.DEFAULT_ROUNDING).applying(daysAsRebatableEmployer, 2, FbtUtils.DEFAULT_ROUNDING)
		
		return grossTax.minus(fbtRebate)
	}
	
	private Money calculateFbt(Money taxableValue, Ratio grossUpRate, Money concessionAmount, Ratio fbtRate) {
		Money m = taxableValue.applying(grossUpRate, 2, FbtUtils.DEFAULT_ROUNDING).minus(concessionAmount);
		if (m.isNegative()) {
			m = FbtUtils.ZERO_MONEY_AMT;
		}
		return m.applying(fbtRate, 2, FbtUtils.DEFAULT_ROUNDING);
	}
	
}
