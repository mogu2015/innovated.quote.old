package com.innovated.iris.services

import com.google.i18n.phonenumbers.PhoneNumberUtil
import com.google.i18n.phonenumbers.Phonenumber
import com.innovated.iris.util.*
import com.innovated.iris.domain.*
import com.innovated.iris.domain.enums.*
import com.innovated.iris.domain.lookup.*
import com.innovated.iris.domain.auth.*;
import com.innovated.iris.enums.QuoteStatus;
import com.innovated.iris.server.fbt.*
import grails.util.Environment
import jinterestrate.*
import org.codehaus.groovy.grails.commons.ConfigurationHolder

import java.text.*
import com.domainlanguage.base.*;
import com.domainlanguage.money.*;
import org.codehaus.groovy.grails.commons.DefaultGrailsDomainClass

class QuoteService implements IQuoteService {
    // injected services & resources
    def authenticateService
    def parameterLookupService
    def insurancePostcodeService
    def messageSource
    //def packageComparisonHandlerService
    def fbtService
    def taxService
    def financeApplicationService

    def userService

    def defaultLocale = java.util.Locale.getDefault()

    boolean transactional = true

    /**
     * This method will get a list of quotes viewable by the currently
     * logged-in user.
     *
     * @return List of quotes viewable by the logged-in user
     */
    def getQuoteList() {
        def isAdmin = authenticateService.ifAnyGranted('ROLE_SUPER, ROLE_ADMIN')
        def principal = authenticateService.principal()
        def userLoggedIn = User.findByUsername(principal.username)

        if (isAdmin) {
            return Quote.list(sort: "lastUpdated", order: "desc")
        } else {
            def customer = userLoggedIn?.customer
            if (customer) {
                if (customer.entity.type == "Individual") {
                    return Quote.findAllByCustomer(customer, [sort: "lastUpdated", order: "desc"])
                } else {
                    return Quote.withCriteria {
                        employment {
                            eq("employer", customer.entity)
                        }
                        order("lastUpdated", "desc")
                    }
                }
            } else {
                return []
            }
        }
    }

    def getQuote(params) {
        def isAdmin = authenticateService.ifAnyGranted('ROLE_SUPER, ROLE_ADMIN')
        def principal = authenticateService.principal()
        def userLoggedIn = User.findByUsername(principal.username)

        def quoteId = params?.id?.toLong()
        if (isAdmin) {
            return Quote.get(quoteId)
        } else {
            def customer = userLoggedIn?.customer
            if (customer) {
                if (customer.entity.type == "Individual") {
                    return Quote.findByIdAndCustomer(quoteId, customer)
                } else {
                    def empList = Quote.withCriteria {
                        eq("id", quoteId)
                        employment { eq("employer", customer.entity) }
                    }
                    return (empList?.size() > 0) ? empList[0] : null
                }
            } else {
                return null
            }
        }
    }

    def newQuote() {
        def isAdmin = authenticateService.ifAnyGranted('ROLE_SUPER, ROLE_ADMIN')
        def principal = authenticateService.principal()
        def userLoggedIn = User.findByUsername(principal.username)

        def q = new Quote()
        if (!isAdmin) {
            def customer = userLoggedIn?.customer
            if (customer) {
                if (customer.entity.type == "Individual") {
                    // pre-populate the quote with the customer
                    q.properties = [customer: customer]
                }
            }
        }
        return q;
    }

    def save(quote, params) {
        def q = update(quote, params, true)
        //changeStatus(q, QuoteStatus.QUOTE_CREATED, "Initial quote created")

        // add an initial (empty) finance application to the quote on first save.
        def finAppl = financeApplicationService.newApplication(q)

        return q
    }

    def update(quote, params, lookup) {
        checkNumericProperties(params, quote)
        if (lookup) {
            parameterLookupService.lookupNovatedAll(quote)
        }
        quote.save(failOnError: true)
        calculate(params, quote, lookup)
        return quote
    }

    def delete(quote) {
        def success = false
        def isAdmin = authenticateService.ifAnyGranted('ROLE_SUPER, ROLE_ADMIN')
        if (isAdmin) {    // only admins can delete!
            try {
                quote.delete(flush: true)
                success = true
            }
            catch (Exception e) {
                // log the exception?
                success = false
            }
        }
        return success
    }

    def clone(quote, params) {
        //
        return quote
    }

    def hasPreferredSelected(quote) {
        def currentSelection = Quote.withCriteria {
//            employment {
//                eq("employer", quote?.employment?.employer)
//            }
            eq("customer", quote.customer)
            or {
                eq("status", QuoteStatus.QUOTE_CONFIRMED)    // "selected"
                eq("status", QuoteStatus.QUOTE_ACCEPTED)
                eq("status", QuoteStatus.QUOTE_APPROVED)
            }
        }
        return (currentSelection.size() > 0 ? currentSelection : false)
    }

    def changeStatus(quote, newStatus, notes) {
        def success = false
        def isAdmin = authenticateService.ifAnyGranted('ROLE_SUPER, ROLE_ADMIN')
        def principal = authenticateService.principal()
        def username = (principal) ? principal.username : "IRIS_SYSTEM"

        Quote.withTransaction { status ->
            // admins can change any state - all others can only
            def currStatus = quote.status

            // are we allowed to change state?
            if (isAdmin || (newStatus.code > currStatus.code && newStatus.code <= QuoteStatus.QUOTE_APPROVED.getCode())) {
                try {
                    // if needed, "de-select" any other quote first!
                    def sels = hasPreferredSelected(quote)
                    if (sels) {
                        sels.each {
                            it.status = QuoteStatus.QUOTE_CREATED
                            it.save(flush: true)
                        }
                    }

                    def sc = new QuoteStateChange(quoteId: quote?.id, oldState: currStatus, newState: newStatus, changedBy: username, comment: notes)
                    quote.status = newStatus
                    if (quote.save() && sc.save()) {
                        // recalc?
//                        calculate([:], quote, false)
                        success = true
                    }
                }
                catch (Exception e) {
                    success = false
                }
            } else {
                success = false
            }

            if (!success) {
                status.setRollbackOnly()
            }
        }
        return success
    }

    def getQuoteExportTemplate(quote) {
        def custEmployer = Customer.findWhere(entity: quote?.employment?.employer)
        if (custEmployer == null) {
            return QuoteExportTemplateParameter.BASE_TMPL
        } else {
            def lookup = parameterLookupService.lookupQuoteExportTemplate(quote.fromDate, custEmployer.code)
            if (lookup) {
                return lookup.templateName?.toString()
            } else {
                return QuoteExportTemplateParameter.BASE_TMPL
            }
        }
    }

    def compactComparisonTypes(quote) {
        def typeList = []
        def types = PackagingComparisonItemType.getTypes()
        types.eachWithIndex { type, idx ->
            boolean atLeastOneNonZeroAmt = false
            quote.comparisons.each {
                def found = it.items.find { it.type == type }
                if (found && (found.amount > 0))
                    atLeastOneNonZeroAmt = true
            }
            if (atLeastOneNonZeroAmt)
                typeList << type
        }
        return typeList
    }

    /**
     * Make sure any params that represent numbers, are cleansed
     * and initiallized properly!!
     */
    def checkNumericProperties(params, quote) {
        def quoteDomain = new DefaultGrailsDomainClass(Quote.class)
        def excludedProps = ["version", "id", "constraints", "metaclass", "errors"]
        def props = quoteDomain.properties.findAll { !excludedProps.contains(it.name) }
        props.each { p ->
            if (Number.class.isAssignableFrom(p.type) || (p.type.isPrimitive() && p.type != boolean.class)) {
                def found = params.find { it.key == p.name }
                if (found && (found.value == null || found.value == "")) {
                    params.(p.name) = 0
                }
            }
        }
        quote.properties = params
    }

    /**
     * This method is to deal with the dynamic lists of vehicle options,
     * dealer and other charges, on the quote input/edit screen(s).
     */
    def handleQuoteAmountItems(includedItems, params, quote, isFirstSave) {
        // FIRST - remove any current option amounts from the quote - then delete them...
        includedItems.each { k, v ->
            def removeFromMethodName = "removeFrom" + k[0].toUpperCase() + k[1..-1]
            (v).findAllWhere(quote: quote).each {
                quote.(removeFromMethodName + "")(it)
                it.delete()
            }
        }

        if (isFirstSave) {
            includedItems.each { k, v ->
                if ("optionItems".equals(k)) {
                    def addToMethodName = "addTo" + k[0].toUpperCase() + k[1..-1]

                    quote.options?.tokenize(",").collect { it.trim() }.each { opt ->
                        quote.(addToMethodName + "")(new QuoteItemOption(name: opt))
                    }
                }
            }
        } else {
            // SECOND - add any values found in the "params" map...
            includedItems.each { k, v ->
                def addToMethodName = "addTo" + k[0].toUpperCase() + k[1..-1]
                def found = params.find { it.key == k }
                if (found) {
                    def items = []
                    found.value.each {
                        if (it.key.isNumber()) {
                            double itemAmt = 0.0
                            try {
                                itemAmt = NumberFormat.getInstance().parse(it.value.value + "").doubleValue()
                            }
                            catch (Exception e) { /*e.printStackTrace()*/
                            }
                            //if (itemAmt != 0) {
                            items << [index: it.key, item: v.newInstance(quote: quote, name: it.value.name, value: itemAmt)]
                            //}
                        }
                    }
                    if (items.size > 0) {
                        Collections.sort(items, { o1, o2 -> o1.index.compareTo(o2.index) } as Comparator)
                        items.eachWithIndex { it, i ->
                            quote.(addToMethodName + "")(it.item)
                        }
                    }
                }
            }
        }
        quote.save(failOnError: true)
    }

    def handleComparisons(params, quote) {
        // firstly, delete any UN-SELECTED comparisons!!!
        def currComparisons = PackageComparison.findAllWhere(quote: quote)
        currComparisons.each {
            if (!it.selected) {
                quote.removeFromComparisons(it)        // remove the association
                it.delete()                            // delete the item
            }
        }

        // don't add any default comparison types *IF* there is already one selected!
        if (quote.comparisons?.size() == 0) {
            // add all valid comparisons for this vehicle
            quote.allowedFbtCalcMethods.eachWithIndex { it, i ->
//                quote.comparisons.add(new PackageComparison(isDefault: (i == 0 && (!quote.allowsFbtEcm || it == TaxableValueType.FBT_EXEMPT)), comparisonOnly: false, usingEcm: false, tvType: it, name: it.name))
//                if (quote.allowsFbtEcm && (it != TaxableValueType.FBT_EXEMPT)) {
//                    quote.comparisons.add(new PackageComparison(isDefault: (i == 0 && quote.allowsFbtEcm), comparisonOnly: false, usingEcm: true, tvType: it, name: "${it.name} (ECM)"))
//                }

                if (quote.allowsFbtEcm) {
                    if (quote.allowsFbtEcm && (it != TaxableValueType.FBT_EXEMPT)) {
                        quote.comparisons.add(new PackageComparison(isDefault: (i == 0 && quote.allowsFbtEcm), comparisonOnly: false, usingEcm: true, tvType: it, name: "${it.name} (ECM)"))
                    }
                } else {
                    quote.comparisons.add(new PackageComparison(isDefault: (i == 0 && (!quote.allowsFbtEcm || it == TaxableValueType.FBT_EXEMPT)), comparisonOnly: false, usingEcm: false, tvType: it, name: it.name))
                }
            }
        }
        // add a "Not packaged" comparison
        quote.comparisons.add(0, new PackageComparison(isDefault: false, comparisonOnly: true, usingEcm: true, tvType: TaxableValueType.FBT_EXEMPT, name: "Not Packaged"))

        // add another comparison for "No Vehicle" packaged! - REMOVED
        //quote.comparisons.add(0, new PackageComparison(isDefault:false, noVehicle:true, comparisonOnly:true, usingEcm:true, tvType:TaxableValueType.FBT_EXEMPT, name:"No Vehicle"))

        // we have to save here or we get NPE in the Package Handler!!
        quote.save(failOnError: true, flush: true)

        // calculate FBT, Benefits, etc
        double noPackageNet = 0
        double bestDiffAmt = 0.0
        int bestDiffIdx = 0
        quote.comparisons.eachWithIndex { it, i ->
            def pch = new DefaultPackageComparisonHandler(it, fbtService, taxService)
            pch.handle()

            // is this comparison the most financially advantageous?
            if (i == 0) {
                noPackageNet = it.items[-1].amount
            } else {
                double diff = it.items[-1].amount - noPackageNet
                if (diff > bestDiffAmt) {   //is this the best salary comparison?
                    bestDiffAmt = diff
                    bestDiffIdx = i
                }
            }
        }

        //println "best diff amt = ${bestDiffAmt}, best idx = ${bestDiffIdx}"

        // finally, flag the best comparison & mark it as selected?!?
        quote.comparisons.eachWithIndex { it, i ->
            it.isBest = (bestDiffIdx == i)
            //it.selected = (bestDiffIdx == i)
        }
    }

    def calculate(params, quote, isFirstSave) {
        def numLeaseYears = quote.leaseTerm / 12
        quote.isRegional = insurancePostcodeService.isRegional(quote.address?.postcode)

        def gstLookup = parameterLookupService.lookupGstRate(quote.fromDate)
        def gstRate = gstLookup ? gstLookup.rate : 0.1
        def invFreq = quote.defaultPayFreq.code

        // sum the option & dealer items first
        handleQuoteAmountItems([optionItems: QuoteItemOption, dealerItems: QuoteItemDealer], params, quote, isFirstSave)
        def optionsTotal = 0.0 + (quote.optionItems.sum { it.value } ?: 0.0)
        def dealerTotal = 0.0 + (quote.dealerItems.sum { it.value } ?: 0.0)

        // work out vehicle price, taxes and base value (FBT)
        quote.purchasePrice = quote.listPrice + optionsTotal + dealerTotal
        quote.purchasePriceGstCalc = quote.purchasePrice / (gstRate * 100)
        quote.purchasePriceGst = quote.purchasePriceGstCalc + quote.purchasePriceGstAdj

        // calculate Luxury Car Tax here...
//        def lctFuelEconomy = (quote?.vehicle?.fuelConsumptionCombined) ?: (quote?.vehicle?.fuelConsumptionCity + quote?.vehicle?.fuelConsumptionHighway) / 2
//        if (lctFuelEconomy > quote.lctFuelConsumption) {
//            def lctDiff = (quote.purchasePrice + quote.purchasePriceGst) - quote.lctThreshold
//            quote.luxuryTaxCalc = (lctDiff > 0) ? lctDiff * (gstRate * 100) / 11 * quote.lctRate : 0.0
//            quote.luxuryTax = quote.luxuryTaxCalc + quote.luxuryTaxAdj
//        } else {
        def lctDiff = (quote.purchasePrice + quote.purchasePriceGst) - quote.lctFuelEfficientThreshold
        quote.luxuryTaxCalc = (lctDiff > 0) ? lctDiff * (gstRate * 100) / 11 * quote.lctFuelEfficientRate : 0.0
        quote.luxuryTax = quote.luxuryTaxCalc + quote.luxuryTaxAdj
//        }

        // sum 'accessories'
        handleQuoteAmountItems([accessoryItems: QuoteItemAccessory], params, quote, isFirstSave)
        def accessoryTotal = 0.0 + (quote.accessoryItems.sum { it.value } ?: 0.0)

        quote.fbtBaseValue = quote.purchasePrice + quote.purchasePriceGst + quote.luxuryTax + accessoryTotal

        // sum 'other costs' - calculating values where applicable
        handleQuoteAmountItems([inclGstItems: QuoteItemInclGst, noGstItems: QuoteItemNoGst], params, quote, isFirstSave)
        if (isFirstSave) {
            // add rego, ctp & stamp duty to all new quotes by default!
            quote.addToInclGstItems(new QuoteItemInclGst(quote: quote, name: "CTP", value: quote.annualCtpCost))
            quote.addToNoGstItems(new QuoteItemNoGst(quote: quote, name: "Registration", value: quote.annualRegoCost))

            // -- stamp duty needs to  be calculated first!!
            quote.psdRate = (quote.stateString == "NSW" && quote.fbtBaseValue > quote.psdThreshold) ? quote.psdRateHigh : quote.psdRateLow
            quote.psdAmount = quote.psdRate * quote.fbtBaseValue
            quote.addToNoGstItems(new QuoteItemNoGst(quote: quote, name: "Purchase Stamp Duty", value: quote.psdAmount))
        }
        def inclGstTotal = 0.0 + (quote.inclGstItems.sum { it.value } ?: 0.0)
        def noGstTotal = 0.0 + (quote.noGstItems.sum { it.value } ?: 0.0)

        // finally, calculate vehicle drive away price & financed amount
        quote.driveAwayPrice = quote.fbtBaseValue + inclGstTotal + noGstTotal

        def priceBefore = quote.vehFinAmount

        def gstClaimThreshold = quote.lctThreshold * (gstRate / (1 + gstRate))
        def lessGst = (quote.purchasePriceGst > gstClaimThreshold) ? gstClaimThreshold : quote.purchasePriceGst
        quote.vehFinAmount = quote.driveAwayPrice - (quote.vehFinAmountInclGst ? 0.0 : lessGst)

        boolean priceChanged = (quote.vehFinAmount != priceBefore)

        // calculate Residual values & percentages
        if (isFirstSave) {
            quote.rvRate = (quote.kmPerYear >= quote.rvHiKmThreshold) ? quote.rvMinHiKm : quote.rvMin
            quote.rvPercentage = quote.rvRate * 100
            quote.rvValue = quote.vehFinAmount * quote.rvRate
            quote.rvAltValue = quote.driveAwayPrice * quote.rvRate

        } else {
            quote.rvRate = quote.rvPercentage / 100
            if (quote.rvChoice == 0/* || priceChanged*/) {    // drive RV value from percentage!
                quote.rvValue = quote.vehFinAmount * quote.rvRate
            } else {
                // leave the value as the user entered it!
            }
            quote.rvAltValue = quote.driveAwayPrice * quote.rvRate
        }
        // round up residuals to the nearest dollar? (this is to mimick the Excel Spreadsheet quote)
        quote.rvValue = Math.ceil(quote.rvValue)
        quote.rvAltValue = Math.ceil(quote.rvAltValue)

        // sum 'financed insurances'
        handleQuoteAmountItems([insuranceItems: QuoteItemInsurance], params, quote, isFirstSave)
        if (isFirstSave) {
            // add the following financed insurances & expenses to new quotes by default!
            def compAmt = (quote.isRegional) ? quote.compInsurRegional : quote.compInsurMetro
            quote.addToInsuranceItems(new QuoteItemInsurance(quote: quote, name: "Comprehensive Insurance", value: compAmt))
            //quote.addToInsuranceItems(new QuoteItemInsurance(quote:quote, name:"GAP Insurance",           value:600.0))
            quote.addToInsuranceItems(new QuoteItemInsurance(quote: quote, name: "Financier Documentation", value: quote.finDocFee))
        }
        quote.insuranceItemsTotal = 0.0 + (quote.insuranceItems.sum { it.value } ?: 0.0)

        // calculate brokerage & include in financed amount!
        if (quote.brokerageChoice == 0) {
            quote.brokerageVehicle = quote.vehFinAmount * quote.brokeragePercent / 100
            quote.brokerageVehicle = Math.ceil(quote.brokerageVehicle)        //==== ROUND-UP to match Excel!! ====//
            quote.brokerageOther = quote.insuranceItemsTotal * quote.brokeragePercent / 100
        }
        quote.brokerageTotal = quote.brokerageVehicle + quote.brokerageOther

        quote.finAmount = quote.vehFinAmount + quote.insuranceItemsTotal + quote.brokerageTotal

        // calculate monthly finance repayment
        def finNumPayments = quote.leaseTerm - quote.finNumDeferredPayments
        /*if (isFirstSave) {
            quote.finNumPayments = quote.leaseTerm
        } else {
            if (quote.finNumPayments < 1) {
                quote.finNumPayments = quote.leaseTerm
            }
        }*/
        double inAdvance = InterestRate.pmt(quote.finInterestRate / 12, finNumPayments, -1.0 * quote.finAmount, quote.rvValue, InterestRate.PRAE_NUMERANDO);
        double inArrears = InterestRate.pmt(quote.finInterestRate / 12, finNumPayments, -1.0 * quote.finAmount, quote.rvValue, InterestRate.POST_NUMERANDO);
        quote.finMthlyPymt = CurrencyUtils.roundNearestCent(((quote.finPmtInAdvance) ? inAdvance : inArrears) + quote.finMthlyPymtAdj)

        double finInsurInAdvance = 0.0
        double finInsurInArrears = 0.0
        if (quote.insuranceItemsTotal != 0) {
            finInsurInAdvance = InterestRate.pmt(quote.finInterestRate / 12, finNumPayments, -1.0 * (quote.insuranceItemsTotal + quote.brokerageOther), 0.0, InterestRate.PRAE_NUMERANDO);
            finInsurInArrears = InterestRate.pmt(quote.finInterestRate / 12, finNumPayments, -1.0 * (quote.insuranceItemsTotal + quote.brokerageOther), 0.0, InterestRate.POST_NUMERANDO);
        }
        quote.finInsurMthlyPymt = CurrencyUtils.roundNearestCent((quote.finPmtInAdvance) ? finInsurInAdvance : finInsurInArrears)
        quote.finVehMthlyPymt = quote.finMthlyPymt - quote.finMthlyPymtAdj - quote.finInsurMthlyPymt
        quote.finMthlyPymtGst = CurrencyUtils.roundNearestCent(quote.finMthlyPymt / (gstRate * 100))

        quote.annualFinanceVehicle = CurrencyUtils.roundUpNearestCent((quote.finVehMthlyPymt * 12) / invFreq) * invFreq
        quote.annualFinanceInsur = CurrencyUtils.roundUpNearestCent((quote.finInsurMthlyPymt * 12) / invFreq) * invFreq
        def tmpFinanceGst = (quote.annualFinanceVehicle + quote.annualFinanceInsur) / (gstRate * 100)
        quote.annualFinanceGst = CurrencyUtils.roundNearestCent(tmpFinanceGst / quote.defaultPayFreq.code) * quote.defaultPayFreq.code

        //println "finVeh = ${quote.annualFinanceVehicle}, finInsur = ${quote.annualFinanceInsur}, finGst = ${quote.annualFinanceGst}"

        // calculate annual maintenance costs
        def totalLeaseKm = quote.kmPerYear * numLeaseYears
        quote.annualMaintenance = CurrencyUtils.roundNearestCent(quote.maintCostPer1000km * ((totalLeaseKm - quote.maintKmAdj) / quote.leaseTerm * 12) / 1000)
        quote.annualMaintenance = CurrencyUtils.roundUpNearestCent(quote.annualMaintenance / quote.defaultPayFreq.code) * quote.defaultPayFreq.code

        // tyres
        def tyresDivisor = quote.leaseTerm
        if (quote.leaseTerm > 12) {
            tyresDivisor -= 6
        }
        //def numTyres = Math.floor(quote.leaseTerm / 12 * quote.kmPerYear / 40000) * 4
        quote.annualTyres = CurrencyUtils.roundUpNearestCent((quote.numTyres * (quote.tyreChoice == 1 ? quote.costPerTyrePremium : quote.costPerTyreStandard)) / tyresDivisor) * 12

        // comp insurance & fuel costs
        if (isFirstSave) {
            if (quote.isRegional) {
                quote.annualCompInsur = (quote.compInsurIsUnder25) ? quote.compInsurUnder25Regional : quote.compInsurRegional
                quote.fuelCostPerLitre = quote.fuelCostPerLitreRegional
            } else {
                quote.annualCompInsur = (quote.compInsurIsUnder25) ? quote.compInsurUnder25Metro : quote.compInsurMetro
                quote.fuelCostPerLitre = quote.fuelCostPerLitreMetro
            }
        }
        def fuelPerMonth = quote.fuelCostPerLitre * quote.kmPerYear * (quote.vehicle.fuelConsumptionCombined * quote.fuelEconomyBuffer * quote.fuelEconomyFactor / 100) / 12
        quote.annualFuel = CurrencyUtils.roundUpNearestCent(fuelPerMonth) * 12

        // get total (vehicle) running costs - used to calculate the TV for Operating Cost Method...
        // finance + maintenance + insurance + fuel + tyres + rego/ctp
        def runningCostsMap = [
                "Finance"                : CurrencyUtils.roundNearestCent(quote.annualFinanceVehicle),
                "Financed Insurance(s)"  : CurrencyUtils.roundNearestCent(quote.annualFinanceInsur),
                "Maintenance"            : CurrencyUtils.roundNearestCent(quote.annualMaintenance),
                "Tyres"                  : CurrencyUtils.roundNearestCent(quote.annualTyres),
                "Fuel"                   : CurrencyUtils.roundNearestCent(quote.annualFuel),
                "Comprehensive Insurance": CurrencyUtils.roundNearestCent(quote.annualCompInsur),
                "Registration"           : CurrencyUtils.roundNearestCent(quote.annualRegoCost + quote.annualCtpCost)
        ]
        def tmpCosts = []
        runningCostsMap.each { tmpCosts << it?.value }

        quote.annualRunningCosts = tmpCosts.sum()

        //def runningCosts = Money.valueOf(quote.annualRunningCosts, FbtUtils.DEFAULT_CURRENCY)
        //println "Total Operating Costs = ${runningCosts}"

        // get total vehicle costs (annual)
        quote.annualTotalCosts = 0.0 + (retrieveBreakdown(quote).sum { it.annually } ?: 0.0)

        //def totalCosts = Money.valueOf(quote.annualTotalCosts, FbtUtils.DEFAULT_CURRENCY)
        //println "Total Vehicle Costs = ${totalCosts}"
        //println "Total Vehicle Costs (incl GST) = ${quote.annualTotalCosts * 1.1}"

        // run/generate packaging benefit comparisons
        handleComparisons(params, quote)

        //println "share rate = ${quote.employerShareRate}"

        // save the sucker
        if(quote.hasErrors()){
            println(quote.errors.toString())
        }

        quote.save(failOnError: true)
    }

    public List retrieveTermsAndConditions(Quote quote) {
        def custEmployer = Customer.findWhere(entity: quote?.employment?.employer)
        def quoteTermsParam = parameterLookupService.lookupQuoteTermsAndConditions(quote.fromDate, custEmployer?.code)
        //println "quote terms: " + quoteTermsParam
        if (quoteTermsParam) {
            //return QuoteItemTerms.findAllWhere(quoteTermsParameter:quoteTermsParam)
            def terms = []
            quoteTermsParam.terms.each {
                it.refresh()
                terms << it
            }
            return terms
        } else {
            return []
        }
    }

    public List retrieveBreakdown(Quote quote) {
        def breakdown = []
        /*if (quote.id) {
            quote.refresh()		// catch any long running sessions...
        }*/

        def freqAmt = 0.0
        def freqGst = 0.0

        // quotation breakdown of FINANCE category
        freqAmt = CurrencyUtils.roundNearestCent(quote.annualFinanceVehicle / quote.defaultPayFreq.code)
        freqGst = CurrencyUtils.roundNearestCent(freqAmt / 10)
        def finAnnualTmp = freqAmt * quote.defaultPayFreq.code
        breakdown.add([
                category   : message("quote.breakdown.table.label.vehicleFinance", null, "Vehicle Finance"),
                annually   : finAnnualTmp,
                monthly    : CurrencyUtils.roundUpNearestCent(finAnnualTmp / 12),
                bimonthly  : CurrencyUtils.roundUpNearestCent(finAnnualTmp / 24),
                fortnightly: CurrencyUtils.roundUpNearestCent(finAnnualTmp / 26),
                weekly     : CurrencyUtils.roundUpNearestCent(finAnnualTmp / 52),
                supplier   : Supplier.get(quote.finSupplierId)?.entity?.name,
                fixedStr   : "Fixed",
                freqGst    : freqGst
        ])

        // quotation breakdown of FINANCED INSURANCE/OTHER COSTS category
        //def finInsurAnnualTmp = CurrencyUtils.roundNearestCent(quote.finInsurMthlyPymt * 12)
        freqAmt = CurrencyUtils.roundNearestCent(quote.annualFinanceInsur / quote.defaultPayFreq.code)
        freqGst = CurrencyUtils.roundNearestCent(freqAmt / 10)
        def finInsurAnnualTmp = freqAmt * quote.defaultPayFreq.code
        breakdown.add([
                category   : message("quote.breakdown.table.label.financedItems", null, "Insurances/Other - Financed"),
                annually   : finInsurAnnualTmp,
                monthly    : CurrencyUtils.roundUpNearestCent(finInsurAnnualTmp / 12),
                bimonthly  : CurrencyUtils.roundUpNearestCent(finInsurAnnualTmp / 24),
                fortnightly: CurrencyUtils.roundUpNearestCent(finInsurAnnualTmp / 26),
                weekly     : CurrencyUtils.roundUpNearestCent(finInsurAnnualTmp / 52),
                supplier   : Supplier.get(quote.finSupplierId)?.entity?.name,
                fixedStr   : "Fixed",
                freqGst    : freqGst
        ])

        // quotation breakdown of MAINTENANCE COSTS category
        freqAmt = CurrencyUtils.roundNearestCent(quote.annualMaintenance / quote.defaultPayFreq.code)
        freqGst = CurrencyUtils.roundNearestCent(freqAmt / 10)
        def maintenanceAnnualTmp = freqAmt * quote.defaultPayFreq.code
        breakdown.add([
                category   : message("quote.breakdown.table.label.maintenance", null, "Maintenance"),
                annually   : maintenanceAnnualTmp,
                monthly    : CurrencyUtils.roundUpNearestCent(maintenanceAnnualTmp / 12),
                bimonthly  : CurrencyUtils.roundUpNearestCent(maintenanceAnnualTmp / 24),
                fortnightly: CurrencyUtils.roundUpNearestCent(maintenanceAnnualTmp / 26),
                weekly     : CurrencyUtils.roundUpNearestCent(maintenanceAnnualTmp / 52),
                supplier   : "",
                fixedStr   : "Budget",
                freqGst    : freqGst
        ])

        // quotation breakdown of TYRE COSTS category
        freqAmt = CurrencyUtils.roundNearestCent(quote.annualTyres / quote.defaultPayFreq.code)
        freqGst = CurrencyUtils.roundNearestCent(freqAmt / 10)
        def tyreAnnualTmp = freqAmt * quote.defaultPayFreq.code
        //def tyresLabel = (quote.numTyres/4 == 1) ? " (1 set of 4)" : " (${quote.numTyres/4} sets of 4)"
        def tyresLabel = " (${quote.numTyres} during lease period)"
        def tyreArgs = new Object[1]
        tyreArgs[0] = tyresLabel
        breakdown.add([
                category   : message("quote.breakdown.table.label.tyres", tyreArgs, "Tyres"),
                annually   : tyreAnnualTmp,
                monthly    : CurrencyUtils.roundUpNearestCent(tyreAnnualTmp / 12),
                bimonthly  : CurrencyUtils.roundUpNearestCent(tyreAnnualTmp / 24),
                fortnightly: CurrencyUtils.roundUpNearestCent(tyreAnnualTmp / 26),
                weekly     : CurrencyUtils.roundUpNearestCent(tyreAnnualTmp / 52),
                supplier   : "",
                fixedStr   : "Budget",
                freqGst    : freqGst
        ])

        // quotation breakdown of FUEL COSTS category
        freqAmt = CurrencyUtils.roundNearestCent(quote.annualFuel / quote.defaultPayFreq.code)
        freqGst = CurrencyUtils.roundNearestCent(freqAmt / 10)
        def fuelAnnualTmp = freqAmt * quote.defaultPayFreq.code
        def fuelArgs = new Object[1]
        fuelArgs[0] = " (${quote.fuelTypeUsed.abbreviation})"
        breakdown.add([
                category   : message("quote.breakdown.table.label.fuel", fuelArgs, "Fuel"),
                annually   : fuelAnnualTmp,
                monthly    : CurrencyUtils.roundUpNearestCent(fuelAnnualTmp / 12),
                bimonthly  : CurrencyUtils.roundUpNearestCent(fuelAnnualTmp / 24),
                fortnightly: CurrencyUtils.roundUpNearestCent(fuelAnnualTmp / 26),
                weekly     : CurrencyUtils.roundUpNearestCent(fuelAnnualTmp / 52),
                supplier   : Supplier.get(quote.fuelSupplierId)?.entity?.name,
                fixedStr   : "Budget",
                freqGst    : freqGst
        ])

        // quotation breakdown of COMP INSURANCE COSTS category
        freqAmt = CurrencyUtils.roundNearestCent(quote.annualCompInsur / quote.defaultPayFreq.code)
        freqGst = CurrencyUtils.roundNearestCent(freqAmt / 10)
        def compInsurAnnualTmp = freqAmt * quote.defaultPayFreq.code
        breakdown.add([
                category   : message("quote.breakdown.table.label.compinsur", null, "Comprehensive Insurance"),
                annually   : compInsurAnnualTmp,
                monthly    : CurrencyUtils.roundUpNearestCent(compInsurAnnualTmp / 12),
                bimonthly  : CurrencyUtils.roundUpNearestCent(compInsurAnnualTmp / 24),
                fortnightly: CurrencyUtils.roundUpNearestCent(compInsurAnnualTmp / 26),
                weekly     : CurrencyUtils.roundUpNearestCent(compInsurAnnualTmp / 52),
                supplier   : Supplier.get(quote.compInsurSupplierId)?.entity?.name,
                fixedStr   : "Budget",
                freqGst    : freqGst
        ])

        // quotation breakdown of REGO (& CTP) category
        freqAmt = CurrencyUtils.roundNearestCent((quote.annualRegoCost + quote.annualCtpCost) / quote.defaultPayFreq.code)
        freqGst = CurrencyUtils.roundNearestCent(freqAmt / 10)
        def regoAnnualTmp = freqAmt * quote.defaultPayFreq.code
        breakdown.add([
                category   : message("quote.breakdown.table.label.rego", null, "Registration"),
                annually   : regoAnnualTmp,
                monthly    : CurrencyUtils.roundUpNearestCent(regoAnnualTmp / 12),
                bimonthly  : CurrencyUtils.roundUpNearestCent(regoAnnualTmp / 24),
                fortnightly: CurrencyUtils.roundUpNearestCent(regoAnnualTmp / 26),
                weekly     : CurrencyUtils.roundUpNearestCent(regoAnnualTmp / 52),
                supplier   : Supplier.get(quote.regoSupplierId)?.entity?.name,
                fixedStr   : "Budget",
                freqGst    : freqGst
        ])

        // quotation breakdown of ROADSIDE COSTS category
        freqAmt = CurrencyUtils.roundNearestCent(quote.annualRoadside / quote.defaultPayFreq.code)
        freqGst = CurrencyUtils.roundNearestCent(freqAmt / 10)
        def roadsideAnnualTmp = freqAmt * quote.defaultPayFreq.code
        breakdown.add([
                category   : message("quote.breakdown.table.label.roadside", null, "Roadside Assistance"),
                annually   : roadsideAnnualTmp,
                monthly    : CurrencyUtils.roundUpNearestCent(roadsideAnnualTmp / 12),
                bimonthly  : CurrencyUtils.roundUpNearestCent(roadsideAnnualTmp / 24),
                fortnightly: CurrencyUtils.roundUpNearestCent(roadsideAnnualTmp / 26),
                weekly     : CurrencyUtils.roundUpNearestCent(roadsideAnnualTmp / 52),
                supplier   : Supplier.get(quote.roadsideSupplierId)?.entity?.name,
                fixedStr   : "Budget",
                freqGst    : freqGst
        ])

        // quotation breakdown of FLEET MANAGEMENT category
        freqAmt = CurrencyUtils.roundNearestCent(quote.annualFeeFltMng / quote.defaultPayFreq.code)
        freqGst = CurrencyUtils.roundNearestCent(freqAmt / 10)
        def feeFltMngAnnualTmp = freqAmt * quote.defaultPayFreq.code
        breakdown.add([
                category   : message("quote.breakdown.table.label.fleetManagement", null, "Management"),
                annually   : feeFltMngAnnualTmp,
                monthly    : CurrencyUtils.roundUpNearestCent(feeFltMngAnnualTmp / 12),
                bimonthly  : CurrencyUtils.roundUpNearestCent(feeFltMngAnnualTmp / 24),
                fortnightly: CurrencyUtils.roundUpNearestCent(feeFltMngAnnualTmp / 26),
                weekly     : CurrencyUtils.roundUpNearestCent(feeFltMngAnnualTmp / 52),
                supplier   : "inNovated Leasing Australia",
                fixedStr   : "Fixed",
                freqGst    : freqGst
        ])

        // quotation breakdown of DETAIL/WASH COSTS category
        freqAmt = CurrencyUtils.roundNearestCent(quote.annualDetailWash / quote.defaultPayFreq.code)
        freqGst = CurrencyUtils.roundNearestCent(freqAmt / 10)
        def detailWashAnnualTmp = freqAmt * quote.defaultPayFreq.code
        breakdown.add([
                category   : message("quote.breakdown.table.label.detailWash", null, "Detail/Wash"),
                annually   : detailWashAnnualTmp,
                monthly    : CurrencyUtils.roundUpNearestCent(detailWashAnnualTmp / 12),
                bimonthly  : CurrencyUtils.roundUpNearestCent(detailWashAnnualTmp / 24),
                fortnightly: CurrencyUtils.roundUpNearestCent(detailWashAnnualTmp / 26),
                weekly     : CurrencyUtils.roundUpNearestCent(detailWashAnnualTmp / 52),
                supplier   : "",
                fixedStr   : "Budget",
                freqGst    : freqGst
        ])

        // quotation breakdown of OTHER COSTS category
        freqAmt = CurrencyUtils.roundNearestCent(quote.annualOther / quote.defaultPayFreq.code)
        freqGst = CurrencyUtils.roundNearestCent(freqAmt / 10)
        def otherAnnualTmp = freqAmt * quote.defaultPayFreq.code
        breakdown.add([
                category   : message("quote.breakdown.table.label.other", null, "Other"),
                annually   : otherAnnualTmp,
                monthly    : CurrencyUtils.roundUpNearestCent(otherAnnualTmp / 12),
                bimonthly  : CurrencyUtils.roundUpNearestCent(otherAnnualTmp / 24),
                fortnightly: CurrencyUtils.roundUpNearestCent(otherAnnualTmp / 26),
                weekly     : CurrencyUtils.roundUpNearestCent(otherAnnualTmp / 52),
                supplier   : "",
                fixedStr   : "Budget",
                freqGst    : freqGst
        ])

        // quotation breakdown of FBT REPORTING category
        freqAmt = CurrencyUtils.roundNearestCent(quote.annualFeeFbtRep / quote.defaultPayFreq.code)
        freqGst = CurrencyUtils.roundNearestCent(freqAmt / 10)
        def feeFbtRepAnnualTmp = freqAmt * quote.defaultPayFreq.code
        breakdown.add([
                category   : message("quote.breakdown.table.label.fbtReporting", null, "FBT"),
                annually   : feeFbtRepAnnualTmp,
                monthly    : CurrencyUtils.roundUpNearestCent(feeFbtRepAnnualTmp / 12),
                bimonthly  : CurrencyUtils.roundUpNearestCent(feeFbtRepAnnualTmp / 24),
                fortnightly: CurrencyUtils.roundUpNearestCent(feeFbtRepAnnualTmp / 26),
                weekly     : CurrencyUtils.roundUpNearestCent(feeFbtRepAnnualTmp / 52),
                supplier   : "inNovated Leasing Australia",
                fixedStr   : "Fixed",
                freqGst    : freqGst
        ])

        // quotation breakdown of ADMIN category
        freqAmt = CurrencyUtils.roundNearestCent(quote.annualFeeAdmin / quote.defaultPayFreq.code)
        freqGst = CurrencyUtils.roundNearestCent(freqAmt / 10)
        def feeAdminAnnualTmp = freqAmt * quote.defaultPayFreq.code
        breakdown.add([
                category   : message("quote.breakdown.table.label.administration", null, "Admin"),
                annually   : feeAdminAnnualTmp,
                monthly    : CurrencyUtils.roundUpNearestCent(feeAdminAnnualTmp / 12),
                bimonthly  : CurrencyUtils.roundUpNearestCent(feeAdminAnnualTmp / 24),
                fortnightly: CurrencyUtils.roundUpNearestCent(feeAdminAnnualTmp / 26),
                weekly     : CurrencyUtils.roundUpNearestCent(feeAdminAnnualTmp / 52),
                supplier   : "inNovated Leasing Australia",
                fixedStr   : "Fixed",
                freqGst    : freqGst
        ])

        // quotation breakdown of BANKING CHARGES category
        freqAmt = CurrencyUtils.roundNearestCent(quote.annualFeeBankChg / quote.defaultPayFreq.code)
        freqGst = CurrencyUtils.roundNearestCent(freqAmt / 10)
        def feeBankChgAnnualTmp = freqAmt * quote.defaultPayFreq.code
        breakdown.add([
                category   : message("quote.breakdown.table.label.bankingCharges", null, "Banking Charges"),
                annually   : feeBankChgAnnualTmp,
                monthly    : CurrencyUtils.roundUpNearestCent(feeBankChgAnnualTmp / 12),
                bimonthly  : CurrencyUtils.roundUpNearestCent(feeBankChgAnnualTmp / 24),
                fortnightly: CurrencyUtils.roundUpNearestCent(feeBankChgAnnualTmp / 26),
                weekly     : CurrencyUtils.roundUpNearestCent(feeBankChgAnnualTmp / 52),
                supplier   : "inNovated Leasing Australia",
                fixedStr   : "Fixed",
                freqGst    : freqGst
        ])

        return breakdown
    }

    private String message(String code, Object[] args, String defaultMsg) {
        return messageSource.getMessage(code, args, defaultMsg, defaultLocale)
    }

    /**
     *
     * @param number
     * @return
     */
    public String phoneNumberFormat(String number) {

        def result = ""
        if ("".equals(number) || number == null) {
            return ""
        }
        try {
            PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance()
            Phonenumber.PhoneNumber swissNumberProto = phoneUtil.parse(number, "AU")
            result = phoneUtil.format(swissNumberProto, PhoneNumberUtil.PhoneNumberFormat.NATIONAL)
        } catch (Exception e) {
            return ""
        }

        return result

    }
    /**
     * generate notification model used by quote status change process
     * @param quote
     * @return
     */
    public HashMap<String, String> generateEmailModel(Quote quote) {
        def result = [:]
        def user = User.findByCustomer(quote.customer)

        if (user) {
            def employerLabel = "Employer"
            def employerName
            def exceptEmployers = ['BUCOGO', 'ASANDNSW', 'PUBLICNSW', 'REBATABLE', 'PBI0001']
            if (!quote.company) {
                employerName = "unknown"
            } else {
                if (exceptEmployers.contains(quote.company?.employerCode)) {
                    employerLabel = "Employer Type"
                }
                employerName = quote.company?.tradingName
            }

            def phones = Phone.findAllByEntity(user.customer?.entity)

            SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy HH:mm:ss")

            result = [
                    fullName     : user?.customer?.entity?.name,
                    employerLabel: employerLabel,
                    employer     : employerName,
                    email        : user?.email,
                    phone        : phones.size() > 0 ? phoneNumberFormat(phones.get(0).number.toString()) : "",
                    uri          : "${userService.makeUri()}/tools/viewquote/${quote.id}".toString().encodeAsHTML(),
                    quoteId      : quote.id,
                    dateCreated  : sdf.format(quote.dateCreated),
                    dateUpdated  : sdf.format(quote.lastUpdated),
                    payCyle      : quote.defaultPayFreq.getName().toString(),
                    purchasePrice: quote.listPrice
            ]
        }

        return result
    }

    private Double getAnnualSavingAmount(Quote quote) {

        def noPackageNet = quote.comparisons.get(0).items[-1]

        def best = PackageComparison.findByQuoteAndIsBest(quote, true)
        def bestPackageNet = best.items[-1]

        def annualSavingAmt = bestPackageNet.amount - noPackageNet.amount

        return annualSavingAmt > 0 ? annualSavingAmt : 0
    }

    /**
     * generate email model used by email send to the customer
     * @param quote
     * @return
     */
    public HashMap generateCustEmailModel(Quote quote) {

        def result = [:]
        def user = User.findByCustomer(quote.customer)

        def individual = Individual.get(user.customer?.entity?.id)
        def annualSaving = getAnnualSavingAmount(quote)
        if (user) {
            result = [
                    firstName        : individual.firstName.toString(),
                    uri              : "${userService.makeUri()}/tools/viewquote/${quote.id}".toString().encodeAsHTML(),
                    quoteId          : quote.id,
                    vehicle          : "${quote?.vehicle?.yearModel} ${quote?.vehicle?.model?.make?.name} ${quote?.vehicle?.model?.name} ${quote?.vehicle?.variant}".toString(),
                    vehFinAmount     : quote.vehFinAmount,
                    grossAnnualSalary: quote.grossAnnualSalary,
                    leaseTerm        : quote.leaseTerm,
                    priceObtained    : quote.priceObtained ? "true" : null,
                    payCyle          : quote.defaultPayFreq.getName().toString(),
                    annualSaving     : annualSaving,
                    overallSaving    : annualSaving * (quote.leaseTerm / 12),
                    purchasePrice    : quote.listPrice
            ]
        }

        return result
    }

    /**
     *
     * @param subject
     * @param template
     * @param quote
     * @return
     */
    public HashMap generateAdminEmailMsg(String subject, String template, Quote quote) {
        def to
        if (Environment.current == Environment.PRODUCTION) {
            to = "clientservices@innovated.com.au"
        } else {
            to = "liulong@shinetechchina.com"
//            to = "neilc@innovated.com.au"
        }
        def msg = [
                to     : to,
                from   : (Environment.current == Environment.PRODUCTION || Environment.current == Environment.TEST) ? "ClientServices@inNovated.com.au" : "452166191@qq.com",
                subject: subject.toString(),
                view   : "/templates/email/${template}".toString(),
                model  : generateEmailModel(quote)
        ]

        return msg
    }
    /**
     *
     * @param subject
     * @param template
     * @param quote
     * @return
     */
    public HashMap generateCustomerEmailMsg(String subject, String template, Quote quote) {
        def to = "liulong@shinetechchina.com"
//        def to = "neilc@innovated.com.au"
        if (Environment.current == Environment.PRODUCTION) {
            def user = User.findByCustomer(quote.customer)
            if (user?.email) {
                to = user.email.toString()
            }
        }
        def msg = [
                to     : to,
                from   : (Environment.current == Environment.PRODUCTION || Environment.current == Environment.TEST) ? "ClientServices@inNovated.com.au" : "452166191@qq.com",
                subject: subject.toString(),
                view   : "/templates/email/${template}".toString(),
                model  : generateCustEmailModel(quote)
        ]

        return msg
    }
}


