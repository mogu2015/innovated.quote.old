package com.innovated.iris.services

import com.innovated.iris.util.*
import com.innovated.iris.domain.*
import com.innovated.iris.domain.enums.*
import com.innovated.iris.domain.lookup.*
import com.innovated.iris.server.fbt.*

import com.domainlanguage.base.Ratio;
import com.domainlanguage.money.Money;

class PackageComparisonHandlerService {

    boolean transactional = true
	
	def fbtService
	def taxService
	def parameterLookupService

    def handle(comparison) {
		println "handling comparison"
		comparison.refresh()	// refresh instance from the DB
		removeItems(comparison)			// remove any comparison items first!

		double tv = fbtService.taxableValue(comparison.quote.fromDate, comparison.tvType,
			[
				totalKms:comparison.quote.kmPerYear, baseValue:comparison.quote.fbtBaseValue, daysAvailable:1,
				daysInFbtYear:1, contributions:0, operatingCosts:comparison.quote.annualRunningCosts,
				privateUseRate:(1.0 - (comparison.quote.businessUsage/100)),
				commenceDate:comparison.quote.fromDate
			]
		).breachEncapsulationOfAmount().doubleValue()
		println "taxable value test = ${tv}"
		
		double m = fbtService.fbtPayable(comparison.quote.fromDate, comparison.quote.concession, tv, comparison.quote.canClaimGst)
		println "FBT payable test = ${m}"
		
		double t = taxService.incomeTaxPayable(comparison.quote.fromDate, comparison.quote.grossAnnualSalary)
		println "Income tax payable test = ${t}"
		
		def r = parameterLookupService.lookupIncomeTaxRate(new Date(), comparison.quote.grossAnnualSalary)
		if (r) {
			println "Rate = ${r.rate}, for salary = ${comparison.quote.grossAnnualSalary}"
		} else {
			println "Rate IS NULL!"
		}
    }
	
	/*def calculateIncomeTax() {
		double tax = 0.0
		def taxRateParameter = parameterLookupService.lookupIncomeTaxRate(date, taxableIncome)
		if (taxRateParameter) {
			println "ERROR looking up IncomeTaxRateParameter... Returning ZERO income tax payable"
			return tax
		}
		double taxableAmt = taxableIncome - taxRateParameter.lowerValue
		tax = taxableAmt * taxRateParameter.rate + taxRateParameter.amount
		return tax
	}*/
	
	private void removeItems(comparison) {
		def currItems = PackageComparisonItem.findAllWhere(comparison:comparison)
		currItems.each {
			comparison.removeFromItems(it)		// remove the association
			it.delete()							// delete the item
		}
	}
}
