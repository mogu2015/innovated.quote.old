package com.innovated.iris.services

import java.util.Date;

import com.innovated.iris.domain.*;
import com.innovated.iris.domain.application.*;
import com.innovated.iris.enums.HomeOwnershipStatus;
import com.innovated.iris.enums.application.*;
import com.innovated.iris.services.application.*;
import com.innovated.iris.services.importExport.*;

class FinanceApplicationService {

	boolean transactional = true
	
	def newApplication(Quote quote) {
		FinanceApplication app = new FinanceApplication()
		//app.status = FinanceApplicationStatus.CREATED
		app.quote = quote
		
		// Applicant specific details
		app.applicantTitle = ""
		app.applicantFirstName = ""
		app.applicantMiddleName = ""
		app.applicantLastName = ""
		app.applicantDateOfBirth
		app.applicantMaritalStatus = ""
		app.applicantDriversLicenceNum = ""
		app.applicantEmail = ""
		app.applicantPhoneMobile = ""
		app.applicantPhoneWork = ""
		app.applicantPhoneHome = ""
		
		// Residential Details
		app.residenceCurrStatus = HomeOwnershipStatus.MORTGAGE	// "Own", "Mortgage" or "Rent"
		app.residenceCurrThirdPartyName = ""			// Mortage Provider, Landlord, Real Estate or N/A
		app.residenceCurrThirdPartyPhone = ""		// Phone number of party (see above)
		app.residenceCurrAddr1 = ""
		app.residenceCurrAddr2 = ""
		app.residenceCurrAddr3 = ""		// ie. suburb
		app.residenceCurrState = ""
		app.residenceCurrPostcode = ""
		app.residenceCurrYears = 0		// 0 to 99 years
		app.residenceCurrMonths = 0		// 0 to 11 months
		app.residencePrevAddr1 = ""
		app.residencePrevAddr2 = ""
		app.residencePrevAddr3 = ""		// ie. suburb
		app.residencePrevState = ""
		app.residencePrevPostcode = ""
		app.residencePrevYears = 0		// 0 to 99 years
		app.residencePrevMonths = 0		// 0 to 11 months
		
		// Employment Details
		app.employCurrPosition = ""
		app.employCurrName = ""
		app.employCurrContact = ""
		app.employCurrPhone = ""
		app.employCurrYears = 0
		app.employCurrMonths = 0
		app.employPrevName = ""
		app.employPrevContact = ""
		app.employPrevPhone = ""
		app.employPrevYears = 0
		app.employPrevMonths = 0
		
		// Add A&L items
		app.addToAssetItems(new FinanceApplicationAsset(type:FinanceApplicationAssetType.CASH_DEPOSIT, name:"", amount:0))
		app.addToAssetItems(new FinanceApplicationAsset(type:FinanceApplicationAssetType.PROPERTY, name:"", amount:0))
		app.addToAssetItems(new FinanceApplicationAsset(type:FinanceApplicationAssetType.PROPERTY, name:"", amount:0))
		app.addToAssetItems(new FinanceApplicationAsset(type:FinanceApplicationAssetType.PROPERTY, name:"", amount:0))
		app.addToAssetItems(new FinanceApplicationAsset(type:FinanceApplicationAssetType.INVESTMENT, name:"", amount:0))
		app.addToAssetItems(new FinanceApplicationAsset(type:FinanceApplicationAssetType.INVESTMENT, name:"", amount:0))
		app.addToAssetItems(new FinanceApplicationAsset(type:FinanceApplicationAssetType.MOTOR_VEHICLE, name:"", amount:0))
		app.addToAssetItems(new FinanceApplicationAsset(type:FinanceApplicationAssetType.PERSONAL_EFFECTS, name:"", amount:0))
		app.addToAssetItems(new FinanceApplicationAsset(type:FinanceApplicationAssetType.SUPERANNUATION, name:"", amount:0))
		app.addToAssetItems(new FinanceApplicationAsset(type:FinanceApplicationAssetType.SHARES_BONDS, name:"", amount:0))
		app.addToAssetItems(new FinanceApplicationAsset(type:FinanceApplicationAssetType.OTHER, name:"", amount:0))
		
		app.addToLiabilityItems(new FinanceApplicationLiability(type:FinanceApplicationLiabilityType.BANK_LOAN, name:"", amount:0))
		app.addToLiabilityItems(new FinanceApplicationLiability(type:FinanceApplicationLiabilityType.MORTGAGE, name:"", amount:0))
		app.addToLiabilityItems(new FinanceApplicationLiability(type:FinanceApplicationLiabilityType.MORTGAGE, name:"", amount:0))
		app.addToLiabilityItems(new FinanceApplicationLiability(type:FinanceApplicationLiabilityType.MORTGAGE, name:"", amount:0))
		app.addToLiabilityItems(new FinanceApplicationLiability(type:FinanceApplicationLiabilityType.LOAN, name:"", amount:0))
		app.addToLiabilityItems(new FinanceApplicationLiability(type:FinanceApplicationLiabilityType.LOAN, name:"", amount:0))
		app.addToLiabilityItems(new FinanceApplicationLiability(type:FinanceApplicationLiabilityType.OVERDRAFT, name:"", amount:0))
		app.addToLiabilityItems(new FinanceApplicationLiability(type:FinanceApplicationLiabilityType.CREDIT_CARD, name:"", amount:0))
		app.addToLiabilityItems(new FinanceApplicationLiability(type:FinanceApplicationLiabilityType.OTHER, name:"", amount:0))
		
		app.addToIncomeItems(new FinanceApplicationIncome(type:FinanceApplicationIncomeType.NET_SALARY, name:"", amount:0))
		app.addToIncomeItems(new FinanceApplicationIncome(type:FinanceApplicationIncomeType.CAR_ALLOWANCE, name:"", amount:0))
		app.addToIncomeItems(new FinanceApplicationIncome(type:FinanceApplicationIncomeType.INVESTMENT, name:"", amount:0))
		app.addToIncomeItems(new FinanceApplicationIncome(type:FinanceApplicationIncomeType.OTHER, name:"", amount:0))
		
		app.addToExpenseItems(new FinanceApplicationExpense(type:FinanceApplicationExpenseType.MORTGAGE_RENT, name:"", amount:0))
		app.addToExpenseItems(new FinanceApplicationExpense(type:FinanceApplicationExpenseType.HOUSEHOLD, name:"", amount:0))
		app.addToExpenseItems(new FinanceApplicationExpense(type:FinanceApplicationExpenseType.CREDIT_CARD, name:"", amount:0))
		app.addToExpenseItems(new FinanceApplicationExpense(type:FinanceApplicationExpenseType.MOTOR_VEHICLE, name:"", amount:0))
		app.addToExpenseItems(new FinanceApplicationExpense(type:FinanceApplicationExpenseType.OTHER, name:"", amount:0))
		
		// save the quote
		quote.addToFinApplications(app)
		quote.save(failOnError:true)
		return app
	}

    def export(FinanceApplication app, String name, OutputStream out) {
		def s = Supplier.get(app.quote.finSupplierId)
		if (!s) {
			throw new IllegalArgumentException("Unknown Financier identified for Finance Application Export!")
			return
		}
		
		IFinanceApplicationExporter exporter
		// lookup the "exporter" based on the financier of the quote
		// TODO: do a proper lookup here in the future!
		switch (s.code) {
			case "MACLEASE":	// the MacLease exporter will send XML direct to Macquarie (via HTTPS)
				// TODO
				break;
			case "CBAFIN":		//
				// TODO
				break;
			default:	// default is a "CSV" exporter...
				exporter = new DefaultFinanceApplExporter(out, name)
				break;
		}
		
		ExportResponse resp = exporter.export(app)
		return resp.success
    }
}
