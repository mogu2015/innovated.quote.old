package com.innovated.iris.services

import com.innovated.iris.util.CurrencyUtils

import com.innovated.iris.util.*
import com.innovated.iris.domain.*
import com.innovated.iris.domain.enums.*
import com.innovated.iris.domain.lookup.*
import com.innovated.iris.server.fbt.*

import com.domainlanguage.base.Ratio
import com.domainlanguage.money.Money;

class TaxService {

    boolean transactional = true

	def parameterLookupService

	public Money incomeTaxPayable(Date date, Money taxableIncome) {
		Money tax = FbtUtils.ZERO_MONEY_AMT
		def taxRateParameter = parameterLookupService.lookupIncomeTaxRate(date, taxableIncome.breachEncapsulationOfAmount().doubleValue())
		if (!taxRateParameter) {
			println "ERROR looking up IncomeTaxRateParameter... Returning ZERO income tax payable"
			return tax
		}
		Money taxableAmt = taxableIncome.minus(Money.valueOf(taxRateParameter.lowerValue, FbtUtils.DEFAULT_CURRENCY))
		tax = taxableAmt.times(taxRateParameter.rate).plus(Money.valueOf(taxRateParameter.amount, FbtUtils.DEFAULT_CURRENCY))

		return CurrencyUtils.roundUpNearestDollar(tax)
    }

	public Double incomeTaxPayable(Date date, double taxableIncome) {
		Money t = incomeTaxPayable(date, Money.valueOf(taxableIncome, FbtUtils.DEFAULT_CURRENCY))

		return t.breachEncapsulationOfAmount().doubleValue()
	}

	def lookupGstRate(Date date) {
		GstRateParameter gst = parameterLookupService.lookupGstRate(date)
		if (gst) {
			return gst.rate
		} else {
			// TODO: log that i couldn't obtain the DB value!!
			return 0.1		// return 10% as a default!
		}
	}

	def medicareLevyPayable(Date date, double taxableIncome) {
		double levyRate = 0.015
		//get medicare levy rate...
		return (taxableIncome * levyRate)
	}

	def medicareLevySurchargePayable(Date date, double amount) {
		double surchargeRate = 0.01
		//get medicare levy surcharge rate, thresholds, etc...
		return (amount * surchargeRate)
	}

	def taxSavingEmployerShare(Date date, double grossSalary, int payFreq, double packagedAmt, double emplShareRate, double emplAdminFee) {
		double emplShareAmt = 0.0;
		double taxPayable = incomeTaxPayable(date, grossSalary) + medicareLevyPayable(date, grossSalary);
		double taxSaving = 1.0;

		while (taxSaving > 0.001) {
			double tmpTI = grossSalary - emplShareAmt - packagedAmt - emplAdminFee
			double tmpTax = incomeTaxPayable(date, tmpTI) + medicareLevyPayable(date, tmpTI);
			taxSaving = taxPayable - tmpTax;
			emplShareAmt = emplShareAmt + (taxSaving * emplShareRate / 100);
			taxPayable = tmpTax;
		}	// doesn't need to be ZERO, any significant figures greater than 1 cent are enough ;)

		return CurrencyUtils.roundUpNearestCent(emplShareAmt / payFreq) * payFreq;
	}

	def accumulatedDepreciation(double presentValue, double residualValue, double annualRate, int periodMths, boolean useDimishingValueMethod) {
		if (useDimishingValueMethod) {
			// Find the number of periods (in years) that it takes to depreciate to the residual value
			//double numYrs = Math.log(residualValue/presentValue) / Math.log(1.0 - annualRate)

			// workout depn using the formula: V = P(1 - r)^n
			double futureValue = presentValue * Math.pow((1.0 - annualRate), ((double) periodMths / 12.0))
			double accumDepn = presentValue - futureValue
			if (accumDepn > (presentValue - residualValue)) {	// we can't depreciate below the residual value!
				accumDepn = presentValue - residualValue
			}
			//println "DV: accumDepn = ${accumDepn}"
			return accumDepn
		}
		else {	// Straight-line method...
			double accumDepn = (presentValue * annualRate) * ((double) periodMths / 12.0)
			if (accumDepn > (presentValue - residualValue)) {	// we can't depreciate below the residual value!
				accumDepn = presentValue - residualValue
			}
			//println "SL: accumDepn = ${accumDepn}"
			return accumDepn
		}
	}
}
