package com.innovated.iris.services.finance

import org.springframework.transaction.annotation.Transactional;

import com.innovated.iris.enums.finance.PostingResult;
import com.innovated.iris.server.finance.Voucher;

class VoucherPostingService implements IPostingService<Voucher> {

    boolean transactional = false

	/**
	 * 
	 * @param voucher
	 * @return
	 */
	private PostingResult validateVoucher(Voucher voucher) throws RuntimeException {
		// TODO do voucher validation here!!!
		
		// check that voucher balance is zero
		// check that accounts (from & to) are valid
		// check validity of dimensions
		// validate user
		// validate period
		// voucher number seq. validation
		// perform any other checks!...
		
		return null;
	}
	
	@Transactional
	public PostingResult validate(Voucher voucher) {
		def result = validateVoucher(voucher)
		return result
	}
	
	@Transactional
	public PostingResult post(Voucher voucher) {
		// check voucher validity first
		def validity = validateVoucher(voucher)
		if (true) {
			// valid voucher so try to post it!
			
			// don't forget the TransactionLog (audit trail) records creation!
		}
		return validity;
	}
}
