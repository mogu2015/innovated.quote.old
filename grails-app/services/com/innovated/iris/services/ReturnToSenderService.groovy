package com.innovated.iris.services

import com.google.i18n.phonenumbers.PhoneNumberUtil
import com.google.i18n.phonenumbers.Phonenumber
import grails.util.Environment
import org.springframework.transaction.annotation.Transactional;

import com.innovated.iris.domain.Employment;
import com.innovated.iris.domain.auth.User;

class ReturnToSenderService {

	def jmsService

    boolean transactional = false

	@Transactional
    def confirmed(String email, String uid) {
		log.info("User with id $uid has confirmed their email address $email")

		// the user has confirmed there email, so enable their user account! uid == userName
		def user = User.findByUsername(uid)
		if (user) {
			user.enabled = true
			if (user.save(flush:true)) {
				def employ = Employment.findByEmployee(user?.customer?.entity)
				def employer = (employ) ? employ.employer?.name : "Please check on IRIS..."
				def phones = user?.customer?.entity?.phones
				def phoneNumber = phones.size() > 0 ? phones.get(0)?.number : ""
				PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();
				Phonenumber.PhoneNumber swissNumberProto = phoneUtil.parse(phoneNumber, "AU");
				def number = phoneUtil.format(swissNumberProto, PhoneNumberUtil.PhoneNumberFormat.NATIONAL)

				def msg = [
					to: Environment.current == Environment.PRODUCTION ? "clientservices@innovated.com.au" : "liulong@shinetechchina.com",
					from: "iris@innovated.com.au",
					subject: "New User Registered!",
					view: "/templates/email/userRegoNotifyStaff",
					model: [userName:uid, fullName:user?.customer?.entity?.name, employer:employer, email:user?.email, phone: number.toString()]
				]
				jmsService.send([service:'notification', method:'email'], msg)
			}
		}
		return [controller:'user', action:'home']
    }

	def invalid(String uid) {
		log.warn("Confirmation failed for User with id $uid")
		return [controller:'user', action:'confirmFailed']
	}

	def timedOut(String email, String uid) {
		log.warn("User with id $uid failed to confirm email address after 30 days")
	}
}
