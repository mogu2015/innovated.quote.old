package com.innovated.iris.services

import com.innovated.iris.util.*
import com.innovated.iris.domain.*
import com.innovated.iris.domain.enums.*
import com.innovated.iris.domain.lookup.*
import com.innovated.iris.server.fbt.*

import grails.validation.ValidationException as VE

class ParameterLookupService {

    boolean transactional = true

    def echoTest(s) {
        return "ParameterLookupService echoed: " + s
    }

    def lookupNovatedAll(quote) {
        def hql = ""
        def queryParamList = null
        def lookedUp = null

        // lookup input address to get STATE string
        quote.stateString = quote.address?.state?.abbreviation
        quote.addressString = quote.address?.toString()

        //==== VEHICLE RETAIL PRICE ====//
        if (quote?.listPrice && quote?.listPrice > 0) {
            // keep the list price - used vehicle??
        } else {
            def retailPrice = quote.vehicle?.retailPrice
            if (retailPrice) {
                quote.listPrice = ((double) retailPrice) * 10.0 / 11.0
            } else {
                quote.errors.reject("quote.lookup.vehicle.price", "Vehicle retail price lookup failed!")
            }
        }

        //=== CHECK CUSTOMER RECORDS ===//
//        def custEmployee = Customer.findWhere(entity: quote?.employment?.employee)
//        if (custEmployee == null) {
//            quote.errors.reject("quote.employee.not.a.customer", "This employee has no customer record! Please create one first.")
//        }
//        def custEmployer = Customer.findWhere(entity: quote?.employment?.employer)
//        if (custEmployer == null) {
//            quote.errors.reject("quote.employer.not.a.customer", "This employer has no customer record! Please create one first.")
//        }

        def custEmployee = Customer.findByEntity(quote?.customer?.entity)
        if (custEmployee == null) {
            quote.errors.reject("quote.employee.not.a.customer", "This employee has no customer record! Please create one first.")
        }
        def custEmployer = Customer.findByEntity(Entity.get(quote?.company?.id))

        if (custEmployer == null) {
            quote.errors.reject("quote.employer.not.a.customer", "This employer has no customer record! Please create one first.")
        }

        //=== Lookup Rego & CTP ===//
        lookedUp = lookupRegoCtp(quote, custEmployee?.code, custEmployer?.code)
        if (lookedUp) {
            quote.properties = [
                    annualRegoCost : lookedUp.regoCostTwelveMths, annualCtpCost: lookedUp.ctpCostTwelveMths,
                    regoSupplierId : lookedUp.regoSupplier.id, ctpSupplierId: lookedUp.ctpSupplier.id,
                    regoIncludesGst: lookedUp.regoIncludesGst
            ]
        } else {
            //println "error getting regoCtp params"
            quote.errors.reject("quote.lookup.regoCtp", "Lookup Values for Registration and CTP could not be retrieved!")

        }

        //==== lookup PSD values/rates ====//
        lookedUp = lookupPurchaseStampDuty(quote, custEmployee?.code, custEmployer?.code)
        if (lookedUp) {
            quote.properties = [
                    psdRate    : lookedUp.lowValue, psdRateLow: lookedUp.lowValue, psdAmount: 0.0,
                    psdRateHigh: lookedUp.highValue ? lookedUp.highValue : 0, psdThreshold: lookedUp.thresholdValue ? lookedUp.thresholdValue : 0
            ]
        } else {
            quote.errors.reject("quote.lookup.psd", "Lookup Values for purchase stamp duty could not be retrieved!")
        }

        //==== lookup Luxury Car Tax values/rates ====//
        lookedUp = lookupLuxuryCarTax(quote, custEmployee?.code, custEmployer?.code)
        if (lookedUp) {
            quote.properties = [
                    lctRate             : lookedUp.rate, lctThreshold: lookedUp.threshold,
                    lctFuelConsumption  : lookedUp.fuelConsumption, lctCreditThreshold: lookedUp.creditThreshold,
                    lctFuelEfficientRate: lookedUp.fuelEfficientRate, lctFuelEfficientThreshold: lookedUp.fuelEfficientThreshold
            ]
        } else {
            quote.errors.reject("quote.lookup.lct", "Lookup Values for luxury car tax could not be retrieved!")
        }

        //==== lookup Luxury Car ALLOWANCE values/rates ====//
        lookedUp = lookupLuxuryCarAllowance(quote)
        if (lookedUp) {
            quote.properties = [
                    luxCompanyTaxRate: lookedUp.companyTaxRate, luxDvThreshold: lookedUp.dvThreshold,
                    luxDvRateLow     : lookedUp.dvRateLow, luxDvRateHigh: lookedUp.dvRateHigh, luxSlRate: lookedUp.slRate
            ]
        } else {
            quote.errors.reject("quote.lookup.lux", "Lookup Values for luxury car ALLOWANCE (not LCT) could not be retrieved!")
        }

        //==== RV rates & thresholds, etc ====//
        lookedUp = lookupResidualValue(quote, custEmployee?.code, custEmployer?.code)
        if (lookedUp) {
            quote.properties = [
                    rvMin: lookedUp.min, rvMax: lookedUp.max, rvMinHiKm: lookedUp.minHiKm, rvHiKmThreshold: lookedUp.hiKmThreshold
            ]
        } else {
            quote.errors.reject("quote.lookup.residual", "Lookup Values for Residual Value calculations could not be retrieved!")
        }

        //==== Fee's & brokerage ====//
        lookedUp = lookupFeesBrokerage(quote, custEmployee?.code, custEmployer?.code)
        if (lookedUp) {
            quote.properties = [
                    brokeragePercent: lookedUp.brokeragePercent, annualFeeFltMng: lookedUp.annualFeeFltMng, annualFeeFbtRep: lookedUp.annualFeeFbtRep,
                    annualFeeAdmin  : lookedUp.annualFeeAdmin, annualFeeBankChg: lookedUp.annualFeeBankChg
            ]
        } else {
            quote.errors.reject("quote.lookup.feesBrokerage", "Lookup Values for Fees & Brokerage calculations could not be retrieved!")
        }

        //==== FINANCE ====//
        lookedUp = lookupFinance(quote, custEmployee?.code, custEmployer?.code)
        if (lookedUp) {
            quote.properties = [
                    finSupplierId: lookedUp.defaultSupplier.id, finInterestRate: lookedUp.interestRate,
                    finDocFee    : lookedUp.documentationFee, finAdjAmt: 0.0, finPmtInAdvance: false, finAmount: 0.0
            ]
        } else {
            quote.errors.reject("quote.lookup.finance", "Lookup Values for finance calculation could not be retrieved!")
        }

        //==== MAINTENANCE ====//
        lookedUp = lookupMaintenance(quote)
        if (lookedUp) {
            //println "Maintenance: ${lookedUp.over0km}, ${lookedUp.over20000km}, ${lookedUp.over30000km}, ${lookedUp.over40000km}"
            def maintenanceCostTmp = lookedUp.over0km
            if (quote.kmPerYear >= 40000)
                maintenanceCostTmp = lookedUp.over40000km
            else if (quote.kmPerYear >= 30000)
                maintenanceCostTmp = lookedUp.over30000km
            else if (quote.kmPerYear >= 20000)
                maintenanceCostTmp = lookedUp.over20000km
            quote.properties = [maintCostPer1000km: maintenanceCostTmp]
        } else {
            quote.errors.reject("quote.lookup.maintenance", "Lookup Values for maintenance calculation could not be retrieved!")
        }

        //==== TYRES ====//
        lookedUp = lookupTyres(quote, custEmployee?.code, custEmployer?.code)
        if (lookedUp) {
            def numTyres = Math.floor(quote.leaseTerm / 12 * quote.kmPerYear / 40000) * 4
            quote.properties = [numTyres: numTyres, costPerTyreStandard: lookedUp.standardPrice, costPerTyrePremium: lookedUp.premiumPrice]
        } else {
            quote.errors.reject("quote.lookup.tyres", "Lookup Values for tyres calculation could not be retrieved!")
        }

        //==== FUEL ====//
        quote.fuelTypeUsed = quote.vehicle?.fuelType
        quote.fuelEconomyFactor = quote.fuelTypeUsed?.economyFactor
        lookedUp = lookupFuel(quote, custEmployee?.code, custEmployer?.code)
        if (lookedUp) {
            quote.properties = [
                    fuelSupplierId: lookedUp.defaultSupplier.id, fuelCostPerLitreMetro: lookedUp.dollarsPerLitre, fuelCostPerLitreRegional: lookedUp.dollarsPerLitreRegional
            ]
        } else {
            quote.errors.reject("quote.lookup.fuel", "Lookup Values for fuel calculation could not be retrieved!")
        }

        //==== COMP INSURANCE ====//
        lookedUp = lookupCompInsurance(quote, custEmployee?.code, custEmployer?.code)
        if (lookedUp) {
            quote.properties = [
                    compInsurSupplierId  : lookedUp.defaultSupplier.id, compInsurMetro: lookedUp.metro, compInsurRegional: lookedUp.regional,
                    compInsurUnder25Metro: lookedUp.under25Metro, compInsurUnder25Regional: lookedUp.under25Regional
            ]
        } else {
            quote.errors.reject("quote.lookup.compInsurance", "Lookup Values for comprehensive insurance calculation could not be retrieved!")
        }

        //==== ROADSIDE ====//
        lookedUp = lookupRoadside(quote, custEmployee?.code, custEmployer?.code)
        if (lookedUp) {
            quote.properties = [
                    roadsideSupplierId: lookedUp.defaultSupplier.id, annualRoadside: lookedUp.annualFee,
                    roadsideJoiningFee: lookedUp.joiningFee, roadsideAnnualFee: lookedUp.annualFee
            ]
        } else {
            quote.errors.reject("quote.lookup.compInsurance", "Lookup Values for comprehensive insurance calculation could not be retrieved!")
        }

        // .... other lookups here ... //

        //==== quote comparisons ====//
        def employer = quote?.company
        if (employer) {

//            def exceptEmployers = ['BUCOGO','ASANDNSW','PUBLICNSW','REBATABLE','PBI0001']
            // Clone the TV types that the employer allows
            employer.allowedFbtCalcMethods.each {
                quote.allowedFbtCalcMethods.add(TaxableValueType.get(it.getCode()))
            }
            quote.concession = employer.concessions.max { it.dateFrom }.type
            //get the employers current concession
            quote.allowsFbtEcm = employer.allowsFbtEcm
            quote.canClaimGst = employer.canClaimGst
//            if(!exceptEmployers.contains(employer.employerCode )){
//                quote.defaultPayFreq = employer.defaultPayFreq
//            }
            quote.employerShareFee = employer.employerShareFee
            quote.employerShareRate = employer.employerShareRate

        } else {
            quote.errors.reject("quote.lookup.employer", "Employer was null!")
        }

        quote.validate()
        if (quote.hasErrors()) {
            //println "ERRORS IN LOOKUP SERVICE!"
            throw new VE("The quote contained errors!", quote.errors)
        }
    }

    //==========================================================================
    // Private Lookup Methods
    //==========================================================================

    def lookupQuoteTermsAndConditions(fromDate, employerCode) {
        def hql = "from QuoteTermsParameter as v where v.dateFrom<=? and v.customerCode=? order by v.dateFrom desc"
        def queryParamList = [
                [fromDate, employerCode],
                [fromDate, Customer.BASE_CUST_CODE]
        ]
        return lookupParam(QuoteTermsParameter, hql, queryParamList)
    }

    def lookupQuoteExportTemplate(fromDate, employerCode) {
        def hql = "from QuoteExportTemplateParameter as v where v.dateFrom<=? and v.customerCode=? order by v.dateFrom desc"
        def queryParamList = [
                [fromDate, employerCode],
                [fromDate, Customer.BASE_CUST_CODE]
        ]
        return lookupParam(QuoteExportTemplateParameter, hql, queryParamList)
    }

    def lookupGstRate(fromDate) {
        def hql = "from GstRateParameter as v where v.dateFrom<=? order by v.dateFrom desc"
        def queryParamList = [
                [fromDate]
        ]
        return lookupParam(GstRateParameter, hql, queryParamList)
    }

    def lookupFbtRate(fromDate){
        def hql = "from RatesParameter as v where v.dateFrom<=? and v.rateType='FBT' order by v.dateFrom desc"
        def queryParamList = [
                [fromDate]
        ]
        return lookupParam(RatesParameter, hql, queryParamList)
    }

    def lookupRebateRate(fromDate){
        def hql = "from RatesParameter as v where v.dateFrom<=? and v.dateTo>=? and v.rateDesc='Rebatable' order by v.dateFrom desc"
        def queryParamList = [
                [fromDate, fromDate]
        ]
        return lookupParam(RatesParameter, hql, queryParamList)
    }

    def lookupIncomeTaxRate(fromDate, income) {
        //Integer amt = income.intValue()
        def hql = "from IncomeTaxRateParameter as v where v.dateFrom<=? and (v.lowerValue<=? and v.upperValue>=?) order by v.dateFrom desc"
        def queryParamList = [
                [fromDate, (double) income, (double) income]
        ]
        return lookupParam(IncomeTaxRateParameter, hql, queryParamList)
    }

    def lookupStatutoryMethodRate(fromDate, commenceDate, kms) {
        def hql = "from FbtStatutoryMethodRateParameter as v where v.dateFrom<=? and (v.commenceStart<=? and v.commenceEnd>=?) and (v.lowerValue<=? and v.upperValue>=?) order by v.dateFrom desc"
        def queryParamList = [
                [fromDate, commenceDate, commenceDate, (double) kms, (double) kms]
        ]
        return lookupParam(FbtStatutoryMethodRateParameter, hql, queryParamList)
    }

    def lookupCentsPerKmMethodRate(fromDate, tvType) {
        def hql = "from FbtCentsPerKmMethodRateParameter as v where v.dateFrom<=? and v.taxableValueTypeCode=? order by v.dateFrom desc"
        def queryParamList = [
                [fromDate, tvType.getCode()]
        ]
        return lookupParam(FbtCentsPerKmMethodRateParameter, hql, queryParamList)
    }

    def lookupRegoCtp(quote, employeeCode, employerCode) {
        def hql = "from VehicleRegistrationParameter as v where v.dateFrom<=? and v.customerCode=? and v.state=? order by v.dateFrom desc"
        def queryParamList = [
                [quote.fromDate, employeeCode, quote.stateString],
                [quote.fromDate, employerCode, quote.stateString],
                [quote.fromDate, Customer.BASE_CUST_CODE, quote.stateString]
        ]
        return lookupParam(VehicleRegistrationParameter, hql, queryParamList)
    }

    def lookupFinance(quote, employeeCode, employerCode) {
        def hql = "from FinancierParameter as v where v.dateFrom<=? and v.customerCode=? order by v.dateFrom desc"
        def queryParamList = [
                [quote.fromDate, employeeCode],
                [quote.fromDate, employerCode],
                [quote.fromDate, Customer.BASE_CUST_CODE]
        ]
        return lookupParam(FinancierParameter, hql, queryParamList)
    }

    def lookupPurchaseStampDuty(quote, employeeCode, employerCode) {
        def hql = "from VehicleStampDutyParameter as v where v.dateFrom<=? and v.customerCode=? and v.state=? order by v.dateFrom desc"
        def queryParamList = [
                [quote.fromDate, employeeCode, quote.stateString],
                [quote.fromDate, employerCode, quote.stateString],
                [quote.fromDate, Customer.BASE_CUST_CODE, quote.stateString]
        ]
        return lookupParam(VehicleStampDutyParameter, hql, queryParamList)
    }

    def lookupLuxuryCarTax(quote, employeeCode, employerCode) {
        def hql = "from LuxuryCarTaxParameter as v where v.dateFrom<=? and v.customerCode=? order by v.dateFrom desc"
        def queryParamList = [
                [quote.fromDate, employeeCode],
                [quote.fromDate, employerCode],
                [quote.fromDate, Customer.BASE_CUST_CODE]
        ]
        return lookupParam(LuxuryCarTaxParameter, hql, queryParamList)
    }

    def lookupLuxuryCarAllowance(quote) {
        def hql = "from LuxuryAllowanceParameter as v where v.dateFrom<=? order by v.dateFrom desc"
        def queryParamList = [
                [quote.fromDate]
        ]
        return lookupParam(LuxuryAllowanceParameter, hql, queryParamList)
    }

    def lookupResidualValue(quote, employeeCode, employerCode) {
        def hql = "from ResidualValueParameter as v where v.dateFrom<=? and v.customerCode=? and v.term=? order by v.dateFrom desc"
        def queryParamList = [
                [quote.fromDate, employeeCode, quote.leaseTerm],
                [quote.fromDate, employerCode, quote.leaseTerm],
                [quote.fromDate, Customer.BASE_CUST_CODE, quote.leaseTerm]
        ]
        return lookupParam(ResidualValueParameter, hql, queryParamList)
    }

    def lookupFeesBrokerage(quote, employeeCode, employerCode) {
        def hql = "from FeesBrokerageParameter as v where v.dateFrom<=? and v.customerCode=? order by v.dateFrom desc"
        def queryParamList = [
                [quote.fromDate, employeeCode],
                [quote.fromDate, employerCode],
                [quote.fromDate, Customer.BASE_CUST_CODE]
        ]
        return lookupParam(FeesBrokerageParameter, hql, queryParamList)
    }

    def lookupMaintenance(quote) {
        def hql = "from MaintenanceParameter as v where v.dateFrom<=? and v.variant=? order by v.dateFrom desc"
        def res = lookupParam(MaintenanceParameter, hql, [[quote.fromDate, quote.vehicle]])
        if (res) {
            return res
        } else {
            hql = "from MaintenanceParameter as v where v.dateFrom<=? and v.model=? and v.variant=null order by v.dateFrom desc"
            res = lookupParam(MaintenanceParameter, hql, [[quote.fromDate, quote.vehicle?.model]])
            if (res) {
                return res
            } else {
                hql = "from MaintenanceParameter as v where v.dateFrom<=? and v.make=? and v.model=null and v.variant=null order by v.dateFrom desc"
                return lookupParam(MaintenanceParameter, hql, [[quote.fromDate, quote.vehicle?.model?.make]])
            }
        }
    }

    def lookupTyres(quote, employeeCode, employerCode) {
        /*def hql = "from TyreParameter as v where v.dateFrom<=? and v.customerCode=? and v.size=? order by v.dateFrom desc"
        def queryParamList = [
            [quote.fromDate, employeeCode, quote.vehicle?.tyreSize],
            [quote.fromDate, employerCode, quote.vehicle?.tyreSize],
            [quote.fromDate, Customer.BASE_CUST_CODE, quote.vehicle?.tyreSize]
        ]
        return lookupParam(TyreParameter, hql, queryParamList)*/

        // do a straight lookup on tyre size - if it is not null for this vehicle
        if (quote.vehicle.tyreSize) {
            def hql = "from TyreParameter as v where v.dateFrom<=? and v.size=? order by v.dateFrom desc"
            def res = lookupParam(TyreParameter, hql, [[quote.fromDate, quote.vehicle.tyreSize]])
            if (res) {
                return res
            }
        }

        // otherwise - check through the variant/model/make hierarchy...
        def hql = "from TyreParameter as v where v.dateFrom<=? and v.variant=? order by v.dateFrom desc"
        def res = lookupParam(TyreParameter, hql, [[quote.fromDate, quote.vehicle]])
        if (res) {
            return res
        } else {
            hql = "from TyreParameter as v where v.dateFrom<=? and v.model=? and v.variant=null order by v.dateFrom desc"
            res = lookupParam(TyreParameter, hql, [[quote.fromDate, quote.vehicle?.model]])
            if (res) {
                return res
            } else {
                hql = "from TyreParameter as v where v.dateFrom<=? and v.make=? and v.model=null and v.variant=null order by v.dateFrom desc"
                return lookupParam(TyreParameter, hql, [[quote.fromDate, quote.vehicle?.model?.make]])
            }
        }
    }

    def lookupFuel(quote, employeeCode, employerCode) {
        def hql = "from FuelParameter as v where v.dateFrom<=? and v.customerCode=? and v.state=? and v.type.id=? order by v.dateFrom desc"
        def queryParamList = [
                [quote.fromDate, employeeCode, quote.stateString, quote.fuelTypeUsed.id],
                [quote.fromDate, employerCode, quote.stateString, quote.fuelTypeUsed.id],
                [quote.fromDate, Customer.BASE_CUST_CODE, quote.stateString, quote.fuelTypeUsed.id]
        ]
        return lookupParam(FuelParameter, hql, queryParamList)
    }

    def lookupCompInsurance(quote, employeeCode, employerCode) {
        // determine the insurance class/category of the vehicle
        def lct = lookupLuxuryCarTax(quote, employeeCode, employerCode)
        def insuranceClass = CompInsuranceParameter.CLASS_4CYL
        def price = (quote?.listPrice && quote?.listPrice > 0) ? quote?.listPrice : quote?.vehicle?.retailPrice
        if (price >= lct?.threshold) {
            insuranceClass = CompInsuranceParameter.CLASS_V8PR
        } else {
            if (quote?.vehicle?.cylinders >= 8) {
                insuranceClass = CompInsuranceParameter.CLASS_V8PR
            } else if (quote?.vehicle?.cylinders >= 6) {
                insuranceClass = CompInsuranceParameter.CLASS_6CYL
            } else {
                insuranceClass = CompInsuranceParameter.CLASS_4CYL
            }
        }

        def hql = "from CompInsuranceParameter as v where v.dateFrom<=? and v.customerCode=? and v.state=? and v.type=? order by v.dateFrom desc"
        def queryParamList = [
                [quote.fromDate, employeeCode, quote.stateString, insuranceClass],
                [quote.fromDate, employerCode, quote.stateString, insuranceClass],
                [quote.fromDate, Customer.BASE_CUST_CODE, quote.stateString, insuranceClass]
        ]
        return lookupParam(CompInsuranceParameter, hql, queryParamList)
    }

    def lookupRoadside(quote, employeeCode, employerCode) {
        def hql = "from RoadsideParameter as v where v.dateFrom<=? and v.customerCode=? and v.state=? order by v.dateFrom desc"
        def queryParamList = [
                [quote.fromDate, employeeCode, quote.stateString],
                [quote.fromDate, employerCode, quote.stateString],
                [quote.fromDate, Customer.BASE_CUST_CODE, quote.stateString]
        ]
        return lookupParam(RoadsideParameter, hql, queryParamList)
    }

    //===================================================//
    def lookupParam(clazz, hql, paramList) {
        def res = null
        paramList.find {
            (res = clazz.find(hql, it)) != null
        }
        return res
    }
}
