package com.innovated.iris.services

class GeocoderService {

	def grailsApplication

    boolean transactional = false
	
	// some rough lat/lng co-ords that "frame" australia...
	private final double LAT_UPPER = -10.09867012
	private final double LAT_LOWER = -43.80281871
	private final double LNG_UPPER = 154.16015625
	private final double LNG_LOWER = 112.76367187

	private boolean isValidLatLong(String lat, String lng) {
		try {
			double dlat = Double.parseDouble(lat)
			double dlng = Double.parseDouble(lng)
			
			if ((LAT_LOWER<=dlat && dlat<=LAT_UPPER) && (LNG_LOWER<=dlng && dlng<=LNG_UPPER))
				return true
			else
				return false
		}
		catch (NumberFormatException nfe) {
			return false
		}
	}

	/**
	 * Get latitude and longitude for a given address string
	 * Return a map of the [lat:"",lng:""] or null
	 */
    def getLatLong(String address) {
		def result = [latLngValid:false]
		
		// only call out to the webservice when geocoding is enabled!!!
		if (grailsApplication.config.iris.geocoder.enabled) {
			try {
				// use Tiny Geocoder... for now
				def base = "http://tinygeocoder.com/create-api.php?q="
				def url = new URL(base + URLEncoder.encode(address))
				def connection = url.openConnection()
		
				if (connection.responseCode == 200) {
					def str = connection.content.text
					if (str != null && !"".equals(str)) {
						def latLng = str.split(',')
				
						// test if the co-ord is "most likely" valid
						if (isValidLatLong(latLng[0], latLng[1])) {
							result.latLngValid = true
							result.lat = latLng[0]
							result.lng = latLng[1]
						}
						else {
							result.latLngValid = false
							result.lat = latLng[0]
							result.lng = latLng[1]
						}
					}
				}
			} catch (Exception e) {
				//
			}
			return result
		}
		return result
    }
    
    
}
