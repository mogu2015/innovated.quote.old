package com.innovated.iris.domain

import org.codehaus.groovy.grails.plugins.springsecurity.Secured

import grails.converters.JSON;

import com.innovated.iris.domain.enums.*

//@Secured(['IS_AUTHENTICATED_FULLY'])
@Secured(['ROLE_SUPER', 'ROLE_ADMIN'])
class AddressController {
    
    def geocoderService
    
    def index = { redirect(action:list,params:params) }

    // the delete, save and update actions only accept POST requests
    static allowedMethods = [delete:'POST', save:'POST', update:'POST']

    def list = {
        params.max = Math.min( params.max ? params.max.toInteger() : 10,  100)
        [ addressInstanceList: Address.list( params ), addressInstanceTotal: Address.count() ]
    }

    def show = {
        def addressInstance = Address.get( params.id )

        if(!addressInstance) {
            flash.message = "Address not found with id ${params.id}"
            redirect(action:list)
        }
        else { return [ addressInstance : addressInstance ] }
    }

	@Secured(['IS_AUTHENTICATED_FULLY'])
    def saveAjax = {
    	def ajaxResp = [success:false, title:'Error Saving Address', msg:'An error was encountered while attempting to save your address']
		
		//if (params.entity.id == "null") {
			def cust = Customer.get(params.customer.id)
			params.entity.id = cust?.entity?.id
		//}
			
		// get address type
		def addrType = AddressType.get(params?.addressType?.id)
		if (!addrType) {
			params.addressType = AddressType.findByNameLike("street");
		}
		def addressInstance = new Address(params)

		// try geocoding the address
		
		//save the address TO THE ENTITY first!
		def e = Entity.get(params.entity.id)
		e.addToAddresses(addressInstance)
		//save the entity (& FLUSH!!!)
		if(!addressInstance.hasErrors() && e.save(flush:true)) {
			ajaxResp.success = true
			ajaxResp.title = 'Address Saved'
			ajaxResp.msg = 'Address saved successfully'
			ajaxResp.addr = addressInstance
			ajaxResp.addrStr = addressInstance.toString()
		}
		else {
			ajaxResp.errors = addressInstance.errors
		}
		render ajaxResp as JSON
    }

    def delete = {
        def addressInstance = Address.get( params.id )
        if(addressInstance) {
            try {
            	def entityName = addressInstance.entity.name
                addressInstance.delete(flush:true)
                flash.message = "Address deleted for: ${entityName}"
                redirect(action:list)
            }
            catch(org.springframework.dao.DataIntegrityViolationException e) {
                flash.message = "This address could not be deleted! (Records exist that depend on this address)"
                flash.iconClass = "warning"
                redirect(action:show,id:params.id)
            }
        }
        else {
            flash.message = "The selected address could not be found!"
            redirect(action:list)
        }
    }

    def edit = {
        def addressInstance = Address.get( params.id )

        if(!addressInstance) {
            flash.message = "The selected address could not be found!"
            redirect(action:list)
        }
        else {
            return [ addressInstance : addressInstance ]
        }
    }

    def update = {
        def addressInstance = Address.get( params.id )
        if(addressInstance) {
            if(params.version) {
                def version = params.version.toLong()
                if(addressInstance.version > version) {
                    
                    addressInstance.errors.rejectValue("version", "address.optimistic.locking.failure", "Another user has updated this Address while you were editing.")
                    render(view:'edit',model:[addressInstance:addressInstance])
                    return
                }
            }
            addressInstance.properties = params
            
            // try geocoding the "updated" address
        	def latLng = geocoderService.getLatLong(addressInstance.toString())
        	if (latLng?.latLngValid) {
        		addressInstance.lat = latLng.lat
        		addressInstance.lng = latLng.lng
        	}
			
			// TODO: remove this address from currently linked entity, and associate with the new one!!
            
            if(!addressInstance.hasErrors() && addressInstance.save()) {
                flash.message = "Address updated for: ${addressInstance.entity.name}"
                redirect(action:show,id:addressInstance.id)
            }
            else {
                render(view:'edit',model:[addressInstance:addressInstance])
            }
        }
        else {
            flash.message = "Address not found with id ${params.id}"
            redirect(action:list)
        }
    }

    def create = {
        def addressInstance = new Address()
        addressInstance.properties = params
        return ['addressInstance':addressInstance]
    }

    def save = {
    	
        def addressInstance = new Address(params)
        if (addressInstance.validate()) {
        	// try geocoding the address
        	def latLng = geocoderService.getLatLong(addressInstance.toString())
        	if (latLng?.latLngValid) {
        		addressInstance.lat = latLng.lat
        		addressInstance.lng = latLng.lng
        	}
        	
		    //save the address TO THE ENTITY first!
		    def e = Entity.get(params.entity.id)
		    e.addToAddresses(addressInstance)
			//save the entity (& FLUSH!!!)
		    if(!addressInstance.hasErrors() && e.save(flush:true)) {
		        flash.message = "Address created for: ${addressInstance.entity.name}"
		        redirect(action:show,id:addressInstance.id)
		    }
		    else {
		        render(view:'create',model:[addressInstance:addressInstance])
		    }
        }
        else {
        	render(view:'create',model:[addressInstance:addressInstance])
        }
    }
}
