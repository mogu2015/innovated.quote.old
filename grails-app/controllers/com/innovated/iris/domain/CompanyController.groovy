package com.innovated.iris.domain

import grails.converters.JSON;

import com.innovated.iris.util.PayrollFrequency;

import com.innovated.iris.server.fbt.*
import com.innovated.iris.services.*
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;

import org.codehaus.groovy.grails.plugins.springsecurity.Secured
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.core.io.ClassPathResource;
import org.springframework.ui.jasperreports.JasperReportsUtils;

//@Secured(['IS_AUTHENTICATED_FULLY'])
@Secured(['ROLE_SUPER', 'ROLE_ADMIN'])
class CompanyController {

    def reportService

    def index = { redirect(action: list, params: params) }

    // the delete, save and update actions only accept POST requests
    static allowedMethods = [delete: 'POST', save: 'POST', update: 'POST']

    def list = {
        params.max = Math.min(params.max ? params.max.toInteger() : 10, 100)
        [companyInstanceList: Company.list(params), companyInstanceTotal: Company.count()]
    }

    @Secured(['IS_AUTHENTICATED_FULLY'])
    def listAjax = {
        def data = []
        def companys = []
        if (params.term) {
            companys = Company.findAll("from Company as c where c.tradingName like :tradingName or c.employerCode like :employerCode", [tradingName: "%" + params.term + "%", employerCode: "%" + params.term + "%"])
        }
        def exceptEmployers = ['BUCOGO','ASANDNSW','PUBLICNSW','REBATABLE','PBI0001']
        companys.each {
            Entity entity = Entity.get(it.id)
            if(!exceptEmployers.contains(it.employerCode) && "Company".equals(entity.type)){
                data << [
                        id: it.id, tradingName: it.tradingName, employerCode: it.employerCode, payCyle: it.defaultPayFreq.getCode()
                ]
            }
        }
        render data as JSON
    }

    def show = {
        def companyInstance = Company.get(params.id)

        if (!companyInstance) {
            flash.message = "Company not found with id ${params.id}"
            redirect(action: list)
        } else {
            return buildCompanyEditModel(companyInstance)
        }
    }

    def exportPdf = {
        // exportPdf
        def c = Company.list()

        //chain(controller:'jasper', action:'index', model:[data:c], params:params)

        ClassPathResource res = new ClassPathResource("com/innovated/iris/server/report/${params._file}.jasper")
        JasperReport report = (JasperReport) JRLoader.loadObject(res.getInputStream())

        // setup response outputstream
        if (params.inline)
            response.setHeader("Content-disposition", "attachment; filename=\"CompanyList.pdf\"")
        response.contentType = "application/pdf"
        response.characterEncoding = "UTF-8"
        JasperReportsUtils.renderAsPdf(report, params, c, response.outputStream)
        return
    }

    def concessions = {
        def companyInstance = Company.get(params.id)

        if (!companyInstance) {
            flash.message = "Company not found with id ${params.id}"
            redirect(action: list)
        } else {
            //return buildCompanyEditModel(companyInstance)
            render "TODO: concession list"
        }
    }

    def delete = {
        /*def companyInstance = Company.get( params.id )
        if(companyInstance) {
            try {
                companyInstance.delete(flush:true)
                flash.message = "Company ${params.id} deleted"
                redirect(action:list)
            }
            catch(org.springframework.dao.DataIntegrityViolationException e) {
                flash.message = "Company ${params.id} could not be deleted"
                redirect(action:show,id:params.id)
            }
        }
        else {
            flash.message = "Company not found with id ${params.id}"
            redirect(action:list)
        }*/

        def companyInstance = Company.get(params.id)
        if (companyInstance) {
            companyInstance.withTransaction { status ->
                try {
                    companyInstance.delete(flush: true)
                    flash.message = "Company <b>${companyInstance.name}</b> deleted successfully"
                    redirect(action: list)
                }
                catch (org.springframework.dao.DataIntegrityViolationException e) {
                    status.setRollbackOnly()
                    //flash.message = "Company <b>${companyInstance.name}</b> could not be deleted.<br/>A <b>Customer</b> or <b>Supplier</b> record is linked to this Company!"
                    flash.message = message(code: "company.delete.exception", args: [companyInstance.name])
                    redirect(action: show, id: params.id)
                }
            }
        } else {
            flash.message = "Company not found with id ${params.id}"
            redirect(action: list)
        }
    }

    def edit = {
        def companyInstance = Company.get(params.id)

        if (!companyInstance) {
            flash.message = "Company not found with id ${params.id}"
            redirect(action: list)
        } else {
            return buildCompanyEditModel(companyInstance)
        }
    }

    def update = {
        def returnUrl = params.returnUrl

        def companyInstance = Company.get(params.id)
        if (companyInstance) {
            if (params.version) {
                def version = params.version.toLong()
                if (companyInstance.version > version) {
                    companyInstance.errors.rejectValue("version", "company.optimistic.locking.failure", "Another user has updated this Company while you were editing.")
                    render(view: 'edit', model: buildCompanyEditModel(companyInstance))
                    return
                }
            }
            def freqcode = params["defaultPayFreq.code"]
            params.defaultPayFreq = PayrollFrequency.get(freqcode?.toInteger())
            companyInstance.properties = params
            boolean check = preSaveCheck(companyInstance)

            //save the company TO THE PARENT ENTITY first IF it exists!
            def e = Company.get(params.parent.id)
            if (e != null && check) {
                e.addToChildren(companyInstance)    // save the entity (& FLUSH!!!)
                if (!companyInstance.hasErrors() && e.save(flush: true)) {
                    flash.message = "Company ${companyInstance.id} updated"
                    redirect(action: edit, id: companyInstance.id)
                } else {
                    render(view: 'create', model: buildCompanyEditModel(companyInstance))
                }
            } else {
                if (!companyInstance.hasErrors() && companyInstance.save(flush: true)) {
                    flash.message = "Company ${params.id} updated"
                    if (params.returnUrl) {
                        redirect(uri: params.returnUrl)
                    } else {
                        redirect(action: edit, id: companyInstance.id)
                    }
                } else {
                    render(view: 'edit', model: buildCompanyEditModel(companyInstance))
                }
            }
        } else {
            flash.message = "Company not found with id ${params.id}"
            redirect(action: list)
        }
    }

    def updateAjax = {
        def r = [
                title : "Update Failed!",
                text  : "The company record you requested could not be found.",
                img   : "${resource(dir: 'images', file: 'cross48.png')}",
                errors: null
        ]

        def companyInstance = Company.get(params.id)
        if (companyInstance) {
            //
            params.defaultPayFreq = PayrollFrequency.get(params?.defaultPayFreq?.toInteger())
            companyInstance.properties = params
            boolean check = preSaveCheck(companyInstance)

            //save the company TO THE PARENT ENTITY first IF it exists!
            def e = Company.get(params.parent.id)
            if (e != null && check) {
                e.addToChildren(companyInstance)    // save the entity (& FLUSH!!!)
                if (!companyInstance.hasErrors() && e.save(flush: true)) {
                    r.title = "Update Succeeded!"
                    r.text = "The company record for ${companyInstance.tradingName} was saved successfully."
                    r.img = "${resource(dir: 'images', file: 'tick48.png')}"
                } else {
                    response.status = 403
                    r.text = "An error was encountered while updating the company record for ${companyInstance.tradingName}."
                    r.errors = companyInstance.errors.allErrors
                }
            } else {
                if (!companyInstance.hasErrors() && companyInstance.save(flush: true)) {
                    r.title = "Update Succeeded!"
                    r.text = "The company record for ${companyInstance.tradingName} was saved successfully."
                    r.img = "${resource(dir: 'images', file: 'tick48.png')}"
                } else {
                    response.status = 403
                    r.text = "An error was encountered while updating the company record for ${companyInstance.tradingName}."
                    r.errors = companyInstance.errors.fieldErrors.collect {
                        [field: it.field, message: message(code: it.codes[0], default: "The value for ${it.field} was invalid.")]
                    }
                }
            }
        } else {
            response.status = 404
        }
        render r as JSON
    }

    def create = {
        def companyInstance = new Company()
        companyInstance.properties = params

        // add default
        companyInstance.allowedFbtCalcMethods.add(TaxableValueType.STATUTORY)
        companyInstance.allowedFbtCalcMethods.add(TaxableValueType.OP_COST)
        companyInstance.allowedFbtCalcMethods.add(TaxableValueType.FBT_EXEMPT)
        companyInstance.allowedFbtCalcMethods.add(TaxableValueType.CENTS_PER_KM_MC)

        return buildCompanyEditModel(companyInstance)
    }

    def save = {
        def companyInstance = new Company(params)
        boolean check = preSaveCheck(companyInstance)

        //save the company TO THE PARENT ENTITY first IF it exists!
        def e = Company.get(params.parent.id)
        if (e != null && check) {
            e.addToChildren(companyInstance)    // save the entity (& FLUSH!!!)
            if (!companyInstance.hasErrors() && e.save(flush: true)) {

                flash.message = "Company ${companyInstance.id} created"
                redirect(action: show, id: companyInstance.id)
            } else {
                render(view: 'create', model: buildCompanyEditModel(companyInstance))
            }
        } else {
            if (!companyInstance.hasErrors() && companyInstance.save(flush: true)) {

                flash.message = "Company ${companyInstance.id} created"
                redirect(action: show, id: companyInstance.id)
            } else {
                render(view: 'create', model: buildCompanyEditModel(companyInstance))
            }
        }
    }

    private boolean preSaveCheck(company) {
        boolean checkPassed = true
        if (params.tv_default) {
            //=== deal with Taxable Value preferences
            def validCodes = []
            def prefix = 'ALLOWED_TV_'
            company.allowedFbtCalcMethods.clear()    //remove all FBT taxable value types first

            for (String key in params.keySet()) {
                if (key.contains(prefix) && 'on' == params.get(key)) {
                    def code = Integer.valueOf(key.substring(prefix.length()))
                    validCodes << code
                    company.addToAllowedFbtCalcMethods(TaxableValueType.get(code))
                }
            }
            // pick up the default...
            def defaultCode = Integer.valueOf(params.tv_default)
            if (validCodes.contains(defaultCode)) {
                // move the default code to be first index in the list: allowedFbtCalcMethods
                TaxableValueType tvDefault = TaxableValueType.get(defaultCode)
                def idx = company.allowedFbtCalcMethods.indexOf(tvDefault)
                TaxableValueType removed = company.allowedFbtCalcMethods.remove(idx)
                company.allowedFbtCalcMethods.add(0, removed)
            } else {
                // raise an error on this company record? etc...
                company.errors.reject("company.allowedFbtCalcMethods", "Errors in allowed FBT methods!")
                checkPassed = false
            }

            //=== deal with FBT concession preferences
            def currConcession = Integer.valueOf(params.get("current_concession"))
            if (currConcession) {
                if (company.concessions.size() == 0) {
                    //no concessions currently, so add whatever has been selected
                    company.concessions << new CompanyFbtConcession(dateFrom: new Date(), type: FbtConcessionType.get(currConcession))
                } else {
                    //we already have concessions, so we only add a new one if it's different from the current
                    CompanyFbtConcession old = company.concessions.max { it.dateFrom }
                    if (old.type != FbtConcessionType.get(currConcession)) {
                        company.concessions << new CompanyFbtConcession(dateFrom: new Date(), type: FbtConcessionType.get(currConcession))
                    }
                }
            } else {
                company.errors.reject("company.concessions", "Errors in company concessions!")
                checkPassed = false
            }
        } else {
            company.errors.reject("company.allowedFbtCalcMethods", "Errors in allowed FBT methods!")
            checkPassed = false
        }
        return checkPassed
    }

    private Map buildCompanyEditModel(company) {
        Map<Integer, TaxableValueType> tvTypes = TaxableValueType.lookup
        LinkedHashMap<TaxableValueType, Boolean> tvTypeMap = [:]
        for (tv in tvTypes) {
            tvTypeMap[(tv)] = company.allowedFbtCalcMethods.contains(tv.value)
        }
        return [companyInstance: company, taxableValueMap: tvTypeMap, concessionMap: FbtConcessionType.lookup, currentConcession: company.concessions.max {
            it.dateFrom
        }]
    }
}

