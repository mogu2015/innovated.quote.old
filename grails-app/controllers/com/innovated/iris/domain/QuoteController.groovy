package com.innovated.iris.domain

import grails.converters.JSON;

import java.text.DecimalFormat;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;

import org.codehaus.groovy.grails.plugins.springsecurity.Secured
import org.springframework.core.io.ClassPathResource;
import org.springframework.ui.jasperreports.JasperReportsUtils;

import com.innovated.iris.util.*
import com.innovated.iris.domain.auth.*
import com.innovated.iris.domain.enums.*
import com.innovated.iris.domain.lookup.*
import com.innovated.iris.enums.QuoteStatus;
import com.innovated.iris.services.*
import com.innovated.iris.server.fbt.*

@Secured(['IS_AUTHENTICATED_FULLY'])
class QuoteController {

    def quoteService
    def authenticateService
    def jmsService

    def index = { redirect(action: "list", params: params) }

    // the delete, save and update actions only accept POST requests
    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    //
    private List buildQuoteListForUser(params) {
        def isAdmin = authenticateService.ifAnyGranted('ROLE_SUPER, ROLE_ADMIN')
        def principal = authenticateService.principal()
        def userLoggedIn = User.findByUsername(principal.username)

        if (isAdmin) {
            return Quote.list(params)
        } else {
            def customer = userLoggedIn?.customer
            if (customer) {
                if (customer.entity.type == "Individual") {
                    return Quote.findAllByCustomer(customer, params)
                } else {
                    return Quote.withCriteria { employment { eq("employer", customer.entity) } }
                }
            } else {
                return []
            }
        }
    }

    def list = {
        params.max = Math.min(params.max ? params.max.toInteger() : 10, 100)
        params.sort = params.sort ?: "dateCreated"
        params.order = params.order ?: "desc"
        [quoteInstanceList: buildQuoteListForUser(params), quoteInstanceTotal: Quote.count()]
    }

    def create = {
        def quoteInstance = new Quote(params)

        // load defaults based on user that's logged in!
        def testMsg = "<b>${authenticateService.principal().username}</b>: im a normal user!"
        if (authenticateService.ifAnyGranted('ROLE_SUPER, ROLE_ADMIN')) {
            testMsg = "<b>${authenticateService.principal().username}</b>: im an admin!"
        }

        // build view model based on logged-in users' credentials!!

        return [
                quoteInstance: quoteInstance,
                individuals  : Customer.withCriteria { entity { like('type', '%Individual%') } },
                addresses    : Address.list(),
                testMessage  : testMsg
        ]
    }

    def clone = {
        def q = Quote.get(params.id)

        // do the following in the QuoteService !!!

        // clone only the following properties...
        def includedProps = ["fromDate", "customer", "address", "employment",
                             "grossAnnualSalary", "vehicle", "shape", "transmission",
                             "colourPreference", "kmPerYear", "leaseTerm", "businessUsage"];
        def props = q.properties.findAll { includedProps.contains(it.key) }
        def quoteInstance = new Quote(props)

        // load defaults based on user that's logged in!
        def testMsg = "<b>${authenticateService.principal().username}</b>: im a normal user!"
        if (authenticateService.ifAnyGranted('ROLE_SUPER, ROLE_ADMIN')) {
            testMsg = "<b>${authenticateService.principal().username}</b>: im an admin!"
        }

        // build view model based on logged-in users' credentials!!

        return [
                quoteInstance: quoteInstance,
                individuals  : Customer.withCriteria { entity { like('type', '%Individual%') } },
                addresses    : Address.list(),
                testMessage  : testMsg
        ]
    }

    def test = {
        def q = Quote.get(params.id)
        if (q) {
            def output = "output"
            def phones = q.customer?.entity?.phones
            if (phones.size() > 0) {
                output += ", phones=${phones.size()}"

                if (phones.findAll { it.phoneType.name == "Mobile" }.size() > 0) {
                    output += ", default_mobile=${phones.findAll { it.phoneType.name == "Mobile" }.get(0).toString()}"
                }
                if (phones.findAll { it.phoneType.name == "Home Phone" }.size() > 0) {
                    output += ", default_landline=${phones.findAll { it.phoneType.name == "Home Phone" }.get(0).toString()}"
                }
            }

            render output
        } else {
            render "Could not find quote"
        }
    }

    def accept = {
        def p = PackageComparison.get(params.comparison)
        if (p) {
            p.selected = true
            def q = p.quote
            def notes = "Quote acceptance"
            def statusChanged = quoteService.changeStatus(q, QuoteStatus.QUOTE_ACCEPTED, notes)
            if (p.save(flush: true) && statusChanged) {

                // TODO: notify by email (innov staff & employer) that a quote has been accepted and is awaiting approval...

                def user = User.findByCustomer(q.customer)

                if (user) {
                    def msg = quoteService.generateAdminEmailMsg("Preferred Quote Accepted!", "acceptedQuoteNotifyAdmin", q)
                    jmsService.send([service: 'notification', method: 'email'], msg)
                }

                flash.message = "Your quote acceptance request was successful!"
                flash.classes = "flashOk"
                redirect(controller: "tools", action: "viewquote", id: q?.id)
            } else {
                flash.message = "Your quote acceptance request was unsuccessful! Please contact inNovated Leasing for more details."
                flash.classes = "flashErr"
                redirect(controller: "tools", action: "quote")
            }
        } else {
            flash.message = "Your quote acceptance request could not be processed! Please contact inNovated Leasing for more details."
            flash.classes = "flashErr"
            redirect(controller: "tools", action: "quote")
        }
    }

    def changeStatusAjax = {
        def q = quoteService.getQuote(params)
        if (q && params.status) {
            def status = QuoteStatus.get(params.status?.toInteger())
            if (quoteService.changeStatus(q, status, params.notes))
                render "Quote status successfully changed."
            else
                render "Could not change quote status."
        } else {
            render "Could not change quote status."
        }
    }

    def export = {
        if (!params.id) {
            flash.message = "The requested quote could not be found!"
            redirect(action: "list")
        } else {
            def extension = params.id?.tokenize('.')
            def tmp = extension[0].tokenize('-')
            def aqn = tmp[0]
            def method = tmp[1]

            DecimalFormat fmt = new DecimalFormat(message(code: 'quote.number.mask'))
            Number quoteId = fmt.parse(aqn)
            //println "AQN=${aqn}, Method=${method}, QuoteId=${quoteId}"

            def c = PackageComparison.get(method)
            if (!c) {
                flash.message = "quote.comparison.not.found"
                flash.defaultMessage = "Comparison method for quote ${aqn} could not be found!"
                redirect(action: "show", id: params.id)
            } else {
                def quote = Quote.get(quoteId.intValue())

                // check if a template exists for the employer's code...
                def template = quoteService.getQuoteExportTemplate(quote)
                def filename = "quote-${aqn}-${formatDate(format: 'yyyyMMdd', date: new Date())}.pdf"
                // TODO: construct a better filename too!!
                def mimeType = "application/pdf"

                try {
                    def templPath = "com/innovated/iris/server/report/${template}.jasper".toString()
                    ClassPathResource res = new ClassPathResource(templPath)
                    JasperReport report = (JasperReport) JRLoader.loadObject(res.getInputStream())

                    def quoteAsList = [quote]
                    def reportParams = /*params +*/ [comparison: c, aqn: aqn]

                    // setup response outputstream
                    if (params.download)
                        response.setHeader("Content-disposition", "attachment; filename=\"${filename}\"")
                    response.contentType = mimeType
                    response.characterEncoding = "UTF-8"
                    JasperReportsUtils.renderAsPdf(report, reportParams, quoteAsList, response.outputStream)
                    return
                }
                catch (Exception e) {
                    render(view: "error", model: [exception: e])
                }
            }

        }
    }


    def save = {
        try {
            def quoteInstance = new Quote(params)
            if (!quoteInstance.validate()) {
                //
                render(view: "create", model: [
                        quoteInstance: quoteInstance,
                        individuals  : Customer.withCriteria { entity { like('type', '%Individual%') } },
                        addresses    : Address.list()
                ])
                return
            }

            preSaveCheck(quoteInstance)
            //println "HERE I AM!"
            quoteInstance = quoteService.save(quoteInstance, params)

            def aqn = formatNumber(number: quoteInstance.id, format: message(code: 'quote.number.mask'))
            flash.message = "quote.created"
            flash.args = [aqn]
            flash.defaultMessage = "Quote ${quoteInstance.id} created"
            redirect(action: "show", id: quoteInstance.id)
        }
        catch (grails.validation.ValidationException ve) {
            def quoteInstance = new Quote(params)
            //println "ERRORS! " + quoteInstance.customer
            quoteInstance.errors = ve.errors
            render(view: "create", model: [
                    quoteInstance: quoteInstance,
                    individuals  : Customer.withCriteria { entity { like('type', '%Individual%') } },
                    addresses    : Address.list()
            ])
        }
    }

    def show = {
        def quoteInstance = Quote.get(params.id)
        if (!quoteInstance) {
            try {
                def aqn = formatNumber(number: params.id?.toLong(), format: message(code: 'quote.number.mask'))
                flash.message = "quote.not.found"
                flash.args = [aqn]
                flash.defaultMessage = "Quote not found with id ${params.id}"
            }
            catch (Exception e) {
                flash.message = "The requested quote could not be found!"
            }
            redirect(action: "list")
        } else {
            def breakdown = quoteService.retrieveBreakdown(quoteInstance)
            def totalList = []

            def subTotalAnnual = 0.0 + (breakdown.sum { it.annually } ?: 0.0)
            def subTotalMonth = 0.0 + (breakdown.sum { it.monthly } ?: 0.0)
            def subTotalFnight = 0.0 + (breakdown.sum { it.fortnightly } ?: 0.0)
            def subTotalWeek = 0.0 + (breakdown.sum { it.weekly } ?: 0.0)

            // work out totals & GST and add to the list!
            totalList.add([
                    category   : message(code: "quote.breakdown.table.label.subTotal", default: "Sub-Total"),
                    annually   : subTotalAnnual,
                    monthly    : subTotalMonth,
                    fortnightly: subTotalFnight,
                    weekly     : subTotalWeek,
                    supplier   : ""
            ])
            totalList.add([
                    category   : message(code: "quote.breakdown.table.label.gst", default: "GST"),
                    annually   : CurrencyUtils.roundUpNearestCent(subTotalAnnual / 10),
                    monthly    : CurrencyUtils.roundUpNearestCent(subTotalMonth / 10),
                    fortnightly: CurrencyUtils.roundUpNearestCent(subTotalFnight / 10),
                    weekly     : CurrencyUtils.roundUpNearestCent(subTotalWeek / 10),
                    supplier   : ""
            ])
            totalList.add([
                    category   : message(code: "quote.breakdown.table.label.grandTotal", default: "Grand Total"),
                    annually   : subTotalAnnual + CurrencyUtils.roundUpNearestCent(subTotalAnnual / 10),
                    monthly    : subTotalMonth + CurrencyUtils.roundUpNearestCent(subTotalMonth / 10),
                    fortnightly: subTotalFnight + CurrencyUtils.roundUpNearestCent(subTotalFnight / 10),
                    weekly     : subTotalWeek + CurrencyUtils.roundUpNearestCent(subTotalWeek / 10),
                    supplier   : ""
            ])

            return [
                    quoteInstance       : quoteInstance,
                    quoteBreakdown      : breakdown,
                    quoteBreakdownTotals: totalList,
                    comparisonItemTypes : PackagingComparisonItemType.getTypes()
            ]
        }
    }

    @Secured(['ROLE_ADMIN'])
    def edit = {
        def quoteInstance = Quote.get(params.id)
        if (!quoteInstance) {
            flash.message = "quote.not.found"
            flash.args = [params.id]
            flash.defaultMessage = "Quote not found with id ${params.id}"
            redirect(action: "list")
        } else {
            return buildQuoteEditModel(quoteInstance)
        }
    }

    @Secured(['ROLE_ADMIN'])
    def update = {
        def quoteInstance = Quote.get(params.id)
        if (quoteInstance) {
            // update drop-down values...
            params.concession = FbtConcessionType.get(Integer.valueOf(params.concession))
            params.defaultPayFreq = PayrollFrequency.get(Integer.valueOf(params.defaultPayFreq))

            if (params.version) {
                def version = params.version.toLong()
                if (quoteInstance.version > version) {

                    // should ask the user what to do!!
                    //   * make a duplicate quote (rather than updating)
                    //   * keep the other users' changes (ie. overwrite your changes)
                    //   * keep your own changes (ie. overwrite the other users' changes)

                    quoteInstance.errors.rejectValue("version", "quote.optimistic.locking.failure", "Another user has updated this Quote while you were editing")
                    render(view: "edit", model: buildQuoteEditModel(quoteInstance))
                    return
                }
            }
            quoteInstance.properties = params

            // confirm final pricing if we need to
            boolean priceConfirmed = (params.chk_pricingConfirmed && params.chk_pricingConfirmed == "on")
            if (priceConfirmed && (quoteInstance.pricingConfirmed == null)) {
                quoteInstance.pricingConfirmed = new Date()
                def statusChanged = quoteService.changeStatus(quoteInstance, QuoteStatus.QUOTE_ACTUAL, "Quote price confirmed")
                if (statusChanged) {
                    def user = User.findByCustomer(quoteInstance.customer)
                    def quoteNo = formatNumber(number: quoteInstance.id, format: message(code: 'quote.number.mask'))
                    if (user && !quoteInstance.priceObtained) {
                        def msg = quoteService.generateCustomerEmailMsg("Dealer Pricing Received! (${quoteNo})".toString(), "confirmedQuoteNotifyCustomer", quoteInstance)
                        jmsService.send([service: 'notification', method: 'email'], msg)
                    }
                }
            } else {
                quoteInstance.pricingConfirmed = null
            }

            try {
                // re-do lookup here - only if explicitly told to!
                def relookup = (params.relookup == "true")
//                preSaveCheck(quoteInstance)
                quoteInstance = quoteService.update(quoteInstance, params, relookup)
                quoteInstance.save(flush: true, failOnError: false)
                def aqn = formatNumber(number: quoteInstance.id, format: message(code: 'quote.number.mask'))
                if (relookup) {
                    flash.message = "quote.lookup.updated"
                    flash.args = [aqn]
//, "<a href='http://www.designcodetest.com' target='_blank' title='click here'>here</a>"]
                    flash.classes = "flashOk"
                    flash.defaultMessage = "Quote ${quoteInstance.id} updated <b>AND</b> lookup values refreshed!"
                } else {
                    flash.message = "quote.updated"
                    flash.args = [aqn]
                    flash.classes = "flashOk"
                    flash.defaultMessage = "Quote ${quoteInstance.id} updated"
                }
                redirect(action: "edit"/*(relookup ? "edit" : "show")*/, id: quoteInstance.id)
            }
            catch (grails.validation.ValidationException ve) {
                def qi = Quote.get(params.id)
                qi.properties = params
                qi.errors = ve.errors
                render(view: "edit", model: buildQuoteEditModel(qi))
            }
        } else {
            try {
                def aqn = formatNumber(number: params.id?.toLong(), format: message(code: 'quote.number.mask'))
                flash.message = "quote.not.found"
                flash.args = [aqn]
                flash.defaultMessage = "Quote not found with id ${params.id}"
            }
            catch (Exception e) {
                flash.message = "The requested quote could not be found!"
            }
            redirect(action: "edit", id: params.id)
        }
    }

    @Secured(['ROLE_ADMIN'])
    def deleteAjax = {
        def resp = [success: false, msg: "Failed to delete quote", classes: "flashErr"]
        def aqn = formatNumber(number: params.id?.toLong(), format: message(code: 'quote.number.mask'))
        def quoteInstance = Quote.get(params.id)
        if (quoteInstance) {
            if (quoteService.delete(quoteInstance))
                resp = [success: true, msg: "Successfully deleted quote: ${aqn}", classes: "flashOk"]
            else
                resp = [success: false, msg: "Failed to delete quote: ${aqn}", classes: "flashErr"]
        } else {
            resp = [success: false, msg: "Failed to locate quote ${aqn} for deletion!", classes: "flashWarn"]
        }
        render resp as JSON
    }

    @Secured(['ROLE_ADMIN'])
    def delete = {
        def aqn = formatNumber(number: params.id?.toLong(), format: message(code: 'quote.number.mask'))
        def quoteInstance = Quote.get(params.id)
        if (quoteInstance) {
            try {
                quoteInstance.delete()
                flash.message = "quote.deleted"
                flash.args = [aqn]
                flash.defaultMessage = "Quote ${params.id} deleted"
                redirect(controller: "tools", action: "quote")
            }
            catch (org.springframework.dao.DataIntegrityViolationException e) {
                flash.message = "quote.not.deleted"
                flash.args = [aqn]
                flash.defaultMessage = "Quote ${params.id} could not be deleted"
                //redirect(action: "show", id: params.id)
                redirect(controller: "tools", action: "quote")
            }
        } else {
            try {
                flash.message = "quote.not.found"
                flash.args = [aqn]
                flash.defaultMessage = "Quote not found with id ${params.id}"
            }
            catch (Exception e) {
                flash.message = "The requested quote could not be found!"
            }
            redirect(controller: "tools", action: "quote")
        }
    }

    private void preSaveCheck(quote) {
        //=== deal with Taxable Value preferences
        def validCodes = []
        def prefix = 'ALLOWED_TV_'
        quote.allowedFbtCalcMethods.clear()    //remove all FBT taxable value types first

        for (String key in params.keySet()) {
            if (key.contains(prefix) && 'on' == params.get(key)) {
                def code = Integer.valueOf(key.substring(prefix.length()))
                validCodes << code
                quote.addToAllowedFbtCalcMethods(TaxableValueType.get(code))
            }
        }
        // pick up the default...
        if (params.tv_default) {
            def defaultCode = Integer.valueOf(params.tv_default)
            if (validCodes.contains(defaultCode)) {
                // move the default code to be first index in the list: allowedFbtCalcMethods
                TaxableValueType tvDefault = TaxableValueType.get(defaultCode)
                def idx = quote.allowedFbtCalcMethods.indexOf(tvDefault)
                TaxableValueType removed = quote.allowedFbtCalcMethods.remove(idx)
                quote.allowedFbtCalcMethods.add(0, removed)
            } else {
                // raise an error on this quote record? etc...
                //println "tv_default INVALID = ${defaultCode}"

            }
        }
    }

    private Map buildQuoteEditModel(quote) {
        Map<Integer, TaxableValueType> tvTypes = TaxableValueType.lookup
        LinkedHashMap<TaxableValueType, Boolean> tvTypeMap = [:]
        for (tv in tvTypes) {
            tvTypeMap[(tv)] = quote.allowedFbtCalcMethods.contains(tv.value)
        }
        def payFreqLookup = PayrollFrequency.lookup.entrySet().sort { it.key }
        return [quoteInstance: quote, taxableValueMap: tvTypeMap, concessionMap: FbtConcessionType.lookup, payFreqMap: payFreqLookup]
    }
}
