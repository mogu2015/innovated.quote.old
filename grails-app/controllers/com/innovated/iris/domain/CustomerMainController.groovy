package com.innovated.iris.domain

import grails.converters.JSON;
import grails.converters.XML;

import org.codehaus.groovy.grails.plugins.springsecurity.Secured

@Secured(['ROLE_SUPER', 'ROLE_ADMIN'])
class CustomerMainController {
	// the delete, save and update actions only accept POST requests
    static allowedMethods = [save:"POST", update:"POST", delete:"POST"];

    def customerService

    def index = {
		redirect(action:"list", params:params)
	}
                             
    def list = {
		def customerList = customerService.list(params)
		withFormat {
			html { [customerInstanceList:customerList] }
			js { render customerList as JSON }
			xml { render customerList as XML }
		}
    }

	def create = {
		log.debug("Starting customer creation process")
		def c = customerService.newCustomer()
		c.properties = params
		
		return [customer:c]
    }

	def save = {
		log.debug("Saving customer")
    }

    def show = {
		log.debug("Showing customer")
    }

    def edit = {
		log.debug("Editing customer")
    }

    def update = {
		log.debug("Updating customer")
    }

	def delete = {
		log.debug("Deleting customer")
    }
}
