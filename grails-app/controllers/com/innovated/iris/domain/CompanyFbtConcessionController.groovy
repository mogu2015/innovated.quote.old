package com.innovated.iris.domain

import org.codehaus.groovy.grails.plugins.springsecurity.Secured;

@Secured(['ROLE_SUPER', 'ROLE_ADMIN'])
class CompanyFbtConcessionController {

    def index = { redirect(action: "list", params: params) }

    // the delete, save and update actions only accept POST requests
    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def list = {
        params.max = Math.min(params.max ? params.max.toInteger() : 10,  100)
        [companyFbtConcessionInstanceList: CompanyFbtConcession.list(params), companyFbtConcessionInstanceTotal: CompanyFbtConcession.count()]
    }

    def create = {
        def companyFbtConcessionInstance = new CompanyFbtConcession()
        companyFbtConcessionInstance.properties = params
        return [companyFbtConcessionInstance: companyFbtConcessionInstance]
    }

    def save = {
        def companyFbtConcessionInstance = new CompanyFbtConcession(params)
        if (!companyFbtConcessionInstance.hasErrors() && companyFbtConcessionInstance.save()) {
            flash.message = "companyFbtConcession.created"
            flash.args = [companyFbtConcessionInstance.id]
            flash.defaultMessage = "CompanyFbtConcession ${companyFbtConcessionInstance.id} created"
            redirect(action: "show", id: companyFbtConcessionInstance.id)
        }
        else {
            render(view: "create", model: [companyFbtConcessionInstance: companyFbtConcessionInstance])
        }
    }

    def show = {
        def companyFbtConcessionInstance = CompanyFbtConcession.get(params.id)
        if (!companyFbtConcessionInstance) {
            flash.message = "companyFbtConcession.not.found"
            flash.args = [params.id]
            flash.defaultMessage = "CompanyFbtConcession not found with id ${params.id}"
            redirect(action: "list")
        }
        else {
            return [companyFbtConcessionInstance: companyFbtConcessionInstance]
        }
    }

    def edit = {
        def companyFbtConcessionInstance = CompanyFbtConcession.get(params.id)
        if (!companyFbtConcessionInstance) {
            flash.message = "companyFbtConcession.not.found"
            flash.args = [params.id]
            flash.defaultMessage = "CompanyFbtConcession not found with id ${params.id}"
            redirect(action: "list")
        }
        else {
            return [companyFbtConcessionInstance: companyFbtConcessionInstance]
        }
    }

    def update = {
        def companyFbtConcessionInstance = CompanyFbtConcession.get(params.id)
        if (companyFbtConcessionInstance) {
            if (params.version) {
                def version = params.version.toLong()
                if (companyFbtConcessionInstance.version > version) {
                    
                    companyFbtConcessionInstance.errors.rejectValue("version", "companyFbtConcession.optimistic.locking.failure", "Another user has updated this CompanyFbtConcession while you were editing")
                    render(view: "edit", model: [companyFbtConcessionInstance: companyFbtConcessionInstance])
                    return
                }
            }
            companyFbtConcessionInstance.properties = params
            if (!companyFbtConcessionInstance.hasErrors() && companyFbtConcessionInstance.save()) {
                flash.message = "companyFbtConcession.updated"
                flash.args = [params.id]
                flash.defaultMessage = "CompanyFbtConcession ${params.id} updated"
                redirect(action: "show", id: companyFbtConcessionInstance.id)
            }
            else {
                render(view: "edit", model: [companyFbtConcessionInstance: companyFbtConcessionInstance])
            }
        }
        else {
            flash.message = "companyFbtConcession.not.found"
            flash.args = [params.id]
            flash.defaultMessage = "CompanyFbtConcession not found with id ${params.id}"
            redirect(action: "edit", id: params.id)
        }
    }

    def delete = {
        def companyFbtConcessionInstance = CompanyFbtConcession.get(params.id)
        if (companyFbtConcessionInstance) {
            try {
                companyFbtConcessionInstance.delete()
                flash.message = "companyFbtConcession.deleted"
                flash.args = [params.id]
                flash.defaultMessage = "CompanyFbtConcession ${params.id} deleted"
                redirect(action: "list")
            }
            catch (org.springframework.dao.DataIntegrityViolationException e) {
                flash.message = "companyFbtConcession.not.deleted"
                flash.args = [params.id]
                flash.defaultMessage = "CompanyFbtConcession ${params.id} could not be deleted"
                redirect(action: "show", id: params.id)
            }
        }
        else {
            flash.message = "companyFbtConcession.not.found"
            flash.args = [params.id]
            flash.defaultMessage = "CompanyFbtConcession not found with id ${params.id}"
            redirect(action: "list")
        }
    }
}
