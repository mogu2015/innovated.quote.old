package com.innovated.iris.domain

import org.codehaus.groovy.grails.plugins.springsecurity.Secured

//@Secured(['IS_AUTHENTICATED_FULLY'])
@Secured(['ROLE_SUPER', 'ROLE_ADMIN'])
class IndividualController {
    
    def index = { redirect(action:list,params:params) }

    // the delete, save and update actions only accept POST requests
    static allowedMethods = [delete:'POST', save:'POST', update:'POST']

    def list = {
        params.max = Math.min( params.max ? params.max.toInteger() : 10,  100)
        [ individualInstanceList: Individual.list( params ), individualInstanceTotal: Individual.count() ]
    }

    def show = {
        def individualInstance = Individual.get( params.id )

        if(!individualInstance) {
            flash.message = "Individual not found with id ${params.id}"
            redirect(action:list)
        }
        else { return [ individualInstance : individualInstance ] }
    }

    def delete = {
        def individualInstance = Individual.get( params.id )
        if(individualInstance) {
            try {
                individualInstance.delete(flush:true)
                flash.message = "Individual ${params.id} deleted"
                redirect(action:list)
            }
            catch(org.springframework.dao.DataIntegrityViolationException e) {
                flash.message = "Individual ${params.id} could not be deleted"
                redirect(action:show,id:params.id)
            }
        }
        else {
            flash.message = "Individual not found with id ${params.id}"
            redirect(action:list)
        }
    }

    def edit = {
        def individualInstance = Individual.get( params.id )

        if(!individualInstance) {
            flash.message = "Individual not found with id ${params.id}"
            redirect(action:list)
        }
        else {
            return [ individualInstance : individualInstance ]
        }
    }

    def update = {
        def individualInstance = Individual.get( params.id )
        if(individualInstance) {
            if(params.version) {
                def version = params.version.toLong()
                if(individualInstance.version > version) {
                    
                    individualInstance.errors.rejectValue("version", "individual.optimistic.locking.failure", "Another user has updated this Individual while you were editing.")
                    render(view:'edit',model:[individualInstance:individualInstance])
                    return
                }
            }
            individualInstance.properties = params
            if(!individualInstance.hasErrors() && individualInstance.save()) {
                flash.message = "Individual ${params.id} updated"
                redirect(action:show,id:individualInstance.id)
            }
            else {
                render(view:'edit',model:[individualInstance:individualInstance])
            }
        }
        else {
            flash.message = "Individual not found with id ${params.id}"
            redirect(action:list)
        }
    }

    def create = {
        def individualInstance = new Individual()
        individualInstance.properties = params
        return ['individualInstance':individualInstance]
    }

    def save = {
        def individualInstance = new Individual(params)
        if(!individualInstance.hasErrors() && individualInstance.save()) {
            flash.message = "Individual ${individualInstance.id} created"
            redirect(action:show,id:individualInstance.id)
        }
        else {
            render(view:'create',model:[individualInstance:individualInstance])
        }
    }
}
