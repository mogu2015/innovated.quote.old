package com.innovated.iris.domain.lookup

class FuelParameterController {

    def index = { redirect(action: "list", params: params) }

    // the delete, save and update actions only accept POST requests
    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def list = {
        params.max = Math.min(params.max ? params.max.toInteger() : 10,  100)
        [fuelParameterInstanceList: FuelParameter.list(params), fuelParameterInstanceTotal: FuelParameter.count()]
    }

    def create = {
        def fuelParameterInstance = new FuelParameter()
        fuelParameterInstance.properties = params
        return [fuelParameterInstance: fuelParameterInstance]
    }

    def save = {
        def fuelParameterInstance = new FuelParameter(params)
        if (!fuelParameterInstance.hasErrors() && fuelParameterInstance.save()) {
            flash.message = "fuelParameter.created"
            flash.args = [fuelParameterInstance.id]
            flash.defaultMessage = "FuelParameter ${fuelParameterInstance.id} created"
            redirect(action: "show", id: fuelParameterInstance.id)
        }
        else {
            render(view: "create", model: [fuelParameterInstance: fuelParameterInstance])
        }
    }

    def show = {
        def fuelParameterInstance = FuelParameter.get(params.id)
        if (!fuelParameterInstance) {
            flash.message = "fuelParameter.not.found"
            flash.args = [params.id]
            flash.defaultMessage = "FuelParameter not found with id ${params.id}"
            redirect(action: "list")
        }
        else {
            return [fuelParameterInstance: fuelParameterInstance]
        }
    }

    def edit = {
        def fuelParameterInstance = FuelParameter.get(params.id)
        if (!fuelParameterInstance) {
            flash.message = "fuelParameter.not.found"
            flash.args = [params.id]
            flash.defaultMessage = "FuelParameter not found with id ${params.id}"
            redirect(action: "list")
        }
        else {
            return [fuelParameterInstance: fuelParameterInstance]
        }
    }

    def update = {
        def fuelParameterInstance = FuelParameter.get(params.id)
        if (fuelParameterInstance) {
            if (params.version) {
                def version = params.version.toLong()
                if (fuelParameterInstance.version > version) {
                    
                    fuelParameterInstance.errors.rejectValue("version", "fuelParameter.optimistic.locking.failure", "Another user has updated this FuelParameter while you were editing")
                    render(view: "edit", model: [fuelParameterInstance: fuelParameterInstance])
                    return
                }
            }
            fuelParameterInstance.properties = params
            if (!fuelParameterInstance.hasErrors() && fuelParameterInstance.save()) {
                flash.message = "fuelParameter.updated"
                flash.args = [params.id]
                flash.defaultMessage = "FuelParameter ${params.id} updated"
                redirect(action: "show", id: fuelParameterInstance.id)
            }
            else {
                render(view: "edit", model: [fuelParameterInstance: fuelParameterInstance])
            }
        }
        else {
            flash.message = "fuelParameter.not.found"
            flash.args = [params.id]
            flash.defaultMessage = "FuelParameter not found with id ${params.id}"
            redirect(action: "edit", id: params.id)
        }
    }

    def delete = {
        def fuelParameterInstance = FuelParameter.get(params.id)
        if (fuelParameterInstance) {
            try {
                fuelParameterInstance.delete()
                flash.message = "fuelParameter.deleted"
                flash.args = [params.id]
                flash.defaultMessage = "FuelParameter ${params.id} deleted"
                redirect(action: "list")
            }
            catch (org.springframework.dao.DataIntegrityViolationException e) {
                flash.message = "fuelParameter.not.deleted"
                flash.args = [params.id]
                flash.defaultMessage = "FuelParameter ${params.id} could not be deleted"
                redirect(action: "show", id: params.id)
            }
        }
        else {
            flash.message = "fuelParameter.not.found"
            flash.args = [params.id]
            flash.defaultMessage = "FuelParameter not found with id ${params.id}"
            redirect(action: "list")
        }
    }
}
