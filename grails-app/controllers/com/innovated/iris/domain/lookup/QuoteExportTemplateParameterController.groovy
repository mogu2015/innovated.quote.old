package com.innovated.iris.domain.lookup

import grails.converters.JSON;

import java.util.Date;

class QuoteExportTemplateParameterController {

    def index = { redirect(action: "list", params: params) }

    // the delete, save and update actions only accept POST requests
    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def list = {
        params.max = Math.min(params.max ? params.max.toInteger() : 10,  100)
        [quoteExportTemplateParameterInstanceList: QuoteExportTemplateParameter.list(params), quoteExportTemplateParameterInstanceTotal: QuoteExportTemplateParameter.count()]
    }
	
	def listAjax = {
		def ajax = [success:true, sEcho:params.sEcho, iTotalRecords:0, iTotalDisplayRecords:0, aaData:[]]
		def whereStr = ""
		def orderByStr = ""
		def alias = "c"
		def hdrList = ["chk","dateFrom","customerCode","templateName","dateCreated","lastUpdated"]
		
		// manage search/filtering
		if (params.sSearch) {
			def whereCols = []
			hdrList.eachWithIndex { item, idx ->
				if (idx > 0) whereCols << "${alias}.${item} like '%${params.sSearch}%'"
			}
			whereStr = " where " + whereCols.join(" or ")
		}
		
		// work out sorting!
		if (params.iSortingCols && (params.iSortingCols.toInteger() > 0)) {
			def sortCols = []
			def prefix = 'iSortCol_'
			for (String key in params.keySet()) {
				if (key.startsWith(prefix)) {
					def code = Integer.valueOf(key.substring(prefix.length()))
					sortCols << [id:code, column:params.get(key).toInteger(), order:params.get("sSortDir_"+code)]
				}
			}
			orderByStr = " order by " + sortCols.sort{ it.id }.collect{ alias + "." + hdrList.get(it.column) + " " + it.order }.join(", ")
		}
		
		def total = QuoteExportTemplateParameter.count()
		def sortParams = [:]
		if (params.iDisplayLength.toInteger() == -1) {
			sortParams.max = total
			sortParams.offset = 0
		}
		else {
			sortParams.max = Math.min(params.iDisplayLength ? params.iDisplayLength.toInteger() : 100,  10000)
			sortParams.offset = params.iDisplayStart ? params.iDisplayStart.toInteger() : 0
		}
		
		def query = "from QuoteExportTemplateParameter as ${alias}${whereStr}${orderByStr}"
		def records = QuoteExportTemplateParameter.findAll(query, [], sortParams)
		
		ajax.iTotalRecords = total
		ajax.iTotalDisplayRecords = total
		ajax.aaData = records.collect{ p -> [
			"" + g.checkBox(class:"chkRow", name:"chkRow_"+p.id, value:false, title:"Select this row", checked:false),
			"",
			"" + g.link(action:"show", id:p.id, title:"View this template") { p.id },
			p.dateFrom,
			p.customerCode,
			p.templateName,
			p.dateCreated
		]}
		
		render ajax as JSON
	}

    def create = {
        def quoteExportTemplateParameterInstance = new QuoteExportTemplateParameter()
        quoteExportTemplateParameterInstance.properties = params
        return [quoteExportTemplateParameterInstance: quoteExportTemplateParameterInstance]
    }

    def save = {
        def quoteExportTemplateParameterInstance = new QuoteExportTemplateParameter(params)
        if (!quoteExportTemplateParameterInstance.hasErrors() && quoteExportTemplateParameterInstance.save()) {
            flash.message = "quoteExportTemplateParameter.created"
            flash.args = [quoteExportTemplateParameterInstance.id]
            flash.defaultMessage = "QuoteExportTemplateParameter ${quoteExportTemplateParameterInstance.id} created"
            redirect(action: "show", id: quoteExportTemplateParameterInstance.id)
        }
        else {
            render(view: "create", model: [quoteExportTemplateParameterInstance: quoteExportTemplateParameterInstance])
        }
    }

    def show = {
        def quoteExportTemplateParameterInstance = QuoteExportTemplateParameter.get(params.id)
        if (!quoteExportTemplateParameterInstance) {
            flash.message = "quoteExportTemplateParameter.not.found"
            flash.args = [params.id]
            flash.defaultMessage = "QuoteExportTemplateParameter not found with id ${params.id}"
            redirect(action: "list")
        }
        else {
            return [quoteExportTemplateParameterInstance: quoteExportTemplateParameterInstance]
        }
    }

    def edit = {
        def quoteExportTemplateParameterInstance = QuoteExportTemplateParameter.get(params.id)
        if (!quoteExportTemplateParameterInstance) {
            flash.message = "quoteExportTemplateParameter.not.found"
            flash.args = [params.id]
            flash.defaultMessage = "QuoteExportTemplateParameter not found with id ${params.id}"
            redirect(action: "list")
        }
        else {
            return [quoteExportTemplateParameterInstance: quoteExportTemplateParameterInstance]
        }
    }

    def update = {
        def quoteExportTemplateParameterInstance = QuoteExportTemplateParameter.get(params.id)
        if (quoteExportTemplateParameterInstance) {
            if (params.version) {
                def version = params.version.toLong()
                if (quoteExportTemplateParameterInstance.version > version) {
                    
                    quoteExportTemplateParameterInstance.errors.rejectValue("version", "quoteExportTemplateParameter.optimistic.locking.failure", "Another user has updated this QuoteExportTemplateParameter while you were editing")
                    render(view: "edit", model: [quoteExportTemplateParameterInstance: quoteExportTemplateParameterInstance])
                    return
                }
            }
            quoteExportTemplateParameterInstance.properties = params
            if (!quoteExportTemplateParameterInstance.hasErrors() && quoteExportTemplateParameterInstance.save()) {
                flash.message = "quoteExportTemplateParameter.updated"
                flash.args = [params.id]
                flash.defaultMessage = "QuoteExportTemplateParameter ${params.id} updated"
                redirect(action: "show", id: quoteExportTemplateParameterInstance.id)
            }
            else {
                render(view: "edit", model: [quoteExportTemplateParameterInstance: quoteExportTemplateParameterInstance])
            }
        }
        else {
            flash.message = "quoteExportTemplateParameter.not.found"
            flash.args = [params.id]
            flash.defaultMessage = "QuoteExportTemplateParameter not found with id ${params.id}"
            redirect(action: "edit", id: params.id)
        }
    }

    def delete = {
        def quoteExportTemplateParameterInstance = QuoteExportTemplateParameter.get(params.id)
        if (quoteExportTemplateParameterInstance) {
            try {
                quoteExportTemplateParameterInstance.delete()
                flash.message = "quoteExportTemplateParameter.deleted"
                flash.args = [params.id]
                flash.defaultMessage = "QuoteExportTemplateParameter ${params.id} deleted"
                redirect(action: "list")
            }
            catch (org.springframework.dao.DataIntegrityViolationException e) {
                flash.message = "quoteExportTemplateParameter.not.deleted"
                flash.args = [params.id]
                flash.defaultMessage = "QuoteExportTemplateParameter ${params.id} could not be deleted"
                redirect(action: "show", id: params.id)
            }
        }
        else {
            flash.message = "quoteExportTemplateParameter.not.found"
            flash.args = [params.id]
            flash.defaultMessage = "QuoteExportTemplateParameter not found with id ${params.id}"
            redirect(action: "list")
        }
    }
}
