package com.innovated.iris.domain.lookup

import org.codehaus.groovy.grails.plugins.springsecurity.Secured;

import com.innovated.iris.domain.*
import com.innovated.iris.domain.enums.*

@Secured(['ROLE_SUPER', 'ROLE_ADMIN'])
class VehicleStampDutyParameterController {

    def index = { redirect(action: "list", params: params) }

    // the delete, save and update actions only accept POST requests
    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def list = {
        params.max = Math.min(params.max ? params.max.toInteger() : 10,  100)
        [vehicleStampDutyParameterInstanceList: VehicleStampDutyParameter.list(params), vehicleStampDutyParameterInstanceTotal: VehicleStampDutyParameter.count()]
    }

    def create = {
        def vehicleStampDutyParameterInstance = new VehicleStampDutyParameter()
        vehicleStampDutyParameterInstance.properties = params
        return [
        	vehicleStampDutyParameterInstance: vehicleStampDutyParameterInstance,
        	defaultCode: Customer.BASE_CUST_CODE
        ]
    }

    def save = {
        def vehicleStampDutyParameterInstance = new VehicleStampDutyParameter(params)
        if (!vehicleStampDutyParameterInstance.hasErrors() && vehicleStampDutyParameterInstance.save()) {
            flash.message = "vehicleStampDutyParameter.created"
            flash.args = [vehicleStampDutyParameterInstance.id]
            flash.defaultMessage = "VehicleStampDutyParameter ${vehicleStampDutyParameterInstance.id} created"
            redirect(action: "list")//, id: vehicleStampDutyParameterInstance.id)
        }
        else {
            render(view: "create", model: [vehicleStampDutyParameterInstance: vehicleStampDutyParameterInstance, defaultCode: Customer.BASE_CUST_CODE])
        }
    }

    def show = {
        def vehicleStampDutyParameterInstance = VehicleStampDutyParameter.get(params.id)
        if (!vehicleStampDutyParameterInstance) {
            flash.message = "vehicleStampDutyParameter.not.found"
            flash.args = [params.id]
            flash.defaultMessage = "VehicleStampDutyParameter not found with id ${params.id}"
            redirect(action: "list")
        }
        else {
            return [vehicleStampDutyParameterInstance: vehicleStampDutyParameterInstance, defaultCode: Customer.BASE_CUST_CODE]
        }
    }

    def edit = {
        def vehicleStampDutyParameterInstance = VehicleStampDutyParameter.get(params.id)
        if (!vehicleStampDutyParameterInstance) {
            flash.message = "vehicleStampDutyParameter.not.found"
            flash.args = [params.id]
            flash.defaultMessage = "VehicleStampDutyParameter not found with id ${params.id}"
            redirect(action: "list")
        }
        else {
            return [
            	vehicleStampDutyParameterInstance: vehicleStampDutyParameterInstance,
            	defaultCode: Customer.BASE_CUST_CODE
            ]
        }
    }

    def update = {
        def vehicleStampDutyParameterInstance = VehicleStampDutyParameter.get(params.id)
        if (vehicleStampDutyParameterInstance) {
            if (params.version) {
                def version = params.version.toLong()
                if (vehicleStampDutyParameterInstance.version > version) {
                    
                    vehicleStampDutyParameterInstance.errors.rejectValue("version", "vehicleStampDutyParameter.optimistic.locking.failure", "Another user has updated this VehicleStampDutyParameter while you were editing")
                    render(view: "edit", model: [vehicleStampDutyParameterInstance: vehicleStampDutyParameterInstance, defaultCode: Customer.BASE_CUST_CODE])
                    return
                }
            }
            vehicleStampDutyParameterInstance.properties = params
            if (!vehicleStampDutyParameterInstance.hasErrors() && vehicleStampDutyParameterInstance.save()) {
                flash.message = "vehicleStampDutyParameter.updated"
                flash.args = [params.id]
                flash.defaultMessage = "VehicleStampDutyParameter ${params.id} updated"
                redirect(action: "list")//, id: vehicleStampDutyParameterInstance.id)
            }
            else {
                render(view: "edit", model: [vehicleStampDutyParameterInstance: vehicleStampDutyParameterInstance, defaultCode: Customer.BASE_CUST_CODE])
            }
        }
        else {
            flash.message = "vehicleStampDutyParameter.not.found"
            flash.args = [params.id]
            flash.defaultMessage = "VehicleStampDutyParameter not found with id ${params.id}"
            redirect(action: "edit", id: params.id)
        }
    }

    def delete = {
        def vehicleStampDutyParameterInstance = VehicleStampDutyParameter.get(params.id)
        if (vehicleStampDutyParameterInstance) {
            try {
                vehicleStampDutyParameterInstance.delete()
                flash.message = "vehicleStampDutyParameter.deleted"
                flash.args = [params.id]
                flash.defaultMessage = "VehicleStampDutyParameter ${params.id} deleted"
                redirect(action: "list")
            }
            catch (org.springframework.dao.DataIntegrityViolationException e) {
                flash.message = "vehicleStampDutyParameter.not.deleted"
                flash.args = [params.id]
                flash.defaultMessage = "VehicleStampDutyParameter ${params.id} could not be deleted"
                redirect(action: "edit", id: params.id)
            }
        }
        else {
            flash.message = "vehicleStampDutyParameter.not.found"
            flash.args = [params.id]
            flash.defaultMessage = "VehicleStampDutyParameter not found with id ${params.id}"
            redirect(action: "list")
        }
    }
}
