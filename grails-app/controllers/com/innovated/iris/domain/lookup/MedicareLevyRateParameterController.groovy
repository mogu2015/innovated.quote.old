package com.innovated.iris.domain.lookup

import org.codehaus.groovy.grails.plugins.springsecurity.Secured;

@Secured(['ROLE_SUPER', 'ROLE_ADMIN'])
class MedicareLevyRateParameterController {

    def index = { redirect(action: "list", params: params) }

    // the delete, save and update actions only accept POST requests
    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def list = {
        params.max = Math.min(params.max ? params.max.toInteger() : 10,  100)
        [medicareLevyRateParameterInstanceList: MedicareLevyRateParameter.list(params), medicareLevyRateParameterInstanceTotal: MedicareLevyRateParameter.count()]
    }

    def create = {
        def medicareLevyRateParameterInstance = new MedicareLevyRateParameter()
        medicareLevyRateParameterInstance.properties = params
        return [medicareLevyRateParameterInstance: medicareLevyRateParameterInstance]
    }

    def save = {
        def medicareLevyRateParameterInstance = new MedicareLevyRateParameter(params)
        if (!medicareLevyRateParameterInstance.hasErrors() && medicareLevyRateParameterInstance.save()) {
            flash.message = "medicareLevyRateParameter.created"
            flash.args = [medicareLevyRateParameterInstance.id]
            flash.defaultMessage = "MedicareLevyRateParameter ${medicareLevyRateParameterInstance.id} created"
            redirect(action: "show", id: medicareLevyRateParameterInstance.id)
        }
        else {
            render(view: "create", model: [medicareLevyRateParameterInstance: medicareLevyRateParameterInstance])
        }
    }

    def show = {
        def medicareLevyRateParameterInstance = MedicareLevyRateParameter.get(params.id)
        if (!medicareLevyRateParameterInstance) {
            flash.message = "medicareLevyRateParameter.not.found"
            flash.args = [params.id]
            flash.defaultMessage = "MedicareLevyRateParameter not found with id ${params.id}"
            redirect(action: "list")
        }
        else {
            return [medicareLevyRateParameterInstance: medicareLevyRateParameterInstance]
        }
    }

    def edit = {
        def medicareLevyRateParameterInstance = MedicareLevyRateParameter.get(params.id)
        if (!medicareLevyRateParameterInstance) {
            flash.message = "medicareLevyRateParameter.not.found"
            flash.args = [params.id]
            flash.defaultMessage = "MedicareLevyRateParameter not found with id ${params.id}"
            redirect(action: "list")
        }
        else {
            return [medicareLevyRateParameterInstance: medicareLevyRateParameterInstance]
        }
    }

    def update = {
        def medicareLevyRateParameterInstance = MedicareLevyRateParameter.get(params.id)
        if (medicareLevyRateParameterInstance) {
            if (params.version) {
                def version = params.version.toLong()
                if (medicareLevyRateParameterInstance.version > version) {
                    
                    medicareLevyRateParameterInstance.errors.rejectValue("version", "medicareLevyRateParameter.optimistic.locking.failure", "Another user has updated this MedicareLevyRateParameter while you were editing")
                    render(view: "edit", model: [medicareLevyRateParameterInstance: medicareLevyRateParameterInstance])
                    return
                }
            }
            medicareLevyRateParameterInstance.properties = params
            if (!medicareLevyRateParameterInstance.hasErrors() && medicareLevyRateParameterInstance.save()) {
                flash.message = "medicareLevyRateParameter.updated"
                flash.args = [params.id]
                flash.defaultMessage = "MedicareLevyRateParameter ${params.id} updated"
                redirect(action: "show", id: medicareLevyRateParameterInstance.id)
            }
            else {
                render(view: "edit", model: [medicareLevyRateParameterInstance: medicareLevyRateParameterInstance])
            }
        }
        else {
            flash.message = "medicareLevyRateParameter.not.found"
            flash.args = [params.id]
            flash.defaultMessage = "MedicareLevyRateParameter not found with id ${params.id}"
            redirect(action: "edit", id: params.id)
        }
    }

    def delete = {
        def medicareLevyRateParameterInstance = MedicareLevyRateParameter.get(params.id)
        if (medicareLevyRateParameterInstance) {
            try {
                medicareLevyRateParameterInstance.delete()
                flash.message = "medicareLevyRateParameter.deleted"
                flash.args = [params.id]
                flash.defaultMessage = "MedicareLevyRateParameter ${params.id} deleted"
                redirect(action: "list")
            }
            catch (org.springframework.dao.DataIntegrityViolationException e) {
                flash.message = "medicareLevyRateParameter.not.deleted"
                flash.args = [params.id]
                flash.defaultMessage = "MedicareLevyRateParameter ${params.id} could not be deleted"
                redirect(action: "show", id: params.id)
            }
        }
        else {
            flash.message = "medicareLevyRateParameter.not.found"
            flash.args = [params.id]
            flash.defaultMessage = "MedicareLevyRateParameter not found with id ${params.id}"
            redirect(action: "list")
        }
    }
}
