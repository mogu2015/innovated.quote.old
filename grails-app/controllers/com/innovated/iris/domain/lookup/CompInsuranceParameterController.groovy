package com.innovated.iris.domain.lookup

import org.codehaus.groovy.grails.plugins.springsecurity.Secured;

@Secured(['ROLE_SUPER', 'ROLE_ADMIN'])
class CompInsuranceParameterController {

    def index = { redirect(action: "list", params: params) }

    // the delete, save and update actions only accept POST requests
    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def list = {
        params.max = Math.min(params.max ? params.max.toInteger() : 10,  100)
        [compInsuranceParameterInstanceList: CompInsuranceParameter.list(params), compInsuranceParameterInstanceTotal: CompInsuranceParameter.count()]
    }

    def create = {
        def compInsuranceParameterInstance = new CompInsuranceParameter()
        compInsuranceParameterInstance.properties = params
        return [compInsuranceParameterInstance: compInsuranceParameterInstance]
    }

    def save = {
        def compInsuranceParameterInstance = new CompInsuranceParameter(params)
        if (!compInsuranceParameterInstance.hasErrors() && compInsuranceParameterInstance.save()) {
            flash.message = "compInsuranceParameter.created"
            flash.args = [compInsuranceParameterInstance.id]
            flash.defaultMessage = "CompInsuranceParameter ${compInsuranceParameterInstance.id} created"
            redirect(action: "show", id: compInsuranceParameterInstance.id)
        }
        else {
            render(view: "create", model: [compInsuranceParameterInstance: compInsuranceParameterInstance])
        }
    }

    def show = {
        def compInsuranceParameterInstance = CompInsuranceParameter.get(params.id)
        if (!compInsuranceParameterInstance) {
            flash.message = "compInsuranceParameter.not.found"
            flash.args = [params.id]
            flash.defaultMessage = "CompInsuranceParameter not found with id ${params.id}"
            redirect(action: "list")
        }
        else {
            return [compInsuranceParameterInstance: compInsuranceParameterInstance]
        }
    }

    def edit = {
        def compInsuranceParameterInstance = CompInsuranceParameter.get(params.id)
        if (!compInsuranceParameterInstance) {
            flash.message = "compInsuranceParameter.not.found"
            flash.args = [params.id]
            flash.defaultMessage = "CompInsuranceParameter not found with id ${params.id}"
            redirect(action: "list")
        }
        else {
            return [compInsuranceParameterInstance: compInsuranceParameterInstance]
        }
    }

    def update = {
        def compInsuranceParameterInstance = CompInsuranceParameter.get(params.id)
        if (compInsuranceParameterInstance) {
            if (params.version) {
                def version = params.version.toLong()
                if (compInsuranceParameterInstance.version > version) {
                    
                    compInsuranceParameterInstance.errors.rejectValue("version", "compInsuranceParameter.optimistic.locking.failure", "Another user has updated this CompInsuranceParameter while you were editing")
                    render(view: "edit", model: [compInsuranceParameterInstance: compInsuranceParameterInstance])
                    return
                }
            }
            compInsuranceParameterInstance.properties = params
            if (!compInsuranceParameterInstance.hasErrors() && compInsuranceParameterInstance.save()) {
                flash.message = "compInsuranceParameter.updated"
                flash.args = [params.id]
                flash.defaultMessage = "CompInsuranceParameter ${params.id} updated"
                redirect(action: "show", id: compInsuranceParameterInstance.id)
            }
            else {
                render(view: "edit", model: [compInsuranceParameterInstance: compInsuranceParameterInstance])
            }
        }
        else {
            flash.message = "compInsuranceParameter.not.found"
            flash.args = [params.id]
            flash.defaultMessage = "CompInsuranceParameter not found with id ${params.id}"
            redirect(action: "edit", id: params.id)
        }
    }

    def delete = {
        def compInsuranceParameterInstance = CompInsuranceParameter.get(params.id)
        if (compInsuranceParameterInstance) {
            try {
                compInsuranceParameterInstance.delete()
                flash.message = "compInsuranceParameter.deleted"
                flash.args = [params.id]
                flash.defaultMessage = "CompInsuranceParameter ${params.id} deleted"
                redirect(action: "list")
            }
            catch (org.springframework.dao.DataIntegrityViolationException e) {
                flash.message = "compInsuranceParameter.not.deleted"
                flash.args = [params.id]
                flash.defaultMessage = "CompInsuranceParameter ${params.id} could not be deleted"
                redirect(action: "show", id: params.id)
            }
        }
        else {
            flash.message = "compInsuranceParameter.not.found"
            flash.args = [params.id]
            flash.defaultMessage = "CompInsuranceParameter not found with id ${params.id}"
            redirect(action: "list")
        }
    }
}
