package com.innovated.iris.domain.lookup

import org.codehaus.groovy.grails.plugins.springsecurity.Secured;

@Secured(['ROLE_SUPER', 'ROLE_ADMIN'])
class IncomeTaxRateParameterController {

    def index = { redirect(action: "list", params: params) }

    // the delete, save and update actions only accept POST requests
    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def list = {
        params.max = Math.min(params.max ? params.max.toInteger() : 10,  100)
        [incomeTaxRateParameterInstanceList: IncomeTaxRateParameter.list(params), incomeTaxRateParameterInstanceTotal: IncomeTaxRateParameter.count()]
    }

    def create = {
        def incomeTaxRateParameterInstance = new IncomeTaxRateParameter()
        incomeTaxRateParameterInstance.properties = params
        return [incomeTaxRateParameterInstance: incomeTaxRateParameterInstance]
    }

    def save = {
        def incomeTaxRateParameterInstance = new IncomeTaxRateParameter(params)
        if (!incomeTaxRateParameterInstance.hasErrors() && incomeTaxRateParameterInstance.save()) {
            flash.message = "incomeTaxRateParameter.created"
            flash.args = [incomeTaxRateParameterInstance.id]
            flash.defaultMessage = "IncomeTaxRateParameter ${incomeTaxRateParameterInstance.id} created"
            redirect(action: "show", id: incomeTaxRateParameterInstance.id)
        }
        else {
            render(view: "create", model: [incomeTaxRateParameterInstance: incomeTaxRateParameterInstance])
        }
    }

    def show = {
        def incomeTaxRateParameterInstance = IncomeTaxRateParameter.get(params.id)
        if (!incomeTaxRateParameterInstance) {
            flash.message = "incomeTaxRateParameter.not.found"
            flash.args = [params.id]
            flash.defaultMessage = "IncomeTaxRateParameter not found with id ${params.id}"
            redirect(action: "list")
        }
        else {
            return [incomeTaxRateParameterInstance: incomeTaxRateParameterInstance]
        }
    }

    def edit = {
        def incomeTaxRateParameterInstance = IncomeTaxRateParameter.get(params.id)
        if (!incomeTaxRateParameterInstance) {
            flash.message = "incomeTaxRateParameter.not.found"
            flash.args = [params.id]
            flash.defaultMessage = "IncomeTaxRateParameter not found with id ${params.id}"
            redirect(action: "list")
        }
        else {
            return [incomeTaxRateParameterInstance: incomeTaxRateParameterInstance]
        }
    }

    def update = {
        def incomeTaxRateParameterInstance = IncomeTaxRateParameter.get(params.id)
        if (incomeTaxRateParameterInstance) {
            if (params.version) {
                def version = params.version.toLong()
                if (incomeTaxRateParameterInstance.version > version) {
                    
                    incomeTaxRateParameterInstance.errors.rejectValue("version", "incomeTaxRateParameter.optimistic.locking.failure", "Another user has updated this IncomeTaxRateParameter while you were editing")
                    render(view: "edit", model: [incomeTaxRateParameterInstance: incomeTaxRateParameterInstance])
                    return
                }
            }
            incomeTaxRateParameterInstance.properties = params
            if (!incomeTaxRateParameterInstance.hasErrors() && incomeTaxRateParameterInstance.save()) {
                flash.message = "incomeTaxRateParameter.updated"
                flash.args = [params.id]
                flash.defaultMessage = "IncomeTaxRateParameter ${params.id} updated"
                redirect(action: "show", id: incomeTaxRateParameterInstance.id)
            }
            else {
                render(view: "edit", model: [incomeTaxRateParameterInstance: incomeTaxRateParameterInstance])
            }
        }
        else {
            flash.message = "incomeTaxRateParameter.not.found"
            flash.args = [params.id]
            flash.defaultMessage = "IncomeTaxRateParameter not found with id ${params.id}"
            redirect(action: "edit", id: params.id)
        }
    }

    def delete = {
        def incomeTaxRateParameterInstance = IncomeTaxRateParameter.get(params.id)
        if (incomeTaxRateParameterInstance) {
            try {
                incomeTaxRateParameterInstance.delete()
                flash.message = "incomeTaxRateParameter.deleted"
                flash.args = [params.id]
                flash.defaultMessage = "IncomeTaxRateParameter ${params.id} deleted"
                redirect(action: "list")
            }
            catch (org.springframework.dao.DataIntegrityViolationException e) {
                flash.message = "incomeTaxRateParameter.not.deleted"
                flash.args = [params.id]
                flash.defaultMessage = "IncomeTaxRateParameter ${params.id} could not be deleted"
                redirect(action: "show", id: params.id)
            }
        }
        else {
            flash.message = "incomeTaxRateParameter.not.found"
            flash.args = [params.id]
            flash.defaultMessage = "IncomeTaxRateParameter not found with id ${params.id}"
            redirect(action: "list")
        }
    }
}
