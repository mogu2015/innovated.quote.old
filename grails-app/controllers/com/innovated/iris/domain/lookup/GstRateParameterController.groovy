package com.innovated.iris.domain.lookup

import org.codehaus.groovy.grails.plugins.springsecurity.Secured;

@Secured(['ROLE_SUPER', 'ROLE_ADMIN'])
class GstRateParameterController {

    def index = { redirect(action: "list", params: params) }

    // the delete, save and update actions only accept POST requests
    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def list = {
        params.max = Math.min(params.max ? params.max.toInteger() : 10,  100)
        [gstRateParameterInstanceList: GstRateParameter.list(params), gstRateParameterInstanceTotal: GstRateParameter.count()]
    }

    def create = {
        def gstRateParameterInstance = new GstRateParameter()
        gstRateParameterInstance.properties = params
        return [gstRateParameterInstance: gstRateParameterInstance]
    }

    def save = {
        def gstRateParameterInstance = new GstRateParameter(params)
        if (!gstRateParameterInstance.hasErrors() && gstRateParameterInstance.save()) {
            flash.message = "gstRateParameter.created"
            flash.args = [gstRateParameterInstance.id]
            flash.defaultMessage = "GstRateParameter ${gstRateParameterInstance.id} created"
            redirect(action: "show", id: gstRateParameterInstance.id)
        }
        else {
            render(view: "create", model: [gstRateParameterInstance: gstRateParameterInstance])
        }
    }

    def show = {
        def gstRateParameterInstance = GstRateParameter.get(params.id)
        if (!gstRateParameterInstance) {
            flash.message = "gstRateParameter.not.found"
            flash.args = [params.id]
            flash.defaultMessage = "GstRateParameter not found with id ${params.id}"
            redirect(action: "list")
        }
        else {
            return [gstRateParameterInstance: gstRateParameterInstance]
        }
    }

    def edit = {
        def gstRateParameterInstance = GstRateParameter.get(params.id)
        if (!gstRateParameterInstance) {
            flash.message = "gstRateParameter.not.found"
            flash.args = [params.id]
            flash.defaultMessage = "GstRateParameter not found with id ${params.id}"
            redirect(action: "list")
        }
        else {
            return [gstRateParameterInstance: gstRateParameterInstance]
        }
    }

    def update = {
        def gstRateParameterInstance = GstRateParameter.get(params.id)
        if (gstRateParameterInstance) {
            if (params.version) {
                def version = params.version.toLong()
                if (gstRateParameterInstance.version > version) {
                    
                    gstRateParameterInstance.errors.rejectValue("version", "gstRateParameter.optimistic.locking.failure", "Another user has updated this GstRateParameter while you were editing")
                    render(view: "edit", model: [gstRateParameterInstance: gstRateParameterInstance])
                    return
                }
            }
            gstRateParameterInstance.properties = params
            if (!gstRateParameterInstance.hasErrors() && gstRateParameterInstance.save()) {
                flash.message = "gstRateParameter.updated"
                flash.args = [params.id]
                flash.defaultMessage = "GstRateParameter ${params.id} updated"
                redirect(action: "show", id: gstRateParameterInstance.id)
            }
            else {
                render(view: "edit", model: [gstRateParameterInstance: gstRateParameterInstance])
            }
        }
        else {
            flash.message = "gstRateParameter.not.found"
            flash.args = [params.id]
            flash.defaultMessage = "GstRateParameter not found with id ${params.id}"
            redirect(action: "edit", id: params.id)
        }
    }

    def delete = {
        def gstRateParameterInstance = GstRateParameter.get(params.id)
        if (gstRateParameterInstance) {
            try {
                gstRateParameterInstance.delete()
                flash.message = "gstRateParameter.deleted"
                flash.args = [params.id]
                flash.defaultMessage = "GstRateParameter ${params.id} deleted"
                redirect(action: "list")
            }
            catch (org.springframework.dao.DataIntegrityViolationException e) {
                flash.message = "gstRateParameter.not.deleted"
                flash.args = [params.id]
                flash.defaultMessage = "GstRateParameter ${params.id} could not be deleted"
                redirect(action: "show", id: params.id)
            }
        }
        else {
            flash.message = "gstRateParameter.not.found"
            flash.args = [params.id]
            flash.defaultMessage = "GstRateParameter not found with id ${params.id}"
            redirect(action: "list")
        }
    }
}
