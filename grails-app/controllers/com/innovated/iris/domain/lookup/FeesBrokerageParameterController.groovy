package com.innovated.iris.domain.lookup

import org.codehaus.groovy.grails.plugins.springsecurity.Secured;

@Secured(['ROLE_SUPER', 'ROLE_ADMIN'])
class FeesBrokerageParameterController {

    def index = { redirect(action: "list", params: params) }

    // the delete, save and update actions only accept POST requests
    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def list = {
        params.max = Math.min(params.max ? params.max.toInteger() : 10,  100)
        [feesBrokerageParameterInstanceList: FeesBrokerageParameter.list(params), feesBrokerageParameterInstanceTotal: FeesBrokerageParameter.count()]
    }

    def create = {
        def feesBrokerageParameterInstance = new FeesBrokerageParameter()
        feesBrokerageParameterInstance.properties = params
        return [feesBrokerageParameterInstance: feesBrokerageParameterInstance]
    }

    def save = {
        def feesBrokerageParameterInstance = new FeesBrokerageParameter(params)
        if (!feesBrokerageParameterInstance.hasErrors() && feesBrokerageParameterInstance.save()) {
            flash.message = "feesBrokerageParameter.created"
            flash.args = [feesBrokerageParameterInstance.id]
            flash.defaultMessage = "FeesBrokerageParameter ${feesBrokerageParameterInstance.id} created"
            redirect(action: "show", id: feesBrokerageParameterInstance.id)
        }
        else {
            render(view: "create", model: [feesBrokerageParameterInstance: feesBrokerageParameterInstance])
        }
    }

    def show = {
        def feesBrokerageParameterInstance = FeesBrokerageParameter.get(params.id)
        if (!feesBrokerageParameterInstance) {
            flash.message = "feesBrokerageParameter.not.found"
            flash.args = [params.id]
            flash.defaultMessage = "FeesBrokerageParameter not found with id ${params.id}"
            redirect(action: "list")
        }
        else {
            return [feesBrokerageParameterInstance: feesBrokerageParameterInstance]
        }
    }

    def edit = {
        def feesBrokerageParameterInstance = FeesBrokerageParameter.get(params.id)
        if (!feesBrokerageParameterInstance) {
            flash.message = "feesBrokerageParameter.not.found"
            flash.args = [params.id]
            flash.defaultMessage = "FeesBrokerageParameter not found with id ${params.id}"
            redirect(action: "list")
        }
        else {
            return [feesBrokerageParameterInstance: feesBrokerageParameterInstance]
        }
    }

    def update = {
        def feesBrokerageParameterInstance = FeesBrokerageParameter.get(params.id)
        if (feesBrokerageParameterInstance) {
            if (params.version) {
                def version = params.version.toLong()
                if (feesBrokerageParameterInstance.version > version) {
                    
                    feesBrokerageParameterInstance.errors.rejectValue("version", "feesBrokerageParameter.optimistic.locking.failure", "Another user has updated this FeesBrokerageParameter while you were editing")
                    render(view: "edit", model: [feesBrokerageParameterInstance: feesBrokerageParameterInstance])
                    return
                }
            }
            feesBrokerageParameterInstance.properties = params
            if (!feesBrokerageParameterInstance.hasErrors() && feesBrokerageParameterInstance.save()) {
                flash.message = "feesBrokerageParameter.updated"
                flash.args = [params.id]
                flash.defaultMessage = "FeesBrokerageParameter ${params.id} updated"
                redirect(action: "show", id: feesBrokerageParameterInstance.id)
            }
            else {
                render(view: "edit", model: [feesBrokerageParameterInstance: feesBrokerageParameterInstance])
            }
        }
        else {
            flash.message = "feesBrokerageParameter.not.found"
            flash.args = [params.id]
            flash.defaultMessage = "FeesBrokerageParameter not found with id ${params.id}"
            redirect(action: "edit", id: params.id)
        }
    }

    def delete = {
        def feesBrokerageParameterInstance = FeesBrokerageParameter.get(params.id)
        if (feesBrokerageParameterInstance) {
            try {
                feesBrokerageParameterInstance.delete()
                flash.message = "feesBrokerageParameter.deleted"
                flash.args = [params.id]
                flash.defaultMessage = "FeesBrokerageParameter ${params.id} deleted"
                redirect(action: "list")
            }
            catch (org.springframework.dao.DataIntegrityViolationException e) {
                flash.message = "feesBrokerageParameter.not.deleted"
                flash.args = [params.id]
                flash.defaultMessage = "FeesBrokerageParameter ${params.id} could not be deleted"
                redirect(action: "show", id: params.id)
            }
        }
        else {
            flash.message = "feesBrokerageParameter.not.found"
            flash.args = [params.id]
            flash.defaultMessage = "FeesBrokerageParameter not found with id ${params.id}"
            redirect(action: "list")
        }
    }
}
