package com.innovated.iris.domain.lookup

import org.codehaus.groovy.grails.plugins.springsecurity.Secured;

@Secured(['ROLE_SUPER', 'ROLE_ADMIN'])
class HecsHelpRateParameterController {

    def index = { redirect(action: "list", params: params) }

    // the delete, save and update actions only accept POST requests
    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def list = {
        params.max = Math.min(params.max ? params.max.toInteger() : 10,  100)
        [hecsHelpRateParameterInstanceList: HecsHelpRateParameter.list(params), hecsHelpRateParameterInstanceTotal: HecsHelpRateParameter.count()]
    }

    def create = {
        def hecsHelpRateParameterInstance = new HecsHelpRateParameter()
        hecsHelpRateParameterInstance.properties = params
        return [hecsHelpRateParameterInstance: hecsHelpRateParameterInstance]
    }

    def save = {
        def hecsHelpRateParameterInstance = new HecsHelpRateParameter(params)
        if (!hecsHelpRateParameterInstance.hasErrors() && hecsHelpRateParameterInstance.save()) {
            flash.message = "hecsHelpRateParameter.created"
            flash.args = [hecsHelpRateParameterInstance.id]
            flash.defaultMessage = "HecsHelpRateParameter ${hecsHelpRateParameterInstance.id} created"
            redirect(action: "show", id: hecsHelpRateParameterInstance.id)
        }
        else {
            render(view: "create", model: [hecsHelpRateParameterInstance: hecsHelpRateParameterInstance])
        }
    }

    def show = {
        def hecsHelpRateParameterInstance = HecsHelpRateParameter.get(params.id)
        if (!hecsHelpRateParameterInstance) {
            flash.message = "hecsHelpRateParameter.not.found"
            flash.args = [params.id]
            flash.defaultMessage = "HecsHelpRateParameter not found with id ${params.id}"
            redirect(action: "list")
        }
        else {
            return [hecsHelpRateParameterInstance: hecsHelpRateParameterInstance]
        }
    }

    def edit = {
        def hecsHelpRateParameterInstance = HecsHelpRateParameter.get(params.id)
        if (!hecsHelpRateParameterInstance) {
            flash.message = "hecsHelpRateParameter.not.found"
            flash.args = [params.id]
            flash.defaultMessage = "HecsHelpRateParameter not found with id ${params.id}"
            redirect(action: "list")
        }
        else {
            return [hecsHelpRateParameterInstance: hecsHelpRateParameterInstance]
        }
    }

    def update = {
        def hecsHelpRateParameterInstance = HecsHelpRateParameter.get(params.id)
        if (hecsHelpRateParameterInstance) {
            if (params.version) {
                def version = params.version.toLong()
                if (hecsHelpRateParameterInstance.version > version) {
                    
                    hecsHelpRateParameterInstance.errors.rejectValue("version", "hecsHelpRateParameter.optimistic.locking.failure", "Another user has updated this HecsHelpRateParameter while you were editing")
                    render(view: "edit", model: [hecsHelpRateParameterInstance: hecsHelpRateParameterInstance])
                    return
                }
            }
            hecsHelpRateParameterInstance.properties = params
            if (!hecsHelpRateParameterInstance.hasErrors() && hecsHelpRateParameterInstance.save()) {
                flash.message = "hecsHelpRateParameter.updated"
                flash.args = [params.id]
                flash.defaultMessage = "HecsHelpRateParameter ${params.id} updated"
                redirect(action: "show", id: hecsHelpRateParameterInstance.id)
            }
            else {
                render(view: "edit", model: [hecsHelpRateParameterInstance: hecsHelpRateParameterInstance])
            }
        }
        else {
            flash.message = "hecsHelpRateParameter.not.found"
            flash.args = [params.id]
            flash.defaultMessage = "HecsHelpRateParameter not found with id ${params.id}"
            redirect(action: "edit", id: params.id)
        }
    }

    def delete = {
        def hecsHelpRateParameterInstance = HecsHelpRateParameter.get(params.id)
        if (hecsHelpRateParameterInstance) {
            try {
                hecsHelpRateParameterInstance.delete()
                flash.message = "hecsHelpRateParameter.deleted"
                flash.args = [params.id]
                flash.defaultMessage = "HecsHelpRateParameter ${params.id} deleted"
                redirect(action: "list")
            }
            catch (org.springframework.dao.DataIntegrityViolationException e) {
                flash.message = "hecsHelpRateParameter.not.deleted"
                flash.args = [params.id]
                flash.defaultMessage = "HecsHelpRateParameter ${params.id} could not be deleted"
                redirect(action: "show", id: params.id)
            }
        }
        else {
            flash.message = "hecsHelpRateParameter.not.found"
            flash.args = [params.id]
            flash.defaultMessage = "HecsHelpRateParameter not found with id ${params.id}"
            redirect(action: "list")
        }
    }
}
