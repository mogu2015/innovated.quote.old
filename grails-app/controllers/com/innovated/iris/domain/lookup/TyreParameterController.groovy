package com.innovated.iris.domain.lookup

import org.codehaus.groovy.grails.plugins.springsecurity.Secured

import com.innovated.iris.util.*
import com.innovated.iris.domain.*
import com.innovated.iris.domain.enums.*

import org.springframework.web.multipart.MultipartHttpServletRequest
import org.springframework.web.multipart.commons.CommonsMultipartFile

import org.apache.commons.net.ftp.FTPClient
import java.util.regex.*
import jxl.*

//@Secured(['IS_AUTHENTICATED_FULLY'])
@Secured(['ROLE_SUPER', 'ROLE_ADMIN'])
class TyreParameterController {

    def index = { redirect(action: "list", params: params) }

    // the delete, save and update actions only accept POST requests
    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def list = {
        params.max = Math.min(params.max ? params.max.toInteger() : 100,  100)
        [tyreParameterInstanceList: TyreParameter.list(params), tyreParameterInstanceTotal: TyreParameter.count()]
    }
    
    def importFile = {
    	render(view: "import", model: [:])
    }
    def saveXls = {
    	//println "Importing Tyre Costs from XLS file!"

		if(request instanceof MultipartHttpServletRequest) {
			// get our multipart  
			MultipartHttpServletRequest mpr = (MultipartHttpServletRequest)request;
			CommonsMultipartFile file = (CommonsMultipartFile) mpr.getFile("importFile");
			
			if (!file.empty) {
				// create our workbook
				Workbook workbook = Workbook.getWorkbook(file.inputStream)
				Sheet sheet = workbook.getSheet(0)
				
				def sizeMap = [:]
				def added = 0, updated = 0
				def skipped = []
			
				// skip header row, start at row 1...
				for (int r=1; r<sheet.rows; r++) {
					// get our fields
					def sizeName = sheet.getCell(0, r).contents
					def brand = sheet.getCell(1, r).contents
					def price = sheet.getCell(5, r).contents
					
					Pattern p = Pattern.compile(TyreParameter.SIZE_REGEX)
					Matcher m = p.matcher(sizeName)
					
					if (m.matches()) {
						// build "clean" size string...
						def sizeStr = "${m.group(1)}/${m.group(2)}R${m.group(3)}"
						def v = sizeMap.get(sizeStr)
						if (v) {
							//println "Tyre size already exists in map: ${v.size}"
							if (price && price!="") {
								v.add(CurrencyUtils.roundNearestCent(Double.parseDouble(price)))
							}
						}
						else {
							if (price && price!="") {
								//println "Tyre size DOES NOT EXIST in the map!: ${sizeName}, adding..."
								def prices = []
								prices.add(CurrencyUtils.roundNearestCent(Double.parseDouble(price)))
								sizeMap.put(sizeStr, prices)
							}
						}
					}
					else {
						//println "invalid tyre size format: ${sizeName}"
					}
				}
				workbook.close()
				
				// do the stats...
				//println "# of tyre sizes = " + sizeMap.size()
				
				def importDate = new Date()
				if (params.importAsAt) {
					importDate = params.importAsAt
				}
				def c = Calendar.getInstance();
				c.setTime(importDate);
				c.set(Calendar.HOUR_OF_DAY, 0);
				c.set(Calendar.MINUTE, 0);
				c.set(Calendar.SECOND, 0);
				c.set(Calendar.MILLISECOND, 0);
				println "import date = ${c.time}"
				
				sizeMap.each { k, v ->
					def values = v as double[]
					
					def median = 0.0
					int middle = values.length / 2;  // subscript of middle element
					if (values.length%2 == 1) {
						median = values[middle];
					} else {
						median = (values[middle-1] + values[middle]) / 2.0;
					}
					median = CurrencyUtils.roundNearestCent(median)
					
					def average = CurrencyUtils.roundNearestCent(v.sum() / v.size)
					
					//println "size = ${k}, min = ${v.min()}, max = ${v.max()}, median = ${median}, mean = ${average}, count=${v.size()}"
					
					// save imported tyre prices
					def props = [dateFrom:c.time, customerCode:Customer.BASE_CUST_CODE, size:k, standardPrice:median, premiumPrice:v.max()]
					def isNew = true
					
					def t = TyreParameter.findWhere(dateFrom:c.time, customerCode:Customer.BASE_CUST_CODE, size:k)
					if (t) {	// tyre exists - updated pricing only
						t.properties = [standardPrice:median, premiumPrice:v.max()]
						isNew = false
					} else {
						t = new TyreParameter(props)
						isNew = true
					}
					
					// show results in flash area
					if (t.validate() && t.save()) {
						if (isNew) added++
						else updated++
					} else {
						skipped << k
					}
				}
				
				
				// generate our flash message
				flash.message = "${added} tyre cost(s) added. (${updated} updated)"
				if (skipped.size() > 0) {
					flash.message += "<br/>Sizes Skipped: ${skipped.join(', ')}"
				}
				redirect(action: "list")
			}
			else {
				flash.message = 'Please select a file to import!'
				redirect(action: "importFile")
			}
		}   
		else {
			flash.message = 'Invalid import (Error: 5301)'
			redirect(action: "importFile")
		}
	}

    def create = {
        def tyreParameterInstance = new TyreParameter()
        tyreParameterInstance.properties = params
        return [tyreParameterInstance: tyreParameterInstance]
    }

    def save = {
        def tyreParameterInstance = new TyreParameter(params)
        if (!tyreParameterInstance.hasErrors() && tyreParameterInstance.save()) {
            flash.message = "tyreParameter.created"
            flash.args = [tyreParameterInstance.id]
            flash.defaultMessage = "TyreParameter ${tyreParameterInstance.id} created"
            redirect(action: "show", id: tyreParameterInstance.id)
        }
        else {
            render(view: "create", model: [tyreParameterInstance: tyreParameterInstance])
        }
    }

    def show = {
        def tyreParameterInstance = TyreParameter.get(params.id)
        if (!tyreParameterInstance) {
            flash.message = "tyreParameter.not.found"
            flash.args = [params.id]
            flash.defaultMessage = "TyreParameter not found with id ${params.id}"
            redirect(action: "list")
        }
        else {
            return [tyreParameterInstance: tyreParameterInstance]
        }
    }

    def edit = {
        def tyreParameterInstance = TyreParameter.get(params.id)
        if (!tyreParameterInstance) {
            flash.message = "tyreParameter.not.found"
            flash.args = [params.id]
            flash.defaultMessage = "TyreParameter not found with id ${params.id}"
            redirect(action: "list")
        }
        else {
            return [tyreParameterInstance: tyreParameterInstance]
        }
    }

    def update = {
        def tyreParameterInstance = TyreParameter.get(params.id)
        if (tyreParameterInstance) {
            if (params.version) {
                def version = params.version.toLong()
                if (tyreParameterInstance.version > version) {
                    
                    tyreParameterInstance.errors.rejectValue("version", "tyreParameter.optimistic.locking.failure", "Another user has updated this TyreParameter while you were editing")
                    render(view: "edit", model: [tyreParameterInstance: tyreParameterInstance])
                    return
                }
            }
            tyreParameterInstance.properties = params
            if (!tyreParameterInstance.hasErrors() && tyreParameterInstance.save()) {
                flash.message = "tyreParameter.updated"
                flash.args = [params.id]
                flash.defaultMessage = "TyreParameter ${params.id} updated"
                redirect(action: "show", id: tyreParameterInstance.id)
            }
            else {
                render(view: "edit", model: [tyreParameterInstance: tyreParameterInstance])
            }
        }
        else {
            flash.message = "tyreParameter.not.found"
            flash.args = [params.id]
            flash.defaultMessage = "TyreParameter not found with id ${params.id}"
            redirect(action: "edit", id: params.id)
        }
    }

    def delete = {
        def tyreParameterInstance = TyreParameter.get(params.id)
        if (tyreParameterInstance) {
            try {
                tyreParameterInstance.delete()
                flash.message = "tyreParameter.deleted"
                flash.args = [params.id]
                flash.defaultMessage = "TyreParameter ${params.id} deleted"
                redirect(action: "list")
            }
            catch (org.springframework.dao.DataIntegrityViolationException e) {
                flash.message = "tyreParameter.not.deleted"
                flash.args = [params.id]
                flash.defaultMessage = "TyreParameter ${params.id} could not be deleted"
                redirect(action: "show", id: params.id)
            }
        }
        else {
            flash.message = "tyreParameter.not.found"
            flash.args = [params.id]
            flash.defaultMessage = "TyreParameter not found with id ${params.id}"
            redirect(action: "list")
        }
    }
}
