package com.innovated.iris.domain.lookup

import org.codehaus.groovy.grails.plugins.springsecurity.Secured;

@Secured(['ROLE_SUPER', 'ROLE_ADMIN'])
class MaintenanceParameterController {

    def index = { redirect(action: "list", params: params) }

    // the delete, save and update actions only accept POST requests
    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def list = {
        params.max = Math.min(params.max ? params.max.toInteger() : 10,  100)
        [maintenanceParameterInstanceList: MaintenanceParameter.list(params), maintenanceParameterInstanceTotal: MaintenanceParameter.count()]
    }

    def create = {
        def maintenanceParameterInstance = new MaintenanceParameter()
        maintenanceParameterInstance.properties = params
        return [maintenanceParameterInstance: maintenanceParameterInstance]
    }

    def save = {
        def maintenanceParameterInstance = new MaintenanceParameter(params)
        if (!maintenanceParameterInstance.hasErrors() && maintenanceParameterInstance.save()) {
            flash.message = "maintenanceParameter.created"
            flash.args = [maintenanceParameterInstance.id]
            flash.defaultMessage = "MaintenanceParameter ${maintenanceParameterInstance.id} created"
            redirect(action: "show", id: maintenanceParameterInstance.id)
        }
        else {
            render(view: "create", model: [maintenanceParameterInstance: maintenanceParameterInstance])
        }
    }

    def show = {
        def maintenanceParameterInstance = MaintenanceParameter.get(params.id)
        if (!maintenanceParameterInstance) {
            flash.message = "maintenanceParameter.not.found"
            flash.args = [params.id]
            flash.defaultMessage = "MaintenanceParameter not found with id ${params.id}"
            redirect(action: "list")
        }
        else {
            return [maintenanceParameterInstance: maintenanceParameterInstance]
        }
    }

    def edit = {
        def maintenanceParameterInstance = MaintenanceParameter.get(params.id)
        if (!maintenanceParameterInstance) {
            flash.message = "maintenanceParameter.not.found"
            flash.args = [params.id]
            flash.defaultMessage = "MaintenanceParameter not found with id ${params.id}"
            redirect(action: "list")
        }
        else {
            return [maintenanceParameterInstance: maintenanceParameterInstance]
        }
    }

    def update = {
        def maintenanceParameterInstance = MaintenanceParameter.get(params.id)
        if (maintenanceParameterInstance) {
            if (params.version) {
                def version = params.version.toLong()
                if (maintenanceParameterInstance.version > version) {
                    
                    maintenanceParameterInstance.errors.rejectValue("version", "maintenanceParameter.optimistic.locking.failure", "Another user has updated this MaintenanceParameter while you were editing")
                    render(view: "edit", model: [maintenanceParameterInstance: maintenanceParameterInstance])
                    return
                }
            }
            maintenanceParameterInstance.properties = params
            if (!maintenanceParameterInstance.hasErrors() && maintenanceParameterInstance.save()) {
                flash.message = "maintenanceParameter.updated"
                flash.args = [params.id]
                flash.defaultMessage = "MaintenanceParameter ${params.id} updated"
                redirect(action: "show", id: maintenanceParameterInstance.id)
            }
            else {
                render(view: "edit", model: [maintenanceParameterInstance: maintenanceParameterInstance])
            }
        }
        else {
            flash.message = "maintenanceParameter.not.found"
            flash.args = [params.id]
            flash.defaultMessage = "MaintenanceParameter not found with id ${params.id}"
            redirect(action: "edit", id: params.id)
        }
    }

    def delete = {
        def maintenanceParameterInstance = MaintenanceParameter.get(params.id)
        if (maintenanceParameterInstance) {
            try {
                maintenanceParameterInstance.delete()
                flash.message = "maintenanceParameter.deleted"
                flash.args = [params.id]
                flash.defaultMessage = "MaintenanceParameter ${params.id} deleted"
                redirect(action: "list")
            }
            catch (org.springframework.dao.DataIntegrityViolationException e) {
                flash.message = "maintenanceParameter.not.deleted"
                flash.args = [params.id]
                flash.defaultMessage = "MaintenanceParameter ${params.id} could not be deleted"
                redirect(action: "show", id: params.id)
            }
        }
        else {
            flash.message = "maintenanceParameter.not.found"
            flash.args = [params.id]
            flash.defaultMessage = "MaintenanceParameter not found with id ${params.id}"
            redirect(action: "list")
        }
    }
}
