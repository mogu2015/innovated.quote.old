package com.innovated.iris.domain.lookup

import org.codehaus.groovy.grails.plugins.springsecurity.Secured;

import com.innovated.iris.domain.*
import com.innovated.iris.domain.enums.*

@Secured(['ROLE_SUPER', 'ROLE_ADMIN'])
class ResidualValueParameterController {

    def index = { redirect(action: "list", params: params) }

    // the delete, save and update actions only accept POST requests
    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def list = {
        params.max = Math.min(params.max ? params.max.toInteger() : 100,  100)
        [residualValueParameterInstanceList: ResidualValueParameter.list(params), residualValueParameterInstanceTotal: ResidualValueParameter.count()]
    }

    def create = {
        def residualValueParameterInstance = new ResidualValueParameter()
        residualValueParameterInstance.properties = params
        
        def custCodes = Customer.withCriteria() {
			order("code", "asc")
			projections {
				groupProperty "code"
			}
		}
		custCodes.add(0,Customer.BASE_CUST_CODE)
		
        return [residualValueParameterInstance: residualValueParameterInstance, customerCodes: custCodes]
    }
    
    def test = {
    	def custCodes = Customer.withCriteria() {
			order("code", "asc")
			projections {
				groupProperty "code"
			}
		}
		custCodes.add(0,Customer.BASE_CUST_CODE)
    	
    	render "test method! : " + c
    }

    def save = {
        def residualValueParameterInstance = new ResidualValueParameter(params)
        if (!residualValueParameterInstance.hasErrors() && residualValueParameterInstance.save()) {
            flash.message = "residualValueParameter.created"
            flash.args = [residualValueParameterInstance.id]
            flash.defaultMessage = "ResidualValueParameter ${residualValueParameterInstance.id} created"
            redirect(action: "show", id: residualValueParameterInstance.id)
        }
        else {
            render(view: "create", model: [residualValueParameterInstance: residualValueParameterInstance])
        }
    }

    def show = {
        def residualValueParameterInstance = ResidualValueParameter.get(params.id)
        if (!residualValueParameterInstance) {
            flash.message = "residualValueParameter.not.found"
            flash.args = [params.id]
            flash.defaultMessage = "ResidualValueParameter not found with id ${params.id}"
            redirect(action: "list")
        }
        else {
            return [residualValueParameterInstance: residualValueParameterInstance]
        }
    }

    def edit = {
        def residualValueParameterInstance = ResidualValueParameter.get(params.id)
        if (!residualValueParameterInstance) {
            flash.message = "residualValueParameter.not.found"
            flash.args = [params.id]
            flash.defaultMessage = "ResidualValueParameter not found with id ${params.id}"
            redirect(action: "list")
        }
        else {
        	def custCodes = Customer.withCriteria() {
				order("code", "asc")
				projections {
					groupProperty "code"
				}
			}
			custCodes.add(0,Customer.BASE_CUST_CODE)
            return [residualValueParameterInstance: residualValueParameterInstance, customerCodes: custCodes]
        }
    }

    def update = {
        def residualValueParameterInstance = ResidualValueParameter.get(params.id)
        if (residualValueParameterInstance) {
            if (params.version) {
                def version = params.version.toLong()
                if (residualValueParameterInstance.version > version) {
                    
                    residualValueParameterInstance.errors.rejectValue("version", "residualValueParameter.optimistic.locking.failure", "Another user has updated this ResidualValueParameter while you were editing")
                    render(view: "edit", model: [residualValueParameterInstance: residualValueParameterInstance])
                    return
                }
            }
            residualValueParameterInstance.properties = params
            if (!residualValueParameterInstance.hasErrors() && residualValueParameterInstance.save()) {
                flash.message = "residualValueParameter.updated"
                flash.args = [params.id]
                flash.defaultMessage = "ResidualValueParameter ${params.id} updated"
                redirect(action: "show", id: residualValueParameterInstance.id)
            }
            else {
                render(view: "edit", model: [residualValueParameterInstance: residualValueParameterInstance])
            }
        }
        else {
            flash.message = "residualValueParameter.not.found"
            flash.args = [params.id]
            flash.defaultMessage = "ResidualValueParameter not found with id ${params.id}"
            redirect(action: "edit", id: params.id)
        }
    }

    def delete = {
        def residualValueParameterInstance = ResidualValueParameter.get(params.id)
        if (residualValueParameterInstance) {
            try {
                residualValueParameterInstance.delete()
                flash.message = "residualValueParameter.deleted"
                flash.args = [params.id]
                flash.defaultMessage = "ResidualValueParameter ${params.id} deleted"
                redirect(action: "list")
            }
            catch (org.springframework.dao.DataIntegrityViolationException e) {
                flash.message = "residualValueParameter.not.deleted"
                flash.args = [params.id]
                flash.defaultMessage = "ResidualValueParameter ${params.id} could not be deleted"
                redirect(action: "show", id: params.id)
            }
        }
        else {
            flash.message = "residualValueParameter.not.found"
            flash.args = [params.id]
            flash.defaultMessage = "ResidualValueParameter not found with id ${params.id}"
            redirect(action: "list")
        }
    }
}
