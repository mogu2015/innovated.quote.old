package com.innovated.iris.domain.lookup

import org.codehaus.groovy.grails.plugins.springsecurity.Secured;

@Secured(['ROLE_SUPER', 'ROLE_ADMIN'])
class FbtStatutoryMethodRateParameterController {

    def index = { redirect(action: "list", params: params) }

    // the delete, save and update actions only accept POST requests
    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def list = {
        params.max = Math.min(params.max ? params.max.toInteger() : 10,  100)
        [fbtStatutoryMethodRateParameterInstanceList: FbtStatutoryMethodRateParameter.list(params), fbtStatutoryMethodRateParameterInstanceTotal: FbtStatutoryMethodRateParameter.count()]
    }

    def create = {
        def fbtStatutoryMethodRateParameterInstance = new FbtStatutoryMethodRateParameter()
        fbtStatutoryMethodRateParameterInstance.properties = params
        return [fbtStatutoryMethodRateParameterInstance: fbtStatutoryMethodRateParameterInstance]
    }

    def save = {
        def fbtStatutoryMethodRateParameterInstance = new FbtStatutoryMethodRateParameter(params)
        if (!fbtStatutoryMethodRateParameterInstance.hasErrors() && fbtStatutoryMethodRateParameterInstance.save()) {
            flash.message = "fbtStatutoryMethodRateParameter.created"
            flash.args = [fbtStatutoryMethodRateParameterInstance.id]
            flash.defaultMessage = "FbtStatutoryMethodRateParameter ${fbtStatutoryMethodRateParameterInstance.id} created"
            redirect(action: "show", id: fbtStatutoryMethodRateParameterInstance.id)
        }
        else {
            render(view: "create", model: [fbtStatutoryMethodRateParameterInstance: fbtStatutoryMethodRateParameterInstance])
        }
    }

    def show = {
        def fbtStatutoryMethodRateParameterInstance = FbtStatutoryMethodRateParameter.get(params.id)
        if (!fbtStatutoryMethodRateParameterInstance) {
            flash.message = "fbtStatutoryMethodRateParameter.not.found"
            flash.args = [params.id]
            flash.defaultMessage = "FbtStatutoryMethodRateParameter not found with id ${params.id}"
            redirect(action: "list")
        }
        else {
            return [fbtStatutoryMethodRateParameterInstance: fbtStatutoryMethodRateParameterInstance]
        }
    }

    def edit = {
        def fbtStatutoryMethodRateParameterInstance = FbtStatutoryMethodRateParameter.get(params.id)
        if (!fbtStatutoryMethodRateParameterInstance) {
            flash.message = "fbtStatutoryMethodRateParameter.not.found"
            flash.args = [params.id]
            flash.defaultMessage = "FbtStatutoryMethodRateParameter not found with id ${params.id}"
            redirect(action: "list")
        }
        else {
            return [fbtStatutoryMethodRateParameterInstance: fbtStatutoryMethodRateParameterInstance]
        }
    }

    def update = {
        def fbtStatutoryMethodRateParameterInstance = FbtStatutoryMethodRateParameter.get(params.id)
        if (fbtStatutoryMethodRateParameterInstance) {
            if (params.version) {
                def version = params.version.toLong()
                if (fbtStatutoryMethodRateParameterInstance.version > version) {
                    
                    fbtStatutoryMethodRateParameterInstance.errors.rejectValue("version", "fbtStatutoryMethodRateParameter.optimistic.locking.failure", "Another user has updated this FbtStatutoryMethodRateParameter while you were editing")
                    render(view: "edit", model: [fbtStatutoryMethodRateParameterInstance: fbtStatutoryMethodRateParameterInstance])
                    return
                }
            }
            fbtStatutoryMethodRateParameterInstance.properties = params
            if (!fbtStatutoryMethodRateParameterInstance.hasErrors() && fbtStatutoryMethodRateParameterInstance.save()) {
                flash.message = "fbtStatutoryMethodRateParameter.updated"
                flash.args = [params.id]
                flash.defaultMessage = "FbtStatutoryMethodRateParameter ${params.id} updated"
                redirect(action: "show", id: fbtStatutoryMethodRateParameterInstance.id)
            }
            else {
                render(view: "edit", model: [fbtStatutoryMethodRateParameterInstance: fbtStatutoryMethodRateParameterInstance])
            }
        }
        else {
            flash.message = "fbtStatutoryMethodRateParameter.not.found"
            flash.args = [params.id]
            flash.defaultMessage = "FbtStatutoryMethodRateParameter not found with id ${params.id}"
            redirect(action: "edit", id: params.id)
        }
    }

    def delete = {
        def fbtStatutoryMethodRateParameterInstance = FbtStatutoryMethodRateParameter.get(params.id)
        if (fbtStatutoryMethodRateParameterInstance) {
            try {
                fbtStatutoryMethodRateParameterInstance.delete()
                flash.message = "fbtStatutoryMethodRateParameter.deleted"
                flash.args = [params.id]
                flash.defaultMessage = "FbtStatutoryMethodRateParameter ${params.id} deleted"
                redirect(action: "list")
            }
            catch (org.springframework.dao.DataIntegrityViolationException e) {
                flash.message = "fbtStatutoryMethodRateParameter.not.deleted"
                flash.args = [params.id]
                flash.defaultMessage = "FbtStatutoryMethodRateParameter ${params.id} could not be deleted"
                redirect(action: "show", id: params.id)
            }
        }
        else {
            flash.message = "fbtStatutoryMethodRateParameter.not.found"
            flash.args = [params.id]
            flash.defaultMessage = "FbtStatutoryMethodRateParameter not found with id ${params.id}"
            redirect(action: "list")
        }
    }
}
