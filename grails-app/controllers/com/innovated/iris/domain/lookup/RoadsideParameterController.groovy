package com.innovated.iris.domain.lookup

import org.codehaus.groovy.grails.plugins.springsecurity.Secured;

@Secured(['ROLE_SUPER', 'ROLE_ADMIN'])
class RoadsideParameterController {

    def index = { redirect(action: "list", params: params) }

    // the delete, save and update actions only accept POST requests
    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def list = {
        params.max = Math.min(params.max ? params.max.toInteger() : 10,  100)
        [roadsideParameterInstanceList: RoadsideParameter.list(params), roadsideParameterInstanceTotal: RoadsideParameter.count()]
    }

    def create = {
        def roadsideParameterInstance = new RoadsideParameter()
        roadsideParameterInstance.properties = params
        return [roadsideParameterInstance: roadsideParameterInstance]
    }

    def save = {
        def roadsideParameterInstance = new RoadsideParameter(params)
        if (!roadsideParameterInstance.hasErrors() && roadsideParameterInstance.save()) {
            flash.message = "roadsideParameter.created"
            flash.args = [roadsideParameterInstance.id]
            flash.defaultMessage = "RoadsideParameter ${roadsideParameterInstance.id} created"
            redirect(action: "show", id: roadsideParameterInstance.id)
        }
        else {
            render(view: "create", model: [roadsideParameterInstance: roadsideParameterInstance])
        }
    }

    def show = {
        def roadsideParameterInstance = RoadsideParameter.get(params.id)
        if (!roadsideParameterInstance) {
            flash.message = "roadsideParameter.not.found"
            flash.args = [params.id]
            flash.defaultMessage = "RoadsideParameter not found with id ${params.id}"
            redirect(action: "list")
        }
        else {
            return [roadsideParameterInstance: roadsideParameterInstance]
        }
    }

    def edit = {
        def roadsideParameterInstance = RoadsideParameter.get(params.id)
        if (!roadsideParameterInstance) {
            flash.message = "roadsideParameter.not.found"
            flash.args = [params.id]
            flash.defaultMessage = "RoadsideParameter not found with id ${params.id}"
            redirect(action: "list")
        }
        else {
            return [roadsideParameterInstance: roadsideParameterInstance]
        }
    }

    def update = {
        def roadsideParameterInstance = RoadsideParameter.get(params.id)
        if (roadsideParameterInstance) {
            if (params.version) {
                def version = params.version.toLong()
                if (roadsideParameterInstance.version > version) {
                    
                    roadsideParameterInstance.errors.rejectValue("version", "roadsideParameter.optimistic.locking.failure", "Another user has updated this RoadsideParameter while you were editing")
                    render(view: "edit", model: [roadsideParameterInstance: roadsideParameterInstance])
                    return
                }
            }
            roadsideParameterInstance.properties = params
            if (!roadsideParameterInstance.hasErrors() && roadsideParameterInstance.save()) {
                flash.message = "roadsideParameter.updated"
                flash.args = [params.id]
                flash.defaultMessage = "RoadsideParameter ${params.id} updated"
                redirect(action: "show", id: roadsideParameterInstance.id)
            }
            else {
                render(view: "edit", model: [roadsideParameterInstance: roadsideParameterInstance])
            }
        }
        else {
            flash.message = "roadsideParameter.not.found"
            flash.args = [params.id]
            flash.defaultMessage = "RoadsideParameter not found with id ${params.id}"
            redirect(action: "edit", id: params.id)
        }
    }

    def delete = {
        def roadsideParameterInstance = RoadsideParameter.get(params.id)
        if (roadsideParameterInstance) {
            try {
                roadsideParameterInstance.delete()
                flash.message = "roadsideParameter.deleted"
                flash.args = [params.id]
                flash.defaultMessage = "RoadsideParameter ${params.id} deleted"
                redirect(action: "list")
            }
            catch (org.springframework.dao.DataIntegrityViolationException e) {
                flash.message = "roadsideParameter.not.deleted"
                flash.args = [params.id]
                flash.defaultMessage = "RoadsideParameter ${params.id} could not be deleted"
                redirect(action: "show", id: params.id)
            }
        }
        else {
            flash.message = "roadsideParameter.not.found"
            flash.args = [params.id]
            flash.defaultMessage = "RoadsideParameter not found with id ${params.id}"
            redirect(action: "list")
        }
    }
}
