package com.innovated.iris.domain.lookup

import org.codehaus.groovy.grails.plugins.springsecurity.Secured;

@Secured(['ROLE_SUPER', 'ROLE_ADMIN'])
class VehicleRegistrationParameterController {

    def index = { redirect(action: "list", params: params) }

    // the delete, save and update actions only accept POST requests
    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def list = {
        params.max = Math.min(params.max ? params.max.toInteger() : 10,  100)
        [vehicleRegistrationParameterInstanceList: VehicleRegistrationParameter.list(params), vehicleRegistrationParameterInstanceTotal: VehicleRegistrationParameter.count()]
    }

    def create = {
        def vehicleRegistrationParameterInstance = new VehicleRegistrationParameter()
        vehicleRegistrationParameterInstance.properties = params
        return [vehicleRegistrationParameterInstance: vehicleRegistrationParameterInstance]
    }

    def save = {
        def vehicleRegistrationParameterInstance = new VehicleRegistrationParameter(params)
        if (!vehicleRegistrationParameterInstance.hasErrors() && vehicleRegistrationParameterInstance.save()) {
            flash.message = "vehicleRegistrationParameter.created"
            flash.args = [vehicleRegistrationParameterInstance.id]
            flash.defaultMessage = "VehicleRegistrationParameter ${vehicleRegistrationParameterInstance.id} created"
            redirect(action: "show", id: vehicleRegistrationParameterInstance.id)
        }
        else {
            render(view: "create", model: [vehicleRegistrationParameterInstance: vehicleRegistrationParameterInstance])
        }
    }

    def show = {
        def vehicleRegistrationParameterInstance = VehicleRegistrationParameter.get(params.id)
        if (!vehicleRegistrationParameterInstance) {
            flash.message = "vehicleRegistrationParameter.not.found"
            flash.args = [params.id]
            flash.defaultMessage = "VehicleRegistrationParameter not found with id ${params.id}"
            redirect(action: "list")
        }
        else {
            return [vehicleRegistrationParameterInstance: vehicleRegistrationParameterInstance]
        }
    }

    def edit = {
        def vehicleRegistrationParameterInstance = VehicleRegistrationParameter.get(params.id)
        if (!vehicleRegistrationParameterInstance) {
            flash.message = "vehicleRegistrationParameter.not.found"
            flash.args = [params.id]
            flash.defaultMessage = "VehicleRegistrationParameter not found with id ${params.id}"
            redirect(action: "list")
        }
        else {
            return [vehicleRegistrationParameterInstance: vehicleRegistrationParameterInstance]
        }
    }

    def update = {
        def vehicleRegistrationParameterInstance = VehicleRegistrationParameter.get(params.id)
        if (vehicleRegistrationParameterInstance) {
            if (params.version) {
                def version = params.version.toLong()
                if (vehicleRegistrationParameterInstance.version > version) {
                    
                    vehicleRegistrationParameterInstance.errors.rejectValue("version", "vehicleRegistrationParameter.optimistic.locking.failure", "Another user has updated this VehicleRegistrationParameter while you were editing")
                    render(view: "edit", model: [vehicleRegistrationParameterInstance: vehicleRegistrationParameterInstance])
                    return
                }
            }
            vehicleRegistrationParameterInstance.properties = params
            if (!vehicleRegistrationParameterInstance.hasErrors() && vehicleRegistrationParameterInstance.save()) {
                flash.message = "vehicleRegistrationParameter.updated"
                flash.args = [params.id]
                flash.defaultMessage = "VehicleRegistrationParameter ${params.id} updated"
                redirect(action: "show", id: vehicleRegistrationParameterInstance.id)
            }
            else {
                render(view: "edit", model: [vehicleRegistrationParameterInstance: vehicleRegistrationParameterInstance])
            }
        }
        else {
            flash.message = "vehicleRegistrationParameter.not.found"
            flash.args = [params.id]
            flash.defaultMessage = "VehicleRegistrationParameter not found with id ${params.id}"
            redirect(action: "edit", id: params.id)
        }
    }

    def delete = {
        def vehicleRegistrationParameterInstance = VehicleRegistrationParameter.get(params.id)
        if (vehicleRegistrationParameterInstance) {
            try {
                vehicleRegistrationParameterInstance.delete()
                flash.message = "vehicleRegistrationParameter.deleted"
                flash.args = [params.id]
                flash.defaultMessage = "VehicleRegistrationParameter ${params.id} deleted"
                redirect(action: "list")
            }
            catch (org.springframework.dao.DataIntegrityViolationException e) {
                flash.message = "vehicleRegistrationParameter.not.deleted"
                flash.args = [params.id]
                flash.defaultMessage = "VehicleRegistrationParameter ${params.id} could not be deleted"
                redirect(action: "show", id: params.id)
            }
        }
        else {
            flash.message = "vehicleRegistrationParameter.not.found"
            flash.args = [params.id]
            flash.defaultMessage = "VehicleRegistrationParameter not found with id ${params.id}"
            redirect(action: "list")
        }
    }
}
