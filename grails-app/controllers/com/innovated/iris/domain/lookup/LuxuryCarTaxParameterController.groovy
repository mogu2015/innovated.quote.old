package com.innovated.iris.domain.lookup

import org.codehaus.groovy.grails.plugins.springsecurity.Secured;

@Secured(['ROLE_SUPER', 'ROLE_ADMIN'])
class LuxuryCarTaxParameterController {

    def index = { redirect(action: "list", params: params) }

    // the delete, save and update actions only accept POST requests
    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def list = {
        params.max = Math.min(params.max ? params.max.toInteger() : 10,  100)
        [luxuryCarTaxParameterInstanceList: LuxuryCarTaxParameter.list(params), luxuryCarTaxParameterInstanceTotal: LuxuryCarTaxParameter.count()]
    }

    def create = {
        def luxuryCarTaxParameterInstance = new LuxuryCarTaxParameter()
        luxuryCarTaxParameterInstance.properties = params
        return [luxuryCarTaxParameterInstance: luxuryCarTaxParameterInstance]
    }

    def save = {
        def luxuryCarTaxParameterInstance = new LuxuryCarTaxParameter(params)
        if (!luxuryCarTaxParameterInstance.hasErrors() && luxuryCarTaxParameterInstance.save()) {
            flash.message = "luxuryCarTaxParameter.created"
            flash.args = [luxuryCarTaxParameterInstance.id]
            flash.defaultMessage = "LuxuryCarTaxParameter ${luxuryCarTaxParameterInstance.id} created"
            redirect(action: "show", id: luxuryCarTaxParameterInstance.id)
        }
        else {
            render(view: "create", model: [luxuryCarTaxParameterInstance: luxuryCarTaxParameterInstance])
        }
    }

    def show = {
        def luxuryCarTaxParameterInstance = LuxuryCarTaxParameter.get(params.id)
        if (!luxuryCarTaxParameterInstance) {
            flash.message = "luxuryCarTaxParameter.not.found"
            flash.args = [params.id]
            flash.defaultMessage = "LuxuryCarTaxParameter not found with id ${params.id}"
            redirect(action: "list")
        }
        else {
            return [luxuryCarTaxParameterInstance: luxuryCarTaxParameterInstance]
        }
    }

    def edit = {
        def luxuryCarTaxParameterInstance = LuxuryCarTaxParameter.get(params.id)
        if (!luxuryCarTaxParameterInstance) {
            flash.message = "luxuryCarTaxParameter.not.found"
            flash.args = [params.id]
            flash.defaultMessage = "LuxuryCarTaxParameter not found with id ${params.id}"
            redirect(action: "list")
        }
        else {
            return [luxuryCarTaxParameterInstance: luxuryCarTaxParameterInstance]
        }
    }

    def update = {
        def luxuryCarTaxParameterInstance = LuxuryCarTaxParameter.get(params.id)
        if (luxuryCarTaxParameterInstance) {
            if (params.version) {
                def version = params.version.toLong()
                if (luxuryCarTaxParameterInstance.version > version) {
                    
                    luxuryCarTaxParameterInstance.errors.rejectValue("version", "luxuryCarTaxParameter.optimistic.locking.failure", "Another user has updated this LuxuryCarTaxParameter while you were editing")
                    render(view: "edit", model: [luxuryCarTaxParameterInstance: luxuryCarTaxParameterInstance])
                    return
                }
            }
            luxuryCarTaxParameterInstance.properties = params
            if (!luxuryCarTaxParameterInstance.hasErrors() && luxuryCarTaxParameterInstance.save()) {
                flash.message = "luxuryCarTaxParameter.updated"
                flash.args = [params.id]
                flash.defaultMessage = "LuxuryCarTaxParameter ${params.id} updated"
                redirect(action: "show", id: luxuryCarTaxParameterInstance.id)
            }
            else {
                render(view: "edit", model: [luxuryCarTaxParameterInstance: luxuryCarTaxParameterInstance])
            }
        }
        else {
            flash.message = "luxuryCarTaxParameter.not.found"
            flash.args = [params.id]
            flash.defaultMessage = "LuxuryCarTaxParameter not found with id ${params.id}"
            redirect(action: "edit", id: params.id)
        }
    }

    def delete = {
        def luxuryCarTaxParameterInstance = LuxuryCarTaxParameter.get(params.id)
        if (luxuryCarTaxParameterInstance) {
            try {
                luxuryCarTaxParameterInstance.delete()
                flash.message = "luxuryCarTaxParameter.deleted"
                flash.args = [params.id]
                flash.defaultMessage = "LuxuryCarTaxParameter ${params.id} deleted"
                redirect(action: "list")
            }
            catch (org.springframework.dao.DataIntegrityViolationException e) {
                flash.message = "luxuryCarTaxParameter.not.deleted"
                flash.args = [params.id]
                flash.defaultMessage = "LuxuryCarTaxParameter ${params.id} could not be deleted"
                redirect(action: "show", id: params.id)
            }
        }
        else {
            flash.message = "luxuryCarTaxParameter.not.found"
            flash.args = [params.id]
            flash.defaultMessage = "LuxuryCarTaxParameter not found with id ${params.id}"
            redirect(action: "list")
        }
    }
}
