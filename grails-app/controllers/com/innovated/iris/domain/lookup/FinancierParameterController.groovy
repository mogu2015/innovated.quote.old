package com.innovated.iris.domain.lookup

import org.codehaus.groovy.grails.plugins.springsecurity.Secured;

@Secured(['ROLE_SUPER', 'ROLE_ADMIN'])
class FinancierParameterController {

    def index = { redirect(action: "list", params: params) }

    // the delete, save and update actions only accept POST requests
    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def list = {
        params.max = Math.min(params.max ? params.max.toInteger() : 10,  100)
        [financierParameterInstanceList: FinancierParameter.list(params), financierParameterInstanceTotal: FinancierParameter.count()]
    }

    def create = {
        def financierParameterInstance = new FinancierParameter()
        financierParameterInstance.properties = params
        return [financierParameterInstance: financierParameterInstance]
    }

    def save = {
        def financierParameterInstance = new FinancierParameter(params)
        if (!financierParameterInstance.hasErrors() && financierParameterInstance.save()) {
            flash.message = "financierParameter.created"
            flash.args = [financierParameterInstance.id]
            flash.defaultMessage = "FinancierParameter ${financierParameterInstance.id} created"
            redirect(action: "show", id: financierParameterInstance.id)
        }
        else {
            render(view: "create", model: [financierParameterInstance: financierParameterInstance])
        }
    }

    def show = {
        def financierParameterInstance = FinancierParameter.get(params.id)
        if (!financierParameterInstance) {
            flash.message = "financierParameter.not.found"
            flash.args = [params.id]
            flash.defaultMessage = "FinancierParameter not found with id ${params.id}"
            redirect(action: "list")
        }
        else {
            return [financierParameterInstance: financierParameterInstance]
        }
    }

    def edit = {
        def financierParameterInstance = FinancierParameter.get(params.id)
        if (!financierParameterInstance) {
            flash.message = "financierParameter.not.found"
            flash.args = [params.id]
            flash.defaultMessage = "FinancierParameter not found with id ${params.id}"
            redirect(action: "list")
        }
        else {
            return [financierParameterInstance: financierParameterInstance]
        }
    }

    def update = {
        def financierParameterInstance = FinancierParameter.get(params.id)
        if (financierParameterInstance) {
            if (params.version) {
                def version = params.version.toLong()
                if (financierParameterInstance.version > version) {
                    
                    financierParameterInstance.errors.rejectValue("version", "financierParameter.optimistic.locking.failure", "Another user has updated this FinancierParameter while you were editing")
                    render(view: "edit", model: [financierParameterInstance: financierParameterInstance])
                    return
                }
            }
            financierParameterInstance.properties = params
            if (!financierParameterInstance.hasErrors() && financierParameterInstance.save()) {
                flash.message = "financierParameter.updated"
                flash.args = [params.id]
                flash.defaultMessage = "FinancierParameter ${params.id} updated"
                redirect(action: "show", id: financierParameterInstance.id)
            }
            else {
                render(view: "edit", model: [financierParameterInstance: financierParameterInstance])
            }
        }
        else {
            flash.message = "financierParameter.not.found"
            flash.args = [params.id]
            flash.defaultMessage = "FinancierParameter not found with id ${params.id}"
            redirect(action: "edit", id: params.id)
        }
    }

    def delete = {
        def financierParameterInstance = FinancierParameter.get(params.id)
        if (financierParameterInstance) {
            try {
                financierParameterInstance.delete()
                flash.message = "financierParameter.deleted"
                flash.args = [params.id]
                flash.defaultMessage = "FinancierParameter ${params.id} deleted"
                redirect(action: "list")
            }
            catch (org.springframework.dao.DataIntegrityViolationException e) {
                flash.message = "financierParameter.not.deleted"
                flash.args = [params.id]
                flash.defaultMessage = "FinancierParameter ${params.id} could not be deleted"
                redirect(action: "show", id: params.id)
            }
        }
        else {
            flash.message = "financierParameter.not.found"
            flash.args = [params.id]
            flash.defaultMessage = "FinancierParameter not found with id ${params.id}"
            redirect(action: "list")
        }
    }
}
