package com.innovated.iris.domain.lookup

import org.codehaus.groovy.grails.plugins.springsecurity.Secured;

@Secured(['ROLE_SUPER', 'ROLE_ADMIN'])
class FbtCentsPerKmMethodRateParameterController {

    def index = { redirect(action: "list", params: params) }

    // the delete, save and update actions only accept POST requests
    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def list = {
        params.max = Math.min(params.max ? params.max.toInteger() : 10,  100)
        [fbtCentsPerKmMethodRateParameterInstanceList: FbtCentsPerKmMethodRateParameter.list(params), fbtCentsPerKmMethodRateParameterInstanceTotal: FbtCentsPerKmMethodRateParameter.count()]
    }

    def create = {
        def fbtCentsPerKmMethodRateParameterInstance = new FbtCentsPerKmMethodRateParameter()
        fbtCentsPerKmMethodRateParameterInstance.properties = params
        return [fbtCentsPerKmMethodRateParameterInstance: fbtCentsPerKmMethodRateParameterInstance]
    }

    def save = {
        def fbtCentsPerKmMethodRateParameterInstance = new FbtCentsPerKmMethodRateParameter(params)
        if (!fbtCentsPerKmMethodRateParameterInstance.hasErrors() && fbtCentsPerKmMethodRateParameterInstance.save()) {
            flash.message = "fbtCentsPerKmMethodRateParameter.created"
            flash.args = [fbtCentsPerKmMethodRateParameterInstance.id]
            flash.defaultMessage = "FbtCentsPerKmMethodRateParameter ${fbtCentsPerKmMethodRateParameterInstance.id} created"
            redirect(action: "show", id: fbtCentsPerKmMethodRateParameterInstance.id)
        }
        else {
            render(view: "create", model: [fbtCentsPerKmMethodRateParameterInstance: fbtCentsPerKmMethodRateParameterInstance])
        }
    }

    def show = {
        def fbtCentsPerKmMethodRateParameterInstance = FbtCentsPerKmMethodRateParameter.get(params.id)
        if (!fbtCentsPerKmMethodRateParameterInstance) {
            flash.message = "fbtCentsPerKmMethodRateParameter.not.found"
            flash.args = [params.id]
            flash.defaultMessage = "FbtCentsPerKmMethodRateParameter not found with id ${params.id}"
            redirect(action: "list")
        }
        else {
            return [fbtCentsPerKmMethodRateParameterInstance: fbtCentsPerKmMethodRateParameterInstance]
        }
    }

    def edit = {
        def fbtCentsPerKmMethodRateParameterInstance = FbtCentsPerKmMethodRateParameter.get(params.id)
        if (!fbtCentsPerKmMethodRateParameterInstance) {
            flash.message = "fbtCentsPerKmMethodRateParameter.not.found"
            flash.args = [params.id]
            flash.defaultMessage = "FbtCentsPerKmMethodRateParameter not found with id ${params.id}"
            redirect(action: "list")
        }
        else {
            return [fbtCentsPerKmMethodRateParameterInstance: fbtCentsPerKmMethodRateParameterInstance]
        }
    }

    def update = {
        def fbtCentsPerKmMethodRateParameterInstance = FbtCentsPerKmMethodRateParameter.get(params.id)
        if (fbtCentsPerKmMethodRateParameterInstance) {
            if (params.version) {
                def version = params.version.toLong()
                if (fbtCentsPerKmMethodRateParameterInstance.version > version) {
                    
                    fbtCentsPerKmMethodRateParameterInstance.errors.rejectValue("version", "fbtCentsPerKmMethodRateParameter.optimistic.locking.failure", "Another user has updated this FbtCentsPerKmMethodRateParameter while you were editing")
                    render(view: "edit", model: [fbtCentsPerKmMethodRateParameterInstance: fbtCentsPerKmMethodRateParameterInstance])
                    return
                }
            }
            fbtCentsPerKmMethodRateParameterInstance.properties = params
            if (!fbtCentsPerKmMethodRateParameterInstance.hasErrors() && fbtCentsPerKmMethodRateParameterInstance.save()) {
                flash.message = "fbtCentsPerKmMethodRateParameter.updated"
                flash.args = [params.id]
                flash.defaultMessage = "FbtCentsPerKmMethodRateParameter ${params.id} updated"
                redirect(action: "show", id: fbtCentsPerKmMethodRateParameterInstance.id)
            }
            else {
                render(view: "edit", model: [fbtCentsPerKmMethodRateParameterInstance: fbtCentsPerKmMethodRateParameterInstance])
            }
        }
        else {
            flash.message = "fbtCentsPerKmMethodRateParameter.not.found"
            flash.args = [params.id]
            flash.defaultMessage = "FbtCentsPerKmMethodRateParameter not found with id ${params.id}"
            redirect(action: "edit", id: params.id)
        }
    }

    def delete = {
        def fbtCentsPerKmMethodRateParameterInstance = FbtCentsPerKmMethodRateParameter.get(params.id)
        if (fbtCentsPerKmMethodRateParameterInstance) {
            try {
                fbtCentsPerKmMethodRateParameterInstance.delete()
                flash.message = "fbtCentsPerKmMethodRateParameter.deleted"
                flash.args = [params.id]
                flash.defaultMessage = "FbtCentsPerKmMethodRateParameter ${params.id} deleted"
                redirect(action: "list")
            }
            catch (org.springframework.dao.DataIntegrityViolationException e) {
                flash.message = "fbtCentsPerKmMethodRateParameter.not.deleted"
                flash.args = [params.id]
                flash.defaultMessage = "FbtCentsPerKmMethodRateParameter ${params.id} could not be deleted"
                redirect(action: "show", id: params.id)
            }
        }
        else {
            flash.message = "fbtCentsPerKmMethodRateParameter.not.found"
            flash.args = [params.id]
            flash.defaultMessage = "FbtCentsPerKmMethodRateParameter not found with id ${params.id}"
            redirect(action: "list")
        }
    }
}
