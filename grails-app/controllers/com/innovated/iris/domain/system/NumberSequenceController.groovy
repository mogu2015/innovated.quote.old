

package com.innovated.iris.domain.system


import grails.converters.JSON

class NumberSequenceController {

    def index = { redirect(action: "list", params: params) }

    // the delete, save and update actions only accept POST requests
    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def list = {
        params.max = Math.min(params.max ? params.max.toInteger() : 10,  100)
        [numberSequenceInstanceList: NumberSequence.list(params), numberSequenceInstanceTotal: NumberSequence.count()]
    }
	
	def listAjax = {
		def ajax = [success:true, sEcho:params.sEcho, iTotalRecords:0, iTotalDisplayRecords:0, aaData:[]]
		def whereStr = ""
		def orderByStr = ""
		def alias = "c"
		def hdrList = ["chk","icon","id","code","name","format","lastUpdated"]
		
		// manage search/filtering
		if (params.sSearch) {
			def whereCols = []
			hdrList.eachWithIndex { item, idx ->
				if (idx > 1) whereCols << "${alias}.${item} like '%${params.sSearch}%'"
			}
			whereStr = " where " + whereCols.join(" or ")
		}
		
		// work out sorting!
		if (params.iSortingCols && (params.iSortingCols.toInteger() > 0)) {
			def sortCols = []
			def prefix = 'iSortCol_'
			for (String key in params.keySet()) {
				if (key.startsWith(prefix)) {
					def code = Integer.valueOf(key.substring(prefix.length()))
					sortCols << [id:code, column:params.get(key).toInteger(), order:params.get("sSortDir_"+code)]
				}
			}
			orderByStr = " order by " + sortCols.sort{ it.id }.collect{ alias + "." + hdrList.get(it.column) + " " + it.order }.join(", ")
		}
		
		def total = NumberSequence.count()
		def sortParams = [:]
		if (params.iDisplayLength.toInteger() == -1) {
			sortParams.max = total
			sortParams.offset = 0
		}
		else {
			sortParams.max = Math.min(params.iDisplayLength ? params.iDisplayLength.toInteger() : 100,  10000)
			sortParams.offset = params.iDisplayStart ? params.iDisplayStart.toInteger() : 0
		}
		
		def query = "from NumberSequence as ${alias}${whereStr}${orderByStr}"
		def records = NumberSequence.findAll(query, [], sortParams)

		def iconPath = g.resource(dir:'images/skin', file:'error.png')
		ajax.iTotalRecords = total
		ajax.iTotalDisplayRecords = total
		ajax.aaData = records.collect{ r -> [
			"" + g.checkBox(class:"chkRow", name:"chkRow_"+r.id, value:false, title:"Select this row", checked:false),
			"<img width='16' height='16' alt='NumberSequence icon' src='${iconPath}' title='NumberSequence icon'/>"
			
			,"" + g.link(action:"edit", id:r.id, title:"Edit this NumberSequence record") { r.id }
	
        	,r.code
    
        	,r.name
    
        	,r.format
    
        	,"" + g.formatDate(date:r.lastUpdated)
    
		]}
		render ajax as JSON
	}

    def create = {
        def numberSequenceInstance = new NumberSequence()
        numberSequenceInstance.properties = params
        return [numberSequenceInstance: numberSequenceInstance]
    }

    def save = {
        def numberSequenceInstance = new NumberSequence(params)
        if (!numberSequenceInstance.hasErrors() && numberSequenceInstance.save()) {
            flash.message = "numberSequence.created"
            flash.args = [numberSequenceInstance.id]
            flash.defaultMessage = "NumberSequence ${numberSequenceInstance.id} created"
            redirect(action: "show", id: numberSequenceInstance.id)
        }
        else {
            render(view: "create", model: [numberSequenceInstance: numberSequenceInstance])
        }
    }

    def show = {
        def numberSequenceInstance = NumberSequence.get(params.id)
        if (!numberSequenceInstance) {
            flash.message = "numberSequence.not.found"
            flash.args = [params.id]
            flash.defaultMessage = "NumberSequence not found with id ${params.id}"
            redirect(action: "list")
        }
        else {
            return [numberSequenceInstance: numberSequenceInstance]
        }
    }

    def edit = {
        def numberSequenceInstance = NumberSequence.get(params.id)
        if (!numberSequenceInstance) {
            flash.message = "numberSequence.not.found"
            flash.args = [params.id]
            flash.defaultMessage = "NumberSequence not found with id ${params.id}"
            redirect(action: "list")
        }
        else {
            return [numberSequenceInstance: numberSequenceInstance]
        }
    }

    def update = {
        def numberSequenceInstance = NumberSequence.get(params.id)
        if (numberSequenceInstance) {
            if (params.version) {
                def version = params.version.toLong()
                if (numberSequenceInstance.version > version) {
                    
                    numberSequenceInstance.errors.rejectValue("version", "numberSequence.optimistic.locking.failure", "Another user has updated this NumberSequence while you were editing")
                    render(view: "edit", model: [numberSequenceInstance: numberSequenceInstance])
                    return
                }
            }
            numberSequenceInstance.properties = params
            if (!numberSequenceInstance.hasErrors() && numberSequenceInstance.save()) {
                flash.message = "numberSequence.updated"
                flash.args = [params.id]
                flash.defaultMessage = "NumberSequence ${params.id} updated"
                redirect(action: "show", id: numberSequenceInstance.id)
            }
            else {
                render(view: "edit", model: [numberSequenceInstance: numberSequenceInstance])
            }
        }
        else {
            flash.message = "numberSequence.not.found"
            flash.args = [params.id]
            flash.defaultMessage = "NumberSequence not found with id ${params.id}"
            redirect(action: "edit", id: params.id)
        }
    }

    def delete = {
        def numberSequenceInstance = NumberSequence.get(params.id)
        if (numberSequenceInstance) {
            try {
                numberSequenceInstance.delete()
                flash.message = "numberSequence.deleted"
                flash.args = [params.id]
                flash.defaultMessage = "NumberSequence ${params.id} deleted"
                redirect(action: "list")
            }
            catch (org.springframework.dao.DataIntegrityViolationException e) {
                flash.message = "numberSequence.not.deleted"
                flash.args = [params.id]
                flash.defaultMessage = "NumberSequence ${params.id} could not be deleted"
                redirect(action: "show", id: params.id)
            }
        }
        else {
            flash.message = "numberSequence.not.found"
            flash.args = [params.id]
            flash.defaultMessage = "NumberSequence not found with id ${params.id}"
            redirect(action: "list")
        }
    }
}
