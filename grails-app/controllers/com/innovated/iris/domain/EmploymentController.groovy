package com.innovated.iris.domain

import org.codehaus.groovy.grails.plugins.springsecurity.Secured

//@Secured(['IS_AUTHENTICATED_FULLY'])
@Secured(['ROLE_SUPER', 'ROLE_ADMIN'])
class EmploymentController {

    def index = { redirect(action: "list", params: params) }

    // the delete, save and update actions only accept POST requests
    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def list = {
        params.max = Math.min(params.max ? params.max.toInteger() : 10,  100)
        [employmentInstanceList: Employment.list(params), employmentInstanceTotal: Employment.count()]
    }

    def employees = {
		if (params.id) {
			def employees = Employment.findAll("from Employment as e where e.employer.id=:id", [id:params.id.toLong()])
			render view:"list", model:[employmentInstanceList: employees, employmentInstanceTotal: employees.size()]
		}
		else {
			flash.message = "employment.not.found"
			flash.args = [params.id]
			flash.defaultMessage = "Employment not found with id ${params.id}"
			redirect(action: "list")
		}
    }

    def employers = {
		if (params.id) {
			def employers = Employment.findAll("from Employment as e where e.employee.id=:id", [id:params.id.toLong()])
			render view:"list", model:[employmentInstanceList: employers, employmentInstanceTotal: employers.size()]
		}
		else {
			flash.message = "employment.not.found"
			flash.args = [params.id]
			flash.defaultMessage = "Employment not found with id ${params.id}"
			redirect(action: "list")
		}
    }

    def create = {
        def employmentInstance = new Employment()
        employmentInstance.properties = params
        return [employmentInstance: employmentInstance]
    }

    def save = {
        def employmentInstance = new Employment(params)
        if (!employmentInstance.hasErrors() && employmentInstance.save()) {
            flash.message = "employment.created"
            flash.args = [employmentInstance.id]
            flash.defaultMessage = "Employment ${employmentInstance.id} created"
            redirect(action: "show", id: employmentInstance.id)
        }
        else {
            render(view: "create", model: [employmentInstance: employmentInstance])
        }
    }

    def show = {
        def employmentInstance = Employment.get(params.id)
        if (!employmentInstance) {
            flash.message = "employment.not.found"
            flash.args = [params.id]
            flash.defaultMessage = "Employment not found with id ${params.id}"
            redirect(action: "list")
        }
        else {
            return [employmentInstance: employmentInstance]
        }
    }

    def edit = {
        def employmentInstance = Employment.get(params.id)
        if (!employmentInstance) {
            flash.message = "employment.not.found"
            flash.args = [params.id]
            flash.defaultMessage = "Employment not found with id ${params.id}"
            redirect(action: "list")
        }
        else {
            return [employmentInstance: employmentInstance]
        }
    }

    def update = {
        def employmentInstance = Employment.get(params.id)
        if (employmentInstance) {
            if (params.version) {
                def version = params.version.toLong()
                if (employmentInstance.version > version) {
                    
                    employmentInstance.errors.rejectValue("version", "employment.optimistic.locking.failure", "Another user has updated this Employment while you were editing")
                    render(view: "edit", model: [employmentInstance: employmentInstance])
                    return
                }
            }
            employmentInstance.properties = params
            if (!employmentInstance.hasErrors() && employmentInstance.save()) {
                flash.message = "employment.updated"
                flash.args = [params.id]
                flash.defaultMessage = "Employment ${params.id} updated"
                redirect(action: "show", id: employmentInstance.id)
            }
            else {
                render(view: "edit", model: [employmentInstance: employmentInstance])
            }
        }
        else {
            flash.message = "employment.not.found"
            flash.args = [params.id]
            flash.defaultMessage = "Employment not found with id ${params.id}"
            redirect(action: "edit", id: params.id)
        }
    }

    def delete = {
        def employmentInstance = Employment.get(params.id)
        if (employmentInstance) {
            try {
                employmentInstance.delete()
                flash.message = "employment.deleted"
                flash.args = [params.id]
                flash.defaultMessage = "Employment ${params.id} deleted"
                redirect(action: "list")
            }
            catch (org.springframework.dao.DataIntegrityViolationException e) {
                flash.message = "employment.not.deleted"
                flash.args = [params.id]
                flash.defaultMessage = "Employment ${params.id} could not be deleted"
                redirect(action: "show", id: params.id)
            }
        }
        else {
            flash.message = "employment.not.found"
            flash.args = [params.id]
            flash.defaultMessage = "Employment not found with id ${params.id}"
            redirect(action: "list")
        }
    }
}
