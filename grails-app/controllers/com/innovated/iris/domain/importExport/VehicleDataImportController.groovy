package com.innovated.iris.domain.importExport

import org.codehaus.groovy.grails.plugins.springsecurity.Secured

import com.innovated.iris.domain.*
import com.innovated.iris.domain.enums.*
import com.innovated.iris.domain.lookup.*

import org.springframework.web.multipart.MultipartHttpServletRequest
import org.springframework.web.multipart.commons.CommonsMultipartFile

//@Secured(['IS_AUTHENTICATED_FULLY'])
@Secured(['ROLE_SUPER', 'ROLE_ADMIN'])
class VehicleDataImportController {
	
	def sessionFactory
    def propertyInstanceMap = org.codehaus.groovy.grails.plugins.DomainClassGrailsPlugin.PROPERTY_INSTANCE_MAP
	
    def index = { redirect(action: "list", params: params) }

    // the delete, save and update actions only accept POST requests
    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def list = {
        params.max = Math.min(params.max ? params.max.toInteger() : 100,  100)
        [vehicleDataImportInstanceList: VehicleDataImport.list(params), vehicleDataImportInstanceTotal: VehicleDataImport.count()]
    }
    
    def importFile = {
    	render(view: "import", model: [:])
    }
    
    def saveXml = {
    	if(request instanceof MultipartHttpServletRequest) {
			// get our multipart  
			MultipartHttpServletRequest mpr = (MultipartHttpServletRequest)request;
			CommonsMultipartFile file = (CommonsMultipartFile) mpr.getFile("importFile");
			
			if (!file.empty) {
				println "\nImporting Vehicle Data from XML file: ${file.getOriginalFilename()}"
				
				def added = 0, updated = 0
				def skipped = []
				def timeTaken = System.currentTimeMillis()

				def data = new XmlSlurper().parseText(file.inputStream.text)
				def allRecords = data.Table		//pull out all vehicle data records from XML content
				def vehicleRecordCount = allRecords.size()
				def nullVariants = 0
				def nullVariantMakes = []

				println "# vehicle is file = ${vehicleRecordCount}"
				
				allRecords.eachWithIndex { v, index ->
					def variantStr = ""
					if (v.Variant == null || v.Variant=="") {
						nullVariants++
						nullVariantMakes << v.Make.toString()
						//print "NULL Variant! - "
					} else {
						variantStr = " ${v.Variant}"
					}
					/*
					def str = "[${v.Model_Release_Year} ${v.Make} ${v.Marketing_Model}${variantStr} ${v.Body_Style} ${v.Engine} ${v.Transmission_Type}], PUBID=${v.Published_Vehicle_ID}, VID=${v.Vehicle_ID}, CURRENT=${v.Current_Model}"
					//println str
					
					def vehProperties = v.depthFirst().collect{ it }
					println "#prop = ${vehProperties.size()} ->"
					vehProperties.each {
						println "   name=${it.name()}, value=${it.text()}"
					}
					*/
					def props = [
						publishedVehicleID:        v.Published_Vehicle_ID?.toInteger(),
						vehicleID:                 v.Vehicle_ID?.toInteger(),
						make:                      v.Make?.toString(),
						modelReleaseVersion:       v.Model_Release_Version?.toString(),
						marketingModel:            v.Marketing_Model?.toString(),
						variant:                   v.Variant?.toString(),
						makeModelVariant:          v.Make_Model_Variant?.toString(),
						bodyStyle:                 v.Body_Style?.toString(),
						seatingCapacity:           v.Seating_Capacity?.toInteger(),
						bodySeats:                 v.Body_Seats?.toString(),
						engineDisplacement:        v.Engine_Displacement?.toDouble(),
						engineConfiguration:       v.Engine_Configuration?.toString(),
						induction:                 v.Induction?.toString(),
						engine:                    v.Engine?.toString(),
						transmissionType:          v.Transmission_Type?.toString(),
						fwdGearsNo:                v.Fwd_Gears_No?.toInteger(),
						trans:                     v.Trans?.toString(),
						fuelType:                  v.Fuel_Type?.toString(),
						drivingWheels:             v.Driving_Wheels?.toString(),
						vehicleClass:              v.Vehicle_Class?.toString(),
						fuelConsumption:           (v.Fuel_Consumption==null || v.Fuel_Consumption=="") ? null : v.Fuel_Consumption?.toDouble(),
						fuelConsumptionUrban:      (v.Fuel_Consumption_Urban==null || v.Fuel_Consumption_Urban=="") ? null : v.Fuel_Consumption_Urban?.toDouble(),
						fuelConsumptionExtraUrban: (v.Fuel_Consumption_Extra_Urban==null || v.Fuel_Consumption_Extra_Urban=="") ? null : v.Fuel_Consumption_Extra_Urban?.toDouble(),
						modelReleaseYear:          v.Model_Release_Year?.toInteger(),
						currentModel:              v.Current_Model?.toString()
					]
					
					
					// check if record (vehicle ID) already exists...
					def isNew = true
					def vehicle = VehicleDataImport.findWhere(publishedVehicleID: v.Published_Vehicle_ID?.toInteger())
					if (!vehicle) {
						vehicle = new VehicleDataImport(props)
						if (vehicle.validate() && vehicle.save())
							added++
						else
							skipped << v
					}
					
					// flush GORM cache?
					if (index % 5 == 0) cleanUpGorm()

				}
				println "Total=${vehicleRecordCount}, Added=${added}, Skipped=${skipped.size()}"
				
				//def uniques = nullVariantMakes.unique { a,b -> a.compareTo(b) }
				def uniques = nullVariantMakes.unique()
				println "\nNull Variant Makes:\n${uniques.join(', ')}"
				
				timeTaken = System.currentTimeMillis() - timeTaken
				flash.message = "Data file imported (${vehicleRecordCount} vehicle records... ${nullVariants} [${Math.rint(nullVariants/vehicleRecordCount*100)}%] null variants)!" +
					"\n(Time taken: ${timeTaken/1000}s)"
				redirect(action: "list")
			}
			else {
				flash.message = 'Please select a file to import!'
				redirect(action: "importFile")
			}
		}   
		else {
			flash.message = 'Invalid import (Error: 5301)'
			redirect(action: "importFile")
		}
	}
	
	def cleanUpGorm() {
        def session = sessionFactory.currentSession
        session.flush()
        session.clear()
        propertyInstanceMap.get().clear()
    }

    def create = {
        def vehicleDataImportInstance = new VehicleDataImport()
        vehicleDataImportInstance.properties = params
        return [vehicleDataImportInstance: vehicleDataImportInstance]
    }

    def save = {
        def vehicleDataImportInstance = new VehicleDataImport(params)
        if (!vehicleDataImportInstance.hasErrors() && vehicleDataImportInstance.save()) {
            flash.message = "vehicleDataImport.created"
            flash.args = [vehicleDataImportInstance.id]
            flash.defaultMessage = "VehicleDataImport ${vehicleDataImportInstance.id} created"
            redirect(action: "show", id: vehicleDataImportInstance.id)
        }
        else {
            render(view: "create", model: [vehicleDataImportInstance: vehicleDataImportInstance])
        }
    }

    def show = {
        def vehicleDataImportInstance = VehicleDataImport.get(params.id)
        if (!vehicleDataImportInstance) {
            flash.message = "vehicleDataImport.not.found"
            flash.args = [params.id]
            flash.defaultMessage = "VehicleDataImport not found with id ${params.id}"
            redirect(action: "list")
        }
        else {
            return [vehicleDataImportInstance: vehicleDataImportInstance]
        }
    }

    def edit = {
        def vehicleDataImportInstance = VehicleDataImport.get(params.id)
        if (!vehicleDataImportInstance) {
            flash.message = "vehicleDataImport.not.found"
            flash.args = [params.id]
            flash.defaultMessage = "VehicleDataImport not found with id ${params.id}"
            redirect(action: "list")
        }
        else {
            return [vehicleDataImportInstance: vehicleDataImportInstance]
        }
    }

    def update = {
        def vehicleDataImportInstance = VehicleDataImport.get(params.id)
        if (vehicleDataImportInstance) {
            if (params.version) {
                def version = params.version.toLong()
                if (vehicleDataImportInstance.version > version) {
                    
                    vehicleDataImportInstance.errors.rejectValue("version", "vehicleDataImport.optimistic.locking.failure", "Another user has updated this VehicleDataImport while you were editing")
                    render(view: "edit", model: [vehicleDataImportInstance: vehicleDataImportInstance])
                    return
                }
            }
            vehicleDataImportInstance.properties = params
            if (!vehicleDataImportInstance.hasErrors() && vehicleDataImportInstance.save()) {
                flash.message = "vehicleDataImport.updated"
                flash.args = [params.id]
                flash.defaultMessage = "VehicleDataImport ${params.id} updated"
                redirect(action: "show", id: vehicleDataImportInstance.id)
            }
            else {
                render(view: "edit", model: [vehicleDataImportInstance: vehicleDataImportInstance])
            }
        }
        else {
            flash.message = "vehicleDataImport.not.found"
            flash.args = [params.id]
            flash.defaultMessage = "VehicleDataImport not found with id ${params.id}"
            redirect(action: "edit", id: params.id)
        }
    }

    def delete = {
        def vehicleDataImportInstance = VehicleDataImport.get(params.id)
        if (vehicleDataImportInstance) {
            try {
                vehicleDataImportInstance.delete()
                flash.message = "vehicleDataImport.deleted"
                flash.args = [params.id]
                flash.defaultMessage = "VehicleDataImport ${params.id} deleted"
                redirect(action: "list")
            }
            catch (org.springframework.dao.DataIntegrityViolationException e) {
                flash.message = "vehicleDataImport.not.deleted"
                flash.args = [params.id]
                flash.defaultMessage = "VehicleDataImport ${params.id} could not be deleted"
                redirect(action: "show", id: params.id)
            }
        }
        else {
            flash.message = "vehicleDataImport.not.found"
            flash.args = [params.id]
            flash.defaultMessage = "VehicleDataImport not found with id ${params.id}"
            redirect(action: "list")
        }
    }
}
