package com.innovated.iris.domain

import org.codehaus.groovy.grails.plugins.springsecurity.Secured;

@Secured(['ROLE_SUPER', 'ROLE_ADMIN'])
class QuoteItemAccessoryController {

    def index = { redirect(action: "list", params: params) }

    // the delete, save and update actions only accept POST requests
    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def list = {
        params.max = Math.min(params.max ? params.max.toInteger() : 10,  100)
        [quoteItemAccessoryInstanceList: QuoteItemAccessory.list(params), quoteItemAccessoryInstanceTotal: QuoteItemAccessory.count()]
    }

    def create = {
        def quoteItemAccessoryInstance = new QuoteItemAccessory()
        quoteItemAccessoryInstance.properties = params
        return [quoteItemAccessoryInstance: quoteItemAccessoryInstance]
    }

    def save = {
        def quoteItemAccessoryInstance = new QuoteItemAccessory(params)
        if (!quoteItemAccessoryInstance.hasErrors() && quoteItemAccessoryInstance.save()) {
            flash.message = "quoteItemAccessory.created"
            flash.args = [quoteItemAccessoryInstance.id]
            flash.defaultMessage = "QuoteItemAccessory ${quoteItemAccessoryInstance.id} created"
            redirect(action: "show", id: quoteItemAccessoryInstance.id)
        }
        else {
            render(view: "create", model: [quoteItemAccessoryInstance: quoteItemAccessoryInstance])
        }
    }

    def show = {
        def quoteItemAccessoryInstance = QuoteItemAccessory.get(params.id)
        if (!quoteItemAccessoryInstance) {
            flash.message = "quoteItemAccessory.not.found"
            flash.args = [params.id]
            flash.defaultMessage = "QuoteItemAccessory not found with id ${params.id}"
            redirect(action: "list")
        }
        else {
            return [quoteItemAccessoryInstance: quoteItemAccessoryInstance]
        }
    }

    def edit = {
        def quoteItemAccessoryInstance = QuoteItemAccessory.get(params.id)
        if (!quoteItemAccessoryInstance) {
            flash.message = "quoteItemAccessory.not.found"
            flash.args = [params.id]
            flash.defaultMessage = "QuoteItemAccessory not found with id ${params.id}"
            redirect(action: "list")
        }
        else {
            return [quoteItemAccessoryInstance: quoteItemAccessoryInstance]
        }
    }

    def update = {
        def quoteItemAccessoryInstance = QuoteItemAccessory.get(params.id)
        if (quoteItemAccessoryInstance) {
            if (params.version) {
                def version = params.version.toLong()
                if (quoteItemAccessoryInstance.version > version) {
                    
                    quoteItemAccessoryInstance.errors.rejectValue("version", "quoteItemAccessory.optimistic.locking.failure", "Another user has updated this QuoteItemAccessory while you were editing")
                    render(view: "edit", model: [quoteItemAccessoryInstance: quoteItemAccessoryInstance])
                    return
                }
            }
            quoteItemAccessoryInstance.properties = params
            if (!quoteItemAccessoryInstance.hasErrors() && quoteItemAccessoryInstance.save()) {
                flash.message = "quoteItemAccessory.updated"
                flash.args = [params.id]
                flash.defaultMessage = "QuoteItemAccessory ${params.id} updated"
                redirect(action: "show", id: quoteItemAccessoryInstance.id)
            }
            else {
                render(view: "edit", model: [quoteItemAccessoryInstance: quoteItemAccessoryInstance])
            }
        }
        else {
            flash.message = "quoteItemAccessory.not.found"
            flash.args = [params.id]
            flash.defaultMessage = "QuoteItemAccessory not found with id ${params.id}"
            redirect(action: "edit", id: params.id)
        }
    }

    def delete = {
        def quoteItemAccessoryInstance = QuoteItemAccessory.get(params.id)
        if (quoteItemAccessoryInstance) {
            try {
                quoteItemAccessoryInstance.delete()
                flash.message = "quoteItemAccessory.deleted"
                flash.args = [params.id]
                flash.defaultMessage = "QuoteItemAccessory ${params.id} deleted"
                redirect(action: "list")
            }
            catch (org.springframework.dao.DataIntegrityViolationException e) {
                flash.message = "quoteItemAccessory.not.deleted"
                flash.args = [params.id]
                flash.defaultMessage = "QuoteItemAccessory ${params.id} could not be deleted"
                redirect(action: "show", id: params.id)
            }
        }
        else {
            flash.message = "quoteItemAccessory.not.found"
            flash.args = [params.id]
            flash.defaultMessage = "QuoteItemAccessory not found with id ${params.id}"
            redirect(action: "list")
        }
    }
}
