/**
 * 
 */
package com.innovated.iris.domain;

/**
 * @author tamati
 *
 */
class RegisterUserCommand {
	String firstName
	String lastName
	String email
	String mobile
	String employerCode
	String jobRole
	String userName
	String password
	String passwordConfirm
	
	static constraints = {
		firstName(blank:false, size:3..32)
		lastName(blank:false, size:3..32)
		email(blank:false, email:true)
		mobile(blank:false, size:10..13)
//		employerCode(blank:false, size:6..32)
//		jobRole(blank:false, size:6..64)
		userName(blank:false, size:4..10)
		password(blank:false, size:6..16)
		passwordConfirm(validator: { val, obj ->
			if (obj.properties['password'] != val) {
				return ['registerUserCommand.passwords.dontmatch']
			}
		})
	}
}
