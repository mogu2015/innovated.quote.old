package com.innovated.iris.domain

import org.codehaus.groovy.grails.plugins.springsecurity.Secured

//@Secured(['IS_A/UTHENTICATED_FULLY'])
@Secured(['ROLE_SUPER', 'ROLE_ADMIN'])
class PhoneController {
    
    def index = { redirect(action:list,params:params) }

    // the delete, save and update actions only accept POST requests
    static allowedMethods = [delete:'POST', save:'POST', update:'POST']

    def list = {
        params.max = Math.min( params.max ? params.max.toInteger() : 10,  100)
        [ phoneInstanceList: Phone.list( params ), phoneInstanceTotal: Phone.count() ]
    }

    def show = {
        def phoneInstance = Phone.get( params.id )

        if(!phoneInstance) {
            flash.message = "Phone not found with id ${params.id}"
            redirect(action:list)
        }
        else { return [ phoneInstance : phoneInstance ] }
    }

    def delete = {
        def phoneInstance = Phone.get( params.id )
        if(phoneInstance) {
            try {
                phoneInstance.delete(flush:true)
                flash.message = "Phone ${params.id} deleted"
                redirect(action:list)
            }
            catch(org.springframework.dao.DataIntegrityViolationException e) {
                flash.message = "Phone ${params.id} could not be deleted"
                redirect(action:show,id:params.id)
            }
        }
        else {
            flash.message = "Phone not found with id ${params.id}"
            redirect(action:list)
        }
    }

    def edit = {
        def phoneInstance = Phone.get( params.id )

        if(!phoneInstance) {
            flash.message = "Phone not found with id ${params.id}"
            redirect(action:list)
        }
        else {
            return [ phoneInstance : phoneInstance ]
        }
    }

    def update = {
        def phoneInstance = Phone.get( params.id )
        if(phoneInstance) {
            if(params.version) {
                def version = params.version.toLong()
                if(phoneInstance.version > version) {
                    
                    phoneInstance.errors.rejectValue("version", "phone.optimistic.locking.failure", "Another user has updated this Phone while you were editing.")
                    render(view:'edit',model:[phoneInstance:phoneInstance])
                    return
                }
            }
            phoneInstance.properties = params
            if(!phoneInstance.hasErrors() && phoneInstance.save()) {
                flash.message = "Phone ${params.id} updated"
                redirect(action:show,id:phoneInstance.id)
            }
            else {
                render(view:'edit',model:[phoneInstance:phoneInstance])
            }
        }
        else {
            flash.message = "Phone not found with id ${params.id}"
            redirect(action:list)
        }
    }

    def create = {
        def phoneInstance = new Phone()
        phoneInstance.properties = params
        return ['phoneInstance':phoneInstance]
    }

    def save = {
        def phoneInstance = new Phone(params)
        //save the address TO THE ENTITY first!
        def e = Entity.get(params.entity.id)
        e.addToPhones(phoneInstance)
		//save the entity (& FLUSH!!!)
        if(!phoneInstance.hasErrors() && e.save(flush:true)) {
            flash.message = "Phone ${phoneInstance.id} created"
            redirect(action:show,id:phoneInstance.id)
        }
        else {
            render(view:'create',model:[phoneInstance:phoneInstance])
        }
    }
}
