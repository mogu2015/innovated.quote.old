package com.innovated.iris.domain.application

import grails.converters.JSON;

import java.io.OutputStream;

import com.innovated.iris.domain.Quote;
import com.innovated.iris.enums.HomeOwnershipStatus;
import com.innovated.iris.enums.application.FinanceApplicationStatus;

class FinanceApplicationController {

	def financeApplicationService
	
    def index = { redirect(action: "list", params: params) }

    // the delete, save and update actions only accept POST requests
    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def list = {
        params.max = Math.min(params.max ? params.max.toInteger() : 10,  100)
        [financeApplicationInstanceList: FinanceApplication.list(params), financeApplicationInstanceTotal: FinanceApplication.count()]
    }

    def create = {
        def financeApplicationInstance = new FinanceApplication()
        financeApplicationInstance.properties = params
        return [financeApplicationInstance: financeApplicationInstance]
    }

    def save = {
		// First! Convert enums...
		params?.status = FinanceApplicationStatus.get(params?.status?.toInteger())
		params?.residenceCurrStatus = HomeOwnershipStatus.get(params?.residenceCurrStatus?.toInteger())
		
		def quote = Quote.get(params?.quote?.id?.toInteger())
		def financeApplicationInstance = new FinanceApplication(params)
		if (quote && !financeApplicationInstance.hasErrors()) {
			quote.addToFinApplications(financeApplicationInstance)
			if (quote.save(flush:true)) {
				flash.message = "financeApplication.created"
	            flash.args = [financeApplicationInstance.id]
	            flash.defaultMessage = "FinanceApplication ${financeApplicationInstance.id} created"
	            redirect(action: "show", id: financeApplicationInstance.id)
			}
			else {
				render(view: "create", model: [financeApplicationInstance: financeApplicationInstance])
			}
        }
        else {
            render(view: "create", model: [financeApplicationInstance: financeApplicationInstance])
        }
    }
	
	def export = {
		def appl = FinanceApplication.get(params.id)
		if (appl) {
			//financeApplicationService.export(appl, "financeApplication-1", response.outputStream)
			render appl as JSON
		} else {
			render ([success:false, msg:"Finance Application [ID:${params.id}] could not be exported!"]) as JSON
		}
	}

    def show = {
        def financeApplicationInstance = FinanceApplication.get(params.id)
        if (!financeApplicationInstance) {
            flash.message = "financeApplication.not.found"
            flash.args = [params.id]
            flash.defaultMessage = "FinanceApplication not found with id ${params.id}"
            redirect(action: "list")
        }
        else {
            return [financeApplicationInstance: financeApplicationInstance]
        }
    }
	
	

    def edit = {
        def financeApplicationInstance = FinanceApplication.get(params.id)
        if (!financeApplicationInstance) {
            flash.message = "financeApplication.not.found"
            flash.args = [params.id]
            flash.defaultMessage = "FinanceApplication not found with id ${params.id}"
            redirect(action: "list")
        }
        else {
            return [financeApplicationInstance: financeApplicationInstance]
        }
    }
	
	def ajaxUpdate = {
		println params
		
		render "{success:true}" as JSON
	}

    def update = {
        def financeApplicationInstance = FinanceApplication.get(params.id)
        if (financeApplicationInstance) {
            if (params.version) {
                def version = params.version.toLong()
                if (financeApplicationInstance.version > version) {
                    
                    financeApplicationInstance.errors.rejectValue("version", "financeApplication.optimistic.locking.failure", "Another user has updated this FinanceApplication while you were editing")
                    render(view: "edit", model: [financeApplicationInstance: financeApplicationInstance])
                    return
                }
            }
			
			//remove from old quote
			def oldQuote = financeApplicationInstance.quote
			
			// First! Convert enums...
			params?.status = FinanceApplicationStatus.get(params?.status?.toInteger())
			params?.residenceCurrStatus = HomeOwnershipStatus.get(params?.residenceCurrStatus?.toInteger())
			financeApplicationInstance.properties = params
			
			def quote = Quote.get(params?.quote?.id?.toInteger())
            if (oldQuote && quote && !financeApplicationInstance.hasErrors()) {
				// change quote linkage if required...
				if (oldQuote?.id == quote?.id) {
					oldQuote.removeFromFinApplications(financeApplicationInstance)
					quote.addToFinApplications(financeApplicationInstance)
					oldQuote.save(flush:true)
				}
				
				if (quote.save(flush:true)) {
					flash.message = "financeApplication.updated"
					flash.args = [params.id]
					flash.defaultMessage = "FinanceApplication ${params.id} updated"
					redirect(action: "show", id: financeApplicationInstance.id)
				}
				else {
					render(view: "edit", model: [financeApplicationInstance: financeApplicationInstance])
				}
            }
            else {
                render(view: "edit", model: [financeApplicationInstance: financeApplicationInstance])
            }
        }
        else {
            flash.message = "financeApplication.not.found"
            flash.args = [params.id]
            flash.defaultMessage = "FinanceApplication not found with id ${params.id}"
            redirect(action: "edit", id: params.id)
        }
    }

    def delete = {
        def financeApplicationInstance = FinanceApplication.get(params.id)
        if (financeApplicationInstance) {
            try {
                financeApplicationInstance.delete()
                flash.message = "financeApplication.deleted"
                flash.args = [params.id]
                flash.defaultMessage = "FinanceApplication ${params.id} deleted"
                redirect(action: "list")
            }
            catch (org.springframework.dao.DataIntegrityViolationException e) {
                flash.message = "financeApplication.not.deleted"
                flash.args = [params.id]
                flash.defaultMessage = "FinanceApplication ${params.id} could not be deleted"
                redirect(action: "show", id: params.id)
            }
        }
        else {
            flash.message = "financeApplication.not.found"
            flash.args = [params.id]
            flash.defaultMessage = "FinanceApplication not found with id ${params.id}"
            redirect(action: "list")
        }
    }
}
