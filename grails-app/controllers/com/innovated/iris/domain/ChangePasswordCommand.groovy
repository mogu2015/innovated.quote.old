/**
 * 
 */
package com.innovated.iris.domain;

/**
 * @author tamati
 *
 */
class ChangePasswordCommand {
	Long   id
	String passwordOld
	String passwordNew
	String passwordConfirm
	
	static constraints = {
		id(nullable:false)
		passwordOld(blank:false, size:6..16)
		passwordNew(blank:false, size:6..16)
		passwordConfirm(validator: { val, obj ->
			if (obj.properties['passwordNew'] != val) {
				return ['changePasswordCommand.passwords.dontmatch']
			}
		})
	}
}
