package com.innovated.iris.domain

import org.codehaus.groovy.grails.plugins.springsecurity.Secured;
import org.springframework.core.io.ClassPathResource

@Secured(['ROLE_SUPER', 'ROLE_ADMIN'])
class VehicleController {

    def index = { redirect(action: "list", params: params) }

    // the delete, save and update actions only accept POST requests
    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def list = {
        params.max = Math.min(params.max ? params.max.toInteger() : 10,  100)
        [vehicleInstanceList: Vehicle.list(params), vehicleInstanceTotal: Vehicle.count()]
    }

    def create = {
        def vehicleInstance = new Vehicle()
        vehicleInstance.properties = params
        return [vehicleInstance: vehicleInstance]
    }

    def save = {
        def vehicleInstance = new Vehicle(params)
        
		// Get the vehicle photo file from the multi-part request (if one was selected)
		def f = request.getFile('photo')
		if (!f.empty) {
			// List of OK mime-types
			def okcontents = ['image/png', 'image/jpeg', 'image/gif']
			if (!okcontents.contains(f.getContentType())) {
				flash.message = "Vehicle Photo must be one of: ${okcontents}"
				render(view: "create", model: [vehicleInstance: vehicleInstance])
				return;
			}

			// Save the image and mime type 
			vehicleInstance.photo = f.getBytes() 
			vehicleInstance.photoMimeType = f.getContentType() 
			log.info("File uploaded: " + vehicleInstance.photoMimeType)
		}
		
        
        // get rego if we can!
        vehicleInstance.registration = Registration.get(params.registration.id)
        
        def c = Customer.get(params.owner.id)
        if (c != null) {
        	c.addToAssets(vehicleInstance)
		    if (!vehicleInstance.hasErrors() && c.save(flush:true)) {
		        flash.message = "vehicle.created"
		        flash.args = [vehicleInstance.id]
		        flash.defaultMessage = "Vehicle ${vehicleInstance.id} created"
		        redirect(action: "show", id: vehicleInstance.id)
		    }
		    else {
		        render(view: "create", model: [vehicleInstance: vehicleInstance])
		    }
        }
        else {
        	if (!vehicleInstance.hasErrors() && vehicleInstance.save()) {
		        flash.message = "vehicle.created"
		        flash.args = [vehicleInstance.id]
		        flash.defaultMessage = "Vehicle ${vehicleInstance.id} created"
		        redirect(action: "show", id: vehicleInstance.id)
		    }
		    else {
		        render(view: "create", model: [vehicleInstance: vehicleInstance])
		    }
        }
    }

    def show = {
        def vehicleInstance = Vehicle.get(params.id)
        if (!vehicleInstance) {
            flash.message = "vehicle.not.found"
            flash.args = [params.id]
            flash.defaultMessage = "Vehicle not found with id ${params.id}"
            redirect(action: "list")
        }
        else {
            return [vehicleInstance: vehicleInstance]
        }
    }
    
	def vehicle_image = {
		def v = Vehicle.get(params.id)
		if (!v || !v.photo || !v.photoMimeType) {
			
			// load "default" image from the filesystem!
			File imageFile = new ClassPathResource("/images/iris_logo.png").getFile(); 
			OutputStream out = response.getOutputStream();
			out.write(imageFile);
			out.close();
			imageFile.close();
			
			//response.sendError(404)
			//return;
		} else {
			response.setContentType(v.photoMimeType)
			response.setContentLength(v.photo.size())
			OutputStream out = response.getOutputStream();
			out.write(v.photo);
			out.close();
		}
	}

    def edit = {
        def vehicleInstance = Vehicle.get(params.id)
        if (!vehicleInstance) {
            flash.message = "vehicle.not.found"
            flash.args = [params.id]
            flash.defaultMessage = "Vehicle not found with id ${params.id}"
            redirect(action: "list")
        }
        else {
            return [vehicleInstance: vehicleInstance]
        }
    }

    def update = {
        def vehicleInstance = Vehicle.get(params.id)
        vehicleInstance.registration = Registration.get(params.registration.id)
        if (vehicleInstance) {
            if (params.version) {
                def version = params.version.toLong()
                if (vehicleInstance.version > version) {
                    
                    vehicleInstance.errors.rejectValue("version", "vehicle.optimistic.locking.failure", "Another user has updated this Vehicle while you were editing")
                    render(view: "edit", model: [vehicleInstance: vehicleInstance])
                    return
                }
            }
            vehicleInstance.properties = params
            
            // Get the vehicle photo file from the multi-part request (if one was selected)
			if (params.deletePhoto) {
				println "deleting current vehicle photo..."
				vehicleInstance.photo = null
				vehicleInstance.photoMimeType = 'image/png'
			}
			else {
				def f = request.getFile('photo')
				println "photo: " + f
				if (!f.isEmpty()) {
					// List of OK mime-types
					def okcontents = ['image/png', 'image/jpeg', 'image/gif']
					if (!okcontents.contains(f.getContentType())) {
						flash.message = "Vehicle Photo must be one of: ${okcontents}"
						render(view: "create", model: [vehicleInstance: vehicleInstance])
						return;
					}

					// Save the image and mime type 
					vehicleInstance.photo = f.getBytes() 
					vehicleInstance.photoMimeType = f.getContentType() 
					log.info("File uploaded: " + vehicleInstance.photoMimeType)
				}
			}
            
            if (!vehicleInstance.hasErrors() && vehicleInstance.save(flush:true)) {
                flash.message = "vehicle.updated"
                flash.args = [params.id]
                flash.defaultMessage = "Vehicle ${params.id} updated"
                redirect(action: "show", id: vehicleInstance.id)
            }
            else {
                render(view: "edit", model: [vehicleInstance: vehicleInstance])
            }
        }
        else {
            flash.message = "vehicle.not.found"
            flash.args = [params.id]
            flash.defaultMessage = "Vehicle not found with id ${params.id}"
            redirect(action: "edit", id: params.id)
        }
    }

    def delete = {
        def vehicleInstance = Vehicle.get(params.id)
        if (vehicleInstance) {
            try {
                vehicleInstance.delete()
                flash.message = "vehicle.deleted"
                flash.args = [params.id]
                flash.defaultMessage = "Vehicle ${params.id} deleted"
                redirect(action: "list")
            }
            catch (org.springframework.dao.DataIntegrityViolationException e) {
                flash.message = "vehicle.not.deleted"
                flash.args = [params.id]
                flash.defaultMessage = "Vehicle ${params.id} could not be deleted"
                redirect(action: "show", id: params.id)
            }
        }
        else {
            flash.message = "vehicle.not.found"
            flash.args = [params.id]
            flash.defaultMessage = "Vehicle not found with id ${params.id}"
            redirect(action: "list")
        }
    }
}
