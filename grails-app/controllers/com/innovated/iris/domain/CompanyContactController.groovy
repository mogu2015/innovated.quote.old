package com.innovated.iris.domain

import org.codehaus.groovy.grails.plugins.springsecurity.Secured

//@Secured(['IS_AUTHENTICATED_FULLY'])
@Secured(['ROLE_SUPER', 'ROLE_ADMIN'])
class CompanyContactController {
    
    def index = { redirect(action:list,params:params) }

    // the delete, save and update actions only accept POST requests
    static allowedMethods = [delete:'POST', save:'POST', update:'POST']

    def list = {
        params.max = Math.min( params.max ? params.max.toInteger() : 10,  100)
        [ companyContactInstanceList: CompanyContact.list( params ), companyContactInstanceTotal: CompanyContact.count() ]
    }

    def show = {
        def companyContactInstance = CompanyContact.get( params.id )

        if(!companyContactInstance) {
            flash.message = "CompanyContact not found with id ${params.id}"
            redirect(action:list)
        }
        else { return [ companyContactInstance : companyContactInstance ] }
    }

    def delete = {
        def companyContactInstance = CompanyContact.get( params.id )
        if(companyContactInstance) {
            try {
                companyContactInstance.delete(flush:true)
                flash.message = "CompanyContact ${params.id} deleted"
                redirect(action:list)
            }
            catch(org.springframework.dao.DataIntegrityViolationException e) {
                flash.message = "CompanyContact ${params.id} could not be deleted"
                redirect(action:show,id:params.id)
            }
        }
        else {
            flash.message = "CompanyContact not found with id ${params.id}"
            redirect(action:list)
        }
    }

    def edit = {
        def companyContactInstance = CompanyContact.get( params.id )

        if(!companyContactInstance) {
            flash.message = "CompanyContact not found with id ${params.id}"
            redirect(action:list)
        }
        else {
            return [ companyContactInstance : companyContactInstance ]
        }
    }

    def update = {
        def companyContactInstance = CompanyContact.get( params.id )
        if(companyContactInstance) {
            if(params.version) {
                def version = params.version.toLong()
                if(companyContactInstance.version > version) {
                    
                    companyContactInstance.errors.rejectValue("version", "companyContact.optimistic.locking.failure", "Another user has updated this CompanyContact while you were editing.")
                    render(view:'edit',model:[companyContactInstance:companyContactInstance])
                    return
                }
            }
            companyContactInstance.properties = params
            if(!companyContactInstance.hasErrors() && companyContactInstance.save()) {
                flash.message = "CompanyContact ${params.id} updated"
                redirect(action:show,id:companyContactInstance.id)
            }
            else {
                render(view:'edit',model:[companyContactInstance:companyContactInstance])
            }
        }
        else {
            flash.message = "CompanyContact not found with id ${params.id}"
            redirect(action:list)
        }
    }

    def create = {
        def companyContactInstance = new CompanyContact()
        companyContactInstance.properties = params
        return ['companyContactInstance':companyContactInstance]
    }

    def save = {
        def companyContactInstance = new CompanyContact(params)
        //save the address TO THE ENTITY first!
        def e = Entity.get(params.company.id)
        e.addToContacts(companyContactInstance)
		//save the entity (& FLUSH!!!)
        if(!companyContactInstance.hasErrors() && e.save(flush:true)) {
            flash.message = "CompanyContact ${companyContactInstance.id} created"
            redirect(action:show,id:companyContactInstance.id)
        }
        else {
            render(view:'create',model:[companyContactInstance:companyContactInstance])
        }
    }
}
