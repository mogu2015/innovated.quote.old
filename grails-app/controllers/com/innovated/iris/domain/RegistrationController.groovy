package com.innovated.iris.domain

import org.codehaus.groovy.grails.plugins.springsecurity.Secured;

@Secured(['ROLE_SUPER', 'ROLE_ADMIN'])
class RegistrationController {

    def index = { redirect(action: "list", params: params) }

    // the delete, save and update actions only accept POST requests
    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def list = {
        params.max = Math.min(params.max ? params.max.toInteger() : 10,  100)
        [registrationInstanceList: Registration.list(params), registrationInstanceTotal: Registration.count()]
    }

    def create = {
        def registrationInstance = new Registration()
        registrationInstance.properties = params
        return [registrationInstance: registrationInstance]
    }

    /*def save = {
        def registrationInstance = new Registration(params)
        if (!registrationInstance.hasErrors() && registrationInstance.save()) {
		        // rego saved, so try update the related vehicle
		        def v = Vehicle.get(params.vehicle.id)
				if (v != null) {
					v.registration = registrationInstance
				    if (!v.save(flush:true)) {
				    	flash.message = "registration.created.vehicle.not.updated"
						flash.args = [registrationInstance.id]
						flash.defaultMessage = "Registration ${registrationInstance.id} created, but could not update the related vehicle!"
				    }
				    else {
				    	flash.message = "registration.created"
						flash.args = [registrationInstance.id]
						flash.defaultMessage = "Registration ${registrationInstance.id} created"
				    }
				}
		        else {
				    flash.message = "registration.created"
				    flash.args = [registrationInstance.id]
				    flash.defaultMessage = "Registration ${registrationInstance.id} created"
		        }
		        redirect(action: "show", id: registrationInstance.id)
		    }
		    else {
		        render(view: "create", model: [registrationInstance: registrationInstance])
		    }
        }
    }*/
    
    def save = {
        def registrationInstance = new Registration(params)
        if (!registrationInstance.hasErrors() && registrationInstance.save()) {
            flash.message = "registration.created"
            flash.args = [registrationInstance.id]
            flash.defaultMessage = "Registration ${registrationInstance.id} created"
            redirect(action: "show", id: registrationInstance.id)
        }
        else {
            render(view: "create", model: [registrationInstance: registrationInstance])
        }
    }

    def show = {
        def registrationInstance = Registration.get(params.id)
        if (!registrationInstance) {
            flash.message = "registration.not.found"
            flash.args = [params.id]
            flash.defaultMessage = "Registration not found with id ${params.id}"
            redirect(action: "list")
        }
        else {
            return [registrationInstance: registrationInstance]
        }
    }

    def edit = {
        def registrationInstance = Registration.get(params.id)
        if (!registrationInstance) {
            flash.message = "registration.not.found"
            flash.args = [params.id]
            flash.defaultMessage = "Registration not found with id ${params.id}"
            redirect(action: "list")
        }
        else {
            return [registrationInstance: registrationInstance]
        }
    }

    def update = {
        def registrationInstance = Registration.get(params.id)
        if (registrationInstance) {
            if (params.version) {
                def version = params.version.toLong()
                if (registrationInstance.version > version) {
                    
                    registrationInstance.errors.rejectValue("version", "registration.optimistic.locking.failure", "Another user has updated this Registration while you were editing")
                    render(view: "edit", model: [registrationInstance: registrationInstance])
                    return
                }
            }
            registrationInstance.properties = params
            if (!registrationInstance.hasErrors() && registrationInstance.save()) {
                flash.message = "registration.updated"
                flash.args = [params.id]
                flash.defaultMessage = "Registration ${params.id} updated"
                redirect(action: "show", id: registrationInstance.id)
            }
            else {
                render(view: "edit", model: [registrationInstance: registrationInstance])
            }
        }
        else {
            flash.message = "registration.not.found"
            flash.args = [params.id]
            flash.defaultMessage = "Registration not found with id ${params.id}"
            redirect(action: "edit", id: params.id)
        }
    }

    def delete = {
        def registrationInstance = Registration.get(params.id)
        if (registrationInstance) {
            try {
                registrationInstance.delete()
                flash.message = "registration.deleted"
                flash.args = [params.id]
                flash.defaultMessage = "Registration ${params.id} deleted"
                redirect(action: "list")
            }
            catch (org.springframework.dao.DataIntegrityViolationException e) {
                flash.message = "registration.not.deleted"
                flash.args = [params.id]
                flash.defaultMessage = "Registration ${params.id} could not be deleted"
                redirect(action: "show", id: params.id)
            }
        }
        else {
            flash.message = "registration.not.found"
            flash.args = [params.id]
            flash.defaultMessage = "Registration not found with id ${params.id}"
            redirect(action: "list")
        }
    }
}
