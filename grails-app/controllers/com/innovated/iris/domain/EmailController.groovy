package com.innovated.iris.domain

import org.codehaus.groovy.grails.plugins.springsecurity.Secured

//@Secured(['IS_AUTHENTICATED_FULLY'])
@Secured(['ROLE_SUPER', 'ROLE_ADMIN'])
class EmailController {
    
    def index = { redirect(action:list,params:params) }

    // the delete, save and update actions only accept POST requests
    static allowedMethods = [delete:'POST', save:'POST', update:'POST']

    def list = {
        params.max = Math.min( params.max ? params.max.toInteger() : 10,  100)
        [ emailInstanceList: Email.list( params ), emailInstanceTotal: Email.count() ]
    }

    def show = {
        def emailInstance = Email.get( params.id )

        if(!emailInstance) {
            flash.message = "Email not found with id ${params.id}"
            redirect(action:list)
        }
        else { return [ emailInstance : emailInstance ] }
    }

    def delete = {
        def emailInstance = Email.get( params.id )
        if(emailInstance) {
			try {
				def entity = emailInstance.entity
				entity.removeFromEmails(emailInstance)
				emailInstance.delete(flush:true)
                flash.message = "Email ${params.id} deleted"
                redirect(action:list)
            }
            catch(org.springframework.dao.DataIntegrityViolationException e) {
                flash.message = "Email ${params.id} could not be deleted"
                redirect(action:show,id:params.id)
            }
        }
        else {
            flash.message = "Email not found with id ${params.id}"
            redirect(action:list)
        }
    }

    def edit = {
        def emailInstance = Email.get( params.id )

        if(!emailInstance) {
            flash.message = "Email not found with id ${params.id}"
            redirect(action:list)
        }
        else {
            return [ emailInstance : emailInstance ]
        }
    }

    def update = {
        def emailInstance = Email.get( params.id )
        if(emailInstance) {
            if(params.version) {
                def version = params.version.toLong()
                if(emailInstance.version > version) {
                    
                    emailInstance.errors.rejectValue("version", "email.optimistic.locking.failure", "Another user has updated this Email while you were editing.")
                    render(view:'edit',model:[emailInstance:emailInstance])
                    return
                }
            }
            emailInstance.properties = params
            
            Entity e = Entity.get(params.entity.id)
            if (e != null) {
            	e.addToEmails(emailInstance);
            	if(!emailInstance.hasErrors() && e.save(flush:true)) {
		            flash.message = "Email ${params.id} updated"
		            redirect(action:show,id:emailInstance.id)
		        }
		        else {
		            render(view:'edit',model:[emailInstance:emailInstance])
		        }
            }
            else {
		        if(!emailInstance.hasErrors() && emailInstance.save()) {
		            flash.message = "Email ${params.id} updated"
		            redirect(action:show,id:emailInstance.id)
		        }
		        else {
		            render(view:'edit',model:[emailInstance:emailInstance])
		        }
            }
        }
        else {
            flash.message = "Email not found with id ${params.id}"
            redirect(action:list)
        }
    }

    def create = {
        def emailInstance = new Email()
        emailInstance.properties = params
        return ['emailInstance':emailInstance]
    }

    def save = {
        def emailInstance = new Email(params)
        //save the address TO THE ENTITY first!
        def e = Entity.get(params.entity.id)
        e.addToEmails(emailInstance)
		//save the entity (& FLUSH!!!)
        if(!emailInstance.hasErrors() && e.save(flush:true)) {
            flash.message = "Email ${emailInstance.id} created"
            redirect(action:show,id:emailInstance.id)
        }
        else {
            render(view:'create',model:[emailInstance:emailInstance])
        }
    }
}
