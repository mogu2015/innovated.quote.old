package com.innovated.iris.domain

import org.codehaus.groovy.grails.plugins.springsecurity.Secured;

@Secured(['ROLE_SUPER', 'ROLE_ADMIN'])
class AssetController {

    def index = { redirect(action: "list", params: params) }

    // the delete, save and update actions only accept POST requests
    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def list = {
        params.max = Math.min(params.max ? params.max.toInteger() : 10,  100)
        [assetInstanceList: Asset.list(params), assetInstanceTotal: Asset.count()]
    }

    def create = {
        def assetInstance = new Asset()
        assetInstance.properties = params
        return [assetInstance: assetInstance]
    }

    def save = {
        def assetInstance = new Asset(params)
        if (!assetInstance.hasErrors() && assetInstance.save()) {
            flash.message = "asset.created"
            flash.args = [assetInstance.id]
            flash.defaultMessage = "Asset ${assetInstance.id} created"
            redirect(action: "show", id: assetInstance.id)
        }
        else {
            render(view: "create", model: [assetInstance: assetInstance])
        }
    }

    def show = {
        def assetInstance = Asset.get(params.id)
        if (!assetInstance) {
            flash.message = "asset.not.found"
            flash.args = [params.id]
            flash.defaultMessage = "Asset not found with id ${params.id}"
            redirect(action: "list")
        }
        else {
            return [assetInstance: assetInstance]
        }
    }

    def edit = {
        def assetInstance = Asset.get(params.id)
        if (!assetInstance) {
            flash.message = "asset.not.found"
            flash.args = [params.id]
            flash.defaultMessage = "Asset not found with id ${params.id}"
            redirect(action: "list")
        }
        else {
            return [assetInstance: assetInstance]
        }
    }

    def update = {
        def assetInstance = Asset.get(params.id)
        if (assetInstance) {
            if (params.version) {
                def version = params.version.toLong()
                if (assetInstance.version > version) {
                    
                    assetInstance.errors.rejectValue("version", "asset.optimistic.locking.failure", "Another user has updated this Asset while you were editing")
                    render(view: "edit", model: [assetInstance: assetInstance])
                    return
                }
            }
            assetInstance.properties = params
            if (!assetInstance.hasErrors() && assetInstance.save()) {
                flash.message = "asset.updated"
                flash.args = [params.id]
                flash.defaultMessage = "Asset ${params.id} updated"
                redirect(action: "show", id: assetInstance.id)
            }
            else {
                render(view: "edit", model: [assetInstance: assetInstance])
            }
        }
        else {
            flash.message = "asset.not.found"
            flash.args = [params.id]
            flash.defaultMessage = "Asset not found with id ${params.id}"
            redirect(action: "edit", id: params.id)
        }
    }

    def delete = {
        def assetInstance = Asset.get(params.id)
        if (assetInstance) {
            try {
                assetInstance.delete()
                flash.message = "asset.deleted"
                flash.args = [params.id]
                flash.defaultMessage = "Asset ${params.id} deleted"
                redirect(action: "list")
            }
            catch (org.springframework.dao.DataIntegrityViolationException e) {
                flash.message = "asset.not.deleted"
                flash.args = [params.id]
                flash.defaultMessage = "Asset ${params.id} could not be deleted"
                redirect(action: "show", id: params.id)
            }
        }
        else {
            flash.message = "asset.not.found"
            flash.args = [params.id]
            flash.defaultMessage = "Asset not found with id ${params.id}"
            redirect(action: "list")
        }
    }
}
