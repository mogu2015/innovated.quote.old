package com.innovated.iris.domain

import java.text.SimpleDateFormat;

import jxl.*;
import grails.converters.JSON;

import org.codehaus.groovy.grails.plugins.springsecurity.Secured
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import com.innovated.iris.domain.enums.*

//@Secured(['IS_AUTHENTICATED_FULLY'])
@Secured(['ROLE_SUPER', 'ROLE_ADMIN'])
class MakeModelController {
	def makeModelService
	
    def index = { redirect(action: "list", params: params) }

    // the delete, save and update actions only accept POST requests
    static allowedMethods = [save:"POST", update:"POST", delete:"POST", listAjax:"POST", getModelAjax:"POST"]

    def list = {
        params.max = Math.min(params.max ? params.max.toInteger() : 10,  100)
        [makeModelInstanceList: MakeModel.list(params), makeModelInstanceTotal: MakeModel.count()]
    }

    def importFile = {
    	render(view: "import", model: [:])
    }
	
    def saveXls = {
		if (request instanceof MultipartHttpServletRequest) {
			MultipartHttpServletRequest mpr = (MultipartHttpServletRequest)request;
			MultipartFile file = (CommonsMultipartFile) mpr.getFile("importFile");
			if (!file.empty) {
				def result = makeModelService.importFile2(file)
				flash.message = result.flash.msg
				redirect(action: "list")
			}
			else {
				flash.message = 'Please select a file to import!'
				redirect(action: "importFile")
			}
		}   
		else {
			flash.message = 'Invalid import (Error: 5201)'
			redirect(action: "importFile")
		}
	}

	@Secured(['IS_AUTHENTICATED_FULLY'])
    def listAjax = {
		def data = []
		def models = []
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy")
		
		if (params.term) {
			//models = MakeModel.findAllByDescriptionIlike("%"+params.term+"%", [sort:"description", order:"asc"])
			models = MakeModel.findAll("from MakeModel as m where m.model.make.id=:id and m.description like :description order by m.description", [id:params.makeId.toLong(), description:"%"+params.term+"%"])
		}
		else {
			models = MakeModel.findAll("from MakeModel as m where m.model.make.id=:id order by m.description", [id:params.makeId.toLong()])
		}
		models.each {
			data << [
				id:it.id, label:it.description, value:it.description, mk_id:it.model.make.id,
				make:it.model.make.name, md_id:it.model.id, model:it.model.name, mm_id:it.id,
				make_model:it.description, rrp:it.retailPrice, build:sdf.format(it.buildDate)
			]
		}
		render data as JSON
    }

	@Secured(['IS_AUTHENTICATED_FULLY'])
    def getModelAjax = {
		def ajaxResp = [success:false]
		def model = MakeModel.get(params.id)
		if (model) {
			render model as JSON 
		}
		else {
			response.status = 503
			render ajaxResp as JSON
		}
    }

    /*def testme = {
    	def result
		def val = "Sedan,Wagon,Coupe"
		
		def input = val.tokenize(" ,")
		if (input.size() > 0) {
			def invalid = input.findAll { return !MakeModel.VALID_SHAPES.contains(it) }
			result = (invalid.size() == 0)
			//return invalid ?: ['invalid.shape', MakeModel.VALID_SHAPES.join(", ")]
		}
		else {
			result = true
			//return ['invalid.shape', MakeModel.VALID_SHAPES.join(", ")]
		}
		
		render "validator tests: " + result
    }*/
    
    private def buildModel() {
    	def currYr = Calendar.instance.get(Calendar.YEAR)
    	return [minYear:currYr-9, maxYear:currYr+1, insuranceClasses:["000_4CYL", "001_6CYL", "002_4WD", "003_V8PRESTIGE"]]
    }

    def create = {
        def makeModelInstance = new MakeModel()
        makeModelInstance.properties = params
        return [makeModelInstance: makeModelInstance] + buildModel()
    }

    def save = {
        def makeModelInstance = new MakeModel(params)
        def model = VehicleModel.get(params.model.id)
        if (model != null && makeModelInstance.validate()) {
        	model.addToVariants(makeModelInstance)
        	if (model.save(flush:true)) {
		        flash.message = "makeModel.created"
		        flash.args = [makeModelInstance.id]
		        flash.defaultMessage = "MakeModel ${makeModelInstance.id} created"
		        redirect(action: "show", id: makeModelInstance.id)
		    }
		    else {
		        flash.message = "makeModel.not.created"
                flash.defaultMessage = "MakeModel could not be created. Error saving Vehicle Model!"
		        render(view: "create", model: [makeModelInstance: makeModelInstance] + buildModel())
		    }
        } else {
		    flash.message = "makeModel.not.created"
            flash.defaultMessage = "MakeModel could not be created. Vehicle Model cannot be null!"
		    render(view: "create", model: [makeModelInstance: makeModelInstance] + buildModel())
		}
    }

    def show = {
        def makeModelInstance = MakeModel.get(params.id)
        if (!makeModelInstance) {
            flash.message = "makeModel.not.found"
            flash.args = [params.id]
            flash.defaultMessage = "MakeModel not found with id ${params.id}"
            redirect(action: "list")
        }
        else {
            return [makeModelInstance: makeModelInstance]
        }
    }

    def edit = {
        def makeModelInstance = MakeModel.get(params.id)
        if (!makeModelInstance) {
            flash.message = "makeModel.not.found"
            flash.args = [params.id]
            flash.defaultMessage = "MakeModel not found with id ${params.id}"
            redirect(action: "list")
        }
        else {
            return [makeModelInstance: makeModelInstance] + buildModel()
        }
    }

    def update = {
        def makeModelInstance = MakeModel.get(params.id)
        if (makeModelInstance) {
            if (params.version) {
                def version = params.version.toLong()
                if (makeModelInstance.version > version) {
                    makeModelInstance.errors.rejectValue("version", "makeModel.optimistic.locking.failure", "Another user has updated this MakeModel while you were editing")
                    render(view: "edit", model: [makeModelInstance: makeModelInstance] + buildModel())
                    return
                }
            }
            makeModelInstance.properties = params
            
            def model = VehicleModel.get(params.model.id)
	        if (model != null && makeModelInstance.validate()) {
	        	model.addToVariants(makeModelInstance)
	        	if (model.save(flush:true)) {
		            flash.message = "makeModel.updated"
		            flash.args = [params.id]
		            flash.defaultMessage = "MakeModel ${params.id} updated"
		            redirect(action: "show", id: makeModelInstance.id)
		        }
		        else {
		            render(view: "edit", model: [makeModelInstance: makeModelInstance] + buildModel())
		        }
	        } else {
		        flash.message = "makeModel.not.updated"
            	flash.defaultMessage = "MakeModel could not be updated. Vehicle Model cannot be null!"
		        render(view: "edit", model: [makeModelInstance: makeModelInstance] + buildModel())
            }
        }
        else {
            flash.message = "makeModel.not.found"
            flash.args = [params.id]
            flash.defaultMessage = "MakeModel not found with id ${params.id}"
            redirect(action: "edit", id: params.id)
        }
    }

    def delete = {
        def makeModelInstance = MakeModel.get(params.id)
        if (makeModelInstance) {
            try {
                makeModelInstance.delete()
                flash.message = "makeModel.deleted"
                flash.args = [params.id]
                flash.defaultMessage = "MakeModel ${params.id} deleted"
                redirect(action: "list")
            }
            catch (org.springframework.dao.DataIntegrityViolationException e) {
                flash.message = "makeModel.not.deleted"
                flash.args = [params.id]
                flash.defaultMessage = "MakeModel ${params.id} could not be deleted"
                redirect(action: "show", id: params.id)
            }
        }
        else {
            flash.message = "makeModel.not.found"
            flash.args = [params.id]
            flash.defaultMessage = "MakeModel not found with id ${params.id}"
            redirect(action: "list")
        }
    }
}
