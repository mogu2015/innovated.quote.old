package com.innovated.iris.domain

import org.codehaus.groovy.grails.plugins.springsecurity.Secured;

@Secured(['ROLE_SUPER', 'ROLE_ADMIN'])
class NonVehicleController {

    def index = { redirect(action: "list", params: params) }

    // the delete, save and update actions only accept POST requests
    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def list = {
        params.max = Math.min(params.max ? params.max.toInteger() : 10,  100)
        [nonVehicleInstanceList: NonVehicle.list(params), nonVehicleInstanceTotal: NonVehicle.count()]
    }

    def create = {
        def nonVehicleInstance = new NonVehicle()
        nonVehicleInstance.properties = params
        return [nonVehicleInstance: nonVehicleInstance]
    }

    def save = {
        def nonVehicleInstance = new NonVehicle(params)
        def c = Customer.get(params.owner.id)
        if (c != null) {
        	c.addToAssets(nonVehicleInstance)
		    if (!nonVehicleInstance.hasErrors() && c.save(flush:true)) {
		        flash.message = "nonVehicle.created"
		        flash.args = [nonVehicleInstance.id]
		        flash.defaultMessage = "NonVehicle ${nonVehicleInstance.id} created"
		        redirect(action: "show", id: nonVehicleInstance.id)
		    }
		    else {
		        render(view: "create", model: [nonVehicleInstance: nonVehicleInstance])
		    }
        }
        else {
        	if (!nonVehicleInstance.hasErrors() && nonVehicleInstance.save()) {
		        flash.message = "nonVehicle.created"
		        flash.args = [nonVehicleInstance.id]
		        flash.defaultMessage = "NonVehicle ${nonVehicleInstance.id} created"
		        redirect(action: "show", id: nonVehicleInstance.id)
		    }
		    else {
		        render(view: "create", model: [nonVehicleInstance: nonVehicleInstance])
		    }
        }
    }

    def show = {
        def nonVehicleInstance = NonVehicle.get(params.id)
        if (!nonVehicleInstance) {
            flash.message = "nonVehicle.not.found"
            flash.args = [params.id]
            flash.defaultMessage = "NonVehicle not found with id ${params.id}"
            redirect(action: "list")
        }
        else {
            return [nonVehicleInstance: nonVehicleInstance]
        }
    }

    def edit = {
        def nonVehicleInstance = NonVehicle.get(params.id)
        if (!nonVehicleInstance) {
            flash.message = "nonVehicle.not.found"
            flash.args = [params.id]
            flash.defaultMessage = "NonVehicle not found with id ${params.id}"
            redirect(action: "list")
        }
        else {
            return [nonVehicleInstance: nonVehicleInstance]
        }
    }

    def update = {
        def nonVehicleInstance = NonVehicle.get(params.id)
        if (nonVehicleInstance) {
            if (params.version) {
                def version = params.version.toLong()
                if (nonVehicleInstance.version > version) {
                    
                    nonVehicleInstance.errors.rejectValue("version", "nonVehicle.optimistic.locking.failure", "Another user has updated this NonVehicle while you were editing")
                    render(view: "edit", model: [nonVehicleInstance: nonVehicleInstance])
                    return
                }
            }
            nonVehicleInstance.properties = params
            if (!nonVehicleInstance.hasErrors() && nonVehicleInstance.save()) {
                flash.message = "nonVehicle.updated"
                flash.args = [params.id]
                flash.defaultMessage = "NonVehicle ${params.id} updated"
                redirect(action: "show", id: nonVehicleInstance.id)
            }
            else {
                render(view: "edit", model: [nonVehicleInstance: nonVehicleInstance])
            }
        }
        else {
            flash.message = "nonVehicle.not.found"
            flash.args = [params.id]
            flash.defaultMessage = "NonVehicle not found with id ${params.id}"
            redirect(action: "edit", id: params.id)
        }
    }

    def delete = {
        def nonVehicleInstance = NonVehicle.get(params.id)
        if (nonVehicleInstance) {
            try {
                nonVehicleInstance.delete()
                flash.message = "nonVehicle.deleted"
                flash.args = [params.id]
                flash.defaultMessage = "NonVehicle ${params.id} deleted"
                redirect(action: "list")
            }
            catch (org.springframework.dao.DataIntegrityViolationException e) {
                flash.message = "nonVehicle.not.deleted"
                flash.args = [params.id]
                flash.defaultMessage = "NonVehicle ${params.id} could not be deleted"
                redirect(action: "show", id: params.id)
            }
        }
        else {
            flash.message = "nonVehicle.not.found"
            flash.args = [params.id]
            flash.defaultMessage = "NonVehicle not found with id ${params.id}"
            redirect(action: "list")
        }
    }
}
