package com.innovated.iris.domain

import java.text.ParseException;

import grails.converters.JSON;
import grails.util.GrailsNameUtils;

import java.text.SimpleDateFormat;
import java.util.LinkedHashMap;

import org.codehaus.groovy.grails.plugins.springsecurity.Secured
import org.grails.comments.CommentLink;
import com.innovated.iris.server.fbt.*;
import com.innovated.iris.domain.enums.*
import com.innovated.iris.services.*

@Secured(['ROLE_SUPER', 'ROLE_ADMIN'])
class CustomerController {
	private SimpleDateFormat DATE_DISP_FMT = new SimpleDateFormat("d MMM yyyy")
	
	def authenticateService
	def customerService
	
    def index = { redirect(action: "list", params: params) }

    // the delete, save and update actions only accept POST requests
    static allowedMethods = [save: "POST", update: "POST", delete: "POST", personalDetailsAjax:"POST"]

    def list = {
        params.max = Math.min(params.max ? params.max.toInteger() : 10,  100)
        [customerInstanceList: Customer.list(params), customerInstanceTotal: Customer.count()]
    }

	def view = {
		//println "params: ${params}"
		def tabGsp = [
			"customer":  "/templates/ui/customer/customerdetails",
			"entity":    "/templates/ui/customer/viewentityinfo",
			"employees": "/templates/ui/customer/employeelist",
			"assets":    "/templates/ui/customer/customerassets",
			"comments":  "/templates/ui/customer/comments",
			"entityView":"/templates/ui/customer/entityview",
			"entityEdit":"/templates/ui/customer/entityedit"
		]
		def tab = (params.tab && tabGsp.collect{ it.key }.contains(params.tab)) ? params.tab : "customer"
		
		def user = authenticateService.userDomain()
		def customerInstance = Customer.get(params.id)
		//customerInstance.addComment(user, "tab <b>clicked</b>: ${tab}")
		
		if (!customerInstance) {
			render message(code:"customer.view.error", defaultMessage:"Couldn't load content for {0} tab", args:[tab])
		}
		else {
			if (params.prevParams) {
				customerInstance.properties = params.prevParams
			}
			// pick the right Partial GSP template to render the response
			def tmplStr = tabGsp[tab]
			if (tmplStr) {
				def model = buildEditModel(customerInstance)
				/*if ("comments".equals(tmplStr)) {
					model = model + [commentList:customerInstance.comments.sort{ it.dateCreated }]
				}*/
				render(template:tmplStr, model:model)
			}
			else {
				render message(code:"customer.view.error", defaultMessage:"Couldn't load content for {0} tab", args:[tab])
			}
			
		}
		
	}

    def listAjax = {
        def ajax = [success:true, sEcho:params.sEcho, iTotalRecords:0, iTotalDisplayRecords:0, aaData:[]]
		def whereStr = ""
		def orderByStr = ""
		def alias = "c"
		def hdrList = ["chk","icon","code","entity.name","type.name","status.name"]
		
		// manage search/filtering
		if (params.sSearch) {
			def whereCols = []
			hdrList.eachWithIndex { item, idx ->
				if (idx > 1) whereCols << "${alias}.${item} like '%${params.sSearch}%'"
			}
			whereStr = " where " + whereCols.join(" or ")
		}
		
		// work out sorting!
		if (params.iSortingCols && (params.iSortingCols.toInteger() > 0)) {
			def sortCols = []
			def prefix = 'iSortCol_'
			for (String key in params.keySet()) {
				if (key.startsWith(prefix)) {
					def code = Integer.valueOf(key.substring(prefix.length()))
					sortCols << [id:code, column:params.get(key).toInteger(), order:params.get("sSortDir_"+code)]
				}
			}
			orderByStr = " order by " + sortCols.sort{ it.id }.collect{ alias + "." + hdrList.get(it.column) + " " + it.order }.join(", ")
		}
		
		def total = Customer.count()
		def sortParams = [:]
		if (params.iDisplayLength.toInteger() == -1) {
			sortParams.max = total
			sortParams.offset = 0
		}
		else {
			sortParams.max = Math.min(params.iDisplayLength ? params.iDisplayLength.toInteger() : 100,  10000)
			sortParams.offset = params.iDisplayStart ? params.iDisplayStart.toInteger() : 0
		}
		
		def query = "from Customer as ${alias}${whereStr}${orderByStr}"
		def records = Customer.findAll(query, [], sortParams)
		
		ajax.iTotalRecords = total
		ajax.iTotalDisplayRecords = total
		ajax.aaData = records.collect{ cust -> [
			"" + g.checkBox(class:"chkRow", name:"chkRow_"+cust.id, value:false, title:"Select this row", checked:false),
			"" + iris.customerIcon(customer:cust),
			"" + g.link(action:"show", id:cust.id, title:"View this customer record") { cust.code },
			cust.entity.name,
			cust.type.name,
			cust.status.name
		]}
		
		render ajax as JSON
    }

	def comments = {
		def ajax = [success:true, sEcho:params.sEcho, iTotalRecords:0, iTotalDisplayRecords:0, aaData:[]]
		
		def customerInstance = Customer.get(params.id)
		if (customerInstance) {
			//
		}
		
		render ajax as JSON
	}

    /*def create = {
        def customerInstance = new Customer()
        customerInstance.properties = params
        return [customerInstance: customerInstance]
    }
	
	def save = {
		def customerInstance = new Customer(params)
		if (!customerInstance.hasErrors() && customerInstance.save()) {
			flash.message = "customer.created"
			flash.args = [customerInstance.id]
			flash.defaultMessage = "Customer ${customerInstance.id} created"
			redirect(action: "edit", id: customerInstance.id)
		}
		else {
			render(view: "create", model: [customerInstance: customerInstance])
		}
	}*/
	
	def create = {
		def customerInstance = new Customer()
		customerInstance.properties = params
		return [customerInstance: customerInstance]
	}
	
	def save = {
		def customerInstance = new Customer(params)
		if (!customerInstance.hasErrors() && customerInstance.save()) {
			flash.message = "customer.created"
			flash.args = [customerInstance.id]
			flash.defaultMessage = "Customer ${customerInstance.id} created"
			redirect(action: "edit", id: customerInstance.id)
		}
		else {
			render(view: "create", model: [customerInstance: customerInstance])
		}
	}

	def newCompany = {
		def c = customerService.newCustomer(Entity.COMPANY, null)
		render(view:"createCompany", model:buildEditModel(c))
	}

	def newIndividual = {
		def c = customerService.newCustomer(Entity.INDIVIDUAL, null)
		render(view:"createIndividual", model:buildEditModel(c))
	}
	
	def saveCompany = {
		def c = customerService.newCustomer(Entity.COMPANY, null)
		c.properties = params
		c.entity.name = params.entity.tradingName
		
		// check the FBT related fields for this new company
		if (!preSaveCheck(c.entity)) {
			render(view:"createCompany", model:buildEditModel(c))
			return
		}
		
		c = customerService.save(c, params)	// save the sucker
		if (c.hasErrors() || c.entity?.hasErrors()) {
			render(view:"createCompany", model:buildEditModel(c))
		}
		else {
			redirect(action: "show", id: c?.id)
		}
	}
	
	def saveIndividual = {
		def c = customerService.newCustomer(Entity.INDIVIDUAL, null)
		c.properties = params
		c.entity.name = [params?.entity?.lastName, params?.entity?.firstName].join(', ')
		c = customerService.save(c, params)	// save the sucker
		
		if (c.hasErrors() || c.entity?.hasErrors()) {
			render(view:"createIndividual", model:buildEditModel(c))
		}
		else {
			redirect(action: "show", id: c?.id)
		}
	}

    def personalDetailsAjax = {
		def ajaxResp = [success:false, title:"Error getting customer details", msg:"An error was encountered trying to retrieve this customers details..."]
		def customerInstance = Customer.get(params.id)
		if (customerInstance) {
			ajaxResp.success = true
			ajaxResp.title = 'Customer Details'
			ajaxResp.msg = 'Customer details retrieved successfully'
			
			// get addr, phone, mobiles, etc
			def addresses = customerInstance.entity.addresses.findAll { it.addressType.name == "Street Address" }
			def mobiles = customerInstance.entity.phones.findAll { it.phoneType.name == "Mobile" }
			def emails = customerInstance.entity.emails.findAll { it.defaultType == "To" }
			def jobs = Employment.findAll("from Employment as e where e.employee=:emp order by e.startedEmployment desc", [emp:customerInstance.entity])
			
			/*println "cust: " + customerInstance?.entity
			println "  addresses: " + addresses
			println "  emails:    " + emails
			println "  mobiles:   " + mobiles
			println "  jobs:      " + jobs*/
			
			ajaxResp.addresses = addresses.collect{ [value:it.id, text:it.toString()] }
			ajaxResp.mobiles = mobiles.collect{ [value:it.id, text:it.toString()] }
			ajaxResp.emails = emails.collect{ [value:it.id, text:it.toString()] }
			ajaxResp.jobs = jobs.collect{ [value:it.id, text:it.toString()] }
			ajaxResp.salaries = jobs.findAll{ it.grossSalary != null }.collect{ [value:it.id, text:it.grossSalary] }
		}
		render ajaxResp as JSON
    }
	
	def validateAddress = {
		def ajaxResp = [success:false, title:"Address Created", msg:"Address is valid"]
		println params
		//
		render ajaxResp as JSON
	}

    def show = {
    	def customerInstance
    	if (params.code && !Customer.BASE_CUST_CODE.equals(params.code)) {
    		customerInstance = Customer.findByCode(params.code)
    	} else {
    		customerInstance = Customer.get(params.id)
    	}
        
        if (!customerInstance) {
            flash.message = "customer.not.found"
            flash.args = [params.id]
            flash.defaultMessage = "The selected Customer could not be found"
            redirect(action: "list")
        }
        else {
            //return [customerInstance: customerInstance]
			return buildEditModel(customerInstance)
        }
    }

    def edit = {
        def customerInstance = Customer.get(params.id)
        if (!customerInstance) {
            flash.message = "customer.not.found"
            flash.args = [params.id]
            flash.defaultMessage = "Customer not found with id ${params.id}"
            redirect(action: "list")
        }
        else {
			return buildEditModel(customerInstance)
        }
    }

    def update = {
        def customerInstance = Customer.get(params.id)
        if (customerInstance) {
            if (params.version) {
                def version = params.version.toLong()
                if (customerInstance.version > version) {
                    customerInstance.errors.rejectValue("version", "customer.optimistic.locking.failure", "Another user has updated this Customer while you were editing")
                    //
                    render(view: "edit", model: [customerInstance: customerInstance])
                    return
                }
            }
            customerInstance.properties = params
			try {
				customerInstance.joined = DATE_DISP_FMT.parse(params.joinedDate)
			} catch(ParseException) {
				customerInstance.joined = null
			}
			try {
				customerInstance.ceased = DATE_DISP_FMT.parse(params.ceasedDate)
			} catch(ParseException) {
				customerInstance.ceased = null
			}
			
            if (!customerInstance.hasErrors() && customerInstance.save()) {
                flash.message = "customer.updated"
                flash.args = [ customerInstance?.entity?.name ]
				flash.classes = "flashOk"
                flash.defaultMessage = "Customer ${params.id} updated"
                redirect(action: "edit", id: customerInstance.id)
            }
            else {
				println "we have errors!!"
                render(view: "edit", model:buildEditModel(customerInstance) + [prevParams:customerInstance.properties])
            }
        }
        else {
            flash.message = "customer.not.found"
            flash.args = [params.id]
			flash.classes = "flashErr"
            flash.defaultMessage = "Customer not found with id ${params.id}"
            redirect(action: "edit", id: params.id)
        }
    }
	
	def delete = {
		def customerInstance = Customer.get(params.id)
		if (customerInstance) {
			try {
				customerInstance.delete()
				flash.message = "customer.deleted"
				flash.args = [params.id]
				flash.defaultMessage = "Customer ${params.id} deleted"
				redirect(action: "list")
			}
			catch (org.springframework.dao.DataIntegrityViolationException e) {
				flash.message = "customer.not.deleted"
				flash.args = [params.id]
				flash.defaultMessage = "Customer ${params.id} could not be deleted"
				redirect(action: "show", id: params.id)
			}
		}
		else {
			flash.message = "customer.not.found"
			flash.args = [params.id]
			flash.defaultMessage = "Customer not found with id ${params.id}"
			redirect(action: "list")
		}
	}
	
	
	//==================================================== UTILITY METHODS
	private def buildEditModel(customerInstance) {
		def currConcession = null
		LinkedHashMap<TaxableValueType, Boolean> tvTypeMap = [:]
		if (customerInstance.entity.type == "Company") {
			def company = (Company) customerInstance.entity 
			currConcession = company.concessions.max{ it.dateFrom }
			Map<Integer,TaxableValueType> tvTypes = TaxableValueType.lookup
			for (tv in tvTypes) {
				tvTypeMap[(tv)] = company.allowedFbtCalcMethods.contains(tv.value)
			}
		}
		
		// get comments for this customer
		def comments = CommentLink.withCriteria {
			projections { property "comment" }
			eq "commentRef", customerInstance.id
			eq "type", GrailsNameUtils.getPropertyName(Customer.class)
		}
		
		return [customerInstance:customerInstance, taxableValueMap:tvTypeMap,
			concessionMap:FbtConcessionType.lookup, currentConcession:currConcession,
			commentList:comments.sort{ it.dateCreated }.reverse()
		]
	}
	
	private boolean preSaveCheck(company) {
		boolean checkPassed = true
		if (company.type == "Company") {
			//=== make sure Employer Benefit share figures are NOT blank!
			if (company.employerShareFee == null) company.employerShareFee = 0.0
			if (company.employerShareRate == null) company.employerShareRate = 0.0
			
			//=== deal with Taxable Value preferences
			if (params.tv_default) {
				def validCodes = []
				def prefix = 'ALLOWED_TV_'
				company.allowedFbtCalcMethods.clear()	//remove all FBT taxable value types first
				
				for (String key in params.keySet()) {
					if (key.contains(prefix) && 'on' == params.get(key)) {
						def code = Integer.valueOf(key.substring(prefix.length()))
						validCodes << code
						company.addToAllowedFbtCalcMethods(TaxableValueType.get(code))
					}
				}
				// pick up the default...
				def defaultCode = Integer.valueOf(params.tv_default)
				if (validCodes.contains(defaultCode)) {
					// move the default code to be first index in the list: allowedFbtCalcMethods
					TaxableValueType tvDefault = TaxableValueType.get(defaultCode)
					def idx = company.allowedFbtCalcMethods.indexOf(tvDefault)
					TaxableValueType removed = company.allowedFbtCalcMethods.remove(idx)
					company.allowedFbtCalcMethods.add(0, removed)
				}
				else  {
					// raise an error on this company record? etc...
					company.errors.reject("company.allowedFbtCalcMethods", "Errors in allowed FBT methods!")
					checkPassed = false
				}
				
				//=== deal with FBT concession preferences
				def currConcession = Integer.valueOf(params.get("entity.concessions.current"))
				if (currConcession) {
					if (company.concessions.size() == 0) {
						//no concessions currently, so add whatever has been selected
						company.concessions << new CompanyFbtConcession(dateFrom:new Date(), type:FbtConcessionType.get(currConcession))
					}
					else {
						//we already have concessions, so we only add a new one if it's different from the current
						CompanyFbtConcession old = company.concessions.max{ it.dateFrom }
						if (old.type != FbtConcessionType.get(currConcession)) {
							company.concessions << new CompanyFbtConcession(dateFrom:new Date(), type:FbtConcessionType.get(currConcession))
						}
					}
				} else {
					company.errors.reject("company.concessions", "Errors in company concessions!")
					checkPassed = false
				}
			}
			else {
				company.errors.reject("company.allowedFbtCalcMethods", "Errors in allowed FBT methods!")
				checkPassed = false
			}
		}
		return checkPassed
	}

    
}
