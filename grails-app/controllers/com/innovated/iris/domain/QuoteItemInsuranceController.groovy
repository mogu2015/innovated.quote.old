package com.innovated.iris.domain

import org.codehaus.groovy.grails.plugins.springsecurity.Secured;

@Secured(['ROLE_SUPER', 'ROLE_ADMIN'])
class QuoteItemInsuranceController {

    def index = { redirect(action: "list", params: params) }

    // the delete, save and update actions only accept POST requests
    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def list = {
        params.max = Math.min(params.max ? params.max.toInteger() : 10,  100)
        [quoteItemInsuranceInstanceList: QuoteItemInsurance.list(params), quoteItemInsuranceInstanceTotal: QuoteItemInsurance.count()]
    }

    def create = {
        def quoteItemInsuranceInstance = new QuoteItemInsurance()
        quoteItemInsuranceInstance.properties = params
        return [quoteItemInsuranceInstance: quoteItemInsuranceInstance]
    }

    def save = {
        def quoteItemInsuranceInstance = new QuoteItemInsurance(params)
        if (!quoteItemInsuranceInstance.hasErrors() && quoteItemInsuranceInstance.save()) {
            flash.message = "quoteItemInsurance.created"
            flash.args = [quoteItemInsuranceInstance.id]
            flash.defaultMessage = "QuoteItemInsurance ${quoteItemInsuranceInstance.id} created"
            redirect(action: "show", id: quoteItemInsuranceInstance.id)
        }
        else {
            render(view: "create", model: [quoteItemInsuranceInstance: quoteItemInsuranceInstance])
        }
    }

    def show = {
        def quoteItemInsuranceInstance = QuoteItemInsurance.get(params.id)
        if (!quoteItemInsuranceInstance) {
            flash.message = "quoteItemInsurance.not.found"
            flash.args = [params.id]
            flash.defaultMessage = "QuoteItemInsurance not found with id ${params.id}"
            redirect(action: "list")
        }
        else {
            return [quoteItemInsuranceInstance: quoteItemInsuranceInstance]
        }
    }

    def edit = {
        def quoteItemInsuranceInstance = QuoteItemInsurance.get(params.id)
        if (!quoteItemInsuranceInstance) {
            flash.message = "quoteItemInsurance.not.found"
            flash.args = [params.id]
            flash.defaultMessage = "QuoteItemInsurance not found with id ${params.id}"
            redirect(action: "list")
        }
        else {
            return [quoteItemInsuranceInstance: quoteItemInsuranceInstance]
        }
    }

    def update = {
        def quoteItemInsuranceInstance = QuoteItemInsurance.get(params.id)
        if (quoteItemInsuranceInstance) {
            if (params.version) {
                def version = params.version.toLong()
                if (quoteItemInsuranceInstance.version > version) {
                    
                    quoteItemInsuranceInstance.errors.rejectValue("version", "quoteItemInsurance.optimistic.locking.failure", "Another user has updated this QuoteItemInsurance while you were editing")
                    render(view: "edit", model: [quoteItemInsuranceInstance: quoteItemInsuranceInstance])
                    return
                }
            }
            quoteItemInsuranceInstance.properties = params
            if (!quoteItemInsuranceInstance.hasErrors() && quoteItemInsuranceInstance.save()) {
                flash.message = "quoteItemInsurance.updated"
                flash.args = [params.id]
                flash.defaultMessage = "QuoteItemInsurance ${params.id} updated"
                redirect(action: "show", id: quoteItemInsuranceInstance.id)
            }
            else {
                render(view: "edit", model: [quoteItemInsuranceInstance: quoteItemInsuranceInstance])
            }
        }
        else {
            flash.message = "quoteItemInsurance.not.found"
            flash.args = [params.id]
            flash.defaultMessage = "QuoteItemInsurance not found with id ${params.id}"
            redirect(action: "edit", id: params.id)
        }
    }

    def delete = {
        def quoteItemInsuranceInstance = QuoteItemInsurance.get(params.id)
        if (quoteItemInsuranceInstance) {
            try {
                quoteItemInsuranceInstance.delete()
                flash.message = "quoteItemInsurance.deleted"
                flash.args = [params.id]
                flash.defaultMessage = "QuoteItemInsurance ${params.id} deleted"
                redirect(action: "list")
            }
            catch (org.springframework.dao.DataIntegrityViolationException e) {
                flash.message = "quoteItemInsurance.not.deleted"
                flash.args = [params.id]
                flash.defaultMessage = "QuoteItemInsurance ${params.id} could not be deleted"
                redirect(action: "show", id: params.id)
            }
        }
        else {
            flash.message = "quoteItemInsurance.not.found"
            flash.args = [params.id]
            flash.defaultMessage = "QuoteItemInsurance not found with id ${params.id}"
            redirect(action: "list")
        }
    }
}
