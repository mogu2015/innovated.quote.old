package com.innovated.iris.domain.enums

import org.codehaus.groovy.grails.plugins.springsecurity.Secured;

@Secured(['ROLE_SUPER', 'ROLE_ADMIN'])
class NonVehicleTypeController {

    def index = { redirect(action: "list", params: params) }

    // the delete, save and update actions only accept POST requests
    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def list = {
        params.max = Math.min(params.max ? params.max.toInteger() : 10,  100)
        [nonVehicleTypeInstanceList: NonVehicleType.list(params), nonVehicleTypeInstanceTotal: NonVehicleType.count()]
    }

    def create = {
        def nonVehicleTypeInstance = new NonVehicleType()
        nonVehicleTypeInstance.properties = params
        return [nonVehicleTypeInstance: nonVehicleTypeInstance]
    }

    def save = {
        def nonVehicleTypeInstance = new NonVehicleType(params)
        if (!nonVehicleTypeInstance.hasErrors() && nonVehicleTypeInstance.save()) {
            flash.message = "nonVehicleType.created"
            flash.args = [nonVehicleTypeInstance.id]
            flash.defaultMessage = "NonVehicleType ${nonVehicleTypeInstance.id} created"
            redirect(action: "show", id: nonVehicleTypeInstance.id)
        }
        else {
            render(view: "create", model: [nonVehicleTypeInstance: nonVehicleTypeInstance])
        }
    }

    def show = {
        def nonVehicleTypeInstance = NonVehicleType.get(params.id)
        if (!nonVehicleTypeInstance) {
            flash.message = "nonVehicleType.not.found"
            flash.args = [params.id]
            flash.defaultMessage = "NonVehicleType not found with id ${params.id}"
            redirect(action: "list")
        }
        else {
            return [nonVehicleTypeInstance: nonVehicleTypeInstance]
        }
    }

    def edit = {
        def nonVehicleTypeInstance = NonVehicleType.get(params.id)
        if (!nonVehicleTypeInstance) {
            flash.message = "nonVehicleType.not.found"
            flash.args = [params.id]
            flash.defaultMessage = "NonVehicleType not found with id ${params.id}"
            redirect(action: "list")
        }
        else {
            return [nonVehicleTypeInstance: nonVehicleTypeInstance]
        }
    }

    def update = {
        def nonVehicleTypeInstance = NonVehicleType.get(params.id)
        if (nonVehicleTypeInstance) {
            if (params.version) {
                def version = params.version.toLong()
                if (nonVehicleTypeInstance.version > version) {
                    
                    nonVehicleTypeInstance.errors.rejectValue("version", "nonVehicleType.optimistic.locking.failure", "Another user has updated this NonVehicleType while you were editing")
                    render(view: "edit", model: [nonVehicleTypeInstance: nonVehicleTypeInstance])
                    return
                }
            }
            nonVehicleTypeInstance.properties = params
            if (!nonVehicleTypeInstance.hasErrors() && nonVehicleTypeInstance.save()) {
                flash.message = "nonVehicleType.updated"
                flash.args = [params.id]
                flash.defaultMessage = "NonVehicleType ${params.id} updated"
                redirect(action: "show", id: nonVehicleTypeInstance.id)
            }
            else {
                render(view: "edit", model: [nonVehicleTypeInstance: nonVehicleTypeInstance])
            }
        }
        else {
            flash.message = "nonVehicleType.not.found"
            flash.args = [params.id]
            flash.defaultMessage = "NonVehicleType not found with id ${params.id}"
            redirect(action: "edit", id: params.id)
        }
    }

    def delete = {
        def nonVehicleTypeInstance = NonVehicleType.get(params.id)
        if (nonVehicleTypeInstance) {
            try {
                nonVehicleTypeInstance.delete()
                flash.message = "nonVehicleType.deleted"
                flash.args = [params.id]
                flash.defaultMessage = "NonVehicleType ${params.id} deleted"
                redirect(action: "list")
            }
            catch (org.springframework.dao.DataIntegrityViolationException e) {
                flash.message = "nonVehicleType.not.deleted"
                flash.args = [params.id]
                flash.defaultMessage = "NonVehicleType ${params.id} could not be deleted"
                redirect(action: "show", id: params.id)
            }
        }
        else {
            flash.message = "nonVehicleType.not.found"
            flash.args = [params.id]
            flash.defaultMessage = "NonVehicleType not found with id ${params.id}"
            redirect(action: "list")
        }
    }
}
