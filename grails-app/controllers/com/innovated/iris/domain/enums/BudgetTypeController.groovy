package com.innovated.iris.domain.enums

import org.codehaus.groovy.grails.plugins.springsecurity.Secured;

@Secured(['ROLE_SUPER', 'ROLE_ADMIN'])
class BudgetTypeController {

    def index = { redirect(action: "list", params: params) }

    // the delete, save and update actions only accept POST requests
    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def list = {
        params.max = Math.min(params.max ? params.max.toInteger() : 10,  100)
        [budgetTypeInstanceList: BudgetType.list(params), budgetTypeInstanceTotal: BudgetType.count()]
    }

    def create = {
        def budgetTypeInstance = new BudgetType()
        budgetTypeInstance.properties = params
        return [budgetTypeInstance: budgetTypeInstance]
    }

    def save = {
        def budgetTypeInstance = new BudgetType(params)
        if (!budgetTypeInstance.hasErrors() && budgetTypeInstance.save()) {
            flash.message = "budgetType.created"
            flash.args = [budgetTypeInstance.id]
            flash.defaultMessage = "BudgetType ${budgetTypeInstance.id} created"
            redirect(action: "show", id: budgetTypeInstance.id)
        }
        else {
            render(view: "create", model: [budgetTypeInstance: budgetTypeInstance])
        }
    }

    def show = {
        def budgetTypeInstance = BudgetType.get(params.id)
        if (!budgetTypeInstance) {
            flash.message = "budgetType.not.found"
            flash.args = [params.id]
            flash.defaultMessage = "BudgetType not found with id ${params.id}"
            redirect(action: "list")
        }
        else {
            return [budgetTypeInstance: budgetTypeInstance]
        }
    }

    def edit = {
        def budgetTypeInstance = BudgetType.get(params.id)
        if (!budgetTypeInstance) {
            flash.message = "budgetType.not.found"
            flash.args = [params.id]
            flash.defaultMessage = "BudgetType not found with id ${params.id}"
            redirect(action: "list")
        }
        else {
            return [budgetTypeInstance: budgetTypeInstance]
        }
    }

    def update = {
        def budgetTypeInstance = BudgetType.get(params.id)
        if (budgetTypeInstance) {
            if (params.version) {
                def version = params.version.toLong()
                if (budgetTypeInstance.version > version) {
                    
                    budgetTypeInstance.errors.rejectValue("version", "budgetType.optimistic.locking.failure", "Another user has updated this BudgetType while you were editing")
                    render(view: "edit", model: [budgetTypeInstance: budgetTypeInstance])
                    return
                }
            }
            budgetTypeInstance.properties = params
            if (!budgetTypeInstance.hasErrors() && budgetTypeInstance.save()) {
                flash.message = "budgetType.updated"
                flash.args = [params.id]
                flash.defaultMessage = "BudgetType ${params.id} updated"
                redirect(action: "show", id: budgetTypeInstance.id)
            }
            else {
                render(view: "edit", model: [budgetTypeInstance: budgetTypeInstance])
            }
        }
        else {
            flash.message = "budgetType.not.found"
            flash.args = [params.id]
            flash.defaultMessage = "BudgetType not found with id ${params.id}"
            redirect(action: "edit", id: params.id)
        }
    }

    def delete = {
        def budgetTypeInstance = BudgetType.get(params.id)
        if (budgetTypeInstance) {
            try {
                budgetTypeInstance.delete()
                flash.message = "budgetType.deleted"
                flash.args = [params.id]
                flash.defaultMessage = "BudgetType ${params.id} deleted"
                redirect(action: "list")
            }
            catch (org.springframework.dao.DataIntegrityViolationException e) {
                flash.message = "budgetType.not.deleted"
                flash.args = [params.id]
                flash.defaultMessage = "BudgetType ${params.id} could not be deleted"
                redirect(action: "show", id: params.id)
            }
        }
        else {
            flash.message = "budgetType.not.found"
            flash.args = [params.id]
            flash.defaultMessage = "BudgetType not found with id ${params.id}"
            redirect(action: "list")
        }
    }
}
