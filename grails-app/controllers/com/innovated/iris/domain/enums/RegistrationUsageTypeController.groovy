package com.innovated.iris.domain.enums

import org.codehaus.groovy.grails.plugins.springsecurity.Secured;

@Secured(['ROLE_SUPER', 'ROLE_ADMIN'])
class RegistrationUsageTypeController {

    def index = { redirect(action: "list", params: params) }

    // the delete, save and update actions only accept POST requests
    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def list = {
        params.max = Math.min(params.max ? params.max.toInteger() : 10,  100)
        [registrationUsageTypeInstanceList: RegistrationUsageType.list(params), registrationUsageTypeInstanceTotal: RegistrationUsageType.count()]
    }

    def create = {
        def registrationUsageTypeInstance = new RegistrationUsageType()
        registrationUsageTypeInstance.properties = params
        return [registrationUsageTypeInstance: registrationUsageTypeInstance]
    }

    def save = {
        def registrationUsageTypeInstance = new RegistrationUsageType(params)
        if (!registrationUsageTypeInstance.hasErrors() && registrationUsageTypeInstance.save()) {
            flash.message = "registrationUsageType.created"
            flash.args = [registrationUsageTypeInstance.id]
            flash.defaultMessage = "RegistrationUsageType ${registrationUsageTypeInstance.id} created"
            redirect(action: "show", id: registrationUsageTypeInstance.id)
        }
        else {
            render(view: "create", model: [registrationUsageTypeInstance: registrationUsageTypeInstance])
        }
    }

    def show = {
        def registrationUsageTypeInstance = RegistrationUsageType.get(params.id)
        if (!registrationUsageTypeInstance) {
            flash.message = "registrationUsageType.not.found"
            flash.args = [params.id]
            flash.defaultMessage = "RegistrationUsageType not found with id ${params.id}"
            redirect(action: "list")
        }
        else {
            return [registrationUsageTypeInstance: registrationUsageTypeInstance]
        }
    }

    def edit = {
        def registrationUsageTypeInstance = RegistrationUsageType.get(params.id)
        if (!registrationUsageTypeInstance) {
            flash.message = "registrationUsageType.not.found"
            flash.args = [params.id]
            flash.defaultMessage = "RegistrationUsageType not found with id ${params.id}"
            redirect(action: "list")
        }
        else {
            return [registrationUsageTypeInstance: registrationUsageTypeInstance]
        }
    }

    def update = {
        def registrationUsageTypeInstance = RegistrationUsageType.get(params.id)
        if (registrationUsageTypeInstance) {
            if (params.version) {
                def version = params.version.toLong()
                if (registrationUsageTypeInstance.version > version) {
                    
                    registrationUsageTypeInstance.errors.rejectValue("version", "registrationUsageType.optimistic.locking.failure", "Another user has updated this RegistrationUsageType while you were editing")
                    render(view: "edit", model: [registrationUsageTypeInstance: registrationUsageTypeInstance])
                    return
                }
            }
            registrationUsageTypeInstance.properties = params
            if (!registrationUsageTypeInstance.hasErrors() && registrationUsageTypeInstance.save()) {
                flash.message = "registrationUsageType.updated"
                flash.args = [params.id]
                flash.defaultMessage = "RegistrationUsageType ${params.id} updated"
                redirect(action: "show", id: registrationUsageTypeInstance.id)
            }
            else {
                render(view: "edit", model: [registrationUsageTypeInstance: registrationUsageTypeInstance])
            }
        }
        else {
            flash.message = "registrationUsageType.not.found"
            flash.args = [params.id]
            flash.defaultMessage = "RegistrationUsageType not found with id ${params.id}"
            redirect(action: "edit", id: params.id)
        }
    }

    def delete = {
        def registrationUsageTypeInstance = RegistrationUsageType.get(params.id)
        if (registrationUsageTypeInstance) {
            try {
                registrationUsageTypeInstance.delete()
                flash.message = "registrationUsageType.deleted"
                flash.args = [params.id]
                flash.defaultMessage = "RegistrationUsageType ${params.id} deleted"
                redirect(action: "list")
            }
            catch (org.springframework.dao.DataIntegrityViolationException e) {
                flash.message = "registrationUsageType.not.deleted"
                flash.args = [params.id]
                flash.defaultMessage = "RegistrationUsageType ${params.id} could not be deleted"
                redirect(action: "show", id: params.id)
            }
        }
        else {
            flash.message = "registrationUsageType.not.found"
            flash.args = [params.id]
            flash.defaultMessage = "RegistrationUsageType not found with id ${params.id}"
            redirect(action: "list")
        }
    }
}
