package com.innovated.iris.domain.enums

import org.codehaus.groovy.grails.plugins.springsecurity.Secured

@Secured(['ROLE_SUPER', 'ROLE_ADMIN'])
class CustomerTypeController {
    
    def index = { redirect(action:list,params:params) }

    // the delete, save and update actions only accept POST requests
    static allowedMethods = [delete:'POST', save:'POST', update:'POST']

    def list = {
        params.max = Math.min( params.max ? params.max.toInteger() : 10,  100)
        [ customerTypeInstanceList: CustomerType.list( params ), customerTypeInstanceTotal: CustomerType.count() ]
    }

    def show = {
        def customerTypeInstance = CustomerType.get( params.id )

        if(!customerTypeInstance) {
            flash.message = "CustomerType not found with id ${params.id}"
            redirect(action:list)
        }
        else { return [ customerTypeInstance : customerTypeInstance ] }
    }

    def delete = {
        def customerTypeInstance = CustomerType.get( params.id )
        if(customerTypeInstance) {
            try {
                customerTypeInstance.delete(flush:true)
                flash.message = "CustomerType ${params.id} deleted"
                redirect(action:list)
            }
            catch(org.springframework.dao.DataIntegrityViolationException e) {
                flash.message = "CustomerType ${params.id} could not be deleted"
                redirect(action:show,id:params.id)
            }
        }
        else {
            flash.message = "CustomerType not found with id ${params.id}"
            redirect(action:list)
        }
    }

    def edit = {
        def customerTypeInstance = CustomerType.get( params.id )

        if(!customerTypeInstance) {
            flash.message = "CustomerType not found with id ${params.id}"
            redirect(action:list)
        }
        else {
            return [ customerTypeInstance : customerTypeInstance ]
        }
    }

    def update = {
        def customerTypeInstance = CustomerType.get( params.id )
        if(customerTypeInstance) {
            if(params.version) {
                def version = params.version.toLong()
                if(customerTypeInstance.version > version) {
                    
                    customerTypeInstance.errors.rejectValue("version", "customerType.optimistic.locking.failure", "Another user has updated this CustomerType while you were editing.")
                    render(view:'edit',model:[customerTypeInstance:customerTypeInstance])
                    return
                }
            }
            customerTypeInstance.properties = params
            if(!customerTypeInstance.hasErrors() && customerTypeInstance.save()) {
                flash.message = "CustomerType ${params.id} updated"
                redirect(action:show,id:customerTypeInstance.id)
            }
            else {
                render(view:'edit',model:[customerTypeInstance:customerTypeInstance])
            }
        }
        else {
            flash.message = "CustomerType not found with id ${params.id}"
            redirect(action:list)
        }
    }

    def create = {
        def customerTypeInstance = new CustomerType()
        customerTypeInstance.properties = params
        return ['customerTypeInstance':customerTypeInstance]
    }

    def save = {
        def customerTypeInstance = new CustomerType(params)
        if(!customerTypeInstance.hasErrors() && customerTypeInstance.save()) {
            flash.message = "CustomerType ${customerTypeInstance.id} created"
            redirect(action:show,id:customerTypeInstance.id)
        }
        else {
            render(view:'create',model:[customerTypeInstance:customerTypeInstance])
        }
    }
}
