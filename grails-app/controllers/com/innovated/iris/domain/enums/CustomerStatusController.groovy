package com.innovated.iris.domain.enums

import org.codehaus.groovy.grails.plugins.springsecurity.Secured

@Secured(['ROLE_SUPER', 'ROLE_ADMIN'])
class CustomerStatusController {
    
    def index = { redirect(action:list,params:params) }

    // the delete, save and update actions only accept POST requests
    static allowedMethods = [delete:'POST', save:'POST', update:'POST']

    def list = {
        params.max = Math.min( params.max ? params.max.toInteger() : 10,  100)
        [ customerStatusInstanceList: CustomerStatus.list( params ), customerStatusInstanceTotal: CustomerStatus.count() ]
    }

    def show = {
        def customerStatusInstance = CustomerStatus.get( params.id )

        if(!customerStatusInstance) {
            flash.message = "CustomerStatus not found with id ${params.id}"
            redirect(action:list)
        }
        else { return [ customerStatusInstance : customerStatusInstance ] }
    }

    def delete = {
        def customerStatusInstance = CustomerStatus.get( params.id )
        if(customerStatusInstance) {
            try {
                customerStatusInstance.delete(flush:true)
                flash.message = "CustomerStatus ${params.id} deleted"
                redirect(action:list)
            }
            catch(org.springframework.dao.DataIntegrityViolationException e) {
                flash.message = "CustomerStatus ${params.id} could not be deleted"
                redirect(action:show,id:params.id)
            }
        }
        else {
            flash.message = "CustomerStatus not found with id ${params.id}"
            redirect(action:list)
        }
    }

    def edit = {
        def customerStatusInstance = CustomerStatus.get( params.id )

        if(!customerStatusInstance) {
            flash.message = "CustomerStatus not found with id ${params.id}"
            redirect(action:list)
        }
        else {
            return [ customerStatusInstance : customerStatusInstance ]
        }
    }

    def update = {
        def customerStatusInstance = CustomerStatus.get( params.id )
        if(customerStatusInstance) {
            if(params.version) {
                def version = params.version.toLong()
                if(customerStatusInstance.version > version) {
                    
                    customerStatusInstance.errors.rejectValue("version", "customerStatus.optimistic.locking.failure", "Another user has updated this CustomerStatus while you were editing.")
                    render(view:'edit',model:[customerStatusInstance:customerStatusInstance])
                    return
                }
            }
            customerStatusInstance.properties = params
            if(!customerStatusInstance.hasErrors() && customerStatusInstance.save()) {
                flash.message = "CustomerStatus ${params.id} updated"
                redirect(action:show,id:customerStatusInstance.id)
            }
            else {
                render(view:'edit',model:[customerStatusInstance:customerStatusInstance])
            }
        }
        else {
            flash.message = "CustomerStatus not found with id ${params.id}"
            redirect(action:list)
        }
    }

    def create = {
        def customerStatusInstance = new CustomerStatus()
        customerStatusInstance.properties = params
        return ['customerStatusInstance':customerStatusInstance]
    }

    def save = {
        def customerStatusInstance = new CustomerStatus(params)
        if(!customerStatusInstance.hasErrors() && customerStatusInstance.save()) {
            flash.message = "CustomerStatus ${customerStatusInstance.id} created"
            redirect(action:show,id:customerStatusInstance.id)
        }
        else {
            render(view:'create',model:[customerStatusInstance:customerStatusInstance])
        }
    }
}
