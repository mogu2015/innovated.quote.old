package com.innovated.iris.domain.enums

import org.codehaus.groovy.grails.plugins.springsecurity.Secured;

@Secured(['ROLE_SUPER', 'ROLE_ADMIN'])
class BudgetStatusController {

    def index = { redirect(action: "list", params: params) }

    // the delete, save and update actions only accept POST requests
    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def list = {
        params.max = Math.min(params.max ? params.max.toInteger() : 10,  100)
        [budgetStatusInstanceList: BudgetStatus.list(params), budgetStatusInstanceTotal: BudgetStatus.count()]
    }

    def create = {
        def budgetStatusInstance = new BudgetStatus()
        budgetStatusInstance.properties = params
        return [budgetStatusInstance: budgetStatusInstance]
    }

    def save = {
        def budgetStatusInstance = new BudgetStatus(params)
        if (!budgetStatusInstance.hasErrors() && budgetStatusInstance.save()) {
            flash.message = "budgetStatus.created"
            flash.args = [budgetStatusInstance.id]
            flash.defaultMessage = "BudgetStatus ${budgetStatusInstance.id} created"
            redirect(action: "show", id: budgetStatusInstance.id)
        }
        else {
            render(view: "create", model: [budgetStatusInstance: budgetStatusInstance])
        }
    }

    def show = {
        def budgetStatusInstance = BudgetStatus.get(params.id)
        if (!budgetStatusInstance) {
            flash.message = "budgetStatus.not.found"
            flash.args = [params.id]
            flash.defaultMessage = "BudgetStatus not found with id ${params.id}"
            redirect(action: "list")
        }
        else {
            return [budgetStatusInstance: budgetStatusInstance]
        }
    }

    def edit = {
        def budgetStatusInstance = BudgetStatus.get(params.id)
        if (!budgetStatusInstance) {
            flash.message = "budgetStatus.not.found"
            flash.args = [params.id]
            flash.defaultMessage = "BudgetStatus not found with id ${params.id}"
            redirect(action: "list")
        }
        else {
            return [budgetStatusInstance: budgetStatusInstance]
        }
    }

    def update = {
        def budgetStatusInstance = BudgetStatus.get(params.id)
        if (budgetStatusInstance) {
            if (params.version) {
                def version = params.version.toLong()
                if (budgetStatusInstance.version > version) {
                    
                    budgetStatusInstance.errors.rejectValue("version", "budgetStatus.optimistic.locking.failure", "Another user has updated this BudgetStatus while you were editing")
                    render(view: "edit", model: [budgetStatusInstance: budgetStatusInstance])
                    return
                }
            }
            budgetStatusInstance.properties = params
            if (!budgetStatusInstance.hasErrors() && budgetStatusInstance.save()) {
                flash.message = "budgetStatus.updated"
                flash.args = [params.id]
                flash.defaultMessage = "BudgetStatus ${params.id} updated"
                redirect(action: "show", id: budgetStatusInstance.id)
            }
            else {
                render(view: "edit", model: [budgetStatusInstance: budgetStatusInstance])
            }
        }
        else {
            flash.message = "budgetStatus.not.found"
            flash.args = [params.id]
            flash.defaultMessage = "BudgetStatus not found with id ${params.id}"
            redirect(action: "edit", id: params.id)
        }
    }

    def delete = {
        def budgetStatusInstance = BudgetStatus.get(params.id)
        if (budgetStatusInstance) {
            try {
                budgetStatusInstance.delete()
                flash.message = "budgetStatus.deleted"
                flash.args = [params.id]
                flash.defaultMessage = "BudgetStatus ${params.id} deleted"
                redirect(action: "list")
            }
            catch (org.springframework.dao.DataIntegrityViolationException e) {
                flash.message = "budgetStatus.not.deleted"
                flash.args = [params.id]
                flash.defaultMessage = "BudgetStatus ${params.id} could not be deleted"
                redirect(action: "show", id: params.id)
            }
        }
        else {
            flash.message = "budgetStatus.not.found"
            flash.args = [params.id]
            flash.defaultMessage = "BudgetStatus not found with id ${params.id}"
            redirect(action: "list")
        }
    }
}
