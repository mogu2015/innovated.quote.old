package com.innovated.iris.domain.enums

import org.codehaus.groovy.grails.plugins.springsecurity.Secured;

@Secured(['ROLE_SUPER', 'ROLE_ADMIN'])
class AustralianStateController {

    def index = { redirect(action: "list", params: params) }

    // the delete, save and update actions only accept POST requests
    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def list = {
        params.max = Math.min(params.max ? params.max.toInteger() : 10,  100)
        [australianStateInstanceList: AustralianState.list(params), australianStateInstanceTotal: AustralianState.count()]
    }

    def create = {
        def australianStateInstance = new AustralianState()
        australianStateInstance.properties = params
        return [australianStateInstance: australianStateInstance]
    }

    def save = {
        def australianStateInstance = new AustralianState(params)
        if (!australianStateInstance.hasErrors() && australianStateInstance.save()) {
            flash.message = "australianState.created"
            flash.args = [australianStateInstance.id]
            flash.defaultMessage = "AustralianState ${australianStateInstance.id} created"
            redirect(action: "show", id: australianStateInstance.id)
        }
        else {
            render(view: "create", model: [australianStateInstance: australianStateInstance])
        }
    }

    def show = {
        def australianStateInstance = AustralianState.get(params.id)
        if (!australianStateInstance) {
            flash.message = "australianState.not.found"
            flash.args = [params.id]
            flash.defaultMessage = "AustralianState not found with id ${params.id}"
            redirect(action: "list")
        }
        else {
            return [australianStateInstance: australianStateInstance]
        }
    }

    def edit = {
        def australianStateInstance = AustralianState.get(params.id)
        if (!australianStateInstance) {
            flash.message = "australianState.not.found"
            flash.args = [params.id]
            flash.defaultMessage = "AustralianState not found with id ${params.id}"
            redirect(action: "list")
        }
        else {
            return [australianStateInstance: australianStateInstance]
        }
    }

    def update = {
        def australianStateInstance = AustralianState.get(params.id)
        if (australianStateInstance) {
            if (params.version) {
                def version = params.version.toLong()
                if (australianStateInstance.version > version) {
                    
                    australianStateInstance.errors.rejectValue("version", "australianState.optimistic.locking.failure", "Another user has updated this AustralianState while you were editing")
                    render(view: "edit", model: [australianStateInstance: australianStateInstance])
                    return
                }
            }
            australianStateInstance.properties = params
            if (!australianStateInstance.hasErrors() && australianStateInstance.save()) {
                flash.message = "australianState.updated"
                flash.args = [params.id]
                flash.defaultMessage = "AustralianState ${params.id} updated"
                redirect(action: "show", id: australianStateInstance.id)
            }
            else {
                render(view: "edit", model: [australianStateInstance: australianStateInstance])
            }
        }
        else {
            flash.message = "australianState.not.found"
            flash.args = [params.id]
            flash.defaultMessage = "AustralianState not found with id ${params.id}"
            redirect(action: "edit", id: params.id)
        }
    }

    def delete = {
        def australianStateInstance = AustralianState.get(params.id)
        if (australianStateInstance) {
            try {
                australianStateInstance.delete()
                flash.message = "australianState.deleted"
                flash.args = [params.id]
                flash.defaultMessage = "AustralianState ${params.id} deleted"
                redirect(action: "list")
            }
            catch (org.springframework.dao.DataIntegrityViolationException e) {
                flash.message = "australianState.not.deleted"
                flash.args = [params.id]
                flash.defaultMessage = "AustralianState ${params.id} could not be deleted"
                redirect(action: "show", id: params.id)
            }
        }
        else {
            flash.message = "australianState.not.found"
            flash.args = [params.id]
            flash.defaultMessage = "AustralianState not found with id ${params.id}"
            redirect(action: "list")
        }
    }
}
