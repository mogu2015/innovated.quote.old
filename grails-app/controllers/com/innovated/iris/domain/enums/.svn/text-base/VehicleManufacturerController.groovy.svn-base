package com.innovated.iris.domain.enums

import org.codehaus.groovy.grails.plugins.springsecurity.Secured

import com.innovated.iris.domain.*

import org.springframework.web.multipart.MultipartHttpServletRequest
import org.springframework.web.multipart.commons.CommonsMultipartFile

import org.apache.commons.net.ftp.FTPClient

import jxl.*

//@Secured(['IS_AUTHENTICATED_FULLY'])
@Secured(['ROLE_SUPER', 'ROLE_ADMIN'])
class VehicleManufacturerController {

    def index = { redirect(action: "list", params: params) }

    // the delete, save and update actions only accept POST requests
    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def list = {
        params.max = Math.min(params.max ? params.max.toInteger() : 100,  100)
        [vehicleManufacturerInstanceList: VehicleManufacturer.list(params), vehicleManufacturerInstanceTotal: VehicleManufacturer.count()]
    }
    
    def ftp = {
    	new FTPClient().with {
    		connect "proj1.designcodetest.com"
    		enterLocalPassiveMode()
    		login "proj1", "proj1"
    		changeWorkingDirectory "/public"
    		fileType = FTPClient.BINARY_FILE_TYPE
    		
    		def incomingFile = new File("example_ftp_import.php")
    		incomingFile.withOutputStream { ostream -> retrieveFile "setup.php", ostream }
    		disconnect()
    	}
    	redirect(action: "list")
    }
    
    def importFile = {
    	render(view: "import", model: [:])
    }
    def saveXls = {
    	println "Importing Vehicle Makes from XLS file!"

		if(request instanceof MultipartHttpServletRequest) {
			// get our multipart  
			MultipartHttpServletRequest mpr = (MultipartHttpServletRequest)request;
			CommonsMultipartFile file = (CommonsMultipartFile) mpr.getFile("importFile");
			
			if(!file.empty) {
				// create our workbook
				Workbook workbook = Workbook.getWorkbook(file.inputStream)
				Sheet sheet = workbook.getSheet(0)
			
				def added = 0;
				def skipped = [];
			
				// skip header row, start at row 1...
				for (int r=1; r<sheet.rows; r++) {
					// get our fields
					def makeName = sheet.getCell(0, r).contents
					def v = VehicleManufacturer.findByName(makeName)
				
					if (v) {
						println "Vehicle Make already exists in IRIS: ${makeName}, skipping..."
						skipped += (r + 1)
					} else {
						v = new VehicleManufacturer(name: makeName)
						if (!v.hasErrors() && v.save()) {
							println "Vehicle Make added to IRIS: ${makeName}"
							added++
						} else {
							println "Vehicle Make skipped: ${makeName}"
							skipped += (r + 1)
						}
					}
				}
				workbook.close()
			
				// generate our flash message
				flash.message = "${added} vehicle make(s) added."
				if (skipped.size() > 0) {
					flash.message += "<br/>Rows ${skipped.join(', ')} were skipped because they were incomplete, malformed or duplicates"
				}
				redirect(action: "list")
			}
			else {
				flash.message = 'Please select a file to import!'
				redirect(action: "importFile")
			}
		}   
		else {
			flash.message = 'Invalid import (Error: 5201)'
			redirect(action: "importFile")
		}
	}

    def create = {
        def vehicleManufacturerInstance = new VehicleManufacturer()
        vehicleManufacturerInstance.properties = params
        return [vehicleManufacturerInstance: vehicleManufacturerInstance]
    }

    def save = {
        def vehicleManufacturerInstance = new VehicleManufacturer(params)
        if (!vehicleManufacturerInstance.hasErrors() && vehicleManufacturerInstance.save()) {
            flash.message = "vehicleManufacturer.created"
            flash.args = [vehicleManufacturerInstance.id]
            flash.defaultMessage = "VehicleManufacturer ${vehicleManufacturerInstance.id} created"
            redirect(action: "show", id: vehicleManufacturerInstance.id)
        }
        else {
            render(view: "create", model: [vehicleManufacturerInstance: vehicleManufacturerInstance])
        }
    }

    def show = {
        def vehicleManufacturerInstance = VehicleManufacturer.get(params.id)
        if (!vehicleManufacturerInstance) {
            flash.message = "vehicleManufacturer.not.found"
            flash.args = [params.id]
            flash.defaultMessage = "VehicleManufacturer not found with id ${params.id}"
            redirect(action: "list")
        }
        else {
            return [vehicleManufacturerInstance: vehicleManufacturerInstance]
        }
    }

    def edit = {
        def vehicleManufacturerInstance = VehicleManufacturer.get(params.id)
        if (!vehicleManufacturerInstance) {
            flash.message = "vehicleManufacturer.not.found"
            flash.args = [params.id]
            flash.defaultMessage = "VehicleManufacturer not found with id ${params.id}"
            redirect(action: "list")
        }
        else {
            return [vehicleManufacturerInstance: vehicleManufacturerInstance]
        }
    }

    def update = {
        def vehicleManufacturerInstance = VehicleManufacturer.get(params.id)
        if (vehicleManufacturerInstance) {
            if (params.version) {
                def version = params.version.toLong()
                if (vehicleManufacturerInstance.version > version) {
                    
                    vehicleManufacturerInstance.errors.rejectValue("version", "vehicleManufacturer.optimistic.locking.failure", "Another user has updated this VehicleManufacturer while you were editing")
                    render(view: "edit", model: [vehicleManufacturerInstance: vehicleManufacturerInstance])
                    return
                }
            }
            vehicleManufacturerInstance.properties = params
            if (!vehicleManufacturerInstance.hasErrors() && vehicleManufacturerInstance.save()) {
                flash.message = "vehicleManufacturer.updated"
                flash.args = [params.id]
                flash.defaultMessage = "VehicleManufacturer ${params.id} updated"
                redirect(action: "show", id: vehicleManufacturerInstance.id)
            }
            else {
                render(view: "edit", model: [vehicleManufacturerInstance: vehicleManufacturerInstance])
            }
        }
        else {
            flash.message = "vehicleManufacturer.not.found"
            flash.args = [params.id]
            flash.defaultMessage = "VehicleManufacturer not found with id ${params.id}"
            redirect(action: "edit", id: params.id)
        }
    }

    def delete = {
        def vehicleManufacturerInstance = VehicleManufacturer.get(params.id)
        if (vehicleManufacturerInstance) {
            try {
                vehicleManufacturerInstance.delete()
                flash.message = "vehicleManufacturer.deleted"
                flash.args = [params.id]
                flash.defaultMessage = "VehicleManufacturer ${params.id} deleted"
                redirect(action: "list")
            }
            catch (org.springframework.dao.DataIntegrityViolationException e) {
                flash.message = "vehicleManufacturer.not.deleted"
                flash.args = [params.id]
                flash.defaultMessage = "VehicleManufacturer ${params.id} could not be deleted"
                redirect(action: "show", id: params.id)
            }
        }
        else {
            flash.message = "vehicleManufacturer.not.found"
            flash.args = [params.id]
            flash.defaultMessage = "VehicleManufacturer not found with id ${params.id}"
            redirect(action: "list")
        }
    }
}
