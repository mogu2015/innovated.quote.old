package com.innovated.iris.domain.enums

import grails.converters.JSON
import jxl.*
import org.codehaus.groovy.grails.plugins.springsecurity.Secured
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import com.innovated.iris.domain.*

//@Secured(['IS_AUTHENTICATED_FULLY'])
@Secured(['ROLE_SUPER', 'ROLE_ADMIN'])
class VehicleModelController {

    def index = { redirect(action: "list", params: params) }

    // the delete, save and update actions only accept POST requests
    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def list = {
        params.max = Math.min(params.max ? params.max.toInteger() : 10, 100)
        [vehicleModelInstanceList: VehicleModel.list(params), vehicleModelInstanceTotal: VehicleModel.count()]
    }

    def importFile = {
        render(view: "import", model: [:])
    }

    @Secured(['IS_AUTHENTICATED_ANONYMOUSLY'])
    def getModelAjax = {
        def data = []

        if(params.id && !"null".equals(params.id)){
            def models = VehicleModel.findAllByMake(VehicleManufacturer.get(params.id), [sort: "name", order: "asc"])

            models.each {
                data << [
                        id: it.id, name: it.name
                ]
            }
        }
        render data as JSON

    }
    @Secured(['IS_AUTHENTICATED_FULLY'])
    def getVariantByModelAjax = {
        def variant = []
        def models = []
        models = MakeModel.findAll("from MakeModel as m where m.model.id=:id order by m.description", [id: params.id?.toLong()])

        models.each {
            if (!variant.contains(it.variant) && it.variant && it.retailPrice != 10000) {
                variant << it.variant
            }
            variant.sort()
        }
        render variant as JSON
    }
    def saveXls = {
        println "Importing Vehicle Model from XLS file!"

        if (request instanceof MultipartHttpServletRequest) {
            // get our multipart
            MultipartHttpServletRequest mpr = (MultipartHttpServletRequest) request;
            CommonsMultipartFile file = (CommonsMultipartFile) mpr.getFile("importFile");

            if (!file.empty) {
                // create our workbook
                Workbook workbook = Workbook.getWorkbook(file.inputStream)
                Sheet sheet = workbook.getSheet(0)

                def added = 0;
                def skipped = [];

                // skip header row, start at row 1...
                for (int r = 1; r < sheet.rows; r++) {
                    // get our fields
                    def makeName = sheet.getCell(0, r).contents
                    def modelName = sheet.getCell(1, r).contents
                    def v = VehicleModel.find("from VehicleModel as v where v.name=:model and v.make.name=:make", [model: modelName, make: makeName])

                    if (v) {
                        println "Vehicle Model already exists in IRIS: ${modelName}, skipping..."
                        skipped += (r + 1)
                    } else {
                        def make = VehicleManufacturer.findByName(makeName)
                        v = new VehicleModel(make: make, name: modelName)
                        make.addToModels(v)
                        if (!v.hasErrors() && make.save()) {
                            println "Vehicle Model added to IRIS: ${modelName}"
                            added++
                        } else {
                            println "Vehicle Model skipped: ${modelName}"
                            skipped += (r + 1)
                        }
                    }
                }
                workbook.close()

                // generate our flash message
                flash.message = "${added} vehicle models(s) added."
                if (skipped.size() > 0) {
                    flash.message += "<br/>Rows ${skipped.join(', ')} were skipped because they were incomplete, malformed or duplicates"
                }
                redirect(action: "list")
            } else {
                flash.message = 'Please select a file to import!'
                redirect(action: "importFile")
            }
        } else {
            flash.message = 'Invalid import (Error: 5201)'
            redirect(action: "importFile")
        }
    }

    def create = {
        def vehicleModelInstance = new VehicleModel()
        vehicleModelInstance.properties = params
        return [vehicleModelInstance: vehicleModelInstance]
    }

    def save = {
        def vehicleModelInstance = new VehicleModel(params)
        def make = VehicleManufacturer.get(params.make.id)
        if (make != null) {
            make.addToModels(vehicleModelInstance)
            if (!vehicleModelInstance.hasErrors() && make.save(flush: true)) {
                flash.message = "vehicleModel.created"
                flash.args = [vehicleModelInstance.id]
                flash.defaultMessage = "VehicleModel ${vehicleModelInstance.id} created"
                redirect(action: "show", id: vehicleModelInstance.id)
            } else {
                render(view: "create", model: [vehicleModelInstance: vehicleModelInstance])
            }
        } else {
            if (!vehicleModelInstance.hasErrors() && vehicleModelInstance.save()) {
                flash.message = "vehicleModel.created"
                flash.args = [vehicleModelInstance.id]
                flash.defaultMessage = "VehicleModel ${vehicleModelInstance.id} created"
                redirect(action: "show", id: vehicleModelInstance.id)
            } else {
                render(view: "create", model: [vehicleModelInstance: vehicleModelInstance])
            }
        }
    }

    def show = {
        def vehicleModelInstance = VehicleModel.get(params.id)
        if (!vehicleModelInstance) {
            flash.message = "vehicleModel.not.found"
            flash.args = [params.id]
            flash.defaultMessage = "VehicleModel not found with id ${params.id}"
            redirect(action: "list")
        } else {
            return [vehicleModelInstance: vehicleModelInstance]
        }
    }

    def edit = {
        def vehicleModelInstance = VehicleModel.get(params.id)
        if (!vehicleModelInstance) {
            flash.message = "vehicleModel.not.found"
            flash.args = [params.id]
            flash.defaultMessage = "VehicleModel not found with id ${params.id}"
            redirect(action: "list")
        } else {
            return [vehicleModelInstance: vehicleModelInstance]
        }
    }

    def update = {
        def vehicleModelInstance = VehicleModel.get(params.id)
        if (vehicleModelInstance) {
            if (params.version) {
                def version = params.version.toLong()
                if (vehicleModelInstance.version > version) {

                    vehicleModelInstance.errors.rejectValue("version", "vehicleModel.optimistic.locking.failure", "Another user has updated this VehicleModel while you were editing")
                    render(view: "edit", model: [vehicleModelInstance: vehicleModelInstance])
                    return
                }
            }
            vehicleModelInstance.properties = params

            def make = VehicleManufacturer.get(params.make.id)
            if (make != null) {
                make.addToModels(vehicleModelInstance)
                if (!vehicleModelInstance.hasErrors() && make.save(flush: true)) {
                    flash.message = "vehicleModel.updated"
                    flash.args = [params.id]
                    flash.defaultMessage = "VehicleModel ${params.id} updated"
                    redirect(action: "show", id: vehicleModelInstance.id)
                } else {
                    render(view: "edit", model: [vehicleModelInstance: vehicleModelInstance])
                }
            } else {
                if (!vehicleModelInstance.hasErrors() && vehicleModelInstance.save()) {
                    flash.message = "vehicleModel.updated"
                    flash.args = [params.id]
                    flash.defaultMessage = "VehicleModel ${params.id} updated"
                    redirect(action: "show", id: vehicleModelInstance.id)
                } else {
                    render(view: "edit", model: [vehicleModelInstance: vehicleModelInstance])
                }
            }
        } else {
            flash.message = "vehicleModel.not.found"
            flash.args = [params.id]
            flash.defaultMessage = "VehicleModel not found with id ${params.id}"
            redirect(action: "edit", id: params.id)
        }
    }

    def delete = {
        def vehicleModelInstance = VehicleModel.get(params.id)
        if (vehicleModelInstance) {
            try {
                vehicleModelInstance.delete()
                flash.message = "vehicleModel.deleted"
                flash.args = [params.id]
                flash.defaultMessage = "VehicleModel ${params.id} deleted"
                redirect(action: "list")
            }
            catch (org.springframework.dao.DataIntegrityViolationException e) {
                flash.message = "vehicleModel.not.deleted"
                flash.args = [params.id]
                flash.defaultMessage = "VehicleModel ${params.id} could not be deleted"
                redirect(action: "show", id: params.id)
            }
        } else {
            flash.message = "vehicleModel.not.found"
            flash.args = [params.id]
            flash.defaultMessage = "VehicleModel not found with id ${params.id}"
            redirect(action: "list")
        }
    }
}
