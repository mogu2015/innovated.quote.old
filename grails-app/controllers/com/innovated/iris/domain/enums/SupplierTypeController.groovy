package com.innovated.iris.domain.enums

import org.codehaus.groovy.grails.plugins.springsecurity.Secured;

@Secured(['ROLE_SUPER', 'ROLE_ADMIN'])
class SupplierTypeController {

    def index = { redirect(action: "list", params: params) }

    // the delete, save and update actions only accept POST requests
    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def list = {
        params.max = Math.min(params.max ? params.max.toInteger() : 10,  100)
        [supplierTypeInstanceList: SupplierType.list(params), supplierTypeInstanceTotal: SupplierType.count()]
    }

    def create = {
        def supplierTypeInstance = new SupplierType()
        supplierTypeInstance.properties = params
        return [supplierTypeInstance: supplierTypeInstance]
    }

    def save = {
        def supplierTypeInstance = new SupplierType(params)
        if (!supplierTypeInstance.hasErrors() && supplierTypeInstance.save()) {
            flash.message = "supplierType.created"
            flash.args = [supplierTypeInstance.id]
            flash.defaultMessage = "SupplierType ${supplierTypeInstance.id} created"
            redirect(action: "show", id: supplierTypeInstance.id)
        }
        else {
            render(view: "create", model: [supplierTypeInstance: supplierTypeInstance])
        }
    }

    def show = {
        def supplierTypeInstance = SupplierType.get(params.id)
        if (!supplierTypeInstance) {
            flash.message = "supplierType.not.found"
            flash.args = [params.id]
            flash.defaultMessage = "SupplierType not found with id ${params.id}"
            redirect(action: "list")
        }
        else {
            return [supplierTypeInstance: supplierTypeInstance]
        }
    }

    def edit = {
        def supplierTypeInstance = SupplierType.get(params.id)
        if (!supplierTypeInstance) {
            flash.message = "supplierType.not.found"
            flash.args = [params.id]
            flash.defaultMessage = "SupplierType not found with id ${params.id}"
            redirect(action: "list")
        }
        else {
            return [supplierTypeInstance: supplierTypeInstance]
        }
    }

    def update = {
        def supplierTypeInstance = SupplierType.get(params.id)
        if (supplierTypeInstance) {
            if (params.version) {
                def version = params.version.toLong()
                if (supplierTypeInstance.version > version) {
                    
                    supplierTypeInstance.errors.rejectValue("version", "supplierType.optimistic.locking.failure", "Another user has updated this SupplierType while you were editing")
                    render(view: "edit", model: [supplierTypeInstance: supplierTypeInstance])
                    return
                }
            }
            supplierTypeInstance.properties = params
            if (!supplierTypeInstance.hasErrors() && supplierTypeInstance.save()) {
                flash.message = "supplierType.updated"
                flash.args = [params.id]
                flash.defaultMessage = "SupplierType ${params.id} updated"
                redirect(action: "show", id: supplierTypeInstance.id)
            }
            else {
                render(view: "edit", model: [supplierTypeInstance: supplierTypeInstance])
            }
        }
        else {
            flash.message = "supplierType.not.found"
            flash.args = [params.id]
            flash.defaultMessage = "SupplierType not found with id ${params.id}"
            redirect(action: "edit", id: params.id)
        }
    }

    def delete = {
        def supplierTypeInstance = SupplierType.get(params.id)
        if (supplierTypeInstance) {
            try {
                supplierTypeInstance.delete()
                flash.message = "supplierType.deleted"
                flash.args = [params.id]
                flash.defaultMessage = "SupplierType ${params.id} deleted"
                redirect(action: "list")
            }
            catch (org.springframework.dao.DataIntegrityViolationException e) {
                flash.message = "supplierType.not.deleted"
                flash.args = [params.id]
                flash.defaultMessage = "SupplierType ${params.id} could not be deleted"
                redirect(action: "show", id: params.id)
            }
        }
        else {
            flash.message = "supplierType.not.found"
            flash.args = [params.id]
            flash.defaultMessage = "SupplierType not found with id ${params.id}"
            redirect(action: "list")
        }
    }
}
