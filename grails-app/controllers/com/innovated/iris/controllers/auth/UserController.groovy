package com.innovated.iris.controllers.auth

import com.grailsrocks.emailconfirmation.PendingEmailConfirmation;

import com.innovated.iris.domain.RegisterUserCommand;
import com.innovated.iris.domain.auth.User
import com.innovated.iris.domain.auth.Role
import com.innovated.iris.domain.*

import org.codehaus.groovy.grails.plugins.springsecurity.Secured
import org.jfree.util.Log;

/**
 * User controller.
 */
@Secured(['ROLE_SUPER', 'ROLE_ADMIN'])
class UserController {

    def authenticateService
    def emailConfirmationService
    def userService
    def jmsService
    def quoteService

    // the delete, save and update actions only accept POST requests
    static Map allowedMethods = [delete: 'POST', save: 'POST', update: 'POST']

    def index = {
        redirect action: list, params: params
    }

    def list = {
        params.max = Math.min(params.max ? params.max.toInteger() : 10, 100)
        [userInstanceList: User.list(params), userInstanceTotal: User.count()]
    }

    @Secured(['IS_AUTHENTICATED_FULLY'])
    def changePassword = {
        def person = User.get(params.id)
        if (!person) {
            flash.message = "User could not be found"
            redirect controller: tools
            return
        }
        [cmd: new ChangePasswordCommand(id: person?.id)]
    }

    @Secured(['IS_AUTHENTICATED_FULLY'])
    def updatePassword = { ChangePasswordCommand cmd ->
        if (!cmd.hasErrors()) {
            def person = User.get(cmd.id)
            if (!person) {
                println "cant find person"
                flash.message = "User could not be found"
                redirect controller: tools
                return
            } else {
                def isAdmin = authenticateService.ifAnyGranted('ROLE_SUPER,ROLE_ADMIN')
                def isOwner = false
                def principal = authenticateService.principal()
                def user = User.findByUsername(principal.username)
                if (user && (user?.customer == person?.customer)) {
                    isOwner = true
                }

                // check that the logged in user is ALLOWED to change the password for this user ID!!
                if (isAdmin || isOwner) {
                    person.passwd = authenticateService.encodePassword(cmd.passwordNew)
                    if (person.save(flush: true)) {
                        println "saved OK"
                        flash.message = "Password changed for $person.username"
                        //redirect(controller:"user", action:"show", id:person?.id)
                        if (isAdmin) {
                            redirect action: show, id: person.id
                        } else {
                            redirect controller: user, action: home
                        }
                    } else {
                        println "saved failed"
                        cmd.errors = person.errors
                        flash.message = "Password update failed"
                        render(view: 'changePassword', model: [cmd: cmd])
                    }
                } else {
                    println "cant change!"
                    flash.message = "You don't have sufficient access privileges to change this users' password!"
                    redirect controller: tools
                    return
                }
            }
        } else {
            println "cms has errors"
            render(view: 'changePassword', model: [cmd: cmd])
        }
    }

    @Secured(['IS_AUTHENTICATED_ANONYMOUSLY'])
    def register = {
        [cmd: new RegisterUserCommand()]
    }

    /**
     * Try and save the user (& entity/customer instances).
     * The RuntimeException will cause a transaction rollback if something fails.
     */
    @Secured(['IS_AUTHENTICATED_ANONYMOUSLY'])
    def registerUser = { RegisterUserCommand cmd ->
        if (!cmd.hasErrors()) {
            // 1st, validate the employer registration code!!!
//			def employer = Company.findByEmployerCode(cmd.employerCode)
//			if (!employer) {
//				cmd.errors.rejectValue('employerCode', 'registerUserCommand.employerCode.invalid')
//				render(view:'register', model:[cmd: cmd])
//				return
//			}
//			else {
            // does a user with this email address already exist?
            def user = User.findByUsername(cmd.userName)
            if (user) {
                cmd.errors.rejectValue('userName', 'registerUserCommand.userName.alreadyexists')
                render(view: 'register', model: [cmd: cmd])
                return
            } else {
                // create user account
                try {
                    cmd = userService.registerNewUser(cmd, null)
                    // send email to user for confirmation,...
                    def alreadyPending = PendingEmailConfirmation.findByEmailAddress(cmd.email)
                    if (!alreadyPending) {
                        def msg = [
                                to   : cmd.email, subject: "Online Access Confirmation", token: cmd.userName,
                                model: [
                                        from     : "ClientServices@inNovated.com.au", view: '/templates/email/confirmUserRego',
                                        firstName: cmd.firstName, userName: cmd.userName, password: cmd.password
                                ]
                        ]
                        jmsService.send([service: 'notification', method: 'confirmEmail'], msg)
                    }
                    render(view: 'confirm', model: [cmd: cmd])
                }
                catch (Exception e) {
                    log.error("Couldn't create user account through UserService.registerNewUser", e)
                    render(view: 'register', model: [cmd: cmd])
                }
            }
//			}
        } else {
            render(view: 'register', model: [cmd: cmd])
        }
    }

    @Secured(['IS_AUTHENTICATED_FULLY'])
    def welcome = {
        //redirect(action:"home")
        redirect(controller: "tools", action: "quote")
    }

    @Secured(['IS_AUTHENTICATED_FULLY'])
    def home = {
        def isEmployer = authenticateService.ifAllGranted('ROLE_EMPLOYER')
        if (isEmployer) {
            // list current quotes for the "logged-in" user (or admin user)
            List ql = quoteService.getQuoteList()
            def principal = authenticateService.principal()
            def user = User.findByUsername(principal.username)
            def employer = null
            if (user && user?.customer) {
                employer = user.customer?.entity
            }
            [quoteList: ql, employer: employer]
        } else {
            redirect(controller: "tools", action: "quote")
        }
    }


    @Secured(['IS_AUTHENTICATED_FULLY'])
    def settings = {
        render "user settings page here"
    }

    @Secured(['IS_AUTHENTICATED_FULLY'])
    def mydetails = {

    }

    @Secured(['IS_AUTHENTICATED_ANONYMOUSLY'])
    def confirmFailed = {
        render view: 'confirmFailed'
    }


    def show = {
        def person = User.get(params.id)
        if (!person) {
            flash.message = "User not found with id $params.id"
            redirect action: list
            return
        }
        /*List roleNames = []
        for (role in person.authorities) {
            roleNames << role.authority
        }
        roleNames.sort { n1, n2 ->
            n1 <=> n2
        }*/
        [userInstance: person, roles: person.authorities]
    }

    /**
     * Person delete action. Before removing an existing person,
     * he should be removed from those authorities which he is involved.
     */
    def delete = {

        def person = User.get(params.id)
        if (person) {
            def authPrincipal = authenticateService.principal()
            //avoid self-delete if the logged-in user is an admin
            if (!(authPrincipal instanceof String) && authPrincipal.username == person.username) {
                flash.message = "You can not delete yourself, please login as another admin and try again"
            } else {
                //first, delete this person from People_Authorities table.
                Role.findAll().each { it.removeFromPeople(person) }
                person.delete()
                flash.message = "User $params.id deleted."
            }
        } else {
            flash.message = "User not found with id $params.id"
        }

        redirect action: list
    }

    def edit = {

        def person = User.get(params.id)
        if (!person) {
            flash.message = "User not found with id $params.id"
            redirect action: list
            return
        }

        return buildPersonModel(person)
    }

    /**
     * Person update action.
     */
    def update = {

        def person = User.get(params.id)
        if (!person) {
            flash.message = "User not found with id $params.id"
            redirect action: edit, id: params.id
            return
        }

        long version = params.version.toLong()
        if (person.version > version) {
            person.errors.rejectValue 'version', "person.optimistic.locking.failure",
                    "Another user has updated this User while you were editing."
            render view: 'edit', model: buildPersonModel(person)
            return
        }

        def oldPassword = person.passwd
        person.properties = params
        if (!params.passwd.equals(oldPassword)) {
            person.passwd = authenticateService.encodePassword(params.passwd)
        }
        if (person.save()) {
            Role.findAll().each { it.removeFromPeople(person) }
            addRoles(person)
            redirect action: show, id: person.id
        } else {
            render view: 'edit', model: buildPersonModel(person)
        }
    }

    def create = {
        [userInstance: new User(params), authorityList: Role.list()]
    }

    /**
     * Person save action.
     */
    def save = {

        def person = new User()
        person.properties = params
        person.passwd = authenticateService.encodePassword(params.passwd)
        if (person.save()) {
            addRoles(person)
            redirect action: show, id: person.id
        } else {
            render view: 'create', model: [authorityList: Role.list(), userInstance: person]
        }
    }

    private void addRoles(person) {
        for (String key in params.keySet()) {
            if (key.contains('ROLE') && 'on' == params.get(key)) {
                Role.findByAuthority(key).addToPeople(person)
            }
        }
    }

    private Map buildPersonModel(person) {

        List roles = Role.list()
        roles.sort { r1, r2 ->
            r1.authority <=> r2.authority
        }
        Set userRoleNames = []
        for (role in person.authorities) {
            userRoleNames << role.authority
        }
        LinkedHashMap<Role, Boolean> roleMap = [:]
        for (role in roles) {
            roleMap[(role)] = userRoleNames.contains(role.authority)
        }

        return [userInstance: person, roleMap: roleMap]
    }
}
