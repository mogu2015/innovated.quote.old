package com.innovated.iris.controllers.auth

import org.codehaus.groovy.grails.plugins.springsecurity.Secured

/**
 * Logout Controller (Example).
 */
@Secured(['IS_AUTHENTICATED_ANONYMOUSLY'])
class LogoutController {

	/**
	 * Index action. Redirects to the Spring security logout uri.
	 */
	def index = {
		// TODO  put any pre-logout code here
		redirect(uri: '/j_spring_security_logout')
	}
}
