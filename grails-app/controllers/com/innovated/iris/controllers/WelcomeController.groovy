package com.innovated.iris.controllers


import org.codehaus.groovy.grails.plugins.springsecurity.Secured

/**
 * Login Controller (Example).
 */
@Secured(['ROLE_SUPER', 'ROLE_ADMIN'])
class WelcomeController {

	def index = {
		//
	}
}
