package com.innovated.iris.controllers

import com.innovated.iris.domain.Company
import com.innovated.iris.domain.Customer
import com.innovated.iris.domain.MakeModel
import com.innovated.iris.domain.Quote
import com.innovated.iris.domain.auth.User
import com.innovated.iris.domain.enums.VehicleManufacturer
import com.innovated.iris.domain.enums.VehicleModel
import com.innovated.iris.enums.QuoteStatus
import com.innovated.iris.enums.application.FinanceApplicationAssetType
import com.innovated.iris.server.fbt.TaxableValueType
import com.innovated.iris.util.CurrencyUtils
import com.innovated.iris.util.PayrollFrequency
import grails.converters.JSON
import grails.util.GrailsNameUtils
import org.codehaus.groovy.grails.plugins.springsecurity.Secured
import org.grails.comments.CommentLink

/**
 * Client Tools Controller
 */
//@Secured(['IS_AUTHENTICATED_FULLY'])
@Secured(['ROLE_SUPER', 'ROLE_ADMIN', 'ROLE_USER'])
class ToolsController {
    def jmsService
    def quoteService
    def grailsApplication
    def sessionRegistry
    def authenticateService
    def userService

    private def enableUsedCarQuotes() {
        return authenticateService.ifAnyGranted('ROLE_SUPER, ROLE_ADMIN')
    }

    def index = {
        //
    }

    @Secured(['ROLE_SUPER', 'ROLE_ADMIN'])
    def testme = {

        // test JMS setup
        def dest = [service: 'notification', method: 'email']
        def msg = [
                to  : "tcpaki@gmail.com", from: grailsApplication.config.grails.mail.from, subject: "JMS email test",
                view: "/templates/email/test", model: [name: "tamati paki"]
        ]
        jmsService.send(dest, msg)
        //jmsService.send([service:'notification', method:'sms'], [title:"sms", msg:"my sms message"])
        render "jms tests"

        /*new SessionRegistryImpl().getAllPrincipals()
        new SessionRegistryImpl().getAllSessions(arg0, arg1)
        new SessionRegistryImpl().getSessionInformation(sessionId)*/

        /*def users = sessionRegistry.getAllPrincipals()	// is this a string[] ??
        users.toList().each {
            println "user session for: ${it}"
            def sessions = sessionRegistry.getAllSessions(it, true)
            sessions.each { s ->
                println "       session: lastrequest=${s.lastRequest}, expired=${s.isExpired()}, principal=${s.principal}, sessionId=${s.sessionId}"
            }
        }
        println "==="
        render "tracking user sessions"*/

    }

    def quote = {
        // list current quotes for the "logged-in" user (or admin user)
        List ql = quoteService.getQuoteList()
        render(view: "novatedquote", model: [quoteList: ql])
    }

    @Secured(['IS_AUTHENTICATED_FULLY'])
    def viewquote = {
        def q = quoteService.getQuote(params)

        if (q) {
            render(view: "viewquote", model: buildViewModel(q))
        } else {
            def aqn = formatNumber(number: params.id?.toLong(), format: message(code: 'quote.number.mask'))
            flash.title = "Quote not accessible!"
            flash.message = "The Quote (<b>${aqn}</b>) you've selected to view either could not be found, or you don't have sufficient privileges to access it!"
            flash.image = "${resource(dir: 'images', file: 'cross48.png')}"
            redirect(action: quote)
        }
    }

    private def buildViewModel(q) {
        def breakdown = quoteService.retrieveBreakdown(q)
        def totalList = []
        def subTotalAnnual = 0.0 + (breakdown.sum { it.annually } ?: 0.0)
        def subTotalMonth = 0.0 + (breakdown.sum { it.monthly } ?: 0.0)
        def subTotalFnight = 0.0 + (breakdown.sum { it.fortnightly } ?: 0.0)
        def subTotalWeek = 0.0 + (breakdown.sum { it.weekly } ?: 0.0)
        def subTotalBiMonth = 0.0 + (breakdown.sum { it.bimonthly } ?: 0.0)

        // work out totals & GST and add to the list!
        totalList.add([
                category   : message(code: "quote.breakdown.table.label.subTotal", default: "Sub-Total"),
                annually   : subTotalAnnual,
                monthly    : subTotalMonth,
                fortnightly: subTotalFnight,
                weekly     : subTotalWeek,
                bimonthly  : subTotalBiMonth,
                supplier   : ""
        ])
        totalList.add([
                category   : message(code: "quote.breakdown.table.label.gst", default: "GST"),
                annually   : CurrencyUtils.roundUpNearestCent(subTotalAnnual / 10),
                monthly    : CurrencyUtils.roundUpNearestCent(subTotalMonth / 10),
                fortnightly: CurrencyUtils.roundUpNearestCent(subTotalFnight / 10),
                weekly     : CurrencyUtils.roundUpNearestCent(subTotalWeek / 10),
                bimonthly  : CurrencyUtils.roundUpNearestCent(subTotalBiMonth / 10),
                supplier   : ""
        ])
        totalList.add([
                category   : message(code: "quote.breakdown.table.label.grandTotal", default: "Grand Total"),
                annually   : subTotalAnnual + CurrencyUtils.roundUpNearestCent(subTotalAnnual / 10),
                monthly    : subTotalMonth + CurrencyUtils.roundUpNearestCent(subTotalMonth / 10),
                fortnightly: subTotalFnight + CurrencyUtils.roundUpNearestCent(subTotalFnight / 10),
                weekly     : subTotalWeek + CurrencyUtils.roundUpNearestCent(subTotalWeek / 10),
                bimonthly  : subTotalWeek + CurrencyUtils.roundUpNearestCent(subTotalBiMonth / 10),
                supplier   : ""
        ])

        def emptyFinAppl = [/*applicantTitle:"Mr", applicantFirstName:"Tamati", applicantMiddleName:"Charles", applicantLastName:"Paki"*/]

        // get comments for this customer
        def comments = CommentLink.withCriteria {
            projections { property "comment" }
            eq "commentRef", q.id
            eq "type", GrailsNameUtils.getPropertyName(Quote.class)
        }

        def persistentList = q.comparisons
//        if(persistentList.size() > 0){
//            persistentList.eachWithIndex {item, index->
//                if(!item.isBest && !"Not Packaged".equals(item.name)){
//                    persistentList.remove(index)
//                }
//            }
//        }

        return [
                quoteInstance      : q, quoteBreakdown: breakdown, quoteBreakdownTotals: totalList,
                comparisonItemTypes: quoteService.compactComparisonTypes(q),
                emptyFinAppl       : emptyFinAppl, commentList: comments.sort { it.dateCreated }.reverse()
        ]
    }

    def showTab = {
        def tmplPath = "/templates/ui/quote/tabs/"
        def tabNames = ["checklist", "details", "finance", "insurance", "fuel", "other", "docs", "vehicle"]
        def tabGsp = [:]
        tabNames.each { tabGsp[it] = tmplPath + it }

        // set the default tab is none is passed in (or it's not in the list!)
        def tab = (params.tab && tabNames.contains(params.tab)) ? params.tab : "details"

        def quoteInstance = quoteService.getQuote(params)
        if (!quoteInstance) {
            render message(code: "quote.view.error", defaultMessage: "Couldn't load content for tab: {0}", args: [tab])
        } else {
            if (params.prevParams) {
                quoteInstance.properties = params.prevParams
            }
            // pick the right Partial GSP template to render the response
            def tmplStr = tabGsp[tab]
            if (tmplStr) {
                render(template: tmplStr, model: buildViewModel(quoteInstance))
            } else {
                render message(code: "quote.view.error", defaultMessage: "Couldn't load content for tab: {0}", args: [tab])
            }
        }
    }

    def getFinApplAnlItem = {
        def resp = [success: false, html: ""]
        def tmplPath = "/templates/ui/finance/anlitems/"
        def typeNames = ["asset", "liability", "income", "expense"]
        def gsps = [:]
        typeNames.each { gsps[it] = tmplPath + it }

        if (params.type && typeNames.contains(params.type)) {

            //

            def item = [type: FinanceApplicationAssetType.CASH_DEPOSIT, name: "", amount: 0.0]
            def vm = [id: params.id, item: item]
            resp.success = true
            resp.html = g.render(template: gsps[params.type], model: vm)
        }
        render resp as JSON
    }

    def createquote = {
        def q = quoteService.newQuote()
        def addresses = []
        def mobiles = []
        def emails = []
        def jobs = []
        def employers = []
        def defaultPayFreq
        def clients = Customer.withCriteria { entity { like('type', '%Individual%') } }

        if (q.customer) {
            addresses = q.customer.entity.addresses.findAll { it.addressType.name == "Street Address" }
            mobiles = q.customer.entity.phones.findAll { it.phoneType.name == "Mobile" }
            emails = q.customer.entity.emails.findAll { it.defaultType == "To" }
//			jobs = Employment.findAll("from Employment as e where e.employee=:emp order by e.startedEmployment desc", [emp:q.customer.entity])
            employers = userService.getEmployersByCustomerEmail(q.customer)

//            employers.each {employer->
//                def employment = Employment.findAllByEmployer(employer)
//                if(employment){
//                    jobs << employment
//                }
//            }

            if (employers.size() == 1) {
                q.company = employers.get(0)
                defaultPayFreq = q.company.defaultPayFreq
            }

            if (addresses.size() > 0) {
                q.address = addresses.get(0)
            }
            if (mobiles.size() > 0) {
                q.mobile = mobiles.get(0)
            }
            if (emails.size() > 0) {
                q.email = emails.get(0)
            }
//			if (jobs.size() > 0) {
//				def job = jobs.get(0)
//				q.employment = job
//				if (job.grossSalary) q.grossAnnualSalary = job.grossSalary
//			}
        }

        def yrNow = new Date().getAt(Calendar.YEAR)
        def usedYears = yrNow..(yrNow - 10)
        def makes = VehicleManufacturer.list(sort: "name", order: "asc")
        [quoteInstance: q, yearRange: (yrNow - 2)..(yrNow + 2),
         addresses: addresses, mobiles: mobiles,
         emails: emails, clients: clients,
         enableUsed: enableUsedCarQuotes(),
         usedYears: usedYears,
         currentModelYears: yrNow..(yrNow - 1),
         makes: makes, models: [],
         jobs: jobs,
         employers: employers,selectPayFreq: defaultPayFreq?.code
        ]
    }

    def savequote = {
        def custId = -1L
        def employers = []
        try {
            custId = params?.customer?.id?.toLong()
        } catch (Exception e) {
        }
        def customer = Customer.get(custId)

        def newVehicle = true
        params.vehicleNew = true
//        params.vehicleYear = params.vehicleYearNew
        if (params.newUsed && (params.newUsed.toInteger() == 0)) {
            newVehicle = false
            params.vehicleNew = false
//            params.vehicleYear = params.vehicleYearUsed
        }

        params.priceObtained = params.priceObtained.toBoolean()

        def salaryPeriod = params.salaryPeriod.toInteger()
        def grossSalary = params.grossAnnualSalary.replaceAll(",","").toDouble()
        params.grossAnnualSalary = salaryPeriod * grossSalary

        def q = new Quote(params)
        q.validate()    // validate FIRST, then check for vehicle selection errors...

        def mm = MakeModel.get(params.vehicle.id)
        if (mm) {
            def rrp = mm.retailPrice ? mm.retailPrice : 0.0
            params.listPrice = params.listPrice.replaceAll(",","")
            if(!params.listPrice || params.listPrice.toDouble() < 5000.0 || params.listPrice.toDouble() > rrp + 5000.0){
                q.errors.rejectValue('listPrice', 'quote.vehicle.rrp.invalid', [rrp + 5000].toArray(), 'The <b>vehicle RRP</b> you entered does not fall within the valid range from <b>5000</b> to <b>{0}</b>')
            }
        }else {
            q.errors.rejectValue('vehicle', 'quote.vehicle.selection.invalid', 'A vehicle must be selected!')
        }
        // pay freq must be selected
        if (!params.selectPayFreq || "null".equals(params.selectPayFreq)) {
            q.errors.rejectValue('defaultPayFreq', 'quote.selectPayFreq.selection.invalid', 'A valid payroll frequency must be selected!')
        }else{
            q.defaultPayFreq = PayrollFrequency.get(params?.selectPayFreq?.toInteger())
        }


        def employerEnable = true
        if ("YES".equals(params.employerEnable)) {
            // a employer must be selected
            if (!params.company.id || "null".equals(params.company.id)) {
                q.errors.rejectValue('company', 'quote.company.selection.invalid', 'A valid employer must be entered / selected!')
            }
        } else {
            employerEnable = false
            // a employer type must be selected
            if (!params.employerType || "null".equals(params.employerType)) {
                q.errors.rejectValue('company', 'quote.employerType.selection.invalid', 'A valid employer type must be selected!')
            }

            def employer = Company.findByEmployerCode(params.employerType)
            if(employer){
                params.put("company.id", employer.id)
                q.company = employer
            }

        }
        //}

        // setup for re-edit if needed
        def addresses = []
        def mobiles = []
        def emails = []
        def jobs = []
        def clients = Customer.withCriteria { entity { like('type', '%Individual%') } }

        if (customer) {
            addresses = customer?.entity?.addresses?.findAll { it.addressType.name == "Street Address" }
            mobiles = customer?.entity?.phones?.findAll { it.phoneType.name == "Mobile" }
            emails = customer?.entity?.emails?.findAll { it.defaultType == "To" }
//            jobs = Employment.findAll("from Employment as e where e.employee=:emp order by e.startedEmployment desc", [emp: customer?.entity])
        }

        def yrNow = new Date().getAt(Calendar.YEAR)
        def usedYears = yrNow..(yrNow - 5)
        def makes = VehicleManufacturer.list(sort: "name", order: "asc")
        def selectedMake = VehicleManufacturer.get(params?.nu.newMake)
        def models = selectedMake ? VehicleModel.findAllByMake(selectedMake) : []
        def selectedModel = VehicleModel.get(params?.nu.newModel)

        def selectedVehicles = selectedModel ? MakeModel.findAllByModel(selectedModel) : []
        def variants = []
        selectedVehicles.each {
            if (!variants.contains(it.variant) && it.variant) {
                variants << it.variant
            }
            variants.sort()
        }
        if (q.hasErrors()) {
            render(view: 'createquote', model: [
                    quoteInstance: q, yearRange: (yrNow - 2)..(yrNow + 2), addresses: addresses, mobiles: mobiles,
                    emails: emails, makes: makes,models: models,variants: variants,
                    jobs         : [], clients: clients, enableUsed: false, usedYears: usedYears,
                    nu: params.nu, employers: employers, newVehicle: newVehicle, employerEnable: employerEnable,
                    employerType: params.employerType ? params.employerType : "",
                    selectPayFreq: params.selectPayFreq ? params.selectPayFreq : "",
                    priceObtained: params.priceObtained
            ])
        } else {
            try {
                //preSaveCheck(q)
                q = quoteService.save(q, params)
                if(!q.hasErrors()){
                    q.save(flush: true, failOnError: true)
                    def existQuotes = Quote.findAllByCustomer(customer)
                    if((existQuotes.size() - 1) <= 0){
                        def msg = quoteService.generateCustomerEmailMsg("Welcome to inNovated Leasing","createdQuoteNotifyCustomer",q)
                        jmsService.send([service: 'notification', method: 'email'], msg)
                    }
                }
                flash.message = "quote.created"
                flash.args = [formatNumber(number: q.id, format: message(code: 'quote.number.mask'))]
                flash.defaultMessage = "Quote ${q.id} created"
                flash.classes = "flashOk"
                redirect(action: "viewquote", id: q.id)
            }
            catch (grails.validation.ValidationException ve) {
                render(view: 'createquote', model: [
                        quoteInstance: q, yearRange: (yrNow - 2)..(yrNow + 2), addresses: addresses, mobiles: mobiles,
                        emails: emails, makes: makes,models: models,variants: variants,
                        jobs         : [], clients: clients, enableUsed: false, usedYears: usedYears,
                        nu: params.nu, employers: employers, newVehicle: newVehicle, employerEnable: employerEnable,
                        employerType: params.employerType ? params.employerType : "",
                        selectPayFreq: params.selectPayFreq ? params.selectPayFreq : "",
                        priceObtained: params.priceObtained
                ])
            }
        }
    }

    @Secured(['ROLE_SUPER', 'ROLE_ADMIN', 'ROLE_EMPLOYER'])
    def approvequote = {
        def q = quoteService.getQuote(params);
        if (q && quoteService.changeStatus(q, QuoteStatus.QUOTE_APPROVED, "Quote approved by employer")) {
            def user = User.findByCustomer(q.customer)
            def quoteNo = formatNumber(number:q.id, format:message(code:'quote.number.mask'))
            if (user) {
                def msg = quoteService.generateCustomerEmailMsg("Finance Approval","approvedQuoteNotifyCustomer",q)
                jmsService.send([service: 'notification', method: 'email'], msg)
            }
            flash.message = "quote.status.change.approved"
            flash.args = [formatNumber(number: q.id, format: message(code: 'quote.number.mask'))]
            flash.defaultMessage = "This quote ${q.id} has been successfully approved by the employer!"
            flash.classes = "flashOk"
            redirect(action: "viewquote", id: q.id)
        } else {
            flash.message = "quote.approve.error"
            flash.defaultMessage = "Could not approve quote!"
            flash.classes = "flashErr"
            redirect(action: "quote")
        }
    }

    def selectquote = {
        def q = quoteService.getQuote(params);
        if (q && quoteService.changeStatus(q, QuoteStatus.QUOTE_CONFIRMED, "Quote selected for progression")) {
            // notify administrator the preferred quote has been selected
            def user = User.findByCustomer(q.customer)

            if (user) {
                def adminMsg = quoteService.generateAdminEmailMsg("Preferred Quote Selected","preferredQuoteNotifyAdmin",q)
                jmsService.send([service: 'notification', method: 'email'], adminMsg)

                def custMsg = quoteService.generateCustomerEmailMsg("Preferred Quote Selected","preferredQuoteNotifyCustomer",q)
                jmsService.send([service: 'notification', method: 'email'], custMsg)
            }
            flash.message = "quote.status.change"
            flash.args = [formatNumber(number: q.id, format: message(code: 'quote.number.mask'))]
            flash.defaultMessage = "Status for quote ${q.id} changed successfully!"
            flash.classes = "flashOk"
            redirect(action: "viewquote", id: q.id)
        } else {
            flash.message = "quote.select.error"
            flash.defaultMessage = "Could not select preferred quote!"
            flash.classes = "flashErr"
            redirect(action: "quote")
        }
    }

    def selectquoteAjax = {
        def resp = [alreadySelected: false, selectedAqn: null]
        def q = quoteService.getQuote(params)
        if (q) {
            def pref = quoteService.hasPreferredSelected(q)
            if (pref) {
                def id = pref.get(0).id
                def aqn = formatNumber(number: id, format: message(code: 'quote.number.mask'))
                resp = [alreadySelected: true, selectedId: id, selectedAqn: aqn]
            }
        }
        render resp as JSON
    }

    def validateQuoteAjax = {
        //
    }

    /*def syncPopularDomain = {

        def domains = params.domainList

        def domainList = Arrays.asList(domains.split(","))

        domainList.each { domain->
            def domainInstance = iris.PopularDomain.findByDomain(domain)

            if(!domainInstance){
                def newDomain = new PopularDomain()
                newDomain.domain = domain
                newDomain.save(flush: true)
            }
        }

        render PopularDomain.list() as JSON
    }*/

    private void preSaveCheck(quote) {
        //=== deal with Taxable Value preferences
        def validCodes = []
        def prefix = 'ALLOWED_TV_'
        quote.allowedFbtCalcMethods.clear()    //remove all FBT taxable value types first

        for (String key in params.keySet()) {
            if (key.contains(prefix) && 'on' == params.get(key)) {
                def code = Integer.valueOf(key.substring(prefix.length()))
                validCodes << code
                quote.addToAllowedFbtCalcMethods(TaxableValueType.get(code))
            }
        }
        // pick up the default...
        if (params.tv_default) {
            def defaultCode = Integer.valueOf(params.tv_default)
            if (validCodes.contains(defaultCode)) {
                // move the default code to be first index in the list: allowedFbtCalcMethods
                TaxableValueType tvDefault = TaxableValueType.get(defaultCode)
                def idx = quote.allowedFbtCalcMethods.indexOf(tvDefault)
                TaxableValueType removed = quote.allowedFbtCalcMethods.remove(idx)
                quote.allowedFbtCalcMethods.add(0, removed)
            } else {
                // raise an error on this quote record? etc...
                println "tv_default INVALID = ${defaultCode}"

            }
        }
    }


}
