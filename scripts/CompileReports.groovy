includeTargets << grailsScript("Init")
includeTargets << grailsScript("Package")

target(jasperreportcompile: "Compile all *.jrxml files as *.jasper") {
	depends(packageApp)
	
	// TODO: Implement script here
	def sourceDir = (config.reports.src.location ?: 'src/java')
	/*Ant.echo "baseDir = ${basedir}"
	Ant.echo "sourceDir = ${sourceDir}"
	Ant.echo "classesDir = ${classesDir}"*/
	
	// compile the .jasper files into the web-app WEB-INF/classes folder.
	// these will be on the classpath when deployed as a war file.
	def classesDestDir = "${basedir}/web-app/WEB-INF/classes"
	def imagesSrcDir   = "${basedir}/web-app/images/reports"
	def imagesFolder    = "report-images"
	def imagesDestDir  = "${classesDestDir}/${imagesFolder}"
	
	mkdir(dir:"${classesDestDir}")
	Ant.taskdef(classpathref:"grails.classpath", name:"jrc", classname:"net.sf.jasperreports.ant.JRAntCompileTask")
	jrc(srcdir:"${basedir}/${sourceDir}", destdir:classesDestDir, keepjava:"false", xmlvalidation:"true") {
		include(name:"**/*.jrxml")
	}
	
	// copy report images & report resource/message bundle & fonts
	mkdir(dir:imagesDestDir)
	copy(todir:imagesDestDir) { fileset(dir:imagesSrcDir, excludes:"*.ttf") }
	copy(todir:classesDestDir) {
		fileset(dir:"${basedir}/grails-app/i18n", includes:"reports*.properties")
		fileset(dir:imagesSrcDir, includes:"*.ttf")
	}
	
	// also copy the generated .jasper files into the grails project cache...
	// usually at:     $USER_HOME/.grails/x.x.x/$PROJECT/classes
	mkdir(dir:"${classesDir}/${imagesFolder}")
	copy(todir:"${classesDir}/${imagesFolder}") { fileset(dir:imagesDestDir) }
	copy(todir:classesDir) {
		fileset(dir:classesDestDir, includes:"**/*.jasper, reports*.properties, *.ttf")
	}
	
	event("ReportCompileEnd", ["JasperReports JRXML file compilation complete"])
}

setDefaultTarget(jasperreportcompile)
