/*
 * application.js - App-specific js code
 * 
 */


Number.prototype.formatMoney = function(c, d, t){
	var n = this, c = isNaN(c = Math.abs(c)) ? 2 : c, d = d == undefined ? "," : d, t = t == undefined ? "." : t, s = n < 0 ? "-" : "", i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "", j = (j = i.length) > 3 ? j % 3 : 0;
	return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
};


/*
 * enable the drop-down menu functionality of the main menu on each page
 */
function enableMainMenuDropDowns() {
	//Only shows drop down trigger when js is enabled - Adds empty span tag after ul.subnav
	$("ul.subnav").parent().append("<span></span>");
	//When trigger is clicked...
	$("ul.topnav li span, ul.topnavRight li span").click(function() {
		//Following events are applied to the subnav itself (moving subnav up and down)
		//Drop down the subnav on click
		$(this).parent().find("ul.subnav").slideDown('fast').show();
		$(this).parent().hover(
			function() {
				// do nothing
			},
			function() {
				//When the mouse hovers out of the subnav, move it back up
				$(this).parent().find("ul.subnav").slideUp('slow');
			}
		);
	}).hover(
		//Following events are applied to the trigger (Hover events for the trigger)
		function() { 
			//On hover over, add class "subhover"
			$(this).addClass("subhover");
		},
		function() {	//On Hover Out
			//On hover out, remove class "subhover"
			$(this).removeClass("subhover");
		}
	);
}

function tableRowMouseOver() {
	$(this).addClass("subhover");
}
function tableRowMouseOut() {
	$(this).removeClass("subhover");
}

//===========================================================================
// kick-off
//===========================================================================
$(document).ready(function() {
	// enable main menu
	enableMainMenuDropDowns();
	
	// enable SELECT z-index iFrame fix for IE6 and older
	// AND, PNG transparency fix!
	if ($.browser.msie() && $.browser.version.number() < 7.0) {
		$('#navheader ul, #navheader li').bgiframe();
		$('div.logoDiv').pngFix();
	}
	
	$('.help-tip').tipsy({
		gravity: $.fn.tipsy.autoWE
	}).click(function() {
		return false;
	});
	
	// add tooltips to greybutton's by default
	$('.greybutton').tipsy({ gravity:'s' })
	
	// add highlighting on mouseover to (all) table listings and click selection to rows!
	/*$('table.list tbody tr').mouseover(function() {
		$(this).addClass('highlight');
	}).mouseout(function() {
		$(this).removeClass('highlight');
	}).click(function(e) {
		var $target = $(e.target);
		if (!((e.target.type == 'checkbox') || $target.is("a"))) {
			$(':checkbox', this).attr('checked', function() {
				return !this.checked;
			});
		}
	});*/
	
	// add behaviour to "select all" checkbox of listing tables
	$('table.list thead #chkAll').click(function() {
		$(this).parent().parent().parent().next().find(".chkRow").attr("checked", this.checked);
	});
	
	// hide flash messages after a 5 second period...
	$(".flashMsg").delay(5000).fadeOut('slow');
	
	// add datepickers to all marked elements..
	$('.dateField').datepicker({
		changeMonth: true, changeYear: true, dateFormat: 'd/m/yy', showButtonPanel: true,
		showOtherMonths: true, selectOtherMonths: true, showOn: 'both', buttonImageOnly: true,
		buttonText: 'Select Date...', buttonImage: "/iris/images/skin/calendar.png"
	});
	
});

