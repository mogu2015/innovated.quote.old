<%	import org.codehaus.groovy.grails.orm.hibernate.support.ClosureEventTriggeringInterceptor as Events
	excludedProps = ["version", Events.ONLOAD_EVENT, Events.BEFORE_INSERT_EVENT, Events.BEFORE_UPDATE_EVENT, Events.BEFORE_DELETE_EVENT]
		props = domainClass.properties.findAll { !excludedProps.contains(it.name) && it.type != Set.class }
		Collections.sort(props, comparator.constructors[0].newInstance([domainClass] as Object[]))
		maxCols = Math.min(props.size(), 5) %>
<script type="text/javascript">
\$(document).ready(function() {
	var table = \$('#${domainClass.propertyName}DataTable').dataTable({
		"iDisplayLength": 100, "sPaginationType": "full_numbers",
		"iCookieDuration": 3600, "bStateSave": true, "bProcessing": true, "bServerSide": true,
		"sAjaxSource": "\${createLink(controller:'${domainClass.propertyName}', action:'listAjax')}",
		"aLengthMenu": [[25, 50, 100, 250, 500, 1000], [25, 50, "100 (Default)", 250, 500, 1000]],
		"aaSorting": [[ 3, "asc" ]],	// column sorting defaults (zero-indexed)
		"aoColumns": [
			{ "sClass":"cellLeft", "bSortable":false },	// checkbox column
			{ "sClass":"cellCtr", "bSortable":false }	// icon/spacer column
			<%	props.eachWithIndex { p, i -> if (i < maxCols) { sClass = (i == (maxCols - 1)) ? "cellRight" : "cellCtr" %>
			,{ "sClass":"${sClass}" }<%  }	} %>
		],
		"fnDrawCallback":function(){
			\$('td').bind('mouseenter',function(){ \$(this).parent().children().each(function(){\$(this).addClass('tabelRowHighlight');}); });
			\$('td').bind('mouseleave',function(){ \$(this).parent().children().each(function(){\$(this).removeClass('tabelRowHighlight');}); });
		}
	});
	table.fnSetFilteringDelay(300);	// 300 msec after "keyup" the ajax lookup will fire
});
</script>

<table id="${domainClass.propertyName}DataTable" class="list">
<thead>
	<tr>
		<th class="hdrLeft" width="20"><g:checkBox id="chkAll" name="chkAll" value="\${false}" title="Select all rows" /></th>
		<th class="hdrCtr" width="16">&nbsp;</th>
		<%	props.eachWithIndex { p, i -> if (i < maxCols) { cssClass = (i == (maxCols - 1)) ? "hdrRight" : "hdrCtr" %>
		<th class="${cssClass}"><g:message code="${domainClass.propertyName}.${p.name}" default="${p.naturalName}" /></th><%  }	} %>
	</tr>
</thead>
<tbody>
	<tr>
		<td colspan="${maxCols + 2}" class="dataTables_empty">Loading data from server</td>
	</tr>
</tbody>
</table>
