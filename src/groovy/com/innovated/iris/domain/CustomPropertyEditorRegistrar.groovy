/**
 * 
 */
package com.innovated.iris.domain

import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.beans.PropertyEditorRegistrar;
import org.springframework.beans.PropertyEditorRegistry;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.context.i18n.LocaleContextHolder;

/**
 * @author tamati
 *
 */
class CustomPropertyEditorRegistrar implements PropertyEditorRegistrar {
	def messageSource;
	
	public void registerCustomEditors(PropertyEditorRegistry registry) {
		registry.registerCustomEditor(Date.class, 
			new CustomDateEditor(
				new SimpleDateFormat(messageSource.getMessage("datepicker.date.format", null, "d/M/yyyy", LocaleContextHolder.locale)),
				true
			)
		);
	}
}
