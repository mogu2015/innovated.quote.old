/**
 * 
 */
package com.innovated.iris.server.fbt;

import java.util.Date;

import com.domainlanguage.money.Money;
import com.innovated.iris.services.FbtService;
import com.innovated.iris.services.TaxService;
import com.innovated.iris.util.CurrencyUtils;
import com.innovated.iris.domain.PackageComparison;
import com.innovated.iris.domain.PackageComparisonItem;

/**
 * @author tamati
 *
 */
class DefaultPackageComparisonHandler implements IPackageComparisonHandler {
	private PackageComparison comparison
	private FbtService fbtService
	private TaxService taxService
	
	public DefaultPackageComparisonHandler(PackageComparison comparison, FbtService fbtService, TaxService taxService) {
		this.comparison = comparison
		this.fbtService = fbtService
		this.taxService = taxService
	}
	
	/* (non-Javadoc)
	 * @see com.innovated.iris.server.fbt.IPackageComparisonHandler#handle()
	 */
	public void handle() {
		comparison.refresh()	// refresh instance from the DB
		removeItems()			// remove any comparison items first!

		// add relevant items... TODO: this could be done much better ;(
		comparison.addToItems(new PackageComparisonItem(type:PackagingComparisonItemType.GROSS_SALARY,      amount:getGrossSalary()))
		comparison.addToItems(new PackageComparisonItem(type:PackagingComparisonItemType.PRE_TAX_VEH_EXP,   amount:getPreTaxVehicleExpense()))
		comparison.addToItems(new PackageComparisonItem(type:PackagingComparisonItemType.PRE_TAX_VEH_FBT,   amount:getPreTaxVehicleFbt()))
		comparison.addToItems(new PackageComparisonItem(type:PackagingComparisonItemType.PRE_TAX_VEH_LUX,   amount:getPreTaxVehicleLux()))
		//comparison.addToItems(new PackageComparisonItem(type:PackagingComparisonItemType.PRE_TAX_VEH_TOTAL, amount:getPreTaxVehicleTotal()))
		comparison.addToItems(new PackageComparisonItem(type:PackagingComparisonItemType.PRE_TAX_EMP_SHARE, amount:getPreTaxEmployerShare()))
		comparison.addToItems(new PackageComparisonItem(type:PackagingComparisonItemType.PRE_TAX_EMP_ADMIN, amount:getPreTaxEmployerAdmin()))
		//comparison.addToItems(new PackageComparisonItem(type:PackagingComparisonItemType.PRE_TAX_EMP_TOTAL, amount:getPreTaxEmployerTotal()))
		//comparison.addToItems(new PackageComparisonItem(type:PackagingComparisonItemType.PRE_TAX_OTHER,     amount:getPreTaxOther()))
		//comparison.addToItems(new PackageComparisonItem(type:PackagingComparisonItemType.PRE_TAX_TOTAL,     amount:getPreTaxTotal()))
		comparison.addToItems(new PackageComparisonItem(type:PackagingComparisonItemType.TAXABLE_SALARY,    amount:getTaxableSalary()))
		comparison.addToItems(new PackageComparisonItem(type:PackagingComparisonItemType.TAXES_INCOME,      amount:getTaxesIncome()))
		comparison.addToItems(new PackageComparisonItem(type:PackagingComparisonItemType.TAXES_MCARE_LEVY,  amount:getTaxesMedicareLevy()))
		//comparison.addToItems(new PackageComparisonItem(type:PackagingComparisonItemType.TAXES_HECS_HELP,   amount:getTaxesHecsHelp()))
		//comparison.addToItems(new PackageComparisonItem(type:PackagingComparisonItemType.TAXES_OTHER,       amount:getTaxesOther()))
		comparison.addToItems(new PackageComparisonItem(type:PackagingComparisonItemType.TAXES_TOTAL,       amount:getTaxesTotal()))
		comparison.addToItems(new PackageComparisonItem(type:PackagingComparisonItemType.POST_TAX_VEH_EXP,  amount:getPostTaxVehicle()))
		//comparison.addToItems(new PackageComparisonItem(type:PackagingComparisonItemType.POST_TAX_OTHER,    amount:getPostTaxOther()))
		//comparison.addToItems(new PackageComparisonItem(type:PackagingComparisonItemType.POST_TAX_TOTAL,    amount:getPostTaxTotal()))
		comparison.addToItems(new PackageComparisonItem(type:PackagingComparisonItemType.NET_SALARY,        amount:getNetSalary()))
	}
	
	private double getGrossSalary() {
		return CurrencyUtils.roundUpNearestCent(comparison.quote.grossAnnualSalary)
	}
	private Money calculateTaxableValue() {
		double gstRate = taxService.lookupGstRate(comparison.quote.fromDate)
		double multiplier = /*(comparison.quote.canClaimGst) ? 1.0 :*/ (1.0 + gstRate)
		Money tvMoney = fbtService.taxableValue(comparison.quote.fromDate, comparison.tvType,
			[
				totalKms:comparison.quote.kmPerYear, baseValue:comparison.quote.fbtBaseValue, daysAvailable:1, daysInFbtYear:1,
				contributions:0, operatingCosts:comparison.quote.annualRunningCosts * multiplier,
				privateUseRate:(1.0 - (comparison.quote.businessUsage/100)),
				commenceDate:comparison.quote.fromDate
			]
		)
		return tvMoney
	}
	private double getPreTaxVehicleExpense() {
		double amt = 0.0
		
		if (comparison.noVehicle || comparison.comparisonOnly) {
			amt = 0.0
		} else {
			double gstRate = taxService.lookupGstRate(comparison.quote.fromDate)
			if (comparison.usingEcm) {
				// using ecm, so subtract contributed amount & gst portion of pretax payment
				Money tvMoney = calculateTaxableValue()
				Money fbtPayable = fbtService.fbtPayable(comparison.quote.fromDate, comparison.quote.concession, tvMoney, comparison.quote.canClaimGst)
				Money postTax = fbtService.postTaxContribution(comparison.quote.fromDate, fbtPayable, comparison.quote.canClaimGst)
				
				double postTaxContrib = postTax.breachEncapsulationOfAmount().doubleValue()
				double postTaxContribExGst = postTaxContrib / (1 + gstRate)

				if (comparison.quote.canClaimGst) {
					amt = comparison.quote.annualTotalCosts - postTaxContribExGst
				} else {
					amt = (comparison.quote.annualTotalCosts * (1 + gstRate)) - postTaxContrib
				}
			}
			else {
				if (comparison.quote.canClaimGst) {
					amt = comparison.quote.annualTotalCosts
				} else {
					amt = comparison.quote.annualTotalCosts * (1 + gstRate)
				}
			}
		}
		return CurrencyUtils.roundUpNearestCent(amt)
	}
	private double getPreTaxVehicleFbt() {
		double amount = 0.0
		if (comparison.noVehicle || comparison.comparisonOnly || comparison.usingEcm) {
			amount = 0.0
		}
		else {
			Money tvMoney = calculateTaxableValue()
			double tv = tvMoney.breachEncapsulationOfAmount().doubleValue()
			amount = fbtService.fbtPayable(comparison.quote.fromDate, comparison.quote.concession, tv, comparison.quote.canClaimGst)
		}
		return CurrencyUtils.roundUpNearestCent(amount)
	}
	private double calculateLux() {
		// we don't calculate LUX allowance if the FBT value is less than the LCT threshold!!
		def lctThreshold = comparison.quote.lctThreshold 
		if (comparison.quote.vehicle.fuelConsumptionCombined < comparison.quote.lctFuelConsumption) {
			lctThreshold = comparison.quote.lctFuelEfficientThreshold
		}
		if (comparison.quote.fbtBaseValue < lctThreshold) {
			return 0.0
		}
		
		// if we get here, we're above the threshold - lets calculate the allowance!
		double presentValue = comparison.quote.fbtBaseValue
		if (comparison.quote.fbtBaseValue > comparison.quote.lctThreshold) {
			presentValue = comparison.quote.lctThreshold
		}
		double residualValue = comparison.quote.rvValue
		if (comparison.quote.rvUseAltValue) {
			residualValue = comparison.quote.rvAltValue
		}
		double annualRate = comparison.quote.luxSlRate
		if (comparison.quote.luxUseDv) {
			annualRate = (comparison.quote.kmPerYear > comparison.quote.luxDvThreshold) ? comparison.quote.luxDvRateHigh : comparison.quote.luxDvRateLow
		}
		
		// (D) total rentals over term
		double rentalsOverTerm = (comparison.quote.finVehMthlyPymt * comparison.quote.leaseTerm)
		// (A) calculate interest...
		double interest = rentalsOverTerm + residualValue - comparison.quote.fbtBaseValue
		
		// (B) Setup calculate accumulated depreciation...
		double accumDepn = taxService.accumulatedDepreciation(presentValue, residualValue, annualRate, comparison.quote.leaseTerm, comparison.quote.luxUseDv)
		
		// (D) deductions = interest + depreciation
		double deductions = interest + accumDepn
		
		// calculate annual cost to company
		double annualCost = (((rentalsOverTerm - deductions) / comparison.quote.leaseTerm) * 12.0 * comparison.quote.luxCompanyTaxRate) / (1.0 - comparison.quote.luxCompanyTaxRate)
		
		return annualCost
	}
	private double getPreTaxVehicleLux() {
		double amount = 0.0
		if (comparison.noVehicle || comparison.comparisonOnly) {
			amount = 0.0
		}
		else {
			amount = calculateLux()
		}
		return CurrencyUtils.roundUpNearestCent(amount)
	}
	private double getPreTaxVehicleTotal() {
		return (getPreTaxVehicleExpense() + getPreTaxVehicleFbt() + getPreTaxVehicleLux())
	}
	private double getPreTaxEmployerShare() {
		double amt = 0.0
		if (comparison.noVehicle || comparison.comparisonOnly) {
			amt = 0.0
		}
		else {
			double packagedAmt = getPreTaxVehicleTotal()

			//

			amt = taxService.taxSavingEmployerShare(comparison.quote.fromDate, comparison.quote.grossAnnualSalary,
				comparison.quote.defaultPayFreq.getCode(), packagedAmt, comparison.quote.employerShareRate, comparison.quote.employerShareFee)
		}
		return CurrencyUtils.roundUpNearestCent(amt)
	}
	private double getPreTaxEmployerAdmin() {
		double amount = (comparison.noVehicle || comparison.comparisonOnly) ? 0.0 : comparison.quote.employerShareFee
		return CurrencyUtils.roundUpNearestCent(amount)
	}
	private double getPreTaxEmployerTotal() {
		return (getPreTaxEmployerShare() + getPreTaxEmployerAdmin())
	}
	private double getPreTaxOther() {
		double amt = 0.0
		//
		return amt
	}
	private double getPreTaxTotal() {
		return (getPreTaxVehicleTotal() + getPreTaxEmployerTotal() + getPreTaxOther())
	}
	private double getTaxableSalary() {
		return (getGrossSalary() - getPreTaxTotal())
	}
	private double getTaxesIncome() {
		double amount = taxService.incomeTaxPayable(comparison.quote.fromDate, getTaxableSalary())
		return CurrencyUtils.roundUpNearestCent(amount)
	}
	private double getTaxesMedicareLevy() {
		double amount = taxService.medicareLevyPayable(comparison.quote.fromDate, getTaxableSalary())
		return CurrencyUtils.roundUpNearestCent(amount)
	}
	private double getTaxesHecsHelp() {
		double amt = 0.0
		//
		return amt
	}
	private double getTaxesOther() {
		double amt = 0.0
		//
		return amt
	}
	private double getTaxesTotal() {
		return (getTaxesIncome() + getTaxesMedicareLevy() + getTaxesHecsHelp() + getTaxesOther())
	}
	private double getPostTaxVehicle() {
		double amount = 0.0
		if (comparison.noVehicle || comparison.comparisonOnly || comparison.usingEcm) {
			double gstRate = taxService.lookupGstRate(comparison.quote.fromDate)
			if (comparison.noVehicle) {
				amount = 0.0
			}
			else if (comparison.comparisonOnly) {
				amount = (comparison.quote.annualTotalCosts * (1 + gstRate))// + calculateLux()
			}
			else {
				Money tvMoney = calculateTaxableValue()
				Money fbtPayable = fbtService.fbtPayable(comparison.quote.fromDate, comparison.quote.concession, tvMoney, comparison.quote.canClaimGst)
//				System.out.println("fbtPayable:  "+ fbtPayable.toString());
				Money postTax = fbtService.postTaxContribution(comparison.quote.fromDate, fbtPayable, comparison.quote.canClaimGst)
				amount = postTax.breachEncapsulationOfAmount().doubleValue()
			}
		}
		else {
			amount = 0.0
		}
		return CurrencyUtils.roundUpNearestCent(amount)
	}
	private double getPostTaxOther() {
		double amt = 0.0
		//
		return amt
	}
	private double getPostTaxTotal() {
		return (getPostTaxVehicle() + getPostTaxOther())
	}
	private double getNetSalary() {
		return (getTaxableSalary() - getTaxesTotal() - getPostTaxTotal())
	}
	
	
	private void removeItems() {
		def currItems = PackageComparisonItem.findAllWhere(comparison:comparison)
		currItems.each {
			comparison.removeFromItems(it)		// remove the association
			it.delete()							// delete the item
		}
	}
	
}
