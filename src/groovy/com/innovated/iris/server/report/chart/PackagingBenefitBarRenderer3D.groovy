package com.innovated.iris.server.report.chart;

import java.awt.Color;
import java.awt.Paint;

import org.jfree.chart.renderer.category.BarRenderer3D;
import org.jfree.data.category.CategoryDataset;

class PackagingBenefitBarRenderer3D extends BarRenderer3D {
	/**
	 * Returns the colour:
	 *    green>0, yellow=0 & red <0
	 *
	 * @param row  the series.
	 * @param column  the category.
	 * @return The item color.
	 */
	public Paint getItemPaint(int row, int column) {
		CategoryDataset dataset = getPlot().getDataset();
		double value = dataset.getValue((int) row, (int) column).doubleValue();
		if (value > 0.0)
			return Color.green
		else if (value == 0.0)
			return Color.yellow
		else
			return Color.red
	}
}
