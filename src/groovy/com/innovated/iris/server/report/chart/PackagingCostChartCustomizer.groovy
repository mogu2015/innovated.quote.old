/**
 * 
 */
package com.innovated.iris.server.report.chart;

import java.awt.Color;
import java.awt.Font;
import java.awt.Paint;
import java.text.DecimalFormat;
import java.text.NumberFormat;

import org.jfree.chart.JFreeChart;
import org.jfree.chart.labels.ItemLabelAnchor;
import org.jfree.chart.labels.ItemLabelPosition;
import org.jfree.chart.labels.StandardCategoryItemLabelGenerator;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.renderer.category.BarRenderer;
import org.jfree.chart.renderer.category.BarRenderer3D;
import org.jfree.chart.renderer.category.CategoryItemRenderer;
import org.jfree.data.category.CategoryDataset;
import org.jfree.ui.TextAnchor;

import net.sf.jasperreports.engine.JRAbstractChartCustomizer;
import net.sf.jasperreports.engine.JRChart;

/**
 * @author tamati
 *
 */
class PackagingCostChartCustomizer extends JRAbstractChartCustomizer {
	/**
	 * This method gets called for any special chart customization.
	 */
	public void customize(JFreeChart chart, JRChart jasperChart) {
		// add chart customization here...
		CategoryPlot plot = (CategoryPlot) chart.getPlot()
		//PackagingBenefitBarRenderer3D renderer = new PackagingBenefitBarRenderer3D()
		CategoryItemRenderer renderer = plot.getRenderer()
		
		Font font = new Font("Verdana", Font.BOLD, 7)
		renderer.setBaseItemLabelFont(font)

		renderer.setMaximumBarWidth(0.50)

		renderer.setBaseItemLabelsVisible(true)
		NumberFormat labelFormat = NumberFormat.getCurrencyInstance()
		labelFormat.setMinimumFractionDigits(0)
		labelFormat.setMaximumFractionDigits(0)
		renderer.setBaseItemLabelGenerator(new StandardCategoryItemLabelGenerator("{2}", labelFormat))
		renderer.setBasePositiveItemLabelPosition(new ItemLabelPosition(ItemLabelAnchor.CENTER, TextAnchor.CENTER))
		plot.setRenderer(renderer)
	}
}
