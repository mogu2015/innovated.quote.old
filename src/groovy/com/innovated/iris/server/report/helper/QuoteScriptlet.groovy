/**
 * 
 */
package com.innovated.iris.server.report.helper;

import java.util.List;

import org.codehaus.groovy.grails.commons.ApplicationHolder;
import org.springframework.context.ApplicationContext;

import com.innovated.iris.domain.Customer;
import com.innovated.iris.domain.Employment;
import com.innovated.iris.domain.PackageComparison;
import com.innovated.iris.domain.Quote;
import com.innovated.iris.domain.QuoteItemInsurance;
import com.innovated.iris.domain.QuoteItemOption;
import com.innovated.iris.server.fbt.PackagingComparisonItemType;
import com.innovated.iris.services.IQuoteService;
import com.innovated.iris.util.CurrencyUtils;

import net.sf.jasperreports.engine.JRDefaultScriptlet;
import net.sf.jasperreports.engine.JRScriptletException;

/**
 * @author tamati
 *
 */
class QuoteScriptlet extends JRDefaultScriptlet {
	
	private IQuoteService getQuoteService() {
		ApplicationContext ctx = (ApplicationContext) ApplicationHolder.getApplication().getMainContext();
		return (IQuoteService) ctx.getBean("quoteService");
	}
	
	public double roundNearestCent(double amount) throws JRScriptletException {
		return CurrencyUtils.roundNearestCent(amount)
	}
	
	public double roundUpNearestCent(double amount) throws JRScriptletException {
		return CurrencyUtils.roundUpNearestCent(amount)
	}
	
	public String getDefaultPhoneEmployee(String type) throws JRScriptletException {
		def number = null
		
		Customer customer = (Customer) this.getFieldValue("customer");
		def phones = customer.entity.phones.findAll{ it.phoneType.name == type }
		
		if (phones.size() > 0) {
			number = phones.get(0).toString()
		}
		return number
	}
	
	public String getDefaultEmailEmployee() throws JRScriptletException {
		def email = null
		
		Customer customer = (Customer) this.getFieldValue("customer");
		def emails = customer.entity.emails
		
		if (emails.size() > 0) {
			email = emails.get(0).toString()
		}
		return email
	}
	
	public String getDefaultPhoneEmployer(String type) throws JRScriptletException {
		def number = null
		
		Employment employment = (Employment) this.getFieldValue("employment");
		def phones = employment.employer.phones.findAll{ it.phoneType.name == type }
		
		if (phones.size() > 0) {
			number = phones.get(0).toString()
		}
		return number
	}
	
	public String getDefaultContactEmployer() throws JRScriptletException {
		def name = null
		
		Employment employment = (Employment) this.getFieldValue("employment");
		def contacts = employment.employer.contacts
		
		if (contacts.size() > 0) {
			name = contacts.get(0).toString()
		}
		return name
	}
	
	public String getDefaultAddressEmployer(String type) throws JRScriptletException {
		def address = null
		
		Employment employment = (Employment) this.getFieldValue("employment");
		def addresses = employment.employer.addresses.findAll{ it.addressType.name == type }
		
		if (addresses.size() > 0) {
			address = addresses.get(0).toString()
		}
		return address
	}
	
	public String getDefaultEmailEmployer() throws JRScriptletException {
		def email = null
		
		Employment employment = (Employment) this.getFieldValue("employment");
		def emails = employment.employer.emails
		
		if (emails.size() > 0) {
			email = emails.get(0).toString()
		}
		return email
	}
	
	public String getVehicleOptions() throws JRScriptletException {
		def optionListString = null
		def options = (List<QuoteItemOption>) this.getFieldValue("optionItems");
		if (options.size() > 0) {
			optionListString = options.join(', ')
		} else {
			optionListString = "None"
		}
		return optionListString
	}
	
	public String getInsuranceItems() throws JRScriptletException {
		def itemListString = null
		def items = (List<QuoteItemInsurance>) this.getFieldValue("insuranceItems");
		if (items.size() > 0) {
			itemListString = items.join(', ')
		} else {
			itemListString = "None"
		}
		return itemListString
	}
	
	public double getInputTaxCredit(PackageComparison c, boolean annual) throws JRScriptletException {
		def quote = c.quote
		def preAmt = 0.0 + c.items.find{ it.type == PackagingComparisonItemType.PRE_TAX_VEH_EXP }.amount
		def itc = roundUpNearestCent(preAmt / 10.0 / quote.defaultPayFreq.code)
		if (annual) {
			itc = itc * quote.defaultPayFreq.code
		}
		return (quote.canClaimGst) ? -1.0 * itc : 0.0
	}
	
	public double getFbt(PackageComparison c, boolean annual) throws JRScriptletException {
		def quote = c.quote
		def preAmt = 0.0 + c.items.find{ it.type == PackagingComparisonItemType.PRE_TAX_VEH_FBT }.amount
		def fbt = roundUpNearestCent(preAmt / quote.defaultPayFreq.code)
		if (annual) {
			fbt = fbt * quote.defaultPayFreq.code
		}
		return fbt
	}
	
	public double getLux(PackageComparison c, boolean annual) throws JRScriptletException {
		def quote = c.quote
		def preAmt = 0.0 + c.items.find{ it.type == PackagingComparisonItemType.PRE_TAX_VEH_LUX }.amount
		def lux = roundUpNearestCent(preAmt / quote.defaultPayFreq.code)
		if (annual) {
			lux = lux * quote.defaultPayFreq.code
		}
		return lux
	}
	
	public List getQuoteAsDataSource(PackageComparison chosenComparison) throws JRScriptletException {
		// wrap the quote in a list, and return it!
		return [ chosenComparison.quote ]
	}
	
	public List getNonZeroPackageComparisonItems(List itemList) throws JRScriptletException {
		return itemList.findAll{ it.amount > 0 }
	}
	
	public List getBenefitAnalysisData(PackageComparison chosenComparison) throws JRScriptletException {
		def netSalaryNotPackaged = chosenComparison.quote.comparisons[0].items[-1].amount
		def netSalaryPackaged = chosenComparison.items[-1].amount
		
		def netIncrease = netSalaryPackaged - netSalaryNotPackaged
		def label = "Net Salary Increase"
		
		return [[ series:"Benefit Received", category:"Annual Benefit", value:netIncrease, label:label ]]
	}
	
	public List getBenefitAnalysisBreakdownData(PackageComparison chosenComparison) throws JRScriptletException {
		def data = []
		def quote = chosenComparison.quote
		
		// add comparisons to be analysed, ie:
		//   1. "Vehicle - Not Novated"
		//   2. "Vehicle - Novated"   <- (chosen comparison)
		data << quote.comparisons[0]
		data << chosenComparison
		
		def typeList = []
		def types = PackagingComparisonItemType.getTypes()
		types.eachWithIndex { type, idx ->
			boolean atLeastOneNonZeroAmt = false
			data.each {
				def found = it.items.find{ it.type == type}
				if (found && (found.amount > 0))
					atLeastOneNonZeroAmt = true
			}
			if (atLeastOneNonZeroAmt)
				typeList << type
		}
		
		def returnMap = []
		typeList.eachWithIndex { type, idx ->
			def item1 = quote.comparisons[0].items.find{ it.type == type}
			def item2 = chosenComparison.items.find{ it.type == type}
			returnMap << [
				item:        type,
				not_novated: (type.isCredit || type.isTotal || type.isSubTotal ? 1 : -1) * item1.amount,
				novated:     (type.isCredit || type.isTotal || type.isSubTotal ? 1 : -1) * item2.amount
			]
		}
		return returnMap
	}
	
	public List getRunningCostsBreakdown(PackageComparison chosenComparison) throws JRScriptletException {
		return getQuoteService().retrieveBreakdown(chosenComparison.quote)
	}
	
	public List getQuoteTermsAndConditions(PackageComparison chosenComparison) throws JRScriptletException {
		return getQuoteService().retrieveTermsAndConditions(chosenComparison.quote)
	}
	
	public String getPayCycleSingular(PackageComparison chosenComparison) throws JRScriptletException {
		return chosenComparison.quote.defaultPayFreq.getSingular()
	}
	
	public List getPackagingCostGraphData(PackageComparison comp) throws JRScriptletException {
		def data = []
		
		def quote = comp.quote
		def costNotPackaged = Math.abs(quote.comparisons.get(0).items[-2].amount)
		def salaryNotPackaged = Math.abs(quote.comparisons.get(0).items[-1].amount)
		def salaryPackaged = Math.abs(comp.items[-1].amount)

		def costPerPayNotPackaged = costNotPackaged / quote.defaultPayFreq.getCode()
		def costPerPayPackaged = (costNotPackaged - (salaryPackaged - salaryNotPackaged)) / quote.defaultPayFreq.getCode()

		data << [ series:"Not Novated", category:"Not Novated", value:costPerPayNotPackaged, label:"Not Novated" ]
		data << [ series:"Novated", category:"Novated", value:costPerPayPackaged, label:"Novated" ]
		
		return data
	}
}
