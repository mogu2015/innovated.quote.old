//    This program is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//    Copyright (c) by Christoph Bodner, 2007

package jinterestrate;

/**
 * Every scientific or financial pocket calculator has a built-in SOLVE function to
 * compute a root for <code>f(x) = 0</code>, and here is an open source alternative
 * in Java 5.
 * <p>
 * This module follows the good old Newton method "regula falsi" in a puristic way,
 * interval boundaries are not required, but a guess value as a starting condition. 
 * Special care has been taken to allow functions with undefined <code>f(0)</code>.
 * <p>
 * <b>Criticism:</b> There are functions where this approach will deliver inaccurate results
 * or end up in silly error conditions. Especially bad-conditioned for this method is, e.g., 
 * <code>y = x^3</code>. This would exhaust the internal limit of 100 iterations and find a 
 * ridiculous result near 1e-10. Therefore, inflection points should be avoided.
 * <p>
 * <b>Performance:</b> Using the annuity equation, e.g., approx. 90,000 interest rates per 
 * second can be solved on a 1.5GHz CPU, 8 significant digits guaranteed.
 * <p>
 * <b>Internals:</b> Newton's approach is quite simple from the mathematical concept
 * but a bit tricky to program. In this version there are eight exit points from 
 * the main loop and six different end conditions. This implementation has been developed 
 * to achieve maximum accuracy without sacrificing performance. This is done through a 
 * strategy which could be called "convergence watching". Just keep in mind that loss 
 * of convergence is one of the successful end conditions here.
 * <p>
 * Usually, well-behaved functions will need 10 to 20 vertex points for a root solution
 * at maximum achievable accuracy. There is no option provided to speed up the search
 * procedure through a more generous tolerance because the usage of tolerance thresholds
 * together with exponential equations simply would not work reliable.
 * <p>
 * The source includes implementation comments at full length.
 *
 * @author Christoph Bodner
 * @version 1.01
 */

public class NewtonRoot {
    
    public enum EndCondition {
        /** Success */
        DIRECT_HIT,
        
        /** Success */
        INTERVAL_COLLAPSED,
        
        /** Success */
        CONVERGENCE_LOST,
        
        /** Error: bad conditioned f(x) */
        TOO_MANY_ITERATIONS,
        
        /** Error: function seems to run x-parallel */
        RUNNING_HORIZONTAL, 

        /** Error: f(x) returned NaN, probably a division through zero */
        NOT_A_NUMBER
    }
    
    private final static int MAX_POINTS = 100; 
    private final static double MINIMUM_DX = 1e-15; // only for special cases!
    private EndCondition status;
    private double x, y, interval;
    private int nPoints;
    
    public NewtonRoot() {
    }
    
    /**
     * Solve <code>x</code> for <code>f(x) = 0</code>
     * <p>
     * @param func
     *     function to be called, <code>y = func.f(x)</code>
     * @param xGuess
     *     start value for <code>x</code>, should be a reasonable estimation
     * @param deltaX
     *     the small interval to be used for pseudo-derivations <code>f'(x)</code>,
     *     e.g., 1e-10. It is kept constant over the entire search procedure.
     *     Unplausible small or negative values are silently corrected to an internal
     *     <code>MINIMUM_DX</code>. A too small <code>deltaX</code> may lead to
     *     error status <code>RUNNING_HORIZONTAL</code>.
     * @return
     *     <code>true</code> on success when a result has been found or 
     *     <code>false</code> on error.
     * <p>
     * Use {@linkplain #getX()} for obtaining the result value,
     * {@linkplain #getStatus()} for evaluating the end condition.
     */
    public boolean solve(FunctionOfX func, double xGuess, double deltaX) {
        final double dx = Math.abs(Math.max(deltaX, MINIMUM_DX)); // plausibility enforced
        final int maxPoints = Math.abs(Math.max(20, MAX_POINTS));
        double x1, y1 = 0., x2 = 0., y2 = 0., dy;
        double x1h1 = 0.; // x1 on history level 1 (the last one)
        double x1h2 = 0.; // x1 on history level 2 (the last but one)
        x1 = xGuess;
        nPoints = 0;
        for (;;) {
            if (nPoints >= maxPoints) {
                /*
                 * This end condition serves as some kind of emergency brake and
                 * prevents ill-behaved functions from causing an endless loop.
                 *
                 * Certain functions are simply "bad conditioned" for any root 
                 * search approach. Or deltaX was unsuitable.
                 */
                interval = Math.abs(x1 - x1h1);
                x = x1;
                y = y1;
                status = EndCondition.TOO_MANY_ITERATIONS;
                return false;
            }
            if (nPoints >= 6 && x1 == x1h1) {
                /*
                 * The current extrapolation point is exactly the last one.
                 */
                interval = 0.;
                x = x1;
                y = y1;
                status = EndCondition.INTERVAL_COLLAPSED;
                return true;
            }
            if (nPoints >= 6 && Math.abs(x1 - x1h1) >= Math.abs(x1h1 - x1h2)) {
                /*
                 * The current extrapolation point has ceased to converge.
                 *
                 * Most probably the nature of numerical arithmetics has
                 * caused a loss of significance at such a degree that a
                 * better result of f(x) = 0 is unlikely to come.
                 *
                 * The best result we know is the last convergence point 
                 * on the history stack (x1h1, y1) even if (x2, y2) might
                 * be better---sometimes by chance (and then only about dx).
                 *
                 * By the way, this condition also handles the funny case
                 * x1 == x1h2, i.e., the extrapolation point equals the last
                 * but one vertex which actually happened in tests. If not 
                 * detected, the alternating sequence <x1, x1h2, x1, x1h2,...>
                 * would run endless.
                 *
                 * Notice that y1 has not yet been overwritten in this cycle 
                 * so far.
                 */
                interval = Math.abs(x1h1 - x1h2);
                x = x1h1;
                y = y1;  
                status = EndCondition.CONVERGENCE_LOST;
                return true;
            }
            y1 = func.f(x1);
            nPoints++;
            if (Double.isNaN(y1)) {
                /*
                 * Game over.
                 */
                interval = Double.NaN;
                x = x1;
                y = y1;
                status = EndCondition.NOT_A_NUMBER;
                return false;
            }
            if (nPoints >= 2 && y1 == 0.) {
                /*
                 * The regular exit point: f(x) = 0
                 *
                 * If xGuess was already the result we could stop here. 
                 * And in case of a flat line, however, we prefer the 
                 * appropriate status, therefore nPoints >= 2.
                 */
                interval = 0.;
                x = x1;
                y = y1;
                status = EndCondition.DIRECT_HIT;
                return true;
            }
            /*
             * To compute x2 for pseudo-derivations we flee from zero,
             * therefore
             *     a) x2 != 0
             *     b) x2 never comes dangerously close to zero
             *     c) a sign change between x1 and x2 never happens.
             *
             * Assumes dx > 0 which has already been asserted above.
             */
            if (x1 >= 0.)
                x2 = x1 + dx;
            else
                x2 = x1 - dx;
            y2 = func.f(x2);
            nPoints++;
            if (Double.isNaN(y2)) {
                /*
                 * Game over.
                 */
                interval = Double.NaN;
                x = x2;
                y = y2;
                status = EndCondition.NOT_A_NUMBER;
                return false;
            }
            dy = y2 - y1;
            if (nPoints >= 3 && y2 == 0.) {
                /*
                 * Some constellations prefer this exit point.
                 */
                interval = 0.;
                x = x2;
                y = y2;
                status = EndCondition.DIRECT_HIT;
                return true;
            }
            if (dy == 0.) {
                /*
                 * Maybe the function graph runs really x-parallel, or
                 * floating point arithmetics has led to an enormous loss
                 * of significance. Eventually, deltaX was too small.
                 */
                interval = dx;
                x = x1;
                y = y1;
                status = EndCondition.RUNNING_HORIZONTAL;
                return false;
            }
            /*
             * Notice that (x2, y2) is not part of convergence watching,
             * the only purpose of each (x2, y2) is to provide pseudo-
             * derivations and new x1-vertices. Furthermore, the "flee
             * from zero"-rule, see above, can make x2 jumping around x1
             * (from left to right and vice versa) as the series proceeds.
             *
             * Pushing only x1 on the history stack, maintaining two levels:
             */
            x1h2 = x1h1;
            x1h1 = x1;
            /*
             * Newton's "regula falsi", the secant rule:
             *
             * The intersection of extrapolation line and x-axis becomes the
             * next vertex point to be processed. dy != 0 asserted above.
             */
            x1 = x1 - y2 * ((x2 - x1) / dy); 
            /*
             * If now x1 == 0, we assume that f(0) will be defined.
             * We can only guarantee x2 != 0 in our attempt to support
             * an undefined-f(0)-constraint.
             */
        }
    }
    
    /**
     * @return The status of the end condition of last run of <code>solve()</code>.
     * <br>
     * <code>DIRECT_HIT</code>, 
     * <code>INTERVAL_COLLAPSED</code>, and
     * <code>CONVERGENCE_LOST</code> 
     * correspond to return value true of <code>solve()</code>.
     * <br>
     * All other end conditions are hopefully self-explaining errors.
     */
    public EndCondition getStatus() {
        return status;
    }
    
    /**
     * @return The resulting <code>x</code> where <code>f(x) = 0</code> found by
     * last successfull <code>solve()</code> or the last value in case of error.
     */
    public double getX() {
        return x;
    }
    
    /**
     * @return <code>y = f(x)</code>, ideally <code>0</code>, found by
     * last successfull <code>solve()</code> or the last value in case of error.
     */
    public double getY() {
        return y;
    }
    
    /**
     * @return
     * The interval length between the last two converging vertex-points on the
     * x-axis.
     * <br>
     * <code>DIRECT_HIT</code> implies <code>0</code>, <code>Double.NaN</code> 
     * corresponds to the appropriate error condition.
     */
    public double getInterval() {
        return interval;
    }
        
    /**
     * @return Number of points, i.e., vertices, needed by <code>solve()</code>,
     * a counter how often <code>f(x)</code> was called.
     * <p>
     * The special case where <code>xGuess</code> is already the result is not handled 
     * the fastest way: it requires 2 points to ensure the success status or to figure out 
     * a flat line.
     */
    public int getNPoints() {
        return nPoints;
    }
    
}
