//    This program is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//    Copyright (c) by Christoph Bodner, 2007

package jinterestrate;

/**
 * This class provides financial calculations according to the equation:
 * 
 * <pre>
 * +----------------------------------------------------------------------------+
 * |                                                           nper             |
 * |                nper                             (1 + rate)    - 1          |
 * | pv * (1 + rate)     + pmt * (1 + rate * type) * ----------------- + fv = 0 |
 * |                                                         rate               |
 * +----------------------------------------------------------------------------+
 *  rate : interest rate per period
 *  nper : total number of payment periods per an annuity
 *  pmt  : payment made each period, cannot change over the life of an annuity
 *  pv   : present value (in German: Barwert)
 *  fv   : future value
 *  type : indicates when payments are due:
 *           0 = at the end of the period (postnumerando)
 *           1 = at the beginning of the period (praenumerando)
 * Reference: MS-Excel(TM) help file, OpenOffice documentation
 * </pre>
 * 
 * Symbols and public method names rate(), nper(), pmt(), pv(), fv() are
 * intentionally left as they appear in standard spreadsheet software (English
 * versions), although this may seem somewhat strange compared to Java naming
 * conventions. <br>
 * <br>
 * <b>Hints:</b> <br>
 * a) Always use <code>rateInPercent / 100.</code> <br>
 * b) The money you give the bank has a different sign as the money you get from
 * the bank, otherwise the formula would not work. <br>
 * c) Negative values of variables are fully spreadsheet-compatible as allowed
 * by the equation, even if rate < 0 and nper < 0 may not seem of practical use.
 * <p>
 * <b>Limitations:</b> <br>
 * The accuracy of rate() and nper() is only guaranteed to 8 digits. Testing of
 * billion-sized values showed an actual accuracy of 12 digits with pv(), fv(),
 * and pmt().
 * <p>
 * <b>Discussion:</b> <br>
 * Unfortunately, Java doesn't support the 80-bit extended floating point data
 * type known as "long double" in C/C++. Otherwise we could expect approx. 4
 * digits more in accuracy and a full compliance with spreedsheet functions.
 * But: Who needs more than 5 significant digits in an interest rate?
 * <p>
 * For deficit financing, however, <code>java.math.BigDecimal</code> might be
 * the right tool.
 * 
 * @author Christoph Bodner
 * @version 1.01
 */

public class InterestRate {

	public final static double POST_NUMERANDO = 0d;
	public final static double PRAE_NUMERANDO = 1d;

	private final static double DELTA_X = 1e-10; // Don't change this!

	private NewtonRoot rootForRate, rootForNPeriods;

	public InterestRate() {
		rootForRate = new NewtonRoot();
		rootForNPeriods = new NewtonRoot();
	}

	/*
	 * Introducing helper variable h:
	 *
	 *                                       h - 1
	 *    pv * h + pmt * (1 + rate * type) * ----- + fv = 0
	 *                                       rate
	 *
	 */
	private static double computeH(double rate, double nPeriods) {
		return Math.pow(1. + rate, nPeriods);
	}

	private static double computeEquation(double rate, double nper, double pmt,
			double pv, double fv, double type) {
		/*
		 * Assumes that rate != 0 has already been asserted by the caller of
		 * this function.
		 */
		double h = computeH(rate, nper);
		return pv * h + pmt * (1. + rate * type) * ((h - 1.) / rate) + fv;
	}

	private class RateFunction implements FunctionOfX {
		private double nper, pmt, pv, fv, type;

		public RateFunction(double nper, double pmt, double pv, double fv,
				double type) {
			this.nper = nper;
			this.pmt = pmt;
			this.pv = pv;
			this.fv = fv;
			this.type = type;
		}

		public double f(double rate) {
			return computeEquation(rate, nper, pmt, pv, fv, type);
		}
	}

	private class NPeriodsFunction implements FunctionOfX {
		private double rate, pmt, pv, fv, type;

		public NPeriodsFunction(double rate, double pmt, double pv, double fv,
				double type) {
			this.rate = rate;
			this.pmt = pmt;
			this.pv = pv;
			this.fv = fv;
			this.type = type;
		}

		public double f(double nper) {
			return computeEquation(rate, nper, pmt, pv, fv, type);
		}
	}

	/**
	 * @return the present value
	 */
	public static double pv(double rate, double nper, double pmt, double fv,
			double type) throws InterestRateException {

		double h = computeH(rate, nper);
		if (h == 0. || rate == 0.)
			throw new InterestRateException("present value not defined for specified arguments rate, nper");
		else
			return -(pmt * (1. + rate * type) * ((h - 1.) / rate) + fv) / h;
	}

	/**
	 * @return the future value
	 */
	public static double fv(double rate, double nper, double pmt, double pv,
			double type) throws InterestRateException {

		double h = computeH(rate, nper);
		if (rate == 0.)
			throw new InterestRateException("future value not defined for rate = 0.0");
		else
			return -(pv * h + pmt * (1. + rate * type) * ((h - 1.) / rate));
	}

	/**
	 * Calculates the loan repayment amount for each period of the loan
	 * @param rate	The interest rate to be used (normalized to the period unit in use. ie. annual, monthly, etc) 
	 * @param nper The number of periods (ie. term of the loan)
	 * @param pv The principal amount
	 * @param fv The amount to be remaining at the end of the term. ie. residual amount 
	 * @param type A flag signalling whether repayments occur in-advance (value = 1) or in-arrears (value = 0)
	 * @return
	 * @throws InterestRateException
	 */
	public static double pmt(double rate, double nper, double pv, double fv,
			double type) throws InterestRateException {

		double h = computeH(rate, nper);
		if (rate != 0.) {
			double q = (1. + rate * type) * ((h - 1.) / rate);
			if (q != 0.)
				return -((pv * h + fv) / q);
		}
		throw new InterestRateException(
				"payment not defined for specified arguments");
	}

	/**
	 * @return the interest rate
	 */
	public double rate(double nper, double pmt, double pv, double fv,
			double type) throws InterestRateException {

		FunctionOfX f = new RateFunction(nper, pmt, pv, fv, type);
		/*
		 * Attention: Don't use xGuess = 0.0 for rate! Instead, e.g., a start
		 * value of 0.01 works fine under any circumstances here.
		 */
		if (rootForRate.solve(f, 0.01, DELTA_X))
			return rootForRate.getX();
		else
			throw new InterestRateException(
					"rate: no solution found for specified arguments, "
							+ rootForRate.getStatus());
	}

	/**
	 * @return the number of periods
	 */
	public double nper(double rate, double pmt, double pv, double fv,
			double type) throws InterestRateException {

		FunctionOfX f = new NPeriodsFunction(rate, pmt, pv, fv, type);
		if (rootForNPeriods.solve(f, 1.0, DELTA_X))
			return rootForNPeriods.getX();
		else
			throw new InterestRateException(
					"nper: no solution found for specified arguments, "
							+ rootForNPeriods.getStatus());
	}
}