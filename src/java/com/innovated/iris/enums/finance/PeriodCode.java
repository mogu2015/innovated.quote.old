/**
 * 
 */
package com.innovated.iris.enums.finance;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;

/**
 * @author tamati
 *
 */
public enum PeriodCode {
	OPENING(0, "Opening", "System Journal Posting Type"),
	NORMAL (1, "Normal",  "General Journal Posting Type"),
	CLOSING(2, "Closing", "Customer Invoice Journal Posting Type");
            
    private static final Map<Integer,PeriodCode> lookup = new HashMap<Integer,PeriodCode>();
	static {
         for (PeriodCode s : EnumSet.allOf(PeriodCode.class))
              lookup.put(s.getCode(), s);
    }

    private Integer code;
    private String name;
    private String description;

    private PeriodCode(Integer code, String name, String description) {
         this.code = code;
         this.name = name;
         this.description = description;
    }

    public Integer getCode()       { return code; }
    public String getName()        { return name; }
    public String getDescription() { return description; }

    public static PeriodCode get(Integer code) { 
         return lookup.get(code); 
    }
    
    @Override
    public String toString() {
    	return name;
    }
}
