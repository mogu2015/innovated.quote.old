/**
 * 
 */
package com.innovated.iris.enums.finance;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;

/**
 * @author tamati
 *
 */
public enum DimensionType {
	ENTITY  (0, "Entity",  "Entity ID type dimension"),
	ASSET   (1, "Asset",   "Asset ID type dimension"),
	ACTIVITY(2, "Activty", "Activity type dimension");
	
    private static final Map<Integer,DimensionType> lookup = new HashMap<Integer,DimensionType>();
	static {
         for (DimensionType s : EnumSet.allOf(DimensionType.class))
              lookup.put(s.getCode(), s);
    }

    private Integer code;
    private String name;
    private String description;

    private DimensionType(Integer code, String name, String description) {
         this.code = code;
         this.name = name;
         this.description = description;
    }

    public Integer getCode()       { return code; }
    public String getName()        { return name; }
    public String getDescription() { return description; }

    public static DimensionType get(Integer code) { 
         return lookup.get(code); 
    }
    
    @Override
    public String toString() {
    	return name;
    }
}
