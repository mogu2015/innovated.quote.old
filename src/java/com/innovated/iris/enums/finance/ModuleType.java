/**
 * 
 */
package com.innovated.iris.enums.finance;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;

/**
 * @author tamati
 *
 */
public enum ModuleType {
	SYSTEM_JOURNAL  ( 1, "SYS", "System Journal", "System Journal Posting Type"),
	GENERAL_JOURNAL (10, "GJ",  "General Journal", "General Journal Posting Type"),
	CUST_INV_JOURNAL(20, "CIJ", "Customer Invoice Journal", "Customer Invoice Journal Posting Type"),
	CUST_PAY_JOURNAL(30, "CPY", "Customer Payment Journal", "Customer Payment Journal Posting Type"),
	VEND_INV_JOURNAL(40, "VIJ", "Vendor Invoice Journal", "Vendor Invoice Journal Posting Type"),
	VEND_PAY_JOURNAL(50, "VPJ", "Vendor Payment Journal", "Vendor Payment Journal Posting Type");
            
    private static final Map<Integer,ModuleType> lookup = new HashMap<Integer,ModuleType>();
	static {
         for (ModuleType s : EnumSet.allOf(ModuleType.class))
              lookup.put(s.getCode(), s);
    }

    private Integer code;
    private String key;
    private String text;
    private String description;

    private ModuleType(Integer code, String key, String text, String description) {
         this.code = code;
         this.key = key;
         this.text = text;
         this.description = description;
    }

    public Integer getCode()       { return code; }
    public String getText()        { return text; }
    public String getKey()         { return key; }
    public String getDescription() { return description; }

    public static ModuleType get(Integer code) { 
         return lookup.get(code); 
    }
    
    @Override
    public String toString() {
    	return text;
    }
}
