/**
 * 
 */
package com.innovated.iris.enums.finance;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;

/**
 * @author tamati
 *
 */
public enum LedgerAccountType {
	    ASSET(0, "Asset", "Asset Balance type account"),
	LIABILITY(1, "Liability",  "Liability Balance type account"),
	  REVENUE(2, "Revenue", "Revenue (income) Profit & Loss type account"),
	     COST(3, "Cost",  "Cost (expense) Profit & Loss type account"),
	   HEADER(9, "Header",  "Heading type account (non-posting)"),
	   TOTAL(10, "Total", "Sum/Total type account (non-posting)");
            
    private static final Map<Integer,LedgerAccountType> lookup = new HashMap<Integer,LedgerAccountType>();
	static {
         for (LedgerAccountType s : EnumSet.allOf(LedgerAccountType.class))
              lookup.put(s.getCode(), s);
    }

    private Integer code;
    private String name;
    private String description;

    private LedgerAccountType(Integer code, String name, String description) {
         this.code = code;
         this.name = name;
         this.description = description;
    }

    public Integer getCode()       { return code; }
    public String getName()        { return name; }
    public String getDescription() { return description; }

    public static LedgerAccountType get(Integer code) { 
         return lookup.get(code); 
    }
    
    @Override
    public String toString() {
    	return name;
    }
}
