package com.innovated.iris.enums;

import java.util.EnumSet;
import java.util.Map;
import java.util.TreeMap;

public enum HomeOwnershipStatus {
	OWNER   (0, "Home Owner"),  
	MORTGAGE(1, "Mortgage"),
	RENTING (2, "Renting");

	private static final Map<Integer, HomeOwnershipStatus> lookup = new TreeMap<Integer, HomeOwnershipStatus>();

	static {
		for (HomeOwnershipStatus s : EnumSet.allOf(HomeOwnershipStatus.class))
			lookup.put(s.getCode(), s);
	}

	private int code;
	private String name;

	private HomeOwnershipStatus(int code, String name) {
		this.code = code;
		this.name = name;
	}

	public int getCode() {
		return code;
	}

	public String getName() {
		return name;
	}

	public static HomeOwnershipStatus get(int code) {
		return lookup.get(code);
	}

	@Override
	public String toString() {
		return name;
	}
}
