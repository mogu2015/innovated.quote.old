package com.innovated.iris.enums.application;

import java.util.EnumSet;
import java.util.Map;
import java.util.TreeMap;

public enum FinanceApplicationLiabilityType {
	BANK_LOAN  ( 0, "Bank Loan"),  
	MORTGAGE   ( 5, "Mortgage"),
	LOAN       (10, "Loan"),
	OVERDRAFT  (15, "Overdraft"),
	CREDIT_CARD(20, "Credit Card(s)"),
	OTHER      (25, "Other Liability");

	private static final Map<Integer, FinanceApplicationLiabilityType> lookup = new TreeMap<Integer, FinanceApplicationLiabilityType>();

	static {
		for (FinanceApplicationLiabilityType s : EnumSet.allOf(FinanceApplicationLiabilityType.class))
			lookup.put(s.getCode(), s);
	}

	private int code;
	private String name;

	private FinanceApplicationLiabilityType(int code, String name) {
		this.code = code;
		this.name = name;
	}

	public int getCode() {
		return code;
	}

	public String getName() {
		return name;
	}

	public static FinanceApplicationLiabilityType get(int code) {
		return lookup.get(code);
	}

	@Override
	public String toString() {
		return name;
	}
}
