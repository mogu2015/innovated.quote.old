package com.innovated.iris.enums.application;

import java.util.EnumSet;
import java.util.Map;
import java.util.TreeMap;

public enum FinanceApplicationIncomeType {
	NET_SALARY   ( 0, "Net Salary/Pay (after tax)"),  
	CAR_ALLOWANCE( 5, "Car Allowance"),
	INVESTMENT   (10, "Investment/Rental Income"),
	OTHER        (15, "Other Income");

	private static final Map<Integer, FinanceApplicationIncomeType> lookup = new TreeMap<Integer, FinanceApplicationIncomeType>();

	static {
		for (FinanceApplicationIncomeType s : EnumSet.allOf(FinanceApplicationIncomeType.class)) {
			lookup.put(s.getCode(), s);
		}
		
	}

	private int code;
	private String name;

	private FinanceApplicationIncomeType(int code, String name) {
		this.code = code;
		this.name = name;
	}

	public int getCode() {
		return code;
	}

	public String getName() {
		return name;
	}

	public static FinanceApplicationIncomeType get(int code) {
		return lookup.get(code);
	}

	@Override
	public String toString() {
		return name;
	}
}
