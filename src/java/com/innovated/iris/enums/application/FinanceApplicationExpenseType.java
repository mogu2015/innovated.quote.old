package com.innovated.iris.enums.application;

import java.util.EnumSet;
import java.util.Map;
import java.util.TreeMap;

public enum FinanceApplicationExpenseType {
	MORTGAGE_RENT( 0, "Mortgage Repayments/Rent"),  
	HOUSEHOLD    ( 5, "Household Expenses"),
	CREDIT_CARD  (10, "Credit Card(s)"),
	MOTOR_VEHICLE(15, "Car Payments"),
	OTHER        (20, "Other Expenses");

	private static final Map<Integer, FinanceApplicationExpenseType> lookup = new TreeMap<Integer, FinanceApplicationExpenseType>();

	static {
		for (FinanceApplicationExpenseType s : EnumSet.allOf(FinanceApplicationExpenseType.class))
			lookup.put(s.getCode(), s);
	}

	private int code;
	private String name;

	private FinanceApplicationExpenseType(int code, String name) {
		this.code = code;
		this.name = name;
	}

	public int getCode() {
		return code;
	}

	public String getName() {
		return name;
	}

	public static FinanceApplicationExpenseType get(int code) {
		return lookup.get(code);
	}

	@Override
	public String toString() {
		return name;
	}
}
