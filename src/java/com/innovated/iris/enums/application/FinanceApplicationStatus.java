package com.innovated.iris.enums.application;

import java.util.EnumSet;
import java.util.Map;
import java.util.TreeMap;

public enum FinanceApplicationStatus {
	CANCELLED   ( 0, "Cancelled"),  
	CREATED     ( 5, "Created"),
	COMPLETED   (10, "Submitted"),
	UNDER_REVIEW(15, "Under Review"),
	PENDING     (20, "With Financier"),
	REJECTED    (25, "Rejected"),
	APPROVED    (30, "Approved");

	private static final Map<Integer, FinanceApplicationStatus> lookup = new TreeMap<Integer, FinanceApplicationStatus>();

	static {
		for (FinanceApplicationStatus s : EnumSet.allOf(FinanceApplicationStatus.class))
			lookup.put(s.getCode(), s);
	}

	private int code;
	private String name;

	private FinanceApplicationStatus(int code, String name) {
		this.code = code;
		this.name = name;
	}

	public int getCode() {
		return code;
	}

	public String getName() {
		return name;
	}

	public static FinanceApplicationStatus get(int code) {
		return lookup.get(code);
	}

	@Override
	public String toString() {
		return name;
	}
}
