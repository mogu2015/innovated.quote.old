package com.innovated.iris.enums.application;

import java.util.EnumSet;
import java.util.Map;
import java.util.TreeMap;

public enum FinanceApplicationAssetType {
	CASH_DEPOSIT    ( 0, "Cash/Deposits"),  
	PROPERTY        ( 5, "Property"),
	INVESTMENT      (10, "Investment"),
	MOTOR_VEHICLE   (15, "Motor Vehicle"),
	PERSONAL_EFFECTS(20, "Personal Effects"),
	SUPERANNUATION  (25, "Superannuation"),
	SHARES_BONDS    (30, "Shares/Bonds"),
	OTHER           (35, "Other Asset");

	private static final Map<Integer, FinanceApplicationAssetType> lookup = new TreeMap<Integer, FinanceApplicationAssetType>();

	static {
		for (FinanceApplicationAssetType s : EnumSet.allOf(FinanceApplicationAssetType.class))
			lookup.put(s.getCode(), s);
	}

	private int code;
	private String name;

	private FinanceApplicationAssetType(int code, String name) {
		this.code = code;
		this.name = name;
	}

	public int getCode() {
		return code;
	}

	public String getName() {
		return name;
	}

	public static FinanceApplicationAssetType get(int code) {
		return lookup.get(code);
	}

	@Override
	public String toString() {
		return name;
	}
}
