package com.innovated.iris.enums;

import java.util.EnumSet;
import java.util.Map;
import java.util.TreeMap;

public enum QuoteStatus {
    QUOTE_CANCELLED(0, "Cancelled"),
    QUOTE_CREATED(5, "Create"),
    QUOTE_CONFIRMED(7, "Select"),
    QUOTE_ACTUAL(9, "Submit"),
    QUOTE_ACCEPTED(10, "Accept"),
    QUOTE_APPROVED(15, "Approve"),
    QUOTE_DOCUMENTATION(17, "Documentation"),
    QUOTE_SETTLED(18, "Settled");
//    QUOTE_ACTIVATED(20, "Activated");

    private static final Map<Integer, QuoteStatus> lookup = new TreeMap<Integer, QuoteStatus>();

    static {
        for (QuoteStatus s : EnumSet.allOf(QuoteStatus.class))
            lookup.put(s.getCode(), s);
    }

    private int code;
    private String name;

    private QuoteStatus(int code, String name) {
        this.code = code;
        this.name = name;
    }

    public int getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    public static QuoteStatus get(int code) {
        return lookup.get(code);
    }

    @Override
    public String toString() {
        return name;
    }
}
