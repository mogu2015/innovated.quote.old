/**
 * 
 */
package com.innovated.iris.enums;

import java.util.EnumSet;
import java.util.Map;
import java.util.TreeMap;

/**
 * @author tamati
 *
 */
public enum Gender {
	MALE   ( 1, "Male",    "M"),
	FEMALE ( 2, "Female",  "F"),
	UNKNOWN( 4, "Unknown", "X");
	
	private static final Map<Integer,Gender> lookup = new TreeMap<Integer,Gender>();
	static {
	    for (Gender s : EnumSet.allOf(Gender.class))
	         lookup.put(s.getCode(), s);
	}
	
	private int code;
	private String name;
	private String abbr;
	
	private Gender(int code, String name, String abbr) {
	    this.code = code;
	    this.name = name;
	    this.abbr = abbr;
	}
	
	public int getCode() { return code; }
	public String getName() { return name; }
	public String getAbbr() { return abbr; }
	
	public static Gender get(int code) { 
	    return lookup.get(code); 
	}
	
	@Override
	public String toString() {
		return name;
	}
}
