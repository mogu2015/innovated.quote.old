package com.innovated.iris.util;

import junit.framework.TestCase;

public class BenefitShareTests extends TestCase {

	public void testCalculateEmployerShare() {
		BenefitShareCalculator b = new BenefitShareCalculator();
		double share = b.calculateEmployerShare(107644, 26, 9094.8, 0.50, 110.0);
		assertEquals("Share calculated incorrectly!", 2411.76, share);
	}
	
	/*public void testCalculateEmployerShare2() {
		BenefitShareCalculator b = new BenefitShareCalculator();
		double share = b.calculateEmployerShare2(107644, 26, 9689.42, 0.50, 110.0);
		assertEquals("Share calculated incorrectly!", 2411.76, share);
	}*/
}
