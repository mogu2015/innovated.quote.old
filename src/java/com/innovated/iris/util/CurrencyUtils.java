package com.innovated.iris.util;

import com.domainlanguage.money.Money;

class CurrencyUtils {
    public static double truncateToCents(double amt) {
        return Math.rint(amt * 100) / 100;
    }

    public static double roundNearestDollar(double amt) {
        return Math.rint(amt);
    }

    public static double roundNearestCent(double amt) {
        return Math.rint(amt * 100) / 100;
    }

    public static double roundUpNearestDollar(double amt) {
        return Math.ceil(amt);
    }

    public static double roundUpNearestCent(double amt) {
        return Math.ceil(amt * 100) / 100;
    }

    public static Money roundUpNearestDollar(Money amt) {
        double a = amt.breachEncapsulationOfAmount().doubleValue();
        a = roundUpNearestDollar(a);
        return Money.valueOf(a, amt.breachEncapsulationOfCurrency());
    }
}
