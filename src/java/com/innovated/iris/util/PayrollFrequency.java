/**
 *
 */
package com.innovated.iris.util;

import java.util.Comparator;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

/**
 * @author tamati
 */
public enum PayrollFrequency {
    //	ANNUAL     ( 1, "Annually", "Year"),
//	SEMI_ANNUAL( 2, "Semi-Annually", "Half-Year"),
//	QUARTERLY  ( 4, "Quarterly", "Quarter"),

    WEEKLY(52, "Weekly", "Week"),
    BIMONTHLY(24, "Bi-Monthly", "Bi-Month"),
    FORTNIGHTLY(26, "Fortnightly", "Fortnight"),
    MONTHLY(12, "Monthly", "Month");

    private static final Map<Integer, PayrollFrequency> lookup;

    static {
        /*EnumSet<PayrollFrequency> freqs = EnumSet.allOf(PayrollFrequency.class);
        lookup = new HashMap<Integer, PayrollFrequency>();
		for (PayrollFrequency s : freqs)
	         lookup.put(s.getCode(), s);*/

        lookup = new HashMap<Integer, PayrollFrequency>();
        PayrollFrequency[] freqs = PayrollFrequency.values();
        for (int i = 0; i < freqs.length; i++) {
            lookup.put(freqs[i].getCode(), freqs[i]);
        }
        /*for (int i=(freqs.length-1); i>=0; i--) {
            lookup.put(freqs[i].getCode(), freqs[i]);
		}*/
    }

    private Integer code;
    private String name;
    private String singular;

    private PayrollFrequency(Integer code, String name, String singular) {
        this.code = code;
        this.name = name;
        this.singular = singular;
    }

    public Integer getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    public String getSingular() {
        return singular;
    }

    public static PayrollFrequency get(int code) {
        return lookup.get(code);
    }

    @Override
    public String toString() {
        return name;
    }
}
