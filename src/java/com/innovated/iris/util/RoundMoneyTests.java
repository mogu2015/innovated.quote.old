package com.innovated.iris.util;

import com.domainlanguage.money.Money;
import com.innovated.iris.server.fbt.FbtUtils;

import junit.framework.TestCase;

public class RoundMoneyTests extends TestCase {

	public void testRoundUpNearestDollarMoney() {
		Money amt = Money.valueOf(14.67, FbtUtils.DEFAULT_CURRENCY);
		assertEquals("money amts not equal!", Money.valueOf(15.00, FbtUtils.DEFAULT_CURRENCY), CurrencyUtils.roundUpNearestDollar(amt));
	}
}
