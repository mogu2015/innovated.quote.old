/**
 *
 */
package com.innovated.iris.util;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;

public enum EmployerType {

    BUCOGO(1, "Business / Company / Government"),
    ASANDNSW(2, "NSW Health"),
    PUBLICNSW(3, "Hospitals & Ambulance (Public & non-Profit)"),
    REBATABLE(4, "Rebatable"),
    PBI0001(5, "Public Benevolent Institution");

    private static final Map<Integer, EmployerType> lookup = new HashMap<Integer, EmployerType>();

    static {
        for (EmployerType s : EnumSet.allOf(EmployerType.class))
            lookup.put(s.getCode(), s);
    }

    private int code;
    private String name;

    private EmployerType(int code, String name) {
        this.code = code;
        this.name = name;
    }

    public int getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    public static EmployerType get(int code) {
        return lookup.get(code);
    }

    @Override
    public String toString() {
        return name;
    }
}
