/**
 * 
 */
package com.innovated.iris.util;

/**
 * @author tamati
 *
 */
public class BenefitShareCalculator {
	
	public double calculateEmployerShare(double grossSalary, int payFreq, double packagedAmt,
			double emplShareRate, double emplAdminFee) {
		double emplShareAmt = 0.0;
		double taxPayable = lookupTaxPayable(grossSalary, true);
		double taxSaving = 0.0;
		
		do {
			double tmpTax = lookupTaxPayable(grossSalary - emplShareAmt - packagedAmt - emplAdminFee, true);
			taxSaving = taxPayable - tmpTax;
			emplShareAmt = emplShareAmt + (taxSaving * emplShareRate);
			taxPayable = tmpTax;
		} while (taxSaving > 0.001);	// doesn't need to be ZERO, any significant figures greater than 1 cent are enough ;)
		
		return CurrencyUtils.roundUpNearestCent(emplShareAmt / payFreq) * payFreq;
	}
	
	public double calculateEmployerShare2(double grossSalary, int payFreq, double packagedAmt,
			double emplShareRate, double emplAdminFee) {
		double emplShareAmt = 0.0;
		double taxPayable = lookupTaxPayable(grossSalary, true);
		double taxSaving = 0.0;
		
		do {
			double tmpTax = lookupTaxPayable(grossSalary - emplShareAmt - packagedAmt - emplAdminFee, true);
			taxSaving = taxPayable - tmpTax;
			emplShareAmt = emplShareAmt + (taxSaving * emplShareRate);
			taxPayable = tmpTax;
		} while (taxSaving > 0.001);	// doesn't need to be ZERO, any significant figures greater than 1 cent are enough ;)
		
		return CurrencyUtils.roundUpNearestCent(emplShareAmt / payFreq) * payFreq;
	}
	
	private double calculateTaxSaving(double grossSalary, int payFreq, double packagedAmt,
			double emplShareRate, double emplAdminFee) {
		return 0;
	}
	
	private double lookupTaxPayable(double income, boolean includeMcareLevy) {
		double mcareLevy = (includeMcareLevy) ? income * 0.015 : 0.0;
		double tax = 0.0;
		if (income >= 0 && income <= 6000) {
			//System.out.println("tax bracket = 0 - 6,000");
			tax = 0.0;
		} else if (income >= 6001 && income <= 35000) {
			//System.out.println("tax bracket = 6,001 - 35,000");
			tax = (income - 6000) * 0.15;
		} else if (income >= 35001 && income <= 80000) {
			//System.out.println("tax bracket = 35,001 - 80,000");
			tax = 4350 + ((income - 35000) * 0.30);
		} else if (income >= 80001 && income <= 180000) {
			//System.out.println("tax bracket = 80,001 - 180,000");
			tax = 17850 + ((income - 80000) * 0.38);
		} else {
			//System.out.println("tax bracket = Over 180,000");
			tax = 55850 + ((income - 180000) * 0.45);
		}
		System.out.println("tax payable = " + (tax + mcareLevy));
		return tax + mcareLevy;
	}
}
