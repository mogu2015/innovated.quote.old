/**
 * 
 */
package com.innovated.iris.services;

import java.util.List;

import com.innovated.iris.domain.Quote;

/**
 * @author tamati
 *
 */
public interface IQuoteService {
	public List retrieveBreakdown(Quote quote);
	public List retrieveTermsAndConditions(Quote quote);
}
