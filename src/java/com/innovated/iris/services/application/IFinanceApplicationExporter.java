/**
 * 
 */
package com.innovated.iris.services.application;

import com.innovated.iris.domain.application.FinanceApplication;

/**
 * @author tamati
 *
 */
public interface IFinanceApplicationExporter {
	public ExportResponse export(FinanceApplication application);
}
