/**
 * 
 */
package com.innovated.iris.services.application;

/**
 * @author tamati
 *
 */
public class ExportResponse {
	private boolean success;
	private Integer code;
	private String message;
	
	public ExportResponse(boolean success, Integer code, String message) {
		this.success = success;
		this.code = code;
		this.message = message;
	}
	
	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
	@Override
	public String toString() {
		return "[" + code + "]: " + message;
	}
}
