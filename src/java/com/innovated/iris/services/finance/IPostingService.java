/**
 * 
 */
package com.innovated.iris.services.finance;

import com.innovated.iris.enums.finance.PostingResult;

/**
 * @author tamati
 *
 */
public interface IPostingService<K> {
	public PostingResult validate(K object);
	public PostingResult post(K object);
}
