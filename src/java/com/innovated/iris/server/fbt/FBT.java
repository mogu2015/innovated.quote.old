/**
 * 
 */
package com.innovated.iris.server.fbt;

import java.util.Date;

import com.domainlanguage.money.Money;

/**
 * A basic class to exercise FBT functions
 * 
 * @author tamati
 */
public class FBT {
	private FbtRecord fbtRecord;
	private Date sessionDate;
	
	public FBT(FbtRecord fbtRecord, Date sessionDate) {
		this.fbtRecord = fbtRecord;
		this.sessionDate = sessionDate;
	}
	
	@SuppressWarnings("unused")
	private boolean checkSessionDateValid() {
		return this.fbtRecord.getFbtPeriod().includes(this.sessionDate);
	}
	
	private Money calculateTaxableValue() {
		return fbtRecord.getTvCalculator().calculate();
	}
	
	private double calculateGrossUpRate() {
		return fbtRecord.getGrossUpRate();
	}
	
	private double calculateFbtRate() {
		return fbtRecord.getFbtRate();
	}
	
	private Money calculateGrossTax() {
		/*
		 * GROSS_TAX = (TaxableValue x GrossUpRate x FBTRate)
		 */
		return calculateTaxableValue().times(calculateGrossUpRate()).times(calculateFbtRate());
	}
	
	private Money calculateFbtRebate() {
		Money rebate = FbtUtils.ZERO_MONEY_AMT;
		
		if (fbtRecord.isRebateable()) {
			rebate = fbtRecord.getRebateCalculator().calculate();
		}
		// else, return ZERO for the rebate amount
		
		return rebate;
	}
	
	public Money fbtPayable() {
		// [ FBT_Payable = Gross_Tax - FBT_Rebate ]
		return calculateGrossTax().minus(calculateFbtRebate());
	}
	
	public Money contributionPayable() {
		
		return null;
	}
}
