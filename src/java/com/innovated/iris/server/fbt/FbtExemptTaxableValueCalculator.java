/**
 * 
 */
package com.innovated.iris.server.fbt;

import com.domainlanguage.money.Money;

/**
 * @author tamati
 *
 */
public class FbtExemptTaxableValueCalculator implements ITaxableValue {

	/* (non-Javadoc)
	 * @see com.innovated.iris.server.fbt.ITaxableValue#calculate()
	 */
	public Money calculate() {
		return FbtUtils.ZERO_MONEY_AMT;
	}

}
