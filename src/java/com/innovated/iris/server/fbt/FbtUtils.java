/**
 * 
 */
package com.innovated.iris.server.fbt;

import java.math.BigDecimal;
import java.util.Currency;
import java.util.Date;

import org.joda.time.DateTime;
import org.joda.time.Days;

import com.domainlanguage.base.Ratio;
import com.domainlanguage.intervals.Interval;
import com.domainlanguage.money.Money;

/**
 * @author tamati
 *
 */
public class FbtUtils {
	public static final Currency DEFAULT_CURRENCY = Currency.getInstance("AUD");
	public static final int DEFAULT_ROUNDING = BigDecimal.ROUND_HALF_UP;
	public static final int GROSS_UP_SCALE = 4;
	public static final Money ZERO_MONEY_AMT = Money.valueOf(0.0, DEFAULT_CURRENCY);
	
	public static Ratio ratioInvert(Ratio ratio) {
		double rate = ratio.decimalValue(10, DEFAULT_ROUNDING).doubleValue();
		return Ratio.of(BigDecimal.valueOf(1.0 / rate));
	}
	
	public static int getDaysInDateInterval(Interval dateInterval) {
		Date start = ((Date) dateInterval.lowerLimit());
		Date end = ((Date) dateInterval.upperLimit());
		
		DateTime dt1 = new DateTime(start.getTime());
		DateTime dt2 = new DateTime(end.getTime());
		
		Days d = Days.daysBetween(dt1, dt2);
		return d.getDays() + 1;
	}
	
	public static int getDaysInCalendarYear(Date date) {
		return getDaysInDateInterval(getCalendarYear(date));
	}
	
	public static int getDaysInFinancialYear(Date date) {
		return getDaysInDateInterval(getFinancialYear(date));
	}
	
	public static int getDaysInFbtYear(Date date) {
		return getDaysInDateInterval(getFbtYear(date));
	}
	
	/**
	 * Given a date object, this method returns an <code>Interval</code> of <code>Date</code>'s
	 * defining the calendar year within which this <code>date</code> falls.
	 * @param date The date you want to check
	 * @return an <code>Interval</code> defining the calendar year the date falls within
	 */
	public static Interval getCalendarYear(Date date) {
		DateTime dt = new DateTime(date.getTime());
		DateTime dt1 = dt.withDayOfYear(1);
		DateTime dt2 = dt.dayOfYear().withMaximumValue();
		return Interval.closed(dt1.toDate(), dt2.toDate());
	}
	
	/**
	 * Given a date object, this method returns an <code>Interval</code> of <code>Date</code>'s
	 * defining the financial year within which this <code>date</code> falls.
	 * @param date The date you want to check
	 * @return an <code>Interval</code> defining the financial year the date falls within
	 */
	public static Interval getFinancialYear(Date date) {
		int startOffset = 0;
		int endOffset = 0;
		
		DateTime dt = new DateTime(date.getTime());
		if (dt.getMonthOfYear() < 7) {
			startOffset = -1;
			endOffset = 0;
		}
		else {
			startOffset = 0;
			endOffset = 1;
		}
		
		DateTime dt1 = dt.plusYears(startOffset).withMonthOfYear(7).dayOfMonth().withMinimumValue();
		DateTime dt2 = dt.plusYears(endOffset).withMonthOfYear(6).dayOfMonth().withMaximumValue();
		return Interval.closed(dt1.toDate(), dt2.toDate());
	}
	
	/**
	 * Given a date object, this method returns an <code>Interval</code> of <code>Date</code>'s
	 * defining the FBT year within which this <code>date</code> falls.
	 * @param date The date you want to check
	 * @return an <code>Interval</code> defining the FBT year the date falls within
	 */
	public static Interval getFbtYear(Date date) {
		int startOffset = 0;
		int endOffset = 0;
		
		DateTime dt = new DateTime(date.getTime());
		if (dt.getMonthOfYear() < 4) {
			startOffset = -1;
			endOffset = 0;
		}
		else {
			startOffset = 0;
			endOffset = 1;
		}
		
		DateTime dt1 = dt.plusYears(startOffset).withMonthOfYear(4).dayOfMonth().withMinimumValue();
		DateTime dt2 = dt.plusYears(endOffset).withMonthOfYear(3).dayOfMonth().withMaximumValue();
		return Interval.closed(dt1.toDate(), dt2.toDate());
	}
}
