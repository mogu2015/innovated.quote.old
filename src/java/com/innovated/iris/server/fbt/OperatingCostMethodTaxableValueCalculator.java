/**
 * 
 */
package com.innovated.iris.server.fbt;

import java.math.BigDecimal;

import com.domainlanguage.base.Ratio;
import com.domainlanguage.money.Money;

/**
 * @author tamati
 *
 */
public class OperatingCostMethodTaxableValueCalculator implements ITaxableValue {

	private Money operatingCosts;
	private Ratio privateUseRate;
	private Money contributions;
	
	public OperatingCostMethodTaxableValueCalculator(Money operatingCosts,
			Ratio privateUseRate, Money contributions) {
		this.operatingCosts = operatingCosts;
		this.privateUseRate = privateUseRate;
		this.contributions = contributions;
	}
	
	public OperatingCostMethodTaxableValueCalculator(double operatingCosts,
			double privateUseRate, double contributions) {
		this(Money.valueOf(operatingCosts, FbtUtils.DEFAULT_CURRENCY), Ratio
				.of(BigDecimal.valueOf(privateUseRate)), Money.valueOf(
				contributions, FbtUtils.DEFAULT_CURRENCY));
	}

	/* (non-Javadoc)
	 * @see com.innovated.iris.server.fbt.ITaxableValue#calculate()
	 */
	public Money calculate() {
		Money m = operatingCosts.applying(privateUseRate, 2, FbtUtils.DEFAULT_ROUNDING).minus(contributions);
		
		if (m.isNegative()) {
			return FbtUtils.ZERO_MONEY_AMT;
		}
		else {
			return m;
		}
	}

}
