/**
 * 
 */
package com.innovated.iris.server.fbt;

import java.util.Date;

import com.domainlanguage.intervals.Interval;

/**
 * @author tamati
 *
 */
public class AnnualizedKms {
	public static final int CALENDAR_YEAR = 1;
	public static final int FINANCIAL_YEAR = 2;
	public static final int FBT_YEAR = 3;
	
	private Interval dateInterval;
	private Interval kmInterval;
	
	@SuppressWarnings("unchecked")
	public AnnualizedKms(Interval dateInterval, Interval kmInterval) {
		if (dateInterval.upperLimit().compareTo(dateInterval.lowerLimit()) < 0) {
			throw new IllegalArgumentException("End date cannot be before start date");
		}
		if (kmInterval.upperLimit().compareTo(kmInterval.lowerLimit()) < 0) {
			throw new IllegalArgumentException("End odometer cannot be less than start odometer");
		}
		
		this.dateInterval = dateInterval;
		this.kmInterval = kmInterval;
	}
	
	public AnnualizedKms(Date startDate, int startKms, Date endDate, int endKms) {
		this(Interval.closed(startDate, endDate), Interval.closed(Integer
				.valueOf(startKms), Integer.valueOf(endKms)));
	}
	
	public int calculate(int type) {
		int kmsTravelled = ((Integer) kmInterval.upperLimit()).intValue() - ((Integer) kmInterval.lowerLimit()).intValue();
		int daysInPeriod = FbtUtils.getDaysInDateInterval(dateInterval);
		int daysInYear = 0;
		
		switch (type) {
			case CALENDAR_YEAR:
				daysInYear = FbtUtils.getDaysInCalendarYear((Date) dateInterval.upperLimit());
				break;
			case FINANCIAL_YEAR:
				daysInYear = FbtUtils.getDaysInFinancialYear((Date) dateInterval.upperLimit());
				break;
			default:	// FBT_YEAR is the default
				daysInYear = FbtUtils.getDaysInFbtYear((Date) dateInterval.upperLimit());
				break;
		}
		
		double tmp = (double)kmsTravelled / (double)daysInPeriod * (double)daysInYear;
		return (int) Math.floor(tmp);
	}
}
