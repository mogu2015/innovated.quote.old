/**
 * 
 */
package com.innovated.iris.server.fbt;

import java.math.BigDecimal;

import com.domainlanguage.base.Ratio;
import com.domainlanguage.money.Money;

/**
 * @author tamati
 *
 */
public class StatutoryMethodTaxableValueCalculator implements ITaxableValue {

	private Money baseValue;
	private Ratio statutoryPercentage;
	private Ratio daysHeldRatio;
	private Money contributions;
	
	public StatutoryMethodTaxableValueCalculator(Money baseValue,
			Ratio statutoryPercentage, Ratio daysHeldRatio, Money contributions) {
		this.baseValue = baseValue;
		this.statutoryPercentage = statutoryPercentage;
		this.daysHeldRatio = daysHeldRatio;
		this.contributions = contributions;
	}

	public StatutoryMethodTaxableValueCalculator(double baseValue, double statutoryPercentage,
			int daysAvailable, int daysInFbtYear, double contributions) {
		this(Money.valueOf(baseValue, FbtUtils.DEFAULT_CURRENCY), Ratio.of(BigDecimal.valueOf(statutoryPercentage)),
				Ratio.of(daysAvailable, daysInFbtYear), Money.valueOf(contributions, FbtUtils.DEFAULT_CURRENCY));
	}


	/* (non-Javadoc)
	 * @see com.innovated.iris.server.fbt.ITaxableValue#calculate()
	 */
	public Money calculate() {
		Money m = baseValue.applying(statutoryPercentage, 2,
				FbtUtils.DEFAULT_ROUNDING).applying(daysHeldRatio, 2,
				FbtUtils.DEFAULT_ROUNDING).minus(contributions);
		
		if (m.isNegative()) {
			return FbtUtils.ZERO_MONEY_AMT;
		}
		else {
			return m;
		}
	}

}
