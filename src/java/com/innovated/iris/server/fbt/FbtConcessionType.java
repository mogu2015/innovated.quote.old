package com.innovated.iris.server.fbt;

import com.innovated.iris.services.RatesService;

import java.util.*;

/**
 * @author tamati
 */
public enum FbtConcessionType {

    NO_CONCESSION(1, "No FBT Concessions", 0, false),
    EXEMPT_17K_CAP(2, "FBT Exemption ($17,667 capping threshold)", 17667, false),
    EXEMPT_30K_CAP(3, "FBT Exemption ($31,177 capping threshold)", 31177, false),
    REBATE_30K_CAP(4, "FBT Rebate ($31,177 capping threshold)", 31177, true);

    private static RatesService ratesService;

    private static final Map<Integer, FbtConcessionType> lookup = new HashMap<Integer, FbtConcessionType>();

    static {
        for (FbtConcessionType s : EnumSet.allOf(FbtConcessionType.class))
            lookup.put(s.getCode(), s);
    }

    private Integer code;
    private String name;
    private Integer cap;
    private Boolean rebatable;

    private FbtConcessionType(Integer code, String name, Integer cap, Boolean rebatable) {
        this.code = code;
        this.name = name;
        this.cap = cap;
        this.rebatable = rebatable;
    }
    public Integer getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    public Integer getCap() {
        return cap;
    }

    public Boolean isRebatable() {
        return rebatable;
    }

    public static FbtConcessionType get(Integer code) {
        return lookup.get(code);
    }

    @Override
    public String toString() {
        return name;
    }
}

