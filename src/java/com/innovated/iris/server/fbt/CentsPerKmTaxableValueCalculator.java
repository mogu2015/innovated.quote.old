/**
 * 
 */
package com.innovated.iris.server.fbt;

import com.domainlanguage.money.Money;

/**
 * @author tamati
 *
 */
public class CentsPerKmTaxableValueCalculator implements ITaxableValue {

	private double dollarsPerKm;
	private int totalKms;
	private Money contribution;
	
	public CentsPerKmTaxableValueCalculator(double dollarsPerKm, int totalKms, double contributions) {
		this.dollarsPerKm = dollarsPerKm;
		this.totalKms = totalKms;
		this.contribution = Money.valueOf(contributions, FbtUtils.DEFAULT_CURRENCY);
	}
	
	/* (non-Javadoc)
	 * @see com.innovated.iris.server.fbt.ITaxableValue#calculate()
	 */
	public Money calculate() {
		double tmp = dollarsPerKm * totalKms;
		Money m = Money.valueOf(tmp, FbtUtils.DEFAULT_CURRENCY).minus(contribution);
		
		if (m.isNegative()) {
			return FbtUtils.ZERO_MONEY_AMT;
		}
		else {
			return m;
		}
	}

}
