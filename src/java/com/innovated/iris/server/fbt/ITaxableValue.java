/**
 * 
 */
package com.innovated.iris.server.fbt;

import com.domainlanguage.money.Money;

/**
 * @author tamati
 *
 */
public interface ITaxableValue {
	public Money calculate();
}
