/**
 * 
 */
package com.innovated.iris.server.fbt;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author tamati
 *
 */
public enum PackagingComparisonItemType {
	GROSS_SALARY     (10, "Gross Income", "Gross Salary", false, false, 0,  0, true),
	
	PRE_TAX_VEH_EXP  (15, "Pre-Tax", "Vehicle (Pre-Tax)",     false, false,  0,  0, false),
	PRE_TAX_VEH_FBT  (20, "Pre-Tax", "Vehicle FBT",          false, false,  0,  0, false),
	PRE_TAX_VEH_LUX  (25, "Pre-Tax", "Vehicle LUX",          false, false,  0,  0, false),
	//PRE_TAX_VEH_TOTAL(30, "Pre-Tax", "Vehicle Total",        false, true,  15, 29, false),
	PRE_TAX_EMP_SHARE(35, "Pre-Tax", "Employer Share",       false, false,  0,  0, false),
	PRE_TAX_EMP_ADMIN(40, "Pre-Tax", "Employer Admin Fee",   false, false,  0,  0, false),
	//PRE_TAX_EMP_TOTAL(45, "Pre-Tax", "Employer Share Total", false, true,  35, 44, false),
	//PRE_TAX_OTHER    (50, "Pre-Tax", "Pre-Tax Other",        false, false,  0,  0, false),
	//PRE_TAX_TOTAL    (55, "Pre-Tax", "Pre-Tax Total",        false,  true,  15, 54, false),
	
	TAXABLE_SALARY   (60, "Taxable Income", "Taxable Salary", true, false, 1, 59, false),
	
	TAXES_INCOME     (65, "Taxes", "Income Tax",     false, false,  0,  0, false),
	TAXES_MCARE_LEVY (70, "Taxes", "Medicare Levy",  false, false,  0,  0, false),
	//TAXES_HECS_HELP  (75, "Taxes", "HECS/HELP Debt", false, false,  0,  0, false),
	//TAXES_OTHER      (80, "Taxes", "Taxes Other",    false, false,  0,  0, false),
	TAXES_TOTAL      (85, "Taxes", "Taxes Total",    false, true,  65, 84, false),
	
	POST_TAX_VEH_EXP (90,  "Post-Tax", "Vehicle (Post-Tax)", false, false,  0,  0, false),
	//POST_TAX_OTHER   (95,  "Post-Tax", "Post-Tax Other",   false, false,  0,  0, false),
	//POST_TAX_TOTAL   (100, "Post-Tax", "Post-Tax Total",   false, true,  90, 99, false),
	
	NET_SALARY       (105, "Net Income", "Net Salary", true, false, 1, 104, false);

	
    private static final Map<Integer,PackagingComparisonItemType> lookup = new HashMap<Integer,PackagingComparisonItemType>();
	static {
         for (PackagingComparisonItemType s : EnumSet.allOf(PackagingComparisonItemType.class))
              lookup.put(s.getCode(), s);
    }

    private Integer code;
    private String group;
    private String name;
	private Boolean isTotal;
	private Boolean isSubTotal;
	private Integer totalBegin, totalEnd;
	private Boolean isCredit;

    private PackagingComparisonItemType(Integer code, String group, String name, Boolean isTotal,
    		Boolean isSubTotal, Integer totalBegin, Integer totalEnd, Boolean isCredit) {
         this.code = code;
         this.group = group;
         this.name = name;
         this.isTotal = isTotal;
         this.isSubTotal = isSubTotal;
         this.totalBegin = totalBegin;
         this.totalEnd = totalEnd;
         this.isCredit = isCredit;
    }

    public Integer getCode()       { return code; }
    public String getGroup()       { return group; }
    public String getName()        { return name; }
    public Boolean isTotal()       { return isTotal; }
    public Boolean isSubTotal()    { return isSubTotal; }
    public Integer getTotalBegin() { return totalBegin; }
    public Integer getTotalEnd()   { return totalEnd; }
    public Boolean isCredit()      { return isCredit; }

    public static List<PackagingComparisonItemType> getTypes() {
    	List<PackagingComparisonItemType> l = new ArrayList<PackagingComparisonItemType>();
    	for (PackagingComparisonItemType s : EnumSet.allOf(PackagingComparisonItemType.class))
        	l.add(s);
    	return l;
    }
    
    public static PackagingComparisonItemType get(Integer code) { 
         return lookup.get(code); 
    }
    
    @Override
    public String toString() {
    	return name;
    }
}
