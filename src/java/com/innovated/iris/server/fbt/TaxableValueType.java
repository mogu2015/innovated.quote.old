/**
 *
 */
package com.innovated.iris.server.fbt;

import java.util.*;

/**
 * @author tamati
 */
public enum TaxableValueType {
    STATUTORY(1, "Statutory Formula"),
    OP_COST(2, "Operating Cost"),
    FBT_EXEMPT(3, "FBT Exempt"),
    CENTS_PER_KM_MC(4, "Cents/Km, Motor cycles"),
    CENTS_PER_UPTO_2500CC(5, "Cents/Km, 0 - 2500cc"),
    CENTS_PER_OVER_2500CC(6, "Cents/Km, Over 2500cc");

    private static final Map<Integer, TaxableValueType> lookup = new HashMap<Integer, TaxableValueType>();

    static {
        for (TaxableValueType s : EnumSet.allOf(TaxableValueType.class))
            lookup.put(s.getCode(), s);
    }

    private int code;
    private String name;

    private TaxableValueType(int code, String name) {
        this.code = code;
        this.name = name;
    }

    public int getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    public static TaxableValueType get(int code) {
        return lookup.get(code);
    }

    @Override
    public String toString() {
        return name;
    }
}

