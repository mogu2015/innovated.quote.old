/**
 * 
 */
package com.innovated.iris.server.fbt;

import java.util.Date;
import com.domainlanguage.intervals.Interval;

/**
 * @author tamati
 *
 */
public class FbtRecord {
	private Interval fbtYear;
	private Interval fbtPeriod;
	private boolean rebateable;
	private boolean employerClaimsGST;
	private ITaxableValue tvCalculator;
	private IRebateable rebateCalculator;
	private GrossUpRate grossUp;
	
	public FbtRecord(Date fbtPeriodStart, Date fbtPeriodEnd) {
		fbtPeriod = Interval.closed(fbtPeriodStart, fbtPeriodEnd);
	}
		
	public Interval getFbtYear() {
		return fbtYear;
	}

	public Interval getFbtPeriod() {
		return fbtPeriod;
	}

	public boolean isRebateable() {
		return rebateable;
	}

	public void setRebateable(boolean rebateable) {
		this.rebateable = rebateable;
	}

	public ITaxableValue getTvCalculator() {
		return tvCalculator;
	}

	public void setTvCalculator(ITaxableValue tvCalculator) {
		this.tvCalculator = tvCalculator;
	}

	public IRebateable getRebateCalculator() {
		return rebateCalculator;
	}

	public void setRebateCalculator(IRebateable rebateCalculator) {
		this.rebateCalculator = rebateCalculator;
	}
	
	public double getGrossUpRate() {
		if (employerClaimsGST) {
			return grossUp.type1();
		}
		else {
			return grossUp.type2();
		}
	}
	
	public double getFbtRate() {
		return grossUp.getFbtRate().decimalValue(3, FbtUtils.DEFAULT_ROUNDING).doubleValue();
	}

	public boolean isEmployerClaimsGST() {
		return employerClaimsGST;
	}

	public void setEmployerClaimsGST(boolean employerClaimsGST) {
		this.employerClaimsGST = employerClaimsGST;
	}

	public void setGrossUp(GrossUpRate grossUp) {
		this.grossUp = grossUp;
	}
	
}
