/**
 * 
 */
package com.innovated.iris.server.fbt;

/**
 * @author tamati
 *
 */
public class TaxableValueCalculatorFactory {

	public static final String LOW_VALUE = "low_value";
	public static final String HIGH_VALUE = "high_value";
	public static final String VALUE = "value";
	public static final String PERCENTAGE = "percentage";
	
	public static ITaxableValue getCalculator(TaxableValueType type) {
		/*ITaxableValue tv = null;
		
		switch (type) {
		case CENTS_PER_OVER_2500CC:
			tv = new CentsPerKmTaxableValueCalculator(dollarsPerKm, totalKms, contribution);
			break;
		case CENTS_PER_UPTO_2500CC:
			tv = new CentsPerKmTaxableValueCalculator(dollarsPerKm, totalKms, contribution);		
			break;
		case CENTS_PER_KM_MC:
			tv = new CentsPerKmTaxableValueCalculator(dollarsPerKm, totalKms, contribution);
			break;
		case FBT_EXEMPT:
			tv = new FbtExemptTaxableValueCalculator();
			break;
		case OP_COST:
			tv = new OperatingCostMethodTaxableValueCalculator(operatingCosts, privateUseRate, contributions);
			break;
		default:	// return Statutory by default
			tv = new StatutoryMethodTaxableValueCalculator();
			break;
		}
		
		return tv;*/
		
		return new FbtExemptTaxableValueCalculator();
	}
	
}
