/**
 * 
 */
package com.innovated.iris.server.fbt;

import java.math.BigDecimal;

import com.domainlanguage.base.Ratio;


/**
 * @author tamati
 *
 */
public class GrossUpRate {
	private Ratio fbtRate;
	private Ratio gstRate;
	
	/**
	 * @param in
	 */
	public GrossUpRate(Ratio fbtRate, Ratio gstRate) {
		this.fbtRate = fbtRate;
		this.gstRate = gstRate;
	}
	
	public GrossUpRate(double fbtRate, double gstRate) {
		this(Ratio.of(BigDecimal.valueOf(fbtRate)), Ratio.of(BigDecimal.valueOf(gstRate)));
	}
	
	public double type1() {
		BigDecimal fbtRate = this.fbtRate.decimalValue(3, FbtUtils.DEFAULT_ROUNDING);
		BigDecimal gstRate = this.gstRate.decimalValue(3, FbtUtils.DEFAULT_ROUNDING);
		
		BigDecimal n = fbtRate.add(gstRate);
		BigDecimal d1 = BigDecimal.ONE.subtract(fbtRate);
		BigDecimal d2 = BigDecimal.ONE.add(gstRate);
		BigDecimal d = d1.multiply(d2).multiply(fbtRate);
		
		return n.divide(d, FbtUtils.GROSS_UP_SCALE, FbtUtils.DEFAULT_ROUNDING).doubleValue();
	}
	
	public double type2() {
		BigDecimal fbtRate = this.fbtRate.decimalValue(3, FbtUtils.DEFAULT_ROUNDING);
		BigDecimal n = BigDecimal.ONE;
		BigDecimal d = BigDecimal.ONE.subtract(fbtRate);
		return n.divide(d, FbtUtils.GROSS_UP_SCALE, FbtUtils.DEFAULT_ROUNDING).doubleValue();
	}

	public Ratio getFbtRate() {
		return fbtRate;
	}

	public void setFbtRate(Ratio fbtRate) {
		this.fbtRate = fbtRate;
	}

	public Ratio getGstRate() {
		return gstRate;
	}

	public void setGstRate(Ratio gstRate) {
		this.gstRate = gstRate;
	}

}
