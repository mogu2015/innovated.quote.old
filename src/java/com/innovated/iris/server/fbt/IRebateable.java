/**
 * 
 */
package com.innovated.iris.server.fbt;

import com.domainlanguage.money.Money;

/**
 * @author tamati
 *
 */
public interface IRebateable {
	public Money calculate();
}
