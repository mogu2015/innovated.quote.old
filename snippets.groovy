// pseudo-code for the budget quote process...

def calculateAnnualAmountFuel(int kmPerAnnum, double litresPer100Km, double dollarsPerLitre) {
	double annualAmount = (((double) kmPerAnnum / 100.0) * litresPer100Km) / dollarsPerLitre
	// create Money object from annual amount
	return annualAmount
}






/*    url: /customer/edit    */

<g:render template="/templates/ui/customer/customerdetails" />
<g:render template="/templates/ui/customer/viewentityinfo" />
<g:if test="${customerInstance.entity.type == 'Company'}">
	<g:render template="/templates/ui/customer/employeelist" />
</g:if>
<g:render template="/templates/ui/customer/customerassets" />