package com.innovated.iris.domain

import com.innovated.iris.domain.enums.VehicleManufacturer;
import com.innovated.iris.domain.enums.VehicleModel;

import grails.test.*

class MakeModelTests extends GrailsUnitTestCase {
	def make
	def model
	
	protected void setUp() {
		super.setUp()
		mockDomain(VehicleManufacturer)
		make = new VehicleManufacturer(name:"Mazda")
		mockDomain(VehicleModel)
		model = new VehicleModel(name:"3", make:make)
	}
	
	protected void tearDown() {
		super.tearDown()
	}
	
	void testValidVehicleModel() {
		def m = new MakeModel(model:model)
		mockForConstraintsTests(MakeModel, [m])

		assertFalse m.validate()
		assertEquals 'VehicleModel cannot be NULL', 'nullable', m.errors['model']
	}
}
