package com.innovated.iris.services

import grails.test.*

class GeocoderServiceTests extends GrailsUnitTestCase {
    def geocoderService
    
    protected void setUp() {
        super.setUp()
        
        geocoderService = new GeocoderService()
    }

    protected void tearDown() {
        super.tearDown()
    }

    void testGetLatLongWithGoodAddress() {
		def result = geocoderService.getLatLong("60 Junction Rd, Leumeah NSW 2560 Australia")
		log.info("result = " + result)
		assertTrue result.latLngValid
    }
    
    void testGetLatLongWithGoodAddressOutsideAustralia() {
		def result = geocoderService.getLatLong("skyline dr salt lake city utah USA")
		log.info("result = " + result)
		assertFalse result.latLngValid
    }
    
    void testGetLatLongWithBadAddress() {
		def result = geocoderService.getLatLong("v fdg ddgh")
		log.info("result = " + result)
		assertFalse result.latLngValid
    }
}
