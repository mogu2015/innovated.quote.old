package com.innovated.iris.services

import grails.test.*

class UserServiceTests extends GrailsUnitTestCase {
    protected void setUp() {
        super.setUp()
    }

    protected void tearDown() {
        super.tearDown()
    }

    void testCustomerCodeGeneration() {
		def u = new UserService()
		def code = u.getCustomerCode("Paki, Tamati")
		println "cust_code = ${code}"
		assertEquals("CPAKI001", code)
    }
}
