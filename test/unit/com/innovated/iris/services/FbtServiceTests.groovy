package com.innovated.iris.services

import grails.test.*

import com.innovated.iris.util.*
import com.innovated.iris.domain.*
import com.innovated.iris.domain.enums.*
import com.innovated.iris.domain.lookup.*
import com.innovated.iris.server.fbt.*

import com.domainlanguage.base.Ratio;
import com.domainlanguage.money.Money;

class FbtServiceTests extends GrailsUnitTestCase {
    protected void setUp() {
        super.setUp()
    }

    protected void tearDown() {
        super.tearDown()
    }

	//======== Test FBT payable using "Money" arg
    void testFbtPayableMoney() {
		def s = new FbtService()
		Money m = s.fbtPayable(new Date(), FbtConcessionType.NO_CONCESSION, FbtUtils.ZERO_MONEY_AMT, true)
		assertEquals(FbtUtils.ZERO_MONEY_AMT, m)
    }
    //======== Test FBT payable using "double" arg
    void testFbtPayableDouble() {
		def s = new FbtService()
		double m = s.fbtPayable(new Date(), FbtConcessionType.NO_CONCESSION, 0.0, true)
		assertEquals(0.0, m)
    }
    
    //======== Test "No Concession"
    void testFbtPayableNoCessionCanClaimGst() {
		def s = new FbtService()
		double m = s.fbtPayable(new Date(), FbtConcessionType.NO_CONCESSION, 1000, true)
		assertEquals(960.09, m)
    }
    void testFbtPayableNoCessionCannotClaimGst() {
		def s = new FbtService()
		double m = s.fbtPayable(new Date(), FbtConcessionType.NO_CONCESSION, 1000, false)
		assertEquals(869.18, m)
    }
    
    //======== Test "$17,000 FBT free threshold"
    void testFbtPayable17kThresholdCanClaimGst() {
		def s = new FbtService()
		double m = s.fbtPayable(new Date(), FbtConcessionType.EXEMPT_17K_CAP, 1000, true)
		assertEquals(0.0, m)
    }
    void testFbtPayable17kThresholdCannotClaimGst() {
		def s = new FbtService()
		double m = s.fbtPayable(new Date(), FbtConcessionType.EXEMPT_17K_CAP, 1000, false)
		assertEquals(0.0, m)
    }
    void testFbtPayable17kThresholdHighTvCanClaimGst() {
		def s = new FbtService()
		double m = s.fbtPayable(new Date(), FbtConcessionType.EXEMPT_17K_CAP, 40000, true)
		assertEquals(30498.42, m)
    }
    void testFbtPayable17kThresholdHighTvCannotClaimGst() {
		def s = new FbtService()
		double m = s.fbtPayable(new Date(), FbtConcessionType.EXEMPT_17K_CAP, 40000, false)
		assertEquals(26862.12, m)
    }
    
    //======== Test "$30,000 FBT free threshold"
    void testFbtPayable30kThresholdCanClaimGst() {
		def s = new FbtService()
		double m = s.fbtPayable(new Date(), FbtConcessionType.EXEMPT_30K_CAP, 1000, true)
		assertEquals(0.0, m)
    }
    void testFbtPayable30kThresholdCannotClaimGst() {
		def s = new FbtService()
		double m = s.fbtPayable(new Date(), FbtConcessionType.EXEMPT_30K_CAP, 1000, false)
		assertEquals(0.0, m)
    }
    void testFbtPayable30kThresholdHighTvCanClaimGst() {
		def s = new FbtService()
		double m = s.fbtPayable(new Date(), FbtConcessionType.EXEMPT_30K_CAP, 40000, true)
		assertEquals(24453.42, m)
    }
    void testFbtPayable30kThresholdHighTvCannotClaimGst() {
		def s = new FbtService()
		double m = s.fbtPayable(new Date(), FbtConcessionType.EXEMPT_30K_CAP, 40000, false)
		assertEquals(20817.12, m)
    }
    
    //======== Test "Rebatable - $30,000 threshold"
    void testFbtPayableRebatable30kCanClaimGst() {
		def s = new FbtService()
		double m = s.fbtPayable(new Date(), FbtConcessionType.REBATE_30K_CAP, 1000, true)
		assertEquals(499.25, m)
    }
    void testFbtPayableRebatable30kCannotClaimGst() {
		def s = new FbtService()
		double m = s.fbtPayable(new Date(), FbtConcessionType.REBATE_30K_CAP, 1000, false)
		assertEquals(451.97, m)
    }
    void testFbtPayableRebatable30kHighTvCanClaimGst() {
		def s = new FbtService()
		double m = s.fbtPayable(new Date(), FbtConcessionType.REBATE_30K_CAP, 40000, true)
		assertEquals(31707.42, m)
    }
    void testFbtPayableRebatable30kHighTvCannotClaimGst() {
		def s = new FbtService()
		double m = s.fbtPayable(new Date(), FbtConcessionType.REBATE_30K_CAP, 40000, false)
		assertEquals(28071.12, m)
    }
	
	//============= Taxable value tests
	void testTaxableValue() {
		//
		def tenthMay2011       = new GregorianCalendar(2011, Calendar.MAY,    10).time
		def thirtyFirstMar2012 = new GregorianCalendar(2012, Calendar.MARCH,  31).time
		
		FbtStatutoryMethodRateParameter stat = new FbtStatutoryMethodRateParameter(dateFrom:tenthMay2011, commenceStart:tenthMay2011, commenceEnd:thirtyFirstMar2012, customerCode:Customer.BASE_CUST_CODE, taxableValueTypeCode:TaxableValueType.STATUTORY.getCode(), lowerValue:    0, upperValue: 14999, rate:0.20)
		def otherControl = mockFor(ParameterLookupService) 
		otherControl.demand.lookupStatutoryMethodRate { stat }
		
		def s = new FbtService()
		s.parameterLookupService = otherControl.createMock()
		
		Date now = new Date()
		Money m = s.taxableValue(now, TaxableValueType.STATUTORY, [totalKms:25000, baseValue:25000, daysAvailable:1, daysInFbtYear:1, contributions:0, commenceDate:now])
		assertEquals(FbtUtils.ZERO_MONEY_AMT, m)
	}
	
	/*new FbtStatutoryMethodRateParameter(dateFrom:asAtDate, customerCode:Customer.BASE_CUST_CODE, taxableValueTypeCode:TaxableValueType.STATUTORY.getCode(), lowerValue:15000, upperValue: 24999, rate:0.20)
	new FbtStatutoryMethodRateParameter(dateFrom:asAtDate, customerCode:Customer.BASE_CUST_CODE, taxableValueTypeCode:TaxableValueType.STATUTORY.getCode(), lowerValue:25000, upperValue: 40000, rate:0.11)
	new FbtStatutoryMethodRateParameter(dateFrom:asAtDate, customerCode:Customer.BASE_CUST_CODE, taxableValueTypeCode:TaxableValueType.STATUTORY.getCode(), lowerValue:40001, upperValue:999999, rate:0.07)*/
    
}

